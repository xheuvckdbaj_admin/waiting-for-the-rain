# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="upnext"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.ahsrst.cn/a/202106/434032.html',
                'https://www.ahsrst.cn/a/202111/493441.html',
                'https://www.ahsrst.cn/a/201505/41938.html',
                'https://www.ahsrst.cn/a/202111/493221.html',
                'https://www.ahsrst.cn/a/202107/439448.html',
                'https://www.ahsrst.cn/a/202111/493249.html',
                'https://www.ahsrst.cn/a/202110/486780.html',
                'https://www.ahsrst.cn/a/202111/493455.html',
                'https://www.ahsrst.cn/a/201609/190172.html',
                'https://www.ahsrst.cn/a/202108/456237.html',
                'https://www.ahsrst.cn/a/202111/493111.html',
                'https://www.ahsrst.cn/a/202111/493001.html',
                'https://www.ahsrst.cn/a/202111/494047.html',
                'https://www.ahsrst.cn/a/202111/492982.html',
                'https://www.ahsrst.cn/a/202111/491233.html',
                'https://www.ahsrst.cn/a/202111/490776.html',
                'https://www.ahsrst.cn/a/202111/493389.html',
                'https://www.ahsrst.cn/a/202105/426320.html',
                'https://www.ahsrst.cn/a/202111/493003.html',
                'https://www.ahsrst.cn/a/202111/493199.html',
                'https://www.ahsrst.cn/a/202108/462404.html',
                'https://www.ahsrst.cn/a/202109/476047.html',
                'https://www.ahsrst.cn/a/202111/493257.html',
                'https://www.ahsrst.cn/a/202111/493379.html',
                'https://www.ahsrst.cn/a/202111/493222.html',
                'https://www.ahsrst.cn/a/201706/299739.html',
                'https://www.ahsrst.cn/a/202111/493411.html',
                'https://www.ahsrst.cn/a/202111/493155.html',
                'https://www.ahsrst.cn/a/202111/493328.html',
                'https://www.ahsrst.cn/a/202111/493433.html',
                'https://www.ahsrst.cn/a/202111/493440.html',
                'https://www.ahsrst.cn/a/202111/494051.html',
                'https://www.ahsrst.cn/a/202111/493212.html',
                'https://www.ahsrst.cn/a/202111/492936.html',
                'https://www.ahsrst.cn/a/202111/493223.html',
                'https://www.ahsrst.cn/a/202108/453140.html',
                'https://www.ahsrst.cn/a/202111/493492.html',
                'https://www.ahsrst.cn/a/202107/447930.html',
                'https://www.ahsrst.cn/a/201706/291342.html',
                'https://www.ahsrst.cn/a/202111/494036.html',
                'https://www.ahsrst.cn/a/202111/493414.html',
                'https://www.ahsrst.cn/a/202111/492962.html',
                'https://www.ahsrst.cn/a/202106/433964.html',
                'https://www.ahsrst.cn/a/201710/323224.html',
                'https://www.ahsrst.cn/a/202109/472753.html',
                'https://www.ahsrst.cn/a/202111/493963.html',
                'https://www.ahsrst.cn/a/202109/476165.html',
                'https://www.ahsrst.cn/a/202111/492383.html',
                'https://www.ahsrst.cn/a/202111/494029.html',
                'https://www.ahsrst.cn/a/202111/493962.html',
                'https://www.ahsrst.cn/a/202111/493312.html',
                'https://www.ahsrst.cn/a/202111/492644.html',
                'https://www.ahsrst.cn/a/201706/294982.html',
                'https://www.ahsrst.cn/a/202111/494091.html',
                'https://www.ahsrst.cn/a/202111/491059.html',
                'https://www.ahsrst.cn/a/202111/494000.html',
                'https://www.ahsrst.cn/a/202111/494098.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
