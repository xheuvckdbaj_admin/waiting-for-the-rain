# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import requests
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.FindFile import find_file
from pythonProject.FindFile import find_file
from pythonProject.b百度上传付费文档.b整理文档.allyunxing import mainzhengli
from pythonProject.b百度上传付费文档.百度上传.readTxt import readTxt
from pythonProject.b百度上传付费文档.百度账号登录.百度安全验证 import anquanyanzheng

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档检查账号是否传满 import \
    mainTitleClassifyUploadApiGuish
from pythonProject.创作活动文档.playwright创作活动文档.删除单个账号已被传过的文档 import mainTitleClassifyUploadDelete
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

from pywinauto import Desktop
from pywinauto.keyboard import *


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser


def panduanTitle(title):
    panduan = True
    if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title or '年级' in title \
            or '图' in title or '.' in title or '×' in title or (len(title) < 5):
        panduan = False
    return panduan


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getNumberPager(browser):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
        return pagerNumber
    except:
        return 0


def deleteWord(folderPath, title):
    filePath = folderPath + '\\' + title + '.docx'
    try:
        os.remove(filePath)
    except:
        print('无法删除' + folderPath)


def ctrlx(oldFilePaths, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
    for oldFilePath in oldFilePaths:
        try:
            shutil.move(oldFilePath, newPath)
        except:
            print('剪切不掉' + oldFilePath)
            try:
                os.remove(oldFilePath)
            except:
                print('删除失败')


def getFileNameStr(folderPath):
    keyStrs = []
    filePaths = find_file(folderPath, [])
    if len(filePaths) >= 1:
        for filePath in filePaths:
            titleStr = os.path.basename(filePath).replace('.docx', '')
            keyStrs.append(titleStr)
    return keyStrs


def on_request(request):
    try:
        htmlPath = r'D:\图片'
        makeFolder(htmlPath)
        title = 'haha'
        # print(f'Request URL: {request.url}')
        # print(f'Request Headers: {request.headers}')
        # if request.post_data is not None:
        #     print(f'Request Body: {request.post_data}')
        if 'https://passport.baidu.com/cap/img?ak=' in request.url:
            requests_text = requests.get(url=request.url, headers=request.headers, timeout=5.0).content
            thmlpath = htmlPath + '\\' + title + '.jpg'
            f = open(thmlpath, 'wb')
            f.write(requests_text)
            # print('图片下载成功----', title)
    except:
        print('图片下载失败')




def clickButton(ele):
    try:
        ele.click()
        time.sleep(0.1)
        ele.click()
        time.sleep(0.1)
        ele.click()
    except:
        print('')


def getNewPage(page, url):
    try:
        page.goto(url,timeout=6000)
        page.wait_for_timeout(500)
        try:
            page.get_by_role("button", name="我知道啦").click()
        except:
            print('')
    except:
        page.wait_for_timeout(500)
        getNewPage(page, url)


def getNewPageMore(page, url):
    try:
        page.goto(url,timeout=6000)
        page.wait_for_timeout(500)
    except:
        page.wait_for_timeout(500)
        getNewPageMore(page, url)


def submitword(page, file_chooser, fileDocxPaths, successFloder):
    # 设置请求监听器
    page.on('request', on_request)
    file_chooser.set_files(fileDocxPaths)
    app = Desktop()
    dialog = app['打开']
    # dialog.print_control_identifiers()
    # dialog.print_control_identifiers(depth=None, filename=None)
    # dialog['Edit'].type_keys(wordFilePath, with_spaces=True)
    clickButton(dialog['Button2'])
    panduan = False
    panduan = anquanyanzheng(page, panduan)
    if panduan == False:

        try:
            if len(fileDocxPaths) >1:
                page.wait_for_timeout(7000)
                vipele = page.locator('//div[@class="selected-all-box"]/label/span/span')
                buttonele = page.get_by_role('button', name='确认提交')
                if not vipele.count() == 0 and (not buttonele.count() == 0):
                    page.locator('//div[@class="selected-all-box"]/label/span/span').click()
                    page.set_default_timeout(1000)
                    page.get_by_role('button', name='批量修改').click()
                    page.set_default_timeout(1000)
                    page.get_by_role("radio", name="VIP文档").click()
                    page.set_default_timeout(1000)
                    page.get_by_role('button', name='保存修改').click()
                    page.set_default_timeout(1000)
                    page.get_by_role('button', name='确认提交').click()
                    print('上传成功：' + str(fileDocxPaths))
                    # filePathxs = find_file(folderPath, [])
                    # print('已经成功上传' + str(dataNumber) + '个文档')
                    # dataNumber1 = len(filePathxs)
                    # print('此次成功上传' + str(dataNumber1) + '个文档')
                    # together = dataNumber + dataNumber1
                    # print('一共成功上传' + str(together) + '个文档')
                    # longx = dataNumber1
                    # for filePathx in filePathxs:
                    ctrlx(fileDocxPaths, successFloder)
            else:
                page.wait_for_timeout(4000)
                vipele = page.get_by_role("radio", name="VIP文档")
                buttonele = page.get_by_role('button', name='确认提交')
                if not vipele.count() == 0 and (not buttonele.count() == 0):
                    page.get_by_role("radio", name="VIP文档").click()
                    page.set_default_timeout(1000)
                    page.get_by_role('button', name='确认提交').click()
                    print('上传成功：' + str(fileDocxPaths))
                    ctrlx(fileDocxPaths, successFloder)
        except:
            print('上传失败')


def deleteFile(wordFolderPath):
    filePaths = find_file(wordFolderPath)
    for filepath in filePaths:
        try:
            os.remove(filepath)
        except:
            print('删除失败', filepath)


def singlyUpload(page, titleFolderPath, cookiesPath, successFloder, i, uploadPanduan):
    xpathStrButton = '//div[@class="upload-doc btn-posution"]/div[@class="add-new-btn add-new-btn-right"]/button'
    xpathStrEle = '//button[@class="el-button add-new-btn add-new-btn-right el-button--primary el-button--mini"]'
    try:
        if page.locator(xpathStrButton).count() == 0 and page.locator(xpathStrEle).count() == 1:
            print('上传完毕')
            uploadPanduan = True
        else:
            page.locator(xpathStrButton).nth(0).first.click()
            with page.expect_file_chooser() as fc_info:
                page.locator(xpathStrButton).nth(0).first.click()
            panduanUpload = True
            file_chooser = fc_info.value
            fc_info.is_done()
            fileDocxPath = titleFolderPath
            submitword(page, file_chooser, fileDocxPath, successFloder)
        return uploadPanduan
    except:
        print('上传失败')


def uploadFile(page, titleFolderPath, cookiesPath, successFloder, i, number, uploadPanduan):
    url = 'https://cuttlefish.baidu.com/shopmis?_wkts_=1679576168528&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation'
    if (i + 1) == number:
        panduanUpload = False
        getNewPage(page, url)
    else:
        getNewPageMore(page, url)
    singlyUpload(page, titleFolderPath, cookiesPath, successFloder, i, uploadPanduan)


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1679576742834&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1679576742834&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.get_by_role("button", name="Close").click()
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page


def mainTitleClassifyUpload(playwright: Playwright, folderPath, cookiesPath, successFloder, number) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()

    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    titleFolderPaths = os.listdir(folderPath)
    time.sleep(1)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    if not len(titleFolderPaths) == 0:
        uploadPanduan = False
        oneFilePaths = []
        for i, titleFolderPathName in enumerate(titleFolderPaths):
            if i < 65:
                titleFolderPath = os.path.join(folderPath, titleFolderPathName)
                oneFilePaths.append(titleFolderPath)
                if (i + 1) % number == 0:
                    uploadFile(page, oneFilePaths, cookiesPath, successFloder, i, number, uploadPanduan)
                    if uploadPanduan == True:
                        break
                    oneFilePaths = []
            if i>=60 and i<120:
                titleFolderPath = os.path.join(folderPath, titleFolderPathName)
                oneFilePaths.append(titleFolderPath)
                uploadFile(page, oneFilePaths, cookiesPath, successFloder, i, number, uploadPanduan)
                if uploadPanduan == True:
                        break
                oneFilePaths = []


def mainTitleClassifyUploadApi(folderPath, cookiesPath, successFloder):
    number = 7
    with sync_playwright() as playwright:
        mainTitleClassifyUpload(playwright, folderPath, cookiesPath, successFloder, number)
    # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


def main():
    folderPath = r'D:\文档\百度付费上传文档\修改后百度活动文档'
    # panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    successFloder = r'D:\文档\百度付费上传文档\百度上传成功'
    cookiesPath1 = './baiduCookies1.txt'
    cookiesPath2 = './baiduCookies2.txt'
    cookiesPath3 = './baiduCookies3.txt'
    cookiesPath4 = './baiduCookies4.txt'
    cookiesPath5 = './baiduCookies5.txt'
    cookiesPath6 = './baiduCookies6.txt'
    cookiesPath7 = './baiduCookies7.txt'
    cookiesPath8 = './baiduCookies8.txt'
    cookiesPath9 = './baiduCookies9.txt'
    cookiesPath10 = './baiduCookies10.txt'
    cookiesPath11 = './baiduCookies11.txt'
    cookiesPath12 = './baiduCookies12.txt'
    cookiesPath13 = './baiduCookies13.txt'
    cookiesPath14 = './baiduCookies14.txt'
    cookiesPath15 = './baiduCookies15.txt'
    cookiesPath16 = './baiduCookies16.txt'
    cookiesPath17 = './baiduCookies17.txt'
    cookiesPath18 = './baiduCookies18.txt'
    cookiesPath19 = './baiduCookies19.txt'
    cookiesPath20 = './baiduCookies20.txt'
    cookiesPath21 = './baiduCookies21.txt'
    cookiesPath22 = './baiduCookies22.txt'
    cookiesPath23 = './baiduCookies23.txt'
    cookiesPath24 = './baiduCookies24.txt'
    cookiesPath25 = './baiduCookies25.txt'
    print('------------------账号1开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath1, successFloder)
    print('------------------账号2开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath2,successFloder)
    print('------------------账号3开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath3,successFloder)
    print('------------------账号4开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath4,successFloder)
    print('------------------账号5开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath5,successFloder)
    print('------------------账号6开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath6,successFloder)
    print('------------------账号7开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath7,successFloder)
    print('------------------账号8开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath8,successFloder)
    print('------------------账号9开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath9,successFloder)
    print('------------------账号10开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath10,successFloder)
    print('------------------账号11开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath11,successFloder)
    print('------------------账号12开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath12,successFloder)
    print('------------------账号13开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath13,successFloder)
    print('------------------账号14开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath14,successFloder)
    print('------------------账号15开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath15,successFloder)
    print('------------------账号16开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath16,successFloder)
    print('------------------账号17开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath17,successFloder)
    print('------------------账号18开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath18,successFloder)
    print('------------------账号19开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath19,successFloder)
    print('------------------账号20开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath20,successFloder)
    print('------------------账号21开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath21,successFloder)
    print('------------------账号22开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath22,successFloder)
    print('------------------账号23开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath23,successFloder)
    print('------------------账号24开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath24,successFloder)
    print('------------------账号25开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath25,successFloder)
    time.sleep(40000)


if __name__ == '__main__':
    for i in range(0, 1000):
        mainzhengli()
        main()
