import os
import threading
import time

import openai
from docx import Document
from docx.enum.base import XmlMappedEnumMember
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn

from pythonProject.b百度上传付费文档.使用chatGPT生成文档.chatGPT生成单个文件 import main_chatGPTSingle


def main_chatGPT(file_lines, folderPath, api_key, start, last):
    for i, line in enumerate(file_lines):
        if i >= start and i < last:
            title = line.replace('\n', '')
            main_chatGPTSingle(title, folderPath, api_key)
            time.sleep(20)



if __name__ == '__main__':
    api_key = ["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"]
    # 标题存放文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'
    folderPath = r'D:\文档\百度活动文档\百度html'
    titleFilePath = txtPath + '\\' + 'all' + '.txt'
    file = open(titleFilePath, 'r', encoding='utf-8')
    file_lines = file.readlines()
    startNumber = 0
    last = len(file_lines)
    print(last)

    if last >= 320:
        last_chatNUmber = 320
    else:
        last_chatNUmber = last
    chatAn = int((last_chatNUmber - startNumber) / 5)
    t1 = threading.Thread(target=main_chatGPT,
                          args=(file_lines, folderPath, api_key, 0, startNumber + chatAn))
    t2 = threading.Thread(target=main_chatGPT,
                          args=(file_lines, folderPath, api_key, startNumber + chatAn, startNumber + chatAn * 2))
    t3 = threading.Thread(target=main_chatGPT,
                          args=(file_lines, folderPath, api_key, startNumber + chatAn * 2, startNumber + chatAn * 3))
    t4 = threading.Thread(target=main_chatGPT,
                          args=(file_lines, folderPath, api_key, startNumber + chatAn * 3, startNumber + chatAn * 4))
    t5 = threading.Thread(target=main_chatGPT,
                          args=(file_lines, folderPath, api_key, startNumber + chatAn * 4, last_chatNUmber))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()

    # main_chatGPT(txtPath, folderPath, api_key.txt)
