# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'}




# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #url='http://blog.sina.com.cn/s/blog_4b2cb00e0102ds94.html'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        #print(title)
        #title = str(soup.select('.artical > .articalTitle > h2.titName.SG_txta')[0].text).replace('武书连','').replace('<','').replace('>','')
        content=str(soup.select('.item-desc')[0])

        #print(newContent)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(content)



# 获取最终网址
def getUrl(thmlpath,urls):
    for url in urls:
        requests_text = requests.get(url=url, headers=headers).content
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list=soup.select('.content > .middle-content > .item-link')
        #print(url_list)
        for a in a_list:
                href=a.get('href')
                newUrl='https://guoxue.baike.so.com'+href
                print(newUrl)
                title = a.text.replace('\n','').replace('?','')
                getContent(thmlpath,newUrl,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    print(requests_text)
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.article_blk > .articleList > div.articleCell.SG_j_linedot1 > p.atc_main.SG_dot > .atc_title > a')
    #print(url_list)
    for a in a_list:
            src=a.get('href')
            href=src
            print(href)
            title=a.text
            getContent(thmlpath,href,title)

def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2):


                    oldUrl = url
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)







def getHtml(strUrl, locs):
    urls=[]
    for strName in locs:
            url = strUrl%strName
            for i in range(1,30):
               newUrl=url+str(i)
               urls.append(newUrl)
    return urls



def main(thmlpath):
    strUrl='https://guoxue.baike.so.com/query/index?guoxue_fe_inner=feguoxue&type=gaokao_zuowen&loc=%s&page='
    locs=['hunyinfuwu','xuexiaojiaoyu','shengyufuwu','bingyizhuanye','gongzuofuwu','jiejiafuli','gongzifuwu',
          'shehuibaozhang','renshifuwu',
          'zhufangbaozhang','fangchanjiaoyi','cheliangjiaotong','chujingrujing','jinrongfuwu',
          'wangluoshenghuo','qiyebanshi','yanglaofuwu','fanwenxieyi','changyongzhengming','陕西卷']
    urls=getHtml(strUrl, locs)
    getUrl(thmlpath, urls)
    print(urls)



if __name__ == '__main__':
    thmlpath = r'E:\高考作文'
    main(thmlpath)
