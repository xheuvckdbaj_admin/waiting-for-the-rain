# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('#my')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.richtitle > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.con_list_2 > ul > li > a')
    for i,a in enumerate(a_list):
        try:
            href=a.get('href')
            print(href)
            getHtml(thmlpath, href)
        except:
            print('error')
            continue

def getPageUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('ul.pagelist > a')[-1].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    hrefUrl=href.replace(href.split('_')[-1],'')
    for i in range(1,pageNumber+1):
        newUrl=url+hrefUrl+str(i)+'.html'
        getListUrl(thmlpath, newUrl)
def getLawTitle(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.daohang > ul > li > a')
    for i, a in enumerate(a_list):
        href = a.get('href')
        newUrl = href
        try:
            getPageUrl(thmlpath,newUrl)
        except:
            print('只有一页')
            getListUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('div.hdright > ul > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl=href
        getLawTitle(thmlpath, newUrl)
def main(thmlpath):
#https://www.chinawenwang.com/
#www.01hn.com
                url='http://www.fanwenmuban.com/'
                getTitleUrl(thmlpath,url)



if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
