# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.b百度上传付费文档.百度搜索爬寻.网站选择 import paywebsiteJude
from pythonProject.创作活动文档.方式一下载百度活动文档.全网爬取.网站选择 import websiteJude
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


def compareTitle(title, content):
    panduan = True
    if "《" in title and "》" in title:
        title = re.search('《(.*?)》', title)[0]
    if title in content:
        panduan = True
    return panduan


def getUrl(driver, element):
    element.click()
    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    driver.switch_to.window(n[-1])
    time.sleep(0.5)
    try:
        element = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, '//div'))
        )
    except:
        time.sleep(0.1)
    # 智能等待，超过时间则抛异常
    driver.implicitly_wait(6)
    wenKuUrl = driver.current_url
    return wenKuUrl


def getFirstUrl(folderPath, keyStr, urls, driver):
    try:
        try:
            elements = driver.find_elements(By.XPATH, '//h3[@class="c-title t t tts-title"]/a')
            for i, element in enumerate(elements):
                content = element.text
                hrefUrl = getUrl(driver, element)
                panduan = paywebsiteJude(content, folderPath, driver, keyStr, urls, hrefUrl)
        except:
            elements = driver.find_elements(By.XPATH, '//h3[@class="c-title t t tts-title"]/a')
            for i, element in enumerate(elements):
                content = element.text
                hrefUrl = getUrl(driver, element)
                panduan = paywebsiteJude(content, folderPath, driver, keyStr, urls, hrefUrl)
        for i in range(0, 5):
            try:
                print('第%d页' % (i + 2))
                driver.find_elements(By.XPATH, '//div[@class="page-inner_2jZi2"]/a[@class="n"]')[-1].click()
                n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                driver.switch_to.window(n[-1])
                time.sleep(2)
                try:
                    element = WebDriverWait(driver, 5).until(
                        EC.presence_of_element_located((By.XPATH, '//h3[@class="c-title t t tts-title"]/a'))
                    )
                except:
                    time.sleep(0.1)
                elementxs = driver.find_elements(By.XPATH, '//h3[@class="c-title t t tts-title"]/a')
                for i, elementx in enumerate(elementxs):
                    content = elementx.text
                    hrefUrl = getUrl(driver, elementx)
                    panduan = paywebsiteJude(content, folderPath, driver, keyStr, urls, hrefUrl)
            except:
                print('这已经是最后一页了')
                break

    except:
        time.sleep(5)
        print('driver错误')
        strUrl = 'https://www.baidu.com/'
        driver.get(strUrl)


def insertContent(folderPath, keyStr, urls, driver):
    try:
        time.sleep(0.1)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span/input[@class="s_ipt"]'))
            )
        except:
            time.sleep(1)
        driver.find_element(By.XPATH, '//span/input[@class="s_ipt"]').clear()
        try:
            driver.find_element(By.XPATH, '//span/input[@class="s_ipt"]').send_keys(keyStr)
        except:
            driver.find_element(By.XPATH, '//span/input[@class="se-input adjust-input"]').send_keys(keyStr)
        time.sleep(2)
        driver.find_element(By.XPATH, '//span/input[@class="s_ipt"]').send_keys(Keys.ENTER)
        time.sleep(0.2)
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//i[@class="c-icon icons_2hrV4"]'))
            )
        except:
            time.sleep(0.1)
        ActionChains(driver).move_to_element(driver.find_element(By.XPATH, '//i[@class="c-icon icons_2hrV4"]')).perform()
        driver.find_element(By.CSS_SELECTOR, 'div.tool_3HMbZ').click()
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span[@id="timeRlt"]'))
            )
        except:
            time.sleep(0.1)
        ActionChains(driver).move_to_element(
            driver.find_element(By.XPATH, '//span[@id="timeRlt"]')).perform()
        driver.find_element(By.XPATH, '//span[@id="timeRlt"]').click()
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, '//ul[@class="file_ul_2a1K5"]/li[@class="time_li_3_ArK pop_li_1YiIN pointer_u-S_a"]'))
            )
        except:
            time.sleep(0.1)
        ActionChains(driver).move_to_element(
            driver.find_element(By.XPATH,
                                '//ul[@class="file_ul_2a1K5"]/li[@class="time_li_3_ArK pop_li_1YiIN pointer_u-S_a"]')).perform()
        driver.find_element(By.XPATH,
                            '//ul[@class="file_ul_2a1K5"]/li[@class="time_li_3_ArK pop_li_1YiIN pointer_u-S_a"]').click()
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located(
                    (By.XPATH, '//div'))
            )
        except:
            time.sleep(0.1)
        getFirstUrl(folderPath, keyStr, urls, driver)
    except:
        print('error')


def makeFolder(floderPath, fileName):
    print(fileName)
    floderPath = floderPath + '\\' + fileName
    os.makedirs(floderPath)
    return floderPath


def main(folderPath, urls, txtPath, driver, number,last):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i, line in enumerate(lines):
        # try:
        keyStr = line.replace('\n', '')
        if i >= number and i < last:
            try:
                print(str(i) + '百度搜索' + '----', line)
                strUrl = 'https://www.baidu.com'
                newUrl = strUrl
                driver.get(newUrl)
                time.sleep(1)
            except:
                driver = getDriver('b')
                strUrl = 'https://www.baidu.com'
                newUrl = strUrl
                driver.get(newUrl)
                time.sleep(1)
            if 5 == i % 10:
                try:
                    driver.quit()
                    driver = getDriver(str(i))
                    driver.get(newUrl)
                except:
                    print('error')
            insertContent(folderPath, keyStr, urls, driver)
    # except:
    #     print('打开浏览器出错')


def mianBaiduWPayx(txtPath, folderPath, wangzhiPath, number,last):
    urls = []
    file = open(wangzhiPath, 'w', encoding='utf-8')
    # txtFilePaths = find_file(txtPath)
    # for txtFilePath in txtFilePaths:
    #     if 'all.txt' in txtFilePath:
    driver = getDriver('b')

    main(folderPath, urls, txtPath, driver, number,last)
    driver.quit()
    for i, url in enumerate(urls):
        file.write(url + '\n')
    file.close()
    return urls


if __name__ == '__main__':
    # 标题存放文件夹
    txtPath = r'./标题.txt'
    folderPath = r'D:\文档\百度付费上传文档\百度html'
    wangzhiPath = r'./wangzhi.txt'
    number = 0
    last=10000000
    mianBaiduWPayx(txtPath, folderPath, wangzhiPath, number,last)
