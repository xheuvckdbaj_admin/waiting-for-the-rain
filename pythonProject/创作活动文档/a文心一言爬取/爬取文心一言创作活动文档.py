import os
import time

from docx import Document
from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn
from pythonProject.创作活动文档.a文心一言爬取.addCookiesPlaywenxin import addCookiesPlaywenxin
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay


def sezhiParas(doc, content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5  # 1.5倍行距 单倍行距 1.0


def sezhiTtitle(doc, content):
    if not '年' in content:
        run = doc.add_heading('', 0).add_run(content)  # 应用场景示例标题
        run.font.name = u'宋体'
        run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
        run.font.size = Pt(18)
        # run.font.color.rgb = RGBColor(250,000,000)
        doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER


def sezhiPaper(doc):
    section = doc.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(25.4)
    section.right_margin = Mm(25.4)
    section.top_margin = Mm(25.4)
    section.bottom_margin = Mm(25.4)
    section.header_distance = Mm(12.7)
    section.footer_distance = Mm(12.7)


def markjudge(contentStr):
    if (contentStr.startswith('。') or contentStr.startswith('，') or contentStr.startswith('：')
            or contentStr.startswith('！') or contentStr.startswith('？') or contentStr.startswith('、')
            or contentStr.startswith('；') or contentStr.startswith(')')):
        contentStr = contentStr[1:]
    if contentStr.endswith('，') or contentStr.endswith('、') or contentStr.endswith('(') or contentStr.endswith('{') \
            or contentStr.endswith('[') or contentStr.endswith('·') or contentStr.endswith('——'):
        contentStr = contentStr[0:-1]
    if (not (contentStr.endswith('.') or contentStr.endswith('。') or contentStr.endswith('！')
             or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
             or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
             or contentStr.endswith('：') or contentStr.endswith('；') or contentStr.endswith('：'))):
        contentStr = contentStr
    return contentStr


def deletesingle(contentStr):
    if len(contentStr) == 1:
        contentStr = ''
    return contentStr


paghraDownPath = './删除活动文档段落违禁词.txt'


def deletePragram(paghraDownPath, content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or (word in content and
                              not (content.startswith('1.') or content.startswith('1、')))
    if panduan:
        content = ''
    return content


def merrageLastPrage(i, file_lines, contentStr):
    if i == (len(file_lines) - 1):
        if (not (contentStr.endswith('.') or contentStr.endswith('。') or contentStr.endswith('！')
                 or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
                 or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
                 or contentStr.endswith('；') or contentStr.endswith(';') or contentStr.endswith('；')
                 or contentStr.startswith('（')
                 or contentStr.startswith('2'))):
            contentStr = ''
    if contentStr.endswith('1.') or contentStr.endswith('2.') or contentStr.endswith('3.') \
            or contentStr.endswith('4.') or contentStr.endswith('5.') or contentStr.endswith('6.') \
            or contentStr.endswith('7.') or contentStr.endswith('8.') or contentStr.endswith('9.') \
            or contentStr.endswith('0.'):
        contentStr = ''
    return contentStr


def merrageLastPrageContentStr(i, file_lines, contentStr):
    if i == (len(file_lines) - 1):
        if contentStr.endswith('：'):
            contentStr = ''
    return contentStr


def getNewPage(page, url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)


def run(brower, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page, "https://yiyan.baidu.com/")
    cookies = addCookiesPlaywenxin(cookiesPath)
    # 设置cookies
    brower.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page, "https://yiyan.baidu.com/")
    # 设置cookies
    brower.add_cookies(cookies)
    xpathStrChuanzuo = '//div[@class="HjrMd8LI"]/span[@class="KTFDGqJT"]'
    time.sleep(0.5)
    try:
        page.locator(xpathStrChuanzuo).click()
    except Exception as e:
        print(str(e))
    return page


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
        print(filePath, '创建成功！！！')


def getTitle(page1, fileTtitle):
    elements = page1.locator('xpath=//span[@style="cursor: pointer;"]')
    for i in range(0, elements.count()):
        contentStr = elements.nth(i).text_content().strip()
        print(contentStr)
        fileTtitle.write(contentStr + '\n')


def getRunPage(brower, page, cookiesPath):
    run(brower, page, cookiesPath)
    xpathTiwen = '//div[@class="JRWXI4iU"]/textarea[@class="ant-input wBs12eIN"]'
    elements = page.locator(xpathTiwen)
    if elements.count() == 0:
        getRunPage(brower, page, cookiesPath)
    time.sleep(0.5)


def makeWordChatGPT(folderPath, title, content):
    try:
        title = title
        filePathTxt = folderPath + '\\' + title + '.txt'
        filePathDocx = folderPath + '\\' + title + '.docx'
        doc = Document()
        core_properties = doc.core_properties
        core_properties.author = ''
        sezhiPaper(doc)
        sezhiTtitle(doc, title)
        file = open(filePathTxt, 'w', encoding='utf-8')
        file.write(content)
        file.close()
        filer = open(filePathTxt, 'r', encoding='utf-8')
        file_lines = filer.readlines()
        for i, line in enumerate(file_lines):
            contentStr = line.replace('\n', '')
            contentStr = merrageLastPrage(i, file_lines, contentStr)
            contentStr = merrageLastPrageContentStr(i, file_lines, contentStr)
            contentStrA = markjudge(contentStr)
            contentStrB = deletesingle(contentStrA)
            contentStrC = deletePragram(paghraDownPath, contentStrB)
            sezhiParas(doc, contentStrC)
        doc.save(filePathDocx)
        filer.close()
    except:
        print('文档修改错误error')


def getTitleForquchong(playwright: Playwright, titles, htmlFolder, cookiesPath, startNumber, last):
    brower = playwright.chromium.launch_persistent_context(
        # 指定本机用户缓存地址
        user_data_dir=r"D:\ChromeProfile",
        # 指定本机google客户端exe的路径
        executable_path=r"C:\Program Files\Google\Chrome\Application\chrome.exe",
        # 要想通过这个下载文件这个必然要开  默认是False
        accept_downloads=True,
        # 设置不是无头模式
        headless=False,
        bypass_csp=True,
        slow_mo=10,
        # 跳过检测
        args=['--disable-blink-features=AutomationControlled', '--remote-debugging-port=9222']
    )
    # context=brower.new_context()
    page = brower.new_page()
    page.set_default_timeout(6000)
    getRunPage(brower, page, cookiesPath)
    getNewPage(page, "https://yiyan.baidu.com/")
    # time.sleep(100)
    # xpathStrChuanzuo = '//div[@class="HjrMd8LI"]/span[@class="KTFDGqJT"]'
    # time.sleep(0.5)
    # try:
    #     page.locator(xpathStrChuanzuo).click()
    # except Exception as e:
    #     print(str(e))
    xpathTiwen = '//div[@class="JRWXI4iU"]/textarea[@class="ant-input wBs12eIN"]'
    for i, line in enumerate(titles):
        if i >= startNumber and i < last:
            try:
                title = line.replace('\n', '')
                print(str(i) + '-----', title)
                page.reload()
                page.locator(xpathTiwen).click()
                page.locator(xpathTiwen).fill(title)
                page.locator(xpathTiwen).press('Enter')
                # page.press('id=kw', 'Enter')
                time.sleep(50)
                xpathContent = '//div[@class="FZ3QiFoa"]/div[@class="H7oUCk_o"]'
                content = page.locator(xpathContent).nth(0).text_content().strip()
                print(content)
                if 'AI' in content or 'gpt-3.5' in content or 'sorry' in content \
                        or '人工智能' in content or '对不起' in content or '抱歉' in content \
                        or 'Sorry' in content or '智能辅助' in content \
                        or '请提供更多的上下文信息' in content or 'The server' in content or 'OpenAI' in content \
                        or '请提供更多信息' in content or '文心' in content:
                    print('不必生成文档')
                else:
                    makeWordChatGPT(htmlFolder, title, content)
            except Exception as e:
                print(str(e))
    # page.get_by_text("文档", exact=True).click()
    # time.sleep(0.1)
    # getTitle(page, fileTtitle)
    # for i in range(2,40):
    #     page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
    #     page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(str(i))
    #     page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
    #     time.sleep(0.5)
    #     getTitle(page, fileTtitle)


def getPassTitle(titles, htmlFolder, cookiesPath, startNumber, last):
    with sync_playwright() as playwright:
        getTitleForquchong(playwright, titles, htmlFolder, cookiesPath, startNumber, last)


def mainPassTitleWenxin(titles, htmlFolder, cookiesPath, startNumber, last):
    makeFolder(htmlFolder)

    getPassTitle(titles, htmlFolder, cookiesPath, startNumber, last)


def main():
    titles = []
    htmlFolder = r'D:\文档\百度活动文档25\百度html\其他网站'
    cookiesPath = r'./baiduCookies1.txt'
    startNumber = 0
    last = 100
    mainPassTitleWenxin(titles, htmlFolder, cookiesPath, startNumber, last)


if __name__ == '__main__':
    main()
