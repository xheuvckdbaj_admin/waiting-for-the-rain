# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText4 = str(soup.select('div.content')[0]).replace('<br/>','</p><p>')
    contentText3=re.sub('W(.*?)M','',contentText4)
    contentText2 = re.sub('w(.*?)M', '', contentText3)
    contentText1 = re.sub('W(.*?)m', '', contentText2)
    contentText = re.sub('w(.*?)m', '', contentText1)
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.title > h3')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getAllUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.con > ul.ul > li > a')
    for a in a_list:
        href=a.get('href')
        try:
            getHtml(thmlpath, href)
        except:
            print('错误')
            continue
def getPageUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.showpage > a')[0].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    url=href.split('_')[0]+'_'
    for i in range(1,pageNumber+1):
        newUrl=url+str(i)+'.html'
        getAllUrl(thmlpath, newUrl)
def getLitterTitle2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.box.box3 > div.bar > a')
    for a in a_list:
        href=a.get('href')
        getAllUrl(thmlpath, href)
        try:
            getPageUrl(thmlpath, href)
        except:
            print('error')
            continue
def getLitterTitle(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.box.box3 > div.bar > a')
    for a in a_list:
        href=a.get('href')
        try:
            getLitterTitle2(thmlpath, href)
        except:
            getPageUrl(thmlpath, href)
def getTitleUrl2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.box.box2 > div.bar > a')
    for a in a_list:
        href=a.get('href')
        getLitterTitle(thmlpath, href)
def getTitleUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.con > div > ul > li > a')
    for a in a_list:
        href=a.get('href')
        getAllUrl(thmlpath, href)
        try:
            getPageUrl(thmlpath, href)
        except:
            print('error')
            continue
def main(thmlpath):
            #WwW.YBaSk.CoM
            #WwW.yBaSK.Com
            #Www.ybaSK.cOM
            file=open('./lianjie.txt','r')
            lines=file.readlines()
            for line in lines:
                url=line.replace('\n','')
                getTitleUrl(thmlpath, url)
                # getTitleUrl2(thmlpath, url)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
