# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from pythonProject.FindFile import find_file, find_name
from pythonProject.初中.敏感词过滤.minganci import filterWord

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


def deletePara(file):
    for para in file.paragraphs:

        para.text = para.text.replace('\n', ' ')
        if para.text == "" or para.text == '\n' or para.text == ' ':
            # print('第{}段是空行段'.format(i))
            para.clear()  # 清除文字，并不删除段落，run也可以,
            delete_paragraph(para)


def tihuanguanjianci(lines, para):
    for line in lines:
        keyValue = line.replace('\n', '')
        key = keyValue.split('===>')[0]
        value = keyValue.split('===>')[1]
        para.text = para.text.replace(key, value)


def panduanFileName(fileName):
    panduan = len(fileName) >= 8 \
              and (not ('党' in fileName or '政府' in fileName or '七一' in fileName or
                        '书记' in fileName or '干部' in fileName or
                        '部队' in fileName or '中央' in fileName or
                        '习近平' in fileName or '毒' in fileName or
                        '政治' in fileName or '社团' in fileName or
                        '百年' in fileName or
                        '两会' in fileName or '局长' in fileName or '军' in fileName or
                        '毛泽东' in fileName or '微信' in fileName or
                        '微博' in fileName or 'QQ' in fileName or'qq' in fileName or
                        '伤' in fileName or '说说' in fileName or
                        '不忘初心' in fileName or '邓小平' in fileName or
                        '入团' in fileName or '全会' in fileName or '十九大' in fileName or
                        '二十大' in fileName or '区委' in fileName or '县委' in fileName or
                        '两会' in fileName or '市委' in fileName or '省委' in fileName))
    return panduan


def deleteLaterParaPanduan(i, content, fileName):
    panduan = False
    content = content.replace(' ', '')
    if (not (i == 1 and i == 2)) and len(content) < 100:
        titleContent = fileName.replace('.docx', '').replace('[', '').replace(']', '').replace('【', '').replace('】',
                                                                                                                '').replace(
            '(', '').replace(')', '').replace(' ', '')
        content = content.replace('[', '').replace(']', '').replace('【', '').replace('】', '').replace('(', '').replace(
            ')', '').replace(' ', '')
        panduan = ((content[-5:-1] == titleContent[-5:-1]) or (content[0:5] == titleContent[0:5]))
    if content == '-' or content == '1':
        panduan = True
    return panduan


# def modifyString(content):
#     content=content.replace('2004','2022').replace('2005','2022').replace('2006','2022').replace('2007','2022').replace('2008','2022').\
#         replace('2009','2022').replace('2010','2022').replace('2011','2022').replace('2012','2022').\
#         replace('2013','2022').replace('2014','2022').replace('2015','2022').replace('2016','2022').\
#         replace('2017','2022').replace('2018','2022').replace('2019','2022').replace('2020','2022').replace('2021','2022')
#     return content
def deleAdvertising(filePath, newFolderPath, midFolderPath, newfileNames, lines, front, later):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        try:
            file = docx.Document(filePath)

            fileName = os.path.basename(filePath)

            if panduanFileName(fileName):
                # print("段落数:" + str(len(file.paragraphs)))
                # print('删除前图形图像的数量：', len(file.inline_shapes))
                i = 0
                str = []
                number = 0
                for para in file.paragraphs:
                    # 替换关键词，洗稿
                    tihuanguanjianci(lines, para)
                    number += len(para.text)
                    # para.text=modifyString(para.text)
                    print(para.text)
                    if '独家资料之' in para.text or '认真看完了' in para.text \
                            or '微信关注' in para.text or '让你涨分更多' in para.text \
                            or '更多初中数学资料' in para.text \
                            or '更多资料尽在' in para.text or '微信搜索' in para.text \
                            or '关注微信' in para.text or '下载' in para.text \
                            or '公众号' in para.text or '探究群' in para.text \
                            or '更多资料' in para.text or '全部下载' in para.text \
                            or '资料分享' in para.text or '小编' in para.text \
                            or 'time' in para.text or '收藏' in para.text or '编号' in para.text \
                            or '精彩文章' in para.text or '发布时间' in para.text \
                            or '收藏' in para.text or '标签' in para.text or '来源：' in para.text or \
                            '上一篇：' in para.text or '返回课文' in para.text or '资料' in para.text or 'qq' in para.text \
                            or '学科网' in para.text or 'http' in para.text or 'Com' in para.text or '来源:' in para.text \
                            or '电话' in para.text or '微信' in para.text \
                            or '139' in para.text or 'www' in para.text \
                            or 'com' in para.text or '来源' in para.text \
                            or '邮箱' in para.text or '@' in para.text \
                            or '数据源于' in para.text or '转载' in para.text \
                            or 'www' in para.text or '135' in para.text \
                            or '拨打' in para.text or '客服微信' in para.text \
                            or '联系' in para.text or '作者' in para.text \
                            or '上一页' in para.text or '小编整理' in para.text \
                            or 'com' in para.text or '励志台' in para.text or '大家收集' in para.text \
                            or '作者简介' in para.text or 'QQ' in para.text or '原文地址' in para.text \
                            or '出自：' in para.text or '转载' in para.text or '发布时间' in para.text \
                            or '欢送阅读' in para.text or '下面是整理' in para.text or '欢送' in para.text \
                            or '为您整理' in para.text or '网编' in para.text or '对你有帮助' in para.text \
                            or '感谢你阅读' in para.text or '发表时间' in para.text or '关注我们' in para.text \
                            or '收藏本站' in para.text or '转载' in para.text or '本站' in para.text \
                            or '欢迎参考' in para.text or '欢迎阅读' in para.text or '大家整理' in para.text \
                            or '资料部分文字内容：' in para.text or '责任编辑：' in para.text or '供大家参考' in para.text \
                            or '网络收集整理' in para.text or '本文来源' in para.text or 'http' in para.text \
                            or '第一文档网' in para.text or '.cn' in para.text or 'Www' in para.text \
                            or 'dYhzdl' in para.text or '转载于' in para.text or '范文网' in para.text \
                            or '二维码' in para.text or '关注即可' in para.text or '精心整理' in para.text \
                            or '浏览器极速模式' in para.text or '给大家带来帮助' in para.text \
                            or '编辑推荐' in para.text or '【推荐】' in para.text or '页码' in para.text \
                            or '大全' in para.text or '点击查看更多' in para.text or '心得体会专题' in para.text \
                            or '>>>' in para.text or '仅供参考' in para.text or '感谢您的阅读' in para.text \
                            or '在下载' in para.text or 'PAGE' in para.text or '精品文档' in para.text or \
                            '查看更多' in para.text or '欢迎大家阅读' in para.text or '供您参考' in para.text or \
                            '.COM' in para.text or 'F132' in para.text or 'f132' in para.text or \
                            '77cn' in para.text or '免费范文网' in para.text or '收藏本站' in para.text or \
                            '百度搜索' in para.text or '在线全文阅读' in para.text or '网络整理' in para.text or \
                            '免责声明：' in para.text or '相关热词搜索：' in para.text or '来源：' in para.text or \
                            '蒲公英阅读网' in para.text or '本文已影响人' in para.text or '小编' in para.text or \
                            '编辑推荐' in para.text or 'gz85.COM' in para.text or '范文参考网' in para.text or \
                            '以上就是这篇范文' in para.text or '收藏下' in para.text or '收藏本页' in para.text or \
                            '为了方便大家的阅读' in para.text or '希望对您有帮助' in para.text or '应该跟大家分享' in para.text or \
                            '相关信息请访问' in para.text or 'hyheiban' in para.text or 'com' in para.text or \
                            '范文参考网' in para.text or 'fwwang' in para.text or '.cn' in para.text or \
                            '01hn' in para.text or 'chinawenwang' in para.text or ' 阅读：' in para.text or \
                            ' 登录:' in para.text or ' 平台:' in para.text or '888:' in para.text or '地址:' in para.text or \
                            'vip3:' in para.text:
                        str.append(i)
                    if deleteLaterParaPanduan(i, para.text, fileName):
                        str.append(i)
                    if len(para.text) < 100 and ('相关文章' in para.text \
                                                 or '小编精心推荐' in para.text or '了解更多' in para.text \
                                                 or '信箱：' in para.text or '数据来源' in para.text or '联系方式：' in para.text \
                                                 or '人还看了：' in para.text or '猜你喜欢' in para.text or '快捷搜索' in para.text \
                                                 or '相关作文：' in para.text or '猜你感兴趣：' in para.text or '猜你感兴趣的：' in para.text \
                                                 or '相关关键词：' in para.text or '参考文献：' in para.text or '相关热词搜索：' in para.text \
                                                 or '免责声明' in para.text or '产生版权问题' in para.text or '请联系我们及时删除' in para.text \
                                                 or '相关文章' in para.text or '来源：' in para.text \
                                                 or '更多类似' in para.text or '类似范文' in para.text or '推荐阅读' in para.text \
                                                 or '文章推荐' in para.text or '热词搜索：' in para.text or '更多文章' in para.text \
                                                 or '下一页更多精彩' in para.text or '猜你喜欢' in para.text or '还会看：' in para.text \
                                                 or '推荐访问:' in para.text or '相关推荐：' in para.text  or '人还看' in para.text \
                                                 or '推荐：' in para.text or '相关推荐文章：' in para.text or '相关：' in para.text or
                                                 '相关文章：' in para.text or '作者：' in para.text or '参考文献' in para.text or '上一篇范文：' in para.text):
                        print(para.text)
                        for par in range(i, len(file.paragraphs)):
                            str.append(par)
                    i += 1
                for i in str:
                    paragraph = file.paragraphs[i]
                    paragraph.clear()
                deletePara(file)
                # print('删除前图形图像的数量：', len(file.inline_shapes))   -2021学年第二学期教学工作总结
                fileName = fileName.replace('2016', '2022').replace('2017', '2022').replace(
                    '2018', '2022').replace('2019', '2022').replace('2020', '2022').replace('【微信公众号：wkgx985 免费获取】',
                                                                                            '').replace('2015',
                                                                                                        '2022').replace(
                    '2010', '2022').replace('2008', '2022').replace('2009', '2022').replace('2007', '2022').replace(
                    '2006', '2022').replace('2005', '2022').replace(
                    '2011', '2022').replace('2012', '2022').replace('2013', '2022').replace('2014', '2022').replace(
                    '2019', '2022').replace(
                    '2020', '2022').replace('2004', '2022') \
                    .replace('2017', '2022').replace('2018', '2022') \
                    .replace('2015', '2022').replace('2016', '2022').replace('2021', '2022') \
                    .replace('._', '').replace('★', '').replace('#', '') \
                    .replace('•', '').replace('！', '').replace('?', '').replace('？', '') \
                    .replace('•', '').replace('~', '').replace('✓', '').replace('.', '').replace('+', '') \
                    .replace(',', '').replace('docx', '').replace('公众号：', '') \
                    .replace('､', '').replace('。', '').replace('▪', '').replace('，', '') \
                    .replace('．', '').replace('公众号:', '').replace('：', '') \
                    .replace('、', '').replace('（喜子的商铺）', '').replace('·', '') \
                    .replace('原创', '精品').replace('转载', '精品') \
                    .replace('&lt', '精品').replace(';', '精品').replace('&gt', '精品') \
                    .replace('11', '').replace('＊', '').replace(',', '')
                file_name2 = ''.join(fileName.split())
                file_name1 = re.sub('【.*?】', '', file_name2)
                file_name = re.sub('www.*?com', '', file_name1)
                paths = r'./敏感词过滤/minganci.txt'
                if number > 500:
                    if file.save(newFolderPath + '\\' + file_name + '.docx') == None:
                        # newFile = docx.Document(newFolderPath + '\\' + fileName)
                        # delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                        os.remove(filePath)  # 删除原来修改前的docx文件
                    if file_name in newfileNames:
                        os.remove(filePath)
            else:
                file.save(midFolderPath + '\\' + fileName)
                os.remove(filePath)
        except:
            print('错误')


def deleAdvertising2(filePath, newFolderPath, midFolderPath, newfileNames):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        try:
            file = docx.Document(filePath)
            fileName = os.path.basename(filePath)
            if panduanFileName(fileName):
                # print("段落数:" + str(len(file.paragraphs)))
                # print('删除前图形图像的数量：', len(file.inline_shapes))
                i = 0
                str = []
                number = 0
                for para in file.paragraphs:
                    # 替换关键词，洗稿
                    number += len(para.text)
                if number > 500:
                    if file.save(newFolderPath + '\\' + fileName) == None:
                        # newFile = docx.Document(newFolderPath + '\\' + fileName)
                        # delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                        os.remove(filePath)  # 删除原来修改前的docx文件
                    if fileName in newfileNames:
                        os.remove(filePath)
                else:
                    os.remove(filePath)

            else:
                file.save(midFolderPath + '\\' + fileName)
                os.remove(filePath)
        except:
            print('错误')


# word.Quit()
def shear_dile(oldFolderPath,newFolderPath, midFolderPath, newfileNames, lines):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i,d in enumerate(os.listdir(oldFolderPath)):
                    shear_dile(os.path.join(oldFolderPath, d),newFolderPath, midFolderPath, newfileNames, lines)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.docx'):
            print(oldFolderPath)
            deleAdvertising(oldFolderPath, newFolderPath, midFolderPath, newfileNames, lines, 0, 0)


def modifyContent2(oldFolderPath, newFolderPath, midFolderPath):
    filePaths = find_file(oldFolderPath, [])
    newfileNames = find_name(newFolderPath, [])
    for i, filePath in enumerate(filePaths):
            if not '$' in filePath:
                print(filePath)
                deleAdvertising2(filePath, newFolderPath, midFolderPath, newfileNames)  # 修改，删除

def modifyContent(oldFolderPath, newFolderPath, midFolderPath):
    newfileNames = find_name(newFolderPath, [])
    paths = r'./敏感词过滤/doc_replace.txt'
    fileTxt = open(paths, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    shear_dile(oldFolderPath,newFolderPath, midFolderPath, newfileNames, lines)


def main2():
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'D:\文档\百度活动文档\百度html\其他网站\百度文档'
    newFolderPath = r'E:\文档\上传'
    midFolderPath = r'G:\文档\需要修改文件名'
    modifyContent2(oldFolderPath, newFolderPath, midFolderPath)


def main1():
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'D:\文档\百度活动文档\百度html\其他网站\百度文档'
    newFolderPath = r'D:\文档\百度活动文档\百度html\其他网站\百度文档'
    midFolderPath = r'D:\文档\需要修改的文件'
    modifyContent(oldFolderPath, newFolderPath, midFolderPath)


if __name__ == '__main__':
    main1()
    main2()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
