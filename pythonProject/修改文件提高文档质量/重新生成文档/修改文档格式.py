# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from docx import Document
from docx.enum.base import XmlMappedEnumMember
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn

from pythonProject.FindFile import find_file, find_name
from pythonProject.修改文件提高文档质量.重新生成文档.panduan import modifyPara

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def panduanFileName(fileName):
    panduan=len(fileName) >= 8 and (not ('党' in fileName or '政府' in fileName or
                                         '书记' in fileName or '干部' in fileName or
                                         '部队' in fileName or '中央' in fileName or
                                         '习近平' in fileName or '毒' in fileName or
                                         '政治' in fileName or '社团' in fileName or
                                         '百年' in fileName or '领导班子' in fileName or
                                         '两会' in fileName or '局长' in fileName or
                                         '微信' in fileName or 'QQ' in fileName or
                                         'qq' in fileName or '建国' in fileName) )

    return  panduan


def panduanFileName2(fileName):
    panduan = len(fileName) >= 8 and (not (
                                           '习近平' in fileName or '毛泽东' in fileName or
                                           '邓小平' in fileName ))

    return panduan
def addfanben(content):
    content=content.replace('范文','').replace('范本','').replace('自我评价','自我评价范文').replace('演讲稿','演讲稿范文').replace('作文','作文范文').\
        replace('总结','总结范文').replace('承诺书','承诺书范文').replace('辩论稿','辩论稿范文').replace('协议','协议范文').\
        replace('通知','通知范文').replace('说明书','说明书范文').replace('评语','评语范文').replace('心得体会','心得体会范文').\
        replace('征文','征文范文').replace('报告','报告范文').replace('主持词','主持词范文').replace('合同','合同范本').\
        replace('寄语','寄语范文').replace('开幕词','开幕词范文').replace('读书心得','读书心得范文').replace('方案','方案范文').\
        replace('自我鉴定','自我鉴定范文').replace('工作计划','工作计划范文').replace('活动方案','活动方案范文')
    return content

def sezhiPaper(doc):
    section = doc.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(25.4)
    section.right_margin = Mm(25.4)
    section.top_margin = Mm(25.4)
    section.bottom_margin = Mm(25.4)
    section.header_distance = Mm(12.7)
    section.footer_distance = Mm(12.7)
def sezhigeshi(doc,content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体 (中文正文)'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    run.font.bold = True
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    # para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5 # 1.5倍行距 单倍行距 1.0
def sezhishuxin(doc,content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体 (中文正文)'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    # para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5 # 1.5倍行距 单倍行距 1.0
def sezhiParas(doc,content):
    # 段落正文
    para = doc.add_paragraph('')
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    run = para.add_run(content)
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5 # 1.5倍行距 单倍行距 1.0
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
def  sezhiLittleTtitle(doc,content):
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'黑体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'黑体')
    run.font.size = Pt(14)
    run.font.color.rgb = RGBColor(250,000,000)
    run.font.bold = True
    doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
def  sezhiTtitle(doc,content):
    if not '年' in content:
        run = doc.add_heading('', 0).add_run(content)  # 应用场景示例标题
        run.font.name = u'宋体'
        run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
        run.font.size = Pt(18)
        # run.font.color.rgb = RGBColor(250,000,000)
        doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

def panduanHead(content):
    panduan=content.endswith('一：') or content.endswith('二：') or content.endswith('三：') or \
    content.endswith('四：') or content.endswith('五：') or content.endswith('六：') or \
    content.endswith('七：') or content.endswith('八：') or content.endswith('九：') or \
    content.endswith('十：') \
    or content.endswith('（一）') or content.endswith('（二）') or \
    content.endswith('（三）') or content.endswith('（四）') or content.endswith('（五）') or \
    content.endswith('（六）') or content.endswith('（七）') or content.endswith('（八）') or \
    content.endswith('（九）') or content.endswith('（十）') or content.endswith('(一)') or \
            content.endswith('(一)') or content.endswith('(二)') or content.endswith('(三)') or \
            content.endswith('(四)') or content.endswith('(五)') or content.endswith('(六)') or \
            content.endswith('(七)') or content.endswith('(八)') or content.endswith('(九)') or \
            content.endswith('(十)') or content.endswith('〔一〕') or content.endswith('〔二〕') or \
            content.endswith('〔三〕') or \
            content.endswith('〔四〕') or content.endswith('〔五〕') or content.endswith('〔六〕') or \
            content.endswith('〔七〕') or content.endswith('〔八〕') or content.endswith('〔九〕') or \
            content.endswith('〔十〕') or content.endswith('【一】') or content.endswith('【二】') or \
            content.endswith('【三】') or \
            content.endswith('【四】') or content.endswith('【五】') or content.endswith('【六】') or \
            content.endswith('【七】') or content.endswith('【八】') or content.endswith('【九】') or \
            content.endswith('【十】') or content.endswith('【篇一】') or content.endswith('【篇二】') or \
            content.endswith('【篇三】') or \
            content.endswith('【篇四】') or content.endswith('【篇五】') or content.endswith('【篇六】') or \
            content.endswith('【篇七】') or content.endswith('【篇八】') or content.endswith('【篇九】') or \
            content.endswith('【篇十】') or content.endswith('篇一') or content.endswith('篇二') or \
            content.endswith('篇三') or \
            content.endswith('篇四') or content.endswith('篇五') or content.endswith('篇六') or \
            content.endswith('篇七') or content.endswith('篇八') or content.endswith('篇九') or \
            content.endswith('篇十')  or content.startswith('第1篇') or content.startswith('第2篇') or \
            content.startswith('第3篇') or \
            content.startswith('第4篇') or content.startswith('第5篇') or content.startswith('第6篇') or \
            content.startswith('第7篇') or content.startswith('第8篇') or content.startswith('第9篇') or \
            content.startswith('第10篇')
    return panduan
def panduanPara(content):


    panduan=len(content) < 45 and(content.startswith('一、') or content.startswith('二、') or \
    content.startswith('三、') or \
    content.startswith('四、') or content.startswith('五、') or content.startswith('六、') or \
    content.startswith('七、') or content.startswith('八、') or content.startswith('九、') or \
    content.startswith('十、'))
    return panduan
def makePagrah(i, title, content,doc):
    if modifyPara(i, title, content):
        # print(file.paragraphs[i].style)
        if panduanHead(content):
            sezhiLittleTtitle(doc, content)
        elif (content.startswith('尊敬的') or content.startswith('各位') or
              content.startswith('亲爱的') or content.startswith('朋友') or len(
                    content) < 12) and content.endswith('：'):
            sezhishuxin(doc, content)
        elif panduanPara(content):
            sezhigeshi(doc, content)
        else:
            sezhiParas(doc, content)
def markContent(contentStr):
    panduan =False
    reg = '\d{5}'
    hh = re.search(reg, contentStr)
    # if len(contentStr)<2 and len(contentStr) > 0:
    #     panduan = True
    #     fileName = fileName + '----' + '内容长度为1'
    # if 'VIP' in contentStr:
    #     panduan=True
    #     fileName=fileName+'----'+'内容含有VIP'
    # if '⼯' in contentStr:
    #     panduan = True
    #     fileName = fileName + '----' + '内容含有未识别汉字'
    # if '文库' in contentStr or '提供的信息' in contentStr or \
    #         '抱歉' in contentStr or '回答这个问题' in contentStr \
    #         or '对不起' in contentStr or '请提供' in contentStr or \
    #         'AI' in contentStr or '语言模型' in contentStr \
    #         or '无法提供您' in contentStr or '对我的回答' in contentStr:
    #     panduan = True
    #     fileName = fileName + '----' + '内容含有文库或ai标志'
    if not (hh==None):
        contentStr=''
    if (contentStr.startswith('。') or contentStr.startswith('，') or contentStr.startswith('：')
        or contentStr.startswith('！') or contentStr.startswith('？') or contentStr.startswith('、')
            or contentStr.startswith('；') or contentStr.startswith(')')) :
        contentStr = contentStr[1:-1]
    # if contentStr.startswith('的') and (not contentStr.startswith('的确')):
    #     fileName = fileName + '----' + '内容含有的'
    # if contentStr.startswith(')'):
    #     fileName = fileName + '----' + '内容以)开头'
    if contentStr.endswith('，') or contentStr.endswith('、') or contentStr.endswith('(') or contentStr.endswith('{') \
        or contentStr.endswith('[') or contentStr.endswith('·') or contentStr.endswith('——'):
        contentStr = contentStr[0:-2]
    # if len(contentStr) >100 and (not (contentStr.endswith('.') or contentStr.endswith('。')  or contentStr.endswith('！')
    #                                   or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
    #                                   or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
    #                                   or contentStr.endswith('：'))):
    #     print(contentStr)
    #     panduan = True
    #     fileName = fileName + '----' + '内容没有标点符号结束'
    return contentStr
def makeWord(filePath,newPath):
    filrdocx = filePath.split(".")[len(filePath.split(".")) - 1]
    if filrdocx == 'docx' :
        try:
            file = docx.Document(filePath)
            fileName = os.path.basename(filePath)
            # fileName = addfanben(fileName)
            # number = 0
            if len(file.paragraphs)==0:
                print('删除文档',filePath)
                # os.remove(filePath)
            if not len(file.paragraphs)==0:
                panduanNull = True
                title = fileName.replace('.docx', '')
                doc = Document()
                core_properties = doc.core_properties
                core_properties.author = ''
                sezhiPaper(doc)
                sezhiTtitle(doc, title)
                for i in range(0,len(file.paragraphs)):
                    pattern = '[a-z]{2}'
                    title = title.strip().replace(' ', '')
                    panduan = re.search(pattern, file.paragraphs[i].text)
                    content=str(file.paragraphs[i].text).strip()
                    pattern = '#\d+'
                    content = re.sub(pattern, '', content)
                    content=markContent(content)
                    # if 'VIP' in content:
                    #     os.remove(filePath)
                    if '⼯' in content:
                        panduanNull =False
                        break
                    if panduan==None:
                        content = str(content).replace('　　','').replace(' ', '').replace('\n','')
                    if ' ' in content:
                        contentList=content.split(' ')
                        for content in contentList:
                            makePagrah(i, title, content, doc)
                    else:
                        makePagrah(i, title, content, doc)
                if panduanNull:
                    # doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
                    # fileName=addfanben(fileName)
                    fileName=fileName.replace('.docx.docx','.docx')
                    doc.save(newPath + '\\' + fileName)
                    os.remove(filePath)
        except:
            print('error')
def modifyContent(filePath,newPath):
    dir = os.listdir(filePath)
    for i in dir:
        i = os.path.join(filePath, i)
        if os.path.isdir(i):
            modifyContent(i,newPath)
        else:
            if not '$' in i:
                print(i)
                makeWord(i, newPath)  # 修改，删除广告

def mainGeshi(oldFolderPath,newPath):
    modifyContent(oldFolderPath,newPath)
if __name__ == '__main__':
    oldFolderPath = r'D:\文档\百度活动文档\中介'
    newPath = r'D:\文档\百度活动文档\修改后百度活动文档'
    mainGeshi(oldFolderPath,newPath)
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
