# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import requests
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.FindFile import find_file
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档检查账号是否传满 import \
    mainTitleClassifyUploadApiGuish
from pythonProject.创作活动文档.playwright创作活动文档.删除单个账号已被传过的文档 import mainTitleClassifyUploadDelete
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.登录百度账号 import maindenglu
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.百度安全验证 import anquanyanzheng
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt
from pywinauto import Desktop
from pywinauto.keyboard import *


# def getBrowserNow():
#     options = webdriver.ChromeOptions()
#     options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
#     browser = webdriver.Chrome(options=options)
#     time.sleep(1)
#     return browser
#
#
# def panduanTitle(title):
#     panduan = True
#     if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title or '年级' in title \
#             or '图' in title or '.' in title or '×' in title or (len(title) < 5):
#         panduan = False
#     return panduan
#
#
# def addCookies(browser, url, filePath):
#     browser.get(url)
#     time.sleep(1)
#     cookies = readTxt.readTxt(filePath)
#     for item in cookies.split(';'):
#         cookie = {}
#         itemname = item.split('=')[0]
#         iremvalue = item.split('=')[1]
#         cookie['domain'] = '.baidu.com'
#         cookie['name'] = itemname.strip()
#         cookie['value'] = urllib.parse.unquote(iremvalue).strip()
#         browser.add_cookie(cookie)
#     browser.get(url)
#
#
# def getNewWindow(browser, xpathStr, timeNumber):
#     n = browser.window_handles
#     browser.switch_to.window(n[-1])
#     time.sleep(1)
#     browser.switch_to.window(browser.window_handles[0])
#     element = WebDriverWait(browser, timeNumber).until(
#         EC.presence_of_element_located(
#             (By.XPATH, xpathStr)
#         )
#     )
#
#
# def getNumberPager(browser):
#     try:
#         xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
#         pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
#         return pagerNumber
#     except:
#         return 0
#
#
# def deleteWord(folderPath, title):
#     filePath = folderPath + '\\' + title + '.docx'
#     try:
#         os.remove(filePath)
#     except:
#         print('无法删除' + folderPath)
#
#
#
#
#
# def getFileNameStr(folderPath):
#     keyStrs = []
#     filePaths = find_file(folderPath, [])
#     if len(filePaths) >= 1:
#         for filePath in filePaths:
#             titleStr = os.path.basename(filePath).replace('.docx', '')
#             keyStrs.append(titleStr)
#     return keyStrs
#
#
# def getTitleNumber(wordFolderPath):
#     titleNumber = 1000000
#     if wordFolderPath.endswith('推荐'):
#         titleNumber = -1
#     if wordFolderPath.endswith('学前教育'):
#         titleNumber = 0
#     if wordFolderPath.endswith('基础教育'):
#         titleNumber = 1
#     if wordFolderPath.endswith('高校与高等教育'):
#         titleNumber = 2
#     if wordFolderPath.endswith('资格考试'):
#         titleNumber = 3
#     if wordFolderPath.endswith('法律'):
#         titleNumber = 4
#     if wordFolderPath.endswith('建筑'):
#         titleNumber = 5
#     if wordFolderPath.endswith('互联网'):
#         titleNumber = 6
#     if wordFolderPath.endswith('行业资料'):
#         titleNumber = 7
#     if wordFolderPath.endswith('政务民生'):
#         titleNumber = 8
#     if wordFolderPath.endswith('商品说明书'):
#         titleNumber = 9
#     if wordFolderPath.endswith('实用模板'):
#         titleNumber = 10
#     if wordFolderPath.endswith('生活娱乐'):
#         titleNumber = 11
#     return titleNumber
#
#
# def selectHeading(page, wordFolderPath):
#     try:
#         time.sleep(1)
#         pageNumber = getTitleNumber(wordFolderPath)
#         if pageNumber >= 0 and pageNumber < 12:
#             # pageNumber最小为0
#             if pageNumber >= 9:
#                 xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
#                 page.locator(xpathStrs).click()
#             xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
#             xpathStrScue = '//div[@class="privilege-list"]/div[@class="privilege-item-container action"]'
#             try:
#                 page.locator(xpathStr).nth(pageNumber).click()
#             except:
#                 page.locator(xpathStr).nth(pageNumber).click()
#             time.sleep(0.5)
#             # titleText = page.locator(xpathStrScue).text.strip()
#             if not getTitleNumber(wordFolderPath) == pageNumber:
#                 titleEl = page.locator(xpathStr).nth(pageNumber).click()
#                 time.sleep(1)
#     except:
#         selectHeading(page, wordFolderPath)
#
#
# def clickButton(ele):
#     try:
#         ele.click()
#         time.sleep(0.1)
#         ele.click()
#         time.sleep(0.1)
#         ele.click()
#     except:
#         print('')
#
#
# def panduanLastPage(page):
#     panduan = 1
#     try:
#         xpathStrButton = '//button[@class="btn-next"]'
#         disabled = page.locator(xpathStrButton).get_attribute('disabled')
#         if disabled == 'disabled':
#             panduan = 0
#         else:
#             panduan = 1
#         return panduan
#     except:
#         panduan = 0
#         return panduan
#
#
# def getNewPage(page, url):
#     try:
#         page.goto(url)
#         time.sleep(0.5)
#     except:
#         time.sleep(1)
#         getNewPage(page, url)
#
#
# def ClickNextPage(page, wordFolderPath, pagerNumber, wordNumber):
#     try:
#         # pageNumber最大为1，最小为0
#         xpathStr = '//button[@class="btn-next"]/i[@class="el-icon el-icon-arrow-right"]'
#         if panduanLastPage(page) == 1:
#             try:
#                 page.locator(xpathStr).click()
#                 time.sleep(1)
#             except:
#                 print('点击下一页失败')
#         elif panduanLastPage(page) == 0:
#             print('已经上传完毕或者没刷新出来')
#             pagerNumber = 100
#         return pagerNumber
#     except:
#         wordFolderPaths = os.listdir(wordFolderPath)
#         if len(wordFolderPaths) > 5:
#             print('点击下一页fast，重新开始上传')
#             getNewPage(page, 'https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
#
#             # getNewWindow(browser, '//div[@class="major-btns"]', 20)
#             selectHeading(page, wordFolderPath)
#             pageXpathStr = '//div[@class="el-input el-pagination__editor is-in-pagination"]/input[@class="el-input__inner"]'
#             page.locator(pageXpathStr).click()
#             page.locator(pageXpathStr).fill(str(pagerNumber))
#             page.locator(pageXpathStr).press("Enter")
#             time.sleep(0.5)
#         return 0
def ctrlx(oldFilePath, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
        try:
            shutil.move(oldFilePath, newPath)
        except:
            print('剪切不掉' + oldFilePath)
            try:
                os.remove(oldFilePath)
            except:
                print('删除失败')
#
# def submitword(page, file_chooser, fileDocxPath,newPath):
#     file_chooser.set_files(fileDocxPath)
#     page.wait_for_timeout(5000)
#     page.get_by_role('button', name='确认提交').click()
#     print('上传成功：' + fileDocxPath)
#     ctrlx(fileDocxPath, newPath)
#
#
# def deleteFile(wordFolderPath):
#     filePaths = find_file(wordFolderPath)
#     for filepath in filePaths:
#         try:
#             os.remove(filepath)
#         except:
#             print('删除失败', filepath)
#
#
# def singlyUpload(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles, panduanUpload, panduanFilePath):
#     xpathStrTitle = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
#     xpathStrTitleScue = '//div[@class="doc-row disabled-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
#     titleNumbers = page.locator(xpathStrTitle).count()
#     if not os.path.exists(panduanFilePath):
#         numberAhuzi = 1 / 0
#     for titleNumber in range(0, titleNumbers):
#         xpathStrButton = '//div[@class="doc-row"]/div[@class="upload-doc"]/div[@class="add-new-btn add-new-btn-right"]/button'
#         xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button[@class="el-button no-agree btn el-button--default el-button--mini"]'
#         try:
#             titleText = page.locator(xpathStrTitle).nth(titleNumber).get_attribute('title').strip()
#         except:
#             titleText = '*******'
#             print('有其他人正在上传')
#         if titleText in keyStrs and (not titleText in passedTitles) and (not '~' in titleText):
#             try:
#                 page.locator(xpathStrButton).nth(titleNumber).first.click()
#                 with page.expect_file_chooser() as fc_info:
#                     page.locator(xpathStrButton).nth(titleNumber).first.click()
#                 panduanUpload = True
#                 file_chooser = fc_info.value
#                 fileDocxPath = os.path.join(wordFolderPath, titleText) + '.docx'
#                 submitword(page, file_chooser, fileDocxPath)
#                 break
#             except:
#                 try:
#
#                     buttonSucess = page.locator(xpathStrEle)
#                     if buttonSucess.count() > 0:
#                         # 刷新操作
#                         fileTxt = open(panduanFilePath, 'w', encoding='utf-8')
#                         # mainTitleClassifyUploadApiGuish(page,folderPath, cookiesPath, panduanFilePath)
#                         # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)
#                         print('账号已经传完')
#                         fileTxt.write('账号已经传完')
#                         fileTxt.close()
#                         deleteFile(wordFolderPath)
#                 except Exception as e:
#                     print(str(e))
#     return panduanUpload
#
#
# def xunhuanPage(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber,
#                 wordNumber,
#                 panduanFilePath):
#     print(pagerNumber)
#     panduanUpload = singlyUpload(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles, panduanUpload,
#                                  panduanFilePath)
#     print(panduanUpload)
#     if panduanUpload == False:
#         pagerNumber = pagerNumber + 1
#         pagerNumber = ClickNextPage(page, wordFolderPath, pagerNumber, wordNumber)
#         if not pagerNumber == 100:
#             pagerNumber = xunhuanPage(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles,
#                                       panduanUpload, pagerNumber,
#                                       wordNumber, panduanFilePath)
#         return pagerNumber
#     else:
#         return pagerNumber
#
#
# def uploadFile(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber,
#                panduanFilePath):
#     panduanUpload = False
#     getNewPage(page, 'https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
#
#     xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
#     if pagerNumber == 0:
#         try:
#             time.sleep(0.5)
#             page.get_by_role("button", name="Close").click()
#         except:
#             print('没有关闭任务规则')
#         try:
#             page.get_by_role("button", name="我知道啦").click()
#         except:
#             print('点击创作活动失败')
#         try:
#             time.sleep(0.5)
#             page.get_by_role("button", name="Close").click()
#         except:
#             print('没有关闭任务规则')
#     try:
#         selectHeading(page, wordFolderPath)
#     except:
#         time.sleep(2)
#         selectHeading(page, wordFolderPath)
#     if pagerNumber > 0:
#         try:
#             pageXpathStr = '//div[@class="el-input el-pagination__editor is-in-pagination"]/input[@class="el-input__inner"]'
#             # browser.find_element(By.XPATH, pageXpathStr).clear()
#             # time.sleep(0.3)
#             page.locator(pageXpathStr).click()
#             page.locator(pageXpathStr).fill(str(pagerNumber))
#             page.locator(pageXpathStr).press("Enter")
#             time.sleep(1)
#         except:
#             print('网页没刷新')
#             pagerNumber = uploadFile(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles, pagerNumber,
#                                      wordNumber, panduanFilePath)
#
#     pagerNumber = xunhuanPage(page, folderPath, cookiesPath, wordFolderPath, keyStrs, passedTitles, panduanUpload,
#                               pagerNumber, wordNumber,
#                               panduanFilePath)
#     return pagerNumber


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def getNewPage(page, url):
    try:
        page.goto(url)
    except:
        time.sleep(1)
        getNewPage(page, url)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.get_by_role("button", name="Close").click()
        page.wait_for_timeout(500)
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page


def find_fileDing(wordFolder, filePaths=[]):
    dir = os.listdir(wordFolder)
    for i, name in enumerate(dir):
        name = os.path.join(wordFolder, name)
        if os.path.isdir(name):
            find_fileDing(name)
        else:
            filePath = name
            filePaths.append(filePath)


    return filePaths

def clickNextTitle(page, pageNumber):
    page.wait_for_timeout(500)
    if pageNumber >= 0:

        if  not page.get_by_role("button", name="我知道啦").count()==0:
            try:
                page.get_by_role("button", name="我知道啦").click()
            except:
                print('')
        # pageNumber最小为0
        if pageNumber >= 9:
            xpathStrs = '//div[@class="arrow-wrap control"]/button[contains(@class, "el-button el-button--default")]'
            className = page.locator(xpathStrs).nth(1).get_attribute('class')
            if not 'disabled' in className:
                page.locator(xpathStrs).nth(1).click()
                page.wait_for_timeout(500)
        else:
            xpathStrs = '//div[@class="arrow-wrap control"]/button[contains(@class, "el-button el-button--default")]'
            className=page.locator(xpathStrs).nth(0).get_attribute('class')
            if not 'disabled' in className:
                page.locator(xpathStrs).nth(0).click()
                page.wait_for_timeout(500)
        # xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
        xpathStr = '//div[@class="privilege-list"]/div[contains(@class, "privilege-item-container")]'

        page.locator(xpathStr).nth(pageNumber).click()
        page.wait_for_timeout(1000)
def clickNextPage(page,pageNumber):
    # try:

        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(pageNumber)
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
        page.wait_for_timeout(1000)
    # except:
    #     print('error')

def on_request(request):
    try:
        htmlPath = r'D:\文档\百度活动文档\图片'
        makeFolder(htmlPath)
        title = 'haha'
        # print(f'Request URL: {request.url}')
        # print(f'Request Headers: {request.headers}')
        # if request.post_data is not None:
        #     print(f'Request Body: {request.post_data}')
        if 'https://passport.baidu.com/cap/img?ak=' in request.url:
            requests_text = requests.get(url=request.url, headers=request.headers, timeout=5.0).content
            thmlpath = htmlPath + '\\' + title + '.jpg'
            f = open(thmlpath, 'wb')
            f.write(requests_text)
            # print('图片下载成功----', title)
    except:
        print('图片下载失败')
def clickButton(ele):
    try:
        ele.click()
        time.sleep(0.1)
        ele.click()
        time.sleep(0.1)
        ele.click()
    except:
        print('')
def uploadSubmit(page, file_chooser, filePath, successFloder):
    # 设置请求监听器
    page.on('request', on_request)
    file_chooser.set_files(filePath)
    app = Desktop()
    dialog = app['打开']
    # dialog.print_control_identifiers()
    # dialog.print_control_identifiers(depth=None, filename=None)
    # dialog['Edit'].type_keys(wordFilePath, with_spaces=True)
    clickButton(dialog['Button2'])
    panduan = False
    panduan = anquanyanzheng(page, panduan)
    if panduan == False:

        try:
                page.wait_for_timeout(4000)
                buttonele = page.get_by_role('button', name='确认提交')
                if not buttonele.count() == 0:
                    page.get_by_role('button', name='确认提交').click()
                    page.wait_for_timeout(1000)
                    page.get_by_role('button', name='查看已提交').click()
                    print('上传成功：' + str(filePath))
                    ctrlx(filePath, successFloder)
        except:
            print('上传失败')
    return panduan
def getYemianPage(page,filePath, successFloder):
    fileName=os.path.basename(filePath).replace('.docx','')
    xpath='//div[@class="doc-row"]'
    elements=page.locator(xpath)
    xpathStrButton='//div[@class="upload-doc"]/div[@class="add-new-btn add-new-btn-right"]/button[@class="el-button upload-btn btn el-button--default el-button--mini"]'
    for i in range(0,elements.count()):
        title=elements.nth(i).locator('//div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]').get_attribute('title')
        if title==fileName:
            print(title)
            elements.nth(i).locator(xpathStrButton).click()
            with page.expect_file_chooser() as fc_info:
                elements.nth(i).locator(xpathStrButton).click()
            file_chooser = fc_info.value
            fc_info.is_done()
            #上传文档
            return uploadSubmit(page, file_chooser, filePath, successFloder)
def findPage(page, titleNumber,pageNumber, filePath, successFloder):
    # 找到对应的模块
    clickNextTitle(page, titleNumber)
    # 找到对应的页数
    clickNextPage(page, pageNumber)
    # 找到文档对应的页面标题
    panduan = getYemianPage(page, filePath, successFloder)
    if panduan == True:
        findPage(page, titleNumber,pageNumber, filePath, successFloder)
def uploadFileBaidu(page,folderPath,successFloder):
    page.goto('https://cuttlefish.baidu.com/shopmis?_wkts_=1710558465152#/taskCenter/majorTask')
    page.wait_for_timeout(1000)
    if not page.get_by_role("button", name="我知道啦").count() == 0:
        try:
            page.get_by_role("button", name="我知道啦").click()
        except:
            print('')
    if not page.get_by_role("button", name="我知道了").count() == 0:
        try:
            page.get_by_role("button", name="我知道了").click()
        except:
            print('')
    filePaths = find_fileDing(folderPath)
    for number,filePath in enumerate(filePaths):
        try:
            print(filePath)
            pathName = os.path.basename(filePath).replace('.txt', '')
            pageNumber = filePath.replace(os.path.basename(filePath), '').split('\\')[-2].replace('page','')
            lanmu = filePath.replace(os.path.basename(filePath), '').split('\\')[-3]
            print(lanmu,pageNumber)
            moduleNames = ['推荐', '学前教育', '基础教育', '高校与高等教育', '语言/资格考试', '法律', '建筑', '互联网',
                           '行业资料', '政务民生', '商品说明书', '实用模板', '生活娱乐']
            titleNumber=0
            for i ,moduleName in enumerate(moduleNames):
                if lanmu==moduleName:
                    titleNumber=i
            #找到上传页面
            findPage(page, titleNumber, pageNumber, filePath, successFloder)

            time.sleep(1)
        except:
            print('跳转失败')
            getNewPage(page,
                       "https://cuttlefish.baidu.com/shopmis?_wkts_=1710314752043#/taskCenter/majorTask")
            page.wait_for_timeout(500)
            continue
    # dataNumber = 0
    # for titleFolderPathName in titleFolderPaths:
    #     try:
    #         passedTitles = []
    #         pagerNumber = 0
    #         titleFolderPath = os.path.join(folderPath, titleFolderPathName)
    #         wordFolderPaths = os.listdir(titleFolderPath)
    #         wordNumber = 100
    #         if len(wordFolderPaths) < 100:
    #             wordNumber = len(wordFolderPaths)
    #         for i in range(0, wordNumber):
    #             # for wordFolderPathName in wordFolderPaths:
    #             #     print(dataNumber)
    #             keyStrs = getFileNameStr(titleFolderPath)
    #             if not keyStrs == None:
    #                 print(pagerNumber)
    #                 pagerNumber = int(
    #                     uploadFile(page, folderPath, cookiesPath, titleFolderPath, keyStrs, passedTitles, pagerNumber,
    #                                wordNumber, panduanFilePath))
    #                 if pagerNumber == 100:
    #                     break
    #                     break
    #         wordFolderPaths2 = os.listdir(titleFolderPath)
    #         uploadNumber = len(wordFolderPaths) - len(wordFolderPaths2)
    #         print('一共上传%d个文档' % uploadNumber)
    #         if len(wordFolderPaths) == 0:
    #             try:
    #                 os.rmdir(titleFolderPath)
    #             except:
    #                 print('无法删除' + titleFolderPath)
    #     except:
    #         print('该模块上传失败')


def mainTitleClassifyUpload(playwright: Playwright, folderPath, cookiesPath,successFloder) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    # time.sleep(1000)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    page.wait_for_timeout(500)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    page.get_by_text("创作活动", exact=True).click()
    page.get_by_role("button", name="我知道了").click()
    uploadFileBaidu(page, folderPath,successFloder)
    # filePath = folderPath + '\\' + str(1) + '.txt'


def mainTitleClassifyUploadApi(folderPath, cookiesPath,successFloder):
    with sync_playwright() as playwright:
        mainTitleClassifyUpload(playwright, folderPath, cookiesPath,successFloder)
    # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


if __name__ == '__main__':
    folderPath = r'D:\文档\百度活动文档\百度活动文档上传'
    cookiesPath1 = './baiduCookies1.txt'
    successFloder='D:\文档\百度活动文档\百度上传成功'
    # panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    # cookiesPath = './baiduCookies1.txt'
    mainTitleClassifyUploadApi(folderPath, cookiesPath1,successFloder)
    # page=maindenglu(cookiesPath1)
    # page.wait_for_timeout(500000)


