import os
import re


def modifyFile(filefolder):
    dir = os.listdir(filefolder)
    for i in dir:
        i = os.path.join(filefolder, i)
        if os.path.isdir(i):
            continue
        else:

            if '$' in i:
                os.remove(i)
                print(i)
            fil = i.split('.docx')[0]
            os.rename(i, fil + '.doc')

def find_file1(filefolder,newfolder):
    dir = os.listdir(filefolder)
    for fileName in dir:

            i = os.path.join(filefolder, fileName)
            if os.path.isdir(i):
                find_file1(filefolder)
            else:
                fil = fileName.replace('2016', '2021').replace('2017', '2021').replace(
                    '2018', '2021').replace('2019', '2021').replace('2020', '2021').replace('【微信公众号：wkgx985 免费获取】','').replace('2015', '2021').replace(
                    '2010', '2021').replace('2008', '2021').replace('2009', '2021').replace('2007', '2021').replace('2006', '').replace('2005','2021').replace(
                    '2011', '2021').replace('2012', '2021').replace('2013', '2021').replace('2014', '2021').replace('2019', '2021').replace('2020', '2021') \
                    .replace('2017', '2021').replace('2018', '2021') \
                    .replace('._', '').replace('★', '').replace('#', '') \
                    .replace('•', '').replace('！', '').replace('?', '').replace('？', '') \
                    .replace('•', '').replace('~', '').replace('✓', '').replace('.', '').replace('+', '') \
                    .replace(',', '').replace('docx', '').replace('公众号：', '') \
                    .replace('､', '').replace('。', '').replace('▪', '').replace('，', '') \
                    .replace('公众号:', '').replace('：', '') \
                    .replace('、', '').replace('（喜子的商铺）', '').replace('·', '')\
                    .replace('原创', '精品').replace('转载', '精品')\
                    .replace('&lt', '精品').replace(';', '精品').replace('&gt', '精品').replace('ppt', '.ppt')
                file_name2 = ''.join(fil.split())
                file_name1 = re.sub('【.*?】', '', file_name2)
                file_name = re.sub('www.*?com', '', file_name1)
                os.rename(i,newfolder+'\\'+fil)


if __name__ == '__main__':
    filefolder = r'D:\文档\上传'
    newfolder = r'D:\文档\上传1'
    find_file1(filefolder,newfolder)