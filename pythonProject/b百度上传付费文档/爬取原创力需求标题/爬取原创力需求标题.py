import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)

def getTitleNumber(zimuStr):
    # 0为a,1为b,2为c,3为d,4为e,5为f,6为g,7为h,8为i,9为j,10为k,11为m,12为n,13为o,14为p,15为q,16为q,17为r,18为s,19为t,20为u,21为v,22为w,23为x
    titleNumber=0
    if zimuStr=='a':
        titleNumber=0
    elif zimuStr=='b':
        titleNumber=1
    elif zimuStr=='c':
        titleNumber=2
    elif zimuStr=='d':
        titleNumber=3
    elif zimuStr=='e':
        titleNumber=4
    elif zimuStr=='f':
        titleNumber=5
    elif zimuStr=='g':
        titleNumber=6
    elif zimuStr=='h':
        titleNumber=7
    elif zimuStr=='i':
        titleNumber=8
    elif zimuStr=='j':
        titleNumber=9
    elif zimuStr=='k':
        titleNumber=10
    elif zimuStr=='m':
        titleNumber=11
    elif zimuStr=='n':
        titleNumber=12
    elif zimuStr=='o':
        titleNumber=13
    # 0为a,1为b,2为c,3为d,4为e,5为f,6为g,7为h,8为i,9为j,10为k,11为m,12为n,13为o,
    # 14为p,15为q,16为q,17为r,18为s,19为t,20为u,21为v,22为w,23为x
    elif zimuStr=='p':
        titleNumber=14
    elif zimuStr=='q':
        titleNumber=15
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0

def run(playwright: Playwright, titlePath) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    time.sleep(1)
    page.goto("https://max.book118.com/guarantee.html")
    time.sleep(2)

    xpathTitle = '//tbody/tr/td[@class="col-keywords"]/a'
    elementThrees = page.locator(xpathTitle)
    for number in range(0, 7000):
                            try:
                                makeFolder(titlePath)
                                newTitlePath = titlePath + '\\' + str(number) + '.txt'
                                file = open(newTitlePath, 'w', encoding='utf-8')
                                for i in range(0, elementThrees.count()):
                                    title = elementThrees.nth(i).get_attribute('title')
                                    title = title.replace('.docx', '').replace('.doc', '').replace('.ppt', '') \
                                        .replace('.pptx', '').replace('.pdf', '').strip()
                                    print(title)
                                    file.write(title + '\n')
                                file.close()
                                xpathNext='//tr/td/div[@class="page"]/ul[@class="paging"]/li/a[@class="next"]'
                                page.locator(xpathNext).nth(0).click()
                            except Exception as e:
                                print(str(e))
                            time.sleep(10)


        # page1.get_by_role("link", name="下一页").click()
        # page1.goto("https://max.book118.com/doclist/list-317-4.html")
        # page1.get_by_role("link", name="计算机应用/办公自动化").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name=" 外语学习 ").click()
        # page1.get_by_role("link", name="俄语").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name=" 报告/分析 ").click()

        # ---------------------
        # context.close()
        # browser.close()


def yuanchuangli(titlePath):
    with sync_playwright() as playwright:
        run(playwright, titlePath)


def main():
    titlePath = r'D:\文档\百度付费上传文档\百度关键词标题\原创力需求标题'
    yuanchuangli(titlePath)


if __name__ == '__main__':
    main()
