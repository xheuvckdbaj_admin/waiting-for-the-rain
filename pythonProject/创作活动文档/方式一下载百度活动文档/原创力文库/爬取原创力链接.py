import time

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.创作活动文档.爬取活动文档标题 import readTxt
import urllib

def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.book118.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)
def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )

def inputKey(driver,lianjieFile,key):
    driver.find_element(By.XPATH, '//input[@class="search-control"]').clear()
    driver.find_element(By.XPATH,'//input[@class="search-control"]').send_keys(key)
    time.sleep(10)
    driver.find_element(By.XPATH, '//input[@class="search-control"]').send_keys(Keys.ENTER)
    try:
        getNewWindow(driver, '//div[@class="title"]', 1000)
    except:
        print('没有内容，或者网速过慢')
    elements=driver.find_elements(By.XPATH,'//div[@class="title"]/a')

    for element in elements:
        title=element.get_attribute('title')
        if mainPanDuanTitle(key, title):
            hrefUrl=element.get_attribute('href')
            print(hrefUrl)
            lianjieFile.write(hrefUrl+'----'+key+'\n')
            break
def mian_Douding(titleFolderPath,txtFilePath,cookiesPath):
    driver=getDriver('douding')
    driver.get('https://max.book118.com/search.html?q=%E4%BD%9C%E4%B8%BA%E8%87%AA%E5%B7%B1%E7%9A%84%E8%A1%8C%E5%8A%A8%E6%8C%87%E5%8D%97.%E4%B8%A4%E5%AD%A6%E4%B8%80%E5%81%9A%E6%98%AF%E5%AD%A6%E5%AD%A6')
    time.sleep(10)
    file=open(titleFolderPath+'\\'+'all.txt','r',encoding='utf-8')
    lianjieFile=open(txtFilePath,'w',encoding='utf-8')
    key_lines=file.readlines()
    for i,key_line in enumerate(key_lines):
        try:
            key=key_line.replace('\n','')
            print(str(i)+'---------'+key)
            inputKey(driver, lianjieFile, key)
        except:
            print('该关键词出错')
    lianjieFile.close()
if __name__ == '__main__':
    titleFolderPath=r'D:\文档\百度活动文档\百度活动文档标题'
    txtFilePath=r'./yuanchuangli.txt'
    cookiesPath=r'./cookies.txt'
    mian_Douding(titleFolderPath,txtFilePath,cookiesPath)