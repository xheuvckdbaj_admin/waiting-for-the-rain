# -*- coding:utf-8 -*-
import docx
import requests


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36'}
def getZip(thmlpath,url):
    requests_text = requests.get(url=url, headers=headers)
    print(str(requests_text.content.decode(encoding='gbk')))
    with open(thmlpath, 'wb') as df:
        for chunk in requests_text.iter_content(chunk_size=128):
            df.write(chunk)

def main():
    thmlpath = r'E:\test\hello.zip'
    url='http://down8.5156edu.com/showzipdown4.php?f_type1=2&id=132702'
    getZip(thmlpath,url)

if __name__ == '__main__':

    main()
