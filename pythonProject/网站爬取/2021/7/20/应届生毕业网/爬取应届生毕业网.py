# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="shangxiaye"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.yjbys.cn/zhichang/qiuzhixin/295477.html',
                'https://www.yjbys.cn/zhichang/zijianxin/295475.html',
                'https://www.yjbys.cn/zhichang/cizhixin/294286.html',
                'https://www.yjbys.cn/zhichang/qingjiatiao/295017.html',
                'https://www.yjbys.cn/zhichang/yingyu/236916.html',
                'https://www.yjbys.cn/zhichang/mianshi/262288.html',
                'https://www.yjbys.cn/zhichang/bishiti/13601.html',
                'https://www.yjbys.cn/zhichang/jiuyezhidao/294457.html',
                'https://www.yjbys.cn/zhichang/jiuyeqianjing/278132.html',
                'https://www.yjbys.cn/zhichang/zhiyeguihua/279454.html',
                'https://www.yjbys.cn/zhichang/qiuzhiliyi/295165.html',
                'https://www.yjbys.cn/zhichang/shixibaogao/295480.html',
                'https://www.yjbys.cn/zhichang/shixizongjie/295329.html',
                'https://www.yjbys.cn/zhichang/biyezengyan/295311.html',
                'https://www.yjbys.cn/zhichang/biyeganyan/294983.html',
                'https://www.yjbys.cn/shiyongwen/geyan/295473.html',
                'https://www.yjbys.cn/shiyongwen/heci/293154.html',
                'https://www.yjbys.cn/shiyongwen/kouhao/294999.html',
                'https://www.yjbys.cn/shiyongwen/jiyu/295122.html',
                'https://www.yjbys.cn/shiyongwen/chengnuoshu/295384.html',
                'https://www.yjbys.cn/shiyongwen/fayangao/295476.html',
                'https://www.yjbys.cn/shiyongwen/shuokegao/295014.html',
                'https://www.yjbys.cn/shiyongwen/daoyouci/295397.html',
                'https://www.yjbys.cn/shiyongwen/changyishu/295421.html',
                'https://www.yjbys.cn/shiyongwen/weituoshu/295444.html',
                'https://www.yjbys.cn/shiyongwen/guangbogao/294938.html',
                'https://www.yjbys.cn/shiyongwen/yanjianggao/295235.html',
                'https://www.yjbys.cn/shiyongwen/ganxiexin/295474.html',
                'https://www.yjbys.cn/shiyongwen/xieyishu/295468.html',
                'https://www.yjbys.cn/shiyongwen/jiantaoshu/295445.html',
                'https://www.yjbys.cn/shiyongwen/zhuchici/295232.html',
                'https://www.yjbys.cn/shiyongwen/zhufuyu/295367.html',
                'https://www.yjbys.cn/shiyongwen/baozhengshu/295430.html',
                'https://www.yjbys.cn/shiyongwen/shenqingshu/295478.html',
                'https://www.yjbys.cn/hr/zhaopinxuanba/2942.html',
                'https://www.yjbys.cn/hr/yuangongguanli/293286.html',
                'https://www.yjbys.cn/hr/xinchouguanli/242912.html',
                'https://www.yjbys.cn/hr/xinchouguanli/242912.html',
                'https://www.yjbys.cn/hr/jixiaokaohe/276717.html',
                'https://www.yjbys.cn/hr/peixunfazhan/100288.html',
                'https://www.yjbys.cn/hr/rencaizhanlue/13544.html',
                'https://www.yjbys.cn/hr/qiyewenhua/290318.html',
                'https://www.yjbys.cn/hr/gangweizhize/295299.html',
                'https://www.yjbys.cn/chuangye/news/99150.html',
                'https://www.yjbys.cn/chuangye/jihua/295467.html',
                'https://www.yjbys.cn/chuangye/gushi/282510.html',
                'https://www.yjbys.cn/chuangye/qiyeguanli/291826.html',
                'https://www.yjbys.cn/chuangye/xiangmu/289223.html',
                'https://www.yjbys.cn/yanjiusheng/zhuanye/12759.html',
                'https://www.yjbys.cn/yanjiusheng/fuxi/59328.html',
                'https://www.yjbys.cn/liuxue/zhuanye/291836.html',
                'https://www.yjbys.cn/liuxue/news/282579.html',
                'https://www.yjbys.cn/liuxue/shenqing/295167.html',
                'https://www.yjbys.cn/liuxue/feiyong/294909.html',
                'https://www.yjbys.cn/liuxue/jingyan/25888.html',
                'https://www.yjbys.cn/chuangye/qiyeguanli/291826.html',
                'https://www.yjbys.cn/chuangye/xiangmu/289223.html',
                'https://www.shuniao.com/huodongjihua/66859.html',
                'https://www.shuniao.com/gongzuojihua/66879.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
