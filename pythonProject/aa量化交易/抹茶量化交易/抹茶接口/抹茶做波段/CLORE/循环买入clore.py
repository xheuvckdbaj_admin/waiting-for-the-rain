import datetime
import decimal
import math
import time
from decimal import Decimal, ROUND_HALF_UP

from pythonProject.aa量化交易.抹茶量化交易.抹茶接口.MEXC import Client, demo


def main():
    client=Client.Client('mx0vgltuaCRckhMEVJ','863b284575b947f481f45ed17604435c')
    # response=client._getSysTimeStamp()
    # client._getDefaultSymbols()
    # response=client._getDepth('BTCUSDT','1000')
    response=client._getSelfSymbols()
    # response=client._getExchangeInfo('DNXUSDT')
    # response = client._getTrades('DNXUSDT','100')
    # for text in response.json():
    #     # print(text['isBuyerMaker'])
    #     # if text['isBuyerMaker']==True:
    #         print(text)
    # response=client._getAggTrades('DNXUSDT','','','500')

    # response = client._getKlines('DNXUSDT','1m','','','1000')
    # response = client._getAvgprice('DNXUSDT')
    # response = client._get24Hr('DNXUSDT')
    # response = client._getPrice('DNXUSDT')
    # response = client._getBookTicker('DNXUSDT')
    response = client._postorder('DNXUSDT','SELL','LIMIT','10','10','0.678','','','')
    response =client._getAccount('5000','')
    # trade=demo.mexc_trade(consts.DOMAIN, 'mx0vgltuaCRckhMEVJ','863b284575b947f481f45ed17604435c')
    # params = {'symbol': 'DNXUSDT',
    #           'side': 'BUY',
    #           'type': 'LIMIT',
    #           'quoteOrderQty': '10',
    #           'price': '0.625',
    #           'quantity':'10'}
    # # params={}
    # response=trade.post_order(params)
    # print(response.json())
    # print(response.status_code)
    # for text in response:
    #     print(text)
    # print(str(response.json()["serverTime"]))
    # return t + "Z"
def mochamain(symbol,equal,highBuy,lowBuy):
    decimal.getcontext().prec = 4
    client = Client.Client('mx0vgltuaCRckhMEVJ', '863b284575b947f481f45ed17604435c')
    #查询还有多少枚dnx
    response = client._getAccount('5000', '')
    maps=response['balances']
    dnxNumber=0
    for map in maps:
        asset=map['asset']
        if asset==symbol.replace('USDT',''):
            free=map['free']
            dnxNumber=decimal.Decimal(free)
        if asset=='USDT':
            USDTfree = map['free']
            USDTNumber = decimal.Decimal(USDTfree)
    print(dnxNumber,USDTNumber)
    #查询24小时dnx最高价格和最低价格
    response = client._get24Hr(symbol)
    highPrice=response['highPrice']
    lowPrice = response['lowPrice']
    askprice=response['askPrice']
    bidPrice=response['bidPrice']
    print(highPrice,lowPrice)

    #计算出买入价格区间
    bandFre = (Decimal(highPrice) - Decimal(lowPrice)) / Decimal(lowPrice)
    if highBuy=='':
        highBuy = decimal.Decimal((bandFre * Decimal(0.5) + Decimal(1)) * Decimal(lowPrice))
    else:
        highBuy=decimal.Decimal(highBuy)
    if lowBuy=='':
        lowBuy = decimal.Decimal((bandFre * Decimal(0.1) + Decimal(1)) * Decimal(lowPrice))
    else:
        lowBuy = decimal.Decimal(lowBuy)
    print(highBuy, lowBuy)

    #根据USDT计算出格数与格数价格差
    n_usdt = math.ceil(Decimal(math.sqrt(USDTNumber*2 / int(equal) + Decimal(0.25))) - Decimal(0.5))
    print(n_usdt)
    diffrPrice_usdt = decimal.Decimal((highBuy - lowBuy) / n_usdt)
    print(diffrPrice_usdt)

    #分批依次委托买入dnx
    for i in range(0,1000000):
        try:
            # time.sleep(1)
            if i !=(n_usdt-1):
                numberPrice=(1)*int(equal)
                buyPrice=highBuy-Decimal(i)*Decimal(0.00000003)
                number=decimal.Decimal(Decimal(numberPrice) /buyPrice)
                print(buyPrice)
                response = client._postorder(symbol, 'BUY', 'LIMIT', str(float(number)), str(float(numberPrice)), str(float(buyPrice)), '', '', '')
                print(response)
            else:
                response = client._getAccount('5000', '')
                maps = response['balances']
                dnxNumber = 0
                for map in maps:
                    asset = map['asset']
                    if asset == 'USDT':
                        USDTfree = map['free']
                        USDTNumber = decimal.Decimal(USDTfree)
                numberPrice=int(USDTNumber)
                number = decimal.Decimal(Decimal(numberPrice) / highBuy)
                response = client._postorder(symbol, 'BUY', 'LIMIT', str(float(number)), str(float(numberPrice)), str(float(lowBuy)), '', '','')
            if i%20==19:
                #获取当前价格
                response = client._getPrice(symbol)
                nowPrice=response['price']
                price=Decimal(nowPrice)*Decimal(1.004)
                response = client._getAccount('5000', '')
                maps = response['balances']
                dnxNumber = 0
                for map in maps:
                    asset = map['asset']
                    if asset == symbol.replace('USDT', ''):
                        free = map['free']
                        dnxNumber = decimal.Decimal(free)
                number = int(dnxNumber)
                response = client._postorder(symbol, 'SELL', 'LIMIT', str(float(number)), str(float(number)),
                                             str(float(price)), '', '', '')
        except:
            print('error')

if __name__ == '__main__':
    symbol='CLOREUSDT'
    equal='6'
    highBuy='50'
    lowBuy='0.006'
    mochamain(symbol,equal,highBuy,lowBuy)