import win32com.client as win32


def groupFile(files,fileName):
    # 打开word软件
    word = win32.gencache.EnsureDispatch('Word.Application')
    # 非可视化运行
    word.Visible = False

    output = word.Documents.Add()  # 新建合并后空白文档
    for file in files:
        print(file)
        output.Application.Selection.Range.InsertFile(file)  # 拼接文档

    # 获取合并后文档的内容
    doc = output.Range(output.Content.Start, output.Content.End)
    #doc.Font.Name = "黑体"  # 设置字体

    output.SaveAs(r'E:\试题专项\小学\卷子'+'\\'+fileName+'.docx')  # 保存
    output.Close()


def main():
    # 需要合并的文档路径，这里有个文档1.docx，2.docx，3.docx.
    files = [r'E:\试题专项\小学\语文\一年级\18看图、写作\1.docx', r'E:\试题专项\小学\语文\一年级\18看图、写作\2.docx',
             r'E:\试题专项\小学\语文\一年级\18看图、写作\3.docx', r'E:\试题专项\小学\语文\一年级\18看图、写作\4.docx',
             r'E:\试题专项\小学\语文\一年级\18看图、写作\5.docx']
    fileName='1.docx'
    groupFile(files,fileName)


if __name__ == '__main__':
    main()