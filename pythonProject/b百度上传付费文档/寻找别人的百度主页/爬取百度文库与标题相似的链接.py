import time

from playwright.sync_api import Playwright, sync_playwright, expect


def run(playwright: Playwright, titlePath, wangzhiPath) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://wenku.baidu.com/?_wkts_=1686275933666")
    page.get_by_placeholder("搜索文档").click()
    file = open(titlePath, 'r', encoding='utf-8')
    fileWangzhi = open(wangzhiPath, 'w', encoding='utf-8')
    file_lines = file.readlines()
    file.close()
    for line in file_lines:
        try:
            keyTitle = line.replace('\n', '')
            page.get_by_role("textbox").nth(0).fill(keyTitle)
            with page.expect_popup() as page1_info:
                page.get_by_role("textbox").nth(0).press("Enter")
            page1 = page1_info.value
            time.sleep(1)
            elements = page1.locator('xpath=//a[@class="search-result-title_33fc4"]')
            print(elements.count())
            for i in range(0, elements.count()):
                content = elements.nth(i).text_content().replace(' - 百度文库', '')
                print(content+'-----'+keyTitle)
                # if content == keyTitle:
                href = elements.nth(i).get_attribute('href')
                fileWangzhi.write(content + '###' + href + '\n')
                print(content, href)
            page1.close()
        except Exception as e:
            print(str(e))
        # for element in elements:
        #     href=element.get_attribute('href')
        #     print(href)
        # with page1.expect_popup() as page2_info:
        #     page1.get_by_role("link", name="千字文歌曲简谱 - 百度文库").click()
        # page2 = page2_info.value

        # ---------------------
    context.close()
    browser.close()
    fileWangzhi.close()


def main():
    titlePath = r'./别人做过的标题.txt'
    wangzhiPath = r'./别人通过活动文档的网址.txt'

    with sync_playwright() as playwright:
        run(playwright, titlePath, wangzhiPath)


if __name__ == '__main__':
    main()
