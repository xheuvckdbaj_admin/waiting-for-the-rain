# -*- coding:utf-8 -*-
import os
import traceback
from win32com import client

import FindFile

'''
将对应文件夹下的doc文件转为docx文件
'''


def doc_to_docx(path):
    if os.path.splitext(path)[1] == ".doc"  :
        try:
            word = client.Dispatch('Word.Application')
            doc = word.Documents.Open(path)  # 目标路径下的文件
            doc.SaveAs(os.path.splitext(path)[0] + ".docx", 16)  # 转化后路径下的文件
            doc.Close()
            word.Quit()
            #os.remove(os.path.splitext(path)[0] + ".doc")
            print("hello")
        except Exception as e:
            traceback.print_exc()

def find_file(path, ext, file_list=[]):
    dir = os.listdir(path)
    for i in dir:
        i = os.path.join(path, i)
        if os.path.isdir(i):
            find_file(i, ext, file_list)
        else:
            if ext == os.path.splitext(i)[1]:
                file_list.append(i)
    return file_list

def transition(oldFolderPath):
    dir_path = oldFolderPath # 批量转换文件夹
    ext = ".doc"
    file_list = find_file(dir_path, ext)
    for file in file_list:
        if not '$' in file:
            doc_to_docx(file)

def main():
    oldFolderPath=r'F:\test'
    transition(oldFolderPath)
    #delDOC(oldFolderPath)

if __name__ == '__main__':
    main()