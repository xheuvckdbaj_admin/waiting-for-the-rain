# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r".\cookies.txt"

def getElement(browser,hrefs,file_handle):
    for e in range(0, len(hrefs)):
        try:
            print(hrefs[e])
            browser.get(hrefs[e])
            time.sleep(0.2)
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//li[@class="list-group-item"]'))
            )

            source=browser.find_element_by_css_selector('li.list-group-item > button.btn.btn-primary').get_attribute('data-sourceid')
            newUrl='https://pan.baidu.com/s/'+source
            print(newUrl)

            file_handle.write(newUrl + '\n')
        except:
            print('资源不存在')
            continue
def getBrowser():
    try:
        browser = webdriver.Chrome()
        return browser
    except:
        time.sleep(10)
        return getBrowser()

def getLinUrl(law,url,key,way,file_handle):
        error=law
    # try:
        # browser = getBrowser(r'https://www.lingfengyun.com/')
        # 添加cookies
        browser = getBrowser()
        browser.get('https://www.laisoyixia.com/')
        addLaisoucookies.addCookies(browser, 'https://www.laisoyixia.com/', filePath)
        newUrl = url % (key,1,way)
        browser.get(newUrl)
        text=browser.find_element_by_xpath('//ul[@class="pager"]/li[@class="white-space"]').text
        allPage=int(re.match('当前 1/(.*)页',text).group(1))
        print(allPage)

        for page in range(1,allPage):
            newUrl = url % (key, page,way)
            browser.get(newUrl)
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="result-piece"]'))
            )
            eles=browser.find_elements_by_xpath('//div[@class="result-piece"]/div[@class="row"]/div[@class="col-lg-10 col-xs-9"]/'
                                                'span[@class="title"]/a[@class="source-title"]')
            print()
            hrefs=[]
            for ele in eles:
                href=ele.get_attribute('href')
                hrefs.append(href)
            print(hrefs)
            getElement(browser, hrefs, file_handle)
        browser.quit()
    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,key,path):
    url='https://www.laisoyixia.com/s/search?q=%s&currentPage=%d&f=%d&o=1'
    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')
    ways=[4,6,8]
    for way in ways:
        getLinUrl(law, url, key, way, file_handle)
    file_handle.close()


def main():
    keys=['人保车险条款',
'发布会',
'企业信息公开制度',
'廉政意见',
'证明格式函',
'环境工程原理',
'人才培养',
'奖学金推荐信',
'担保合同',
'生物安全培训',
'事务性通知',
'医学心理学',
'员工奖惩管理办法',
'工作证明模板',
'病例讨论模板',
'谈心谈话',
'网络安全',
'停课通知',
'课前五分钟演讲素材',
'防疫班会',
'信息系统',
'业务规模',
'流量计算',
'收入证明',
'专家推荐信',
'管理咨询与诊断',
'品牌全案策划',
'新型智慧城市',
'顶层设计',
'教师发展测评',
'国家标准分类',
'企业年度总结',
'排放标准',
'作息时间安排',
'工资发放明细',
'推优自荐书',
'行业现状',
'绘本故事',
'廉洁教育',
'方案汇报',
'分红协议',
'封面设计',
'个人成就',
'核心素养',
'股份年报',
'工作推进',
'基本要素',
'标志牌规范',
'行为规范',
'施工招标',
'个人年度计划',
'扫黑除恶',
'贫困户证明',
'群众的意见',
'排污许可申请',
'形势与政策',
'就业创业',
'自学考试',
'成本公式']
    for key in keys:
            print(key)
            path=r'D:\文档\url\来搜一下'
            law=0
            login(law,key,path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
