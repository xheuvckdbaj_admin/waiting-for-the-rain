# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.article')[0])
            title = soup.select('div.article_main > h1.title')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')


def getHtml2(thmlpath, strUrl):
    try:
        print(strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('div.body-detail')[0])
        title = soup.select('div.body-left > h1.body-title')[0].text.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('编码出错')
def main(thmlpath):
    url='https://china.findlaw.cn/fagui/p_1/%d.html'
    url2='https://china.findlaw.cn/law/article_%d.html'
    for i in range(1,401024):
        newUrl=url%i
        getHtml(thmlpath, newUrl)
    for i in range(1,20672):
        newUrl=url2%i
        getHtml2(thmlpath, newUrl)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
