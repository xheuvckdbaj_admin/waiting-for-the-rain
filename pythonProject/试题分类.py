# -*- coding:utf-8 -*-
from datetime import time

import docx
import os


from FindFile import findAllFile, find_file
from blank import blank
from judgement import delBlankFile
from parameter import parameter
from win32com import client as wc
'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def  deleAdvertising(filePath,newFolderPath,front,later):
    doc=filePath.split(".")[len(filePath.split("."))-1]
    #word = wc.Dispatch("Word.Application")
    if doc=='docx':

            file=docx.Document(filePath)
            fileName = os.path.basename(filePath)
            if len(fileName)<11:
                fileName='全国中考'+fileName
                print(fileName)

            #print("段落数:" + str(len(file.paragraphs)))

            #print('删除前图形图像的数量：', len(file.inline_shapes))
            i = 0
            str=[]
            file.paragraphs[0].text=''
            if'正确读音' in file.paragraphs[1].text or '正确的读音' in file.paragraphs[1].text \
                    or '音节正确' in file.paragraphs[1].text or '恰当的读音' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\1拼音'
                file.paragraphs[1].text = '一、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif '写词语' in file.paragraphs[1].text or '连一连' in file.paragraphs[1].text:
                newFolderPath = r'E:\试题专项\小学\语文\三年级\2拼写词语'
                file.paragraphs[1].text='二、'+file.paragraphs[1].text
                # blank(file.paragraphs[0])
                file.save(newFolderPath + '\\' + fileName)
            # delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'正确的字' in file.paragraphs[1].text or '下面的字' in file.paragraphs[1].text or '合适的字' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\3字'
                file.paragraphs[1].text = '三、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'下列词语' in file.paragraphs[1].text :
                newFolderPath=r'E:\试题专项\小学\语文\三年级\4词语'
                file.paragraphs[1].text = '四、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'课文' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\5课文填空'
                file.paragraphs[1].text = '五、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'词语填空' in file.paragraphs[1].text or '关联词' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\6关联词填空、词语填空'
                file.paragraphs[1].text = '六、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件 选词填空
            elif'什么意思' in file.paragraphs[1].text or '指的是' in file.paragraphs[1].text \
                    or '正确的解释' in file.paragraphs[1].text or '选词填空' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\7字词解释'
                file.paragraphs[1].text = '七、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'恰当的词语' in file.paragraphs[1].text or '词语运用' in file.paragraphs[1].text or '造句' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\8字词语运用'
                file.paragraphs[1].text = '八、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'习俗' in file.paragraphs[1].text or '节日' in file.paragraphs[1].text or '实践' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\9节日，习俗'
                file.paragraphs[1].text = '九、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'我会想' in file.paragraphs[1].text or '聊天室' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\11情景对话'
                file.paragraphs[1].text = '十一、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'把字句' in file.paragraphs[1].text or '拟人句' in file.paragraphs[1].text \
                    or '病句' in file.paragraphs[1].text or '诗' in file.paragraphs[1].text or '修辞手法' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\12诗词，文言文修改病句、把字句'
                file.paragraphs[1].text = '十二、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'照样子' in file.paragraphs[1].text or '造句' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\13句子'
                file.paragraphs[1].text = '十三、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'排序' in file.paragraphs[1].text or '排队' in file.paragraphs[1].text or '排列' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\14排序'
                file.paragraphs[1].text = '十四、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'量词' in file.paragraphs[1].text :
                newFolderPath=r'E:\试题专项\小学\语文\三年级\15量词、叠词'
                file.paragraphs[1].text = '十五、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif'阅读课文片段' in file.paragraphs[1].text or '阅读课文《' in file.paragraphs[1].text or '课内阅读' in file.paragraphs[1].text:
                newFolderPath=r'E:\试题专项\小学\语文\三年级\16课内阅读'
                file.paragraphs[1].text = '十六、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件
            elif '阅读理解' in file.paragraphs[1].text or '课外阅读' in file.paragraphs[1].text \
                    or '快乐阅读' in file.paragraphs[1].text or '阅读短文' in file.paragraphs[1].text \
                    or '阅读下文' in file.paragraphs[1].text or '阅读语段' in file.paragraphs[1].text:
                newFolderPath = r'E:\试题专项\小学\语文\三年级\17看图回答问题、课外阅读'
                file.paragraphs[1].text = '十七、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件 续写
            elif '写话' in file.paragraphs[1].text or '主题练笔' in file.paragraphs[1].text\
                    or '续写' in file.paragraphs[1].text or '写作' in file.paragraphs[1].text or '作文' in file.paragraphs[1].text:
                newFolderPath = r'E:\试题专项\小学\语文\三年级\18看图写作'
                file.paragraphs[1].text = '十八、' + file.paragraphs[1].text
                file.save(newFolderPath + '\\' + fileName)
                os.remove(filePath)  # 删除原来修改前的docx文件


        #word.Quit()


def main():
    #os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'E:\试题专项\小学\语文\三年级'
    newFolderPath = r'E:\试题专项\小学\语文\三年级'
    dir = os.listdir(oldFolderPath)
    #file_list=[]
    for i in dir:
        filePath = os.path.join(oldFolderPath, i)
        if os.path.isfile(filePath):
            if not '$' in filePath:
                print(filePath)
                deleAdvertising(filePath, newFolderPath,0, 0) # 修改，删除广告
if __name__ == '__main__':
    main()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
