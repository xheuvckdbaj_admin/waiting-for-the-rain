import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_puchengziyuan(url, htmlPath, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.entry-head > h1')[0].text.replace('\r','').replace('\n','').replace(' ','')
        list_soupa = str(soup.select('div.entry-content')[0])
        content=str(soup.select('div.entry-content > div.entry-copyright')[0])
        list_soupa=list_soupa.replace(title, title).replace(content,'')
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('蒲城资源下载成功----', title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.puchedu.cn/ziyuan/2944.html'
    htmlPath = r'D:\文档\百度付费上传文档\百度html\第一范文网'
    keyStr = 'kaixin'
    title = ''
    mian_puchengziyuan(url, htmlPath, keyStr, title)
