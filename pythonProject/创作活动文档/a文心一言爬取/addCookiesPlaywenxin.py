import json
import urllib.parse


def readTxt(filePath):
    with open(filePath, "r",encoding='utf-8') as f:  # 打开文件
        data = f.read()  # 读取文件
        print(str(data))
    return  str(data)
def addCookiesPlaywenxin(filePath):
    cookiesJion=[]
    cookies = readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['path'] = '/'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        cookiesJion.append(cookie)
    return  cookiesJion

if __name__ == '__main__':
    addCookiesPlaywenxin('./baiduCookies.txt')