#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/8/29 16:18
# @Author  : ziyu
# @File    : yanzhengma.py
# @Software: PyCharm
# 导入必要的库
# pip install captcha numpy matplotlib tensorflow-gpu
from captcha.image import ImageCaptcha
import matplotlib.pyplot as plt
import numpy as np
import random
import cv2

# %matplotlib inline
# %config InlineBackend.figure_format = 'retina'

import string
characters = string.digits + string.ascii_uppercase
print(characters)

width, height, n_len, n_class = 128, 64, 4, len(characters)


import tensorflow as tf
# import tensorflow.keras.backend as K
# 防止 tensorflow 占用所有显存
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(sess)


from tensorflow.keras.utils import Sequence
# 定义数据生成器
class CaptchaSequence(Sequence):
    def __init__(self, characters, batch_size, steps, n_len=4, width=128, height=64):
        self.characters = characters
        self.batch_size = batch_size
        self.steps = steps
        self.n_len = n_len
        self.width = width
        self.height = height
        self.n_class = len(characters)
        self.generator = ImageCaptcha(width=width, height=height)
    
    def __len__(self):
        return self.steps

    def __getitem__(self, idx):
        X = np.zeros((self.batch_size, self.height, self.width, 3), dtype=np.float32)
        y = [np.zeros((self.batch_size, self.n_class), dtype=np.uint8) for i in range(self.n_len)]
        for i in range(self.batch_size):
            random_str = ''.join([random.choice(self.characters) for j in range(self.n_len)])
            X[i] = np.array(self.generator.generate_image(random_str)) / 255.0
            for j, ch in enumerate(random_str):
                y[j][i, :] = 0
                y[j][i, self.characters.find(ch)] = 1
        return X, y
# 定义网络结构
from tensorflow.keras.models import *
from tensorflow.keras.layers import *

input_tensor = Input((height, width, 3))
x = input_tensor
for i, n_cnn in enumerate([2, 2, 2, 2, 2]):
    for j in range(n_cnn):
        x = Conv2D(32*2**min(i, 3), kernel_size=3, padding='same', kernel_initializer='he_uniform')(x)
        x = BatchNormalization()(x)
        x = Activation('relu')(x)
    x = MaxPooling2D(2)(x)

x = Flatten()(x)
x = [Dense(n_class, activation='softmax', name='c%d'%(i+1))(x) for i in range(n_len)]
model = Model(inputs=input_tensor, outputs=x)

print(model.summary())

def decode(y):
    y = np.argmax(np.array(y), axis=2)[:,0]
    return ''.join([characters[x] for x in y])
data = CaptchaSequence(characters, batch_size=1, steps=1)
X, y = data[0]
print(X.shape)

model.load_weights('cnn_best.h5')
# 预测生成的数据X
y_pred = model.predict(X)
# 输出预测结果
print(decode(y_pred))
# 画出结果
# plt.title('real: %s\npred:%s'%(decode(y), decode(y_pred)))
# plt.imshow(X[0], cmap='gray')
# plt.axis('off')


# 读取图片预测
image_data = cv2.imread(r'D:\test\genimage.jpg')
image_data = cv2.resize(image_data,(128,64))
image_data = image_data[np.newaxis,:] 
# 预测生成的数据X
y_pred = model.predict(image_data)
# 输出预测结果
print(decode(y_pred))
print(decode(y_pred))
