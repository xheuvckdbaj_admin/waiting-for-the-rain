# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.congmiao.com/gongzuobaogao/187887.html',
                'https://www.congmiao.com/shijianbaogao/188707.html',
                'https://www.congmiao.com/shixibaogao/188753.html',
                'https://www.congmiao.com/shuzhibaogao/188422.html',
                'https://www.congmiao.com/diaochabaogao/188706.html',
                'https://www.congmiao.com/zichabaogao/188720.html',
                'https://www.congmiao.com/lizhibaogao/188722.html',
                'https://www.congmiao.com/cizhibaogao/188777.html',
                'https://www.congmiao.com/gongzuozongjie/188773.html',
                'https://www.congmiao.com/huodongzongjie/188693.html',
                'https://www.congmiao.com/nianzhongzongjie/188455.html',
                'https://www.congmiao.com/shixizongjie/188686.html',
                'https://www.congmiao.com/xuexizongjie/188335.html',
                'https://www.congmiao.com/peixunzongjie/188444.html',
                'https://www.congmiao.com/biaoyangxin/188770.html',
                'https://www.congmiao.com/daoqianxin/188769.html',
                'https://www.congmiao.com/jieshaoxin/187866.html',
                'https://www.congmiao.com/cizhixin/188704.html',
                'https://www.congmiao.com/qiuzhixin/188782.html',
                'https://www.congmiao.com/zijianxin/188776.html',
                'https://www.congmiao.com/ganxiexin/188771.html',
                'https://www.congmiao.com/hetong/188791.html',
                'https://www.congmiao.com/jihua/188758.html',
                'https://www.congmiao.com/fangan/188785.html',
                'https://www.congmiao.com/yanjianggao/188786.html',
                'https://www.congmiao.com/fayangao/188767.html',
                'https://www.congmiao.com/zhuchigao/188685.html',
                'https://www.congmiao.com/yaoqinghan/188784.html',
                'https://www.congmiao.com/cehuashu/188687.html',
                'https://www.congmiao.com/shenqingshu/188752.html',
                'https://www.congmiao.com/baozhengshu/188793.html',
                'https://www.congmiao.com/jianyishu/188783.html',
                'https://www.congmiao.com/zhiyeguihua/183425.html',
                'https://www.congmiao.com/ziwojieshao/188780.html',
                'https://www.congmiao.com/ziwopingjia/188383.html',
                'https://www.congmiao.com/ziwojianding/188787.html',
                'https://www.congmiao.com/xindetihui/188764.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    # main(thmlpath)
    mainGetHtml(thmlpath, txtFilepath)
