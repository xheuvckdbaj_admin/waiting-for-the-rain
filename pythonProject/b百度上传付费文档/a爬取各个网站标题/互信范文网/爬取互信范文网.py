import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_huxinfanwen(url,file,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div > h2')[0].text.split('|')[0]
        print(title)
        file.write(title+'\n')
    except:
        print('error')

if __name__ == '__main__':
    # url = 'https://www.huxinfoam.com/baogaozongjie/340327/'
    # htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    # keyStr='kaixin'
    # title=''
    # mian_huxinfanwen(url,htmlPath,keyStr,title)

    url = 'https://www.huxinfoam.com/%s/%s/'
    htmlPath = r'D:\文档\百度付费上传文档\需要的标题及其chatgpt\title\互信范文网.txt'
    keyStr = 'kaixin'
    title = ''
    file = open(htmlPath, 'w', encoding='utf-8')
    keys = ['baogaozongjie', 'fanwendaquan',
            'xindetihui', 'zhichangzhinan',
            'yanjiangfayan', 'jianghuazhici']
    for key in keys:
        for i in range(1, 840327):
            newUrl = url % (key, str(i))
            # print(newUrl)
            mian_huxinfanwen(newUrl, file, keyStr, title)
    file.close()