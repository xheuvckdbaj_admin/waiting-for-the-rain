# -*- coding:utf-8 -*-
from selenium.webdriver import ActionChains
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addcookies1
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}

def getNewWindow(browser,xpathStr,timeLen):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeLen).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser



def login(strUrl):
    try:
        browser=webdriver.Chrome()
        browser.get(strUrl)
        addcookies1.addCookies(browser, strUrl)
        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        print('当前句柄: ', n)  # 会打印所有的句柄
        # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
        browser.switch_to.window(n[-1])
        time.sleep(5)
        html = browser.page_source
        for i in range(0,50000):
            browser.find_element_by_xpath('//div[@class="nav-item"][1]').click()
            getNewWindow(browser, '//div[@class="el-select select-status"]', 100)
            browser.find_element_by_xpath('//div[@class="el-select select-status"]').click()
            getNewWindow(browser, '//li', 100)
            browser.find_elements_by_css_selector('li.el-select-dropdown__item')[2].click()
            getNewWindow(browser, '//div[@class="cell"]/button[@class="el-button el-button--default el-button--mini"]', 100)
            element = browser.find_element_by_xpath(
                '//div[@class="cell"]/button[@class="el-button el-button--default el-button--mini"][1]')
            print(element.text)
            if element.text=='编辑':
                element.click()
                getNewWindow(browser, '//label[@class="input-item"]/input[@class="mr6"]', 100)
                browser.find_elements_by_xpath('//label[@class="input-item"]/input[@class="mr6"]')[1].click()
                getNewWindow(browser, '//span[@class="btn-submit"]', 100)
                browser.find_element_by_xpath('//span[@class="btn-submit"]').click()
                getNewWindow(browser, '//div[@class="dialog-footer"]/div[@class="btn-view-submitted-know"]', 100)
                browser.find_element_by_xpath('//div[@class="dialog-footer"]/div[@class="btn-view-submitted-know"]').click()
                getNewWindow(browser, '//div[@class="el-select select-status"]', 100)
    except:
        print('出错了')
        time.sleep(10)
        browser.quit()
        login(strUrl)

def main(thmlpath):
            strUrl='https://cuttlefish.baidu.com/shopmis#/commodityManage/documentation'
            login(strUrl)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main(thmlpath)
