import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright
def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
def clickPage(page):
    time.sleep(5)
    page.get_by_text("下一页").click()
def getPageTitles(page, titlePath):
    file=open(titlePath,'w',encoding='utf-8')
    time.sleep(1)
    elements=page.locator('//div[@class="doc-item"]/div[@class="title"]')
    eleNumbers=elements.count()
    print(eleNumbers)
    for i in range(0,eleNumbers):
        title=elements.nth(i).text_content().strip()
        print(title)
        file.write(title+'\n')
    file.close()

def getTitles(page, titlefolder,startNumber,endNumber):
    newTitlefolder=titlefolder+'\\'+str(startNumber)
    makeFolder(newTitlefolder)
    page.locator('//div[@class="price-range"]/div[@class="price-start"]/input[@type="number"]').nth(0)
    page.get_by_role("spinbutton").first.click()
    page.get_by_role("spinbutton").first.fill(str(startNumber))

    time.sleep(0.2)
    page.get_by_role("spinbutton").nth(1).click()
    page.get_by_role("spinbutton").nth(1).fill(str(endNumber))

    time.sleep(0.2)
    page.get_by_text("确认").nth(1).click()
    time.sleep(0.2)
    titlePath=newTitlefolder+'\\'+'1.txt'
    getPageTitles(page, titlePath)
    for i in range(2,21):
        clickPage(page)
        titlePath = newTitlefolder + '\\' +str(i)+ '.txt'
        getPageTitles(page, titlePath)

    time.sleep(100)
def run(playwright: Playwright,titlefolder,titlefolderAll,fileNoPass,number):
    urlFolder=titlefolderAll+'\\'+'url'
    titleFolderx = titlefolderAll + '\\' + 'title'
    makeFolder(urlFolder)
    makeFolder(titleFolderx)
    brower = playwright.chromium.launch(headless=True)
    context = brower.new_context()
    page = context.new_page()
    page.set_default_timeout(6000)
    titlePath=titlefolder+'\\'+'title.txt'
    file=open(titlePath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for i,line in enumerate(file_lines):
        if i > number:
            try:
                key=line.replace('\n','')
                print(key)
                urlPath=urlFolder+'\\'+key+'.txt'
                urlFile=open(urlPath,'w',encoding='utf-8')
                # titlePathx = titleFolderx + '\\' + key + '.txt'
                # titleFile = open(titlePathx, 'w', encoding='utf-8')
                page.goto('https://wenku.baidu.com/?_wkts_=1695785138509')
                time.sleep(2)
                page.locator("form").get_by_role("textbox").click()
                page.locator("form").get_by_role("textbox").fill(key)
                with page.expect_popup() as page1_info:
                    page.locator("form").get_by_role("textbox").press("Enter")
                page1 = page1_info.value
                time.sleep(2.5)
                elements=page1.locator('//a[@class="search-result-title_33fc4"]')
                numbers=elements.count()
                for i in range(0,numbers):
                    href=elements.nth(i).get_attribute('href')
                    print(href)
                    urlFile.write(href+'\n')
                urlFile.close()
                page1.close()
                time.sleep(0.2)
            except Exception as e:
                fileNoPass.write(line)
                print(str(e))
                print('error')
                page.close()
                context.close()
                brower.close()
                brower = playwright.chromium.launch(headless=True)
                context = brower.new_context()
                page = context.new_page()
                page.set_default_timeout(6000)
                continue
    # for i in range(0,10000):
    #     try:
    #         startNumber=i
    #         endNumber=startNumber+1
    #         getTitles(page, titlefolder,startNumber,endNumber)
    #         time.sleep(50)
    #     except Exception as e:
    #         print(str(e))
    #         continue
def yuanchuangli(titlefolder,titlefolderAll,fileNoPass,number):
    with sync_playwright() as playwright:
        run(playwright, titlefolder,titlefolderAll,fileNoPass,number)


def main():
    titlefolder = r'D:\文档\百度付费上传文档\标题'
    titlefolderAll=r'D:\文档\百度付费上传文档\标题'
    noPassTitlePath=r'./title.txt'
    fileNoPass=open(noPassTitlePath,'w',encoding='utf-8')
    number=-1
    yuanchuangli(titlefolder,titlefolderAll,fileNoPass,number)
    fileNoPass.close()
if __name__ == '__main__':
    main()
