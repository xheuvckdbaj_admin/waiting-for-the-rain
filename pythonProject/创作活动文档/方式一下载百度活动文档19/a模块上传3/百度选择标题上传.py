import math
import os
import threading

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.chatGPT自动生成文档.chatGPT自动生成文档 import main_chatGPT
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档 import mainTitleClassifyUploadApi
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.判断百度账号是否传满 import panduanUploadOver
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.合并txt内容 import merge3
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.挑选需要的标题 import selectTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.爬取整个账号的标题 import getAllTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.爬取整个账号的标题playwright import crawTitleAll
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.百度账号上传过的标题 import mainPassTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.allyunxing import main_arrangeDocx
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.shear import shear_dile
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.百度上传文档分类 import panduanName


def mergeTxt(titleFolder, path):
    txtpaths = find_file(titleFolder)
    newTxtPath = path + '\\' + 'all.txt'
    paths = []
    txtfile_line = open(newTxtPath, 'w', encoding='utf-8')
    for txtpath in txtpaths:
        if txtpath.endswith('.txt'):
            print(txtpath)
            try:
                file_line = open(txtpath, 'r', encoding="utf-8")
                readlines = file_line.readlines()
                for line in readlines:
                    if not line in paths:
                        # paths.append(line)
                        txtfile_line.write(line)
                file_line.close()
                # os.remove(txtpath)
            except:
                try:
                    file_line = open(txtpath, 'r', encoding="gbk")
                    readlines = file_line.readlines()
                    for line in readlines:
                        if not line in paths:
                            # paths.append(line)
                            txtfile_line.write(line)
                    file_line.close()
                    # os.remove(txtpath)
                except:
                    print('gbk读不出')
                    continue
    txtfile_line.close()


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def download_slect(folderPath, file_lines, apiKeyPath, startNumberApi, lastNUmber_api):
    file_apiKey = open(apiKeyPath, 'r', encoding='utf-8')
    api_keys = file_apiKey.readlines()

    file_apiKey.close()
    last = len(file_lines)
    print(last)
    if last >= 0:
        if last >= lastNUmber_api:
            last_chatNUmber = lastNUmber_api
        else:
            last_chatNUmber = last
        chatAn = math.ceil((last_chatNUmber - startNumberApi) / 8)

        t1 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[0].replace('\n', '').strip(), startNumberApi, startNumberApi + chatAn))
        t2 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[1].replace('\n', '').strip(), startNumberApi + chatAn,startNumberApi + chatAn * 2))
        t3 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[2].replace('\n', '').strip(), startNumberApi + chatAn * 2,startNumberApi + chatAn * 3))
        t4 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[3].replace('\n', '').strip(), startNumberApi + chatAn * 3,startNumberApi + chatAn * 4))
        t5 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[4].replace('\n', '').strip(), startNumberApi + chatAn * 4,startNumberApi + chatAn * 5))
        t6 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[5].replace('\n', '').strip(), startNumberApi + chatAn * 5,startNumberApi + chatAn * 6))
        t7 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[6].replace('\n', '').strip(), startNumberApi + chatAn * 6,startNumberApi + chatAn * 7))
        t8 = threading.Thread(target=main_chatGPT, args=(file_lines, folderPath, api_keys[7].replace('\n', '').strip(), startNumberApi + chatAn * 7,startNumberApi + chatAn * 8))
        # t9 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_keys[8].replace('\n', '').strip(), startNumberApi + chatAn * 8,startNumberApi + chatAn * 9))
        # t10 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_keys[9].replace('\n', '').strip(), startNumberApi + chatAn * 9, last_chatNUmber))

        t1.daemon = 1
        t1.setDaemon(True)
        t2.setDaemon(True)
        t3.setDaemon(True)
        t4.setDaemon(True)
        t5.setDaemon(True)
        t6.setDaemon(True)
        t7.setDaemon(True)
        t8.setDaemon(True)
        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()
        t8.start()
        # t9.start()
        # t10.start()
        # t11.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
        t6.join()
        t7.join()
        t8.join()
        # t9.join()
        # t10.join()
        # t11.join()


def getTitleHuo(titleFolder, titleFolderClassify, passTitlePath, cookiesPath, selectPath, titlePath):
    # 爬取账号所有标题
    getAllTitle(titleFolder, titleFolderClassify, passTitlePath, cookiesPath)
    mergeTxt(titleFolder, titleFolder)
    # 选择必会通过的标题
    selectTitle(selectPath, titlePath)


def mainBaiduSelectUpload19(cookiesPath):
    passTitleFolder = r'D:\文档\百度活动文档19\百度上传过的标题'
    titleFolder = r'D:\文档\百度活动文档19\百度活动文档标题'
    titleFolderClassify = r'D:\文档\百度活动文档19\用来分类文档标题'
    # 爬取账号前几天上传过的标题
    try:
        mainPassTitle(passTitleFolder, cookiesPath)
    except:
        print('新号')
    passTitlePath = r'D:\文档\百度活动文档19\百度上传过的标题\titlePass.txt'
    selectPath = r'./必会通过标题.txt'
    titleAllPath = r'D:\文档\百度活动文档19\百度活动文档标题' + '\\' + 'all.txt'
    selectTitlePath = titleAllPath.replace('all', 'select')
    # 爬取账号所有标题,选择必会通过的标题
    crawTitleAll(cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath)
    fileSelect = open(selectTitlePath, 'r', encoding='utf-8')
    file_lines = fileSelect.readlines()
    fileSelect.close()
    folderPath = r'D:\文档\百度活动文档19\百度html\其他网站'
    mediumFolderPath = r'D:\文档\百度活动文档19\中介'
    midFolderPath = r'D:\文档\自查报告网url'
    newPath = r'D:\文档\百度活动文档19\修改后百度活动文档'
    uploadFolder = r'D:\文档\百度活动文档19\百度活动文档上传'
    apiKeyPath = r'./api_key.txt'
    titleFilePath = titleFolder + '\\' + 'select' + '.txt'
    panduanFilePath = r'D:\文档\百度活动文档19\百度活动文档上传\账号\账号.txt'
    makeFolder(panduanFilePath.replace('\账号.txt', ''))
    number = math.ceil(len(file_lines) / 10)
    if number == 0:
        long = 0
    elif number == 1:
        long = 1
    elif number == 2:
        long = 2
    elif number > 2 and number <= 5:
        long = 3
    elif number > 5 and number <= 10:
        long = 4
    elif number > 10 and number <= 12:
        long = 5
    elif number > 12:
        long = 6
    if number > 0:
        for i in range(0, long):
            if i == 0:
                startNumberApi = i
                lastNUmber_api = i + 12
            elif i == 1:
                startNumberApi = 12
                lastNUmber_api = 12 + 12
            elif i == 2:
                startNumberApi = 24
                lastNUmber_api = 24 + 32
            elif i == 3:
                startNumberApi = 56
                lastNUmber_api = 56 + 52
            elif i == 4:
                startNumberApi = 108
                lastNUmber_api = 108 + 22
            elif i == 5:
                startNumberApi = 130
                lastNUmber_api = 130 + 32

            # 下载文档
            download_slect(folderPath, file_lines, apiKeyPath, startNumberApi, lastNUmber_api)

            # 整理文档
            main_arrangeDocx(folderPath, mediumFolderPath, midFolderPath, newPath)
            # 分类文档
            shear_dile(newPath, newPath)
            # word转pdf
            # mainWordZhuanPdf(newPath)
            # 通过标题文件分类文档
            panduanName(titleFolderClassify, uploadFolder, newPath)
            # # 上传文档
            # # cookiesPath = './baiduCookies.txt'
            # # dataNumber = 0
            # # mainTitleClassify(folderPath, cookiesPath, newPath, dataNumber)

            fileW = open(panduanFilePath, 'w', encoding='utf-8')
            fileW.write('')
            fileW.close()
            mainTitleClassifyUploadApi(uploadFolder, cookiesPath, panduanFilePath)
            if panduanUploadOver(cookiesPath):
                break
    for i in range(0, 8):
        if not panduanUploadOver(cookiesPath):
            if i==0:
                startNumberApi = 0
                lastNUmber_api = 100
                # crawTitleAll(cookiesPath, titleFolder, titleFolderClassify, passTitlePath, selectTitlePath, selectPath,titleAllPath)
                fileSelect = open(selectTitlePath, 'r', encoding='utf-8')
                file_lines = fileSelect.readlines()
                fileSelect.close()
                # 下载文档
                download_slect(folderPath, file_lines, apiKeyPath, startNumberApi, lastNUmber_api)
                # 整理文档
                main_arrangeDocx(folderPath, mediumFolderPath, midFolderPath, newPath)
                # 分类文档
                panduanName(titleFolderClassify, uploadFolder, newPath)
                mainTitleClassifyUploadApi(uploadFolder, cookiesPath, panduanFilePath)
            fileTitle = open(titleAllPath, 'r', encoding='utf-8')
            fileTitle_lines = fileTitle.readlines()
            fileSelect.close()
            titleNumber = len(fileTitle_lines) / 2
            startNumberApi = titleNumber + i * 20
            lastNUmber_api = 20 + startNumberApi
            download_slect(folderPath, fileTitle_lines, apiKeyPath, startNumberApi, lastNUmber_api)
            main_arrangeDocx(folderPath, mediumFolderPath, midFolderPath, newPath)
            panduanName(titleFolderClassify, uploadFolder, newPath)
            mainTitleClassifyUploadApi(uploadFolder, cookiesPath, panduanFilePath)
        else:
            break


if __name__ == '__main__':
    cookiesPath = './baiduCookies1.txt'
    mainBaiduSelectUpload19(cookiesPath)