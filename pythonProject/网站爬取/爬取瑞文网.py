# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.ruiwen.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('div > h1')[0].text
    print(title)
    list_soupa = str(soup.select('.article > .content')[0])
    print(list_soupa)
    content=re.sub('<a href=.*?</a></p>','',list_soupa)
    title = soup.select('.article > .title')[0].text
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('a')
    #print(url_list)
    for a in a_list:
        try:
            href=a.get('href')
            title=a.get('title')
            getContent(thmlpath,href,title)
        except:
            print('错误')
            continue
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('#NEW_INFO_LIST > dl')
    #print(url_list)
    for u in url_list:
            src=u.select('dt > a')[0].get('href')
            href='http://www.banzhuren.cn'+src
            print(href)
            title = u.select('dt > a')[0].text
            getContent(thmlpath,href,title)

def getUrlPageNum(url,str):
    for pageNum in range(1,1000):
        try:
            if pageNum==1:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    newUrl=url+str+'.html'
                    getUrlIndex(thmlpath, newUrl)

            else:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    oldUrl=url+str+'_%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(thmlpath, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['chengyu','jinyici','fanyici','miyu','xiehouyu','hanyupinyin','tongyici',
           'zaoju','langsonggao','shuokegao','jiaoxuefansi','zhengwen','jiaoxuesheji',
           'yuwen/biyuju','yuwen/nirenju','yuwen/paibiju','yuwen/fanwenju','yuwen/xiti','yuwen/gushijueju','deyu',
           'yuwen/shujiazuoye','yuwen/hanjiazuoye','yuwen/xiezuozhidao','yuwen/kewaizhishi',
           'yuwen/putonghua','jingdianmeiwen','juzi','shige','sanwen','meiwensuibi','riji','zhaichao','renshengzheli',
           'lizhi','gushi','mingyan','qianming','shuoshuo','zuoyouming','yulu',
           'huayu','qingshu','zuowen/jieri','zuowen/renwu','zuowen/dongwu','zuowen/zhiwu','zuowen/shijianzuowen','zuowen/ziran',
           'zuowen/qinggan',
           'zuowen/gongde','zuowen/zheli','zuowen/chengzhang','zuowen/wenhua','zuowen/lizhi','zuowen/diyu',
           'zuowen/redianhuati', 'zuowen/qitahuati', 'zuowen/chuzhong', 'zuowen/gaozhong', 'zuowen/danyuan', 'zuowen/yingyu', 'zuowen/mingyanjingju',
           'zuowen/haocihaoju', 'zuowen/youmeiduanluo', 'zuowen/shishilunju', 'zuowen/chengyugushi', 'zuowen/zheligushi', 'zuowen/mingrengushi',
           'zuowen/lishidiangu', 'zuowen/zhongkaosucai', 'zuowen/gaokaosucai', 'zuowen/xieren', 'zuowen/xushi', 'zuowen/xiejing', 'zuowen/zhuangwu',
           'zuowen/yilunwen', 'zuowen/guanhougan', 'zuowen/shuomingwen', 'zuowen/youji', 'zuowen/shuxin', 'zuowen/shenqingshu',
           'zuowen/tonghua', 'zuowen/geci', 'zuowen/kuoxie', 'zuowen/gaixie', 'zuowen/xuxie', 'zuowen/kaocha', 'zuowen/shuqing',
           'zuowen/kantu', 'zuowen/xiangxiang', 'zuowen/manhua', 'zuowen/cailiao', 'zuowen/huodong', 'zuowen/yuyan',
           'zuowen/dushubiji', 'zuowen/heci', 'zuowen/jianyishu', 'zuowen/qingjiatiao', 'zuowen/qishixiezuo', 'zuowen/jiyu', 'meiwensuibi',
           'zhaichao', 'shi', 'ci', 'qu', 'wenyanwen', 'guji',
           'pingyu', 'zhufuyu', 'taici', 'rutuan', 'yanjianggao', 'zhuchici', 'shixibaogao',
           'shuzhibaogao', 'zongjie', 'hetongfanben', 'gongzuojihua', 'cizhibaogao', 'cehuashu',
           'ziwojianding', 'kouhao', 'xindetihui', 'yaoqinghan', 'jiantaoshu', 'liyichangshi', 'jieshaoxin',
           'duhougan', 'daoyouci', 'lunwen', 'wenmi', 'zhouji', 'tongzhi',
           'kaixuediyike', 'jianli', 'shehuishijian', 'qiuzhixin', 'xieyishu', 'weituoshu', 'zhengming',
           'jiaoxuejihua', 'chengnuoshu', 'zichabaogao', 'ziwojieshao', 'guangbogao', 'xuexijihua',
           'jiayougao', 'fayangao', 'yingjiyuan', 'ziwopingjia', 'kaichangbai', 'shiyongwen/ganyan', 'biaoyu',
           'guanggaoci', 'huanyingci', 'biyeganyan', 'wenan', 'biyezengyan', 'biyeliuyan',
           'qianziwen', 'wenhouyu', 'banjiangci', 'baozhengshu', 'jingxuangao', 'xinwengao', 'huodongzongjie',
           'shangyejihuashu', 'chuangyejihuashu', 'zijianxin', 'shiti', 'jiaoan', 'kejian','sc'
           ]
    for s in str:
        url = groupUrl(s)
        getUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'E:\瑞文网'
    main(thmlpath)
