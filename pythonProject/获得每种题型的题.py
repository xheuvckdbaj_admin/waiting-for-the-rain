import random
import win32com.client as win32
from docx import Document

from FindFile import find_name, find_file
from 四川标题 import getFileName


def getTitlePath(path,fileName):
    doc1 = Document()
    doc1.add_heading(fileName, 0)
    doc1.add_heading('考试时间：60分钟 满分：100分', 4)
    doc1.add_heading('姓名：          班级：            考号：       ', 3)
    filePath = path + "\\" + 'all.docx'
    doc1.save(filePath)
    return filePath


def getFiles(path):
    word = win32.gencache.EnsureDispatch('Word.Application')
    # 非可视化运行
    word.Visible = False

    output = word.Documents.Add()  # 新建合并后空白文档

    # 拼接文档

    file_list = []
    path1 = r'E:\试题专项\小学\语文\三年级\1拼音'
    path2 = r'E:\试题专项\小学\语文\三年级\2拼写词语'
    path3 = r'E:\试题专项\小学\语文\三年级\3字'
    path4 = r'E:\试题专项\小学\语文\三年级\4词语'
    path5 = r'E:\试题专项\小学\语文\三年级\5课文填空'
    path6 = r'E:\试题专项\小学\语文\三年级\6关联词填空、词语填空'
    path7 = r'E:\试题专项\小学\语文\三年级\7字词解释'
    path8 = r'E:\试题专项\小学\语文\三年级\8字词语运用'
    path9 = r'E:\试题专项\小学\语文\三年级\9节日，习俗'
    path10 = r'E:\试题专项\小学\语文\三年级\10课外知识'
    path11 = r'E:\试题专项\小学\语文\三年级\11情景对话'
    path12 = r'E:\试题专项\小学\语文\三年级\12诗词，文言文修改病句、把字句'
    path13 = r'E:\试题专项\小学\语文\三年级\13句子'
    path14 = r'E:\试题专项\小学\语文\三年级\14排序'
    path15 = r'E:\试题专项\小学\语文\三年级\15量词、叠词'
    path16 = r'E:\试题专项\小学\语文\三年级\16课内阅读'
    path17 = r'E:\试题专项\小学\语文\三年级\17看图回答问题、课外阅读'
    path18 = r'E:\试题专项\小学\语文\三年级\18看图写作'
    fileName = getFileName()
    #file_list.append(getTitlePath(path,fileName))
    files1 = find_file(path1, [])
    files2 = find_file(path2, [])
    files3 = find_file(path3, [])
    files4 = find_file(path4, [])
    files5 = find_file(path5, [])
    files6 = find_file(path6, [])
    files7 = find_file(path7, [])
    files8 = find_file(path8, [])
    files9 = find_file(path9, [])
    files10 = find_file(path10, [])
    files11 = find_file(path11, [])
    files12 = find_file(path12, [])
    files13 = find_file(path13, [])
    files14 = find_file(path14, [])
    files15 = find_file(path15, [])
    files16 = find_file(path16, [])
    files17 = find_file(path17, [])
    files18 = find_file(path18, [])

    output.Application.Selection.Range.InsertFile(files18[random.randint(0, len(files18) - 1)])

    output.Application.Selection.Range.InsertFile(files17[random.randint(0, len(files17) - 1)])
    #file_list.append(files17[random.randint(0, len(files17) - 1)])

    output.Application.Selection.Range.InsertFile(files16[random.randint(0, len(files16) - 1)])
    #file_list.append(files16[random.randint(0, len(files16) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files15[random.randint(0, len(files15) - 1)])
    #file_list.append(files15[random.randint(0, len(files15) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files14[random.randint(0, len(files14) - 1)])
    #file_list.append(files14[random.randint(0, len(files14) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files13[random.randint(0, len(files13) - 1)])
    #file_list.append(files13[random.randint(0, len(files13) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files12[random.randint(0, len(files12) - 1)])
    #file_list.append(files12[random.randint(0, len(files12) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files11[random.randint(0, len(files11) - 1)])
    #file_list.append(files11[random.randint(0, len(files11) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files10[random.randint(0, len(files10) - 1)])
    #file_list.append(files10[random.randint(0, len(files10) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files9[random.randint(0, len(files9) - 1)])
    #file_list.append(files9[random.randint(0, len(files9) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files8[random.randint(0, len(files8) - 1)])
    #file_list.append(files8[random.randint(0, len(files8) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files7[random.randint(0, len(files7) - 1)])
    #file_list.append(files7[random.randint(0, len(files7) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files6[random.randint(0, len(files6) - 1)])
    #file_list.append(files6[random.randint(0, len(files6) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files5[random.randint(0, len(files5) - 1)])
    #file_list.append(files5[random.randint(0, len(files5) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files4[random.randint(0, len(files4) - 1)])
    #file_list.append(files4[random.randint(0, len(files4) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files3[random.randint(0, len(files3) - 1)])
    #file_list.append(files3[random.randint(0, len(files3) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files2[random.randint(0, len(files2) - 1)])
    #file_list.append(files2[random.randint(0, len(files2) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files1[random.randint(0, len(files1) - 1)])
    #file_list.append(files1[random.randint(0, len(files1) - 1)])
    #print(files1[random.randint(0, len(files1) - 1)])

    output.Application.Selection.Range.InsertFile(getTitlePath(path,fileName))
    # 获取合并后文档的内容
    doc = output.Range(output.Content.Start, output.Content.End)
    # doc.Font.Name = "黑体"  # 设置字体

    output.SaveAs(r'E:\试题专项\小学\卷子' + '\\' + fileName + '.docx')  # 保存
    output.Close()
    return file_list
    # print(files)
    # print(path1+'\\'+str(i))
    # print(files[random.randint(0,len(files)-1)])


def main():
    path = r'E:\试题专项\小学\卷子'
    # getTitlePath(path)
    for i in range(1,10500):
        file_list = getFiles(path)


if __name__ == '__main__':
    main()