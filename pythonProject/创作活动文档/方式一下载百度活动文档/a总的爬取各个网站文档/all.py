import os

from pythonProject.创作活动文档.方式一下载百度活动文档.三六零问答.all360wenda import main360WenDa
from pythonProject.创作活动文档.方式一下载百度活动文档.头条搜索.alljinRiTouTiao import mainTouTiaoSouSuo
from pythonProject.创作活动文档.方式一下载百度活动文档.搜狗问问.allsougouwenwen import mainSouGouWenWen
from pythonProject.创作活动文档.方式一下载百度活动文档.百度教育.下载百度教育文档 import mainBaiDuJiaoYu
from pythonProject.创作活动文档.方式一下载百度活动文档.百度文库.总获取百度文档word import mainBaiDuWenDang
from pythonProject.创作活动文档.方式一下载百度活动文档.百度问答.allUpload import mainBaiDuWenDa
from pythonProject.创作活动文档.爬取活动文档标题.获取百度定产文档的标题 import mainTitle


def makeFolder(filePath):
    if  not os.path.exists(filePath):
        os.makedirs(filePath)
def main():
    # 百度账号的cookies存放的文件夹，每个账号的cookies不同，每次运行时，都要把账号cookies复制到该文件夹
    cookiesPath = './baiduCookies1.txt'
    # 获取活动文档的标题存放的文件夹
    folderPath = r'D:\文档\百度活动文档\百度活动文档标题'
    # 爬取文档后，文档存放的文件夹
    filePath = r'D:\文档\百度活动文档\百度html'
    makeFolder(filePath+'\\'+'百度文档')
    makeFolder(filePath+'\\'+'百度问答')
    makeFolder(filePath+'\\'+'百度教育 ')
    makeFolder(filePath+'\\'+'搜狗问问')
    makeFolder(filePath+'\\'+'头条搜索')
    makeFolder(filePath+'\\'+'三六零问答')
    # 爬取百度活动文档的标题
    mainTitle(folderPath, cookiesPath)
    # #爬取头条搜索
    mainTouTiaoSouSuo(folderPath, filePath + '\\' + '头条搜索')
    # # 爬取百度问答
    mainBaiDuWenDa(folderPath, filePath + '\\' + '百度问答')
    # #爬取搜狗问问
    mainSouGouWenWen(folderPath, filePath+'\\'+'搜狗问问')
    # 爬取360问答的文档，并保存
    main360WenDa(folderPath,filePath+'\\'+'三六零问答')
    # 爬取百度文档
    mainBaiDuWenDang(cookiesPath, folderPath, filePath + '\\' + '百度文档')

if __name__ == '__main__':
    main()