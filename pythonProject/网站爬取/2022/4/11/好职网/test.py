import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        contentText = str(soup.select('div.content')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>').replace('（www.simayi.net）','')
        # contentText=re.sub('<span>(.*?)</span>','',content)
        title = str(soup.select('div.article > h1.title')[0].text).split('\n')[0]
        print(title)
        title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(contentText)
        f.close()
    except:
        print('error')
if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    url='https://www.haojob123.com/zhichang/gongzuojihua/%d.html'
    for i in range(200812,201112):
        print('-------',i)
        strUrl=url%i
        getHtml(thmlpath, strUrl)