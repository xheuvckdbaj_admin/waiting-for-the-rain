# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        contentText = str(soup.select('div.content')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>').replace('（www.simayi.net）','')
        # contentText=re.sub('<span>(.*?)</span>','',content)
        title = str(soup.select('div.article > h1.title')[0].text).split('\n')[0]
        print(title)
        title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(contentText)
        f.close()
    except:
        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            contentText = str(soup.select('div.content')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>').replace(
                '（www.simayi.net）', '')
            # contentText=re.sub('<span>(.*?)</span>','',content)
            title = str(soup.select('div.article > h1.title')[0].text).split('\n')[0]
            print(title)
            title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(contentText)
            f.close()
        except:
            print('error')


def getListUrl(thmlpath, strUrl,browser):
    browser.get(strUrl)
    a_list=browser.find_elements_by_xpath('//div[@class="list_wrap"]/ul[@class="new"]/li[@class="list_itme"]/a[@class="itme_bt"]')
    for i,a in enumerate(a_list):
        href=a.get_attribute('href')
        print(href)
        # getHtml(thmlpath, href)



def main(txtPath,thmlpath):
        file=open(txtPath,'r',encoding='utf-8')
        file_lines=file.readlines()
        for i,line in enumerate(file_lines):
            print('-----------',i)
            url=line.replace('\n','')
            getHtml(thmlpath, url)
if __name__ == '__main__':
    txtPath=r'./url.txt'
    thmlpath = r'E:\文档\自查报告网'
    main(txtPath,thmlpath)
