import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_qiqifanwenwang(url, file, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.ar_title > h1')[0].text
        print(title)
        file.write(title+'\n')
    except:
        print('error')

if __name__ == '__main__':
    # url = 'http://www.n77.org/jiaoxueziyuan/zhutibanhui/ganen/151832.html'
    # htmlPath = r'D:\文档\百度活动文档\百度html'
    # keyStr = 'kaixin'
    # title = ''
    # mian_qiqifanwenwang(url, htmlPath, keyStr, title)

    url = 'http://www.n77.org/%s/ganen/%s.html'
    htmlPath = r'D:\文档\百度付费上传文档\需要的标题及其chatgpt\title\七七范文网.txt'
    keyStr = 'kaixin'
    title = ''
    file = open(htmlPath, 'w', encoding='utf-8')
    keys = ['jiaoxueziyuan/zhutibanhui', 'jiaoxueziyuan/jiaoan',
            'jiaoxueziyuan/jiaoxuefansi', 'jiaoxueziyuan/shuokegao',
            'jiaoxueziyuan/jiaoxueshilu', 'jiaoxueziyuan/jiaoshipingyu',
            'jiaoxueziyuan/suibi', 'jiaoxueziyuan/xushi',
            'jiaoxueziyuan/sheji', 'jiaoxueziyuan/pingyu',
            'jiaoxueziyuan/zhicheng', 'gongzuofanwen/gongzuozongjie',
            'gongzuofanwen/gongzuobaogao', 'gongzuofanwen/wenmiziliao',
            'gongzuofanwen/wenancehua', 'gongzuofanwen/huodongfangan',
            'gongzuofanwen/gongzuojihua', 'shiyongwendang/shenqingshu',
            'shiyongwendang/yanjianggao', 'shiyongwendang/zhuchigao',
            'shiyongwendang/geleigaojian', 'shiyongwendang/jianghuazhici',
            'shiyongwendang/tiaojushuxin', 'shiyongwendang/hetongmoban',
            'shiyongwendang/ziwojianding', 'shiyongwendang/ziwopingjia',
            'shiyongwendang/daoyouci', 'shiyongwendang/xindetihui']
    for key in keys:
        for i in range(1083, 251832):
            newUrl = url % (key, str(i))
            # print(newUrl)
            mian_qiqifanwenwang(newUrl, file, keyStr, title)
    file.close()
