# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import time
import urllib
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.百度搜索.网站选择 import websiteJude
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options=webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser=webdriver.Chrome(options=options)
    time.sleep(1)
    return browser
def panduanTitle(title):
    panduan = True
    if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title:
        panduan = False
    return panduan

def addCookies(browser, url, filePath):
        browser.get(url)
        time.sleep(1)
        cookies = readTxt.readTxt(filePath)
        for item in cookies.split(';'):
            cookie = {}
            itemname = item.split('=')[0]
            iremvalue = item.split('=')[1]
            cookie['domain'] = '.baidu.com'
            cookie['name'] = itemname.strip()
            cookie['value'] = urllib.parse.unquote(iremvalue).strip()
            browser.add_cookie(cookie)
        browser.get(url)

def getNewWindow(browser,xpathStr,timeNumber):
    try:
        n=browser.window_handles
        browser.switch_to.window(n[-1])
        time.sleep(1)
        element=WebDriverWait(browser,timeNumber).until(
            EC.presence_of_element_located(
                (By.XPATH,xpathStr)
            )
        )
    except:
        print('error')
def grapcontent(folderPath,driver,keyStr,urls):
    try:
        element = driver.find_element(By.XPATH, '//div[@class="title-row_2ymNF"]/a')
        websiteJude(element, folderPath, driver, keyStr, urls)
        # driver.close()
    except:
        elements = driver.find_elements(By.XPATH, '//h3[@class="c-title t t tts-title"]/a')
        for i, element in enumerate(elements):
            try:
                title = element.text
                if mainPanDuanTitle(keyStr, title):
                    print(keyStr + '++++++++' + title)
                    # baidu=driver.find_element(By.XPATH,'//span[@class="c-color-gray"]')
                    panduan = websiteJude(element, folderPath, driver, keyStr, urls)
                    if panduan:
                        break
            except:
                driver.close()
                break
                print('网站选择出错')
        # driver.close()
def getTitle(folderPath,browser,file,urls):
    xpathStr='//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    try:
        getNewWindow(browser, xpathStr, 10)
    except:
        print('error')
    elements=browser.find_elements(By.XPATH,xpathStr)
    for element in elements:
        try:
            title=element.get_attribute('title')
            if panduanTitle(title):
                print(title)
                file.write(title+'\n')
                try:
                    element.click()
                except:
                    time.sleep(0.5)
                    element.click()
                getNewWindow(browser, '//input', 15)
                grapcontent(folderPath,browser, title, urls)
                browser.close()
                getNewWindow(browser, xpathStr, 15)
        except:
            print('爬取出错')
            continue

def ClickNextPage(browser,pageNumber):
    try:
        #pageNumber最大为1，最小为0
        xpathStr='//ul[@class="el-pager"]/li[@class="number"]'
        browser.find_elements(By.XPATH,xpathStr)[pageNumber].click()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(2)
    except:
        print('error')
def clickNextTitle(browser,pageNumber):
    # pageNumber最小为0
    if pageNumber==9:
        xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
        browser.find_element(By.XPATH,xpathStrs).click()
        getNewWindow(browser, xpathStrs, 10)
        time.sleep(2)
    xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
    browser.find_elements(By.XPATH,xpathStr)[pageNumber].click()
    getNewWindow(browser, xpathStr, 10)
    time.sleep(2)
def mainTitleClassify(titlePath,folderPath,cookiesPath,urls):
   browser=getDriver('title1')
   url='https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask'
   addCookies(browser, url, cookiesPath)
   time.sleep(1)
   # getTitle(browser)
   # browser=getBrowserNow1()
   filePath = titlePath + '\\' + str(1) + '.txt'
   file=open(filePath, 'w', encoding='utf-8')
   getTitle(folderPath,browser,file,urls)
   for pageNumber in range(0,6):
     if pageNumber ==3:
        for i in range(0,3):
            ClickNextPage(browser, pageNumber)
            getTitle(folderPath,browser,file,urls)
     ClickNextPage(browser, pageNumber)
     getTitle(folderPath,browser,file,urls)
   file.close()
   for titleNumber in range(0,12):
     filePath = titlePath + '\\' + str(titleNumber+2) + '.txt'
     file = open(filePath, 'w', encoding='utf-8')
     clickNextTitle(browser, titleNumber)
     getTitle(folderPath,browser,file,urls)
     for pageNumber in range(0, 6):
         if pageNumber == 3:
             for i in range(0, 3):
                 ClickNextPage(browser, pageNumber)
                 getTitle(folderPath,browser,file,urls)
         ClickNextPage(browser, pageNumber)
         getTitle(folderPath,browser,file,urls)
     file.close()

   time.sleep(100)

def getHuodongDocx(titlePath,folderPath, cookiesPath,wangzhiPath):
    urls=[]
    file=open(wangzhiPath,'w',encoding='utf-8')
    mainTitleClassify(titlePath,folderPath, cookiesPath,urls)
    for i, url in enumerate(urls):
        file.write(url+'\n')
if __name__ == '__main__':
    titlePath=r'D:\文档\百度活动文档\百度活动文档标题'
    folderPath = r'D:\文档\百度活动文档\百度html'
    cookiesPath = './baiduCookies1.txt'
    wangzhiPath='./wangzhi.txt'
    getHuodongDocx(titlePath,folderPath, cookiesPath,wangzhiPath)