
#中文字符gbk转utf-8
# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os

from datashape import unicode

from pythonProject.FindFile import find_name


def gbk2utf8(raw):

        rs=raw.encode('raw_unicode_escape') #转为机器识别字符串
        s=repr(rs)
        ss=unicode(eval(s),"gbk")
        #gbk解码为unicode
        utf8_str=raw.encode('utf-8').decode(encoding = 'utf-8')   #unicode编码为utf-8
        return utf8_str

def makeHTML(path,string):
    with open(path,'w',encoding='utf-8') as df:
        df.write(gbk2utf8(string))
def zhuanhuan(oldfolder,newfolder):
    filename_list=find_name(oldfolder)
    for name in filename_list:
        #oldPath = r'E:\成\初中生班主任评语.html'
        newpath = newfolder +'\\'+ name
        print(oldfolder+'\\'+name)
        if not '~$' in name and name.endswith('html'):
            try:
                ss=''
                with open(oldfolder+'\\'+name, 'r',encoding='gbk') as df:
                    ss=df.read().replace('\n','').replace('<br>','<p/><P>').replace('<br/>','<p/><P>').replace(' ','')
                with open(newpath, 'w', encoding='utf-8') as df:
                    df.write(gbk2utf8(ss))
                os.remove(oldfolder+'\\'+name)
            except:
                continue
def main():
    oldfolder=r'E:\文档\豆丁文档\百度文档html2'
    newfolder=r'E:\文档\豆丁文档\百度文档html3'
    zhuanhuan(oldfolder,newfolder)
if __name__ == '__main__':
    main()