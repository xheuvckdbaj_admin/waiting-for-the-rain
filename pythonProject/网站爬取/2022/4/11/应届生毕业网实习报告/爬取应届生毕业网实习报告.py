# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        contentText = str(soup.select('div.content')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>').replace('（www.simayi.net）','')
        # contentText=re.sub('<span>(.*?)</span>','',content)
        title = str(soup.select('div.article > h1.title')[0].text).split('\n')[0]
        print(title)
        title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(contentText)
        f.close()
    except:
        print('error')


def getListUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.main-left > div.newlist > dt > a')
    for i,a in enumerate(a_list):
        if i==0:
            href=a.get('href')
            number=int(href.split('/')[-1].replace('.html',''))
            strUrl=href.replace(href.split('/')[-1],'')
            hrefUrl=strUrl+'%d.html'
            for i in range(1,number+500):
                newUrl=hrefUrl%i
                print(newUrl)
                getHtml(thmlpath, newUrl)

# def getLittleTitleUrl(thmlpath, strUrl):
#     requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
#     soup = BeautifulSoup(requests_text, 'lxml')
#     a_list = soup.select('div.menu > ul > li > a')
#     for i,a in enumerate(a_list):
#         if i > 0:
#             href=a.get('href')
#             getListUrl(thmlpath, href)

# def getTitleUrl2(thmlpath, strUrl):
#     requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
#     soup = BeautifulSoup(requests_text, 'lxml')
#     a_list = soup.select('div.header_box > div.nav > a')
#     for i,a in enumerate(a_list):
#             href = a.get('href')
#             getListUrl(thmlpath, href)

def getTitleUrl(txtPath,thmlpath, strUrl):
    file=open(txtPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for line in file_lines:
        url=line.replace('\n','')
        getListUrl(thmlpath, url)

# def getLianjie(url,txtPath):
#     file=open(txtPath,'w',encoding='utf-8')
#     requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
#     soup = BeautifulSoup(requests_text, 'lxml')
#     a_list = soup.select('div.chNews > h3 > span > a')
#     for i,a in enumerate(a_list):
#         href=a.get('href')
#         file.write(href+'\n')
#     file.close()
def main(thmlpath):
            txtPath=r'./lianjie.txt'
            newUrl='https://www.ktw.cn/'
            getTitleUrl(txtPath,thmlpath, newUrl)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
