# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        time.sleep(10)
        #url='http://m.pincai.com/article/2300000.htm'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('#ArtContent')[0])
        print(title)
        #print(list_soupa)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def getUrl2(thmlpath,url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    onclick=soup.select('#ArtContent > input')[0].get('onclick')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.get('title')
            newUrl='https:'+href
            print(href)
            getContent(thmlpath,href,title)
# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#AListBox > ul > li > a')
    time.sleep(100)
    for a in a_list:
            time.sleep(100)
            href=a.get('href')
            title=a.get('title')
            newUrl='https:'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    time.sleep(100)
    getUrl(thmlpath, strUrl)
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('#CutPage > a')
    a_str=a_list[len(a_list)-1].get('href')
    newUrl=strUrl+'index_%d.thml'
    print(a_str)
    number=int(re.match('(.*?)index_(.*?).html',a_str).group(2))
    for i in range(2,number+1):
        href=newUrl%i
        getUrl(thmlpath, href)


def getHtml2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('#AListBox > ul > li > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        newUrl='https:'+href
        getHtml3(thmlpath, newUrl)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.IndexDl > dd > a')
    for a in a_list:
        time.sleep(100)
        href = a.get('href')
        newUrl='https:'+href
        getHtml3(thmlpath, newUrl)

def main(thmlpath):
            #number=1
            strUrl='https://www.diyifanwen.com/fanwen/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档\大文斗范文网'
    main(thmlpath)
