import re

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_zhixiaozhaoshengwang(url,htmlPath,keyStr,title):
    try:
        driver=getDriver('zhixiao')
        driver.get(url)
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="title"]/h1'))
        )
        title=driver.find_element(By.XPATH,'//div[@class="title"]/h1').text
        title = re.sub('（(.*)）', '', title)
        title = re.sub('\d+篇', '', title)
        elements=driver.find_elements(By.XPATH,'//iframe')
        list_soupa=driver.find_element(By.XPATH, '//div[@class="article_content"]').get_attribute('innerHTML').replace(title,keyStr).split('免责声明')[0]
        content = ''
        for element in elements:
            content=element.get_attribute('innerHTML')
            list_soupa=list_soupa.replace(content,'')
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('职校招生网下载成功----', keyStr)
    except:
        print('error')
if __name__ == '__main__':
    url = 'http://5isxw.com/show-33933.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_zhixiaozhaoshengwang(url,htmlPath,keyStr,title)