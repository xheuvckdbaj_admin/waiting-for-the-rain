import os
import shutil

from pythonProject.FindFile import find_file


def ctrlx(oldFilePath,newPath):
    #if oldFilePath.endswith('html'):
        #移动文件夹示例
        shutil.move(oldFilePath,newPath)
def main():
    oldPath = r'G:\中考（2）'
    newPathDoc = r'G:\文档\oc'
    newPathPdfDocx = r'G:\文档\ocx1'
    newPathPdf=r'G:\文档\df'
    newPathPpt = r'G:\文档\ppt'
    newPathZip = r'G:\文档\zip'


    for oldFilePath in os.listdir(oldPath):
        #print(oldFilePath)
        try:
            if oldFilePath.endswith('.doc') or oldFilePath.endswith('.DOC'):
                print(oldFilePath + '需要')
                ctrlx(oldPath+'\\'+oldFilePath, newPathDoc)
            elif oldFilePath.endswith('.docx') or oldFilePath.endswith('.DOCX'):
                print(oldFilePath + '需要')
                ctrlx(oldPath + '\\' + oldFilePath, newPathPdfDocx)
            elif oldFilePath.endswith('.pdf') or oldFilePath.endswith('.PDF'):
                print(oldFilePath + '需要')
                ctrlx(oldPath + '\\' + oldFilePath, newPathPdf)
            elif oldFilePath.endswith('.pptx') or oldFilePath.endswith('.PPTX') or oldFilePath.endswith('.pps') \
                    or oldFilePath.endswith('.ppt') or oldFilePath.endswith('.PPT'):
                print(oldFilePath + '需要')
                ctrlx(oldPath + '\\' + oldFilePath, newPathPpt)
            elif oldFilePath.endswith('.html') or oldFilePath.endswith('.wps')  or oldFilePath.endswith('.txt'):
                print(oldFilePath + '需要')
                ctrlx(oldPath + '\\' + oldFilePath, newPathDoc)
            elif oldFilePath.endswith('.zip') or oldFilePath.endswith('.rar')  or oldFilePath.endswith('.gz') \
                    or oldFilePath.endswith('.cab'):
                print(oldFilePath + '需要')
                ctrlx(oldPath + '\\' + oldFilePath, newPathZip)

            elif os.path.isfile(oldPath+'\\'+oldFilePath) and (not oldFilePath.endswith('.docx') and not oldFilePath.endswith('.DOCX') and not oldFilePath.endswith('.doc') \
            and not oldFilePath.endswith('.DOC') and not oldFilePath.endswith('.PPT') and not oldFilePath.endswith('.pptx') \
            and not oldFilePath.endswith('.PPTX') and not oldFilePath.endswith('.pdf') and not oldFilePath.endswith('.PDF') \
            and not oldFilePath.endswith('.ppt') and not oldFilePath.endswith('.html') and not oldFilePath.endswith('.HTML') \
            and not oldFilePath.endswith('.wps') and not oldFilePath.endswith('.rar') and not oldFilePath.endswith('.RAR') \
            and not oldFilePath.endswith('.zip') and not oldFilePath.endswith('.ZIP') \
            and not oldFilePath.endswith('.tar') and not oldFilePath.endswith('.gz') \
            and not oldFilePath.endswith('.7Z') and not oldFilePath.endswith('.cab') and not oldFilePath.endswith('.txt')):
                    print(oldFilePath+'删除')
                    os.remove(oldPath+'\\'+oldFilePath)
        except:
            print('错误')
            continue

if __name__ == '__main__':
    main()