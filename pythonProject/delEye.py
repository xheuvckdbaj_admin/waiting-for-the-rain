# -*- coding:utf-8 -*-
from os.path import splitext

import win32com
from docx import Document
from numba import jit

from FindFile import find_file, find_name
from blank import blank
from wordFormat import wordFormat

document = Document()
import warnings

warnings.filterwarnings("ignore")
import os

'''
删除眉脚眉页
'''

def delEye(fileName,folder,newPath):
    msword = win32com.client.Dispatch('Word.Application')
    if not '$' in fileName :
        fn=folder+'\\'+fileName
        fn_new=''.join(splitext(fn))
        print(fn)
        if fileName.split('.')[len(fileName.split('.'))-1]=='docx':
            try:
                word=Document(fn)
                word.settings.odd_and_even_pages_header_footer = False
                for section in word.sections:
                    section.different_first_page_header_footer = False
                    section.header.is_linked_to_previous = True
                    section.footer.is_linked_to_previous = True
                    #os.remove('F:\\文件上传\\docx\\(新人教版)磁现象磁场.docx')
                #blank(word.paragraphs)
                #word.Sections(1).Footers(1).PageNumbers.Add()
                word.save(newPath+'\\'+fileName)
                doc = msword.Documents.Open(newPath+'\\'+fileName)
                doc.Sections(1).Footers(1).PageNumbers.Add()
                if doc.SaveAs(newPath+'\\'+fileName)==None :
                    os.remove(fn)
                doc.Close()
            except:
                print('错误')
            #startfile(fn_new)

def main():
    folder=r'E:\没处理的docx文件'
    newFolder=r'E:\除去眉页后的文件'

    fileNames=find_name(folder)
    for fileName in fileNames:
        #wordFormat(folder,fileName)
        #wordFormat(folder+'\\'+fileName)
       #os.remove(r'C:\Users\Administrator\Desktop\文档上传\新建文件夹 (2)\1997-2015上海中考数学试卷.docx')
        delEye(fileName,folder,newFolder)
    #new_method = getattr(obj, self._new_method_name)
    #child = new_method()
    #setattr(child, key, value)


if __name__ == '__main__':
    main()