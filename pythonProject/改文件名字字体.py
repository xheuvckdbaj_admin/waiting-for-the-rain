import os

import docx
from docx import Document
from docx.shared import Pt

from FindFile import find_file
def mifyFile(filefolder):
    a = 0
    dir = os.listdir(filefolder)
    for fileName in dir:
        filePath = os.path.join(filefolder, fileName)
        if os.path.isdir(filePath):
            continue
        else:
          if not '~$' in filePath:
            a+=1
            name=str(a)+'.docx'
            doc1 = Document()
            file=docx.Document(filePath)
            i=0
            for para in file.paragraphs:
              i+=1
              paragraph = doc1.add_paragraph().add_run(para.text)
              if i==1:
                paragraph.bold=True

              #paragraph.font.name='微软雅黑'
              #paragraph.font.size=Pt(12)
            # 新增文档标题
            #doc1.add_heading('如何使用 Python 创建和操作 Word', 0)
            path=filefolder+'\\'+name
            doc1.save(path)
            os.remove(filePath)
def main():
    filefolder = r'E:\试题专项\小学\语文\二年级\18看图回答问题'

    mifyFile(filefolder)
if __name__ == '__main__':
    main()