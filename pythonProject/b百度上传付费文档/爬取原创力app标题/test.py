import re

import requests

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def main(titlePath):
    url1='https://api.book118.com/applet/index/getHotDocs?app=APP&version=3.2.8&page=%d&pagesize=20'
    url2='https://api.book118.com/applet/index/getSoleDocs?app=APP&version=3.2.8&page=%d&pagesize=20'
    url3='https://api.book118.com/applet/index/getOriginalDocs?app=APP&version=3.2.8&page=%d&pagesize=20'
    # url='https://api.book118.com/applet/index/getNewDocs?app=APP&version=3.2.8&page=%d&pagesize=20'
    urls=[url1,url2,url3]
    for number,url in enumerate(urls):
        if number==0:
            newtitlePath=titlePath+'\\'+'Hot'
        elif number==1:
            newtitlePath = titlePath + '\\' + 'Sole'
        elif number==2:
            newtitlePath = titlePath + '\\' + 'Original'
        for i in range(0,10000):
            newPath=newtitlePath+str(i)+'.txt'
            file=open(newPath,'w',encoding='utf-8')
            newUrl=url%i
            content=str(requests.get(url=newUrl, headers=headers).content.decode(encoding='utf-8'))
            print(content)
            titles=re.findall(r"\"title\":\"(.*?)\"",content)
            print(titles)
            for title in titles:
                titleStr=title.replace('.docx','').replace('.doc','')\
                    .replace('.pptx','').replace('.ppt','').replace('PPTX','').replace('.PPT','')\
                    .replace('.pdf','').replace('.xls','')
                file.write(titleStr+'\n')
                print(titleStr)
            file.close()
if __name__ == '__main__':
    titlePath=r'D:\文档\百度付费上传文档\百度关键词标题\原创力精选标题'
    main(titlePath)