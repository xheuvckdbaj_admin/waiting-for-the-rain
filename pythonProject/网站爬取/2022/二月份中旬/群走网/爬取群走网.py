# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('#content')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('#title > strong')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.data-sitemapli > ul > li > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.fwwang.cn'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('error')
            continue
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    pageContent = soup.select('div.xy.Baidu_paging_indicator > span')[0].text
    allPage=int(re.findall('共(.*?)页/(.*?)条',pageContent)[0][0])
    for i in range(1,allPage+1):
        newUrl=strUrl+str(i)+'/'
        getListUrl(thmlpath, newUrl)
# def getTitleUrl2(thmlpath,strUrl):
#     requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
#     soup = BeautifulSoup(requests_text, 'lxml')
#     a_list = soup.select('div.ct > div > a')
#     for a in a_list:
#         href = a.get('href')
#         newUrl = 'http://www.papers8.cn' + href
#         try:
#             getPageUrl(thmlpath, newUrl)
#         except:
#             getListUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.linkbox > ul > li > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.fwwang.cn'+href
        try:
                getPageUrl(thmlpath, newUrl)
        except:
                getListUrl(thmlpath, newUrl)
def getMoreListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.list > ul > li > h6 > a')
    for a in a_list:
        href=a.get('href')
        newUrl='https://www.qunzou.com'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('gbk出错')
            continue
def getMorePageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href= soup.select('div.showpage > ul > a')[-1].get('href')
    allPage=int(href.split('_')[-1].replace('.html',''))
    url=href.replace(href.split('_')[-1],'')
    for i in range(1,allPage+1):
        newUrl=url+str(i)+'.html'
        getMoreListUrl(thmlpath, newUrl)

def main(thmlpath):
            #打开文本图片集
                strUrls=['https://www.qunzou.com/xuexi/','https://www.qunzou.com/haoci/','https://www.qunzou.com/haoju/',
                         'https://www.qunzou.com/zuowen/','https://www.qunzou.com/duhougan/','https://www.qunzou.com/guanhougan/','https://www.qunzou.com/fanwen/','https://www.qunzou.com/yuedu/',
                         'https://www.qunzou.com/gongzuo/','https://www.qunzou.com/jihua/','https://www.qunzou.com/jiaoxue/','https://www.qunzou.com/yanjianggao/','https://www.qunzou.com/zhuchici/',
                         'https://www.qunzou.com/kouhao/','https://www.qunzou.com/biaoyu/','https://www.qunzou.com/shuzhibaogao/','https://www.qunzou.com/ziliao/']
                for url in strUrls:
                    getMorePageUrl(thmlpath,url)

if __name__ == '__main__':

    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
