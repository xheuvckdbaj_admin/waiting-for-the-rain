import time

from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.allyunxing import main_arrangeDocx
from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.合并txt内容 import merge
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.获取活动文档标题 import mainTitleClassify
from pythonProject.创作活动文档.方式一下载百度活动文档.三六零搜索.爬取360搜索 import mianSanLiuLing
from pythonProject.创作活动文档.方式一下载百度活动文档.中国搜索.allchina import mian_zhonguosousuo
from pythonProject.创作活动文档.方式一下载百度活动文档.今日头条.allJinRiWebsit import main_jinritoutiao
from pythonProject.创作活动文档.方式一下载百度活动文档.全网爬取.allWebsit import main_baidu, makeFolder
from pythonProject.创作活动文档.方式一下载百度活动文档.夸克搜索.夸克搜索爬取 import main_kuake
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索.allWebsitBiYing import main_biying
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索国际版.allWebsitBiYing import main_biyingGuoji
from pythonProject.创作活动文档.方式一下载百度活动文档.搜狗搜索.allWebsitSougou import main_sougou
from pythonProject.创作活动文档.方式一下载百度活动文档.百度文库.总获取百度文档word import mainBaiDuWenDang
from pythonProject.创作活动文档.方式一下载百度活动文档.百度问答.allUpload import mainBaiDuWenDa


def main():
    # 该账号cookies
    cookiesPath = './baiduCookies1.txt'
    # 标题存放文件夹
    titleFolder1 = r'E:\文档\百度活动文档\百度活动文档标题'
    titleFolder2 = r'E:\文档\百度活动文档2\百度活动文档标题'
    titleFolder3 = r'E:\文档\百度活动文档3\百度活动文档标题'
    titleFolder4 = r'E:\文档\百度活动文档4\百度活动文档标题'
    # 文档存放文件夹
    folderPath1 = r'E:\文档\百度活动文档\百度html'
    folderPath2 = r'E:\文档\百度活动文档2\百度html'
    folderPath3 = r'E:\文档\百度活动文档3\百度html'
    folderPath4 = r'E:\文档\百度活动文档4\百度html'
    # 中介文件夹
    mediumFolderPath1 = r'E:\文档\百度活动文档\中介'
    mediumFolderPath2 = r'E:\文档\百度活动文档2\中介'
    mediumFolderPath3 = r'E:\文档\百度活动文档3\中介'
    mediumFolderPath4 = r'E:\文档\百度活动文档4\中介'
    # 不要设置的文件夹，不需要管他
    midFolderPath = r'E:\文档\自查报告网url'
    # 中介文件夹
    # 修改格式后最终存放文档的文件夹，也就是文档上传文件夹，最终文件夹
    newPath1 = r'E:\文档\百度活动文档\修改后百度活动文档'
    newPath2 = r'E:\文档\百度活动文档2\修改后百度活动文档'
    newPath3 = r'E:\文档\百度活动文档3\修改后百度活动文档'
    newPath4 = r'E:\文档\百度活动文档4\修改后百度活动文档'
   #未爬取的网站存放的文件
    wangzhiPath1 = r'./wangzhi1.txt'
    wangzhiPath2 = r'./wangzhi2.txt'
    wangzhiPath3 = r'./wangzhi3.txt'
    wangzhiPath4 = r'./wangzhi4.txt'
    #标题算法测试——爬取的文档原标题与活动标题对比
    titleTestFolder=r'E:\文档\百度活动文档\标题算法测试360'
    makeFolder(titleTestFolder)
    for i in range(0, 2000):
        startnumber = 0 + (3* i)
        last = startnumber + 3
        fileTest=open(titleTestFolder+'\\'+str(i)+'.txt','w',encoding='utf-8')
        #360搜索账号一
        mianSanLiuLing(fileTest, titleFolder1, folderPath1, wangzhiPath1, startnumber, last)
        main_arrangeDocx(folderPath1, mediumFolderPath1, midFolderPath, newPath1)
        #360搜索账号二
        mianSanLiuLing(fileTest, titleFolder2, folderPath2, wangzhiPath2, startnumber, last)
        main_arrangeDocx(folderPath2, mediumFolderPath2, midFolderPath, newPath2)
        #360搜索账号三
        try:
            mianSanLiuLing(fileTest, titleFolder3, folderPath3, wangzhiPath3, startnumber, last)
            main_arrangeDocx(folderPath3, mediumFolderPath3, midFolderPath, newPath3)
        except:
            print('error')
        #360搜索账号四
        try:
            mianSanLiuLing(fileTest, titleFolder4, folderPath4, wangzhiPath4, startnumber, last)
            main_arrangeDocx(folderPath4, mediumFolderPath4, midFolderPath, newPath4)
        except:
            print('error')
        fileTest.close()
    merge(titleTestFolder, titleTestFolder)


if __name__ == '__main__':
    time.sleep(2000)
    main()
