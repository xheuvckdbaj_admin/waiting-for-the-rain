# -*- coding:utf-8 -*-

import time
import addcookies
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
filePath=r".\cookies.txt"
def getNewWindow(browser,xpathStr):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser

def getFile(path,number,last):
    txtFile = open(path, 'r')
    hrefs = txtFile.readlines()
    browser = webdriver.Chrome()
    for i in range(number,last):
        try:
            print(i)
            browser.get(hrefs[i])
            if i==number:
                addcookies.addCookies(browser, hrefs[i], filePath)
            try:
                browser.find_element_by_class_name('zbyDdwb').click()
            except:
                print('error')
            try:
                browser.find_element_by_css_selector('span.text').click()
            except:
                continue
            browser=getNewWindow(browser, '//span[@class="treeview-txt"]')
            browser.find_elements_by_css_selector('span.treeview-txt')[2].click()
            browser = getNewWindow(browser, '//a[@class="g-button  g-button-blue-large"]')
            browser.find_element_by_css_selector('div.dialog-footer.g-clearfix > a.g-button.g-button-blue-large').click()
            time.sleep(2)
        except:
            print('错误')
            continue
def main():
    path=r'G:\文档\url\all.txt'
    # number初始值为0
    number=0
    last=15000
    getFile(path,number,last)
if __name__ == '__main__':
    main()