# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'https://www.chazidian.com/zujuan/shiti/' + str + '/'
    return url
def getNewUrl(url,str):
    newUrl=url+str
    return newUrl

# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    list_soupa = str(soup.select('.article-main')[0])
    #print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('.item-ct')
    url_list1 = soup.select('.main-ct-left')
    for div in url_list1:
        li_list1=div.select('ul > li')
        for li in li_list1:
            href=li.select('a')[0].get('href')
            title=li.select('a')[0].get('title')
            newUrl=f'https://fanwen.chazidian.com'+href
            getUrlPageNum(thmlpath, newUrl)
    print(url_list)
    for u in url_list:
            li_list=u.select('ul > li')
            for li in li_list:
                title=li.select('a')[0].get('title')
                href=li.select('a')[0].get('href')
                newUrl=f'https://fanwen.chazidian.com'+href
                #print(title)
                #print(newUrl)
                getContent(thmlpath, newUrl, title)
#获取最终的地址
def getUrlIndex(path,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    div_list=soup.select('.listpage-main-left > .listpage-item')
    #print(url_list)
    for div in div_list:
            src=div.select('h4 > a')[0].get('href')
            href=f'https://fanwen.chazidian.com'+src
            print(href)
            title = div.select('h4 > a')[0].get('title')
            getContent(path,href,title)

def getUrlPageNum(path,url):
    for pageNum in range(1,1000):
        try:
                    oldUrl=url+'%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(path, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)
def mkpathdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)
def getcatalog(path,url):
    for catalog in range(224,231):
        st='_'+catalog
        newUrl = getNewUrl(url, st)
        str=['知识点','鲁教版','人教版','湘教版','中图版','中图版上海','大纲版']

def getxiaoxuegrade(path,url):
    for xiaoxue_i in range(1, 7):
        str1 = ['一年级', '二年级', '三年级', '四年级', '五年级', '六年级']
        newPath=''
        if xiaoxue_i == 1:
            yinianji_path = path + '\\' + str1[0]
            mkpathdir(yinianji_path)
            newPath=yinianji_path
        elif xiaoxue_i == 2:
            ernianji_path = path + '\\' + str1[1]
            mkpathdir(ernianji_path)
            newPath=ernianji_path
        elif xiaoxue_i == 3:
            sannianji_path = path + '\\' + str1[2]
            mkpathdir(sannianji_path)
            newPath =sannianji_path
        elif xiaoxue_i == 4:
            sinianji_path = path + '\\' + str1[3]
            mkpathdir(sinianji_path)
            newPath =sinianji_path
        elif xiaoxue_i == 5:
            wunianji_path = path + '\\' + str1[4]
            mkpathdir(wunianji_path)
            newPath =wunianji_path
        elif xiaoxue_i == 6:
            liunianji_path = path + '\\' + str1[5]
            mkpathdir(liunianji_path)
            newPath =liunianji_path
        st = '_' + str(xiaoxue_i)
        newUrl = getNewUrl(url,st)

def getchuzhonggrade(path,url):
    for chuzhong_i in range(7, 10):
        newPath = ''
        str1 = ['一年级', '二年级', '三年级']
        if chuzhong_i == 7:
            yinianji_path = path + '\\' + str1[0]
            mkpathdir(yinianji_path)
            newPath =yinianji_path
        elif chuzhong_i == 8:
            ernianji_path = path + '\\' + str1[1]
            mkpathdir(ernianji_path)
            newPath =ernianji_path
        elif chuzhong_i == 9:
            sannianji_path = path + '\\' + str1[2]
            mkpathdir(sannianji_path)
            newPath =sannianji_path
        st = '_' + str(chuzhong_i)
        newUrl = getNewUrl(url,st)
def getgaozhonggrade(path,url):
    for gaozhong_i in range(10, 13):
        newPath =''
        str1 = ['一年级', '二年级', '三年级']
        if gaozhong_i == 10:
            yinianji_path = path + '\\' + str1[0]
            mkpathdir(yinianji_path)
            newPath =yinianji_path
        elif gaozhong_i == 11:
            ernianji_path = path + '\\' + str1[1]
            mkpathdir(ernianji_path)
            newPath =ernianji_path
        elif gaozhong_i == 12:
            sannianji_path = path + '\\' + str1[2]
            mkpathdir(sannianji_path)
            newPath =sannianji_path

        st = '_' + str(gaozhong_i)
        newUrl = getNewUrl(url,st)
def getgrade(path,url):
    if '小学' in path:
        getxiaoxuegrade(path,url)
    elif '初中' in path:
        getchuzhonggrade(path,url)
    elif '高中' in path:
        getgaozhonggrade(path,url)


def main(thmlpath):

    path=thmlpath
    for i in range(1,22):
        u=str(i)
        url = groupUrl(u)
        str1 = ['语文', '数学', '英语']
        if i<=3:
            chuzhong_path = path + '\\小学'
            mkpathdir(chuzhong_path)
            if i==1:
                chuzhong_yuwen_path = chuzhong_path +'\\'+str1[0]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path,url)
            if i == 2:
                chuzhong_shuxue_path = chuzhong_path + '\\'+str1[1]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path,url)

            if i == 3:
                chuzhong_yingyu_path = chuzhong_path + '\\'+str1[2]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path,url)

        elif i>3 and i<=12:
            str1=['语文', '数学', '英语', '物理', '化学', '生物', '政治', '历史', '地理']
            chuzhong_path = path + '\\初中'
            mkpathdir(chuzhong_path)
            if i == 4:
                chuzhong_yuwen_path = chuzhong_path + '\\' + str1[0]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path, url)
            if i == 5:
                chuzhong_shuxue_path = chuzhong_path + '\\' + str1[1]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path, url)

            if i == 6:
                chuzhong_yingyu_path = chuzhong_path + '\\' + str1[2]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path, url)
            if i == 7:
                chuzhong_yuwen_path = chuzhong_path + '\\' + str1[3]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path, url)
            if i == 8:
                chuzhong_shuxue_path = chuzhong_path + '\\' + str1[4]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path, url)

            if i == 9:
                chuzhong_yingyu_path = chuzhong_path + '\\' + str1[5]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path, url)
            if i == 10:
                chuzhong_yuwen_path = chuzhong_path + '\\' + str1[6]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path, url)
            if i == 11:
                chuzhong_shuxue_path = chuzhong_path + '\\' + str1[7]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path, url)

            if i == 12:
                chuzhong_yingyu_path = chuzhong_path + '\\' + str1[8]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path, url)

        elif i>12 and i<=21:
            str1 = ['语文', '数学', '英语', '物理', '化学', '生物', '政治', '历史', '地理']
            chuzhong_path = path + '\\高中'
            mkpathdir(chuzhong_path)
            if i == 13:
                chuzhong_yuwen_path = chuzhong_path + '\\' + str1[0]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path, url)
            if i == 14:
                chuzhong_shuxue_path = chuzhong_path + '\\' + str1[1]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path, url)

            if i == 15:
                chuzhong_yingyu_path = chuzhong_path + '\\' + str1[2]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path, url)
            if i == 16:
                chuzhong_yuwen_path = chuzhong_path + '\\' + str1[3]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path, url)
            if i == 17:
                chuzhong_shuxue_path = chuzhong_path + '\\' + str1[4]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path, url)

            if i == 18:
                chuzhong_yingyu_path = chuzhong_path + '\\' + str1[5]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path, url)
            if i == 19:
                chuzhong_yuwen_path = chuzhong_path + '\\' + str1[6]
                mkpathdir(chuzhong_yuwen_path)
                getgrade(chuzhong_yuwen_path, url)
            if i == 20:
                chuzhong_shuxue_path = chuzhong_path + '\\' + str1[7]
                mkpathdir(chuzhong_shuxue_path)
                getgrade(chuzhong_shuxue_path, url)

            if i == 21:
                chuzhong_yingyu_path = chuzhong_path + '\\' + str1[8]
                mkpathdir(chuzhong_yingyu_path)
                getgrade(chuzhong_yingyu_path, url)




if __name__ == '__main__':
    thmlpath = r'E:\试题专项'
    main(thmlpath)
