import re

391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.yp-uqd411')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>').replace(' 　　','<p><p/>').replace('　','<p><p/>').replace(' ','<p><p/>')
            return list_soupa
        except:
            return ''
def getContent(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = soup.select('div.fsize-24.mb-1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
    ul_list = soup.select('div.ta-r > ul.cm-pagination')
    content=getHtml(thmlpath, strUrl)
    url=strUrl.replace('.html','')+'_%d.html'
    if len(ul_list)>0:
        context = soup.select('div.ta-r > ul.cm-pagination > li > a')[0].text
        number=int(re.findall('共(.*?)页:',context)[0])
        for i in range(2,number+1):
            newUrl=url%i
            content=content+getHtml(thmlpath, newUrl)
    else:
        print('内容只有一页')
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)


def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.ym-jreb9u-item > div.ym-jreb9u-tit > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getContent(thmlpath, href)
def getPageUrl(thmlpath,strUrl):
    url=strUrl+'s%d.html'
    for i in range(1,80):
        try:
            newUrl=url%i
            getListUrl(thmlpath, newUrl)
        except:
            print('已经是最后一页')
            continue
def main(txtPath,thmlpath):
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        url=line.replace('\n','')
        getPageUrl(thmlpath, url)


if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\自查报告网'
    main(txtPath,thmlpath)
