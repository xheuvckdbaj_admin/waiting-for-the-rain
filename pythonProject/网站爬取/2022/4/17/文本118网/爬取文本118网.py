# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            ulContent='--------------------------------------------------正文内容开始----------------------------------------------------------'
            list_soupa = str(soup.select('div.content')[0]).replace(ulContent,'')
            title = soup.select('div.title > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.detail > div.container-content > div.right-bar > h3 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.txt118.com'+href
        getHtml(thmlpath, newUrl)

def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.page_link > div.inner > div.inner_2 > a')[-1].get('href')
    allPageNumber=int(href.split('-')[-1].replace('.shtm',''))
    url='http://www.txt118.com'+href
    hrefUrl=url.replace(href.split('-')[-1],'')+'%d.shtm'
    for i in range(1,allPageNumber+1):
        newUrl=hrefUrl%i
        getListUrl(thmlpath, newUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('#nav > ul > li > a')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            newUrl='http://www.txt118.com'+href
            getPageUrl(thmlpath, newUrl)

def main(thmlpath):
    url='http://www.txt118.com/index.html'
    getTitleUrl(thmlpath,url)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
