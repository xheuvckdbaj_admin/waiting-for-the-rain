# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup

from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


filePath=r'./cookies.txt.txt'
def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver



def getContent(thmlpath, strUrl,title):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        content = str(soup.select('div.Article_text > div.text')[0])
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w',encoding='utf-8')
        f.write(content)
        f.close()
    except:
        print('错误')
def getAllUrl(thmlpath, strUrl):
    # try:
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.last > ul > li > a')
    for a in a_list:
        href=a.get('href')
        newHref='https://www.zxxk.com'+href
        title=a.get('title').replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        getContent(thmlpath, newHref,title)


# except:
#     c
def getHtml(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        fenye = str(soup.select('div.fenye > div')[-1].text)
        allPage=int(re.findall('共(.*?)页',fenye)[0])
        for page in range(1,allPage+1):
            newUrl=strUrl+'/p'+str(page)+'.html'
            getAllUrl(thmlpath, newUrl)
    except:
        print('error')
def main(thmlpath):
            # driver = webdriver.Chrome()  # 打开chrome浏览器
            #相关热词搜索
            numbers=[3,4,5,16]
            strUrl='https://www.zxxk.com/wk/%d'
            for i in numbers:
                newUrl=strUrl%i
                print(newUrl)
                getHtml(thmlpath, newUrl)


if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\学科网文库'
    main(thmlpath)
