import zlib, struct

# def mainx():

def main():
    input_docin = r'C:\Users\Administrator\Downloads\head_460514392_0.docin'
    output_swf = r'E:\文档\自查报告网'
    docin = open(input_docin, 'rb')
    width = struct.unpack('i', docin.read(4))[0]
    height = struct.unpack('i', docin.read(4))[0]
    pages = struct.unpack('i', docin.read(4))[0]
    head_len = struct.unpack('i', docin.read(4))[0]
    swf_header = docin.read(head_len)
    print('%s, %s, %s, %s' % (width, height, pages, head_len))
    part = 1
    while 1:
        part_len = docin.read(4)
        if part_len:
            part_len = struct.unpack('i', part_len)[0]
            print(part_len)
            part_cont = docin.read(part_len)

            part_paper = zlib.decompress(swf_header) + zlib.decompress(part_cont)
            part_swf = '%s%s%s%s%s%s' % (struct.pack('b', 70),
                                         struct.pack('b', 87),
                                         struct.pack('b', 83),
                                         struct.pack('b', 9),
                                         struct.pack('i', len(part_paper)),
                                         part_paper)
            swf_file = open(output_swf + '//%s.swf' % (part), 'wb')
            swf_file.write(part_swf)
            swf_file.close()
            part += 1
        else:
            break
    docin.close()
if __name__ == '__main__':
    main()