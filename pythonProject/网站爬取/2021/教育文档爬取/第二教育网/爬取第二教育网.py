# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import readTxt
import checkIp
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

filePath = r'./cookies.txt.txt'
filePath1=r'.\ip网址.txt'

def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url



def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)


def set_chrome_pref(path):
    prefs = {"download.default_directory": path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)  # 打开chrome浏览器
    time.sleep(10)
    return driver


def getContent(browser, thmlpath, strUrl):
    try:
        getBrowserUrl(browser, strUrl)
        # addLingcookies.addCookies(browser, strUrl, filePath)
        browser.find_element_by_xpath(
            '//div[@class="istbutton fl"]/div[@class="xz_xunlei"]/div[@class="xz_x1"]/a').click()


    except:
        print('错误')


def main(thmlpath):
    browser = set_chrome_pref(thmlpath)
    # number=1
    strUrl = 'https://s.dearedu.com/download?aid=%d.html'
    for i in range(100000, 6530000):
        browser=getBrowser(browser, newUrl)
        print('------------', i)
        time.sleep(1.5)
        newUrl = strUrl % i
        getContent(browser, thmlpath, newUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档\第二教育网'
    main(thmlpath)
