# -*- coding:utf-8 -*-

import time

import requests

import addcookies
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
filePath=r".\cookies.txt"
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getNewWindow(browser,xpathStr):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, 100).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser

def getFile(path,number,last):
    # txtFile = open(path, 'r')
    # hrefs = txtFile.readlines()
    browser = webdriver.Chrome()
    for i in range(number,last):
        # try:
            print(i)
            browser.get('http://pan.baidu.com/share/link?uk=1764487400&shareid=1574299998')
            if i==number:
                addcookies.addCookies(browser, 'http://pan.baidu.com/share/link?uk=1764487400&shareid=1574299998', filePath)
            try:
                browser.find_element_by_class_name('zbyDdwb').click()
            except:
                print('error')
            try:
                browser.find_elements_by_css_selector('span.text')[1].click()
            except:
                continue
            try:
                browser=getNewWindow(browser, '//a[@class="underline"]')
            except:
                continue

            for i in range(0, 300):
                browser.find_element_by_css_selector('a.underline').click()
                time.sleep(0.5)
                try:
                    browser = getNewWindow(browser, '//img[@class="img-code"]')
                except:
                    continue
                href=browser.find_element_by_css_selector('img.img-code').get_attribute('src')
                sponse=requests.get(url=href, headers=headers).content

                newPath = path + '\\' + str(time.time()) + '.jpg'
                file_handle = open(newPath, mode='wb')
                file_handle.write(sponse)
        # except:
        #     print('错误')
        #     continue
def main():
    path=r'D:\验证码'
    number=0
    last=2000
    getFile(path,number,last)
if __name__ == '__main__':
    main()