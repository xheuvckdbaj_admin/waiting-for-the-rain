import time
import urllib

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.百度问答.下载百度知道文档 import getContentBaiduzhidao
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}



def getUrlAndTitle(folderPath,driver,urls,keyStr):
    try:
        elements = driver.find_elements(By.XPATH,
            '//dl[@class="dl"]/dt[@class="dt mb-3 line"]/a[@class="ti"]')
        for i, element in enumerate(elements):
            # try:
                title = element.text.replace(' ', '').strip()
                if mainPanDuanTitle(keyStr, title):
                    href = element.get_attribute('href')
                    hrefUrl = href
                    print(hrefUrl)
                    lineStr = hrefUrl + '————' + keyStr
                    getContentBaiduzhidao(hrefUrl, folderPath, keyStr)
                    urls.append(lineStr)
                    break
            # except:
            #     print('error')
            #     continue
    except:
        print('error')
def getPageUrl(folderPath,key,url,driver,urls):
    try:
        keyStr=key.replace('/','')
        driver.get(url)
        time.sleep(0.5)
        driver.find_element(By.XPATH,'//div[@class="search-cont clearfix"]/form[@class="search-form"]/input[@class="hdi"]').send_keys(key)
        time.sleep(0.1)
        driver.find_element(By.XPATH,'//div[@class="search-cont clearfix"]/form[@class="search-form"]/input').send_keys(Keys.ENTER)
        print(key)
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//input[@class="hdi"]'))
            )
        except:
            time.sleep(0.1)
        getUrlAndTitle(folderPath,driver,urls,keyStr)
    except:
        print('error')

def getKey(folderPath,txtPath,url,driver,urls,startnumber,last):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i,line in enumerate(lines):
        if i >= startnumber and i < last:
            key=line.replace('\n','')
            print(str(i)+'百度问答-----------------',key)
            getPageUrl(folderPath,key, url,driver,urls)
    # pathFile.close()
def mainWen(txtPath,folderPath,startnumber,last):
    urls=[]
    driver =getDriver('wenda')
    url='https://zhidao.baidu.com/'
    txtFilePath=txtPath+'\\all.txt'
    getKey(folderPath,txtFilePath, url,driver,urls,startnumber,last)
    driver.quit()
    return urls


if __name__ == '__main__':
    txtPath = r'E:\文档\百度活动文档\全网获取文档\全网url'
    folderPath=r'D:\文档\百度活动文档\百度html\其他网站'
    mainWen(txtPath,folderPath)
