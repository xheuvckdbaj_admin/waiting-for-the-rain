# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from pythonProject.FindFile import find_file, find_name
from pythonProject.初中.敏感词过滤.minganci import filterWord

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


def deletePara(file):
    for para in file.paragraphs:

        para.text = para.text.replace('\n', ' ')
        if para.text == "" or para.text == '\n' or para.text == ' ':
            # print('第{}段是空行段'.format(i))
            para.clear()  # 清除文字，并不删除段落，run也可以,
            delete_paragraph(para)


def tihuanguanjianci(lines, para):
    for line in lines:
        keyValue = line.replace('\n', '')
        key = keyValue.split('===>')[0]
        # print(key)
        value = keyValue.split('===>')[1]
        para.text = para.text.replace(key, value)

def deleteLaterParaPanduan(i, content, fileName):
    panduan = False
    content = content.replace(' ', '')
    if (not (i == 1 and i == 2)) and len(content) < 100:
        titleContent = fileName.replace('.docx', '').replace('[', '').replace(']', '').replace('【', '').replace('】',
                                                                                                                '').replace(
            '(', '').replace(')', '').replace(' ', '')
        content = content.replace('[', '').replace(']', '').replace('【', '').replace('】', '').replace('(', '').replace(
            ')', '').replace(' ', '')
        panduan = ((content[-5:-1] == titleContent[-5:-1]) or (content[0:5] == titleContent[0:5]))
    if content == '-' or content == '1' or content == '2' or content == '3' or content == '4' or content == '5':
        panduan = True
    return panduan

paghraPath='./删除活动文档段落违禁词.txt'
def panduanPaghra(paghraPath,content):
    file = open(paghraPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan

filterTxt='./内容敏感词.txt'
def filterDocx(panduan,filterTxt,content):
    file=open(filterTxt,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        key=line.replace('\n','')
        panduan=panduan or (key in content)
    return panduan

paghraDownPath='./删除以下段落的违禁词.txt'
def panduanPaghraDown(paghraDownPath,content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan
def deleAdvertising(filePath, newFolderPath, midFolderPath, newfileNames, front, later):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        # try:
                file = docx.Document(filePath)
                fileName = os.path.basename(filePath)
                fileName = fileName
                # print("段落数:" + str(len(file.paragraphs)))
                # print('删除前图形图像的数量：', len(file.inline_shapes))
                str = []
                contentStr=''
                for i,para in enumerate(file.paragraphs):
                    # 替换关键词，洗稿
                    contentStr=contentStr+para.text
                file_name=fileName
                if 'VIP' in contentStr or len(contentStr)<100 :
                    if file.save(newFolderPath + '\\' + file_name + '.docx') == None:
                        os.remove(filePath)  # 删除原来修改前的docx文件
                    if file_name in newfileNames:
                        os.remove(filePath)
                else:
                    print('内容没有含有vip')
        # except:
        #     print('错误')
# word.Quit()
def shear_dile(oldFolderPath,newFolderPath,midFolderPath, newfileNames):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i,d in enumerate(os.listdir(oldFolderPath)):
                    shear_dile(os.path.join(oldFolderPath, d),newFolderPath, midFolderPath, newfileNames)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.docx'):
            print(oldFolderPath)
            deleAdvertising(oldFolderPath, newFolderPath,midFolderPath, newfileNames, 0, 0)

def modifyContent(oldFolderPath, newFolderPath,midFolderPath):
    newfileNames = find_name(newFolderPath, [])
    paths = r'./doc_replace.txt'
    # fileTxt = open(paths, 'r', encoding='utf-8')
    # lines = fileTxt.readlines()
    shear_dile(oldFolderPath,newFolderPath,midFolderPath, newfileNames)

def mainModifyVip(oldFolderPath,newFolderPath,midFolderPath):
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    modifyContent(oldFolderPath, newFolderPath,midFolderPath)

if __name__ == '__main__':
    oldFolderPath = r'D:\文档\百度活动文档\修改后百度活动文档'
    newFolderPath = r'D:\文档\百度活动文档\百度html'
    midFolderPath = r'E:\文档\自查报告网url'
    mainModifyVip(oldFolderPath,newFolderPath,midFolderPath)
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
