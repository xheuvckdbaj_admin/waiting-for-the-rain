import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getContent(hrefUrl,htmlPath,title,driver):
    try:
        driver.get(hrefUrl)
        td=''
        try:
            td=driver.find_element(By.XPATH,'//div[@class="answer_layout_3yYc6m f-s-m-l f-c-2"]')
        except:
            try:
                td = driver.find_element(By.XPATH,'//div[@class="p-meta"]')
            except:
                td=''
        list_soupa = td.get_attribute('innerHTML')
        print(title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def mianJin(urls,htmlPath):
    driver = getDriver('toutiao')
    for url in urls:
        hrefUrl = url.replace('\n', '').split('————')[0]
        title = url.replace('\n', '').split('————')[-1]
        getContent(hrefUrl,htmlPath,title,driver)
    driver.quit()

if __name__ == '__main__':
    htmlPath=r'E:\文档\百度活动文档\全网获取文档\全网html'
    urls=['https://so.toutiao.com/s/search_wenda_pc/list/?enter_from=search_result&qid=6930935777216413960&qname=%E5%87%BA%E9%99%A2%E5%B0%8F%E7%BB%93%E4%B9%A6%E5%86%99%E8%A6%81%E6%B1%82%E5%8F%8A%E5%86%85%E5%AE%B9%EF%BC%9F&search_id=20220923141021010212185165001EDEFF&enter_answer_id=7097557733083251239&outer_show_aid=7097557733083251239&query=%E5%87%BA%E9%99%A2%E5%B0%8F%E7%BB%93%E5%8F%AF%E6%89%8B%E5%86%99%E5%A2%9E%E5%8A%A0%E5%86%85%E5%AE%B9'
          '————法制第一课广播稿']
    mianJin(urls,htmlPath)