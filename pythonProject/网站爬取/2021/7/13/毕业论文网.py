# -*- coding:utf-8 -*-
import re


import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #time.sleep(10)
        #url='http://m.pincai.com/article/2300000.htm'
        try:
            requests_text = requests.get(url=url, headers=headers).content

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.content')[0])
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url):

    requests_text = requests.get(url=url, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')

    a_list=soup.select('.e2 > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            newUrl='http://www.lw54.com'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    try:
        requests_text = requests.get(url=strUrl, headers=headers).content
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('.pagelist > li > a')
        hrefStr=a_list[len(a_list)-1].get('href')
        numberStr=re.match('(.*?)_(.*?)_(.*?).html',hrefStr).group(3)
        newUrl=hrefStr.split(numberStr)[0]+'%d.html'
        for i in range(1,int(numberStr)):
            newHref=newUrl%i
            getUrl(thmlpath, newHref)
    except:
        print('error')

def getHtml2(thmlpath, strUrl):

    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')

    a_list = soup.select('.more > a')
    for i in range(0,len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        newUrl='http://www.lw54.com'+href
        getHtml3(thmlpath, newUrl)
def getHtml(thmlpath, strUrl,number):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.w960.center > ul > li > a')
    for i in range(1, len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        getHtml2(thmlpath, href)
def main(thmlpath):
            number=203376
            strUrl='http://www.lw54.com/'
            getHtml(thmlpath, strUrl,number)


if __name__ == '__main__':
    thmlpath = r'D:\文档\毕业论文网'
    main(thmlpath)
