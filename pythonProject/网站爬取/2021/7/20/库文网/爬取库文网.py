# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="near_s"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.kuwen.com/diaoyanbaogao/article_255670.html',
                'https://www.kuwen.com/kaitibaogao/article_214334.html',
                'https://www.kuwen.com/jietibaogao/article_255762.html',
                'https://www.kuwen.com/dushubaogao/article_205201.html',
                'https://www.kuwen.com/sixiangbaogao/article_239522.html',
                'https://www.kuwen.com/zichabaogao/article_255669.html',
                'https://www.kuwen.com/zipingbaogao/article_195280.html',
                'https://www.kuwen.com/kaochabaogao/article_245727.html',
                'https://www.kuwen.com/jingpinbaogao/article_246954.html',
                'https://www.kuwen.com/zhuanzhengbaogao/article_255858.html',
                'https://www.kuwen.com/shuzhibaogao/article_255864.html',
                'https://www.kuwen.com/ziwojieshao/article_255991.html',
                'https://www.kuwen.com/gongzuojingli/article_176931.html',
                'https://www.kuwen.com/shixizhouji/article_255784.html',
                'https://www.kuwen.com/shixibaogao/article_255930.html',
                'https://www.kuwen.com/shixixinde/article_255916.html',
                'https://www.kuwen.com/zhuanzhengshenqing/article_255956.html',
                'https://www.kuwen.com/zhiyeguihua/article_243066.html',
                'https://www.kuwen.com/gongzuofangan/article_255873.html',
                'https://www.kuwen.com/gongzuohuibao/article_252473.html',
                'https://www.kuwen.com/cizhibaogao/article_255948.html',
                'https://www.kuwen.com/gerenzongjie/article_255971.html',
                'https://www.kuwen.com/xuexizongjie/article_255944.html',
                'https://www.kuwen.com/gongzuozongjie/article_255966.html',
                'https://www.kuwen.com/zhuanzhengzongjie/article_255970.html',
                'https://www.kuwen.com/huodongzongjie/article_255961.html',
                'https://www.kuwen.com/xiaoshouzongjie/article_255958.html',
                'https://www.kuwen.com/huiyizongjie/article_243626.html',
                'https://www.kuwen.com/xiangmuzongjie/article_255957.html',
                'https://www.kuwen.com/zhouzongjie/article_255969.html',
                'https://www.kuwen.com/yuezongjie/article_255950.html',
                'https://www.kuwen.com/jiduzongjie/article_255816.html',
                'https://www.kuwen.com/jiantaoshu/article_255960.html',
                'https://www.kuwen.com/baozhengshu/article_255964.html',
                'https://www.kuwen.com/chengnuoshu/article_255943.html',
                'https://www.kuwen.com/zerenshu/article_255772.html',
                'https://www.kuwen.com/xieyishu/article_255946.html',
                'https://www.kuwen.com/weituoshu/article_255951.html',
                'https://www.kuwen.com/jianyishu/article_255979.html',
                'https://www.kuwen.com/yixiangshu/article_249078.html',
                'https://www.kuwen.com/changyishu/article_255999.html',
                'https://www.kuwen.com/shenqingshu/article_255993.html',
                'https://www.kuwen.com/qiuzhixin/article_255937.html',
                'https://www.kuwen.com/cizhixin/article_255954.html',
                'https://www.kuwen.com/jieshaoxin/article_255811.html',
                'https://www.kuwen.com/xindetihui/article_255866.html',
                'https://www.kuwen.com/yanjianggao/article_256000.html',
                'https://www.kuwen.com/fayangao/article_255955.html',
                'https://www.kuwen.com/guangbogao/article_255962.html',
                'https://www.kuwen.com/zhuchigao/article_255968.html',
                'https://www.kuwen.com/jiayougao/article_255940.html',
                'https://www.kuwen.com/guanggaoci/article_252635.html',
                'https://www.kuwen.com/daoyouci/article_255959.html',
                'https://www.kuwen.com/zhujiuci/article_255135.html',
                'https://www.kuwen.com/daxieci/article_255965.html',
                'https://www.kuwen.com/zhongkaozhufuyu/article_253399.html',
                'https://www.kuwen.com/gaokaozhufuyu/article_251924.html',
                'https://www.kuwen.com/shengxuezhufuyu/article_252445.html',
                'https://www.kuwen.com/biyezhufuyu/article_255451.html',
                'https://www.kuwen.com/xinnianzhufuyu/article_255984.html',
                'https://www.kuwen.com/jierizhufuyu/article_252802.html',
                'https://www.kuwen.com/shiyezhufuyu/article_226820.html',
                'https://www.kuwen.com/jiehunzhufuyu/article_255813.html',
                'https://www.kuwen.com/qiaoqianzhufuyu/article_251918.html',
                'https://www.kuwen.com/maimaihetong/article_255877.html',
                'https://www.kuwen.com/xiaoshouhetong/article_255905.html',
                'https://www.kuwen.com/gouxiaohetong/article_255731.html',
                'https://www.kuwen.com/daixiaohetong/article_250350.html',
                'https://www.kuwen.com/gonghuohetong/article_255987.html',
                'https://www.kuwen.com/caigouhetong/article_255729.html',
                'https://www.kuwen.com/yunshuhetong/article_255732.html',
                'https://www.kuwen.com/guanggaohetong/article_255749.html',
                'https://www.kuwen.com/danjiahetong/article_75239.html',
                'https://www.kuwen.com/jiedaihetong/article_255741.html',
                'https://www.kuwen.com/jiekuanhetong/article_255910.html',
                'https://www.kuwen.com/zhiyahetong/article_250258.html',
                'https://www.kuwen.com/danbaohetong/article_251726.html',
                'https://www.kuwen.com/baozhenghetong/article_227020.html',
                'https://www.kuwen.com/diyahetong/article_255918.html',
                'https://www.kuwen.com/jianzhuhetong/article_255935.html',
                'https://www.kuwen.com/zhuangxiuhetong/article_255952.html',
                'https://www.kuwen.com/shigonghetong/article_255929.html',
                'https://www.kuwen.com/chengbaohetong/article_255900.html',
                'https://www.kuwen.com/gongchenghetong/article_255921.html',
                'https://www.kuwen.com/jianfanghetong/article_250460.html',
                'https://www.kuwen.com/shejihetong/article_255976.html',
                'https://www.kuwen.com/laodonghetong/article_255690.html',
                'https://www.kuwen.com/lvyouhetong/article_255701.html',
                'https://www.kuwen.com/weixiuhetong/article_255939.html',
                'https://www.kuwen.com/jiagonghetong/article_255926.html',
                'https://www.kuwen.com/zengyuhetong/article_251229.html',
                'https://www.kuwen.com/gufenhetong/article_255894.html',
                'https://www.kuwen.com/jitihetong/article_250589.html',
                'https://www.kuwen.com/hehuohetong/article_250656.html',
                'https://www.kuwen.com/hezuohetong/article_255981.html',
                'https://www.kuwen.com/dailihetong/article_255887.html',
                'https://www.kuwen.com/weituohetong/article_255884.html',
                'https://www.kuwen.com/zufanghetong/article_255857.html',
                'https://www.kuwen.com/zulinhetong/article_255667.html',
                'https://www.kuwen.com/zuchehetong/article_252388.html',
                'https://www.kuwen.com/zhuanzuhetong/article_241707.html',
                'https://www.kuwen.com/zhuanranghetong/article_255994.html',
                'https://www.kuwen.com/maifanghetong/article_240978.html',
                'https://www.kuwen.com/goufanghetong/article_255947.html',
                'https://www.kuwen.com/shoufanghetong/article_177177.html',
                'https://www.kuwen.com/ershoufanghetong/article_255988.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
