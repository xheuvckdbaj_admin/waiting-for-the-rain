import os
import shutil


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)

def find_file(wordFolder,filePaths=[]):
    makeFolder(wordFolder)
    dir = os.listdir(wordFolder)
    for i, name in enumerate(dir):
            name = os.path.join(wordFolder, name)
            if os.path.isdir(name):
                find_file(name)
            else:
                filePath = name
                filePaths.append(filePath)
    return filePaths
def find_nullFile(wordFolder):
    filePaths = []
    dir = os.listdir(wordFolder)
    for i, name in enumerate(dir):
            name = os.path.join(wordFolder, name)
            if os.path.isdir(name):
                find_nullFile(name)
            else:
                filePath = name
                filePaths.append(filePath)
    return filePaths

def makeFolder(floderPath):
    if not os.path.exists(floderPath):
        os.makedirs(floderPath)
    return floderPath
def getFileName(wordFolder):
    fileNameNoDocxs=[]
    wordFiles = find_nullFile(wordFolder)
    for wordFile in wordFiles:
        fileNameNoDocx = os.path.basename(wordFile).replace('.docx', '')
        fileNameNoDocxs.append(fileNameNoDocx)
    return fileNameNoDocxs
def panduanNameDing(titleFolder,uploadFolder,wordFolder):
    fileNameNoDocxs=getFileName(wordFolder)
    filePaths=find_file(titleFolder)
    print(len(filePaths))
    numberDocx=0
    for filePath in filePaths:
        try:
            number=1
            file=open(filePath,'r',encoding='utf-8')
            keys=file.readlines()
            print(filePath)
            print(keys)
            for fileNameNoDocx in fileNameNoDocxs:

                if numberDocx < 500:
                    if fileNameNoDocx+'\n' in keys:
                        try:
                            pathName = os.path.basename(filePath).replace('.txt', '')
                            lanmu = filePath.replace(os.path.basename(filePath), '').split('\\')[-2]
                            newfloderPath = uploadFolder + '\\' + lanmu + '\\' + pathName
                            if not os.path.exists(newfloderPath):
                                makeFolder(newfloderPath)
                            shutil.move(wordFolder+'\\'+fileNameNoDocx+'.docx', newfloderPath)
                            number+=1
                            numberDocx+=1
                        except:
                            continue
        except:
            print('分类失败')
    #删除空文件夹
    pathNamexs=['推荐','学前教育','基础教育','高校与高等教育','资格考试',
                '法律','建筑','互联网','行业资料','政务民生','商品说明书','实用模板','生活娱乐']
    for i,pathNameS in enumerate(pathNamexs):
        newfloderPath = uploadFolder + '\\' + pathNameS
        if os.path.exists(newfloderPath):
            filePaths = find_file(newfloderPath)
            if len(filePaths)==0:
                try:
                    shutil.rmtree(newfloderPath)
                except Exception as e:
                    print(str(e),newfloderPath+'删除失败！！！')


def main():
    #存放活动文档标题的文件夹
    titleFolder=r'D:\文档\百度活动文档\用来分类文档标题'
    #修改后需要分类百度活动文档的文件夹
    wordFolder=r'D:\文档\百度活动文档\修改后百度活动文档'
    #存放分类后需要上传的活动文档的文件夹
    uploadFolder=r'D:\文档\百度活动文档\百度活动文档上传'
    panduanNameDing(titleFolder, uploadFolder, wordFolder)
if __name__ == '__main__':
    main()