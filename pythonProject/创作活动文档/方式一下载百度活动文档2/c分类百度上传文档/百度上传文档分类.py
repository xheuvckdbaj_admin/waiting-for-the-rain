import os
import shutil

def find_file(wordFolder):
    filePaths=[]
    dir = os.listdir(wordFolder)
    for i, name in enumerate(dir):
            name = os.path.join(wordFolder, name)
            if os.path.isdir(name):
                find_file(name)
            else:
                filePath = name
                filePaths.append(filePath)
    return filePaths

def makeFolder(floderPath,fileName):
    print(fileName)
    floderPath=floderPath+'\\'+fileName
    os.makedirs(floderPath)
    return floderPath
def getFileName(wordFolder):
    fileNameNoDocxs=[]
    wordFiles = find_file(wordFolder)
    for wordFile in wordFiles:
        fileNameNoDocx = os.path.basename(wordFile).replace('.docx', '')
        fileNameNoDocxs.append(fileNameNoDocx)
    return fileNameNoDocxs
def panduanName(titleFolder,uploadFolder,wordFolder):
    fileNameNoDocxs=getFileName(wordFolder)
    filePaths=find_file(titleFolder)
    numberDocx=0
    for filePath in filePaths:
        number=1
        pathName=os.path.basename(filePath).replace('.txt', '')
        if pathName == '1':
            pathNamex = '1推荐'
        elif pathName == '2':
            pathNamex = '2学前教育'
        elif pathName == '3':
            pathNamex = '3基础教育'
        elif pathName == '4':
            pathNamex = '4高校与高等教育'
        elif pathName == '5':
            pathNamex = '5资格考试'
        elif pathName == '6':
            pathNamex = '6法律'
        elif pathName == '7':
            pathNamex = '7建筑'
        elif pathName == '8':
            pathNamex = '8互联网'
        elif pathName == '9':
            pathNamex = '9行业资料'
        elif pathName == 'a10':
            pathNamex = '10政务民生'
        elif pathName == 'a11':
            pathNamex = '11商品说明书'
        elif pathName == 'a12':
            pathNamex = '12实用模板'
        elif pathName == 'a13':
            pathNamex = '13生活娱乐'
        elif pathName == 'all':
            os.remove(filePath)
            continue
        newfloderPath=uploadFolder+'\\'+pathNamex
        if not os.path.exists(newfloderPath):
            makeFolder(uploadFolder, pathNamex)
        file=open(filePath,'r',encoding='utf-8')
        keys=file.readlines()
        for fileNameNoDocx in fileNameNoDocxs:
            if numberDocx < 5000:
                if fileNameNoDocx+'\n' in keys:
                    # pagerNumber=str(int(number/10))
                    # classifyfloderPath = newfloderPath + '\\' + pagerNumber
                    # if not os.path.exists(classifyfloderPath):
                    #     makeFolder(newfloderPath, pagerNumber)
                    try:
                        shutil.move(wordFolder+'\\'+fileNameNoDocx+'.docx', newfloderPath)
                        number+=1
                        numberDocx+=1
                    except:
                        continue

def main():
    #存放活动文档标题的文件夹
    titleFolder=r'G:\文档\百度活动文档2\百度活动文档标题'
    #修改后需要分类百度活动文档的文件夹
    wordFolder=r'G:\文档\百度活动文档2\修改后百度活动文档'
    #存放分类后需要上传的活动文档的文件夹
    uploadFolder=r'G:\文档\百度活动文档2\百度活动文档上传'
    panduanName(titleFolder, uploadFolder, wordFolder)
if __name__ == '__main__':
    main()