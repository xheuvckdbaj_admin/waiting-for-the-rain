import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_lvtu(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div > h1')[0].text 
        
        list_soupa=str(soup.select('div.detail-conts')[0]).replace('相关推荐','').replace(title,title).split('延伸阅读：')[0]
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('律图下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.64365.com/zs/1501796.aspx'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\文书帮'
    keyStr='kaixin'
    title=''
    mian_lvtu(url,htmlPath,keyStr,title)