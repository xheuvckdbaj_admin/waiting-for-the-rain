# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'https://www.hunanhr.cn/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('div > h1')[0].text
    #print(title)
    list_soupa = str(soup.select('.zhengwen > .content')[0])
    print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    ul_list=soup.select('.listlie > .lie1')
    #print(url_list)
    for u in ul_list:
            href=u.select('a')[0].get('href')
            print(href)
            title = u.select('a')[0].text
            getContent(thmlpath,href,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('#NEW_INFO_LIST > dl')
    #print(url_list)
    for u in url_list:
            src=u.select('dt > a')[0].get('href')
            href='http://www.banzhuren.cn'+src
            print(href)
            title = u.select('dt > a')[0].text
            getContent(thmlpath,href,title)

def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2000):
        try:
            if pageNum==1:
                oldUrl = url + 'index' + '.html'
                print(oldUrl)
                getUrl(thmlpath, oldUrl)


            else:
                    oldUrl = url + str(pageNum) + '.html'
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)

        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['gongzuozongjie','gongzuojihua','duhougan','fayangao','xindetihui','shenqingshudaquan','sixianghuibao',
           'shuzhibaogao','zuowendaquan','jiaoxuesheji','zhuantifanwen','yanjianggao','jiaoxuejihua',
           'jiaoxuefansi','jiaoanxiazai','fuxibeike','shijuanxiazai','daoxuean','jiaoxuezongjie','yuxijiangjie','danyuanceshi']
    for s in str:
        url = groupUrl(s)
        getUrlPageNum(thmlpath,url)


if __name__ == '__main__':
    thmlpath = r'E:\标题文件'
    main(thmlpath)
