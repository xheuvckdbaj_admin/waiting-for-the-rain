import re


def mainPanDuanTitle(keyStr,title):
    panduan=False
    if "《" in keyStr and "》" in keyStr:
        titleStr=re.search('《(.*?)》',keyStr)[0].replace('《','').replace('》','')
        if titleStr in title and (keyStr[-4:] in title or keyStr[-6:-2] in title):
            panduan=True
    replaceStr=keyStr.replace(' ','').replace('，','').replace(',','').replace('。','').replace('幼儿园','').replace('《','').replace('》','').replace('"','').replace('"','')
    reStr=re.sub('\d+字', '', replaceStr)
    if reStr[0:4] in title and (reStr[-4:] in title or reStr[-6:-2] in title):
        panduan=True
    return panduan

if __name__ == '__main__':
    keyStr='形容黑心肝的搞笑句子'
    title='形容黑心肝的句子'
    panduan=mainPanDuanTitle(keyStr, title)
    print(panduan)