import time

import requests
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def set_chrome_pref():

    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    driver = webdriver.Chrome(chrome_options=option)  # 打开chrome浏览器
    time.sleep(10)
    return driver
def crawing(browser):
    url='https://www.wenkub.com/c-0001400005-%d-0-0-0-0-0-9-1-0.html'
    for number in range(3106,9557):
        filePath = r'E:\文档\文库吧党政相关' + '\\' + str(time.time()) + '.txt'
        file_handle = open(filePath, 'w')
        newUrl=url%number
        browser.get(newUrl)
        elements=browser.find_elements_by_xpath('//ul/li/div[@class="doc-list-title"]/h3/a')
        for element in elements:
                    href=element.get_attribute('href')
                    print(href)
                    file_handle.write(href+'\n')
        file_handle.close()
def main():
    browser = set_chrome_pref()
    path=r''
    crawing(browser)
if __name__ == '__main__':
    main()