import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_chuangwen(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.article-header > h1')[0].text 
        
        content=str(soup.select('div.article-tags')[0])
        list_soupa=str(soup.select('div.article-content')[0]).replace('相关推荐','').replace(content,'').replace(title,title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('创闻头条下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://www.cwtea.net/article/189681.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\优文网'
    keyStr='kaixin'
    title=''
    mian_chuangwen(url,htmlPath,keyStr,title)