# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


filePath=r'./cookies.txt.txt'
def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver


def getHtml(browser,thmlpath, strUrl):
    # try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        content = str(soup.select('#article > #article_body')[0])
        title=str(soup.select('#article > h1')[0].text)
        print(title)
        title = title.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(content)
    # except:
    #     c
def getContent(thmlpath, newUrl):
    # try:
        requests_text = str(requests.get(url=newUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        content = str(soup.select('div.darti-content')[0])
        newContent=re.sub('转(.*)cn','',content)
        title = str(soup.select('div.darti-head > h1.darti-title')[0].text).replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w',encoding='utf-8')
        f.write(newContent)
        f.close()
    # except:
    #     print('错误')

def main(thmlpath):
            strUrl='https://www.dyhzdl.cn/content/%d.html'
            for i in range(500,399130):
                newUrl=strUrl%i
                getContent(thmlpath, newUrl)


if __name__ == '__main__':
    #第一文档网
    #转载于 :Www.dYhzdl.cn :
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
