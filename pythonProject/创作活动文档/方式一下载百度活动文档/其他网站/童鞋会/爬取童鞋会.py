import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_tongxiehui(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup0 = BeautifulSoup(requests_text, 'lxml')
        hrefUrl=soup0.select('p.bm_more > a')[0].get('href')
        requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.title > h1')[0].text 
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.content-txt')[0]).replace('相关推荐','').replace(title,keyStr).split('推荐访问:')[0]
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('童鞋会下载成功----',keyStr)
    except:
        # try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            title = soup.select('div.title > h1')[0].text.replace('（', '').replace('）', '')
            title = re.sub('\d+篇', '', title)
            list_soupa = str(soup.select('div.content-txt')[0]).replace('相关推荐', '').replace(title, keyStr).split('推荐访问:')[0]
            thmlpath = htmlPath + '\\' + keyStr + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
            print('童鞋会下载成功----', keyStr)
        # except:
        #     print('error')

if __name__ == '__main__':
    url = 'https://tongxiehui.net/by/5ee6f6c19e0cd.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_tongxiehui(url,htmlPath,keyStr,title)