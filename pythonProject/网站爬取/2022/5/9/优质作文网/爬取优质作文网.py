import re

391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getListUrl(thmlpath,file,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.artbox_l > div.artbox_l_t > a')
        for i,a in enumerate(a_list):
            href=a.get('href')
            print(href)
            file.write(href+'\n')
    except:
        print('error')
def getSmallTitleUrl(thmlpath, file,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.index_tit > span > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getListUrl(thmlpath, file, href)
def getTitleUrl(thmlpath, file,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('#nav > li > a')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            getSmallTitleUrl(thmlpath, file, href)

def main(txtPath,thmlpath):
    file=open(txtPath,'w',encoding='utf-8')
    url='https://www.yzzuowen.com/'
    getTitleUrl(thmlpath, file,url)
    file.close()

def getHtml(thmlpath, strUrl):
        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0]).replace('<br>', '<p><p/>').replace('<br/>',
                                                                                                 '<p><p/>').replace(
                ' 　　', '<p><p/>').replace('　', '<p><p/>').replace(' ', '<p><p/>')
            title = soup.select('div.article > h1.title')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')
def getTopUrl(thmlpath,TopNumber,strUrl):
    if TopNumber < 3200:
        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            hrefUrl = soup.select('span.pre > a')[0].get('href')
            getHtml(thmlpath, strUrl)
            getTopUrl(thmlpath, TopNumber, hrefUrl)
        except:
            print('没有上一篇')
def getDownUrl(thmlpath,downNumber,strUrl):
    if downNumber < 3200:
        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            hrefUrl = soup.select('span.next > a')[0].get('href')
            getHtml(thmlpath, strUrl)
            getTopUrl(thmlpath, downNumber, hrefUrl)
        except:
            print('没有下一篇')

def mianx(txtPath,thmlpath):
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        TopNumber=1
        downNumber=1
        url=line.replace('\n','')
        getHtml(thmlpath, url)
        getTopUrl(thmlpath, TopNumber, url)
        getDownUrl(thmlpath, downNumber, url)

if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\自查报告网'
    # main(txtPath,thmlpath)
    mianx(txtPath, thmlpath)
