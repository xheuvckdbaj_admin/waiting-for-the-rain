import os
import re
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options  # 手机模式
from selenium.webdriver.common.by import By


def getDriver():
    # 设置手机型号，这设置为iPhone 6
    mobile_emulation = {"deviceName": "iPhone 7"}
    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    # 启动配置好的浏览器
    driver = webdriver.Chrome(options=options)
    return  driver


def saveFile(thmlpath,url,title,driver,keyStr):
    try:
        print(url)
        # 输入网址
        driver.get(url)
        time.sleep(0.2)
        td = driver.find_element(By.XPATH,'//tbody')
        # td = driver.find_element(By.XPATH,'//body')
        content = td.text
        # print(content)
        list_soupa = td.get_attribute('innerHTML').replace(title,keyStr).split('点击加载更多')[0].split('开通VIP，免费享6亿+内容')[-1].replace('开通VIP','')\
            .replace('点击立即咨询，了解更多详情','').replace('咨询','').replace('商品类型','').replace('品质优享','').replace('VIP专享','').replace('全部最热最新筛选','').split('搜索')[0]
        if not '该文档已被删除' in list_soupa:
            title=title.replace(' - ','')
            thmlpath = thmlpath + '\\' + keyStr + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
    except:
        # try:
            print(url)
            # 这里输入网址
            driver.get(url)
            time.sleep(0.2)
            # td = driver.find_element(By.XPATH,'//tbody')
            td = driver.find_element(By.XPATH,'//body')
            content = td.text
            list_soupa = td.get_attribute('innerHTML').replace(title,keyStr).split('点击加载更多')[0].split('开通VIP，免费享6亿+内容')[-1].replace('开通VIP','')\
            .replace('点击立即咨询，了解更多详情','').replace('咨询','').replace('商品类型','').replace('品质优享','').replace('VIP专享','').replace('全部最热最新筛选','').split('搜索')[0]
            if not '该文档已被删除' in list_soupa:
                title = title.replace(' - ', '')
                thmlpath = thmlpath + '\\' + keyStr + '.html'
                f = open(thmlpath, 'w', encoding='utf-8')
                f.write(list_soupa)
        # except:
        #     print('error')

def main_other_baiDu(thmlpath,url,keyStr,title):
    if 'wk.baidu.com' in url or 'wenku.baidu.com' in url:
        phoneUrl = url.split('.html')[0] + '?pcqq.c2c&bfetype=old'
        driver = getDriver()
        title = title.replace('（', '').replace('）', '')
        title = re.sub('\d+篇', '', title)
        saveFile(thmlpath, phoneUrl, title, driver, keyStr)
        driver.quit()
if __name__ == '__main__':
    url='https://wenku.baidu.com/view/7e2fd126757f5acfa1c7aa00b52acfc789eb9ff3.html?_wkts_=1685764213414&bdQuery=%E6%8A%8A%E7%A7%91%E5%AD%A6%E5%8F%91%E5%B1%95%E8%A7%82%E8%B4%AF%E7%A9%BF%E4%BA%8E%E5%8F%91%E5%B1%95%E7%9A%84%E6%95%B4%E4%B8%AA%E8%BF%87%E7%A8%8B%E5%92%8C%E5%90%84%E4%B8%AA%E6%96%B9%E9%9D%A2%E5%90%AF%E7%A4%BA'
    # url=r'https://wenku.baidu.com/view/7e2fd126757f5acfa1c7aa00b52acfc789eb9ff3?pcqq.c2c&bfetype=old'
    title='应聘铁路公司自我介绍对比1'
    thmlpath=r'D:\文档\百度活动文档\百度html\其他网站\百度文库'
    keyStr='应聘铁路公司自我介绍对比'
    main_other_baiDu(thmlpath,url,keyStr,title)