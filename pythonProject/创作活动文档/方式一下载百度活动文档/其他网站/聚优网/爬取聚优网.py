import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_jy135(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.article > h1')[0].text 
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.content')[0]).replace('相关推荐','').replace(title,keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('聚优网下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.jy135.com/ziyuan/gongzuojihua/819744.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\聚优网'
    keyStr='kaixin'
    title=''
    mian_jy135(url,htmlPath,keyStr,title)