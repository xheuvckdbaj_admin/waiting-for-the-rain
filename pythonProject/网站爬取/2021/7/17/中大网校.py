# -*- coding:utf-8 -*-
import re
import time

import docx
import requests

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath,url):

        #time.sleep(10)
            #url='http://www.beiku.com/xieyishu/article_145325.html'
        try:

            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.content')[0])
            title = str(soup.select('.main_cont > h1')[0].text).replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w')
            f.write(list_soupa)
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = requests.get(url=url, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    href=soup.select('.newsList > li > a')
    newHref='https://www.hunanhr.cn/index.php'+href
    getContent(thmlpath,newHref,title)



def getHtml3(thmlpath, strUrl):
    getUrl(thmlpath, strUrl)
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.showPages > a')
    hrefStr=a_list[len(a_list)-2].get('href')
    num=int(re.match('(.*?)_(.*?).html',hrefStr).group(2))
    newUrl=strUrl+'index_%d.html'
    for i in (2,num+1):
        newhref=newUrl%i
        getUrl(thmlpath, newhref)

def getHtml2(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.news__hdNav > li > a')
    for a in a_list:
        href = a.get('href')
        newUrl = 'https:' + href
        getHtml3(thmlpath, newUrl)
def getHtml(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.news__nav > .news__navItem > a')
    for i in range(0, len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        newUrl='https:'+href
        getHtml2(thmlpath, newUrl)
def combination(thmlpath, strUrl,number):
    for i in range(number,250000):
        newUrl=strUrl%i
        getContent(thmlpath, newUrl)
def main(thmlpath):
            number=1
            strUrl='https://www.wangxiao.cn/'
            getHtml(thmlpath, strUrl)



if __name__ == '__main__':
    thmlpath = r'E:\中大网校'
    main(thmlpath)
