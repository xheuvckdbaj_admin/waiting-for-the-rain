# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.kt250.com/fanwen/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        #url='http://blog.sina.com.cn/s/blog_4b2cb00e0102ds94.html'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        #print(title)
        title = str(soup.select('.artical > .articalTitle > h2.titName.SG_txta')[0].text).replace('武书连','').replace('<','').replace('>','')
        content=str(soup.select('.artical > div.articalContent')[0])

        #print(newContent)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(content)
    except:
        print('error')


# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = requests.get(url=url, headers=headers)
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.news-list-left-content > ul > li > h2 > a')
    #print(url_list)
    for a in a_list:
            href=a.get('href')
            newUrl='https://www.51test.net'+href
            print(newUrl)
            title = a.text
            getContent(thmlpath,newUrl,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    print(requests_text)
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.article_blk > .articleList > div.articleCell.SG_j_linedot1 > p.atc_main.SG_dot > .atc_title > a')
    #print(url_list)
    for a in a_list:
            src=a.get('href')
            href=src
            print(href)
            title=a.text
            getContent(thmlpath,href,title)

def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2):


                    oldUrl = url
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)







def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            getUrl(thmlpath, url)
        except:
            continue


def main(thmlpath):
    for pageNum in range(1, 1000):
        #url = 'https://s.weibo.com/article?q=%E9%AB%98%E8%80%83%E5%BF%97%E6%84%BF&Refer=SListRelpage_box&page='
        #oldUrl = url+str(pageNum)
        oldUrl='http://blog.sina.com.cn/s/articlelist_1261219854_0_%d.html'%pageNum
        print(oldUrl)
       # getUrl(thmlpath, oldUrl)
        getUrlIndex(thmlpath,oldUrl)


if __name__ == '__main__':
    thmlpath = r'E:\武书连的博客'
    main(thmlpath)
