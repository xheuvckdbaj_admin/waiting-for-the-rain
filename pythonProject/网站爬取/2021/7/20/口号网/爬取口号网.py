# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.kouhaowang.net/junxun/article_78222.html',
                'https://www.kouhaowang.net/lage/article_78134.html',
                'https://www.kouhaowang.net/jingsai/article_77555.html',
                'https://www.kouhaowang.net/xiaoqing/article_77977.html',
                'https://www.kouhaowang.net/xueleifengbiaoyu/article_77604.html',
                'https://www.kouhaowang.net/youeryuanyundonghui/article_77907.html',
                'https://www.kouhaowang.net/xiaoxueyundonghui/article_78218.html',
                'https://www.kouhaowang.net/zhongxueshengyundonghui/article_77346.html',
                'https://www.kouhaowang.net/daxueyundonghui/article_77026.html',
                'https://www.kouhaowang.net/xiaoyunhui/article_78096.html',
                'https://www.kouhaowang.net/jiayou/article_78153.html',
                'https://www.kouhaowang.net/laladui/article_77805.html',
                'https://www.kouhaowang.net/yundong/article_78233.html',
                'https://www.kouhaowang.net/fangzhen/article_77804.html',
                'https://www.kouhaowang.net/duiwu/article_77938.html',
                'https://www.kouhaowang.net/xiaoshou/article_78145.html',
                'https://www.kouhaowang.net/xiaozu/article_77893.html',
                'https://www.kouhaowang.net/jili/article_78238.html',
                'https://www.kouhaowang.net/gongsi/article_78234.html',
                'https://www.kouhaowang.net/qiye/article_78168.html',
                'https://www.kouhaowang.net/biaoyu/article_78141.html',
                'https://www.kouhaowang.net/anquanbiaoyu/article_78235.html',
                'https://www.kouhaowang.net/anquan/article_78220.html',
                'https://www.kouhaowang.net/huanbao/article_78062.html',
                'https://www.kouhaowang.net/shequ/article_78034.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
