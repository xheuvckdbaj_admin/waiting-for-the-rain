# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.yxfanwen.com/zhichang/gongzuozongjie/238792.html',
                'https://www.yxfanwen.com/zhichang/huodongzongjie/238648.html',
                'https://www.yxfanwen.com/zhichang/gongzuojihua/238779.html',
                'https://www.yxfanwen.com/zhichang/gongzuofangan/238649.html',
                'https://www.yxfanwen.com/zhichang/gongzuobaogao/234424.html',
                'https://www.yxfanwen.com/zhichang/gongzuocehua/236282.html',
                'https://www.yxfanwen.com/zhichang/shixibaogao/238783.html',
                'https://www.yxfanwen.com/zhichang/cizhibaogao/238798.html',
                'https://www.yxfanwen.com/zhichang/shuzhibaogao/238011.html',
                'https://www.yxfanwen.com/zhichang/zhiyeguihua/236195.html',
                'https://www.yxfanwen.com/hetong/laodong/238603.html',
                'https://www.yxfanwen.com/hetong/goufang/238403.html',
                'https://www.yxfanwen.com/hetong/zulin/238751.html',
                'https://www.yxfanwen.com/hetong/xiaoshou/238784.html',
                'https://www.yxfanwen.com/hetong/shigong/238786.html',
                'https://www.yxfanwen.com/hetong/gongcheng/238781.html',
                'https://www.yxfanwen.com/hetong/chengbao/238698.html',
                'https://www.yxfanwen.com/hetong/jiekuan/238600.html',
                'https://www.yxfanwen.com/jiaoxue/zongjie/238620.html',
                'https://www.yxfanwen.com/jiaoxue/jihua/238789.html',
                'https://www.yxfanwen.com/jiaoxue/fansi/238775.html',
                'https://www.yxfanwen.com/jiaoxue/sheji/238711.html',
                'https://www.yxfanwen.com/jiaoxue/shuokegao/238390.html',
                'https://www.yxfanwen.com/jiaoxue/tingkebaogao/216695.html',
                'https://www.yxfanwen.com/jiaoxue/jiaoan/238797.html',
                'https://www.yxfanwen.com/zhici/yanjianggao/238801.html',
                'https://www.yxfanwen.com/zhici/fayangao/238721.html',
                'https://www.yxfanwen.com/zhici/zhuchici/238662.html',
                'https://www.yxfanwen.com/zhici/zhici/238788.html',
                'https://www.yxfanwen.com/zhici/heci/237980.html',
                'https://www.yxfanwen.com/zhici/daxieci/238778.html',
                'https://www.yxfanwen.com/zhici/ganyan/238093.html',
                'https://www.yxfanwen.com/zhici/ziwojieshao/238791.html',
                'https://www.yxfanwen.com/shuxin/ganxiexin/238767.html',
                'https://www.yxfanwen.com/shuxin/jieshaoxin/237597.html',
                'https://www.yxfanwen.com/shuxin/cizhixin/238695.html',
                'https://www.yxfanwen.com/shuxin/qiuzhixin/238799.html',
                'https://www.yxfanwen.com/shuxin/biaoyangxin/238782.html',
                'https://www.yxfanwen.com/shuxin/daoqianxin/238696.html',
                'https://www.yxfanwen.com/shuxin/jiantaoshu/238804.html',
                'https://www.yxfanwen.com/shuxin/baozhengshu/238777.html',
                'https://www.yxfanwen.com/shuxin/changyishu/238772.html',
                'https://www.yxfanwen.com/shuxin/chengnuoshu/238803.html',
                'https://www.yxfanwen.com/shuxin/jianyishu/238757.html',
                'https://www.yxfanwen.com/shuxin/zerenshu/237756.html',
                'https://www.yxfanwen.com/shuxin/qingjiatiao/238050.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
