# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="yincao_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.yincao.com/fanwen/gongzuozongjie/75267.html',
                'https://www.yincao.com/fanwen/ziwozongjie/75266.html',
                'https://www.yincao.com/fanwen/gongzuojihua/75316.html',
                'https://www.yincao.com/fanwen/huodongzongjie/75265.html',
                'https://www.yincao.com/fanwen/shenqingshu/75292.html',
                'https://www.yincao.com/fanwen/gongzuofangan/75114.html',
                'https://www.yincao.com/fanwen/zhiyeguihua/67344.html',
                'https://www.yincao.com/fanwen/qiuzhixin/75291.html',
                'https://www.yincao.com/fanwen/gerenjianli/75168.html',
                'https://www.yincao.com/fanwen/ziwojianding/75176.html',
                'https://www.yincao.com/hetong/fanben/75100.html',
                'https://www.yincao.com/hetong/fangwuzulin/75313.html',
                'https://www.yincao.com/hetong/goufang/75233.html',
                'https://www.yincao.com/hetong/laodong/75257.html',
                'https://www.yincao.com/hetong/maimai/75296.html',
                'https://www.yincao.com/hetong/shigonghetong/75312.html',
                'https://www.yincao.com/hetong/xiaoshou/75294.html',
                'https://www.yincao.com/hetong/gongcheng/75295.html',
                'https://www.yincao.com/hetong/zhuanrangxieyi/75303.html',
                'https://www.yincao.com/hetong/jiekuan/75146.html',
                'https://www.yincao.com/zhici/ziwojieshao/75304.html',
                'https://www.yincao.com/zhici/yanjianggao/75308.html',
                'https://www.yincao.com/zhici/fayangao/75139.html',
                'https://www.yincao.com/zhici/zhuchigao/75245.html',
                'https://www.yincao.com/zhici/fanwen/75096.html',
                'https://www.yincao.com/zhici/heci/73604.html',
                'https://www.yincao.com/zhici/daxieci/75140.html',
                'https://www.yincao.com/zhici/zhufuyu/75214.html',
                'https://www.yincao.com/zhici/wenhouyu/75039.html',
                'https://www.yincao.com/zhici/ganyan/75285.html',
                'https://www.yincao.com/jiaoxue/zongjie/75228.html',
                'https://www.yincao.com/jiaoxue/jihua/75287.html',
                'https://www.yincao.com/jiaoxue/fansi/75147.html',
                'https://www.yincao.com/jiaoxue/sheji/75261.html',
                'https://www.yincao.com/jiaoxue/shuokegao/74732.html',
                'https://www.yincao.com/jiaoxue/jiaoan/75309.html',
                'https://www.yincao.com/jiaoxue/dushubiji/75307.html',
                'https://www.yincao.com/shuxin/ganxiexin/75305.html',
                'https://www.yincao.com/shuxin/jieshaoxin/74334.html',
                'https://www.yincao.com/shuxin/cizhixin/73726.html',
                'https://www.yincao.com/shuxin/biaoyangxin/75298.html',
                'https://www.yincao.com/shuxin/daoqianxin/75314.html',
                'https://www.yincao.com/shuxin/weiwenxin/75249.html',
                'https://www.yincao.com/shuxin/jiantaoshu/75086.html',
                'https://www.yincao.com/shuxin/baozhengshu/75235.html',
                'https://www.yincao.com/shuxin/changyishu/75125.html',
                'https://www.yincao.com/shuxin/chengnuoshu/75302.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
