import os
import time

from docx import Document
from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn
from pythonProject.创作活动文档.a文心一言爬取.addCookiesPlaywenxin import addCookiesPlaywenxin
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
def sezhiParas(doc, content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5  # 1.5倍行距 单倍行距 1.0


def sezhiTtitle(doc, content):
    if not '年' in content:
        run = doc.add_heading('', 0).add_run(content)  # 应用场景示例标题
        run.font.name = u'宋体'
        run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
        run.font.size = Pt(18)
        # run.font.color.rgb = RGBColor(250,000,000)
        doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER


def sezhiPaper(doc):
    section = doc.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(25.4)
    section.right_margin = Mm(25.4)
    section.top_margin = Mm(25.4)
    section.bottom_margin = Mm(25.4)
    section.header_distance = Mm(12.7)
    section.footer_distance = Mm(12.7)


def markjudge(contentStr):
    if (contentStr.startswith('。') or contentStr.startswith('，') or contentStr.startswith('：')
            or contentStr.startswith('！') or contentStr.startswith('？') or contentStr.startswith('、')
            or contentStr.startswith('；') or contentStr.startswith(')')):
        contentStr = contentStr[1:]
    if contentStr.endswith('，') or contentStr.endswith('、') or contentStr.endswith('(') or contentStr.endswith('{') \
            or contentStr.endswith('[') or contentStr.endswith('·') or contentStr.endswith('——'):
        contentStr = contentStr[0:-1]
    if (not (contentStr.endswith('.') or contentStr.endswith('。') or contentStr.endswith('！')
             or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
             or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
             or contentStr.endswith('：') or contentStr.endswith('；') or contentStr.endswith('：'))):
        contentStr = contentStr
    return contentStr


def deletesingle(contentStr):
    if len(contentStr) == 1:
        contentStr = ''
    return contentStr


paghraDownPath = './删除活动文档段落违禁词.txt'


def deletePragram(paghraDownPath, content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or (word in content and
                              not (content.startswith('1.') or content.startswith('1、')))
    if panduan:
        content = ''
    return content


def merrageLastPrage(i, file_lines, contentStr):
    if i == (len(file_lines) - 1):
        if (not (contentStr.endswith('.') or contentStr.endswith('。') or contentStr.endswith('！')
                 or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
                 or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
                 or contentStr.endswith('；') or contentStr.endswith(';') or contentStr.endswith('；')
                 or contentStr.startswith('（')
                 or contentStr.startswith('2'))):
            contentStr = ''
    if contentStr.endswith('1.') or contentStr.endswith('2.') or contentStr.endswith('3.') \
            or contentStr.endswith('4.') or contentStr.endswith('5.') or contentStr.endswith('6.')\
            or contentStr.endswith('7.') or contentStr.endswith('8.') or contentStr.endswith('9.') \
            or contentStr.endswith('0.'):
        contentStr = ''
    return contentStr


def merrageLastPrageContentStr(i, file_lines, contentStr):
    if i == (len(file_lines) - 1):
        if contentStr.endswith('：'):
            contentStr = ''
    return contentStr
def getNewPage(page,url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)
def run(brower, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page, "https://yiyan.baidu.com/")
    cookies = addCookiesPlaywenxin(cookiesPath)
    # 设置cookies
    brower.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,"https://yiyan.baidu.com/")
    # 设置cookies
    brower.add_cookies(cookies)
    xpathStrChuanzuo = '//div[@class="HjrMd8LI"]/span[@class="KTFDGqJT"]'
    time.sleep(0.5)
    try:
        page.locator(xpathStrChuanzuo).click()
    except Exception as e:
        print(str(e))
    return page
def makeFolder(filePath):
    if  not os.path.exists(filePath):
        os.makedirs(filePath)
        print(filePath,'创建成功！！！')
def getTitle(page1,fileTtitle):
    elements = page1.locator('xpath=//span[@style="cursor: pointer;"]')
    for i in range(0, elements.count()):
        contentStr = elements.nth(i).text_content().strip()
        print(contentStr)
        fileTtitle.write(contentStr + '\n')
def getRunPage(brower, page, cookiesPath):
    run(brower, page, cookiesPath)
    xpathTiwen = '//div[@class="JRWXI4iU"]/textarea[@class="ant-input wBs12eIN"]'
    elements = page.locator(xpathTiwen)
    if elements.count() == 0:
        getRunPage(brower, page, cookiesPath)
    time.sleep(0.5)
def makeWordChatGPT(folderPath, title, content):
    try:
        title = title
        filePathTxt = folderPath + '\\' + title + '.txt'
        filePathDocx = folderPath + '\\' + title + '.docx'
        doc = Document()
        core_properties = doc.core_properties
        core_properties.author = ''
        sezhiPaper(doc)
        sezhiTtitle(doc, title)
        file = open(filePathTxt, 'w', encoding='utf-8')
        file.write(content)
        file.close()
        filer = open(filePathTxt, 'r', encoding='utf-8')
        file_lines = filer.readlines()
        for i, line in enumerate(file_lines):
            contentStr = line.replace('\n', '')
            contentStr = merrageLastPrage(i, file_lines, contentStr)
            contentStr = merrageLastPrageContentStr(i, file_lines, contentStr)
            contentStrA = markjudge(contentStr)
            contentStrB = deletesingle(contentStrA)
            contentStrC = deletePragram(paghraDownPath, contentStrB)
            sezhiParas(doc, contentStrC)
        doc.save(filePathDocx)
        filer.close()
    except:
        print('文档修改错误error')
def getTitleForquchong(playwright: Playwright,title,htmlFolder,cookiesPath):
    brower=playwright.chromium.launch_persistent_context(
        # 指定本机用户缓存地址
        user_data_dir=r"D:\ChromeProfile",
        # 指定本机google客户端exe的路径
        executable_path=r"C:\Program Files\Google\Chrome\Application\chrome.exe",
        # 要想通过这个下载文件这个必然要开  默认是False
        accept_downloads=True,
        # 设置不是无头模式
        headless=False,
        bypass_csp=True,
        slow_mo=10,
        # 跳过检测
        args=['--disable-blink-features=AutomationControlled', '--remote-debugging-port=9222']
    )
    # context=brower.new_context()
    page=brower.new_page()
    page.set_default_timeout(6000)
    getRunPage(brower, page, cookiesPath)
    getNewPage(page, "https://yiyan.baidu.com/")
    # time.sleep(100)
    # xpathStrChuanzuo = '//div[@class="HjrMd8LI"]/span[@class="KTFDGqJT"]'
    # time.sleep(0.5)
    # try:
    #     page.locator(xpathStrChuanzuo).click()
    # except Exception as e:
    #     print(str(e))
    xpathTiwen='//div[@class="JRWXI4iU"]/textarea[@class="ant-input wBs12eIN"]'
    page.locator(xpathTiwen).click()
    page.locator(xpathTiwen).fill(title)
    page.locator(xpathTiwen).press('Enter')
    # page.press('id=kw', 'Enter')
    time.sleep(40)
    xpathTiwen = '//div[@class="FZ3QiFoa"]/div[@class="H7oUCk_o"]'
    content=page.locator(xpathTiwen).nth(0).text_content().strip()
    print(content)
    makeWordChatGPT(htmlFolder, title, content)
    # page.get_by_text("文档", exact=True).click()
    # time.sleep(0.1)
    # getTitle(page, fileTtitle)
    # for i in range(2,40):
    #     page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
    #     page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(str(i))
    #     page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
    #     time.sleep(0.5)
    #     getTitle(page, fileTtitle)

def download_slect(folderPath, file_lines, apiKeyPath, startNumberApi, lastNUmber_api):
    file_apiKey = open(apiKeyPath, 'r', encoding='utf-8')
    api_keys = file_apiKey.readlines()

    file_apiKey.close()
    last = len(file_lines)
    print(last)
    if last >= 0:
        if last >= lastNUmber_api:
            last_chatNUmber = lastNUmber_api
        else:
            last_chatNUmber = last
        chatAn = math.ceil((last_chatNUmber - startNumberApi) / 8)

        t1 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[0].replace('\n', '').strip(), startNumberApi, startNumberApi + chatAn))
        t2 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[1].replace('\n', '').strip(), startNumberApi + chatAn,
        startNumberApi + chatAn * 2))
        t3 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[2].replace('\n', '').strip(), startNumberApi + chatAn * 2,
        startNumberApi + chatAn * 3))
        t4 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[3].replace('\n', '').strip(), startNumberApi + chatAn * 3,
        startNumberApi + chatAn * 4))
        t5 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[4].replace('\n', '').strip(), startNumberApi + chatAn * 4,
        startNumberApi + chatAn * 5))
        t6 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[5].replace('\n', '').strip(), startNumberApi + chatAn * 5,
        startNumberApi + chatAn * 6))
        t7 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[6].replace('\n', '').strip(), startNumberApi + chatAn * 6,
        startNumberApi + chatAn * 7))
        t8 = threading.Thread(target=main_chatGPT, args=(
        file_lines, folderPath, api_keys[7].replace('\n', '').strip(), startNumberApi + chatAn * 7,
        startNumberApi + chatAn * 8))
        # t9 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_keys[8].replace('\n', '').strip(), startNumberApi + chatAn * 8,startNumberApi + chatAn * 9))
        # t10 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_keys[9].replace('\n', '').strip(), startNumberApi + chatAn * 9, last_chatNUmber))

        t1.daemon = 1
        t1.setDaemon(True)
        t2.setDaemon(True)
        t3.setDaemon(True)
        t4.setDaemon(True)
        t5.setDaemon(True)
        t6.setDaemon(True)
        t7.setDaemon(True)
        t8.setDaemon(True)
        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()
        t8.start()
        # t9.start()
        # t10.start()
        # t11.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
        t6.join()
        t7.join()
        t8.join()
        # t9.join()
        # t10.join()
        # t11.join()

def getPassTitle(title,htmlFolder,cookiesPath):
    with sync_playwright() as playwright:
        getTitleForquchong(playwright,title,htmlFolder,cookiesPath)
def mainPassTitle(title,htmlFolder,cookiesPath):
    makeFolder(htmlFolder)

    getPassTitle(title,htmlFolder,cookiesPath)

def main():
    title = '英语pep试卷'
    htmlFolder=r'D:\文档\百度活动文档\百度html\其他网站'
    cookiesPath = r'./baiduCookies1.txt'
    mainPassTitle(title,htmlFolder,cookiesPath)
if __name__ == '__main__':
    main()