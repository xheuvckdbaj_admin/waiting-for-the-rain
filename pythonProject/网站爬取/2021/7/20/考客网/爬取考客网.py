# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.kaoke.com/xuexi/youerjiaoyu/187480.html',
                'https://www.kaoke.com/xuexi/xiaoxueyuwen/187458.html',
                'https://www.kaoke.com/xuexi/xiaoxueshuxue/187466.html',
                'https://www.kaoke.com/xuexi/xiaoxueyingyu/187449.html',
                'https://www.kaoke.com/xuexi/chuzhongyuwen/187408.html',
                'https://www.kaoke.com/xuexi/chuzhongshuxue/187404.html',
                'https://www.kaoke.com/xuexi/chuzhongyingyu/187407.html',
                'https://www.kaoke.com/xuexi/chuzhongyingyu/187407.html',
                'https://www.kaoke.com/xuexi/gaozhongyuwen/187499.html',
                'https://www.kaoke.com/jiaoxue/kejian/185782.html',
                'https://www.kaoke.com/jiaoxue/jiaoan/185705.html',
                'https://www.kaoke.com/jiaoxue/shiti/186392.html',
                'https://www.kaoke.com/jiaoxue/jiaoxuejihua/187507.html',
                'https://www.kaoke.com/jiaoxue/jiaoxuezongjie/186753.html',
                'https://www.kaoke.com/jiaoxue/shuokegao/186340.html',
                'https://www.kaoke.com/jiaoxue/jiaoxuefansi/187476.html',
                'https://www.kaoke.com/kaoshi/youshengxiao/186647.html',
                'https://www.kaoke.com/kaoshi/xiaoshengchu/187495.html',
                'https://www.kaoke.com/kaoshi/zhongkao/187490.html',
                'https://www.kaoke.com/kaoshi/gaokao/187470.html',
                'https://www.kaoke.com/kaoshi/zigekaoshi/151686.html',
                'https://www.kaoke.com/kaoshi/caihuikaoshi/147571.html',
                'https://www.kaoke.com/hetong/fanben/187205.html',
                'https://www.kaoke.com/hetong/fangwuzulin/187414.html',
                'https://www.kaoke.com/hetong/goufang/186793.html',
                'https://www.kaoke.com/hetong/laodong/177155.html',
                'https://www.kaoke.com/hetong/maimai/187500.html',
                'https://www.kaoke.com/hetong/shigong/187512.html',
                'https://www.kaoke.com/hetong/xiaoshou/187505.html',
                'https://www.kaoke.com/hetong/gongcheng/187494.html',
                'https://www.kaoke.com/jianghua/yanjianggao/187416.html',
                'https://www.kaoke.com/jianghua/fayangao/187506.html',
                'https://www.kaoke.com/jianghua/zhuchigao/187374.html',
                'https://www.kaoke.com/jianghua/heci/183421.html',
                'https://www.kaoke.com/jianghua/zhujiuci/185835.html',
                'https://www.kaoke.com/jianghua/daxieci/187487.html',
                'https://www.kaoke.com/jianghua/huanyingci/172543.html',
                'https://www.kaoke.com/jianghua/baogaotihui/186805.html',
                'https://www.kaoke.com/shuxin/ganxiexin/187504.html',
                'https://www.kaoke.com/shuxin/jieshaoxin/186725.html',
                'https://www.kaoke.com/shuxin/cizhixin/187417.html',
                'https://www.kaoke.com/shuxin/biaoyangxin/187483.html',
                'https://www.kaoke.com/shuxin/daoqianxin/187509.html',
                'https://www.kaoke.com/shuxin/weiwenxin/187510.html',
                'https://www.kaoke.com/shuxin/qiuzhixin/187479.html',
                'https://www.kaoke.com/shuxin/jiantaoshu/187498.html',
                'https://www.kaoke.com/fanwen/gongzuozongjie/187491.html',
                'https://www.kaoke.com/fanwen/nianzhongzongjie/186840.html',
                'https://www.kaoke.com/fanwen/gongzuojihua/187493.html',
                'https://www.kaoke.com/fanwen/gongzuofangan/187372.html',
                'https://www.kaoke.com/fanwen/gongzuocehua/186766.html',
                'https://www.kaoke.com/fanwen/huodongcehua/187511.html',
                'https://www.kaoke.com/fanwen/zhiyeguihua/173515.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
