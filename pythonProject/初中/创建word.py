#coding:utf-8
# 导入模块
from docx import Document
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Inches,Pt
from docx.shared import RGBColor
path=r'E:\文档\test'
doc = Document()
core_properties = doc.core_properties
core_properties.author = ''  # 新建文档对象
par1=doc.add_paragraph('')
run1=par1.add_run('标题.txt')
run1.font.color.rgb = RGBColor(255,55,55)
run1.font.name=u'宋体' #设置插入的字体
run1.font.size = Pt(15)
par1.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

par2  = doc.add_paragraph('第一个段落：')

run_ = par2 .add_run(u'（一）扩大人民民主，保证人民当家作主。人民当家作主是社会主义民主政治的本质和核心。'
                     '要健全民主制度，丰富民主形式，拓宽民主渠道，依法实行民主选举、民主决策、民主管理、民主监督，'
                     '保障人民的知情权、参与权、表达权、监督权。支持人民代表大会依法履行职能，善于使党的主张通过法定程序成为国家意志；'
                     '保障人大代表依法行使职权，密切人大代表同人民的联系，'
                     '建议逐步实行城乡按相同人口比例选举人大代表。支持人民政协围绕团结和民主两大主题履行职能，'
                     '推进政治协商、民主监督、参政议政制度建设；把政治协商纳入决策程序，完善民主监督机制，提高参政议政实效。'
                     '坚持各民族一律平等，保证民族自治地方依法行使自治权。推进决策科学化、民主化，完善决策信息和智力支持系统。'
                     '加强公民意识教育，树立社会主义民主法治、自由平等、公平正义理念。')
run_.font.name=u'宋体' #设置插入的字体
run_.font.size = Pt(15)
par2.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
# 左缩进,0.5 英寸
par2.paragraph_format.left_indent = Inches(0.5)
# 右缩进,20 磅
par2.paragraph_format.right_indent = Pt(20)
# 首行缩进
par2.paragraph_format.first_line_indent = Inches(0.5)
par2.paragraph_format.line_spacing = 1.5
# 段前间距
par2.paragraph_format.space_before = Pt(5)
# 段后间距
par2.paragraph_format.space_after = Pt(5)

doc.save(path+'\hello.docx')

