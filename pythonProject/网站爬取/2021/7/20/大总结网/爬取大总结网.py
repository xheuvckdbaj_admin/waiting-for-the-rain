# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.dazongjie.com/gongzuozongjie/geren/19579.html',
                'https://www.dazongjie.com/gongzuozongjie/xiaoshou/19519.html',
                'https://www.dazongjie.com/gongzuozongjie/jiaoshi/19571.html',
                'https://www.dazongjie.com/gongzuozongjie/zhuanzheng/19582.html',
                'https://www.dazongjie.com/gongzuozongjie/yuangong/19580.html',
                'https://www.dazongjie.com/nianzhongzongjie/gongsi/5870.html',
                'https://www.dazongjie.com/nianzhongzongjie/xiaoshou/19359.html',
                'https://www.dazongjie.com/nianzhongzongjie/yuangong/19570.html',
                'https://www.dazongjie.com/nianzhongzongjie/qita/19546.html',
                'https://www.dazongjie.com/shixizongjie/geren/13438.html',
                'https://www.dazongjie.com/shixizongjie/biye/19393.html',
                'https://www.dazongjie.com/shixizongjie/shengchan/17289.html',
                'https://www.dazongjie.com/shixizongjie/dinggang/19379.html',
                'https://www.dazongjie.com/shixizongjie/shixizong/19371.html',
                'https://www.dazongjie.com/shixizongjie/shujia/5948.html',
                'https://www.dazongjie.com/huodongzongjie/jieri/18920.html',
                'https://www.dazongjie.com/huodongzongjie/xiaoyuan/18833.html',
                'https://www.dazongjie.com/huodongzongjie/youxi/10672.html',
                'https://www.dazongjie.com/huodongzongjie/dushu/18856.html',
                'https://www.dazongjie.com/huodongzongjie/gongsi/18848.html',
                'https://www.dazongjie.com/huodongzongjie/huwai/16976.html',
                'https://www.dazongjie.com/junxunzongjie/xuesheng/19273.html',
                'https://www.dazongjie.com/junxunzongjie/yuangong/16232.html',
                'https://www.dazongjie.com/junxunzongjie/junxun/19577.html',
                'https://www.dazongjie.com/junxunzongjie/huwai/19032.html',
                'https://www.dazongjie.com/zongjiebaogao/zicha/13778.html',
                'https://www.dazongjie.com/zongjiebaogao/zhou/18239.html',
                'https://www.dazongjie.com/zongjiebaogao/yuedu/19533.html',
                'https://www.dazongjie.com/zongjiebaogao/jidu/19044.html',
                'https://www.dazongjie.com/zongjiebaogao/bannian/19573.html',
                'https://www.dazongjie.com/zongjiebaogao/diaochabaogao/19408.html',
                'https://www.dazongjie.com/xuexizongjie/geren/17741.html',
                'https://www.dazongjie.com/xuexizongjie/xuesheng/18930.html',
                'https://www.dazongjie.com/xuexizongjie/ganbu/4017.html',
                'https://www.dazongjie.com/xuexizongjie/jiaoshi/18783.html',
                'https://www.dazongjie.com/xuexizongjie/peixun/16582.html',
                'https://www.dazongjie.com/xuexizongjie/xuexixinde/19567.html',
                'https://www.dazongjie.com/gongzuojihua/jiaoxue/19576.html',
                'https://www.dazongjie.com/gongzuojihua/xuexiao/19569.html',
                'https://www.dazongjie.com/gongzuojihua/youeryuan/19574.html',
                'https://www.dazongjie.com/gongzuojihua/geren/19583.html',
                'https://www.dazongjie.com/gongzuojihua/niandu/19581.html',
                'https://www.dazongjie.com/gongzuojihua/yuedu/19575.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
