# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.今日头条.网站选择 import websiteJudeJin
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


def compareTitle(title,content):
    panduan=True
    if "《" in title and "》" in title:
        title=re.search('《(.*?)》',title)[0]
    if title in content:
        panduan=True
    return panduan


def getFirstUrl(folderPath,keyStr,urls,driver):
    try:
        time.sleep(0.1)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span[@class="text-ellipsis"]'))
            )
        except:
            time.sleep(1)
        elements=driver.find_elements(By.XPATH,'//a[@class="text-ellipsis text-underline-hover"]')
        for i,element in enumerate(elements):
            if i <10:
                title=element.text
                if mainPanDuanTitle(keyStr, title):
                    print(keyStr + '++++++++' + title)
                    panduan = websiteJudeJin(element, folderPath, driver, keyStr, urls)
                    break
    except:
        time.sleep(5)
        print('driver错误')
        strUrl = 'https://www.toutiao.com/'
        driver.get(strUrl)



def makeFolder(floderPath,fileName):
    print(fileName)
    floderPath=floderPath+'\\'+fileName
    os.makedirs(floderPath)
    return floderPath
def main(folderPath,urls,txtPath,driver,number,last):
        file=open(txtPath,'r',encoding='utf-8')
        lines=file.readlines()
        strUrl = 'https://so.toutiao.com/search?dvpf=pc&source=input&keyword=%s'
        for i,line in enumerate(lines):
            if i >= number and i < last:
                try:
                    print(str(i)+'----',line)
                    try:
                        newUrl=strUrl%line
                        driver.get(newUrl)
                    except:
                        driver = getDriver('jin')
                        newUrl = strUrl % line
                        driver.get(newUrl)
                    if 45==i%50:
                        try:
                            driver.quit()
                            driver = getDriver(str(i))
                            driver.get(strUrl)
                        except:
                            print('error')
                    keyStr=line.replace('\n','')
                    print(keyStr)
                    getFirstUrl(folderPath,keyStr, urls,driver)
                except:
                    print('打开浏览器出错')

def mianjinritoutiao(txtPath,folderPath,wangzhiPath,number,last):
    urls=[]
    file=open(wangzhiPath,'w',encoding='utf-8')
    txtFilePaths = find_file(txtPath)
    txtFilePath = txtPath + '\\all.txt'
    driver = getDriver('jin')
    main(folderPath,urls, txtFilePath, driver,number,last)
    driver.quit()
    for i,url in enumerate(urls):
        file.write(url+'\n')
    file.close()
    return urls
if __name__ == '__main__':
    #标题存放文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'
    folderPath=r'D:\文档\百度活动文档\百度html\其他网站'
    wangzhiPath=r'./wangzhi.txt'
    number=0
    last=100000000000
    mianjinritoutiao(txtPath,folderPath,wangzhiPath,number,last)
