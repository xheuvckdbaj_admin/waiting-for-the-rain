import os
import time

import nose.util

import addLingcookies
import requests
from selenium import webdriver
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    # option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver
def getNewWindow(browser,xpathStr,number):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-原创力下载])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, number).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def keepRename(path,newPath,newtitle):
    files = find_file(path,[])
    if len(files) == 1 :
        if str(files[0]).endswith('doc') or str(files[0]).endswith('DOC')\
                or str(files[0]).endswith('docx') or str(files[0]).endswith('DOCX')\
                or str(files[0]).endswith('pdf') or str(files[0]).endswith('PDF')\
                or str(files[0]).endswith('ppt') or str(files[0]).endswith('PPT')\
                or str(files[0]).endswith('pptx') or str(files[0]).endswith('PPTX'):
            endFile=''
            if str(files[0]).endswith('doc') :
                endFile='.doc'
            elif str(files[0]).endswith('docx'):
                endFile = '.docx'
            elif str(files[0]).endswith('DOC'):
                endFile = '.DOC'
            elif str(files[0]).endswith('DOCX'):
                endFile = '.DOCX'
            elif str(files[0]).endswith('pdf'):
                endFile = '.pdf'
            elif str(files[0]).endswith('PDF'):
                endFile = '.PDF'
            elif str(files[0]).endswith('ppt'):
                endFile = '.ppt'
            elif str(files[0]).endswith('pptx'):
                endFile = '.pptx'
            fileName = newPath + '\\' + newtitle+endFile
            try:
                os.rename(files[0], fileName)
            except:
                os.remove(files[0])
        else:
            time.sleep(5)
            keepRename(path, newPath, newtitle)
    elif len(files) == 0:
        time.sleep(10)
        keepRename(path, newPath, newtitle)
    else:
            print(len(files))
            for file in files:
                print(file)
            e=1/0

def crawing(browser,key,path,newPath,i,numberFirst,pageFirst):
    url='https://max.book118.com/search.html?page=%d&q=%s&order=2&type=%d'
    for number in range(numberFirst,4):
        for page in range(pageFirst,6):
                print('number:',number)
                print(('page:',page))
                print('i:',i)
                newUrl=url%(page,key,number)
                browser.get(newUrl)
                #i初始值为0
                addLingcookies.addCookies(browser, newUrl, './cookies.txt',i)
                # browser.get(newUrl)
                hrefs=[]
                try:
                    elements=browser.find_elements_by_xpath('//ul[@class="list"]/li/div[@class="title"]/a')
                    for element in elements:
                        href=element.get_attribute('href')
                        hrefs.append(href)
                        print(href)
                except:
                    continue

                for hf in hrefs:
                    browser.get(hf)
                    browser.find_element_by_xpath('//ul[@class="operate"]/li/a[@class="btn btn-download"]').click()
                    title=browser.find_element_by_xpath('//div[@class="detail"]/div[@class="title"]/h1').text
                    try:
                        getNewWindow(browser, '//div[@class="layui-layer-content"]/div[@class="btns"]/button', 5)
                    except:
                        continue
                    button=browser.find_element_by_xpath('//div[@class="layui-layer-content"]/div[@class="btns"]/button')
                    if button.text=='立即下载':
                        button.click()
                        print(title)
                        newtitle=str(title).replace('•','')
                        time.sleep(6)
                        keepRename(path, newPath, newtitle)
                    elif button.text=='我知道了':
                        i+=1
                        print(i)
                        crawing(browser, key, path, i, number, page)
                        break
        pageFirst=1


def main():
    path = r"G:\文档\原创力文档"
    newPath=r'G:\文档\原创力改名后的文档'
    browser = set_chrome_pref(path)
    key='分班考试'
    #i初始值为0,numberFirst初始值为1,pageFirst初始值为1
    i=0
    numberFirst=1
    pageFirst=1
    crawing(browser,key,path,newPath,i,numberFirst,pageFirst)
if __name__ == '__main__':
    main()