# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        time.sleep(2)
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.content > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    # getContent(thmlpath, strUrl)
    try:
        getBrowser(browser,strUrl)
        href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
        print(href)
        file_line.write(href+'\n')
        getHtml(thmlpath, href, number, browser,file_line)
    except:
        print('该模块已完毕')

def main(thmlpath):
            number=134000
            browser = webdriver.Chrome()
            strUrls=[
                'https://www.sanmu.com/xindetihui/181656.html',
                'https://www.sanmu.com/shijicailiao/181678.html',
                'https://www.sanmu.com/yingjiyuan/181677.html',
                'https://www.sanmu.com/guizhangzhidu/181623.html',
                'https://www.sanmu.com/dabianzhuang/179646.html',
                'https://www.sanmu.com/qingjiatiao/181396.html',
                'https://www.sanmu.com/zhengming/181477.html',
                'https://www.sanmu.com/tongzhi/181644.html',
                'https://www.sanmu.com/zhuchici/181468.html',
                'https://www.sanmu.com/jieshuoci/181335.html',
                'https://www.sanmu.com/banjiangci/144736.html',
                'https://www.sanmu.com/daoyouci/181632.html',
                'https://www.sanmu.com/gongzuozongjie/181671.html',
                'https://www.sanmu.com/nianzhongzongjie/181499.html',
                'https://www.sanmu.com/gongzuobaogao/176675.html',
                'https://www.sanmu.com/shijianbaogao/181010.html',
                'https://www.sanmu.com/shixibaogao/181672.html',
                'https://www.sanmu.com/gongzuojihua/181665.html',
                'https://www.sanmu.com/huodongjihua/181630.html',
                'https://www.sanmu.com/hetong/181666.html',
                'https://www.sanmu.com/cehuashu/181331.html',
                'https://www.sanmu.com/zhiyeguihua/161099.html',
                'https://www.sanmu.com/ziwojieshao/181620.html',
                'https://www.sanmu.com/zhuanzhengshenqing/181394.html',
                'https://www.sanmu.com/cizhixin/179691.html',
                'https://www.sanmu.com/qiuzhixin/181679.html',
                'https://www.sanmu.com/zijianxin/181618.html',
                'https://www.sanmu.com/jiaoxuesheji/181351.html',
                'https://www.sanmu.com/jiaoxuejihua/181663.html',
                'https://www.sanmu.com/jiaoxuezongjie/181475.html',
                'https://www.sanmu.com/jiaoxuefansi/181626.html',
                'https://www.sanmu.com/jiaoxuexinde/179048.html',
                'https://www.sanmu.com/tingkebaogao/144938.html',
                'https://www.sanmu.com/shuokegao/181393.html',
                'https://www.sanmu.com/jiaoan/181628.html',
                'https://www.sanmu.com/shenqingshu/181668.html',
                'https://www.sanmu.com/baozhengshu/181659.html',
                'https://www.sanmu.com/changyishu/181606.html',
                'https://www.sanmu.com/chengnuoshu/181673.html',
                'https://www.sanmu.com/jianyishu/181660.html',
                'https://www.sanmu.com/jiantaoshu/181664.html',
                'https://www.sanmu.com/ganxiexin/181655.html',
                'https://www.sanmu.com/biaoyangxin/181639.html',
                'https://www.sanmu.com/daoqianxin/181670.html',
                'https://www.sanmu.com/weiwenxin/181622.html',
                'https://www.sanmu.com/yanjianggao/181674.html',
                'https://www.sanmu.com/fayangao/181631.html',
                'https://www.sanmu.com/zhuchigao/181340.html',
                'https://www.sanmu.com/jiayougao/181641.html',
                'https://www.sanmu.com/guangbogao/181627.html',
                'https://www.sanmu.com/jingxuangao/147101.html',
                'https://www.sanmu.com/jianghuagao/181625.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网2'
    main(thmlpath)
