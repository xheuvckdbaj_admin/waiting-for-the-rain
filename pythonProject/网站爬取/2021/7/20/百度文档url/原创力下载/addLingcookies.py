import time
import urllib
import readTxt
from selenium import webdriver
#i初始值为0
def addCookies(browser,url,filePath,i):
        browser.get(url)
        time.sleep(1)
        #i初始值为0
        cookies=readTxt.readTxt(filePath,i)
        for item in cookies.split(';'):
                cookie = {}
                itemname = item.split('=')[0]
                iremvalue = item.split('=')[1]
                cookie['domain'] = '.book118.com'
                cookie['name'] = itemname.strip()
                cookie['value'] = urllib.parse.unquote(iremvalue).strip()
                browser.add_cookie(cookie)
        browser.get(url)
def main():
        url='https://pan.baidu.com/s/1fF59s'
        browser=webdriver.Chrome()
        filePath=r"C:\Users\Administrator\Desktop\cookies.txt"
        addCookies(browser,url,filePath)

if __name__ == '__main__':
        main()