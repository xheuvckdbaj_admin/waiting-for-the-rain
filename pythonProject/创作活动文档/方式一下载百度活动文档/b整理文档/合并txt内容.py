import os

from pythonProject.FindFile import find_file


def merge(folderpath,path):
    txtpaths=find_file(folderpath)
    newTxtPath=path+'\\'+'all.txt'
    paths=[]
    txtfile_line=open(newTxtPath,'w',encoding='utf-8')
    for txtpath in txtpaths:
        if txtpath.endswith('.txt'):
            print(txtpath)
            try:
                file_line=open(txtpath,'r',encoding="utf-8")
                readlines=file_line.readlines()
                for line in readlines:
                    if not line in paths:
                        if not ('试题' in line or '卷子' in line or '高考' in line or '中考' in line or '答案' in line):
                            paths.append(line.replace('2020', '2023').replace('2021', '2023').replace('2022', '2023'))
                file_line.close()
                # os.remove(txtpath)
            except:
                try:
                    file_line = open(txtpath, 'r', encoding="gbk")
                    readlines = file_line.readlines()
                    for line in readlines:
                        if not line in paths:
                            if not ('试题' in line or '卷子' in line or '高考' in line or '中考' in line or '答案' in line):
                                paths.append(line.replace('2020','2023').replace('2021','2023').replace('2022','2023'))
                    file_line.close()
                    # os.remove(txtpath)
                except:
                    print('gbk读不出')
                    continue
    for ph in paths:
        try:
            txtfile_line.write(ph)
        except:
            print('error')
            continue
    txtfile_line.close()
def main():
    folderpath=r'D:\文档\百度付费上传文档\百度关键词标题'
    path=r'D:\文档\百度付费上传文档\百度关键词标题'
    merge(folderpath,path)
if __name__ == '__main__':
    main()