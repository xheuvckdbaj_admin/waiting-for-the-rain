import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_duwenxue(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('h1.article-title')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        a_contents=soup.select('div.article-content > a')
        list_soupa = str(soup.select('div.article-content')[0]).replace(title, keyStr).replace('专题分类:','').replace('&gt;','')
        for aElement in a_contents:
            content=aElement.text
            list_soupa =list_soupa.replace(content,'')
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('读文学下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://m.duwenxue.com/chengyu/15.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_duwenxue(url,htmlPath,keyStr,title)