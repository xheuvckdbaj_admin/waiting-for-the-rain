# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import re
import time
import urllib
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options=webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser=webdriver.Chrome(options=options)
    time.sleep(1)
    return browser

def addCookies(browser, url, filePath):
        browser.get(url)
        time.sleep(1)
        cookies = readTxt.readTxt(filePath)
        for item in cookies.split(';'):
            cookie = {}
            itemname = item.split('=')[0]
            iremvalue = item.split('=')[1]
            cookie['domain'] = '.baidu.com'
            cookie['name'] = itemname.strip()
            cookie['value'] = urllib.parse.unquote(iremvalue).strip()
            browser.add_cookie(cookie)
        browser.get(url)

def getNewWindow(browser,xpathStr,timeNumber):
    n=browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element=WebDriverWait(browser,timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH,xpathStr)
        )
    )
def getTitle(browser,file):
    xpathStr='//span[@class="doc-title"]'
    getNewWindow(browser, xpathStr, 10)
    elements=browser.find_elements(By.XPATH,xpathStr)
    for element in elements:
        title=element.get_attribute('title')
        print(title)
        file.write(title+'\n')

def ClickNextPage(browser,pageNumber):
    #pageNumber最大为1，最小为0
    xpathStr='//ul[@class="el-pager"]/li[@class="number"]'
    browser.find_elements(By.XPATH,xpathStr)[pageNumber].click()
    getNewWindow(browser, xpathStr, 10)
    time.sleep(2)
def saveFile(folderPath,title,driver,keyStr,url,number):
    number += 1
    try:
        if number < 5:
            # driver.find_element(By.XPATH,'div[@class="doc-title normal-screen-mode normal-screen-mode"]').text
            driver.get(url)
            time.sleep(0.5)
            element=driver.find_element(By.XPATH, '//div[@class="load-more-link"]')
            text=driver.find_element(By.XPATH, '//div[@class="load-more-link"]/div/span[@class="text"]').get_attribute('innerHTML').replace('\n','').replace('        ','').replace('    ','')
            print(text)
            if  not '剩余0%未加载' in text:
                try:
                    element.click()
                except:
                    time.sleep(0.5)
                    element.click()
            n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
            driver.switch_to.window(n[-1])
            time.sleep(0.5)
            try:
                element = WebDriverWait(driver, 5).until(
                    EC.presence_of_element_located((By.XPATH, '//div[@class="ql-editor"]'))
                )
            except:
                time.sleep(0.1)
            td = driver.find_element(By.XPATH, '//div[@class="ql-editor"]')
            list_soupa = td.get_attribute('innerHTML').replace(title,keyStr)
            title = title.replace(' - ', '')
            thmlpath = folderPath + '\\' + keyStr + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        else:
            print('该文档不能下载')
    except:
        saveFile(folderPath, title, driver, keyStr, url,number)
def mainBaiDuVip(folderPath,cookiesPath,title,keyStr,url):
    if 'wenku.baidu.com' in url:
        driver = getDriver('baidu')
        addCookies(driver, url, cookiesPath)
        title = re.sub('\d+篇', '', title)
        title=re.sub('【.*?】','',title)
        title = title.replace('（', '').replace('）', '').replace('）', '')
        number=0
        saveFile(folderPath,title,driver,keyStr,url,number)
        driver.quit()



if __name__ == '__main__':
    folderPath = r'D:\文档\百度活动文档\中介'
    cookiesPath = './baiduCookies1.txt'
    title='kaixin'
    keyStr='hhh'
    url='https://wenku.baidu.com/view/488f743049649b6648d747ee?fr=hp_sub'
    mainBaiDuVip(folderPath,cookiesPath,title,keyStr,url)