# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        #url='http://m.pincai.com/article/2300000.htm'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('.Text')[0])
        print(title)
        #print(list_soupa)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.lan-left > .lie > ul > li > a')
    for a in a_list:
            time.sleep(10)
            href=a.get('href')
            print(href)
            getContent(thmlpath,href)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.text_list > li > .list_title > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        newUrl='https://www.dawendou.com'+href
        title=a.get('title').replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
        print(title)
        getContent(thmlpath, newUrl,title)

def getHtml2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.box.smalllist > .title > span > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        newUrl='https://www.dawendou.com'+href
        getHtml3(thmlpath, newUrl)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title=str(soup.select('div.content > div > h1')[0].text)
    print(title)
    content = str(soup.select('div.article-article')[0])
    title = title.replace('?', '').replace('？', '').replace('|', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')\
        .replace('&lpar','').replace(';','').replace('&rpar','')
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)
    f.close()
def main(thmlpath,number):
            #猜你喜欢：
            #number=1
            strUrl='https://www.cncoolm.com/gerenjianli/2020/0731/%d.html'
            for i in range(number,number+10):
                print('----------------',i)
                try:
                    newUrl=strUrl%i
                    getHtml(thmlpath, newUrl)
                except:
                    print('错误')
                    continue


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    number=-9
    last=706684
    for i in range(0,1000000):
        number+=10
        main(thmlpath,number)
