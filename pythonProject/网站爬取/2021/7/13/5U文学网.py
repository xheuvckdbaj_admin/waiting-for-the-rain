# -*- coding:utf-8 -*-
import re


import requests

from bs4 import BeautifulSoup

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #time.sleep(10)
            #url='http://www.beiku.com/xieyishu/article_145325.html'
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#contentText')[0])
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.e2 > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.text
            newUrl='http://www.lw54.com'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    print(strUrl)
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.ls_list_li.list_li.ts  h3 > a')

    for i in range(0,len(a_list)):
        href=a_list[i].get('href')
        title=a_list[i].get('title').replace('<b>','').replace('</b>','')
        newUrl='http://www.5utours.com'+href
        getContent(thmlpath, newUrl,title)

def getHtml2(thmlpath, strUrl):
    getHtml3(thmlpath, strUrl)
    try:

        #strUrl='https://www.beiku.com/xieyishu/'
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('.pnum > ul > a')
        hrefStr=a_list[len(a_list)-2].get('href')
        numStr=re.match('(.*?)_(.*?).html',hrefStr).group(2)
        href=hrefStr.split(numStr)[0]+'%d.html'
        for i in range(2,int(numStr)+1):
            newHref='http://www.5utours.com'+href%i
            getHtml3(thmlpath, newHref)
    except:
        print('error')
def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.fr.lm_head_r > li > a')
    for i in range(0, len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        newUrl='http://www.5utours.com'+href
        getHtml2(thmlpath, newUrl)
def main(thmlpath):
            number=1
            strUrl='http://www.5utours.com/fanwen/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'D:\文档\5U文学网'
    main(thmlpath)
