# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('#content')[0]).replace('<br/>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.liew1.essayblock > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    h3_list = soup.select('div.introli > h3')
    for i,h3 in enumerate(h3_list):
        href=h3.select('a')[-1].get('href')
        newUrl='http://yanjiang.com.cn'+href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.dede_pages.mar-t10 > ul.pagelist > li > a')[-1].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    url=strUrl+href.replace(href.split('_')[-1],'')
    for i in range(1,pageNumber+1):
        newUrl=url+str(i)+'.html'
        getListUrl(thmlpath, newUrl)

def main(thmlpath):

                url='http://yanjiang.com.cn/ziliao/'
                getPageUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
