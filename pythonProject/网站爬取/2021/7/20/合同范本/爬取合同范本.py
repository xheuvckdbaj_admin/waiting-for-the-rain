# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="shangxiaye"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.pinda.net.cn/fangchanhetong/57604.html',
                'https://www.pinda.net.cn/jinronghetong/57522.html',
                'https://www.pinda.net.cn/jiekuanhetong/57605.html',
                'https://www.pinda.net.cn/maimaihetong/57658.html',
                'https://www.pinda.net.cn/chengbaohetong/57656.html',
                'https://www.pinda.net.cn/jingyinghetong/57464.html',
                'https://www.pinda.net.cn/maoyihetong/57545.html',
                'https://www.pinda.net.cn/fuwuhetong/57655.html',
                'https://www.pinda.net.cn/dailihetong/57660.html',
                'https://www.pinda.net.cn/chenglanhetong/57603.html',
                'https://www.pinda.net.cn/gongyonghetong/56956.html',
                'https://www.pinda.net.cn/zulinhetong/57657.html',
                'https://www.pinda.net.cn/yunshuhetong/57659.html',
                'https://www.pinda.net.cn/jijianhetong/57654.html',
                'https://www.pinda.net.cn/cangchuhetong/57453.html',
                'https://www.pinda.net.cn/tuoguanhetong/55094.html',
                'https://www.pinda.net.cn/laodonghetong/57646.html',
                'https://www.pinda.net.cn/jishuhetong/57502.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
