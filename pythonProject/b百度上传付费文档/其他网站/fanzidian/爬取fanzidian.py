import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_fanzidian(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.title > h1')[0].text
        title=re.sub('\d+篇','',title)
        title=re.sub('（(.*)）', '', title)
        list_soupa=str(soup.select('div.content')[0]).replace(title,title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('fanzidian下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.fanzidian.com/jingdian/6569927588.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_fanzidian(url,htmlPath,keyStr,title)