# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import threading
import time
import urllib


from pythonProject.FindFile import find_file
from pythonProject.word转pdf.word转pdf import mainWordZhuanPdf
from pythonProject.创作活动文档.chatGPT自动生成文档.chatGPT自动生成文档 import main_chatGPT
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档 import mainTitleClassifyUploadApi
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档pdf import mainTitleClassifyUploadApiPdf
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档不删除被传过的文档 import \
    mainTitleClassifyUploadApiNodelete
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档不删除被传过的文档pdf import \
    mainTitleClassifyUploadApiNodeletePdf
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传.上传文档 import mainTitleClassify
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传.单个文件上传 import mainTitleClassifyUpload
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传.必应搜索.allWebsitBiYing import main_biying
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传.选择模块获取活动文档标题 import mainTitleClassifyMokuai
from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.allyunxing import main_arrangeDocx
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.shear import shear_dile
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.百度上传文档分类 import panduanName
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.百度上传文档分类2 import panduanNamePdf
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索.必应搜索爬取 import mianBiYing
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
def arrangeWord(folderPath,titleFolderClassify,mediumFolderPath,midFolderPath,newPath,uploadFolder):
    for i in range(0,50):
        if i ==0:
            time.sleep(80)
        fileNumberPaths=[]
        filePaths=find_file(folderPath)
        for filePath in filePaths:
            if '.docx' in filePath and (not '---' in filePath):
                fileNumberPaths.append(filePath)
        if not len(fileNumberPaths)==0:
            # 整理文档
            main_arrangeDocx(folderPath, mediumFolderPath, midFolderPath, newPath)
            # 分类文档
            shear_dile(newPath, newPath)
            # mainWordZhuanPdf(newPath)
            panduanName(titleFolderClassify, uploadFolder, newPath)
            time.sleep(5)
def uploadWord(folderPath, cookiesPath,panduanFilePath):
    for i in range(0,25):
        time.sleep(10)
        if i ==0:
            time.sleep(90)
        mainTitleClassifyUploadApiNodelete(folderPath, cookiesPath, panduanFilePath)

def download_api(titleTestFolder,folderPath,wangzhiPath,titleFolder,apiKeyPath,titleNumber,startNumberBiying,startNumberApi,lastNUmber_api):
    fileName = str(titleNumber)
    if titleNumber >= 10:
        fileName = 'a' + fileName
    titleFilePath = titleFolder + '\\' + fileName + '.txt'
    fileTest = open(titleTestFolder + '\\' + 'biying' + '.txt', 'w', encoding='utf-8')
    fileTxt = open(titleFilePath, 'r', encoding='utf-8')
    file_apiKey=open(apiKeyPath,'r',encoding='utf-8')
    api_key=file_apiKey.readline().strip()
    lines = fileTxt.readlines()
    last = len(lines)
    print(last)
    an = int((last - startNumberBiying) / 6)
    if last >=lastNUmber_api:
        last_chatNUmber=lastNUmber_api
    else:
        last_chatNUmber=last
    chatAn = int((last_chatNUmber - startNumberApi) / 10)
    # mianBiYing(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying, startNumberBiying + an)
    t1 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying,startNumberBiying + an))
    t2 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying + an,startNumberBiying + an * 2))
    t3 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying + an * 2,startNumberBiying + an * 3))
    t4 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying + an * 3,startNumberBiying + an * 4))
    t5 = threading.Thread(target=mianBiYing, args=(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying + an * 4, startNumberBiying + an * 5))
    t6 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, startNumberBiying + an * 5, last))

    # t7 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi, startNumberApi+chatAn))
    # t8 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi+chatAn, startNumberApi+chatAn*2))
    # t9 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi+chatAn*2, startNumberApi+chatAn*3))
    # t10 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi+chatAn*3, startNumberApi+chatAn*4))
    # t11 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi+chatAn*4, last_chatNUmber+chatAn*5))
    # t1 = threading.Thread(target=main_chatGPT,args=(titleFilePath, folderPath, api_key, startNumberApi+chatAn*5, startNumberApi + chatAn*6))
    # t2 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi + chatAn*6, startNumberApi + chatAn * 7))
    # t3 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi + chatAn * 7, startNumberApi + chatAn * 8))
    # t4 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_key, startNumberApi + chatAn * 8, startNumberApi + chatAn * 9))
    # t5 = threading.Thread(target=main_chatGPT,args=(titleFilePath, folderPath, api_key, startNumberApi + chatAn * 9, last_chatNUmber))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    # t7.start()
    # t8.start()
    # t9.start()
    # t10.start()
    # t11.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()
    # t7.join()
    # t8.join()
    # t9.join()
    # t10.join()
    # t11.join()
def download_biying(titleTestFolder,folderPath,wangzhiPath,titleFolder,apiKeyPath,titleNumber,startNumberBiying,startNumberApi,lastNUmber_api
                    ,mediumFolderPath,midFolderPath,newPath,uploadFolder,titleFolderClassify,cookiesPath,panduanFilePath):
    fileName = str(titleNumber)
    if titleNumber >= 10:
        fileName = 'a' + fileName
    titleFilePath = titleFolder + '\\' + fileName + '.txt'
    fileTest = open(titleTestFolder + '\\' + 'biying' + '.txt', 'w', encoding='utf-8')
    fileTxt = open(titleFilePath, 'r', encoding='utf-8')
    file_apiKey = open(apiKeyPath, 'r', encoding='utf-8')
    api_keys = file_apiKey.readlines()
    lines = fileTxt.readlines()
    last = len(lines)
    print(last)
    if last >= 5:
        an = int((last - startNumberBiying) / 6)
        if last >= lastNUmber_api:
            last_chatNUmber = lastNUmber_api
        else:
            last_chatNUmber = last
        chatAn = int((last_chatNUmber - startNumberApi)/8)

        t1 = threading.Thread(target=main_chatGPT,args=(lines, folderPath, api_keys[0].replace('\n','').strip(), startNumberApi, startNumberApi + chatAn))
        t2 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[1].replace('\n','').strip(), startNumberApi + chatAn, startNumberApi + chatAn * 2))
        t3 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[2].replace('\n','').strip(), startNumberApi + chatAn * 2, startNumberApi + chatAn * 3))
        t4 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[3].replace('\n','').strip(), startNumberApi + chatAn * 3, startNumberApi + chatAn * 4))
        t5 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[4].replace('\n','').strip(), startNumberApi + chatAn * 4, startNumberApi + chatAn * 5))
        t6 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[5].replace('\n', '').strip(), startNumberApi + chatAn * 5, startNumberApi + chatAn * 6))
        t7 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[6].replace('\n', '').strip(), startNumberApi + chatAn * 6,startNumberApi + chatAn * 7))
        t8 = threading.Thread(target=main_chatGPT, args=(lines, folderPath, api_keys[7].replace('\n', '').strip(), startNumberApi + chatAn * 7,startNumberApi + chatAn * 8))
        # t9 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_keys[8].replace('\n', '').strip(), startNumberApi + chatAn * 8,startNumberApi + chatAn * 9))
        # t10 = threading.Thread(target=main_chatGPT, args=(titleFilePath, folderPath, api_keys[9].replace('\n', '').strip(), startNumberApi + chatAn * 9, last_chatNUmber))

        t12 = threading.Thread(target=arrangeWord,args=(folderPath, titleFolderClassify, mediumFolderPath, midFolderPath, newPath, uploadFolder))
        t13 = threading.Thread(target=uploadWord,args=(uploadFolder, cookiesPath, panduanFilePath))

        t1.daemon = 1
        t1.start()
        t2.start()
        t3.start()
        t4.start()
        t5.start()
        t6.start()
        t7.start()
        t8.start()
        # t9.start()
        # t10.start()
        # t11.start()
        t12.start()
        t13.start()

        t1.join()
        t2.join()
        t3.join()
        t4.join()
        t5.join()
        t6.join()
        t7.join()
        t8.join()
        # t9.join()
        # t10.join()
        # t11.join()
        t12.join()
        t13.join()



def mokuaiUpload(titleNumber,panduanFilePath):
    titleFolder = r'D:\文档\百度活动文档6\百度活动文档标题'
    titleFolderClassify = r'D:\文档\百度活动文档6\用来分类文档标题'
    cookiesPath = './baiduCookies.txt'
    # 爬取标题
    mainTitleClassifyMokuai(titleFolder, titleFolderClassify, cookiesPath, titleNumber)
    # 下载文档
    titleTestFolder = r'D:\文档\百度活动文档6\标题算法测试必应'
    makeFolder(titleTestFolder)
    folderPath = r'D:\文档\百度活动文档6\百度html\其他网站'
    wangzhiPath = r'./wangzhi.txt'
    apiKeyPath = r'./api_key.txt'

    mediumFolderPath = r'D:\文档\百度活动文档6\中介'
    midFolderPath = r'D:\文档\自查报告网url'
    newPath = r'D:\文档\百度活动文档6\修改后百度活动文档'
    uploadFolder = r'D:\文档\百度活动文档6\百度活动文档上传'
    startNumberBiying = 0
    startNumberApi = 0
    lastNUmber_api = 200
    download_biying(titleTestFolder, folderPath, wangzhiPath, titleFolder, apiKeyPath, titleNumber, startNumberBiying,
                    startNumberApi, lastNUmber_api,mediumFolderPath,midFolderPath,newPath,uploadFolder,titleFolderClassify,cookiesPath,panduanFilePath)
    # 整理文档
    main_arrangeDocx(folderPath, mediumFolderPath, midFolderPath, newPath)
    # 分类文档
    shear_dile(newPath, newPath)
    #word转pdf
    # mainWordZhuanPdf(newPath)
    #通过标题文件分类文档
    panduanName(titleFolderClassify, uploadFolder, newPath)
    # # 上传文档
    folderPath = r'D:\文档\百度活动文档6\百度活动文档上传'
    # # cookiesPath = './baiduCookies.txt'
    # # dataNumber = 0
    # # mainTitleClassify(folderPath, cookiesPath, newPath, dataNumber)
    mainTitleClassifyUploadApi(folderPath, cookiesPath,panduanFilePath)
    mainTitleClassifyUploadApi(folderPath, cookiesPath, panduanFilePath)
if __name__ == '__main__':
    panduanFilePath = r'D:\文档\百度活动文档6\百度活动文档上传\账号\账号.txt'
    titleNumbers=[9]
    fileW = open(panduanFilePath, 'w', encoding='utf-8')
    fileW.write('')
    fileW.close()
    for titleNumber in titleNumbers:
        file=open(panduanFilePath,'r',encoding='utf-8')
        content=file.readline()
        file.close()
        if not content=='账号已经传完':
            mokuaiUpload(titleNumber,panduanFilePath)
