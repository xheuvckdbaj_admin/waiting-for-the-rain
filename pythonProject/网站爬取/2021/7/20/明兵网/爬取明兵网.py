# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.mingbing.com/fanwen/gongzuozongjie/44008.html',
                'https://www.mingbing.com/fanwen/gongzuojihua/43995.html',
                'https://www.mingbing.com/fanwen/gongzuofangan/43841.html',
                'https://www.mingbing.com/fanwen/huodongzongjie/43973.html',
                'https://www.mingbing.com/fanwen/shenqingshu/44002.html',
                'https://www.mingbing.com/fanwen/cehuashu/43926.html',
                'https://www.mingbing.com/baogao/shixi/43910.html',
                'https://www.mingbing.com/baogao/shuzhi/43885.html',
                'https://www.mingbing.com/baogao/cizhi/43893.html',
                'https://www.mingbing.com/baogao/shehuishijian/43980.html',
                'https://www.mingbing.com/baogao/shiyongqi/42874.html',
                'https://www.mingbing.com/baogao/xindetihui/43982.html',
                'https://www.mingbing.com/hetong/fanben/43955.html',
                'https://www.mingbing.com/hetong/fangwuzulin/44006.html',
                'https://www.mingbing.com/hetong/goufang/43908.html',
                'https://www.mingbing.com/hetong/laodong/43981.html',
                'https://www.mingbing.com/hetong/maimai/44007.html',
                'https://www.mingbing.com/hetong/shigong/44014.html',
                'https://www.mingbing.com/yanjiang/ziwojieshao/44011.html',
                'https://www.mingbing.com/yanjiang/yanjianggao/43998.html',
                'https://www.mingbing.com/yanjiang/fayangao/44018.html',
                'https://www.mingbing.com/yanjiang/zhuchigao/43974.html',
                'https://www.mingbing.com/yanjiang/zhici/44013.html',
                'https://www.mingbing.com/yanjiang/heci/39667.html',
                'https://www.mingbing.com/jiaoyu/jiaoxuezongjie/43874.html',
                'https://www.mingbing.com/jiaoyu/jiaoxuejihua/43999.html',
                'https://www.mingbing.com/jiaoyu/jiaoxuefansi/44017.html',
                'https://www.mingbing.com/jiaoyu/jiaoxuesheji/43917.html',
                'https://www.mingbing.com/jiaoyu/shuokegao/43372.html',
                'https://www.mingbing.com/jiaoyu/jiaoan/43996.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
