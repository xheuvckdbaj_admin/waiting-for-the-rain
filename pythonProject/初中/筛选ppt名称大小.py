# -*- coding:utf-8 -*-
import re
from datetime import time

import docx
import os



from pythonProject.FindFile import find_file, find_name
from pythonProject.文件剪切 import ctrlx

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def  deleAdvertising(filePath,newFolderPath,newfileNames):
    ppt=filePath.split(".")[len(filePath.split("."))-1]
    #word = wc.Dispatch("Word.Application")
    if ppt=='ppt' or ppt=='pptx' :
        fileName = os.path.basename(filePath)
        if len(fileName) < 11:
            if fileName in newfileNames:
                print(fileName+'与需要修改文件夹里的文档重复，正在删除')
                os.remove(filePath)
            else:
                print(fileName+'文档名称过短，正在移动到需要修改文件夹')
                newPathPpt=newFolderPath+'\\'+fileName
                ctrlx(filePath, newPathPpt)




def main1():
    #os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'E:\文档\ppt'
    newFolderPath = r'E:\文档\需要修改的文件夹'
    filePaths = find_file(oldFolderPath,[])
    newfileNames = find_name(newFolderPath,[])
    for filePath in filePaths:
        if not '$' in filePath:
            print(filePath)
            deleAdvertising(filePath, newFolderPath,newfileNames) # 修改，删除广告
if __name__ == '__main__':

    main1()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
