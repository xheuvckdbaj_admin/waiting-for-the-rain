# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from pythonProject.FindFile import find_file, find_name
from pythonProject.初中.敏感词过滤.minganci import filterWord

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


def markjudge(contentStr,fileName):
    panduan =False
    reg = '\d{5}'
    hh = re.search(reg, contentStr)
    # if len(contentStr)<2 and len(contentStr) > 0:
    #     panduan = True
    #     fileName = fileName + '----' + '内容长度为1'
    if 'VIP' in contentStr:
        panduan=True
        fileName=fileName+'----'+'内容含有VIP'
    if '⼯' in contentStr:
        panduan = True
        fileName = fileName + '----' + '内容含有未识别汉字'
    if '文库' in contentStr:
        panduan = True
        fileName = fileName + '----' + '内容含有文库'
    if not (hh==None):
        fileName = fileName + '----' + '内容含电话或身份证'
    if (contentStr.startswith('。') or contentStr.startswith('，') or contentStr.startswith('：')
        or contentStr.startswith('！') or contentStr.startswith('？') or contentStr.startswith('、')
            or contentStr.startswith('；') or contentStr.startswith(')')) :
        panduan = True
        fileName = fileName + '----' + '内容以标点符号开始'
    if contentStr.startswith('的') and (not contentStr.startswith('的确')):
        panduan = True
        fileName = fileName + '----' + '内容含有的'
    if contentStr.startswith(')'):
        panduan = True
        fileName = fileName + '----' + '内容以)开头'
    if contentStr.endswith('，') or contentStr.endswith('、') or contentStr.endswith('(') or contentStr.endswith('{') \
        or contentStr.endswith('[') or contentStr.endswith('·') or contentStr.endswith('——'):
        panduan = True
        fileName = fileName + '----' + '内容以句中标点符号结束'
    # if len(contentStr) >100 and (not (contentStr.endswith('.') or contentStr.endswith('。')  or contentStr.endswith('！')
    #                                   or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
    #                                   or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
    #                                   or contentStr.endswith('：'))):
    #     print(contentStr)
    #     panduan = True
    #     fileName = fileName + '----' + '内容没有标点符号结束'
    return fileName
def lastMarkjudge(contentStr,fileName):
    # if (not (contentStr.endswith('.') or contentStr.endswith('。')  or contentStr.endswith('！')or contentStr.endswith(')')
    #                                   or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
    #                                   or contentStr.endswith('......') or contentStr.endswith('”')
    #          or contentStr.endswith('！') or contentStr.endswith('。') or contentStr.endswith('日')
    #                                   )):
    #     fileName = fileName + '----' + '最后一段内容没有句末标点符号结尾'
    if contentStr.startswith('1、'):
        fileName = fileName + '----' + '最后一段内容还有序号'
    return fileName
def deleAdvertising(filePath, newFolderPath, newfileNames, front, later):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        try:
                file = docx.Document(filePath)
                fileName = os.path.basename(filePath)
                fileName = fileName.replace('.docx','').split('----')[0]
                # print("段落数:" + str(len(file.paragraphs)))
                # print('删除前图形图像的数量：', len(file.inline_shapes))
                str = []
                contentStr=''
                panduan=False
                for i,para in enumerate(file.paragraphs):
                    # 替换关键词，洗稿⼯
                    contentStr=contentStr+para.text
                    fileName = markjudge(para.text, fileName)
                    if '----' in fileName:
                        panduan=True
                        break
                    # if len(file.paragraphs)==(i+1) :
                    #     fileName =lastMarkjudge(contentStr, fileName)
                    #     print(fileName)
                    #     if '----' in fileName:
                    #         panduan = True
                    #         break
                if len(contentStr)<30 :
                    panduan = True
                    fileName = fileName + '----' + '全文内容长度低于100'
                file_name = fileName
                if panduan :
                    if file.save(newFolderPath + '\\' + file_name + '.docx') == None:
                        os.remove(filePath)  # 删除原来修改前的docx文件
                    if file_name in newfileNames:
                        os.remove(filePath)
                else:
                    print('内容没有含有vip')
        except:
            print('错误')
# word.Quit()
def shear_dile(oldFolderPath,newFolderPath, newfileNames):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i,d in enumerate(os.listdir(oldFolderPath)):
                    shear_dile(os.path.join(oldFolderPath, d),newFolderPath, newfileNames)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.docx'):
            print(oldFolderPath)
            deleAdvertising(oldFolderPath, newFolderPath, newfileNames, 0, 0)

def modifyContent(oldFolderPath, newFolderPath):
    newfileNames = find_name(newFolderPath, [])
    paths = r'./doc_replace.txt'
    fileTxt = open(paths, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    shear_dile(oldFolderPath,newFolderPath, newfileNames)

def mainModifyVip(oldFolderPath,newFolderPath):
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    modifyContent(oldFolderPath, newFolderPath)

if __name__ == '__main__':
    oldFolderPath = r'D:\文档\百度活动文档\中介'
    newFolderPath = r'D:\文档\百度活动文档\用来分类文档标题'
    mainModifyVip(oldFolderPath,newFolderPath)
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
