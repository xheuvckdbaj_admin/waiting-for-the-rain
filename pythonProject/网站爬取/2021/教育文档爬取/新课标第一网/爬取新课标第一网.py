# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getContent(thmlpath, strUrl,title):

    requests_text = requests.get(url=strUrl, headers=headers).content
    thmlpath = thmlpath + '\\' + title + '.rar'
    f = open(thmlpath, 'wb')
    f.write(requests_text)
    f.close()

def getHtml(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('tr > td > li > a')[1].get('href')
        uploadUrl='https://www.xkb1.com'+href
        # contentText=re.sub('<span>(.*?)</span>','',content)
        soup1=soup.select('tr')[4]
        title=str(soup1.select('td > a')[0].text)
        title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
        print(title)
        getContent(thmlpath, uploadUrl,title)
    except:
        print('error')
def main(thmlpath):
           #下面是课件范文网小编为您推荐建党100周年讲话体会和心得五篇。
            strUrl='https://www.xkb1.com/plus/download.php?open=0&aid=%d&cid=3'
           #400
            for i in range(30000,223692):
                # try:
                    print(i)
                    newUrl=strUrl%i
                    getHtml(thmlpath, newUrl)
                # except:
                #     print('出错')
                #     continue


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
