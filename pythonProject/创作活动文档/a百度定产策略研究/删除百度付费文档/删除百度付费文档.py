import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay

def getNewPage(page,url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)
def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page, "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,"https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    return page
def makeFolder(filePath):
    if  not os.path.exists(filePath):
        os.makedirs(filePath)
        print(filePath,'创建成功！！！')
def deleteTitle(page1):
    elements = page1.locator("text=删除")
    for i in range(0,elements.count()):
        try:
            elements.nth(0).click()
            time.sleep(0.5)
            page1.get_by_role("button", name="确定").click()
            time.sleep(2)
        except Exception as e:
            print(str(e))

def getTitle(page1,fileTtitle):
    elements = page1.locator('xpath=//span[@style="cursor: pointer;"]')
    for i in range(0, elements.count()):
        contentStr = elements.nth(i).text_content().strip()
        print(contentStr)
        fileTtitle.write(contentStr + '\n')
def getLocator(page,number):
    for i in range(number, 100000000):
        try:
            page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
            page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(str(i))
            page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
            time.sleep(0.5)
            deleteTitle(page)
        except:
            getNewPage(page,"https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
            page.get_by_text("文档", exact=True).click()
            time.sleep(0.1)
            getLocator(page, i+1)
def getTitleForquchong(playwright: Playwright,fileTtitle,cookiesPath):
    brower=playwright.chromium.launch(headless=False)
    context=brower.new_context()
    page=context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    page.get_by_text("文档", exact=True).click()
    time.sleep(0.5)
    page.get_by_text("付费文档", exact=True).click()
    time.sleep(0.1)
    # getTitle(page, fileTtitle)
    getLocator(page,40000)



def getPassTitle(fileTtitle,cookiesPath):
    with sync_playwright() as playwright:
        getTitleForquchong(playwright,fileTtitle, cookiesPath)
def mainPassTitle(passTitleFolder,cookiesPath):
    makeFolder(passTitleFolder)
    titlePassPath=passTitleFolder+'\\'+'titlePass.txt'
    fileTtitle=open(titlePassPath,'w',encoding='utf-8')
    getPassTitle(fileTtitle, cookiesPath)
    fileTtitle.close()
def main():
    passTitleFolder = r'D:\文档\百度活动文档\百度上传过的标题'
    cookiesPath = r'./baiduCookies1.txt'
    mainPassTitle(passTitleFolder,cookiesPath)
if __name__ == '__main__':
    main()