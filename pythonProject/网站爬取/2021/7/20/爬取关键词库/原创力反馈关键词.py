# -*- coding:utf-8 -*-

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
filePath=r".\cookies.txt"
def set_chrome_pref(path):
    # prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    # option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver
def getNewWindow(browser,xpathStr,number):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, number).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def getFile(browser,path):
            browser.get('https://max.book118.com/guarantee.html')
            # getNewWindow(browser, '//div[@class="hot"]/div[@class="hd"]/div[@class="nav"]/a[@id="needs"]', 100)
            # browser.find_element_by_xpath('//div[@class="hot"]/div[@class="hd"]/div[@class="nav"]/a[@id="needs"]').click()
            # getNewWindow(browser, '//div[@class="page"]/ul[@class="paging"]/li/a', 100)
            # browser.find_elements_by_xpath('//div[@class="page"]/ul[@class="paging"]/li/a')[-2].click()
            time.sleep(1)
            for i in range(0,2093):
                try:
                    newPath=path+'\\'+str(time.time())+'.txt'
                    file_line=open(newPath,'w')

                    getNewWindow(browser, '//div[@class="page"]', 100)
                    browser.find_elements_by_xpath('//div[@class="page"]/ul[@class="paging"]/li/a[@class="next"]')[0].click()
                    getNewWindow(browser, '//tbody/tr/td[@class="col-keywords"]/a', 100)
                    elements=browser.find_elements_by_xpath('//tbody/tr/td[@class="col-keywords"]/a')
                    for ele in elements:
                        try:
                            title=ele.get_attribute('title')
                            print(title)
                            file_line.write(title+'\n')
                        except:
                            print('保存失败')
                            continue
                    file_line.close()
                except:
                    print('cuowu')
                    continue
def main():
    path=r'E:\关键词'
    browser = set_chrome_pref(path)
    getFile(browser,path)
if __name__ == '__main__':
    main()