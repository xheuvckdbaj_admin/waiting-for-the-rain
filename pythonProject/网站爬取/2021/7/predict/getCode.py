import torch
from torch.autograd import Variable
import utils
import dataset
import os
from PIL import Image

import crnn
import params
import glob
import argparse




image_path = './1.jpg'
def getCode(image_path):
    model_path = './netCRNN_91_3750.pth'
    # net init
    nclass = len(params.alphabet) + 1
    model = crnn.CRNN(params.imgH, params.nc, nclass, params.nh)
    if torch.cuda.is_available():
        model = model.cuda()

    # load model
    print('loading pretrained model from %s' % model_path)
    if params.multi_gpu:
        model = torch.nn.DataParallel(model)
    model.load_state_dict(torch.load(model_path, map_location='cpu'))

    converter = utils.strLabelConverter(params.alphabet)

    transformer = dataset.resizeNormalize((100, 32))

    image_list = glob.glob(image_path+'/*')

    acc_num = 0
    image = Image.open(image_path).convert('L')
    image = transformer(image)
    if torch.cuda.is_available():
        image = image.cuda()
    image = image.view(1, *image.size())
    image = Variable(image)
    model.eval()
    preds = model(image)
    _, preds = preds.max(2)
    preds = preds.transpose(1, 0).contiguous().view(-1)
    preds_size = Variable(torch.LongTensor([preds.size(0)]))
    raw_pred = converter.decode(preds.data, preds_size.data, raw=True)
    sim_pred = converter.decode(preds.data, preds_size.data, raw=False)
    # print('%-20s => %-20s' % (raw_pred, sim_pred))
    print(sim_pred)
if __name__ == '__main__':
    image_path = r'D:\验证码\1632740132.3229778.jpg'
    getCode(image_path)

