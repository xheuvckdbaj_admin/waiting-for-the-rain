# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #url='http://m.pincai.com/article/2300000.htm'
        try:
            requests_text = requests.get(url=url, headers=headers,verify=False).content

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#zoomtext')[0])
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#AListBox > ul > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.get('title')
            newUrl='https:'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.infoco3 > ul > li > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        title=a.text
        getContent(thmlpath, href,title)


def getHtml2(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers,verify=False).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.left-item > li > .la > h3 > a')
    for i in range(0, len(a_list)):
        a = a_list[i]
        href = a.get('href').replace('..','')
        title=a.text.replace('?','').replace('？','').replace('|','')\
                    .replace('”','').replace('*','').replace('/','').replace('\\','')
        newUrl='https://www.lztai.com'+href
        getContent(thmlpath, newUrl,title)

def getHtml(thmlpath, strUrl):
    getHtml2(thmlpath, strUrl)
    newUrl=strUrl+'index_%d.html'
    for i in range(2,6700):
        href=newUrl%i
        getHtml2(thmlpath, href)

def main(thmlpath):
            strmap=['fanwen/','shiyongwen/']
            for string in strmap:

                strUrl='https://www.lztai.com/'
                newUrl=strUrl+string
                getHtml(thmlpath, newUrl)


if __name__ == '__main__':
    thmlpath = r'D:\文档\励志台'
    main(thmlpath)
