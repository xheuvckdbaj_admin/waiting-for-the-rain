# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.conbody')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.title > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.data-sitemapli > ul > li > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.fwwang.cn'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('error')
            continue
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    pageContent = soup.select('div.xy.Baidu_paging_indicator > span')[0].text
    allPage=int(re.findall('共(.*?)页/(.*?)条',pageContent)[0][0])
    for i in range(1,allPage+1):
        newUrl=strUrl+str(i)+'/'
        getListUrl(thmlpath, newUrl)
# def getTitleUrl2(thmlpath,strUrl):
#     requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
#     soup = BeautifulSoup(requests_text, 'lxml')
#     a_list = soup.select('div.ct > div > a')
#     for a in a_list:
#         href = a.get('href')
#         newUrl = 'http://www.papers8.cn' + href
#         try:
#             getPageUrl(thmlpath, newUrl)
#         except:
#             getListUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.linkbox > ul > li > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.fwwang.cn'+href
        try:
                getPageUrl(thmlpath, newUrl)
        except:
                getListUrl(thmlpath, newUrl)
def getMoreListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm_center > ul > li > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.fwwang.cn'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('gbk出错')
            continue
def getMorePageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    li = soup.select('div.xy.Baidu_paging_indicator > li')[-3]
    allPage = int(li.select('a')[0].get('href').split('_')[-1].replace('.html', ''))
    url=li.select('a')[0].get('href').replace(allPage,'').replace('.html','')
    for i in range(1,allPage+1):
        newUrl=strUrl+url+str(i)+'.html'
        getMoreListUrl(thmlpath, newUrl)
def getTitopUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.titop > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.fwwang.cn'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('----error')
def getTitleUrl2(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.linkbox > ul > li > a')
    for i,a in enumerate(a_list):
            href=a.get('href')
            newUrl='http://www.fwwang.cn'+href
            try:
                    getMorePageUrl(thmlpath,newUrl)
            except:
                try:
                    getMoreListUrl(thmlpath, newUrl)
                except:
                    getTitopUrl(thmlpath,newUrl)
def main(thmlpath):
            #打开文本图片集
            # 相关推荐文章：
            # 推荐：
            #范文参考网
            #以上就是这篇范文
            #收藏下
            #收藏本页
            #为了方便大家的阅读
            #希望对您有帮助
            #应该跟大家分享
                strUrls=['http://www.fwwang.cn/tags.php']
                for url in strUrls:
                    getTitleUrl(thmlpath,url)
def main2(thmlpath):
    strUrls = ['http://www.fwwang.cn/data/sitemap.html']
    for url in strUrls:
        getTitleUrl2(thmlpath, url)
if __name__ == '__main__':

    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
    main2(thmlpath)
