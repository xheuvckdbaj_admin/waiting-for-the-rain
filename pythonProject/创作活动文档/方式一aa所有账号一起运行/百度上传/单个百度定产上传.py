# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import requests
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.方式一aa所有账号一起运行.b整理文档.allyunxing import mainzhengli
from pythonProject.创作活动文档.方式一aa所有账号一起运行.使用chatGPT生成文档.chatGPT多线程生成文档 import \
    getKeyTitleDing
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.上传单个账号文档 import uploadFileBaidu
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.删除标题文件 import remove_folder
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.剪切没传的文档和删除上传文件夹 import shumove_folder
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.标题选择 import getUploadTitle
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.爬取标题并分类标题 import crawingTitle
from pythonProject.创作活动文档.方式一aa所有账号一起运行.百度上传.百度上传文档分类 import panduanNameDing
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

from pywinauto import Desktop
from pywinauto.keyboard import *


def getNewPage(page, url):
    try:
        page.goto(url)
    except:
        time.sleep(1)
        getNewPage(page, url)


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.get_by_role("button", name="Close").click()
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page


def mainTitleClassifyUpload(playwright: Playwright, cookiesPath, accounts, titleFolder, classifyFolder, passTitlePath,
                            folderNoPassPath, keyFilePath, titleNumber, apiKeyPath, folderPath, threadNumber,
                            newFolderPath, midFolderPath, newPath, uploadFolder, successFloder, noUploadFloder) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    page.wait_for_timeout(500)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    # time.sleep(1000)
    # 剪切未上传的文档
    # uploadFolder上传文档文件夹
    shumove_folder(uploadFolder, noUploadFloder)
    # 删除标题文件夹
    remove_folder(titleFolder, classifyFolder)
    # 爬取标题
    # accounts：不能上传的账号数 # titleFolder 标题存放的文件夹 # classifyFolder 分类标题的文件夹
    # passTitlePath 通过的标题路径 # folderNoPassPath 未开通账号cookies保存路径 # cookiesPath cookies路径
    crawingTitle(page, accounts, titleFolder, classifyFolder, passTitlePath, folderNoPassPath, cookiesPath)
    # 合并并挑选一定数量的标题
    # keyFilePath 排序关键词路径
    # folderpath标题路径
    # titleNumber标题数量
    qualityPath = getUploadTitle(keyFilePath, titleFolder, titleNumber)
    # 多线程生成文档
    # apiKeyPath chatGPTapi存放路径
    # titlePath标题存放路径
    # folderPath文档生成路径
    # threadNumber 线程数量，暂时固定8个
    getKeyTitleDing(apiKeyPath, qualityPath, folderPath, threadNumber)
    # 整理文档
    # oldFolderPathx 需要修改格式文档的文件夹
    # newFolderPath 中介文件夹
    # midFolderPath 不要设置的文件夹，不需要管他
    # newPath 修改格式后最终存放文档的文件夹，也就是文档上传文件夹，最终文件夹
    # mainzhengli(folderPath, newFolderPath, midFolderPath, newPath)
    # 分类文档
    # classifyFolder分类标题路径
    # uploadFolder存放分类后需要上传的活动文档的文件夹
    # newPath需要分类文档的路径
    panduanNameDing(classifyFolder, uploadFolder, newPath)
    # 百度定产上传
    # uploadFolder存放分类后需要上传的活动文档的文件夹
    # successFloder上传成功后的文档路径

    uploadFileBaidu(page, uploadFolder, successFloder)
    return page


def maindenglu(cookiesPath, accounts, titleFolder, classifyFolder, passTitlePath, folderNoPassPath, keyFilePath,
               titleNumber, apiKeyPath, folderPath, threadNumber, newFolderPath, midFolderPath, newPath, uploadFolder,
               successFloder, noUploadFloder):
    with sync_playwright() as playwright:
        return mainTitleClassifyUpload(playwright, cookiesPath, accounts, titleFolder, classifyFolder, passTitlePath,
                                       folderNoPassPath, keyFilePath, titleNumber, apiKeyPath, folderPath, threadNumber,
                                       newFolderPath, midFolderPath, newPath, uploadFolder, successFloder,
                                       noUploadFloder)

    # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


def main():
    folderNoPassPath = r'D:\文档\百度活动文档\未开通账号'
    titleFolder = r'D:\文档\百度活动文档\百度活动文档标题'
    classifyFolder = r'D:\文档\百度活动文档\用来分类文档标题'
    passTitlePath = r'D:\文档\百度活动文档\百度上传成功'
    keyFilePath = r'./排序后的关键词.txt'
    titleNumber = 12
    apiKeyPath = r'./api_key.txt'
    folderPath = r'D:\文档\百度活动文档\修改后百度活动文档'
    threadNumber = 8
    # 中介文件夹
    newFolderPath = r'D:\文档\百度活动文档\中介'
    # 不要设置的文件夹，不需要管他
    midFolderPath = r'D:\文档\百度活动文档\有问题文档'
    # 修改格式后最终存放文档的文件夹，也就是文档上传文件夹，最终文件夹
    newPath = r'D:\文档\百度活动文档\修改后百度活动文档'
    # 存放分类后需要上传的活动文档的文件夹
    uploadFolder = r'D:\文档\百度活动文档\百度活动文档上传'
    # 百度上传成功的文档路径
    successFloder = r'D:\文档\百度活动文档\百度上传成功'
    noUploadFloder = r'D:\文档\百度活动文档\未上传文档'
    # 黑名单账号
    accounts = []
    accountNumber = 112
    cookiesPath = './baiduCookies%s.txt'
    for i in range(1, 208):

        if i > accountNumber:
            print('账号%s上传' % str(i))
            cookiesPath1 = cookiesPath % str(i)
            maindenglu(cookiesPath1, accounts, titleFolder, classifyFolder,
                       passTitlePath, folderNoPassPath, keyFilePath, titleNumber,
                       apiKeyPath, folderPath, threadNumber, newFolderPath, midFolderPath,
                       newPath, uploadFolder, successFloder, noUploadFloder)


if __name__ == '__main__':
    main()
