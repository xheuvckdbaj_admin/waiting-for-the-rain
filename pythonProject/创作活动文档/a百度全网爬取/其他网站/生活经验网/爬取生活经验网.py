import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_shenghuojingyan(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.title > h1')[0].text.replace('\n','').strip()
        # content=str(soup.select('div.excellent_articles_box')[0])
        list_soupa = str(soup.select('div.article_content')[0]).replace(title, title).split('上篇：')[0]
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('生活经验网下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.ryycha.cn/jingyan/229147.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\第一范文网'
    keyStr='kaixin'
    title=''
    mian_shenghuojingyan(url,htmlPath,keyStr,title)