import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getContent(hrefUrl,htmlPath,title,driver):
    try:
        requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        driver.get(hrefUrl)
        td=driver.find_element(By.XPATH,'//pre[@class="replay-info-txt answer_con"]')
        list_soupa = td.get_attribute('innerHTML')
        print(title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def mian_sohu(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.text-title > h1')[0].text
        list_soupa=str(soup.select('article.article')[0]).replace(title,keyStr).split('撰稿/')[0]
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('搜狐下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.sohu.com/a/589107182_114988?edtsign=FB0AAB71EA65A46158E9449B6E28C7F843D83960&edtcode=smdrxf16Yp%2BizJv5GT0Zmg%3D%3D&spm=smpc.home.top-news2.3.1664509282425q3Bi7rL&_f=index_news_2'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\搜狐网'
    keyStr='kaixin'
    title=''
    mian_sohu(url,htmlPath,keyStr,title)