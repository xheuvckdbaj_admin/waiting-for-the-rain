# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.dx-det > div')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.dx-det > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,url):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.dx-list3 > div > a')
        for i,a in enumerate(a_list):
            try:
                href=a.get('href')
                getHtml(thmlpath, href)
            except:
                print('gbk出错')
                continue
    except:
        print('error')

def getPageUrl(thmlpath,url):
    getListUrl(thmlpath, url)
    hrefUrl=url+'list_%d.html'
    for i in range(1,20):
        newUrl=hrefUrl%i
        getListUrl(thmlpath, newUrl)

def getLawTitleUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.dx-list2 > h3 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getPageUrl(thmlpath, href)
def getTitleUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('div.dx-conl > div.dx-lbck3 > p > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getLawTitleUrl(thmlpath, href)
def main(thmlpath):
    #作者：
   url='https://www.xchen.com.cn/'
   getTitleUrl(thmlpath, url)
if __name__ == '__main__':

    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
