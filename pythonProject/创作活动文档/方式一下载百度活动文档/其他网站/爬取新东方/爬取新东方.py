import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_xindongfang(url, htmlPath, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.conL > p.article_title')[0].text
        list_soupa = str(soup.select('div.article_content')[0])
        list_soupa=list_soupa.replace(title, keyStr).split('延伸阅读')[0]
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('新东方下载成功----', keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://cs.xdf.cn/gk/gkgezzdyy/202003/118573256.html'
    htmlPath = r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr = 'kaixin'
    title = ''
    mian_xindongfang(url, htmlPath, keyStr, title)
