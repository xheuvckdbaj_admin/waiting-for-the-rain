def getProportion(key,passTitlePath,noPassTitlePath):
    passNumber=0
    noPassNumber=0
    passFile=open(passTitlePath,'r',encoding='utf-8')
    passFile_lines=passFile.readlines()
    for line in passFile_lines:
        if key in line:
            print(line)
            passNumber=passNumber+1
    noPassFile = open(noPassTitlePath, 'r', encoding='utf-8')
    noPassFile_lines = noPassFile.readlines()
    for line in noPassFile_lines:
        if key in line:
            print(line)
            noPassNumber = noPassNumber + 1
    allNumber=passNumber+noPassNumber
    passZhanbi=passNumber*100/allNumber
    noPassZhanbi = noPassNumber * 100 / allNumber
    print(key+'，一共上传的数量：'+str(allNumber)+'，通过的数量：'+str(passNumber)+
          '，未通过的数量：'+str(noPassNumber)+'，通过的占比：'+str(passZhanbi)+'%'+'，未通过的占比：'+str(noPassZhanbi)+'%')
def main():
    passTitlePath=r'D:\文档\账号通过的标题\6.25\账号通过的标题\账号通过的标题\all.txt'
    noPassTitlePath=r'D:\文档\账号没通过的标题\6.25\账号没通过的标题\账号没通过的标题\all.txt'
    key='演讲稿'
    getProportion(key, passTitlePath, noPassTitlePath)
if __name__ == '__main__':
    main()