import os
import shutil

from pythonProject.FindFile import find_file
def ctrlx(oldFilePath, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
        try:
            shutil.move(oldFilePath, newPath)
        except:
            print('剪切不掉' + oldFilePath)
            try:
                os.remove(oldFilePath)
            except:
                print('删除失败')
def find_file_ding(path,file_list=[]):
    dir = os.listdir(path)
    for i in dir:
        i = os.path.join(path, i)
        if os.path.isdir(i):
            file_list=find_file(i)
        else:
                file_list.append(i)
    return file_list
def shumove_folder(uploadFolder,newPath):
    filePaths=find_file_ding(uploadFolder)
    for filePath in filePaths:
        ctrlx(filePath, newPath)
    dir = os.listdir(uploadFolder)
    for path in dir:
        path = os.path.join(uploadFolder, path)
        if os.path.exists(path):
            shutil.rmtree(path)
            print('成功删除文件夹：',path)

def main():
    # #存放活动文档标题的文件夹
    # titleFolder = r'D:\文档\百度活动文档\百度活动文档标题'
    # classifyFolder = r'D:\文档\百度活动文档\用来分类文档标题'
    #存放分类后需要上传的活动文档的文件夹
    uploadFolder=r'D:\文档\百度活动文档\百度活动文档上传'
    newPath=r'D:\文档\百度活动文档\未上传文档'
    shumove_folder(uploadFolder,newPath)
if __name__ == '__main__':
    main()