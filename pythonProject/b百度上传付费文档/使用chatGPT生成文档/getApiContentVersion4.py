import openai

def getApiContentByTitleAndKey(api_key,prompt):
    try:
        openai.api_key = api_key
        model_engine = "gpt-3.5-turbo"
        # answer_language='zh-CN'
        # try:
        completions = openai.ChatCompletion.create(
            model=model_engine,
            messages=[
                # {'role':'system','content':'你是一个文档生产者。'},
                {'role': 'user', 'content': "请写一篇1200字左右的文章，先给出文章的目录，再根据文章目录依次生成内容，文章的主题是: "+prompt}
            ]
        )

        # message = completions.choices[0].text
        # print(completions)
        result = ''
        for choice in completions.choices:
            result += choice.message.content
        # print(result)
        return result
    except Exception as e:
        return str(e)

def main():
    api_key = "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"
    prompt = "手部防护用品项目投资立项备案申请"
    apiContent=getApiContentByTitleAndKey(api_key,prompt)
    print(apiContent)
if __name__ == '__main__':
    main()