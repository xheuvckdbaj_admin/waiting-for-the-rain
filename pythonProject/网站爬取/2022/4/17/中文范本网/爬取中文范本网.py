# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            contentStr = '#专栏作家#'
            list_soupa = str(soup.select('#newsnr')[0]).split(contentStr)[0]
            title = soup.select('div.nhtAlwBox > dl > dt > h1')[0].text.replace('?', '').replace('？', '').replace('|',
                                                                                                                  '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('li.item > div.item-content > h3.item-title > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.gou999.com'+href
        getHtml(thmlpath, newUrl)

def getPageUrl(thmlpath,strUrl):
    try:
        getListUrl(thmlpath, strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        lastHref = soup.select('div.page > a')[-1].get('href')
        allPageNumber=int(lastHref.split('_')[-1].replace('.html',''))
        url='http://www.gou999.com'+lastHref
        hrefUrl=url.replace(lastHref.split('_')[-1],'')+'%d.html'
        for i in range(2,allPageNumber+1):
            newUrl=hrefUrl%i
            print(newUrl)
            getListUrl(thmlpath, newUrl)
    except:
        getListUrl(thmlpath, strUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title_href=soup.select('div.qj_li > div.qj_li_t > h2 > a')[0].get('href')
    newUrl='http://www.gou999.com'+title_href
    getPageUrl(thmlpath, newUrl)
    a_list = soup.select('div.qj_li > div.qj_li_t > div.qj_li_lm > a')
    for i,a in enumerate(a_list):
            href=a.get('href')
            newUrl='http://www.gou999.com'+href
            print(newUrl)
            getPageUrl(thmlpath, newUrl)

def main(thmlpath):
    url='http://www.gou999.com/'
    getTitleUrl(thmlpath,url)
def mainx(thmlpath):
    url = 'http://www.gou999.com/fanwen/%d.html'
    for i in range(209657, 381029):
        newUrl = url % i
        getHtml(thmlpath, newUrl)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
    mainx(thmlpath)
