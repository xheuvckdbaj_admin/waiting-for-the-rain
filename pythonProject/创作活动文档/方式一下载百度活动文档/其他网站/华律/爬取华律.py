import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_hualv(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        try:
            title=soup.select('div.reader-txt > h1')[0].text
        except:
            title = soup.select('div.det-title > h1')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        try:
            list_soupa=str(soup.select('div.reader-txt')[0]).replace(title,keyStr)
        except:
            list_soupa = str(soup.select('div.det-nr')[0]).replace(title, keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('华律下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.66law.cn/laws/54931.aspx'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_hualv(url,htmlPath,keyStr,title)