# -*- coding:utf-8 -*-
import os
import xml.etree.cElementTree as ET

import docx
from docx import Document

from FindFile import find_file, find_name

'''
找出空白行，并删除
'''
def delete_paragraph(paragraph):
    p = paragraph._element
    'print(paragraph._p, paragraph._element)'
    p.getparent().remove(p)
    paragraph._p = paragraph._element = None
def blank(paragraphs):
    for num, paragraphs in enumerate(paragraphs):
                rIds=[]
                f=paragraphs._element.xml
                root = ET.fromstring(f)
                pictr_str = "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}r"
                pictrs = root.findall(pictr_str)
                image_str = "*/{urn:schemas-microsoft-com:vml}shape/{urn:schemas-microsoft-com:vml}imagedata"
                for pictr in pictrs:
                    # 获得所有<v:imagedata>标签
                    pict = pictr.findall(image_str)
                    if len(pict) > 0:
                        rIds.append(
                            pict[0].attrib['{http://schemas.openxmlformats.org/officeDocument/2006/relationships}id'])
                if len(paragraphs.text.strip()) == 0 and len(rIds)==0:
                    'print(format(num))'
                    paragraphs.clear()  # 清除文字，并不删除段落，run也可以,paragraph.run.clear()
                    # del paragraphs
                    delete_paragraph(paragraphs)
def main():
    filePath=r'D:\文档\爱华文迷网'
    newfilePath=r'D:\文档\需要意愿除去链接'
    file_list=find_name(filePath)

    for fileName in file_list:
        if not '~$' in fileName and 'docx' in fileName:
            print(fileName)
            file = Document(filePath+'\\'+fileName)
            blank(file.paragraphs)
            file.save(newfilePath+'\\'+fileName)
            os.remove(filePath+'\\'+fileName)
if __name__ == '__main__':
    main()