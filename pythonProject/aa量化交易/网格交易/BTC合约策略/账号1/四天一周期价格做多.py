import time
from decimal import Decimal

import requests
from okx import Account, Trade, PublicData, MarketData
from okx.Trade import TradeAPI

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

#获取最大最小值
def getHandL80H(instId, marketAPI):
    zhiyings={}
    result_market_SWAP = marketAPI.get_candlesticks(instId, '', '', '4H', '20')
    hightPrices = []
    lossPrices = []
    print(result_market_SWAP)
    for i in range(0, 20):
        hight0 = Decimal(result_market_SWAP['data'][i][2])
        loss0 = Decimal(result_market_SWAP['data'][i][3])
        hightPrices.append(hight0)
        lossPrices.append(loss0)
    zhiyings['max']=str(max(hightPrices))
    zhiyings['min']=str(min(lossPrices))
    return zhiyings

def getMA(instId, marketAPI, Hnumber):
    millisecondNumber = int(Hnumber) * 3600000
    # （1）获取当前MA5
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '5')
    cnumber5 = 0
    for i in range(0, 5):
        c0 = result_market_SWAP['data'][i][4]
        cnumber5 += Decimal(c0)
    MA5 = Decimal(cnumber5 / 5)
    # （2）获取当前MA10
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '10')
    cnumber10 = 0
    for i in range(0, 10):
        c0 = result_market_SWAP['data'][i][4]
        cnumber10 += Decimal(c0)
    MA10 = Decimal(cnumber10 / 10)
    # （3）获取当前MA20
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '20')
    cnumber20 = 0
    for i in range(0, 20):
        c0 = result_market_SWAP['data'][i][4]
        cnumber20 += Decimal(c0)
    MA20 = Decimal(cnumber20 / 20)
    print(MA20)
    # （4）获取当前MA30
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '30')
    cnumber30 = 0
    for i in range(0, 30):
        c0 = result_market_SWAP['data'][i][4]
        cnumber30 += Decimal(c0)
    MA30 = Decimal(cnumber30 / 30)
    # （5）获取当前MA60
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '60')
    cnumber60 = 0
    for i in range(0, 60):
        c0 = result_market_SWAP['data'][i][4]
        cnumber60 += Decimal(c0)
    MA60 = Decimal(cnumber60 / 60)
    # （5）获取当前MA80
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '80')
    cnumber80 = 0
    for i in range(0, 80):
        c0 = result_market_SWAP['data'][i][4]
        cnumber80 += Decimal(c0)
    MA80 = Decimal(cnumber80 / 80)
    return [MA5, MA10, MA20, MA30, MA60,MA80]


def MakeBTC_SWAP(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber):

    # 获取当前币种合约价格
    marketAPI = MarketData.MarketAPI(api_key, secret_key, passphrase, False, flag)
    result_market_SWAP = marketAPI.get_ticker(instId)
    nowPrice = result_market_SWAP['data'][0]['last']
    print(nowPrice)
    # 获取当前币种80小时最高最低价
    zhiyings=getHandL80H(instId, marketAPI)
    maxPrice=zhiyings['max']
    print(maxPrice)
    minPrice = zhiyings['min']
    print(minPrice)
    # 获取当前币种各个均线价格,MA5,MA10,MA20,MA30,MA60,MA80
    MAs = getMA(instId, marketAPI, '0')
    # # 获取24小时前币种各个均线价格,MA5,MA10,MA20,MA30,MA60
    # beforeMAs = getMA(instId, marketAPI, '24')
    # # 计算出当前趋势走向
    # trend = (MAs[1] - beforeMAs[1]) / MAs[1]
    # if trend > Decimal(0):
    #     # 今日上涨
    #     print(instId, '今日上涨:', trend)
    # else:
    #     # 今日下跌
    #     print(instId, '今日下跌:', trend)
    # 设置杠杆倍数
    accountAPI = Account.AccountAPI(api_key, secret_key, passphrase, False, flag)
    accountAPI.set_leverage('15', 'isolated', instId, '', 'long')
    # 计算出阶梯买入价格和买入张数
    # 计算出买入价格
    buyPrice = ((Decimal(minPrice)+(Decimal(MAs[-1] + Decimal(minPrice)) / 2))/2).quantize(Decimal("0.000000000000000"))
    if Decimal(nowPrice) <= buyPrice:
        buyPrice = (Decimal(Decimal(nowPrice) * Decimal(0.99))).quantize(Decimal("0.000000000000000"))
    # 计算出买入张数
    sheetNumber = int(Decimal(int(Unumber) * 15) / (buyPrice * Decimal(coinNumber)))
    # 计算出止盈止损价格
    # 止盈价格
    surplusPrice = (Decimal(maxPrice) * Decimal(0.990)).quantize(Decimal("0.000000000000000"))
    if sheetNumber > 0:
        # 买进合约（单位：张），并设置止盈止损
        tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
        result_order = tradeAPI.place_order(instId, 'isolated', 'buy', 'limit', str(sheetNumber), '', '', '', 'long',
                                            str(buyPrice),
                                            '', '', str(surplusPrice), str(surplusPrice), '', '',
                                            '', '', '', '', '')
        if result_order['data'][0]['sMsg'] == 'Order placed':
            print('下单成功',
                  '买了%s张%s,价格：%s，止盈价格：%s' % (str(sheetNumber), instId, str(buyPrice), str(surplusPrice)))
        else:
            print('下单失败：%s'%instId)

def runCoin(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber):
    try:

        MakeBTC_SWAP(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber)
    except Exception as e:
        time.sleep(2)
        print('------------------------------报错了', instId)
        print(str(e))
        runCoin(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber)
def mainCoinSwapAll4d():
    flag = "0"  # live trading: 0, demo trading: 1
    api_key = 'dcc90281-f342-4166-ad66-110a941ae3e6'
    secret_key = '9B2084459A62DC52E669482ED8DDBD0A'
    passphrase = '#Xsy103171'
    coinss=[
        {'instId': 'BTC-USDT-SWAP', 'coinNumber': '0.01', 'Unumber': '35'},
        {'instId': 'ETH-USDT-SWAP', 'coinNumber': '0.1', 'Unumber': '20'},
        {'instId': 'TON-USDT-SWAP', 'coinNumber': '1', 'Unumber': '12'},
        {'instId': 'LTC-USDT-SWAP', 'coinNumber': '1', 'Unumber': '12'},
        {'instId': 'EOS-USDT-SWAP', 'coinNumber': '10', 'Unumber': '12'},
        {'instId': 'XRP-USDT-SWAP', 'coinNumber': '100', 'Unumber': '12'},
        {'instId': 'ADA-USDT-SWAP', 'coinNumber': '100', 'Unumber': '12'},
        {'instId': 'DOGE-USDT-SWAP', 'coinNumber': '1000', 'Unumber': '12'},
        {'instId': 'TRX-USDT-SWAP', 'coinNumber': '1000', 'Unumber': '12'},
        {'instId': 'SHIB-USDT-SWAP', 'coinNumber': '1000000', 'Unumber': '12'},
        {'instId': 'XMR-USDT-SWAP', 'coinNumber': '0.1', 'Unumber': '12'},
        {'instId': 'FIL-USDT-SWAP', 'coinNumber': '0.1', 'Unumber': '12'},
        {'instId': 'ETC-USDT-SWAP', 'coinNumber': '10', 'Unumber': '12'},
        {'instId': 'ATOM-USDT-SWAP', 'coinNumber': '1', 'Unumber': '12'},
        {'instId': 'OP-USDT-SWAP', 'coinNumber': '1', 'Unumber': '12'},
        {'instId': 'BCH-USDT-SWAP', 'coinNumber': '0.1', 'Unumber': '12'},
        {'instId': 'APE-USDT-SWAP', 'coinNumber': '0.1', 'Unumber': '12'},
        {'instId': 'XLM-USDT-SWAP', 'coinNumber': '100', 'Unumber': '12'}
    ]
    # (1)查询委托合约
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    result_order = tradeAPI.get_order_list('SWAP', '', '', 'limit', '', '', '', '', '')
    instIds=[]
    for data in result_order['data']:
        # print(data['instId'], data['ordId'])
        instIds.append(data['instId'])
    for i,coins in enumerate(coinss):
        instId = coins['instId']
        if not instId in instIds:
            Unumber = coins['Unumber']
            coinNumber = coins['coinNumber']
            runCoin(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber)


if __name__ == '__main__':
    mainCoinSwapAll4d()
