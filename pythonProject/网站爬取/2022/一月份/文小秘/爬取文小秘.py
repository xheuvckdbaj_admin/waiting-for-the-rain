# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        contentText = str(soup.select('div.con_article.con_main')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
        # contentText=re.sub('<span>(.*?)</span>','',content)
        title=str(soup.select('div.ar_title > h1')[0].text).split('\n')[0]
        print(title)
        title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(contentText)
        f.close()
    except:
        print('获取内容失败！！！')
def getAllUrl(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.list_rwap.bj_mt10 > ul.new.bj_mt20 > li.list_itme2 > a.itme_bt')
        for a in a_list:
            href=a.get('href')
            newUrl = 'http://www.wenxm.cn/' + href
            getHtml(thmlpath, newUrl)
    except:
        print('获取AllUrl失败！！！')
def getPageUrl(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.pnum > ul > a')
        href=a_list[len(a_list)-2].get('href')
        allPageNumber=int(href.split('_')[-1].replace('.html',''))
        url='http://www.wenxm.cn'+ href.split('_')[0] + '_' + href.split('_')[1]
        for i in range(2,allPageNumber+1):
            newUrl=url+str(i)+'.html'
            getAllUrl(thmlpath,newUrl)
    except:
        print('没有多余的页数！！！')
def getTitle_bt(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm_title > h3.fl > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.wenxm.cn'+href
        getAllUrl(thmlpath,newUrl)
        getPageUrl(thmlpath,newUrl)
def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.sub_wrap > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.wenxm.cn'+href
        try:
            getTitle_bt(thmlpath,newUrl)
        except:
            print('没有栏目')
            getAllUrl(thmlpath,strUrl)
            getPageUrl(thmlpath,newUrl)
def main(thmlpath):
            #打开文本图片集
            strUrl='http://www.wenxm.cn/'
            getTitleUrl(thmlpath,strUrl)
if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\文小秘'
    main(thmlpath)
