import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_haoxuegongju(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('h1.hn-box-title')[0].text.replace('\n','').strip()
        # content=str(soup.select('div.excellent_articles_box')[0])
        list_soupa = str(soup.select('div.hn-box-body')[0]).replace(title, title)
        elements=soup.select('div.index')
        for i,element in enumerate(elements):
            content=str(elements[i])
            list_soupa=list_soupa.replace(content,'')
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('好学工具下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.hnzsks.net/chengyu/s572u9u/'
    htmlPath=r'D:\文档\百度活动文档\百度html\第一范文网'
    keyStr='kaixin'
    title=''
    mian_haoxuegongju(url,htmlPath,keyStr,title)