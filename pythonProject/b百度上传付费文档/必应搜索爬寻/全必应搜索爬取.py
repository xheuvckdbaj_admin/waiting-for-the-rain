# -*- coding:utf-8 -*-
import webbrowser

import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from playwright.sync_api import sync_playwright, Playwright
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.b百度上传付费文档.百度搜索爬寻.网站选择 import paywebsiteJude, paywebsiteJudePlay
from pythonProject.创作活动文档.方式一下载百度活动文档.全网爬取.网站选择 import websiteJude
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


def compareTitle(title, content):
    panduan = True
    if "《" in title and "》" in title:
        title = re.search('《(.*?)》', title)[0]
    if title in content:
        panduan = True
    return panduan


def getUrl(link, executable_path):
    driver = webdriver.PhantomJS(executable_path=r"D:\软件\phantomJS\phantomjs-2.1.1-windows\bin\phantomjs.exe")
    driver.get(link)
    current_url = driver.current_url
    driver.close()
    driver.quit()
    return current_url



def insertContent(folderPath, keyStr, urls, page,executable_path):
    time.sleep(0.5)
    page.locator('//input[@class="sb_form_q"]').click()
    page.locator('//input[@class="sb_form_q"]').clear()
    page.locator('//input[@class="sb_form_q"]').fill(keyStr)
    page.locator('//input[@class="sb_form_q"]').press('Enter')
    # time.sleep(2)
    # page.locator('//span[@class="ftrB"]').click()
    # time.sleep(4)
    # page.locator('//div[@class="ftrD"]/a[@role="menuitem"]').nth(1).click()
    time.sleep(2)
    # page.locator('//ul[@class="file_ul_2a1K5"]/li[@class="time_li_3_ArK pop_li_1YiIN pointer_u-S_a"]').nth(0).click()
    # time.sleep(1000)
    elements = page.locator('//li[@class="b_algo"]/h2/a')
    for i in range(0,elements.count()):
        content = elements.nth(i).text_content()
        current_url=elements.nth(i).get_attribute('href')
        print(current_url)
        # current_url = getUrl(hrefUrl, executable_path)
        print(current_url)
        panduan = paywebsiteJudePlay(content, folderPath, keyStr, urls, current_url)
    for i in range(0,10):
        try:
            print('第%d页' % (i + 2))
            time.sleep(1)
            page.locator('//nav[@role="navigation"]/ul/li/a[@class="sb_pagN sb_pagN_bp b_widePag sb_bp "]').nth(-1).click()
            # driver.find_elements(By.XPATH, '//div[@class="page-inner_2jZi2"]/a[@class="n"]')[-1].click()
            time.sleep(2)
            elements = page.locator('//li[@class="b_algo"]/h2/a')
            for i in range(0, elements.count()):
                content = elements.nth(i).text_content()
                current_url = elements.nth(i).get_attribute('href')
                print(current_url)
                # current_url = getUrl(hrefUrl, executable_path)
                print(current_url)
                panduan = paywebsiteJudePlay(content, folderPath, keyStr, urls, current_url)
        except:
            print('这已经是最后一页了')
            break


def makeFolder(floderPath, fileName):
    print(fileName)
    floderPath = floderPath + '\\' + fileName
    os.makedirs(floderPath)
    return floderPath


def run(playwright: Playwright,folderPath, urls, keyStr,executable_path):
    browser = playwright.chromium.launch(headless=True)
    context = browser.new_context()
    context.set_default_timeout(3000)
    page = context.new_page()
    page.goto("https://cn.bing.com/",timeout = 30000)
    try:
        insertContent(folderPath, keyStr, urls, page,executable_path)
    except Exception as e:
        print(str(e))



def mianbiyingplayWright(txtPath, folderPath, wangzhiPath, number,last,executable_path):
    urls = []
    file = open(wangzhiPath, 'w', encoding='utf-8')
    fileTxt = open(txtPath, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    for i, line in enumerate(lines):
        try:
            keyStr = line.replace('\n', '')
            if i >= number and i < last:
                print(str(i) + '必应搜索' + '----', line)
                with sync_playwright() as playwright:
                    # run(playwright)
                    run(playwright,folderPath, urls, keyStr,executable_path)
        except Exception as e:
            print(str(e))
    for i, url in enumerate(urls):
        file.write(url + '\n')
    file.close()
    return urls


if __name__ == '__main__':
    # 标题存放文件夹
    txtPath = r'./标题.txt'
    folderPath = r'D:\文档\百度付费上传文档\百度html'
    wangzhiPath = r'./wangzhi.txt'
    number = 0
    last=10000000
    executable_path=r'D:\软件\phantomJS'+'\\'+r'phantomjs-2.1.1-windows\bin\phantomjs.exe'
    mianbiyingplayWright(txtPath, folderPath, wangzhiPath, number,last,executable_path)
