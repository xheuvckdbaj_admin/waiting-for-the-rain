import os

from pythonProject.FindFile import find_file


def merge(folderpath,path):
    txtpaths=find_file(folderpath)
    newTxtPath=path+'\\'+'all.txt'
    paths=[]
    txtfile_line=open(newTxtPath,'w')
    for txtpath in txtpaths:
        file_line=open(txtpath,'r')
        readlines=file_line.readlines()
        for line in readlines:
            if not line in paths:
                paths.append(line)
        file_line.close()
        os.remove(txtpath)
    for ph in paths:
        txtfile_line.write(ph)
    txtfile_line.close()
def main():
    folderpath=r'E:\文档\自查报告网'
    path=r'E:\文档\自查报告网'
    merge(folderpath,path)
if __name__ == '__main__':
    main()