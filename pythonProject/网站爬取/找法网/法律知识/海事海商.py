# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
    time.sleep(5)
    print(url)
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = soup.select('div.title.clearfix > h1')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','').replace('\n','').replace('\t','')
    #print(title)
    list_soupa = str(soup.select('#arc_container')[0])
    print(title)

    #print('python' + '\r' + 'java' + '\n' + 'java')

    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)
#第四次获取网址
def getLastUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content)
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.model-classify-list > .common-pagination > a')
    href=a_list[len(a_list)-1].get('href')
    number=int(re.match(url+'index_(.*?).html',href))
    newUrl=url+'index_%d.html'
    for page in range(1,number+1):
        time.sleep(10)
        pageUrl=newUrl%page
#第三次获取网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content)
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.model-classify-list > .common-pagination > a')
    href=a_list[len(a_list)-1].get('href')
    number=int(re.match(url+'index_(.*?).html',href).group(1))
    newUrl=url+'index_%d.html'
    for page in range(1,number+1):
        pageUrl=newUrl%page
        requests_text1 = str(requests.get(url=url, headers=headers).content)
        soup1 = BeautifulSoup(requests_text1, 'lxml')
        a_list1 = soup1.select('ul.list > li.list-item > div > a')
        for a in a_list1:
            f=a.get('href')
            getContent(thmlpath, f)



#第二次获取网址
def getHtml(thmlpath, strUrl):
    #strUrl='https://china.findlaw.cn/gongsifalv/gongsishelifa/gongsishelidongtai/'
    requests_text = str(requests.get(url=strUrl, headers=headers).content)
    soup = BeautifulSoup(requests_text, 'lxml')
    div_list = soup.select('div.common-marry-family.margin-top40')
    if len(div_list)==0:
        getUrl(thmlpath, strUrl)
    else:
        for div in div_list:
            href=div.select('.model-classify-list > a.common-load-more')[0].get('href')
            getHtml(thmlpath,href)
#第一次获取网址
def main(thmlpath):
    str = ['jiehun','lihun','caichanfenge/hunhou','zinvfuyang','hunyinjiatingjiufen']
    url='https://china.findlaw.cn/info/hy/'
    for gongsi in str:
        newUrl=url+gongsi
        getHtml(thmlpath, newUrl)


if __name__ == '__main__':
    thmlpath = r'E:\找法网\法律知识\婚姻法'
    main(thmlpath)
