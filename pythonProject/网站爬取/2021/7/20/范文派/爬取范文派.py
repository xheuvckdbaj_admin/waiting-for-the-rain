# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="fwp_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.fanwenpai.com/zongjie/ziwo/182178.html',
                'https://www.fanwenpai.com/zongjie/shixi/183903.html',
                'https://www.fanwenpai.com/zongjie/huodong/183359.html',
                'https://www.fanwenpai.com/zongjie/geren/183836.html',
                'https://www.fanwenpai.com/zongjie/xuexi/179564.html',
                'https://www.fanwenpai.com/zongjie/huiyi/165858.html',
                'https://www.fanwenpai.com/zongjie/yuedu/182011.html',
                'https://www.fanwenpai.com/zongjie/nianzhong/183913.html',
                'https://www.fanwenpai.com/zongjie/nianzhong/183913.html',
                'https://www.fanwenpai.com/zongjie/gongzuo/183959.html',
                'https://www.fanwenpai.com/baogao/kaiti/173159.html',
                'https://www.fanwenpai.com/baogao/diaocha/183764.html',
                'https://www.fanwenpai.com/baogao/guanhougan/183976.html',
                'https://www.fanwenpai.com/baogao/duhougan/183977.html',
                'https://www.fanwenpai.com/baogao/xindetihui/183961.html',
                'https://www.fanwenpai.com/baogao/shiyongqi/109367.html',
                'https://www.fanwenpai.com/baogao/shehuishijian/181080.html',
                'https://www.fanwenpai.com/baogao/cizhi/183962.html',
                'https://www.fanwenpai.com/baogao/shuzhi/183370.html',
                'https://www.fanwenpai.com/baogao/shixi/183969.html',
                'https://www.fanwenpai.com/zhichang/cizhixin/182825.html',
                'https://www.fanwenpai.com/zhichang/jianli/183694.html',
                'https://www.fanwenpai.com/zhichang/qiuzhixin/183954.html',
                'https://www.fanwenpai.com/zhichang/chengnuoshu/183926.html',
                'https://www.fanwenpai.com/zhichang/yaoqinghan/183972.html',
                'https://www.fanwenpai.com/zhichang/daoqianxin/183966.html',
                'https://www.fanwenpai.com/zhichang/biaoyangxin/183960.html',
                'https://www.fanwenpai.com/zhichang/weiwenxin/183980.html',
                'https://www.fanwenpai.com/zhichang/zijianxin/183973.html',
                'https://www.fanwenpai.com/zhichang/tuijianxin/183203.html',
                'https://www.fanwenpai.com/zhichang/jieshaoxin/183645.html',
                'https://www.fanwenpai.com/zhichang/ganxiexin/183970.html',
                'https://www.fanwenpai.com/zhichang/jiantaoshu/183951.html',
                'https://www.fanwenpai.com/jiaoxue/heibanbao/157168.html',
                'https://www.fanwenpai.com/jiaoxue/shouchaobao/157699.html',
                'https://www.fanwenpai.com/jiaoxue/kejian/166753.html',
                'https://www.fanwenpai.com/jiaoxue/jiaoan/183978.html',
                'https://www.fanwenpai.com/jiaoxue/shuokegao/183915.html',
                'https://www.fanwenpai.com/jiaoxue/sheji/183725.html',
                'https://www.fanwenpai.com/jiaoxue/fansi/183950.html',
                'https://www.fanwenpai.com/jiaoxue/jihua/183983.html',
                'https://www.fanwenpai.com/jiaoxue/zongjie/183838.html',
                'https://www.fanwenpai.com/zhici/liuyan/183948.html',
                'https://www.fanwenpai.com/zhici/ganyan/183753.html',
                'https://www.fanwenpai.com/zhici/wenhouyu/183495.html',
                'https://www.fanwenpai.com/zhici/zhufuyu/183780.html',
                'https://www.fanwenpai.com/zhici/daxieci/183928.html',
                'https://www.fanwenpai.com/zhici/heci/183676.html',
                'https://www.fanwenpai.com/zhici/zhuchici/183485.html',
                'https://www.fanwenpai.com/zhici/fayangao/183885.html',
                'https://www.fanwenpai.com/zhici/yanjianggao/183967.html',
                'https://www.fanwenpai.com/zhici/huanyingci/183554.html',
                'https://www.fanwenpai.com/zhici/ziwopingjia/183498.html',
                'https://www.fanwenpai.com/zhici/ziwojianding/183905.html',
                'https://www.fanwenpai.com/zhici/ziwojieshao/183968.html',
                'https://www.fanwenpai.com/hetong/jiekuan/183677.html',
                'https://www.fanwenpai.com/hetong/zhuanrang/183958.html',
                'https://www.fanwenpai.com/hetong/gongcheng/183982.html',
                'https://www.fanwenpai.com/hetong/xiaoshou/183981.html',
                'https://www.fanwenpai.com/hetong/shigong/183974.html',
                'https://www.fanwenpai.com/hetong/maimai/183965.html',
                'https://www.fanwenpai.com/hetong/laodong/183845.html',
                'https://www.fanwenpai.com/hetong/goufanghetong/183646.html',
                'https://www.fanwenpai.com/hetong/fangwuzulin/183971.html',
                'https://www.fanwenpai.com/hetong/fanben/183964.html',
                'https://www.rtsqs.cn/daxuesibaizi/article_14923.html',
                'https://www.rtsqs.cn/daxuesanbaizi/article_14658.html',
                'https://www.rtsqs.cn/daxueerbaizi/article_10972.html',
                'https://www.rtsqs.cn/daxueyibaizi/article_4570.html',
                'https://www.rtsqs.cn/duituanderenshi/article_15480.html',
                'https://www.rtsqs.cn/geshizys/article_14915.html',
                'https://www.rtsqs.cn/zenmexiezys/article_14976.html',
                'https://www.rtsqs.cn/chuzhongzys/article_13615.html',
                'https://www.rtsqs.cn/chuyizys/article_15478.html',
                'https://www.rtsqs.cn/chuerzys/article_15021.html',
                'https://www.rtsqs.cn/chusanzys/article_15295.html',
                'https://www.rtsqs.cn/gaozhongzys/article_14190.html',
                'https://www.rtsqs.cn/gaoyizys/article_14186.html',
                'https://www.rtsqs.cn/gaoerzys/article_14503.html',
                'https://www.rtsqs.cn/gaosanzys/article_13552.html',
                'https://www.rtsqs.cn/daxuezys/article_15472.html',
                'https://www.rtsqs.cn/fanwenzys/article_14461.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
