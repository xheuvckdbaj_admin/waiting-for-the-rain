import numpy


def selectTitle(selectPath,titlePath):
    selectTitlePath=titlePath.replace('all','select')
    file=open(titlePath,'r',encoding='utf-8')
    fileSelect = open(selectTitlePath, 'w', encoding='utf-8')
    fileClass = open(selectPath, 'r', encoding='utf-8')
    file_lines=file.readlines()
    fileClass_lines = fileClass.readlines()
    titles=[]
    for lineClass in fileClass_lines:
        key = lineClass.replace('\n', '')
        for line in file_lines:
            title = line.replace('\n', '')
            if key in title:
                titles.append(title)
    titles=numpy.unique(titles)
    for title in titles:
        print(title)
        fileSelect.write(title + '\n')
    file.close()
    fileSelect.close()
    return titles


def main():
    selectPath=r'./必会通过标题.txt'
    titlePath=r'D:\文档\百度活动文档\百度活动文档标题'+'\\'+'all.txt'
    selectTitle(selectPath, titlePath)
if __name__ == '__main__':
    main()