import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getContent(hrefUrl,htmlPath,title,driver):
    try:
        requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        driver.get(hrefUrl)
        td=driver.find_element(By.XPATH,'//pre[@class="replay-info-txt answer_con"]')
        list_soupa = td.get_attribute('innerHTML')
        print(title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def mian_rainNew(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.LEFT > h1')[0].text
        list_soupa=str(soup.select('div.content-article')[0]).replace(title,title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('腾讯新闻下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://view.inews.qq.com/k/20220112A0D1CK00?web_channel=wap&openApp=false'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\腾讯新闻'
    keyStr='kaixin'
    title=''
    mian_rainNew(url,htmlPath,keyStr,title)