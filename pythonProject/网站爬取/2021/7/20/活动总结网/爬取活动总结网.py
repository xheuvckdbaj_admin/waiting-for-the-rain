# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="other_s"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.huodongzongjie.com/shu/article_54074.html',
                'https://www.huodongzongjie.com/qinzi/article_54069.html',
                'https://www.huodongzongjie.com/qinzi/article_54069.html',
                'https://www.huodongzongjie.com/cxhdzj/article_54059.html',
                'https://www.huodongzongjie.com/zhongqiujie/article_54065.html',
                'https://www.huodongzongjie.com/tuipuzhou/article_54070.html',
                'https://www.huodongzongjie.com/jianzai/article_54068.html',
                'https://www.huodongzongjie.com/shehuishijian/article_54075.html',
                'https://www.huodongzongjie.com/hdzjgs/article_12197.html',
                'https://www.huodongzongjie.com/chuangxian/article_53458.html',
                'https://www.huodongzongjie.com/tuozhan/article_52991.html',
                'https://www.huodongzongjie.com/anquan/article_53232.html',
                'https://www.huodongzongjie.com/aqscy/article_54055.html',
                'https://www.huodongzongjie.com/laodong/article_52697.html',
                'https://www.huodongzongjie.com/xingqu/article_54050.html',
                'https://www.huodongzongjie.com/shu/article_54074.html',
                'https://www.huodongzongjie.com/baogao/article_54027.html',
                'https://www.huodongzongjie.com/geshi/article_12076.html',
                'https://www.huodongzongjie.com/hdzjmb/article_54073.html',
                'https://www.huodongzongjie.com/ankangbei/article_13027.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
