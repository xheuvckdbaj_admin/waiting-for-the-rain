import os
import re
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options  # 手机模式
from selenium.webdriver.common.by import By


def getDriver():
    # 设置手机型号，这设置为iPhone 6
    mobile_emulation = {"deviceName": "iPhone 7"}
    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    # 启动配置好的浏览器
    driver = webdriver.Chrome(options=options)
    return  driver
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getPhoneDriver


def saveFile(thmlpath,url,keyStr,driver,title):
    try:
        print(url)
        # 输入网址
        driver.get(url)
        time.sleep(0.2)
        td = driver.find_element(By.XPATH,'//tbody')
        # td = driver.find_element(By.XPATH,'//body')
        content = td.text
        # print(content)
        list_soupa = td.get_attribute('innerHTML').replace(title,title).split('点击加载更多')[0].split('开通VIP，免费享6亿+内容')[-1].replace('开通VIP','')\
            .replace('点击立即咨询，了解更多详情','').replace('咨询','').replace('商品类型','').replace('品质优享','').replace('VIP专享','').replace('全部最热最新筛选','').split('搜索')[0]
        if not '该文档已被删除' in list_soupa:
            title=title.replace(' - ','')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
    except:
        try:
            print(url)
            # 这里输入网址
            driver.get(url)
            time.sleep(0.2)
            # td = driver.find_element(By.XPATH,'//tbody')
            td = driver.find_element(By.XPATH,'//body')
            content = td.text
            list_soupa = td.get_attribute('innerHTML').replace(title,title).split('点击加载更多')[0].split('开通VIP，免费享6亿+内容')[-1].replace('开通VIP','')\
            .replace('点击立即咨询，了解更多详情','').replace('咨询','').replace('商品类型','').replace('品质优享','').replace('VIP专享','').replace('全部最热最新筛选','').split('搜索')[0]
            if not '该文档已被删除' in list_soupa:
                title = title.replace(' - ', '')
                thmlpath = thmlpath + '\\' + title + '.html'
                f = open(thmlpath, 'w', encoding='utf-8')
                f.write(list_soupa)
        except:
            print('error')

def main_other_baiDu(thmlpath,url,keyStr,title):
    if 'wk.baidu.com' in url or 'wenku.baidu.com' in url:
        phoneUrl = url.split('.html')[0] + '?pcqq.c2c&bfetype=old'
        driver = getDriver()
        title = title.replace('（', '').replace('）', '')
        title = re.sub('\d+篇', '', title)
        saveFile(thmlpath, phoneUrl, title, keyStr, title)
        driver.quit()
if __name__ == '__main__':
    url=r'https://wenku.baidu.com/view/0284dd48497302768e9951e79b89680203d86be9?pcqq.c2c&bfetype=old'
    title='应聘铁路公司自我介绍对比1'
    thmlpath=r'D:\文档\百度活动文档\百度html\其他网站\百度文库'
    keyStr='应聘铁路公司自我介绍对比'
    main_other_baiDu(thmlpath,url,keyStr,title)