# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getCon(soup):
    list_soupa = ''
    contents = soup.select('.content')
    if len(contents) != 0:
        list_soupa = str(soup.select('.content')[0])
    else:
        contents = soup.select('article.baidu_pl')
        if len(contents) != 0:
            list_soupa = str(soup.select('article.baidu_pl')[0])
        else:
            contents = soup.select('#art_cont')
            if len(contents) != 0:
                list_soupa = str(soup.select('#art_cont')[0])
            else:
                contents = soup.select('#content_views')
                if len(contents) != 0:
                    list_soupa = str(soup.select('#content_views')[0])
                else:
                    contents = soup.select('.index-module_articleWrap_2Zphx ')
                    if len(contents) != 0:
                        list_soupa = str(soup.select('.index-module_articleWrap_2Zphx ')[0])
                    else:
                        contents = soup.select('.content-txt')
                        if len(contents) != 0:
                            list_soupa = str(soup.select('.content-txt')[0])
                        else:
                            contents = soup.select('article')
                            if len(contents) != 0:
                                list_soupa = str(soup.select('article')[0])
                            else:
                                contents = soup.select('.article')
                                if len(contents) != 0:
                                    list_soupa = str(soup.select('.article')[0])
                                else:
                                    contents = soup.select('.conwarp')
                                    if len(contents) != 0:
                                        list_soupa = str(soup.select('.conwarp')[0])
                                    else:
                                        contents = soup.select('.mp-article-content-area')
                                        if len(contents) != 0:
                                            list_soupa = str(soup.select('.mp-article-content-area')[0])
                                        else:
                                            contents = soup.select('.wz')
                                            if len(contents) != 0:
                                                list_soupa = str(soup.select('.wz')[0])
                                            else:
                                                contents = soup.select('.mp-article-content-area')

    return list_soupa
def getDriverCon(driver):
    list_soupa = ''
    # contents = soup.select('.content')
    contents=driver.find_elements(By.XPATH,'//div[@class="content"]')
    if len(contents) != 0:
        # list_soupa = str(soup.select('.content')[0])
        td = driver.find_element(By.XPATH,'//div[@class="content"]')
        list_soupa = td.get_attribute('innerHTML')
    else:
        # contents = soup.select('article.baidu_pl')
        contents = driver.find_elements(By.XPATH,'//article[@class="baidu_pl"]')
        if len(contents) != 0:
            # list_soupa = str(soup.select('article.baidu_pl')[0])
            td = driver.find_element(By.XPATH,'//article[@class="baidu_pl"]')
            list_soupa = td.get_attribute('innerHTML')
        else:
            # contents = soup.select('#art_cont')
            contents = driver.find_elements(By.XPATH,'//div[@id="art_cont"]')
            if len(contents) != 0:
                # list_soupa = str(soup.select('#art_cont')[0])
                td = driver.find_element(By.XPATH,'//div[@id="art_cont"]')
                list_soupa = td.get_attribute('innerHTML')
            else:
                # contents = soup.select('#content_views')
                contents = driver.find_elements(By.XPATH,'//div[@id="content_views"]')
                if len(contents) != 0:
                    # list_soupa = str(soup.select('#content_views')[0])
                    td = driver.find_element(By.XPATH,'//div[@id="content_views"]')
                    list_soupa = td.get_attribute('innerHTML')
                else:
                    # contents = soup.select('.index-module_articleWrap_2Zphx ')
                    contents = driver.find_elements(By.XPATH,'//div[@class="index-module_articleWrap_2Zphx "]')
                    if len(contents) != 0:
                        # list_soupa = str(soup.select('.index-module_articleWrap_2Zphx ')[0])
                        td = driver.find_element(By.XPATH,'//div[@class="index-module_articleWrap_2Zphx ')
                        list_soupa = td.get_attribute('innerHTML')
                    else:
                        # contents = soup.select('.content-txt')
                        contents = driver.find_elements(By.XPATH,'//div[@class="content-txt"]')
                        if len(contents) != 0:
                            # list_soupa = str(soup.select('.content-txt')[0])
                            td = driver.find_element(By.XPATH,'//div[@class="content-txt"]')
                            list_soupa = td.get_attribute('innerHTML')
                        else:
                            # contents = soup.select('article')
                            contents = driver.find_elements(By.XPATH,'//article')
                            if len(contents) != 0:
                                # list_soupa = str(soup.select('article')[0])
                                td = driver.find_element(By.XPATH,'//article')
                                list_soupa = td.get_attribute('innerHTML')
                            else:
                                # contents = soup.select('.article')
                                contents = driver.find_elements(By.XPATH,'//div[@class="article"]')
                                if len(contents) != 0:
                                    # list_soupa = str(soup.select('.article')[0])
                                    td = driver.find_element(By.XPATH,'//div[@class="article"]')
                                    list_soupa = td.get_attribute('innerHTML')
                                else:
                                    # contents = soup.select('.conwarp')
                                    contents = driver.find_elements(By.XPATH,'//div[@class="conwarp"]')
                                    if len(contents) != 0:
                                        # list_soupa = str(soup.select('.conwarp')[0])
                                        td = driver.find_element(By.XPATH,'//div[@class="conwarp"]')
                                        list_soupa = td.get_attribute('innerHTML')
                                    else:
                                        # contents = soup.select('.mp-article-content-area')
                                        contents = driver.find_elements(By.XPATH,'//div[@class="mp-article-content-area"]')
                                        if len(contents) != 0:
                                            # list_soupa = str(soup.select('.mp-article-content-area')[0])
                                            td = driver.find_element(By.XPATH,'//div[@class="mp-article-content-area"]')
                                            list_soupa = td.get_attribute('innerHTML')
                                        else:
                                            # contents = soup.select('.wz')
                                            contents = driver.find_elements(By.XPATH,'//div[@class="wz"]')
                                            if len(contents) != 0:
                                                # list_soupa = str(soup.select('.wz')[0])
                                                td = driver.find_element(By.XPATH,'//div[@class="wz"]')
                                                list_soupa = td.get_attribute('innerHTML')
                                            else:
                                                contents = driver.find_elements(By.XPATH,'//div[@class="Text"]')
                                                if len(contents) != 0:
                                                    # list_soupa = str(soup.select('.wz')[0])
                                                    td = driver.find_element(By.XPATH,'//div[@class="Text"]')
                                                    list_soupa = td.get_attribute('innerHTML')
                                                else:
                                                    contents = driver.find_elements(By.XPATH,'//div[@class="maincontent clearfix"]')
                                                    if len(contents) != 0:
                                                        # list_soupa = str(soup.select('.wz')[0])
                                                        td = driver.find_element(By.XPATH,'//div[@class="maincontent clearfix"]')
                                                        list_soupa = td.get_attribute('innerHTML')
                                                    else:
                                                        contents = driver.find_elements(By.XPATH,'//div[@class="para"]')
                                                        if len(contents) != 0:
                                                            # list_soupa = str(soup.select('.wz')[0])
                                                            td = driver.find_element(By.XPATH,'//div[@class="para"]')
                                                            list_soupa = td.get_attribute('innerHTML')
                                                        else:
                                                            contents = driver.find_elements(By.XPATH,'//div[@class="rich_media_content"]')
                                                            if len(contents) != 0:
                                                                # list_soupa = str(soup.select('.wz')[0])
                                                                td = driver.find_element(By.XPATH,
                                                                    '//div[@class="rich_media_content"]')
                                                                list_soupa = td.get_attribute('innerHTML')
                                                            else:
                                                                contents = driver.find_elements(By.XPATH,'//div[@class="contentMain"]')
                                                                if len(contents) != 0:
                                                                    # list_soupa = str(soup.select('.wz')[0])
                                                                    td = driver.find_element(By.XPATH,'//div[@class="contentMain"]')
                                                                    list_soupa = td.get_attribute('innerHTML')
                                                                else:
                                                                    contents = driver.find_elements(By.XPATH,'//div[@class="contentMain"]')

    return list_soupa
def getContent(thmlpath, url,contentTitle,driver):
    try:
        driver.get(url)
        list_soupa=getDriverCon(driver)
        if list_soupa != '':
            print(contentTitle)
            thmlpath=thmlpath+'\\'+contentTitle+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
    except:
        print('error')
    # try:
    #     requests.packages.urllib3.disable_warnings()
    #     requests_text = str(requests.get(url=url, headers=headers,verify=False).content.decode(encoding='utf-8'))
    #     soup = BeautifulSoup(requests_text, 'lxml')
    #     list_soupa=getCon(soup)
    #     if list_soupa!='':
    #         print(contentTitle)
    #         thmlpath=thmlpath+'\\'+contentTitle+'.html'
    #         f = open(thmlpath, 'w', encoding='utf-8')
    #         f.write(list_soupa)
    # except:
    #     try:
    #         requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    #         soup = BeautifulSoup(requests_text, 'lxml')
    #         list_soupa = getCon(soup)
    #         if list_soupa != '':
    #             print(contentTitle)
    #             thmlpath = thmlpath + '\\' + contentTitle + '.html'
    #             f = open(thmlpath, 'w', encoding='utf-8')
    #             f.write(list_soupa)
    #     except:
    #         print('error')


def compareTitle(title,content):
    panduan=True
    if "《" in title and "》" in title:
        title=re.search('《(.*?)》',title)[0]
    if title in content:
        panduan=True
    return panduan


def getTitle(title,url,thmlpath):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.c-container > div > h3.c-title.t.t.tts-title > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        content=a.text
        print(content)
        if compareTitle(title,content):
            getContent(thmlpath, href,content)
def getFirstUrl(title,url,thmlpath,driver):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        driver.get(url)
        time.sleep(5)
        a_titles=driver.find_elements(By.XPATH,'//h3[@class="c-title t t tts-title"]/a')
        titleMap=[]
        hrefMap=[]
        for a_title in a_titles:
            title=a_title.text
            href=a_title.get_attribute("href")
            print(title)
            print(href)
            titleMap.append(title)
            hrefMap.append(href)
        for i,href in enumerate(hrefMap):
            title=titleMap[i]
            getContent(thmlpath, href, title,driver)
    except:
        print('错误')

    # a_list = soup.select('div.c-row.source_1Vdff.wenda-abstract-source_3NRe0 > a')
    # if len(a_list)>0:
    #     a=a_list[0]
    #     href=a.get('href')
    #     getContent(thmlpath, href, title)
def makeFolder(floderPath,fileName):
    print(fileName)
    floderPath=floderPath+'\\'+fileName
    os.makedirs(floderPath)
    return floderPath
def main(thmlpath,txtPath,driver):
        file=open(txtPath,'r',encoding='utf-8')
        lines=file.readlines()
        strUrl = 'https://www.baidu.com/s?wd=%s&pn=%d0'
        for i,line in enumerate(lines):
            title=line.replace('\n','')
            # keyUrl=strUrl%title
            # folderpath = makeFolder(thmlpath, title)
            folderpath=thmlpath
            for i in range(0,2):
                newUrl=strUrl%(title,i)
                print(newUrl)

                getFirstUrl(title, newUrl, folderpath,driver)
                # getTitle(title,newUrl,folderpath,driver)

        # for i in range(0,100000):
        #         strUrl='https://www.baidu.com/s?wd=%s'
        #         getHtml(thmlpath, strUrl,)



if __name__ == '__main__':
    #标题存放文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'
    #全网下载文档存放文件夹
    thmlpath = r'D:\文档\百度活动文档\百度html\全网文档'
    driver = webdriver.Chrome()
    txtFilePaths = find_file(txtPath)
    for txtFilePath in txtFilePaths:
        main(thmlpath,txtFilePath,driver)
