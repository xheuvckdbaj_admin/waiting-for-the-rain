import requests
from bs4 import BeautifulSoup
from selenium import webdriver

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getContent(hrefUrl,htmlPath,title,driver):
    try:
        requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        driver.get(hrefUrl)
        td=driver.find_element(By.XPATH,'//pre[@class="replay-info-txt answer_con"]')
        list_soupa = td.get_attribute('innerHTML')
        print(title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def mianSou(urls,htmlPath):
    driver = getDriver('sou')
    for url in urls:
        hrefUrl = url.replace('\n', '').split('————')[0]
        title = url.replace('\n', '').split('————')[-1]
        getContent(hrefUrl,htmlPath,title,driver)
    driver.quit()

if __name__ == '__main__':
    htmlPath=r''
    urls=['https://wenwen.sogou.com/z/q656707995.htm']
    mianSou(urls,htmlPath)