# -*- coding:utf-8 -*-
import re
from datetime import time

import docx
import os



from pythonProject.FindFile import find_file, find_name

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None
def panduanFileName(fileName):
    panduan=len(fileName) >= 8 \
            # and (not ('党' in fileName or '政府' in fileName or
            #                              '书记' in fileName or '干部' in fileName or
            #                              '部队' in fileName or '中央' in fileName or
            #                              '习' in fileName or '毒' in fileName or
            #                              '政治' in fileName or '社团' in fileName or
            #                              '百年' in fileName or '领导' in fileName or
            #                              '两会' in fileName or '局长' in fileName or
            #                              '领导' in fileName or '军' in fileName or
            #                              '毛' in fileName or '微信' in fileName or
            #                              '微博' in fileName or 'QQ' in fileName or
            #                              'qq' in fileName or '作文' in fileName or
            #                              '伤' in fileName or '说说' in fileName or
            #                              '什么' in fileName or '怎么' in fileName or
            #                              '哪些' in fileName or '句子' in fileName or '多少' in fileName) )
    return panduan

def  deleAdvertising(filePath,newFolderPath,midFolderPath,newfileNames,front,later):
    doc=filePath.split(".")[len(filePath.split("."))-1]
    #word = wc.Dispatch("Word.Application")
    if doc=='docx' :
        try:
            file=docx.Document(filePath)
            fileName = os.path.basename(filePath)

            if panduanFileName(fileName):
                i=0
                for para in file.paragraphs:
                    para.text=para.text.replace('\n','')
                    if i == len(file.paragraphs) - 1:
                        print(para.text)
                    if para.text == "" or para.text =='\n' or para.text == ' ':
                        # print('第{}段是空行段'.format(i))
                        para.clear()  # 清除文字，并不删除段落，run也可以,
                        delete_paragraph(para)
                    i+=1
                file_name2=''.join(fileName.split())
                file_name1 = re.sub('【.*?】', '', file_name2)
                file_name = re.sub('www.*?com', '', file_name1)
                if 600 > 500 :
                    if file.save(newFolderPath + '\\' + file_name+'.docx')==None :
                            #newFile = docx.Document(newFolderPath + '\\' + fileName)
                            #delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                            # os.remove(filePath)  # 删除原来修改前的docx文件
                        print('succes')
                    if file_name in newfileNames:
                            print('succes')
            else:
                file.save(midFolderPath + '\\' + fileName)
                print('succes')
        except:
            print('错误')

        #word.Quit()

def deletePara(file):
    for para in file.paragraphs:
        para.text = para.text.replace('\n', '')
        if para.text == "" or para.text == '\n' or para.text == ' ':
            # print('第{}段是空行段'.format(i))
            para.clear()  # 清除文字，并不删除段落，run也可以,
            delete_paragraph(para)
def main2():
    #os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'E:\文档\test\1'
    newFolderPath = r'E:\文档\test\2'
    midFolderPath=r'G:\文档\需要修改文件名'
    filePaths = find_file(oldFolderPath,[])
    newfileNames = find_name(newFolderPath,[])
    for filePath in filePaths:
        if not '$' in filePath:
            print(filePath)
            deleAdvertising(filePath, newFolderPath,midFolderPath,newfileNames,0, 0) # 修改，删除广告
def main1():
    #os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'H:\文档\爬取网站\教学范文网'
    newFolderPath = r'H:\文档\上传1'
    midFolderPath=r'D:\文档\需要修改的文件'
    filePaths = find_file(oldFolderPath,[])
    newfileNames = find_name(newFolderPath,[])
    for filePath in filePaths:
        if not '$' in filePath:
            print(filePath)
            deleAdvertising(filePath, newFolderPath,midFolderPath,newfileNames,0, 0) # 修改，删除广告
if __name__ == '__main__':
    # main1()
    main2()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
