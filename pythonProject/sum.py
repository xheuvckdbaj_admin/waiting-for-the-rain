# -*- coding:utf-8 -*-
import os

from FindFile import findAllFile, find_file
from Transition import transition, doc_to_docx
from delDup import getmd5
from delEye import delEye
from modify import deleAdvertising
from parameter import parameter
from wordFormat import wordFormat


def main():
    oldFolderPath=parameter()[0]
    newFolderPath = parameter()[1]

    filePaths=find_file(oldFolderPath)
    for filePath in filePaths:
        #getmd5(filePath)
        wordFormat(filePath)
        delEye(filePath)
        deleAdvertising(filePath,newFolderPath,0,0)
if __name__ == '__main__':
    main()