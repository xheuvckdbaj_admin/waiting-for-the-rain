# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(url,str):
    newurl = url + str
    return newurl


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    list_soupa = str(soup.select('.paper_details')[0])
    left_type = str(soup.select('.paper_details  .left_paper_type')[0])
    content = re.sub(left_type, '', list_soupa).replace('下载', '').replace('收藏', '').replace('纠错', '').replace('+选择', '')

    # print('python' + '\r' + 'java' + '\n' + 'java')
    print(content)
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)

# 获取最终网址
def getUrl(thmlpath,url,i):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    list_soupa = soup.select('.paper_list')
    #print(url_list)
    j=0
    for u in list_soupa:
            j+=1
            i_str=str(i)+'-'+str(j)
            href=u.select('a')[0].get('href')
            print(href)
            mid_url='https://www.chazidian.com'
            newUrl=groupUrl(mid_url, href)
            print(newUrl)
            #title = u.select('dt > a')[0].text
            getContent(thmlpath,newUrl,i_str)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('#NEW_INFO_LIST > dl')
    #print(url_list)
    for u in url_list:
            src=u.select('dt > a')[0].get('href')
            href='http://www.banzhuren.cn'+src
            print(href)
            title = u.select('dt > a')[0].text
            getUrl(thmlpath,href,title)

def getUrlPageNum(url,a,b,strings):
    for pageNum in range(232,251):
        i=0
        try:
            newUrl=groupUrl(url, str(pageNum))
            #print(newUrl)
            for type in range(116,125):
                path=r'E:\试题专项\小学\数学'+'\\'+strings
                try:
                    type_url=groupUrl(newUrl,'_'+str(type))
                    #all_Url=type_url+'/'
                    for page in range(1,500):
                        try:
                            i+=1
                            for data in range(a,b):
                                st='_0_0_0,%d_p'
                                #format(str%data)
                                print(format(st%data))
                                page_url=groupUrl(type_url,format(st%data)+str(page)+'/')
                                #print(page_url)
                                getUrl(path,page_url,i)
                        except:
                            continue
                    #print(type_url)
                except:
                    continue
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main():
    url=f'https://www.chazidian.com/zujuan/shiti/2_1_'
    a=1438
    b=1440
    strings='一年级'
    getUrlPageNum(url,a,b,strings)


if __name__ == '__main__':
    #thmlpath = r'E:\试题专项\小学\语文\一年级'
    main()
