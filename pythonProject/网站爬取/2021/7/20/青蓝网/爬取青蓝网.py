# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="q-h"]/span[@class="h"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.qinglan.com/fanwen/gongzuozongjie/152357.html',
                'https://www.qinglan.com/fanwen/gongzuojihua/152364.html',
                'https://www.qinglan.com/fanwen/gongzuofangan/135811.html',
                'https://www.qinglan.com/fanwen/huodongzongjie/151885.html',
                'https://www.qinglan.com/fanwen/shenqingshu/152354.html',
                'https://www.qinglan.com/fanwen/cehuashu/152325.html',
                'https://www.qinglan.com/fanwen/zhiyeguihua/151455.html',
                'https://www.qinglan.com/fanwen/yaoqinghan/152379.html',
                'https://www.qinglan.com/jiaoxue/zongjie/152098.html',
                'https://www.qinglan.com/jiaoxue/jihua/152373.html',
                'https://www.qinglan.com/jiaoxue/fansi/152374.html',
                'https://www.qinglan.com/jiaoxue/sheji/152332.html',
                'https://www.qinglan.com/jiaoxue/shuokegao/151859.html',
                'https://www.qinglan.com/jiaoxue/jiaoan/152342.html',
                'https://www.qinglan.com/hetong/fanben/152017.html',
                'https://www.qinglan.com/hetong/fangwuzulin/152372.html',
                'https://www.qinglan.com/hetong/goufang/152029.html',
                'https://www.qinglan.com/hetong/laodong/152176.html',
                'https://www.qinglan.com/hetong/maimai/152331.html',
                'https://www.qinglan.com/hetong/shigong/152369.html',
                'https://www.qinglan.com/hetong/xiaoshou/152371.html',
                'https://www.qinglan.com/hetong/gongcheng/152360.html',
                'https://www.qinglan.com/zhici/ziwojieshao/152338.html',
                'https://www.qinglan.com/zhici/yanjianggao/152370.html',
                'https://www.qinglan.com/zhici/fayangao/152361.html',
                'https://www.qinglan.com/zhici/zhuchigao/152132.html',
                'https://www.qinglan.com/zhici/fanwen/151843.html',
                'https://www.qinglan.com/zhici/heci/152365.html',
                'https://www.qinglan.com/zhici/daxieci/152358.html',
                'https://www.qinglan.com/zhici/zhufuyu/152335.html',
                'https://www.qinglan.com/baogao/shixi/152368.html',
                'https://www.qinglan.com/baogao/shuzhi/151950.html',
                'https://www.qinglan.com/baogao/cizhi/152333.html',
                'https://www.qinglan.com/baogao/shehuishijian/150748.html',
                'https://www.qinglan.com/baogao/shiyongqi/151641.html',
                'https://www.qinglan.com/baogao/xindetihui/152382.html',
                'https://www.qinglan.com/baogao/diaocha/152003.html',
                'https://www.qinglan.com/baogao/kaiti/151570.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
