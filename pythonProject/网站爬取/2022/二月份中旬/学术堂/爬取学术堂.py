# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.neirong_text')[0]).replace('<br/>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.neirong_title > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.wz_liebiao > ul > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.xueshutang.com'+href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    pageContent = soup.select('div.pd_c_xslb_left_fenye > ul > li > a')[-1].text
    if '末页'==pageContent:
        href=soup.select('div.pd_c_xslb_left_fenye > ul > li > a')[-1].get('href')
        allPageNumber=href.split('_')[-1].replace('.html','')
        url=strUrl+href.replace(href.split('_')[-1],'')
        for i in range(1,allPageNumber+1):
            newUrl=url+str(i)+'.html'
            getListUrl(thmlpath, newUrl)
    elif '下一页'==pageContent:
        href = soup.select('div.pd_c_xslb_left_fenye > ul > li > a')[-1].get('href')
        allPageNumber = int(href.split('_')[-2].replace('.html', ''))
        url = strUrl + href.replace(href.split('_')[-1], '')
        for i in range(1, allPageNumber + 1):
            newUrl = url + str(i) + '.html'
            getListUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    ul_list = soup.select('div.pd_c_fanwen_fl_list > ul')
    for ul in ul_list:
        a_list=ul.select('li > a')
        for i,a in enumerate(a_list):
            if i>0 and not i==len(a_list)-1:
                href=a.get('href')
                newUrl=href
                if not 'http' in href:
                    newUrl='http://www.xueshutang.com'+href
                getPageUrl(thmlpath, newUrl)
def main(thmlpath):

                url='http://www.xueshutang.com/fanwen/'
                getTitleUrl(thmlpath,url)


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
