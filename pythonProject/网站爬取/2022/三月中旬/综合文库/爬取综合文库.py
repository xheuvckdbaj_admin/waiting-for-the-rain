# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getContent(strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        contentText = str(soup.select('div.con')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>').split('<center')[0]
        return contentText
    except:
        return ''
def getHtml(thmlpath, strUrl,title):
    url=strUrl.replace('.html','')
    hrefUrl=url+'_%d.html'
    content=''
    for i in range(1,5):
        newUrl=hrefUrl%i
        content=content+getContent(newUrl)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)
    f.close()
# def getListUrl(thmlpath,url):
#     try:
#         requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
#         soup = BeautifulSoup(requests_text, 'lxml')
#         a_list = soup.select('div.dx-list3 > div > a')
#         for i,a in enumerate(a_list):
#             try:
#                 href=a.get('href')
#                 getHtml(thmlpath, href)
#             except:
#                 print('gbk出错')
#                 continue
#     except:
#         print('error')

# def getPageUrl(thmlpath,url):
#     getListUrl(thmlpath, url)
#     hrefUrl=url+'list_%d.html'
#     for i in range(1,20):
#         newUrl=hrefUrl%i
#         getListUrl(thmlpath, newUrl)
def getNewWindow(browser,xpathStr):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, 100).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def getLawTitleUrl(file,thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.g_box5 > div.t_4 > h3 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        url='https://www.cmpx.com.cn'+href
        print(url)
        file.write(url+'\n')

def getTitleUrl(txtPath,thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('div.nav > ul > li > a')
    file=open(txtPath,'w')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            url='https://www.cmpx.com.cn'+href
            getLawTitleUrl(file,thmlpath, url)
    file.close()
def main(txtPath,thmlpath):
    #作者：
   url='https://www.cmpx.com.cn/'
   getTitleUrl(txtPath,thmlpath, url)
def getListUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('ul.list2 > li > h3 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        title=a.text
        newUrl='https://www.cmpx.com.cn'+href
        try:
            getHtml(thmlpath, newUrl,title)
        except:
            print('error')
            continue
def getPageUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href=soup.select('div.pagess > ul > li > a')[-1].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    hrefUrl=url+href.replace(href.split('_')[-1],'')+'%d.html'
    for i in range(2,pageNumber+1):
        newUrl=hrefUrl%i
        # print(newUrl)
        getListUrl(thmlpath, newUrl)
def mianx(txtPath,thmlpath):
    file=open(txtPath,'r')
    lines=file.readlines()
    for i,line in enumerate(lines):
        print('-----------',i)
        hrefUrl=line.replace('\n','')
        getListUrl(thmlpath, hrefUrl)
        try:
            getPageUrl(thmlpath, hrefUrl)
        except:
            print('只有一页')
            continue
if __name__ == '__main__':
    #本文档由香当网
    #(https://www.xiangdang.net)
    #用户上传
    txtPath='./lianjie.txt'
    thmlpath = r'E:\文档\自查报告网'
    # main(txtPath,thmlpath)
    mianx(txtPath,thmlpath)