# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import time
import urllib
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.quark.cn'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getTitle(browser, file):
    xpathStr = '//div[@class="taskItem"]/div[@class="desc"]/div[@class="desc-inner"]'
    getNewWindow(browser, xpathStr, 10)
    elements = browser.find_elements(By.XPATH, xpathStr)
    for element in elements:
        title = element.text
        print(title)
        file.write(title + '\n')


def ClickNextPage(browser, pageNumber):
    try:
        # pageNumber最大为1，最小为0
        xpathStr = '//li[@class="ant-pagination-next"]/button[@class="ant-pagination-item-link"]'
        browser.find_elements(By.XPATH, xpathStr)[-1].click()
        getNewWindow(browser, '//div', 10)
        time.sleep(2)
    except:
        print('error')


def getPageNumber(browser):
    xpathStr = '//ul[@class="ant-pagination fenye"]/li/a'
    pageStr = browser.find_elements(By.XPATH, xpathStr)[-1].text
    pageNumber = int(pageStr)
    return pageNumber


def mainTitleKuake(folderPath, cookiesPath):
    browser = getDriver('title1')
    url = 'https://doc.quark.cn/home'
    browser.get(url)
    # addCookies(browser, url, cookiesPath)
    time.sleep(60)
    filePath = folderPath + '\\' + str(1) + '.txt'
    file = open(filePath, 'w', encoding='utf-8')
    getTitle(browser, file)
    file.close()
    pageNumbers = getPageNumber(browser)
    for pageNumber in range(0, pageNumbers - 1):
        filePath = folderPath + '\\' + str(pageNumber + 2) + '.txt'
        file = open(filePath, 'w', encoding='utf-8')
        ClickNextPage(browser, pageNumber)
        getTitle(browser, file)
        file.close()



if __name__ == '__main__':
    folderPath = r'D:\文档\百度活动文档\夸克活动文档\夸克活动文档标题'
    cookiesPath = './kuakeCookies.txt'
    mainTitleKuake(folderPath, cookiesPath)
