import re

import requests
import time
from bs4 import BeautifulSoup
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getBrowserUrl(browser,url):
    try:
        browser.get(url)
    except:
        time.sleep(0.5)
        browser.get(url)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = str(soup.select('div.content > h1')[0].text)
    content=title
    p_list=soup.select('div.content > p')
    for p in p_list:
        content=content+str(p)
    # print(content)
    contentText1=re.sub('<span>(.*?)</span>','',content)
    contentText2 = re.sub('<strong>(.*?)</strong>', '', contentText1)
    contentText3 = re.sub('<div(.*?)</div>', '', contentText2)
    contentText = re.sub('<a(.*?)</a>', '', contentText3)
    title=str(soup.select('div.content > h1')[0].text)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
            #猜你感兴趣：
            #猜你感兴趣的：
            #number=1
            # browser = webdriver.Chrome()
            strUrl='https://www.youhaodu.com/sanwendaquan/2020/0922/%d.html'
            for i in range(1,565850):
                print(i)
                newUrl=strUrl%i
                try:
                    getHtml(thmlpath, newUrl)
                except:
                    print('出错')
                    continue
if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
