# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="rd_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.rdsqs.net/wuqianzi/article_17217.html',
                'https://www.rdsqs.net/geshi/article_20778.html',
                'https://www.rdsqs.net/moban/article_23032.html',
                'https://www.rdsqs.net/liubaizi/article_22684.html',
                'https://www.rdsqs.net/yiqianzi/article_22850.html',
                'https://www.rdsqs.net/yiqianwubaizi/article_22927.html',
                'https://www.rdsqs.net/erqianzi/article_20844.html',
                'https://www.rdsqs.net/erqianwubaizi/article_20866.html',
                'https://www.rdsqs.net/sanqianzi/article_19032.html',
                'https://www.rdsqs.net/zenmexie/article_22586.html',
                'https://www.rdsqs.net/junren/article_22930.html',
                'https://www.rdsqs.net/daxuesheng/article_22961.html',
                'https://www.rdsqs.net/dayi/article_22931.html',
                'https://www.rdsqs.net/daer/article_22895.html',
                'https://www.rdsqs.net/dasan/article_22899.html',
                'https://www.rdsqs.net/dasi/article_22882.html',
                'https://www.rdsqs.net/yanjiusheng/article_22919.html',
                'https://www.rdsqs.net/jiaoshi/article_22915.html',
                'https://www.rdsqs.net/budui/article_18010.html',
                'https://www.rdsqs.net/gaozhongsheng/article_22937.html',
                'https://www.rdsqs.net/cunmin/article_20800.html',
                'https://www.rdsqs.net/tieluzhigong/article_20862.html',
                'https://www.rdsqs.net/zhiyuan/article_22808.html',
                'https://www.rdsqs.net/yisheng/article_22921.html',
                'https://www.rdsqs.net/yisheng/article_22921.html',
                'https://www.rdsqs.net/hushi/article_20950.html',
                'https://www.rdsqs.net/yuangong/article_23015.html',
                'https://www.rdsqs.net/gongwuyuan/article_19275.html',
                'https://www.rdsqs.net/minjing/article_20153.html',
                'https://www.rdsqs.net/nongcun/article_21271.html',
                'https://www.rdsqs.net/nongmin/article_22889.html',
                'https://www.rdsqs.net/cunganbu/article_23016.html',
                'https://www.rdsqs.net/gongren/article_22913.html',
                'https://www.rdsqs.net/jidu/article_21646.html',
                'https://www.rdsqs.net/dangxiao/article_20174.html',
                'https://www.rdsqs.net/dangke/article_20972.html',
                'https://www.rdsqs.net/yubeidangyuan/article_22876.html',
                'https://www.rdsqs.net/zhuanzheng/article_22540.html',
                'https://www.rdsqs.net/dangyuan/article_22483.html',
                'https://www.rdsqs.net/jijifenzi/article_23077.html',
                'https://www.rdsqs.net/daxueshengsxhb/article_22484.html',
                'https://www.rdsqs.net/jiaoshixxhb/article_22849.html',
                'https://www.rdsqs.net/buduixxhb/article_17975.html',
                'https://www.rdsqs.net/duidangderenshi/article_20093.html',
                'https://www.rdsqs.net/geshisxhb/article_22563.html',
                'https://www.rdsqs.net/ganbu/article_21342.html',
                'https://www.rdsqs.net/gongwuyuanrdzz/article_20851.html',
                'https://www.rdsqs.net/jiaoshirdzz/article_21605.html',
                'https://www.rdsqs.net/gaozhongshengrdzz/article_20826.html',
                'https://www.rdsqs.net/daxueshengrdzz/article_22699.html',
                'https://www.rdsqs.net/yanjiushengrdzz/article_21604.html',
                'https://www.rdsqs.net/zhiyuanrdzz/article_21426.html',
                'https://www.rdsqs.net/yishengrdzz/article_22754.html',
                'https://www.rdsqs.net/hushirdzz/article_20998.html',
                'https://www.rdsqs.net/nongminrdzz/article_22508.html',
                'https://www.rdsqs.net/gongrenrdzz/article_21631.html',
                'https://www.rdsqs.net/cunguan/article_21611.html',
                'https://www.rdsqs.net/daxueshengzys/article_22769.html',
                'https://www.rdsqs.net/jiaoshizys/article_20989.html',
                'https://www.rdsqs.net/yubeidangyuanzys/article_21071.html',
                'https://www.rdsqs.net/zenmexiezys/article_17022.html',
                'https://www.rdsqs.net/mobanzys/article_22847.html',
                'https://www.rdsqs.net/fanwenzys/article_22778.html',
                'https://www.rdsqs.net/gongkaicns/article_23080.html',
                'https://www.rdsqs.net/xianjinxjy/article_22958.html',
                'https://www.rdsqs.net/cailiaofenxi/article_22902.html',
                'https://www.rdsqs.net/rudangjsr/article_22828.html',
                'https://www.rdsqs.net/shici/article_22258.html',
                'https://www.rdsqs.net/tanhua/article_23066.html',
                'https://www.rdsqs.net/zongjie/article_22183.html',
                'https://www.rdsqs.net/zichuan/article_21816.html',
                'https://www.rdsqs.net/tigang/article_16955.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
