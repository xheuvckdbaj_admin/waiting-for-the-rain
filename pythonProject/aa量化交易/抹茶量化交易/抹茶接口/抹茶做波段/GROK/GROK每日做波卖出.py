import datetime
import decimal
import math
from decimal import Decimal, ROUND_HALF_UP

from pythonProject.aa量化交易.抹茶量化交易.抹茶接口.MEXC import Client, demo


def main():
    client=Client.Client('mx0vgltuaCRckhMEVJ','863b284575b947f481f45ed17604435c')
    # response=client._getSysTimeStamp()
    # client._getDefaultSymbols()
    # response=client._getDepth('BTCUSDT','1000')
    response=client._getSelfSymbols()
    # response=client._getExchangeInfo('DNXUSDT')
    # response = client._getTrades('DNXUSDT','100')
    # for text in response.json():
    #     # print(text['isBuyerMaker'])
    #     # if text['isBuyerMaker']==True:
    #         print(text)
    # response=client._getAggTrades('DNXUSDT','','','500')

    # response = client._getKlines('DNXUSDT','1m','','','1000')
    # response = client._getAvgprice('DNXUSDT')
    # response = client._get24Hr('DNXUSDT')
    # response = client._getPrice('DNXUSDT')
    # response = client._getBookTicker('DNXUSDT')
    response = client._postorder('DNXUSDT','SELL','LIMIT','10','10','0.678','','','')
    response =client._getAccount('5000','')
    # trade=demo.mexc_trade(consts.DOMAIN, 'mx0vgltuaCRckhMEVJ','863b284575b947f481f45ed17604435c')
    # params = {'symbol': 'DNXUSDT',
    #           'side': 'BUY',
    #           'type': 'LIMIT',
    #           'quoteOrderQty': '10',
    #           'price': '0.625',
    #           'quantity':'10'}
    # # params={}
    # response=trade.post_order(params)
    # print(response.json())
    # print(response.status_code)
    # for text in response:
    #     print(text)
    # print(str(response.json()["serverTime"]))
    # return t + "Z"
def mochamain(symbol,highSell,lowSell,equal):
    client = Client.Client('mx0vgltuaCRckhMEVJ', '863b284575b947f481f45ed17604435c')
    #查询还有多少枚dnx
    response = client._getAccount('5000', '')
    maps=response['balances']
    dnxNumber=0
    for map in maps:
        asset=map['asset']
        if asset==symbol.replace('USDT',''):
            free=map['free']
            dnxNumber=decimal.Decimal(free)
        if asset=='USDT':
            USDTfree = map['free']
            USDTNumber = decimal.Decimal(USDTfree)
    print(dnxNumber,USDTNumber)
    #查询24小时dnx最高价格和最低价格
    response = client._get24Hr(symbol)
    highPrice=response['highPrice']
    lowPrice = response['lowPrice']
    askprice=response['askPrice']
    bidPrice=response['bidPrice']
    print(highPrice,lowPrice)
    #计算出卖出价格区间
    decimal.getcontext().prec = 4
    bandFre=(Decimal(highPrice)-Decimal(lowPrice))/Decimal(lowPrice)
    if highSell=='':
        highSell=decimal.Decimal((bandFre*Decimal(1.4)+Decimal(1))*Decimal(lowPrice))
    else:
        highSell=decimal.Decimal(highSell)
    if lowSell=='':
        lowSell=decimal.Decimal((bandFre * Decimal(0.9) + Decimal(1)) * Decimal(lowPrice))
    else:
        lowSell=decimal.Decimal(lowSell)
    print(highSell,lowSell)

    #根据dnx枚数计算出格数与格数价格差
    n=math.ceil(Decimal(math.sqrt(dnxNumber*2/int(equal)+Decimal(0.25)))-Decimal(0.5))
    print(n)
    diffrPrice=Decimal((highSell-lowSell)/n)
    print(diffrPrice)

    #分批依次委托卖出dnx
    for i in range(0,n):
        if i !=(n-1):
            number=(i+1)*int(equal)
            sellPrice=lowSell+Decimal(i)*diffrPrice
            print(sellPrice)
            response = client._postorder(symbol, 'SELL', 'LIMIT', str(float(number)), str(float(number)), str(float(sellPrice)), '', '', '')
            print(response)
        else:
            response = client._getAccount('5000', '')
            maps = response['balances']
            dnxNumber = 0
            for map in maps:
                asset = map['asset']
                if asset == symbol.replace('USDT',''):
                    free = map['free']
                    dnxNumber = decimal.Decimal(free)
            number=int(dnxNumber)
            response = client._postorder(symbol, 'SELL', 'LIMIT', str(float(number)), str(float(number)), str(float(highSell)), '', '','')

if __name__ == '__main__':
    # 选择币种，比如dnx：DNXUSDT，clore：CLOREUSDT等
    symbol = 'GROKUSDT'
    #等差值，上一格比下一格的数量差值
    equal = '700'
    # 卖出的价格在最高和最低价区间，呈现等差委托
    # 卖出最高价
    highSell='0.008332'
    # 卖出最低价
    lowSell='0.007632'
    mochamain(symbol,highSell,lowSell,equal)