import os.path

from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.百度上传文档分类 import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle


def main_reName(titleFolderPath,pdfFolderPath,modifyFolderPath):
    titleFile=open(titleFolderPath+'\\all.txt','r',encoding='utf-8')
    file_lines=titleFile.readlines()
    for line in file_lines:
        keyStr=line.replace('\n','')
        filePaths=find_file(pdfFolderPath)
        for filePath in filePaths:
            fileName=os.path.basename(filePath)
            if fileName.endswith('.pdf'):
                title=fileName.replace('.pdf','')
                if mainPanDuanTitle(keyStr, title):
                    try:
                        print(title+'-------'+keyStr)
                        os.rename(filePath,modifyFolderPath+'\\'+keyStr+'.pdf')
                    except:
                        print('已经存在该文件')

if __name__ == '__main__':
    titleFolderPath=r'D:\文档\百度活动文档\百度活动文档标题'
    pdfFolderPath=r'D:\文档\冰点文档'
    modifyFolderPath=r'D:\文档\冰点修改后的文档'
    main_reName(titleFolderPath,pdfFolderPath,modifyFolderPath)