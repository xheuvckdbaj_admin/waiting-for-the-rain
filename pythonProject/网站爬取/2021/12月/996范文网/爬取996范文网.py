# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        #url='http://m.pincai.com/article/2300000.htm'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('.Text')[0])
        print(title)
        #print(list_soupa)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.lan-left > .lie > ul > li > a')
    for a in a_list:
            time.sleep(10)
            href=a.get('href')
            print(href)
            getContent(thmlpath,href)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.text_list > li > .list_title > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        newUrl='https://www.dawendou.com'+href
        title=a.get('title').replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
        print(title)
        getContent(thmlpath, newUrl,title)

def getHtml2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.box.smalllist > .title > span > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        newUrl='https://www.dawendou.com'+href
        getHtml3(thmlpath, newUrl)

def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.content')[0])
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.article > h1')[0].text)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
            #以下是本站为大家整理的关于个人扶贫工作述职报告6篇范文，欢迎参考借鉴~
            strUrl='http://www.996site.com/zuowendaquan/2021/1014/%d.html'
            for i in range(1,54868):
                try:
                    print(i)
                    newUrl=strUrl%i
                    getHtml(thmlpath, newUrl)
                except:
                    print('出错')
                    continue


if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\996范文网'
    main(thmlpath)
