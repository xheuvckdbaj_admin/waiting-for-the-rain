# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import time
import urllib

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt
from pywinauto import Desktop
from pywinauto.keyboard import *


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser


def panduanTitle(title):
    panduan = True
    if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title or '年级' in title \
            or '图' in title or '.' in title or '×' in title or (len(title) < 5):
        panduan = False
    return panduan


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getTitle(browser, file):
    xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    try:
        getNewWindow(browser, xpathStr, 10)
    except:
        print('error')
    elements = browser.find_elements(By.XPATH, xpathStr)
    for element in elements:
        title = element.get_attribute('title')
        if panduanTitle(title):
            print(title)
            file.write(title + '\n')


def ClickNextPage(browser, pageNumber):
    try:
        # pageNumber最大为1，最小为0
        xpathStr = '//button[@class="btn-next"]/i[@class="el-icon el-icon-arrow-right"]'
        browser.find_element(By.XPATH, xpathStr).click()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(2)
    except:
        print('error')


def clickNextTitle(browser, pageNumber):
    # pageNumber最小为0
    if pageNumber == 9:
        xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
        browser.find_element(By.XPATH, xpathStrs).click()
        getNewWindow(browser, xpathStrs, 10)
        time.sleep(2)
    xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
    browser.find_elements(By.XPATH, xpathStr)[pageNumber].click()
    getNewWindow(browser, xpathStr, 10)
    time.sleep(2)


def getNumberPager(browser):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
        return pagerNumber
    except:
        return 0


def deleteWord(folderPath, title):
    filePath = folderPath + '\\' + title + '.docx'
    try:
        os.remove(filePath)
    except:
        print('无法删除'+folderPath)

def ctrlx(oldFilePath,newPath):
    #if oldFilePath.endswith('html'):
        #移动文件夹示例
        shutil.move(oldFilePath,newPath)
def getFileNameStr(folderPath, dialog,newPath):
    filePathxs = find_file(folderPath,[])
    for filePathx in filePathxs:
        if '(' in filePathx or '+' in filePathx or '^' in filePathx or '%' in filePathx:
            try:
                print('需要把这个文件剪切到修改后文件夹')
                ctrlx(filePathx, newPath)
            except:
                print('无法剪切' + filePathx)
    keyStr = ''
    filePaths = find_file(folderPath, [])
    if len(filePaths) >= 2:
        for filePath in filePaths:
            titleStr = os.path.basename(filePath).replace('.docx', '')
            print(titleStr)
            keyStr = keyStr + '\"' + titleStr + '\" '
    elif len(filePaths) == 1:
        for filePath in filePaths:
            try:
                print('需要把这个文件剪切到修改后文件夹')
                ctrlx(filePath, newPath)
            except:
                print('无法剪切'+filePath)
        dialog['Button2'].click()
    elif len(filePaths)==0:
        try:
            dialog['Button2'].click()
        except:
            print('关闭出错')
        try:
            os.remove(folderPath)
        except:
            print('无法删除'+folderPath)
    return keyStr


def submitword(browser,folderPath,dataNumber):
    time.sleep(10)
    getNewWindow(browser, '//div[@class="doc-upload-btn-bar"]/button[@class="el-button el-button--primary"]', 10)
    button = browser.find_element(By.XPATH,
                                   '//div[@class="doc-upload-btn-bar"]/button[@class="el-button el-button--primary"]')
    ActionChains(browser).move_to_element(button).perform()
    ActionChains(browser).click(button).perform()
    filePathxs = find_file(folderPath, [])
    print('已经成功上传' + str(dataNumber) + '个文档')
    dataNumber1 = len(filePathxs)
    print('此次成功上传' + str(dataNumber1) + '个文档')
    together = dataNumber + dataNumber1
    print('一共成功上传' + str(together) + '个文档')
    longx = dataNumber1
    for filePathx in filePathxs:
        try:
            os.remove(filePathx)
        except:
            print('删除不掉'+filePathx)
    return longx


def getTitleNumber(wordFolderPath):
    titleNumber = 1000000
    if wordFolderPath.endswith('1推荐'):
        titleNumber = -1
    if wordFolderPath.endswith('2学前教育'):
        titleNumber = 0
    if wordFolderPath.endswith('3基础教育'):
        titleNumber = 1
    if wordFolderPath.endswith('4高校与高等教育'):
        titleNumber = 2
    if wordFolderPath.endswith('5资格考试'):
        titleNumber = 3
    if wordFolderPath.endswith('6法律'):
        titleNumber = 4
    if wordFolderPath.endswith('7建筑'):
        titleNumber = 5
    if wordFolderPath.endswith('8互联网'):
        titleNumber = 6
    if wordFolderPath.endswith('9行业资料'):
        titleNumber = 7
    if wordFolderPath.endswith('10政务民生'):
        titleNumber = 8
    if wordFolderPath.endswith('11商品说明书'):
        titleNumber = 9
    if wordFolderPath.endswith('12实用模板'):
        titleNumber = 10
    if wordFolderPath.endswith('13生活娱乐'):
        titleNumber = 11
    return titleNumber


def selectHeading(browser, wordFolderPath):
    pageNumber = getTitleNumber(wordFolderPath)
    if pageNumber >=0:
        # pageNumber最小为0
        if pageNumber >= 9:
            xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
            browser.find_element(By.XPATH, xpathStrs).click()
            getNewWindow(browser, xpathStrs, 10)
            time.sleep(1)
        xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
        titleEl=browser.find_elements(By.XPATH, xpathStr)[pageNumber]
        ActionChains(browser).click(titleEl).perform()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(1)

def clickButton(ele):
    try:
        ele.click()
        time.sleep(0.2)
        ele.click()
        time.sleep(0.2)
        ele.click()
    except:
        print('')
def uploadFile(browser, titleFolderPath,wordFolderPathName,newPath,dataNumber):
        longx = 0
        browser.get('https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
        getNewWindow(browser, '//div[@class="major-btns"]', 20)
        selectHeading(browser, titleFolderPath)
        wordFolderPath = os.path.join(titleFolderPath, wordFolderPathName)
        print(wordFolderPath)
        el = browser.find_element(By.CSS_SELECTOR, 'div.major-btns > div > div.control-btn')
        ActionChains(browser).click(el).perform()
        time.sleep(0.2)
        # getTitle(browser, file)
        app = Desktop()
        dialog = app['打开']
        # dialog.print_control_identifiers(depth=None, filename=None)
        dialog['Edit'].type_keys(wordFolderPath)
        clickButton(dialog['Button'])
        keyStr = getFileNameStr(wordFolderPath, dialog,newPath)
        print(keyStr)
        if not keyStr == '':
            dialog['Edit'].type_keys(keyStr, with_spaces=True)
            time.sleep(0.2)
            clickButton(dialog['Button'])
            try:
                getNewWindow(browser, '//div[@class="el-dialog__body"]', 10)
                print('需要删除文档')
                elements = browser.find_elements(By.XPATH, '//div[@class="el-dialog__body"]/p[@class="des"]')
                for ele in elements:
                    title = ele.text[1:-1]
                    print(title)
                    deleteWord(wordFolderPath, title)
                # button = browser.find_element(By.XPATH,
                #                               '//div[@class="el-dialog__header"]/button[@class="el-dialog__headerbtn"]')
                # ActionChains(browser).move_to_element(button).perform()
                # ActionChains(browser).click(button).perform()

                longx=uploadFile(browser,titleFolderPath,wordFolderPathName,newPath,dataNumber)
            except:
                print('上传成功')
                longx=submitword(browser,wordFolderPath,dataNumber)
                time.sleep(1)
        else:
            try:
                dialog['Button2'].click()
            except:
                print('不用关闭')
        return longx
def mainTitleClassify(folderPath, cookiesPath,newPath,dataNumber):
    browser = getDriver('title1')
    url = 'https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask'
    addCookies(browser, url, cookiesPath)
    time.sleep(1)
    xpathStr = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    titleEl = browser.find_elements(By.XPATH, xpathStr)[0]
    ActionChains(browser).click(titleEl).perform()
    getNewWindow(browser, xpathStr, 10)
    getNewWindow(browser, '//div[@class="major-btns"]', 20)
    filePath = folderPath + '\\' + str(1) + '.txt'
    titleFolderPaths = os.listdir(folderPath)
    dataNumber = 0
    for titleFolderPathName in titleFolderPaths:
        titleFolderPath = os.path.join(folderPath, titleFolderPathName)
        wordFolderPaths = os.listdir(titleFolderPath)
        for wordFolderPathName in wordFolderPaths:
            print(dataNumber)
            dataNumber=dataNumber+uploadFile(browser, titleFolderPath,wordFolderPathName,newPath,dataNumber)
        if len(wordFolderPaths)==0:
            try:
                os.remove(titleFolderPath)
            except:
                print('无法删除'+titleFolderPath)


if __name__ == '__main__':
    folderPath = r'E:\文档\百度活动文档\百度活动文档上传'
    newPath=r'E:\文档\百度活动文档\修改后百度活动文档'
    cookiesPath = './baiduCookies1.txt'
    dataNumber=0
    mainTitleClassify(folderPath, cookiesPath,newPath,dataNumber)
