import os
import threading

import pypandoc
import requests

from pythonProject.FindFile import find_file


def htmlBeWOrd(src):
    try:
        if src.endswith('.html'):
            print(src)
            docx = src.replace('.html', '.docx').replace('\t', '').replace('\n', '')
            output = pypandoc.convert_file(src, 'docx', outputfile=docx, encoding='utf-8',
                                           extra_args=["-M2GB", "+RTS", "-K64m", "-RTS"])
            os.remove(src)
    except:
        print('错误')
def makeWord(filePaths,startNumber,endNumber):
    for i in range(startNumber,endNumber):
        htmlBeWOrd(filePaths[i])
def shear_dile(src):
    filePaths=find_file(src)
    makeWord(filePaths, 0, len(filePaths))



def mainHtoW():

    folder=r'D:\文档\百度活动文档\百度html\第一范文网'
    shear_dile(folder)
if __name__ == '__main__':
    mainHtoW()
