# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm
import sys
sys.setrecursionlimit(5000)
# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.post_body.mt-3')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.post_title > h1.text-left.mt-4')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getTuijianUrl(thmlpath,soup):
    a_list = soup.select('div.mt-3 > ul.list-group > li > a')
    print('-------------','获取相关')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://shuzhi.soutijiang.com'+href
        getHtml(thmlpath, newUrl)
def getListUrl(thmlpath,url,file):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.list-page.mt-3 > ul > li > h4 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://shuzhi.soutijiang.com'+href
        print(newUrl)
        file.write(newUrl+'\n')
def getLawTitleUrl(thmlpath,url,file):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('ul.navbar-nav > li.nav-item.dropdown > div.dropdown-menu > a.dropdown-item')
    print('--------------------------')
    for i, a in enumerate(a_list):
            href = a.get('href')
            newUrl = 'http://shuzhi.soutijiang.com' + href
            getListUrl(thmlpath, newUrl, file)
def getTitleUrl(thmlpath,url,file):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('ul.navbar-nav > li > a')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            newUrl='http://shuzhi.soutijiang.com'+href
            getListUrl(thmlpath, newUrl,file)
def getNextUrl(thmlpath,url,nextNumber):
    getHtml(thmlpath, url)
    if nextNumber < 2200:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        getTuijianUrl(thmlpath, soup)
        try:
            href=soup.select('div.mt-3 > p > a')[-1].get('href')
            newUrl='http://shuzhi.soutijiang.com'+href
            print(nextNumber,newUrl)
            nextNumber += 1
            getNextUrl(thmlpath, newUrl,nextNumber)
        except:
            print('完毕')
def getTopUrl(thmlpath,url,topNumber):
    getHtml(thmlpath, url)
    if topNumber < 2200:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        getTuijianUrl(thmlpath, soup)
        try:
            href = soup.select('div.mt-3 > p > a')[0].get('href')
            newUrl = 'http://shuzhi.soutijiang.com' + href
            print(topNumber,newUrl)
            topNumber+=1
            getTopUrl(thmlpath, newUrl,topNumber)
        except:
            print('完毕')
def main(thmlpath):
#收藏本页
#范文参考网 www.fwwang.cn
    url='http://shuzhi.soutijiang.com'
    file=open(thmlpath,'w')
    getTitleUrl(thmlpath,url,file)
    getLawTitleUrl(thmlpath,url,file)
    file.close()
def mainx(txtPath,thmlpath):
    file = open(txtPath, 'r')
    file_lines=file.readlines()
    for i,line in enumerate(file_lines):
        newUrl=line.replace('\n','')
        print('---------',newUrl)
        nextNumber=0
        topNumber=0
        getTopUrl(thmlpath, newUrl, topNumber)
        getNextUrl(thmlpath, newUrl,nextNumber)


if __name__ == '__main__':
    txtPath = r'./lianjie.txt'
    main(txtPath)
    thmlpath=r'E:\文档\自查报告网'
    mainx(txtPath,thmlpath)
