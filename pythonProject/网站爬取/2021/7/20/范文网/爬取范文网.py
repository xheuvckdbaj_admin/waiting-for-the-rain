# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="fnear"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.fanwenwang.com/zhichang/gongzuozongjie/173016.html',
                'https://www.fanwenwang.com/zhichang/nianzhongzongjie/172774.html',
                'https://www.fanwenwang.com/zhichang/gongzuojihua/173004.html',
                'https://www.fanwenwang.com/zhichang/gongzuofangan/172902.html',
                'https://www.fanwenwang.com/zhichang/gongzuobaogao/172383.html',
                'https://www.fanwenwang.com/zhichang/shuzhibaogao/169956.html',
                'https://www.fanwenwang.com/zhichang/cizhibaogao/173014.html',
                'https://www.fanwenwang.com/zhichang/jianlimoban/172595.html',
                'https://www.fanwenwang.com/zhichang/zhiyeguihua/172846.html',
                'https://www.fanwenwang.com/shuxin/qiuzhixin/172999.html',
                'https://www.fanwenwang.com/shuxin/cizhixin/172484.html',
                'https://www.fanwenwang.com/shuxin/jieshaoxin/172497.html',
                'https://www.fanwenwang.com/shuxin/tuijianxin/172606.html',
                'https://www.fanwenwang.com/shuxin/weiwenxin/172992.html',
                'https://www.fanwenwang.com/shuxin/biaoyangxin/173017.html',
                'https://www.fanwenwang.com/shuxin/daoqianxin/172993.html',
                'https://www.fanwenwang.com/shuxin/ganxiexin/172998.html',
                'https://www.fanwenwang.com/shuxin/zijianxin/173015.html',
                'https://www.fanwenwang.com/jiaoyu/jiaoxuezongjie/172935.html',
                'https://www.fanwenwang.com/jiaoyu/jiaoxuejihua/173012.html',
                'https://www.fanwenwang.com/jiaoyu/jiaoxuefansi/172985.html',
                'https://www.fanwenwang.com/jiaoyu/tingkebaogao/169948.html',
                'https://www.fanwenwang.com/jiaoyu/shuokegao/172198.html',
                'https://www.fanwenwang.com/jiaoyu/biyelunwen/107071.html',
                'https://www.fanwenwang.com/jiaoyu/kaitibaogao/159505.html',
                'https://www.fanwenwang.com/jiaoyu/biyedabian/169959.html',
                'https://www.fanwenwang.com/zhici/yanjianggao/173010.html',
                'https://www.fanwenwang.com/zhici/fayangao/172995.html',
                'https://www.fanwenwang.com/zhici/guangbogao/172954.html',
                'https://www.fanwenwang.com/zhici/zhuchici/172862.html',
                'https://www.fanwenwang.com/zhici/zhici/172924.html',
                'https://www.fanwenwang.com/zhici/heci/172314.html',
                'https://www.fanwenwang.com/zhici/kaimuci/172934.html',
                'https://www.fanwenwang.com/zhici/bimuci/172731.html',
                'https://www.fanwenwang.com/zhici/daxieci/173009.html',
                'https://www.fanwenwang.com/shiyong/huodongzongjie/172256.html',
                'https://www.fanwenwang.com/shiyong/diaochabaogao/172956.html',
                'https://www.fanwenwang.com/shiyong/zichabaogao/172498.html',
                'https://www.fanwenwang.com/shiyong/xindetihui/173019.html',
                'https://www.fanwenwang.com/shiyong/hetong/172593.html',
                'https://www.fanwenwang.com/shiyong/qingjiatiao/171929.html',
                'https://www.fanwenwang.com/shiyong/tongzhi/173018.html',
                'https://www.fanwenwang.com/shiyong/qishi/171393.html',
                'https://www.fanwenwang.com/shiyong/jietiao/172996.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
