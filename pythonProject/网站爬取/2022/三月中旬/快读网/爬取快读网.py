# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.mycontext')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    return contentText

def getPageUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = str(soup.select('div.g_con > h1')[0].text).split('\n')[0]
    print(title)
    title=title.split(':')[-1]
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    hrefUrl=url.replace('.html','')+'_%d.html'
    content=''
    pageList=soup.select('div.listpages')
    print(len(pageList))
    if len(pageList)==1:
        for i in range(1,3):
            newUrl=hrefUrl%i
            Strcontent=getHtml(newUrl)
            content=content+Strcontent
    else:
        for i in range(1,2):
            newUrl=hrefUrl%i
            Strcontent=getHtml(newUrl)
            content=content+Strcontent
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)
    f.close()

def main(thmlpath):
   url='https://www.kuaidu.com.cn/article/%d.html'
   for i in range(25012,212000):
       try:
           print('----------',i)
           newUrl=url%i
           print(newUrl)
           getPageUrl(thmlpath,newUrl)
       except:
           print('error')
           continue
if __name__ == '__main__':

    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
