from pythonProject.创作活动文档.方式一下载百度活动文档4.b整理文档.allyunxing import main_arrangeDocx
from pythonProject.创作活动文档.方式一下载百度活动文档4.b整理文档.合并txt内容 import merge
from pythonProject.创作活动文档.方式一下载百度活动文档4.c分类百度上传文档.获取活动文档标题 import mainTitleClassify
from pythonProject.创作活动文档.方式一下载百度活动文档4.三六零搜索.爬取360搜索 import mianSanLiuLing
from pythonProject.创作活动文档.方式一下载百度活动文档4.中国搜索.allchina import mian_zhonguosousuo
from pythonProject.创作活动文档.方式一下载百度活动文档4.今日头条.allJinRiWebsit import main_jinritoutiao
from pythonProject.创作活动文档.方式一下载百度活动文档4.全网爬取.allWebsit import main_baidu
from pythonProject.创作活动文档.方式一下载百度活动文档4.夸克搜索.夸克搜索爬取 import main_kuake
from pythonProject.创作活动文档.方式一下载百度活动文档4.必应搜索.allWebsitBiYing import main_biying
from pythonProject.创作活动文档.方式一下载百度活动文档4.必应搜索国际版.allWebsitBiYing import main_biyingGuoji
from pythonProject.创作活动文档.方式一下载百度活动文档4.搜狗搜索.allWebsitSougou import main_sougou
from pythonProject.创作活动文档.方式一下载百度活动文档4.百度文库.总获取百度文档word import mainBaiDuWenDang
from pythonProject.创作活动文档.方式一下载百度活动文档4.百度问答.allUpload import mainBaiDuWenDa


def main():
    # 该账号cookies
    cookiesPath = './baiduCookies1.txt'
    # 标题存放文件夹
    titleFolder = r'D:\文档\百度活动文档10\百度活动文档标题'
    # 文档存放文件夹
    folderPath = r'D:\文档\百度活动文档10\百度html\其他网站'
    # 中介文件夹
    mediumFolderPath = r'D:\文档\百度活动文档10\中介'
    # 不要设置的文件夹，不需要管他
    midFolderPath = r'D:\文档\自查报告网url'
    # 中介文件夹
    # 修改格式后最终存放文档的文件夹，也就是文档上传文件夹，最终文件夹
    newPath = r'D:\文档\百度活动文档10\修改后百度活动文档'
    wangzhiPath = r'./wangzhi.txt'
    # 标题开始值
    startnumber = 0
    #爬取百度定产活动文档标题
    mainTitleClassify(titleFolder, cookiesPath)
    #合并txt文档
    merge(titleFolder, titleFolder)
    allTitleFile = titleFolder + '\\all.txt'
    titleFile = open(allTitleFile, 'r', encoding='utf-8')
    numberTxt = len(titleFile.readlines())
    print(numberTxt)
    startnumber = numberTxt
    for i in range(0, 1):
        startnumber=i
        last = 6000
        print('运行到---------------------', str(startnumber))
        # 爬取搜狗搜索
        # main_sougou(cookiesPath, titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 爬取今日头条
        # main_jinritoutiao(cookiesPath, titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 爬取360搜索
        # mianSanLiuLing(titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 爬取百度文库
        # mainBaiDuWenDang(cookiesPath, titleFolder, folderPath, startnumber, last)
        # 爬取百度问答
        # mainBaiDuWenDa(titleFolder, folderPath, startnumber, last)
        # 爬取百度搜索
        # main_baidu(cookiesPath,titleFolder,folderPath,wangzhiPath,startnumber,last)
        # 爬取必应搜索
        # main_biying(cookiesPath, titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 爬取必应国际版搜索
        # main_biyingGuoji(cookiesPath, titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 中国搜索
        # mian_zhonguosousuo(cookiesPath, titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 夸克搜索
        # main_kuake(titleFolder, folderPath, wangzhiPath, startnumber, last)
        # 整理文档
        # main_arrangeDocx(folderPath, mediumFolderPath, midFolderPath, newPath)


if __name__ == '__main__':
    main()
