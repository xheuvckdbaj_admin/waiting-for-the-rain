import time
from decimal import Decimal

from okx import Trade, MarketData

def getMA(instId, marketAPI, Hnumber):
    millisecondNumber = int(Hnumber) * 3600000
    # （4）获取当前MA30
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '5m','60')
    cnumber60 = 0
    for i in range(0, 60):
        c0 = result_market_SWAP['data'][i][4]
        cnumber60 += Decimal(c0)
    MA60 = Decimal(cnumber60 / 60)

    return MA60
def frequencyHight(api_key,secret_key,passphrase,flag,instId):
    tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
    #取消委托
    result_order = tradeAPI.get_order_list('SPOT', '', '', '', '', '', '', '', '')
    for data in result_order['data']:
        # 取消当前委托
        result_cancel_order = tradeAPI.cancel_order(data['instId'], data['ordId'], '')
        if not result_cancel_order['code'] == '0':
            result_cancel_order = tradeAPI.cancel_order(data['instId'], data['ordId'], '')
    #获取当前币的价格
    marketAPI = MarketData.MarketAPI(api_key, secret_key, passphrase, False, flag)
    result_market_SPOT = marketAPI.get_ticker(instId)
    nowPrice = result_market_SPOT['data'][0]['last']
    print(nowPrice)
    #获取当前5分钟MA60
    MA60 = getMA(instId, marketAPI, '0')


    #比较当前价格与MA30
    rate=(Decimal(nowPrice)-Decimal(MA60))/Decimal(nowPrice)

    if rate > Decimal(0.0038):
        # 获取当前持有的币的数量
        print('该卖出')
        result_order = tradeAPI.place_order(instId, 'cash', 'sell', 'limit', '0.005', '', '', '', '',
                                            str(nowPrice),
                                            '', '', '', '', '', '',
                                            '', '', '', '', '')
    elif rate < Decimal(-0.0038):
        # 获取当前可交易的金额
        print('该买入')
        # instId, tdMode, side, ordType, sz, ccy='', clOrdId='', tag='', posSide='', px='',
        #             reduceOnly='', tgtCcy='', tpTriggerPx='', tpOrdPx='', slTriggerPx='', slOrdPx='',
        #             tpTriggerPxType='', slTriggerPxType='', quickMgnType='', stpId='', stpMode=''
        result_order = tradeAPI.place_order(instId, 'cash', 'buy', 'limit', '0.005', '', '', '', '',
                                            str(nowPrice),
                                            '', '', '', '', '', '',
                                            '', '', '', '', '')
        if result_order['data'][0]['sMsg'] == 'Order placed':
            print('下单成功',
                  '买了%s张%s,价格：%s' % ('2', instId, str(nowPrice)))
    #以当前价格的买入或者卖出
    time.sleep(10000)

def main():
    flag = "0"  # live trading: 0, demo trading: 1
    api_key = 'dcc90281-f342-4166-ad66-110a941ae3e6'
    secret_key = '9B2084459A62DC52E669482ED8DDBD0A'
    passphrase = '#Xsy103171'
    instId='BTC-USDT'
    for i in range(0,100000000):
        frequencyHight(api_key,secret_key,passphrase,flag,instId)

if __name__ == '__main__':
    main()