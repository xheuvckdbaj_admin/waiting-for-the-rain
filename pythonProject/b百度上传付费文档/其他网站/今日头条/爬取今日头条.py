import re

import requests
import urllib
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_jinritoutiao(url,htmlPath,keyStr,title):
    try:
        driver = getDriver('tou')
        driver.get(url)
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="article-content"]/h1')
            )
        )
        title = driver.find_element(By.XPATH, '//div[@class="article-content"]/h1').text
        title = re.sub('（(.*)）', '', title)
        title = re.sub('\d+篇', '', title)
        list_soupa = str(
            driver.find_element(By.XPATH, '//article[@class="syl-article-base tt-article-content syl-page-article syl-device-pc"]').get_attribute('innerHTML').replace(
                title, title))
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('今日头条下载成功----', title)
        f.close()
        driver.quit()
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.toutiao.com/article/6971335815691616805/?channel=&source=search_tab'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_jinritoutiao(url,htmlPath,keyStr,title)