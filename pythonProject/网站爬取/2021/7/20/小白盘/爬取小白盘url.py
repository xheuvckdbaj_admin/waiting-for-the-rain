# -*- coding:utf-8 -*-
import re
import time
import addProxyid

import addcookies
import selenium

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"E:\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\小白盘\cookies.txt"
filePathbaidu=r"E:\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\小白盘\cookiesbaidu.txt"
def getNewWindow(browser,xpathStr):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, 100).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def getBrowser(browser,newUrl):
    try:
        browser = addProxyid.addId(browser,'./ip网址.txt')
        time.sleep(5)
        browser.get(newUrl)
        return browser
    except:
        print('网络延迟')
        if not browser==None:
            browser.close()
        browser=getBrowser(browser,newUrl)
        return browser
def getElement(browser,hrefs,file_handle):
    for e in range(0, len(hrefs)):
        try:
            print(hrefs[e])
            browser.get(hrefs[e])
            time.sleep(0.2)
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="col-md-12 row "]'))
            )

            browser.find_element_by_css_selector('div.col-md-12.row > div.col-md-12.pull-left > a.btn.btn-primary').click()
            browser = getNewWindow(browser, '//span[@class="text"]')
            url=browser.current_url
            print(url)
            file_handle.write(url+'\n')

        except:
            print('资源不存在')
            continue


def getLinUrl(law,url,key,way,file_handle):
        error=law
    # try:
        # browser = getBrowser(r'https://www.lingfengyun.com/')
        # 添加cookies
        browser = webdriver.Chrome()
        browser = getBrowser(browser, 'https://www.xiaobaipan.com/')

        addLaisoucookies.addCookies(browser, 'https://www.xiaobaipan.com/', filePath)
        newUrl = url % (key,way,1)
        browser.get(newUrl)

        elements=browser.find_elements_by_xpath('//ul[@class="pagination"]/li')
        try:
            text=elements[len(elements)-1].text
            allPage=int(re.match('..(.*)',text).group(1))
        except:
            allPage=1
        for page in range(1,allPage+1):
            print('类型：',way)
            print('页码：',page)
            newUrl = url % (key, way,page)
            if page==1:
                browser.get('https://pan.baidu.com/s/1eQIaZfW')
                addcookies.addCookies(browser, 'https://pan.baidu.com/s/1eQIaZfW', filePathbaidu)
            try:
                browser.get(newUrl)
            except:
                continue
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="item-list job-item"]'))
            )
            eles=browser.find_elements_by_xpath('//div[@class="item-list job-item"]/div[@class="col-sm-10  col-xs-10  add-desc-box"]'
                                                '/div[@class="add-details jobs-item fid"]/'
                                                'h4[@class="job-title"]/a')
            print()
            hrefs=[]
            for ele in eles:
                href=ele.get_attribute('href')
                hrefs.append(href)
            print(hrefs)
            getElement(browser, hrefs,file_handle)
    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,key,path,file_handle):
    url='https://www.xiaobaipan.com/list-%s-%d-p%d.html'

    ways=[4,6]
    for way in ways:
        print(way)
        getLinUrl(law, url, key, way,file_handle)


def main():
    # keys=['计算机','公共基础','造价工程','习题答案','辅导讲义','课程标准','政府工作']
    # for key in keys:
            key='成人教育'
            print(key)
            path=r'E:\文档\小白盘url'
            law=0
            newPath = path + '\\' + key + str(time.time()) + '.txt'
            file_handle = open(newPath, mode='w')
            login(law,key,path,file_handle)
            file_handle.close()


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
