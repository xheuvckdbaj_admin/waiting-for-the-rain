# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.yunwen.com/shijianbaogao/149739.html',
                'https://www.yunwen.com/shixibaogao/149917.html',
                'https://www.yunwen.com/lizhibaogao/147650.html',
                'https://www.yunwen.com/cizhibaogao/149876.html',
                'https://www.yunwen.com/shuzhibaogao/149891.html',
                'https://www.yunwen.com/diaoyanbaogao/148996.html',
                'https://www.yunwen.com/qiuzhixin/149906.html',
                'https://www.yunwen.com/cizhixin/149879.html',
                'https://www.yunwen.com/zijianxin/149919.html',
                'https://www.yunwen.com/ganxiexin/149871.html',
                'https://www.yunwen.com/biaoyangxin/149884.html',
                'https://www.yunwen.com/daoqianxin/149799.html',
                'https://www.yunwen.com/cehuashu/149572.html',
                'https://www.yunwen.com/shenqingshu/149903.html',
                'https://www.yunwen.com/baozhengshu/149883.html',
                'https://www.yunwen.com/jianyishu/149890.html',
                'https://www.yunwen.com/yanjianggao/149912.html',
                'https://www.yunwen.com/jiayougao/149882.html',
                'https://www.yunwen.com/shuokegao/149894.html',
                'https://www.yunwen.com/fayangao/149877.html',
                'https://www.yunwen.com/zhuchigao/149224.html',
                'https://www.yunwen.com/guangbogao/149590.html',
                'https://www.yunwen.com/jianghuagao/149886.html',
                'https://www.yunwen.com/gongzuojihua/149904.html',
                'https://www.yunwen.com/jiaoxuejihua/149864.html',
                'https://www.yunwen.com/huodongjihua/149908.html',
                'https://www.yunwen.com/xuexijihua/149895.html',
                'https://www.yunwen.com/niandujihua/149922.html',
                'https://www.yunwen.com/gongzuozongjie/149865.html',
                'https://www.yunwen.com/huodongzongjie/149888.html',
                'https://www.yunwen.com/nianzhongzongjie/149887.html',
                'https://www.yunwen.com/shixizongjie/149885.html',
                'https://www.yunwen.com/gongzuofangan/149581.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)

