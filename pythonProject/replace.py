from docx import Document
import os


OLDPATH = "C:\\Users\\Administrator\\Desktop\\文档上传\\text"
PATH = "C:\\Users\\Administrator\\Desktop\\文档上传\\submit"

DICT = {
    "本文经授权转载": "新的字符串"
}
'''
替换掉某些特定文字
'''

def main():
    for fileName in os.listdir(OLDPATH):
        oldFile = OLDPATH + "\\" + fileName
        newFile = PATH + "\\" + fileName
        if oldFile.split(".")[1] == 'docx':
            document = Document(oldFile)
            document = check(document)
            document.save(newFile)


def check(document):
    # tables
    for table in document.tables:
        for row in range(len(table.rows)):
            for col in range(len(table.columns)):
                for key, value in DICT.items():
                    if key in table.cell(row, col).text:
                        print(key + "->" + value)
                        table.cell(row, col).text = table.cell(row, col).text.replace(key, value)

    # paragraphs
    for para in document.paragraphs:
        for i in range(len(para.runs)):
            for key, value in DICT.items():
                if key in para.runs[i].text:
                    print(key + "->" + value)
                    para.runs[i].text = para.runs[i].text.replace(key, value)

    return document


if __name__ == '__main__':
    main()