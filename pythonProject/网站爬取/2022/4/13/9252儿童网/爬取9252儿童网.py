# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')
def getTitleUrl(file,thmlpath, strUrl,number):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.Relevant > ul > div > li > a')
        for i, a in enumerate(a_list):
            href = a.get('href')
            url = 'http:' + href
            newUrl=url.replace('http:http:','http:')
            print(newUrl)
            getHtml(file,thmlpath, newUrl, number)
    except:
        print('error')

def getHtml(thmlpath, strUrl):

        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            ulContent=str(soup.select('ul.PContent'))
            print(strUrl)
            list_soupa = str(soup.select('div.con.relative')[0]).replace(ulContent,'')
            title = soup.select('article.bg2 > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getListUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.b_n.left > ul > li > b > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='https://www.koomao.com'+href
        getHtml(thmlpath, newUrl)

def getPageUrl(thmlpath, strUrl):
    getListUrl(thmlpath, strUrl)
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.list-page > ul > li > a')[-1].get('href')
        hrefUrl='http://www.9252.com'+href
        allPage=int(hrefUrl.split('_')[-1].replace('.html',''))
        url=hrefUrl.replace(hrefUrl.split('_')[-1],'')
        for i in range(2,allPage+1):
            newUrl=url+str(i)+'.html'
            getListUrl(thmlpath, newUrl)
    except:
        print('error')


def main(thmlpath):
    url='http://www.9252.com/jiaoan/%d.html'
    for i in range(1,93424):
        newUrl=url%i
        getHtml(thmlpath, newUrl)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
