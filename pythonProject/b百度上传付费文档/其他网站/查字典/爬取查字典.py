import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_chazidian(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        try:
            title=soup.select('div.ar_con_title > h1')[0].text 
            title=re.sub('（(.*)）', '', title)
        except:
            title = soup.select('div.article > h1')[0].text
            
        try:
            list_soupa=str(soup.select('div.con_article')[0]).replace('相关推荐','').replace(title,title)
        except:
            list_soupa = str(soup.select('div.content')[0]).replace('相关推荐', '').replace(title, title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('查字典下载成功----',title)
    except:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            try:
                title = soup.select('div.article_title > h1')[0].text.replace('（', '').replace('）', '').replace('\"', '').replace('\"', '')
            except:
                try:
                    title = soup.select('div.article > h1')[0].text.replace('（', '').replace('）', '')
                except:
                    title = soup.select('h1 > #print_title')[0].text.replace('（', '').replace('）', '').replace('(','').replace(')','')
            title = re.sub('\d+篇', '', title)
            try:
                list_soupa = str(soup.select('div.article_detail')[0]).replace('相关推荐', '').replace(title, title)
            except:
                list_soupa = str(soup.select('div.content')[0]).replace('相关推荐', '').replace(title, title)
            thmlpath = htmlPath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
            print('查字典下载成功----', title)
        except:
            print('error')

if __name__ == '__main__':
    url = 'https://www.chazidian.com/fanwen/270996/'
    htmlPath=r'D:\文档\百度付费上传文档\百度html'
    keyStr='kaixin'
    title=''
    mian_chazidian(url,htmlPath,keyStr,title)