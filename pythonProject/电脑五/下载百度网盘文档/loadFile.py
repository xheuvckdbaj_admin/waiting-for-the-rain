import time

import requests
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import addcookies
import getCode

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def set_chrome_pref(loadFilePath):
    prefs = {"download.default_directory": loadFilePath}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)  # 打开chrome浏览器
    time.sleep(10)
    return driver
def getNewWindow(browser,xpathStr,number):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-原创力下载])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, number).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def yanzhengma(browser,path):
    href = browser.find_element_by_css_selector('img.img-code').get_attribute('src')
    try:
        sponse = requests.get(url=href, headers=headers).content
    except:
        print('请求图片页面出错')
        yanzhengma(browser,path)
    newPath = path + '\\' + str(time.time()) + '.jpg'
    file_handle = open(newPath, mode='wb')
    file_handle.write(sponse)
    file_handle.close()
    code = getCode.getCode(newPath)
    browser.find_element_by_css_selector('div.verify-body > input.input-code').send_keys(code)
    browser.find_element_by_css_selector('div.verify-body > input.input-code').send_keys(Keys.ENTER)
    try:
        getNewWindow(browser,'//a[@class="underline"]',10)
        browser.find_element_by_css_selector('a.underline').click()
        print('验证码错误')
        getNewWindow(browser,'//a[@class="underline"]',10)
        yanzhengma(browser, path)

    except:
        time.sleep(5)
        print('成功')



def getshopUrl(filePath,number,path,loadFilePath):
    file_line=open(filePath,'r')
    lines=file_line.readlines()
    browser=set_chrome_pref(loadFilePath)
    for i in range(number,len(lines)):
        if 'baidu' in lines[i]:
            print(i)
            print(lines[i])
            try:
                browser.get(lines[i])
                browser.find_element_by_class_name('zbyDdwb').click()
            except:
                print('error')
            try:
                browser.find_elements_by_css_selector('span.text')[1].click()

            except:
                continue
            try:
                browser = getNewWindow(browser, '//div[@class="g-clearfix g-center"]/div[@class="normalBtnBox"]/'
                                                   'a[@class="g-button g-button-large g-button-gray-large"]/span[@class="g-button-right"]', 2)
                browser.find_element_by_xpath('//div[@class="g-clearfix g-center"]/div[@class="normalBtnBox"]/'
                                                   'a[@class="g-button g-button-large g-button-gray-large"]/span[@class="g-button-right"]').click()
            except:
                print('不存在极速下载')
            try:
                browser=getNewWindow(browser, '//a[@class="underline"]',10)
            except:
                continue
            yanzhengma(browser, path)

def main():
    filePath=r'G:\文档\兄弟盘\all.txt'
    # number初始值为0
    start=702
    #结束自己定
    end=500
    path=r'G:\验证码图片'
    loadFilePath=r"G:\360Downloads"
    getshopUrl(filePath,start,path,loadFilePath)
if __name__ == '__main__':
    main()