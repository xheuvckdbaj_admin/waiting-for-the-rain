import os
import random
import win32com.client as win32
from docx import Document

from FindFile import find_name, find_file
from 文档标题 import getFileName


def getTitlePath(path,fileName):
    doc1 = Document()
    doc1.add_heading(fileName, 0)
    filePath = path + "\\" + 'all.docx'
    doc1.save(filePath)
    return filePath


def getFiles(path,newPath,string,string2,number):
    word = win32.gencache.EnsureDispatch('Word.Application')
    # 非可视化运行
    word.Visible = False



    # 拼接文档

    file_list = []
    files1 = find_name(path, [])

    for file in files1:
        if string in file and string2 in file and not '~$' in file:
            file_list.append(file)

    fileNumber=int(len(file_list)/10)
    for i in range(0,fileNumber):
        output = word.Documents.Add()  # 新建合并后空白文档
        fileName = ''
        for i_number in range(1,number+1):
            file_Name1 = file_list[random.randint(0, len(file_list) - 1)]
            if i_number==number:
                fileName=file_Name1
                print(fileName)
            output.Application.Selection.Range.InsertFile(os.path.join(path, file_Name1))
            #file_list.append(files1[random.randint(0, len(files1) - 1)])
            #print(files1[random.randint(0, len(files1) - 1)])

        output.Application.Selection.Range.InsertFile(getTitlePath(path,fileName))
            # 获取合并后文档的内容
        doc = output.Range(output.Content.Start, output.Content.End)
        doc.Font.Name = "黑体"  # 设置字体
        output.SaveAs(newPath + '\\' + fileName + '.docx')  # 保存
        output.Close()
    return file_list

def main():
    str_list=[
              '寒假计划','实习计划','年度工作计划','工作计划大全','教学工作计划',
        '学习计划', '财务工作计划', '培训工作计划', '下半年工作计划', '学生会工作计划',
        '工作计划表', '少先队工作计划', '工作计划', '暑假计划', '办公室工作计划',
              '德育工作计划','班级工作计划','教师工作计划','幼儿园工作计划','安全工作计划',
              '个人工作计划','工作计划书','工会工作计划','团支部工作计划','党支部工作计划','销售工作计划',
              '护理工作计划','慰问信','承诺书','决心书','毕业生自我鉴定','自我评价','团员自我鉴定','自我鉴定','大学生自我鉴定','工作自我鉴定','个人自我鉴定','党员自我鉴定',
              '实习自我鉴定','自我评定','企业演讲稿','元旦演讲稿','文明礼仪演讲稿','演讲与口才','竞争上岗演讲稿','演讲稿','环保演讲稿','安全演讲稿','师德演讲稿','清明节演讲稿',
              '母亲节演讲稿','就职演讲稿','发言稿','开学典礼发言稿','毕业典礼发言稿','雷锋日演讲稿','读书演讲稿','梦想演讲稿','理想演讲稿','升国旗仪式演讲稿','三分钟演讲稿','民族团结演讲稿',
              '励志演讲','演讲比赛','即兴演讲','护士节演讲','教师节演讲','国旗下演讲稿','大学生演讲稿','感恩演讲稿','教师演讲稿','家长会演讲稿','小学生演讲稿','运动会演讲稿',
              '英语演讲稿','竞选学生会演讲','竞聘演讲','爱国演讲','中学生演讲稿','诚信演讲稿','三八妇女节演讲稿','六一儿童节演讲稿','领导讲话稿','十一国庆节演讲稿','七一建党节演讲','五四青年节演讲',
              '竞选演讲','发言','爱岗敬业演讲','青春演讲','演讲技巧','感谢信','建议书','介绍信','借条','辞职信','心得体会','教育心得体会',
              '安全心得体会','学习心得体会','教师心得体会','军训心得体会','师德师风心得体会','工作心得体会','培训心得体会','读书心得体会','实习心得体会','社会实践心得体会','表扬信','责任书',
              '融资合同','合同书','合同','委托合同','加工合同','广告合同','代理合同','转让合同','聘用合同','承包合同','劳务合同','合作合同',
              '供货合同','集体合同','加盟合同','用工合同','贸易合同','出版合同','仓储保管合同','担保合同','银行信托合同','协议书','家庭合同','教育合同',
              '涉外合同','医药合同','行业合同',
              '服务合同','建筑合同','施工合同','招标合同','投资合同','经营合同','房地产合同','保险合同','技术合同','知识产权合同','建设工程合同','融资租赁合同',
              '承揽合同','劳动合同','证券合同','赠与合同','合同','采购合同','购房合同','销售合同','装修合同','购销合同','租房合同','租赁合同',
              '借款合同','买卖合同','协议','商标专利合同','运输合同','道谢信','请假条','讲话稿','申请书','分手信','证明书','邀请函',
              '口号','军训口号','环保标语','公司宣传语','警示语','爱国标语','班级口号','企业口号','团队口号','团队口号','温馨提示语','安全标语',
              '服务口号','激励口号','运动会口号','标语','广告语','保证书','协议书','评语','幼儿园小班评语','班主任评语','寄语','幼儿园小班寄语',
              '班主任寄语','教师寄语','家长寄语','家长评语','员工评语','作文评语','教师评语','日常评语','差生评语','幼儿园中班评语','幼儿园大班评语','小学生评语',
              '初中生评语','高中生评语','期末评语','学生品德评语','优秀学生评语','中等生评语','新年寄语','公证书','导游词','担保书','活动总结','社会活动总结',
              '校园活动','班级活动','委托书','意向书','说明书','聘任书','挑战书','竞聘书','通知书','留言条','收据','领条',
              '细则','条例','贫困申请书','贫困证明','转正申请书','策划书','通讯稿','广播稿','报告','工作报告','实习报告','述职报告',
              '实习日记','实习','整改报告','整改方案','整改措施','请示报告','竞聘报告','申请报告','社会实践报告','辞职报告','自查报告','情况报告',
              '事迹材料','申报材料','调研报告','调查报告','考察报告','实验报告','社会调查报告']
    path = r'E:\word文档库1'
    newPath=r'E:\卷子1'
    for str in str_list:
        string=str
        string2=''
        number=4
        # getTitlePath(path)
        file_list = getFiles(path,newPath,string,string2,number)


if __name__ == '__main__':
    main()