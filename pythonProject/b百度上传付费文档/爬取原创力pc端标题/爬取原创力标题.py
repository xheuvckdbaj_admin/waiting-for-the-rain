import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)

def getTitleNumber(zimuStr):
    # 0为a,1为b,2为c,3为d,4为e,5为f,6为g,7为h,8为i,9为j,10为k,11为m,12为n,13为o,14为p,15为q,16为q,17为r,18为s,19为t,20为u,21为v,22为w,23为x
    titleNumber=0
    if zimuStr=='a':
        titleNumber=0
    elif zimuStr=='b':
        titleNumber=1
    elif zimuStr=='c':
        titleNumber=2
    elif zimuStr=='d':
        titleNumber=3
    elif zimuStr=='e':
        titleNumber=4
    elif zimuStr=='f':
        titleNumber=5
    elif zimuStr=='g':
        titleNumber=6
    elif zimuStr=='h':
        titleNumber=7
    elif zimuStr=='i':
        titleNumber=8
    elif zimuStr=='j':
        titleNumber=9
    elif zimuStr=='k':
        titleNumber=10
    elif zimuStr=='m':
        titleNumber=11
    elif zimuStr=='n':
        titleNumber=12
    elif zimuStr=='o':
        titleNumber=13
    # 0为a,1为b,2为c,3为d,4为e,5为f,6为g,7为h,8为i,9为j,10为k,11为m,12为n,13为o,
    # 14为p,15为q,16为q,17为r,18为s,19为t,20为u,21为v,22为w,23为x
    elif zimuStr=='p':
        titleNumber=14
    elif zimuStr=='q':
        titleNumber=15
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0
    elif zimuStr=='a':
        titleNumber=0

def run(playwright: Playwright, titlePath, numbers,data) -> None:
    browser = playwright.chromium.launch(headless=True)
    context = browser.new_context()
    page = context.new_page()
    time.sleep(1)
    page.goto("https://max.book118.com/")
    with page.expect_popup() as page1_info:
        page.get_by_role("link", name="海量文档").click()
    page1 = page1_info.value
    time.sleep(2)
    xpath = '//div[@class="nav"]/ul[@class="list"]/li'
    elements = page1.locator(xpath)
    # zimu=data[0:1]
    # if
    elementTwoNumber=0
    for ia in range(0, elements.count()):
        if ia==numbers[0]:
            time.sleep(3)
            twoTitlePath = titlePath + '\\' + str(ia)
            elements.nth(ia).click()
            time.sleep(1)
            xpathTwo = '//div[@class="introduce"]/ul[@class="list"]/li'
            elementTwos = page1.locator(xpathTwo)
            elementTwoNumber=elementTwos.count()
            if data < elementTwoNumber:
                titleNumber=data
                for i in range(0, elementTwos.count()):
                    if i==titleNumber:
                        threTitlePath = twoTitlePath + '\\' + str(i)
                        makeFolder(threTitlePath)
                        elementTwos.nth(i).click()
                        xpathTitle = '//div[@class="doc"]/ul[@class="doc-list"]/li/div[@class="title "]/a[@target="_blank"]'
                        elementThrees = page1.locator(xpathTitle)
                        for number in range(0, 50):
                            try:
                                newTitlePath = threTitlePath + '\\' + str(number) + '.txt'
                                file = open(newTitlePath, 'w', encoding='utf-8')
                                for i in range(0, elementThrees.count()):
                                    title = elementThrees.nth(i).get_attribute('title')
                                    title = title.replace('.docx', '').replace('.doc', '').replace('.ppt', '') \
                                        .replace('.pptx', '').replace('.pdf', '').strip()
                                    print(title)
                                    file.write(title + '\n')
                                file.close()
                                page1.get_by_role("link", name="下一页").click()
                            except Exception as e:
                                print(str(e))
                            time.sleep(600)
        elif ia==numbers[1]:
            time.sleep(3)
            twoTitlePath = titlePath + '\\' + str(ia)
            elements.nth(ia).click()
            time.sleep(1)
            xpathTwo = '//div[@class="introduce"]/ul[@class="list"]/li'
            elementTwos = page1.locator(xpathTwo)
            titleNumber = data - elementTwoNumber
            if titleNumber >=elementTwos.count():
                print('所有标题爬取完毕')
            if data >= elementTwoNumber:
                for i in range(0, elementTwos.count()):
                    if i==titleNumber:
                        threTitlePath = twoTitlePath + '\\' + str(i)
                        makeFolder(threTitlePath)
                        elementTwos.nth(i).click()
                        xpathTitle = '//div[@class="doc"]/ul[@class="doc-list"]/li/div[@class="title "]/a[@target="_blank"]'
                        elementThrees = page1.locator(xpathTitle)
                        for number in range(0, 50):
                            try:
                                newTitlePath = threTitlePath + '\\' + str(number) + '.txt'
                                file = open(newTitlePath, 'w', encoding='utf-8')
                                for i in range(0, elementThrees.count()):
                                    title = elementThrees.nth(i).get_attribute('title')
                                    title = title.replace('.docx', '').replace('.doc', '').replace('.ppt', '') \
                                        .replace('.pptx', '').replace('.pdf', '').strip()
                                    print(title)
                                    file.write(title + '\n')
                                file.close()
                                page1.get_by_role("link", name="下一页").click()
                            except Exception as e:
                                print(str(e))
                            time.sleep(300)

        # page1.get_by_role("link", name="下一页").click()
        # page1.goto("https://max.book118.com/doclist/list-317-4.html")
        # page1.get_by_role("link", name="计算机应用/办公自动化").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name=" 外语学习 ").click()
        # page1.get_by_role("link", name="俄语").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name="下一页").click()
        # page1.get_by_role("link", name=" 报告/分析 ").click()

        # ---------------------
        # context.close()
        # browser.close()


def yuanchuangli(titlePath, numbers,data):
    with sync_playwright() as playwright:
        run(playwright, titlePath, numbers,data)


def main():
    titlePath = r'D:\文档\百度付费上传文档\百度关键词标题\原创力pc标题'
    numbers = [0,1]
    #0为a,1为b,2为c,3为d,4为e,5为f,6为g,7为h,8为i,9为j,10为k,11为l,12为m,13为n,14为o,15为p,16为q,17为r,18为s,19为t,20为u,21为v,22为w,23为x,24为y
    data=1
    yuanchuangli(titlePath, numbers,data)


if __name__ == '__main__':
    main()
