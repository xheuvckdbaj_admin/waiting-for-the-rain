# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getHtml2(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.arccon')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.arctitle > h1')[0].text).split('\n')[-1]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.zoom')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.title > h1')[0].text).split('\n')[-1]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.slist1 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.pmceo.com'+href
        print(newUrl)
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,url):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.mypage > a')[-1].get('href')
        pageNumber=int(href.split('-')[-1].replace('.html',''))
        hrefUrl='http://www.pmceo.com'+href.replace(href.split('-')[-1],'')+'%d.html'
        for i in range(1,pageNumber+1):
            print('---------',i)
            newUrl=hrefUrl%i
            getListUrl(thmlpath, newUrl)
    except:
        print('只有一页')
def getLawTitle(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('table > tr > td.kk > span > #A0')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl = 'http://www.pmceo.com' + href
        getListUrl(thmlpath, newUrl)
        getPageUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('div.menu > ul > li > a')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            newUrl='http://www.pmceo.com'+href
            getLawTitle(thmlpath, newUrl)
def main(thmlpath):
   url='http://www.pmceo.com/'
   getTitleUrl(thmlpath, url)
if __name__ == '__main__':

    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
