# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'https://yuwen.chazidian.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    list_soupa = str(soup.select('.allshowbox')[0])
    #print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)
#获取最终的地址
def getUrlIndex(path,url,title):
    for i in range(1,50):
        try:
            newUrl=''
            if i==1:
                newUrl=url
            else:
                newUrl=url+'?page='+str(i)
            requests_text = str(requests.get(url=newUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            a_list=soup.select('#mulu > .mldy1 > .ml_t1 > h3 > a')
            #print(url_list)
            for a in a_list:
                    href=a.get('href')
                    print(href)
                    title = a.get('title')

                    getContent(path,href,title)
        except:
            print('错误')
            continue
# 获取中间网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    div_list=soup.select('#mulu > .mldy > .ml_c')
    print(div_list)
    for div in div_list:
         a_list1=div.select('a')
         for i in range(0,3):
             print(i)
             href=a_list1[i].get('href')
             title = a_list1[i].get('title')
             getUrlIndex(thmlpath, href,title)
    #         title=li.select('a')[0].get('title')
    #         newUrl=f'https://fanwen.chazidian.com'+href
    #         getUrlPageNum(thmlpath, newUrl)
    # print(url_list)
    # for u in url_list:
    #         li_list=u.select('ul > li')
    #         for li in li_list:
    #             title=li.select('a')[0].get('title')
    #             href=li.select('a')[0].get('href')
    #             newUrl=f'https://fanwen.chazidian.com'+href
    #             #print(title)
    #             #print(newUrl)
    #             getContent(thmlpath, newUrl, title)


def getUrlPageNum(path,url):
    for pageNum in range(1,1000):
        try:
                    oldUrl=url+'%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(path, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str1 = ['yinianji','ernianji','sannianji','sinianji','wunianji','liunianji']
    str2 = ['chuyi', 'chuer', 'chusan']
    str3=['gaoyi', 'gaoer', 'gaosan']
    copy_list=['shangce','xiace']
    number_list=['32','33','34','35','36','37']
    string_lis=[]
    for str in str1:
        for copy in copy_list:
            for number in number_list:
                print(str+copy+number)
                string_lis.append(str+copy+number)
    for str in str2:
        for copy in copy_list:
            for number in number_list:
                print(str+copy+number)
                string_lis.append(str+copy+number)
    for str in str3:
        for copy in copy_list:
                print(str+copy)
                string_lis.append(str+copy)

    for s in string_lis:
        try:
         url = groupUrl(s)
         getUrl(thmlpath, url)
        except:
            print('错误')
            continue


if __name__ == '__main__':
    thmlpath = r'E:\语文网教案'
    main(thmlpath)
