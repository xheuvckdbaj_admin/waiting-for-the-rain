# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from pythonProject.FindFile import find_file, find_name
from pythonProject.初中.敏感词过滤.minganci import filterWord

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


def deletePara(file):
    for para in file.paragraphs:
        para.text = para.text.replace('\n', ' ')
        if para.text == "" or para.text == '\n' or para.text == ' ':
            # print('第{}段是空行段'.format(i))
            para.clear()  # 清除文字，并不删除段落，run也可以,
            delete_paragraph(para)


def tihuanguanjianci(lines, para):
    for line in lines:
        keyValue = line.replace('\n', '')
        key = keyValue.split('===>')[0]
        # print(key)
        value = keyValue.split('===>')[1]
        para.text = para.text.replace(key, value)

titlePath='./标题违禁词.txt'
def panduanFileName(fileName):
    file=open(titlePath,'r',encoding='utf-8')
    lines=file.readlines()
    panduan=True
    for i,line in enumerate(lines):
        key=line.replace('\n','')
        panduan = panduan and (not key in fileName)
    return panduan


def deleteLaterParaPanduan(i, content, fileName):
    panduan = False
    content = content.replace(' ', '')
    if (not (i == 1 and i == 2)) and len(content) < 100:
        titleContent = fileName.replace('.docx', '').replace('[', '').replace(']', '').replace('【', '').replace('】',
                                                                                                                '').replace(
            '(', '').replace(')', '').replace(' ', '')
        content = content.replace('[', '').replace(']', '').replace('【', '').replace('】', '').replace('(', '').replace(
            ')', '').replace(' ', '')
        panduan = ((content[-5:-1] == titleContent[-5:-1]) or (content[0:5] == titleContent[0:5]))
    if content == '-' or content == '1' or content == '2' or content == '3' or content == '4' or content == '5':
        panduan = True
    return panduan

paghraPath='./删除活动文档段落违禁词.txt'
def panduanPaghra(paghraPath,content):
    file = open(paghraPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan

filterTxt='./内容敏感词.txt'
def filterDocx(panduan,filterTxt,content):
    file=open(filterTxt,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        key=line.replace('\n','')
        panduan=panduan or (key in content)
    return panduan

paghraDownPath='./删除以下段落的违禁词.txt'
def panduanPaghraDown(paghraDownPath,content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan
def deleAdvertising(filePath, newFolderPath, midFolderPath, newfileNames, lines, front, later):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        try:
            file = docx.Document(filePath)

            fileName = os.path.basename(filePath)
            if len(fileName)<10:
                fileName=fileName+'范文'
            if panduanFileName(fileName):
                # print("段落数:" + str(len(file.paragraphs)))
                # print('删除前图形图像的数量：', len(file.inline_shapes))
                str = []
                contentStr=''
                for i,para in enumerate(file.paragraphs):
                    # 替换关键词，洗稿
                    tihuanguanjianci(lines, para)
                    contentStr=contentStr+para.text
                    # para.text=modifyString(para.text)
                    # print(para.text)
                    if panduanPaghra(paghraPath,para.text):
                        str.append(i)
                    if deleteLaterParaPanduan(i, para.text, fileName):
                        str.append(i)
                    if len(para.text) < 100 and panduanPaghraDown(paghraDownPath,para.text):
                        print(para.text)
                        for par in range(i, len(file.paragraphs)):
                            str.append(par)
                for i in str:
                    paragraph = file.paragraphs[i]
                    contentStr.replace(paragraph.text,'')
                    paragraph.clear()
                deletePara(file)
                # print('删除前图形图像的数量：', len(file.inline_shapes))   -2021学年第二学期教学工作总结
                fileName = fileName.replace('2016', '2023').replace('2017', '2023').replace(
                    '2018', '2023').replace('2019', '2023').replace('2020', '2023').replace('【微信公众号：wkgx985 免费获取】',
                                                                                            '').replace('2015',
                                                                                                        '2023').replace(
                    '2010', '2023').replace('2008', '2023').replace('2009', '2023').replace('2007', '2023').replace(
                    '2006', '2023').replace('2005', '2023').replace(
                    '2011', '2023').replace('2012', '2023').replace('2013', '2023').replace('2014', '2023').replace(
                    '2019', '2023').replace(
                    '2020', '2023').replace('2004', '2023') \
                    .replace('2017', '2023').replace('2018', '2023') \
                    .replace('2015', '2023').replace('2016', '2023').replace('2021', '2023') \
                    .replace('._', '').replace('★', '').replace('#', '') \
                    .replace('•', '').replace('！', '').replace('?', '').replace('？', '') \
                    .replace('•', '').replace('~', '').replace('✓', '').replace('.', '').replace('+', '') \
                    .replace(',', '').replace('docx', '').replace('公众号：', '') \
                    .replace('､', '').replace('。', '').replace('▪', '').replace('，', '') \
                    .replace('．', '').replace('公众号:', '').replace('：', '') \
                    .replace('、', '').replace('（喜子的商铺）', '').replace('·', '') \
                    .replace('原创', '精品').replace('转载', '精品') \
                    .replace('&lt', '精品').replace(';', '精品').replace('&gt', '精品') \
                    .replace('11', '').replace('＊', '').replace(',', '')
                file_name2 = ''.join(fileName.split())
                file_name1 = re.sub('【.*?】', '', file_name2)
                file_name = re.sub('www.*?com', '', file_name1)
                paths = r'./敏感词过滤/minganci.txt'
                panduan=False
                if len(contentStr) > 500 and (not filterDocx(panduan,filterTxt,contentStr)):
                    if file.save(newFolderPath + '\\' + file_name + '.docx') == None:
                        # newFile = docx.Document(newFolderPath + '\\' + fileName)
                        # delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                        os.remove(filePath)  # 删除原来修改前的docx文件
                    if file_name in newfileNames:
                        os.remove(filePath)
                else:
                    file.save(midFolderPath + '\\' + fileName+ '.docx')
                    os.remove(filePath)
            else:
                file.save(midFolderPath + '\\' + fileName+ '.docx')
                os.remove(filePath)
        except:
            print('错误')
# word.Quit()
def shear_dile(oldFolderPath,newFolderPath,midFolderPath, newfileNames, lines):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i,d in enumerate(os.listdir(oldFolderPath)):
                    shear_dile(os.path.join(oldFolderPath, d),newFolderPath, midFolderPath, newfileNames, lines)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.docx'):
            print(oldFolderPath)
            deleAdvertising(oldFolderPath, newFolderPath,midFolderPath, newfileNames, lines, 0, 0)

def modifyContent(oldFolderPath, newFolderPath,midFolderPath):
    newfileNames = find_name(newFolderPath, [])
    paths = r'./doc_replace.txt'
    fileTxt = open(paths, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    shear_dile(oldFolderPath,newFolderPath,midFolderPath, newfileNames, lines)

def main1():
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'E:\文档\豆丁文档\百度文档html3'
    newFolderPath = r'E:\文档\豆丁文档\百度文档html2'
    midFolderPath = r'E:\文档\自查报告网url'
    modifyContent(oldFolderPath, newFolderPath,midFolderPath)


if __name__ == '__main__':
    main1()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
