# -*- coding:utf-8 -*-
import re
import time



import requests
import selenium
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import checkIp
import addcookies
import readTxt
import getbaiduurl
import addProxyid
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=原创力下载'}
filePath=r"C:\Users\Administrator\Desktop\cookies.txt"
filePath1=r"./ip网址.txt"
def set_chrome_pref():
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    driver = webdriver.Chrome(chrome_options=option)  # 打开chrome浏览器
    time.sleep(10)
    return driver
def getBrowser(browser,number,page,newUrl):
    try:
        if page % 50 == (number % 50):
            browser = addProxyid.addId(browser,filePath1)
            time.sleep(5)
        browser.get(newUrl)
        return browser
    except:
        print('网络延迟')
        browser=getBrowser(browser,page,page,newUrl)
        return browser


def getUrl(file_handle, hrefs, browser):
    for href in hrefs:
        browser.get(href)
        element = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located(
                (By.XPATH, '//button[@class="btn btn-success"]'))
        )
        browser.find_element_by_css_selector('button.btn.btn-success').click()
        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        # browser.switch_to_window(n[-原创力下载])  # driver切换至最新生产的页面
        browser.switch_to.window(n[-1])
        time.sleep(1)
        browser.switch_to.window(browser.window_handles[0])
        element = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="modal-content"]/div[@class="modal-body"]/div/a[@rel="noreferrer"]'))
        )
        newUrl = browser.find_element_by_xpath(
            '//div[@class="modal-content"]/div[@class="modal-body"]/div/a[@rel="noreferrer"]').get_attribute('href')
        print('baidu:',newUrl)
        file_handle.write(newUrl + '\n')
def login(key,file_handle,number,i):
    browser = set_chrome_pref()
    errors=number
    try:
        strUrl='https://www.fastsoso.cn/search?page=%d&k=%s'

        for page in range(number,201):
                errors=page
                print('key',key)
                print('page',page)
                newUrl = strUrl % (page, key)
                browser=getBrowser(browser,number,page,newUrl)
                try:
                    element = WebDriverWait(browser, 100).until(
                                    EC.presence_of_element_located((By.XPATH, '//div[@name="content-title"]/strong/a'))
                                )
                    elements=browser.find_elements_by_xpath('//div[@name="content-title"]/strong/a')
                except:
                    print('该关键词已经下载完毕')
                    browser.quit()
                    break
                hrefs=[]
                for leng in range(0,len(elements)):
                        element=elements[leng]
                        href=element.get_attribute('href')
                        print(href)
                        hrefs.append(href)
                getUrl(file_handle, hrefs, browser)

    except:
            time.sleep(20)
            print('error')
            browser.quit()
            login(key,file_handle,errors,i)
def main(thmlpath):
    #number初始值为1
    number = 1
    keys=['主题班会','结构调整','膳食指南','会计准则','管理办法','学法用法'
          ,'应急演练‘,’安全教育','发言稿','条例','普法考试','自主招生','单招考试','活动简报'
          ,'放假通知‘,’实施意见','高职分类','网络安全']
    for key in keys:

            strUrl='https://www.xiongdipan.com/file/info?uid=Jb616b7L6&aid=L2c1dfe4z8469f09f0&v=3'
            # key='公共基础知识'
            path = r'G:\文档\兄弟盘'
            newPath = path + '\\' +key+ str(time.time()) + '.txt'
            file_handle = open(newPath, mode='w')
            i=0
            login(key,file_handle,number,i)
            number=1
            file_handle.close()


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main(thmlpath)
