import re

391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):
    try:
        print(strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('div.view_td')[0]).replace('<br>', '<p><p/>').replace('<br/>',
                                                                                           '<p><p/>').replace(
            ' 　　', '<p><p/>').replace('　', '<p><p/>').replace(' ', '<p><p/>')
        title = soup.select('div.article_title > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('ul > div > li.t > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl = 'http://www.baby611.com' + href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.pages > div.plist > a')[-2].get('href')
        pageNumber=int(href.split('-')[-1].replace('.html',''))
        url=strUrl.replace('index.html',href.split('-')[0])+'-%d.html'
        for i in range(1,pageNumber+1):
            newUrl=url%i
            getListUrl(thmlpath, newUrl)
    except:
        getListUrl(thmlpath, newUrl)

def getSmallTitleUrl(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.r_317_t > span.tt > a')
        for i, a in enumerate(a_list):
            href = a.get('href')
            newUrl = 'http://www.baby611.com' + href
            getPageUrl(thmlpath, newUrl)
    except:
        getPageUrl(thmlpath, strUrl)

def getTitleUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.r_317_t > span.tt > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.baby611.com'+href
        getSmallTitleUrl(thmlpath, newUrl)
def main(thmlpath):

        url='http://www.baby611.com/jiaoan/'
        getTitleUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
