# -*- coding:utf-8 -*-
import re
import time

import docx
import requests

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(url):

        #time.sleep(10)
            #url='http://www.beiku.com/xieyishu/article_145325.html'
        try:
            time.sleep(10)
            print(url)
            requests_text = requests.get(url=url, headers=headers).content

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#contents')[0])
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            return list_soupa
        except:
            print('error')
            return ''

# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = requests.get(url=url, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.pages > a')
    try:
        pageStr=a_list[0].get('title')
        num=int(re.match('当前第(.*?)页，共(.*?)页',pageStr).group(2))
        newUrl=url.split('.html')[0]+'-%d.html'
    except:
        print('error')
        num=1
        newUrl = url.split('.html')[0] + '-%d.html'
    content=''
    for i in range(1,num+1):
        newHref=newUrl%i
        content=content+getContent(newHref)
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)


def getHtml3(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.lista > p > a')
    for a in a_list:
        href=a.get('href')
        newUrl='https://www.wendangxiazai.com'+href
        title=a.get('title')
        print(title)
        getUrl(thmlpath,newUrl,title)
def getHtml2(thmlpath, strUrl):
    numStr=re.match('(.*?)-(.*?)-(.*?).html',strUrl).group(3)
    newUrl=strUrl.split(numStr+'.html')[0]+'%d.html'
    for i in range(0,200):
        num=i*25
        newHref=newUrl%num
        getHtml3(thmlpath, newHref)
def getHtml(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.menu > a')
    for i in range(0, len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        newUrl='https://www.wendangxiazai.com'+href
        getHtml2(thmlpath, newUrl)
def main(thmlpath):
            number=1
            strUrl='https://www.wendangxiazai.com/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档下载'
    main(thmlpath)
