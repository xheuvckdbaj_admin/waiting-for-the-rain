# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import time
import urllib

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options=webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser=webdriver.Chrome(options=options)
    time.sleep(1)
    return browser

def addCookies(browser, url, filePath):
        browser.get(url)
        time.sleep(1)
        cookies = readTxt.readTxt(filePath)
        for item in cookies.split(';'):
            cookie = {}
            itemname = item.split('=')[0]
            iremvalue = item.split('=')[1]
            cookie['domain'] = '.baidu.com'
            cookie['name'] = itemname.strip()
            cookie['value'] = urllib.parse.unquote(iremvalue).strip()
            browser.add_cookie(cookie)
        browser.get(url)

def getNewWindow(browser,xpathStr,timeNumber):
    n=browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element=WebDriverWait(browser,timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH,xpathStr)
        )
    )
def getTitle(browser,file):
    xpathStr='//div[@class="body-cell align-left el-col el-col-7"]'
    getNewWindow(browser, xpathStr, 10)
    elements=browser.find_elements(By.XPATH,xpathStr)
    for element in elements:
        title=element.text
        print(title)
        file.write(title+'\n')

def ClickNextPage(browser,pageNumber):
    try:
        #pageNumber最大为1，最小为0
        xpathStr='//button[@class="btn-next"]'
        browser.find_elements(By.XPATH,xpathStr)[0].click()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(2)
    except:
        print('点击下一页失败')

def mainKeyTitle(folderPath,cookiesPath):
   browser=webdriver.Chrome()
   url='https://cuttlefish.baidu.com/shopmis#/order'
   addCookies(browser, url, cookiesPath)
   time.sleep(3)
   xpathStr = '//li[@class="el-menu-item"]/span[@class="el-main-new"]'
   titleEl = browser.find_elements(By.XPATH, xpathStr)[6]
   ActionChains(browser).click(titleEl).perform()
   getNewWindow(browser, xpathStr, 10)
   # getTitle(browser)
   # browser=getBrowserNow()
   filePath = folderPath + '\\' + str(1) + '.txt'
   file=open(filePath, 'w', encoding='utf-8')
   getTitle(browser,file)
   file.close()
   for pageNumber in range(0,1500):
       filePath = folderPath + '\\' + str(pageNumber+2) + '.txt'
       file = open(filePath, 'w', encoding='utf-8')
       ClickNextPage(browser, pageNumber)
       getTitle(browser,file)
       file.close()

if __name__ == '__main__':
    folderPath = r'D:\文档\百度付费上传文档\百度关键词标题'
    cookiesPath='./baiduCookies1.txt'
    mainKeyTitle(folderPath,cookiesPath)