# -*- coding:utf-8 -*-
import os

from pythonProject.FindFile import find_file
from pythonProject.文件剪切 import ctrlx

'''
把文件夹内部的东西提出来
'''
def shear_dile(src):
    filePaths=find_file(src)
    for filePath in filePaths:
        if filePath.endswith('.html'):
            docxFile=filePath.replace('.html','.docx')
            if docxFile in filePaths:
                print(filePath)
                os.remove(filePath)
def main():
    src=r'D:\文档\自查报告网'
    shear_dile(src)
if __name__ == '__main__':
    main()