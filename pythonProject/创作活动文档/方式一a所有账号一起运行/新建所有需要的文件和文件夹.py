import os


def makeFolder(filePath):
    if  not os.path.exists(filePath):
        os.makedirs(filePath)
        print(filePath,'创建成功！！！')
    if '修改后百度活动文档' in filePath:
        newTxtPath=filePath+'\\'+'a.txt'
        file=open(newTxtPath,'w',encoding='utf-8')
        file.close()

def main(wendang,huodongwendang,htmlwendang,qitawangzhan,titlewenjianjia,passTitlewenjianjia,uploadwenjianjia,modifywenjianjia,classflywenjianjia,
         zhonjiewenjianjia,passwenjianjia,noPasswenjianjia):
    makeFolder(wendang)
    makeFolder(huodongwendang)
    makeFolder(htmlwendang)
    makeFolder(qitawangzhan)
    makeFolder(titlewenjianjia)
    makeFolder(passTitlewenjianjia)
    makeFolder(uploadwenjianjia)
    makeFolder(modifywenjianjia)
    makeFolder(classflywenjianjia)
    makeFolder(zhonjiewenjianjia)
    makeFolder(passwenjianjia)
    makeFolder(noPasswenjianjia)

if __name__ == '__main__':
    for i in range(1,26):
        numberStr = str(i)
        if i == 1:
            numberStr = ''
        wendang=r'D:\文档'
        huodongwendang=r'D:\文档\百度活动文档'+numberStr
        htmlwendang=r'D:\文档\百度活动文档'+numberStr+'\\'+'百度html'
        qitawangzhan=r'D:\文档\百度活动文档'+numberStr+'\\'+'百度html\\其他网站'
        titlewenjianjia=r'D:\文档\百度活动文档'+numberStr+'\\'+'百度活动文档标题'
        passTitlewenjianjia=r'D:\文档\百度活动文档'+numberStr+'\\'+'百度活动文档上传过的标题'
        uploadwenjianjia=r'D:\文档\百度活动文档'+numberStr+'\\'+'百度活动文档上传'
        modifywenjianjia=r'D:\文档\百度活动文档'+numberStr+'\\'+'修改后百度活动文档'
        classflywenjianjia=r'D:\文档\百度活动文档'+numberStr+'\\'+'用来分类文档标题'
        zhonjiewenjianjia=r'D:\文档\百度活动文档'+numberStr+'\\'+'中介'
        passwenjianjia=r'D:\文档\别人上传过的标题'+'\\'+'账号'+numberStr
        noPasswenjianjia=r'D:\文档\一个周期没传过的标题'+'\\'+'账号'+numberStr
        main(wendang, huodongwendang, htmlwendang, qitawangzhan, titlewenjianjia,passTitlewenjianjia, uploadwenjianjia, modifywenjianjia,
             classflywenjianjia, zhonjiewenjianjia,passwenjianjia,noPasswenjianjia)