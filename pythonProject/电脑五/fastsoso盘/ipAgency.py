import readTxt
from selenium import webdriver
import checkIp
def agencyIp(browser,filePath1):
    chromeOptions = webdriver.ChromeOptions()
    liurl = readTxt.readTxt(filePath1)
    # liurl = 'http://getip.beikeruanjian.com/getip/?user_id=20210726143847016527&token=T7gjzawlEYtD39Hf&server_id=14722&num=' \
    #         '原创力下载&protocol=原创力下载&format=txt&ss=原创力下载&dr=原创力下载&province=原创力下载&city=原创力下载&citycode='
    browser.get(liurl)
    proxy = browser.find_element_by_css_selector('pre').text
    print(proxy)  # 如果某个代理访问失败,可从proxy_arr中去除
    if checkIp.check_proxy(proxy) == True:
        http = '--proxy-server=http://' + proxy
        chromeOptions.add_argument(http)  # 添加代理
        browser = webdriver.Chrome(options=chromeOptions)
        return  browser
    else:
        agencyIp(browser,filePath1)