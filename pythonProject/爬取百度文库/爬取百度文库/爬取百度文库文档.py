import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options  # 手机模式


def dengdaiTime(browser):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-原创力下载])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)

def saveFile(thmlpath,url,driver,title):
    try:
        print(url)
        # 输入网址
        driver.get(url)
        time.sleep(0.2)
        td = driver.find_element_by_xpath('//tbody')
        # td = driver.find_element_by_xpath('//body')
        content = td.text
        # print(content)
        list_soupa = td.get_attribute('innerHTML').split('点击加载更多')[0].replace('开通VIP，免费享6亿+内容','').replace('开通VIP','').replace('点击立即咨询，了解更多详情','').replace('咨询','').split('搜索')[0]
        title=title.replace(' - ','')
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        # try:
            print(url)
            # 输入网址
            driver.get(url)
            time.sleep(0.2)
            # td = driver.find_element_by_xpath('//tbody')
            td = driver.find_element_by_xpath('//div.reader-container')
            content = td.text
            # print(content)
            list_soupa = td.get_attribute('innerHTML').split('点击加载更多')[0].replace('开通VIP，免费享6亿+内容','').replace('开通VIP','').replace('点击立即咨询，了解更多详情','').replace('咨询','').split('搜索')[0]
            title = title.replace(' - ', '')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        # except:
        #     print('error')
def getUrl(filePath,txtPath,driver):
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        # try:
            if i > -1:
                print('----------------------',i)
                hrefUrl=line.replace('\n','').split('————')[0]
                title = line.replace('\n', '').split('————')[-1]
                saveFile(filePath,hrefUrl, driver,title)
        # except:
        #     print('error')
        #     continue

def main(filePath,txtPath):
    # 设置手机型号，这设置为iPhone 6
    mobile_emulation = {"deviceName": "iPhone 7"}
    options = Options()
    options.add_experimental_option("mobileEmulation", mobile_emulation)
    # 启动配置好的浏览器
    driver = webdriver.Chrome(options=options)
    getUrl(filePath,txtPath, driver)

if __name__ == '__main__':
    filePath=r'D:\文档\百度付费上传文档\百度上传'
    txtPath=r'.\url.txt'
    main(filePath,txtPath)