import math
import os
import threading

from pythonProject.b百度上传付费文档.使用chatGPT生成文档.chatGPT4生成单个文件 import main_chatGPT4Single
from pythonProject.b百度上传付费文档.使用chatGPT生成文档.chatGPT生成单个文件 import main_chatGPTSingle


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def makeFile(titleKey, folderPath, apiFile_lines, i):
    content = main_chatGPT4Single(titleKey, folderPath, apiFile_lines[i].replace('\n', ''))
    if 'You exceeded your current quota' in content or 'please check your plan' in content or 'chatGPT额度用光了' in content:
        i = i + 1
        print(i)
        makeFile(titleKey, folderPath, apiFile_lines, i)
    return i


def getWord(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, number):
    i = number * apiNumber
    start = number * titleNumber
    last = start + titleNumber
    for ai, line in enumerate(titleFile_lines):
        if ai >= start and ai < last:
            titleKey = line.replace('\n', '')
            print(titleKey)
            i = makeFile(titleKey, folderPath, apiFile_lines, i)
            i = i + 1
            if i >= len(apiFile_lines):
                i = 0


def getKeyTitle(apiKeyPath, titlePath, folderPath, threadNumber):
    titleFile = open(titlePath, 'r', encoding='utf-8')
    titleFile_lines = titleFile.readlines()
    apiFile = open(apiKeyPath, 'r', encoding='utf-8')
    apiFile_lines = apiFile.readlines()
    titleNumber = math.ceil(len(titleFile_lines) / threadNumber)
    apiNumber = math.ceil(len(apiFile_lines) / threadNumber)
    # 线程1
    t1 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 0))
    # 线程2
    t2 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 1))
    # 线程3
    t3 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 2))
    # # 线程4
    # t4 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 3))
    # # 线程5
    # t5 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 4))
    # # 线程6
    # t6 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 5))
    # # 线程7
    # t7 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 6))
    # # 线程8
    # t8 = threading.Thread(target=getWord, args=(titleFile_lines, folderPath, apiFile_lines, titleNumber, apiNumber, 7))
    t1.daemon = 1
    t1.start()
    t2.start()
    t3.start()
    # t4.start()
    # t5.start()
    # t6.start()
    # t7.start()
    # t8.start()
    t1.join()
    t2.join()
    t3.join()
    # t4.join()
    # t5.join()
    # t6.join()
    # t7.join()
    # t8.join()

def main():
    apiKeyPath = r'./api_key.txt'
    titlePath = r'./标题.txt'
    folderPath = r'D:\文档\百度付费上传文档\百度html\其他网站'
    threadNumber = 3
    makeFolder(folderPath)
    getKeyTitle(apiKeyPath, titlePath, folderPath, threadNumber)


if __name__ == '__main__':
    main()
