# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="fw100_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.fanwen100.com/zhichang/gongzuozongjie/335199.html',
                'https://www.fanwen100.com/zhichang/gongzuojihua/335334.html',
                'https://www.fanwen100.com/zhichang/gongzuofangan/335357.html',
                'https://www.fanwen100.com/zhichang/huodongzongjie/334224.html',
                'https://www.fanwen100.com/zhichang/shenqingshu/335335.html',
                'https://www.fanwen100.com/zhichang/cehuashu/334190.html',
                'https://www.fanwen100.com/zhichang/zhiyeguihua/335304.html',
                'https://www.fanwen100.com/zhichang/yaoqinghan/335337.html',
                'https://www.fanwen100.com/zhichang/chengnuoshu/335217.html',
                'https://www.fanwen100.com/zhichang/qiuzhixin/278119.html',
                'https://www.fanwen100.com/zhichang/cizhixin/335295.html',
                'https://www.fanwen100.com/baogao/shixi/335348.html',
                'https://www.fanwen100.com/baogao/shuzhi/334863.html',
                'https://www.fanwen100.com/baogao/cizhi/335351.html',
                'https://www.fanwen100.com/baogao/shehuishijian/334492.html',
                'https://www.fanwen100.com/baogao/shiyongqi/332937.html',
                'https://www.fanwen100.com/baogao/xindetihui/335310.html',
                'https://www.fanwen100.com/baogao/diaochabaogao/332429.html',
                'https://www.fanwen100.com/baogao/kaitibaogao/320112.html',
                'https://www.fanwen100.com/hetong/fanben/334163.html',
                'https://www.fanwen100.com/hetong/fangwuzulin/335297.html',
                'https://www.fanwen100.com/hetong/goufang/334887.html',
                'https://www.fanwen100.com/hetong/laodong/333778.html',
                'https://www.fanwen100.com/hetong/maimai/335350.html',
                'https://www.fanwen100.com/hetong/shigong/335299.html',
                'https://www.fanwen100.com/hetong/xiaoshou/335290.html',
                'https://www.fanwen100.com/hetong/gongcheng/335339.html',
                'https://www.fanwen100.com/hetong/zhuanrangxieyi/335338.html',
                'https://www.fanwen100.com/hetong/jiekuan/334617.html',
                'https://www.fanwen100.com/hetong/chengbao/335343.html',
                'https://www.fanwen100.com/zhici/ziwojieshao/335347.html',
                'https://www.fanwen100.com/zhici/yanjianggao/335362.html',
                'https://www.fanwen100.com/zhici/fayangao/335239.html',
                'https://www.fanwen100.com/zhici/zhuchigao/334469.html',
                'https://www.fanwen100.com/zhici/zc/335194.html',
                'https://www.fanwen100.com/zhici/heci/334631.html',
                'https://www.fanwen100.com/zhici/daxieci/335274.html',
                'https://www.fanwen100.com/zhici/zhufuyu/335117.html',
                'https://www.fanwen100.com/zhici/wenhouyu/334542.html',
                'https://www.fanwen100.com/zhici/ganyan/334310.html',
                'https://www.fanwen100.com/jiaoxue/jiaoxuezongjie/334413.html',
                'https://www.fanwen100.com/jiaoxue/jiaoxuejihua/335294.html',
                'https://www.fanwen100.com/jiaoxue/jiaoxuefansi/335315.html',
                'https://www.fanwen100.com/jiaoxue/jiaoxuesheji/335177.html',
                'https://www.fanwen100.com/jiaoxue/shuokegao/334561.html',
                'https://www.fanwen100.com/jiaoxue/jiaoan/335329.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
