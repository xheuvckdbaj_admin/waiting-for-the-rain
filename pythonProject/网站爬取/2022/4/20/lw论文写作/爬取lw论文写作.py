# -*- coding:utf-8 -*-
import re
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.entry-content')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>')
            title = soup.select('div.single-header > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace('\r', '').replace('\n', '').replace(' ', '')
            print(title)
            # list_soupa1 = re.sub('<a(.*?)</a>', '', list_soupa)
            # list_soupa2 = re.sub(title, '', list_soupa1)
            # content = re.sub('下载Word文档到电脑，方便收藏和打印', '', list_soupa2).replace('编辑推荐：','').replace('下载Word文档','').replace('TAG标签：','')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.content > ul.content-list > li > h2 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        hrefUrl='http://www.sxsky.net'+href
        getHtml(thmlpath, hrefUrl)
def getPageUrl(thmlpath,strUrl):
    getListUrl(thmlpath, strUrl)
    hrefUrl=strUrl+'index_%d.html'
    for i in range(2,2000):
        try:
            newUrl=hrefUrl%i
            getListUrl(thmlpath, newUrl)
        except:
            print('error')
            continue

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.header-menu.pull-right > ul > li > a')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            newUrl='http://www.sxsky.net'+href
            getPageUrl(thmlpath, newUrl)

def main(thmlpath):
    url='http://www.sxsky.net/'
    getTitleUrl(thmlpath, url)



if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\爬取网站4.20\lw论文写作'
    main(thmlpath)
