# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os.path
import time
import urllib

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一c所有账号单独运行.代码.判断百度账号是否传满 import panduanUploadOver
from pythonProject.创作活动文档.方式一c所有账号单独运行.百度选择标题上传3 import mainBaiduSelectUpload3
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传.模块上传文档 import mokuaiUpload
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver, getDriverYingCang
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser

def makeFolder(filePath):
    if  not os.path.exists(filePath):
        os.makedirs(filePath)
def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


guolvPath = r'./过滤词.txt'


def panduanTitle(title):
    panduan = True
    file = open(guolvPath, 'r', encoding='utf-8')
    file_lines = file.readlines()
    for line in file_lines:
        key = line.replace('\n', '')
        if key in title and (not key==''):
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan


def getTitle(browser):
    eleLengths = []
    xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    try:
        getNewWindow(browser, xpathStr, 5)
    except:
        print('error')
    elements = browser.find_elements(By.XPATH, xpathStr)
    if len(elements) == 20:
        for element in elements:
            title = element.get_attribute('title')
            if panduanTitle(title):
                eleLengths.append(title)
        if len(eleLengths) >= 1:
            return 20
        else:
            return 0
    else:
        for element in elements:
            try:
                title = element.get_attribute('title')
            except:
                title =''
            if panduanTitle(title):
                eleLengths.append(title)
        return len(eleLengths)


def ClickNextPage(browser, pageNumber):
    try:
        # pageNumber最大为1，最小为0
        xpathStr = '//button[@class="btn-next"]/i[@class="el-icon el-icon-arrow-right"]'
        browser.find_element(By.XPATH, xpathStr).click()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(2)
    except:
        print('error')


def clickNextTitle(browser, pageNumber):
    if pageNumber >= 0:
        # pageNumber最小为0
        if pageNumber >= 9:
            xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
            browser.find_element(By.XPATH, xpathStrs).click()
            getNewWindow(browser, xpathStrs, 10)
            time.sleep(2)
        xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
        titleEl = browser.find_elements(By.XPATH, xpathStr)[pageNumber]
        ActionChains(browser).click(titleEl).perform()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(2)


def getNumberPager(browser):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
        return pagerNumber
    except:
        return 0


def mainTitleClassify(browser, number,numberList,cookiesPath):

    clickNextTitle(browser, number - 2)
    titleNumber = getTitle(browser)
    if titleNumber >= 1:
        numberList.append(number)
        print('模块%d更新了' % number)
        # time.sleep(10000)
        mainBaiduSelectUpload3(cookiesPath,number)
    return number

def controlBaidu(cookiesPath, panduanFilePath):
    browser = getDriverYingCang('title1')
    url = 'https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask'
    addCookies(browser, url, cookiesPath)
    numberList=[]
    for ai in range(0, 1000):
        if os.path.exists(panduanFilePath):
            # try:
                content = ''
                print('--------------------------------开始监控')
                browser.get(url)
                time.sleep(1)
                xpathStr = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
                titleEl = browser.find_elements(By.XPATH, xpathStr)[0]
                ActionChains(browser).click(titleEl).perform()
                getNewWindow(browser, xpathStr, 10)
                dingshus = []
                for i in range(1, 14):
                    if not i in numberList:
                        if os.path.exists(panduanFilePath):
                            xpathStrZhanghao = '//div[@class="top-bar-wrapper"]/div[@class="right-bar"]/div[@class="link"]'
                            xpathContent = '//p[@class="major-progress"]/span[@class="context"]'
                            titleEl = browser.find_elements(By.XPATH, xpathStrZhanghao)[0].text
                            content = browser.find_elements(By.XPATH, xpathContent)[0].text
                            print(titleEl+'###', content)
                            number = i
                            dingshu = mainTitleClassify(browser,  number,numberList,cookiesPath)
                            dingshus.append(dingshu)
                        else:
                            print('------------------------账号上传完毕')
                            break
                time.sleep(1)
            # except:
            #     print('因为出现弹窗报错')
        else:
            print('------------------------账号上传完毕')
            break
def dingcanjiance3():
    folderPath = r'D:\文档\百度活动文档3\百度活动文档标题'
    cookiesPath = './cookies/baiduCookies3.txt'
    panduanFilePath = r'D:\文档\百度活动文档3\百度活动文档上传\账号\账号.txt'
    makeFolder(panduanFilePath.replace('\账号.txt', ''))
    file = open(panduanFilePath, 'w', encoding='utf-8')
    file.write('')
    file.close()
    controlBaidu(cookiesPath, panduanFilePath)
if __name__ == '__main__':
    dingcanjiance3()
