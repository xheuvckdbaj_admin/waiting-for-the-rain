# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'https://www.unjs.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('div > h1')[0].text
    print(title)
    list_soupa = str(soup.select('.article > .content')[0])
    print(list_soupa)
    content=re.sub('<a href=.*?</a></p>','',list_soupa)
    title = soup.select('.article > .title')[0].text
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('ul > li > h2 > a')
    #print(url_list)
    for a in a_list:
        try:
            href=a.get('href')
            title=a.get('title')
            getContent(thmlpath,href,title)
        except:
            print('错误')
            continue
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('#NEW_INFO_LIST > dl')
    #print(url_list)
    for u in url_list:
            src=u.select('dt > a')[0].get('href')
            href='http://www.banzhuren.cn'+src
            print(href)
            title = u.select('dt > a')[0].text
            getContent(thmlpath,href,title)

def getUrlPageNum(url,str):
    for pageNum in range(1,1000):
        try:
            if pageNum==1:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    newUrl=url+str+'.html'
                    getUrlIndex(thmlpath, newUrl)

            else:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    oldUrl=url+str+'_%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(thmlpath, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['jiaoxue','tool','zuowen','xuexi','lunwen','jiuye','fenxianghulianwang/gushidaquan',
           'fanwenwang'
         ]
    for s in str:
        url = groupUrl(s)
        getUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'E:\优文网'
    main(thmlpath)
