import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getContent(hrefUrl,htmlPath,title,driver):
    try:
        requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        driver.get(hrefUrl)
        td=driver.find_element(By.XPATH,'//pre[@class="replay-info-txt answer_con"]')
        list_soupa = td.get_attribute('innerHTML')
        print(title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def mian_xuexila(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        try:
            title=soup.select('div.ar_title > h1')[0].text
        except:
            title = soup.select('div.head > h1')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        try:
            list_soupa=str(soup.select('div.con_article')[0]).replace(title,keyStr)
        except:
            list_soupa = str(soup.select('div.wzxx')[0]).replace(title, keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('学习啦下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://m.xuexila.com/zixun/fenshuxian/c1478467.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\学习啦'
    keyStr='kaixin'
    title=''
    mian_xuexila(url,htmlPath,keyStr,title)