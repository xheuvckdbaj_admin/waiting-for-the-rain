
# -*- coding:utf-8 -*-
import docx
import requests


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getZip(thmlpath,url):
    requests_text = requests.get(url=url, headers=headers)
    with open(thmlpath, 'wb') as df:
        for chunk in requests_text.iter_content(chunk_size=128):
            df.write(chunk)

def main():
    thmlpath = r'E:\test\hello.doc'
    url='http://www.zlcool.com/e/DownSys/doaction.php?enews=DownSoft&classid=183&id=9391&pathid=0&pass=edc69537fcf43ffd0ccda4d1a4f84891&p=::::::'
    getZip(thmlpath,url)

if __name__ == '__main__':

    main()
