import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright
def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
def clickPage(page):
    time.sleep(5)
    page.get_by_text("下一页").click()
def getPageTitles(page, titlePath):
    file=open(titlePath,'w',encoding='utf-8')
    time.sleep(1)
    elements=page.locator('//div[@class="doc-item"]/div[@class="title"]')
    eleNumbers=elements.count()
    print(eleNumbers)
    for i in range(0,eleNumbers):
        title=elements.nth(i).text_content().strip()
        print(title)
        file.write(title+'\n')
    file.close()

def getTitles(page, titlefolder,pageNumber):
    newTitlefolder=titlefolder+'\\'+str(time.time())
    makeFolder(newTitlefolder)
    # page.locator('//div[@class="price-range"]/div[@class="price-start"]/input[@type="number"]').nth(0)
    # page.get_by_role("spinbutton").first.click()
    # page.get_by_role("spinbutton").first.fill(str(startNumber))
    #
    # time.sleep(0.2)
    # page.get_by_role("spinbutton").nth(1).click()
    # page.get_by_role("spinbutton").nth(1).fill(str(endNumber))
    #
    # time.sleep(0.2)
    # page.get_by_text("确认").nth(1).click()
    time.sleep(0.2)
    titlePath=newTitlefolder+'\\'+'1.txt'
    getPageTitles(page, titlePath)
    for i in range(2,pageNumber):
        try:
            clickPage(page)
            titlePath = newTitlefolder + '\\' +str(i)+ '.txt'
            getPageTitles(page, titlePath)
        except Exception as e:
            print(str(e))
            break

    time.sleep(100)
def run(playwright: Playwright,titlefolder,url,pageNumber):
    brower = playwright.chromium.launch(headless=False)
    context = brower.new_context()
    page = context.new_page()
    page.set_default_timeout(6000)
    page.goto(url)
    time.sleep(1)
    # for i in range(0,10000):
    #     try:
    #         startNumber=i
    #         endNumber=startNumber+1
    getTitles(page, titlefolder,pageNumber)
        #     time.sleep(50)
        # except Exception as e:
        #     print(str(e))
        #     continue
def yuanchuangli(titlefolder,urlPath,pageNumber):
    file=open(urlPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for i,line in enumerate(file_lines):
        url=line.replace('\n','')
        with sync_playwright() as playwright:
            run(playwright, titlefolder,url,pageNumber)


def main():
    titlefolder = r'D:\文档\百度付费上传文档\百度主页标题\页面标题'
    urlPath=r'./主页网址.txt'

    pageNumber=10000
    yuanchuangli(titlefolder,urlPath,pageNumber)
if __name__ == '__main__':
    main()
