import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay

guolvPath=r'./过滤词.txt'
def panduanTitle(title):
    panduan = True
    file=open(guolvPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    file.close()
    for line in file_lines:
        key=line.replace('\n','')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan
def panduanTitleUpPass(title,passPath):
    panduan = True
    file=open(passPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    file.close()
    for line in file_lines:
        key=line.replace('\n','')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan
def getNewPage(page,url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)
def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page, "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,"https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    return page
def getTitleHuodong(page,file,fileClassify,passTitlePath,titles):
    xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    elements=page.locator(xpathStr)
    for i in range(0,elements.count()):
        try:
            titleStr = elements.nth(i).text_content().strip()
            if panduanTitle(titleStr) and panduanTitleUpPass(titleStr,passTitlePath) and (not titleStr in titles):
                print(titleStr)
                titles.append(titleStr)
                file.write(titleStr + '\n')
                fileClassify.write(titleStr + '\n')
        except Exception as e:
            print(e,'获取标题出错')
            break

def getNextPage(page,number):
    xpathPageStr='//input[@class="el-input__inner"]'
    page.locator(xpathPageStr).click()
    page.locator(xpathPageStr).fill(str(number))
    page.locator(xpathPageStr).press('Enter')
    time.sleep(1)

def getTitleForquchong(playwright: Playwright,cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath):
    titles=[]
    xpathStrButton = '//div[@class="doc-row"]/div[@class="upload-doc"]'
    xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button[@class="el-button no-agree btn el-button--default el-button--mini"]'
    brower=playwright.chromium.launch(headless=False)
    context=brower.new_context()
    page=context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)

    page.get_by_text("创作活动", exact=True).click()
    lanmuStrs = ['学前教育', '基础教育', '高校与高等教育', '语言/资格考试', '法律', '建筑',
                 '互联网', '行业资料', '政务民生', '商品说明书', '实用模板', '生活娱乐']
    time.sleep(0.1)
    locOver = page.locator(xpathStrEle)
    filePath = titleFolder + '\\' + '1' + '.txt'
    file = open(filePath, 'w', encoding='utf-8')
    filePathClassify = titleFolderClassify + '\\' + '1' + '.txt'
    fileClassify = open(filePathClassify, 'w', encoding='utf-8')
    loc=page.locator(xpathStrButton)
    if loc.count() > 0:
        print('推荐','有标题')
        if locOver.count() > 0:
            print('账号已经上传完毕')
        else:
            for number in range(1,11):
                    getNextPage(page, number)
                    getTitleHuodong(page,file,fileClassify,passTitlePath,titles)
    file.close()
    fileClassify.close()
    time.sleep(2)
    for number,lanmuStr in enumerate(lanmuStrs):
        fileName = str(number+2)
        if number > 9:
            fileName = 'a' + str(number+2)
        filePath = titleFolder + '\\' + fileName + '.txt'
        file = open(filePath, 'w', encoding='utf-8')
        filePathClassify = titleFolderClassify + '\\' + fileName + '.txt'
        fileClassify = open(filePathClassify, 'w', encoding='utf-8')
        page.get_by_text(lanmuStr).nth(0).click()
        time.sleep(1)
        locxue = page.locator(xpathStrButton)
        if locxue.count() > 0:
            print(lanmuStr, '有标题')
            locxueOver = page.locator(xpathStrEle)
            if locxueOver.count() > 0:
                print('账号已经上传完毕')
                break
            else:
                for number in range(1, 11):
                        getNextPage(page, number)
                        getTitleHuodong(page,file,fileClassify,passTitlePath,titles)
        file.close()
        fileClassify.close()
        time.sleep(2)
    #挑选必会通过的标题
    fileTitle = open(selectTitlePath, 'w', encoding='utf-8')
    fileAllTitle = open(titleAllPath, 'w', encoding='utf-8')
    fileSelect=open(selectPath, 'r', encoding='utf-8')
    fileSelect_lines=fileSelect.readlines()
    for lineClass in fileSelect_lines:
        key = lineClass.replace('\n', '')
        for line in titles:
            title = line.replace('\n', '')
            if key in title:
                print('必过标题-----',title)
                fileTitle.write(title+'\n')
    for line in titles:
        fileAllTitle.write(line+'\n')
    fileAllTitle.close()
    fileTitle.close()
    fileSelect.close()


def getPassTitle(cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath):
    with sync_playwright() as playwright:
        getTitleForquchong(playwright,cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath)

def panduanAndcrawTitleAll(cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath):
    accountPanduan=False
    getPassTitle(cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath)
def main():
    # passTitleFolder = r'D:\文档\百度活动文档\百度上传过的标题'
    titleFolder = r'D:\文档\百度活动文档\百度活动文档标题'
    titleFolderClassify = r'D:\文档\百度活动文档\用来分类文档标题'
    passTitlePath = r'D:\文档\百度活动文档\百度上传过的标题\titlePass.txt'
    selectTitlePath = r'D:\文档\百度活动文档\百度活动文档标题' + '\\' + 'select.txt'
    titleAllPath = r'D:\文档\百度活动文档\百度活动文档标题' + '\\' + 'all.txt'
    selectPath = r'./必会通过标题.txt'
    cookiesPath = r'./baiduCookies1.txt'
    panduanAndcrawTitleAll(cookiesPath,titleFolder,titleFolderClassify,passTitlePath,selectTitlePath,selectPath,titleAllPath)
if __name__ == '__main__':
    main()