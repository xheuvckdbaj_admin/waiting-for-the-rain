# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm
import  addLingcookies
# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


filePath=r'./cookies.txt'
def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver


def getHtml(browser,thmlpath, strUrl):
    # try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        content = str(soup.select('#article > #article_body')[0])
        contentTxt=re.sub('<td(.*)</td>','',content)
        title=str(soup.select('#article > h1')[0].text)
        print(title)
        title = title.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(contentTxt)
    # except:
    #     c
def getContent(browser,thmlpath, strUrl):
    try:
        getBrowserUrl(browser, strUrl)
        # addLingcookies.addCookies(browser, strUrl, filePath)
        title=browser.find_element_by_xpath('//div[@id="article"]/h1').text.replace('?','').replace('？','').replace('|','')\
                    .replace('”','').replace('*','').replace('/','').replace('\\','')
        content=str(browser.find_element_by_xpath('//div[@id="article"]/div[@id="article_body"]').get_attribute('innerHTML'))
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w')
        f.write(content)
        f.close()
    except:
        print('错误')

def main(thmlpath):
            strings='<tr><td class="hei" align="center">' \
                    '<span style="color:red;">(如果下载不了文档,请使用（谷歌,火狐等浏览器）或者切换为浏览器极速模式下载！' \
                    '文档不全请加客服微信：fanwen9944)</span></td></tr></tbody></table>'
            contentTxt = re.sub('<span style="color:red;">(.*?)</span>', '', strings)
            print(contentTxt)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
