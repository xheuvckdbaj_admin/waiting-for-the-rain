# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from docx import Document
from docx.enum.base import XmlMappedEnumMember
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn

from pythonProject.FindFile import find_file, find_name
from pythonProject.修改文件提高文档质量.重新生成文档.panduan import modifyPara

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def panduanFileName(fileName):
    panduan=len(fileName) >= 8 and (not ('党' in fileName or '政府' in fileName or
                                         '书记' in fileName or '干部' in fileName or
                                         '部队' in fileName or '中央' in fileName or
                                         '习近平' in fileName or '毒' in fileName or
                                         '政治' in fileName or '社团' in fileName or
                                         '百年' in fileName or '领导班子' in fileName or
                                         '两会' in fileName or '局长' in fileName or
                                         '微信' in fileName or 'QQ' in fileName or
                                         'qq' in fileName or '建国' in fileName) )
    return  panduan
def addfanben(content):
    content=content.replace('范文','').replace('范本','').replace('自我评价','自我评价范文').replace('演讲稿','演讲稿范文').replace('作文','作文范文').\
        replace('总结','总结范文').replace('承诺书','承诺书范文').replace('辩论稿','辩论稿范文').replace('协议','协议范文').\
        replace('通知','通知范文').replace('说明书','说明书范文').replace('评语','评语范文').replace('心得体会','心得体会范文').\
        replace('征文','征文范文').replace('报告','报告范文').replace('主持词','主持词范文').replace('合同','合同范本').\
        replace('寄语','寄语范文').replace('开幕词','开幕词范文').replace('读书心得','读书心得范文').replace('方案','方案范文').\
        replace('自我鉴定','自我鉴定范文').replace('工作计划','工作计划范文').replace('活动方案','活动方案范文')
    return content

def sezhiPaper(doc):
    section = doc.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(25.4)
    section.right_margin = Mm(25.4)
    section.top_margin = Mm(25.4)
    section.bottom_margin = Mm(25.4)
    section.header_distance = Mm(12.7)
    section.footer_distance = Mm(12.7)
def sezhigeshi(doc,content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体 (中文正文)'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    run.font.bold = True
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    # para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5 # 1.5倍行距 单倍行距 1.0
def sezhishuxin(doc,content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体 (中文正文)'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    # para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5 # 1.5倍行距 单倍行距 1.0
def sezhiParas(doc,content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5 # 1.5倍行距 单倍行距 1.0
def  sezhiLittleTtitle(doc,content):
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'黑体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'黑体')
    run.font.size = Pt(14)
    run.font.color.rgb = RGBColor(250,000,000)
    run.font.bold = True
    doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
def  sezhiTtitle(doc,content):
    run = doc.add_heading('', 0).add_run(content)  # 应用场景示例标题
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(18)
    # run.font.color.rgb = RGBColor(250,000,000)
    doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

def panduanHead(content):
    panduan=content.endswith('一：') or content.endswith('二：') or content.endswith('三：') or \
    content.endswith('四：') or content.endswith('五：') or content.endswith('六：') or \
    content.endswith('七：') or content.endswith('八：') or content.endswith('九：') or \
    content.endswith('十：') or content.endswith('一') or content.endswith('二') or \
    content.endswith('三') or content.endswith('四') or content.endswith('五') or \
    content.endswith('六') or content.endswith('七') or content.endswith('八') or \
    content.endswith('九') or content.endswith('十') or content.endswith('1') or content.endswith('2') \
    or content.endswith('3') or content.endswith('4') or content.endswith('5') or \
    content.endswith('6') or content.endswith('7') or content.endswith('8') or \
    content.endswith('9') or content.endswith('10') or content.endswith('（一）') or content.endswith('（二）') or \
    content.endswith('（三）') or content.endswith('（四）') or content.endswith('（五）') or \
    content.endswith('（六）') or content.endswith('（七）') or content.endswith('（八）') or \
    content.endswith('（九）') or content.endswith('（十）') or content.endswith('(一)') or \
            content.endswith('(一)') or content.endswith('(二)') or content.endswith('(三)') or \
            content.endswith('(四)') or content.endswith('(五)') or content.endswith('(六)') or \
            content.endswith('(七)') or content.endswith('(八)') or content.endswith('(九)') or \
            content.endswith('(十)') or content.endswith('〔一〕') or content.endswith('〔二〕') or \
            content.endswith('〔三〕') or \
            content.endswith('〔四〕') or content.endswith('〔五〕') or content.endswith('〔六〕') or \
            content.endswith('〔七〕') or content.endswith('〔八〕') or content.endswith('〔九〕') or \
            content.endswith('〔十〕') or content.endswith('【一】') or content.endswith('【二】') or \
            content.endswith('【三】') or \
            content.endswith('【四】') or content.endswith('【五】') or content.endswith('【六】') or \
            content.endswith('【七】') or content.endswith('【八】') or content.endswith('【九】') or \
            content.endswith('【十】') or content.endswith('【篇一】') or content.endswith('【篇二】') or \
            content.endswith('【篇三】') or \
            content.endswith('【篇四】') or content.endswith('【篇五】') or content.endswith('【篇六】') or \
            content.endswith('【篇七】') or content.endswith('【篇八】') or content.endswith('【篇九】') or \
            content.endswith('【篇十】' or content.startswith('第1篇') or content.startswith('第2篇') or \
            content.startswith('第3篇') or \
            content.startswith('第4篇') or content.startswith('第5篇') or content.startswith('第6篇') or \
            content.startswith('第7篇') or content.startswith('第8篇') or content.startswith('第9篇') or \
            content.startswith('第10篇'))
    return panduan
def panduanPara(content):


    panduan=len(content) < 45 and(content.startswith('一、') or content.startswith('二、') or \
    content.startswith('三、') or \
    content.startswith('四、') or content.startswith('五、') or content.startswith('六、') or \
    content.startswith('七、') or content.startswith('八、') or content.startswith('九、') or \
    content.startswith('十、'))
    return panduan
def makePagrah(i, title, content,doc):
    if modifyPara(i, title, content):
        # print(file.paragraphs[i].style)
        if panduanHead(content):
            sezhiLittleTtitle(doc, content)
        elif (content.startswith('尊敬的') or content.startswith('各位') or
              content.startswith('亲爱的') or content.startswith('朋友') or len(
                    content) < 12) and content.endswith('：'):
            sezhishuxin(doc, content)
        elif panduanPara(content):
            sezhigeshi(doc, content)
        else:
            sezhiParas(doc, content)
def makeWord(filePath,newPath):
    filrdocx = filePath.split(".")[len(filePath.split(".")) - 1]
    if filrdocx == 'docx' and  panduanFileName(os.path.basename(filePath)):
        try:
            file = docx.Document(filePath)
            fileName = os.path.basename(filePath)
            fileName = addfanben(fileName)
            title = fileName.replace('.docx','')
            doc = Document()
core_properties = doc.core_properties
core_properties.author = ''
            sezhiPaper(doc)
            sezhiTtitle(doc, title)
            # number = 0
            for i in range(0,len(file.paragraphs)):
                title = title.strip().replace(' ', '')
                content = str(file.paragraphs[i].text).strip().replace('　　','').replace('', '').replace('\n','')
                if ' ' in content:
                    contentList=content.split(' ')
                    for content in contentList:
                        makePagrah(i, title, content, doc)
                else:
                    makePagrah(i, title, content, doc)
            doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            fileName=addfanben(fileName)
            doc.save(newPath + '\\' + fileName)
            os.remove(filePath)

        except:
            print('error')
def modifyContent(filePath,newPath):
    dir = os.listdir(filePath)
    for i in dir:
        i = os.path.join(filePath, i)
        if os.path.isdir(i):
            modifyContent(i,newPath)
        else:
            if not '$' in i:
                print(i)
                makeWord(i, newPath)  # 修改，删除广告

def main1():
    oldFolderPath=r'E:\文档\自查报告网'
    newPath = r'E:\文档\自查报告网url'
    modifyContent(oldFolderPath,newPath)
if __name__ == '__main__':
    main1()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
