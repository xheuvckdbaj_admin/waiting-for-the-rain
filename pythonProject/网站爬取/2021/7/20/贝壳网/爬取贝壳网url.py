# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        time.sleep(2)
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            time.sleep(1)
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')
def getHtml(browser,thmlpath, strUrl,file_line,number):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            browser.get(strUrl)
            href=browser.find_element_by_xpath('//span[@class="jh_next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(browser, thmlpath, href,file_line,number)
            # getContent(thmlpath, strUrl)
        except:
            print('该模块已完毕')

def main(thmlpath):

            filePath=r'E:\文档\test\贝壳网.txt'
            file=open(filePath,'r')
            file_lines=file.readlines()
            browser=webdriver.Chrome()
            for i in range(0,len(file_lines)):
                strUrl=file_lines[i].strip().replace('\n','')
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(browser,thmlpath, strUrl,file_line,number)
                file_line.close()

def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    # main(thmlpath)
    mainGetHtml(thmlpath, txtFilepath)
