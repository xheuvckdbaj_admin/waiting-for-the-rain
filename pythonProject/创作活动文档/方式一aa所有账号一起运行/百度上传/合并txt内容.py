import os

from pythonProject.FindFile import find_file
def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)

def merge(folderpath,path):
    makeFolder(folderpath)
    txtpaths=find_file(folderpath)
    newTxtPath=path+'\\'+'all.txt'
    paths=[]
    txtfile_line=open(newTxtPath,'w',encoding='utf-8')
    for txtpath in txtpaths:
        if txtpath.endswith('.txt'):
            print(txtpath)
            try:
                file_line=open(txtpath,'r',encoding="utf-8")
                readlines=file_line.readlines()
                for line in readlines:
                    if not line in paths:
                        paths.append(line)
                file_line.close()
                # os.remove(txtpath)
            except:
                try:
                    file_line = open(txtpath, 'r', encoding="gbk")
                    readlines = file_line.readlines()
                    for line in readlines:
                        if not line in paths:
                            paths.append(line)
                    file_line.close()
                    # os.remove(txtpath)
                except:
                    print('gbk读不出')
                    continue
    for ph in paths:
        try:
            txtfile_line.write(ph)
        except:
            print('error')
            continue
    txtfile_line.close()
def main():
    folderpath=r'D:\文档\百度活动文档\用来分类文档标题'
    path=r'D:\文档\百度活动文档\用来分类文档标题'
    merge(folderpath,path)
if __name__ == '__main__':
    main()