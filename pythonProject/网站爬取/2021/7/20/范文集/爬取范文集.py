# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        time.sleep(2)
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')

def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.fanwenji.com/zhiyeguihua/68604.html',
                'https://www.fanwenji.com/qiuzhixin/75450.html',
                'https://www.fanwenji.com/ziwojieshao/75456.html',
                'https://www.fanwenji.com/ziwopingjia/74392.html',
                'https://www.fanwenji.com/ziwojianding/75432.html',
                'https://www.fanwenji.com/zhuanzhengshenqing/75414.html',
                'https://www.fanwenji.com/shixibaogao/75454.html',
                'https://www.fanwenji.com/laodonghetong/75297.html',
                'https://www.fanwenji.com/zufanghetong/75427.html',
                'https://www.fanwenji.com/goufanghetong/74351.html',
                'https://www.fanwenji.com/jianzhuhetong/75433.html',
                'https://www.fanwenji.com/zhuangxiuhetong/75419.html',
                'https://www.fanwenji.com/shigonghetong/75420.html',
                'https://www.fanwenji.com/yanjianggao/75451.html',
                'https://www.fanwenji.com/fayangao/75400.html',
                'https://www.fanwenji.com/huanyingci/74359.html',
                'https://www.fanwenji.com/zhuchici/75066.html',
                'https://www.fanwenji.com/ganxiexin/75411.html',
                'https://www.fanwenji.com/biaoyangxin/75399.html',
                'https://www.fanwenji.com/daoqianxin/75434.html',
                'https://www.fanwenji.com/jieshaoxin/74175.html',
                'https://www.fanwenji.com/weiwenxin/75413.html',
                'https://www.fanwenji.com/yaoqinghan/75402.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'D:\文档\范文集'
    txtFilepath = r'D:\文档\范文集\all.txt'
    # main(thmlpath)
    mainGetHtml(thmlpath, txtFilepath)
