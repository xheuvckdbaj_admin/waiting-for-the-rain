import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_guangmingwang(url, htmlPath, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.header > div.titleFont')[0].text
        list_soupa = str(soup.select('article')[0])
        list_soupa=list_soupa.replace(title, keyStr)
        list_soupa = list_soupa.split('来源')[0]
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('光明网下载成功----', keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://m.gmw.cn/baijia/2022-10/18/1303174267.html'
    htmlPath = r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr = 'kaixin'
    title = ''
    mian_guangmingwang(url, htmlPath, keyStr, title)
