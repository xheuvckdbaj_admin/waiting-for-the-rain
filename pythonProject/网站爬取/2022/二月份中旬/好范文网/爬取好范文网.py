# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl,browser):

    # requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    # soup = BeautifulSoup(requests_text, 'lxml')
    browser.get(strUrl)
    content=str(browser.find_element_by_xpath('//div[@class="content"]').get_attribute('innerHTML'))
    title=str(browser.find_element_by_xpath('//div[@class="detail"]/dl/dt/h1').get_attribute('innerHTML'))
    print(title)

    # contentText = str(soup.select('div.content')[0]).replace('<br/>','</p><p>')
    # # contentText=re.sub('<span>(.*?)</span>','',content)
    # title=str(soup.select('div.detail > dl > dt > h1')[0].text).split('\n')[0]
    # print(title)
    # title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
    #     .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(content)
    f.close()
def getTitle(thmlpath, strUrl,file):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm_addon > div.news_title > p > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        file.write(href+'\n')
def writeUrl(thmlpath, strUrl,file):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('ul.one.menus > li > a')
    for i,a in enumerate(a_list):
        if i > 0 and i < (len(a_list)-1):
            href=a.get('href')
            print(href)
            newUrl='http://www.haoword.com'+href
            getTitle(thmlpath, newUrl,file)

def main(thmlpath,txtFilepath):
            #打开文本图片集
            strUrl='http://www.haoword.com/'









            file=open(txtFilepath,'w')
            writeUrl(thmlpath, strUrl,file)
            file.close()


def mainGetHtml(thmlpath, txtFilepath):
    # 打开文本图片集
    strUrl = 'http://www.haoword.com/'
    file = open(txtFilepath, 'r')
    file_lines=file.readlines()

    for i,line in enumerate(file_lines):
        print('-----',i)
        url=line.replace('\n','')
        browser = webdriver.Chrome()
        for nmber in range(407320,944724):
            newUrl=url+str(nmber)+'.htm'
            print(newUrl)
            # newUrl='http://www.haoword.com/gongzuozongjie/dangjian/677320.htm'
            try:
                getHtml(thmlpath, newUrl,browser)
            except:
                print('error')



if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'./lianjie.txt'
    # main(thmlpath,txtFilepath)
    mainGetHtml(thmlpath, txtFilepath)
