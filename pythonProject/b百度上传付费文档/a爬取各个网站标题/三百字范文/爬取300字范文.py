import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_300zi(url,file,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.article-header > h1')[0].text
        print(title)
        file.write(title+'\n')
        # print('300字下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    # url = 'https://www.300zi.com/zhufuyu/1012339.html'
    # htmlPath=r'D:\文档\百度付费上传文档\百度html\其他网站\第一范文网'
    # keyStr='kaixin'
    # title=''
    # mian_300zi(url,htmlPath,keyStr,title)

    url = 'https://www.300zi.com/%s/%s.html'
    htmlPath = r'D:\文档\百度付费上传文档\需要的标题及其chatgpt\title\300字范文.txt'
    keyStr = 'kaixin'
    title = ''
    file = open(htmlPath, 'w', encoding='utf-8')
    keys = ['gongzuozongjie', 'ziwojieshao',
            'xindetihui', 'ziwojianding',
            'yanjianggao', 'gongzuozongjie',
            'cizhibaogao', 'shixibaogao',
            'nianzhongzongjie']
    for key in keys:
        for i in range(100249, 1483249):
            newUrl = url % (key, str(i))
            # print(newUrl)
            mian_300zi(newUrl, file, keyStr, title)
    file.close()