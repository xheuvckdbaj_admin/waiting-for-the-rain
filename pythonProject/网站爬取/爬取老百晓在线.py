# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.lbx777.net/' + str
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('div > h1')[0].text
    print(title)
    list_soupa = str(soup.select('td.unnamed1'))
    print(list_soupa)
    content=[]

    #content=re.sub('<a href=.*?</a></p>','',list_soupa)
    #title = soup.select('.article > .title')[0].text
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,yemian,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('td > a')
    #print(url_list)
    for a in a_list:
        try:
            #a=a_list[27]
            href=a.get('href')
            newUrl=url.replace(yemian,href)
            title=a.text
            getContent(thmlpath,newUrl,title)
        except:
            print('错误')
            continue
def getUrlIndex(thmlpath,string,url):
    requests_text = requests.get(url=url, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('a')
    #print(url_list)
    for a in a_list:
        try:
            #a=a_list[44]
            src=a.get('href')
            href=string+'/'+src
            print(href)
            yemian=src.split('/')[1]
            #title = u.select('dt > a')[0].text
            getUrl(thmlpath,yemian,href)
        except:
            print('错误')
            continue

def getUrlPageNum(url,str):
    for pageNum in range(1,1000):
        try:
            if pageNum==1:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    newUrl=url+str+'.html'
                    getUrlIndex(thmlpath, newUrl)

            else:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    oldUrl=url+str+'_%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(thmlpath, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    ce_list = ['yw01/yice','yw0002/erce','yw03/sance','yw04/sice','yw05/wuce','yw06/liuce','yw07/qice',
           'yw08/bace','yw09/jiuce','yw10/shice','yw11/shiyice','yw12/shierce']
    ban_list=['rb','s','x','y_a','y_s ','xs','b',
           'h','e','j','l','c',
           'k','bj','sh']

    for s in ce_list:
        try:
            string = s.split('/')[0]
            urlString =groupUrl(string)
            url = groupUrl(s)

            for ban in ban_list:
                newUrl=url+'_'+ban+'.htm'
                getUrlIndex(thmlpath,urlString, newUrl)
        except:
            print('ip被封')
            time.sleep(600)
            continue



if __name__ == '__main__':
    thmlpath = r'E:\老百晓'
    main(thmlpath)
