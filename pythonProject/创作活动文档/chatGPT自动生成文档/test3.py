import threading
import time
from docx import Document
from docx.enum.base import XmlMappedEnumMember
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn
from playwright.sync_api import Playwright, sync_playwright, expect

from pythonProject.创作活动文档.chatGPT自动生成文档.chatGPT生成单个文件 import merrageLastPrage, markjudge, \
    merrageLastPrageContentStr, deletesingle, deletePragram, sezhiParas, sezhiPaper, sezhiTtitle

paghraDownPath = './删除活动文档段落违禁词.txt'


def deletePragram(paghraDownPath, content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    if panduan:
        content = ''
    return content
def makecontent(page,titleStr):
    page.goto("https://f.aizhineng.cc/")
    page.get_by_placeholder("它几乎无所不能，点此提问").click()
    page.get_by_placeholder("它几乎无所不能，点此提问").fill(titleStr)
    page.get_by_text("发送").click()
    time.sleep(20)
    message=page.locator('//li[@class="article-content"]/pre').text_content()
    print(message)
    if 'AI' in message or '对不起，我不知道该怎么回答' in message:
        return ''
    else:
        return message
def makeWordChatGPT(folderPath, title, content):
    try:
        title = title
        filePathTxt = folderPath + '\\' + title + '.txt'
        filePathDocx = folderPath + '\\' + title + '.docx'
        doc = Document()
        core_properties = doc.core_properties
        core_properties.author = ''
        sezhiPaper(doc)
        sezhiTtitle(doc, title)
        file = open(filePathTxt, 'w', encoding='utf-8')
        file.write(content)
        file.close()
        filer = open(filePathTxt, 'r', encoding='utf-8')
        file_lines = filer.readlines()
        for i,line in enumerate(file_lines):
            contentStr = line.replace('\n', '')
            contentStr = merrageLastPrage(i,file_lines,contentStr)
            contentStr = merrageLastPrageContentStr(i, file_lines, contentStr)
            contentStrA = markjudge(contentStr)
            contentStrB = deletesingle(contentStrA)
            contentStrC = deletePragram(paghraDownPath, contentStrB)
            sezhiParas(doc, contentStrC)
        doc.save(filePathDocx)
        filer.close()
    except:
        print('文档修改错误error')
def run(playwright: Playwright,api_key,file_lines,folderPath, start, last) -> None:
    browser = playwright.chromium.launch(headless=True)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://f.aizhineng.cc/")
    page.get_by_placeholder("sk-xxxxxxxxxx").click()
    page.get_by_placeholder("sk-xxxxxxxxxx").fill(api_key)
    page.get_by_text("查询余额").click()
    time.sleep(2)
    for i,title in enumerate(file_lines):
        if i >= start and i < last:
            titleStr=title.replace('\n','')
            content=makecontent(page,titleStr).strip()
            # content = str(getChatGPT_content(lengthTitle, api_key)).strip()

            if len(content) < 15:
                content = content + '\n' + makecontent(page,titleStr+',展开说说').strip()
            try:
                makeWordChatGPT(folderPath, titleStr, content)
            except:
                print('chatGPT error')
    # ---------------------
    context.close()
    browser.close()

def makeContentGPT(api_key,file_lines,folderPath, start, last):
    with sync_playwright() as playwright:
        run(playwright,api_key,file_lines,folderPath, start, last)
def main():
    api_key='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'
    txtfolder = r'D:\文档\百度活动文档\百度活动文档标题'
    folderPath='D:\文档\百度活动文档\百度html'
    txtPath=txtfolder+'\\'+'all.txt'
    fileTitle=open(txtPath,'r',encoding='utf-8')
    file_lines=fileTitle.readlines()
    start=0
    last = len(file_lines)
    print(last)
    chatAn = int((last - start) / 5)
    t1 = threading.Thread(target=makeContentGPT,
                          args=(api_key,file_lines,folderPath, 0, chatAn))
    t2 = threading.Thread(target=makeContentGPT,
                          args=(api_key,file_lines,folderPath, chatAn, chatAn * 2))
    t3 = threading.Thread(target=makeContentGPT,
                          args=(api_key,file_lines,folderPath, chatAn * 2, chatAn * 3))
    t4 = threading.Thread(target=makeContentGPT,
                          args=(api_key,file_lines,folderPath, chatAn * 3, chatAn * 4))
    t5 = threading.Thread(target=makeContentGPT,
                          args=(api_key,file_lines,folderPath, chatAn * 4, last))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    # makeContentGPT(api_key,file_lines,folderPath, start, last)
if __name__ == '__main__':
    main()