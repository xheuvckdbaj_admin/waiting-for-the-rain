# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="qian-hou"]/span[@class="hou"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.cehuashuwang.com/geshi/article_104419.html',
                'https://www.cehuashuwang.com/moban/article_108007.html',
                'https://www.cehuashuwang.com/zenmexie/article_49429.html',
                'https://www.cehuashuwang.com/hunli/article_108005.html',
                'https://www.cehuashuwang.com/wangzhan/article_107752.html',
                'https://www.cehuashuwang.com/cuxiaohuodong/article_93152.html',
                'https://www.cehuashuwang.com/tuanrihuodong/article_105973.html',
                'https://www.cehuashuwang.com/shetuanhuodong/article_104830.html',
                'https://www.cehuashuwang.com/xiaoyuanhuodong/article_107545.html',
                'https://www.cehuashuwang.com/banjihuodong/article_107486.html',
                'https://www.cehuashuwang.com/yuandanhuodong/article_107764.html',
                'https://www.cehuashuwang.com/yuandanfangan/article_107881.html',
                'https://www.cehuashuwang.com/qingrenjiehuodong/article_107749.html',
                'https://www.cehuashuwang.com/nvshengjiehuodong/article_106441.html',
                'https://www.cehuashuwang.com/jiandangjiehuodong/article_11315.html',
                'https://www.cehuashuwang.com/yingxinwanhui/article_106600.html',
                'https://www.cehuashuwang.com/biyewanhui/article_107809.html',
                'https://www.cehuashuwang.com/yuandanwanhui/article_105072.html',
                'https://www.cehuashuwang.com/chunjiewanhui/article_107288.html',
                'https://www.cehuashuwang.com/yuanxiaowanhui/article_16624.html',
                'https://www.cehuashuwang.com/bisai/article_106406.html',
                'https://www.cehuashuwang.com/bianlunsai/article_107526.html',
                'https://www.cehuashuwang.com/yundonghui/article_107679.html',
                'https://www.cehuashuwang.com/quweiyundonghui/article_97167.html',
                'https://www.cehuashuwang.com/zhishijingsai/article_105887.html',
                'https://www.cehuashuwang.com/qiye/article_107763.html',
                'https://www.cehuashuwang.com/zanzhu/article_104565.html',
                'https://www.cehuashuwang.com/pinpai/article_106455.html',
                'https://www.cehuashuwang.com/xiangmu/article_107525.html',
                'https://www.cehuashuwang.com/lazanzhu/article_103074.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
