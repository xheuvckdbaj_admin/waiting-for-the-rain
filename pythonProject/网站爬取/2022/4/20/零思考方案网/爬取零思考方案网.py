# -*- coding:utf-8 -*-
import re
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#article-content1')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>')
            title = soup.select('div.content_left > h1 > a')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace('\r', '').replace('\n', '').replace(' ', '')
            print(title)
            # list_soupa1 = re.sub('<a(.*?)</a>', '', list_soupa)
            # list_soupa2 = re.sub(title, '', list_soupa1)
            # content = re.sub('下载Word文档到电脑，方便收藏和打印', '', list_soupa2).replace('编辑推荐：','').replace('下载Word文档','').replace('TAG标签：','')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')

def main(txtPath,thmlpath):
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for a,line in enumerate(lines):
        url=line.replace('\n','')
        for i in range(1,30000):
            print('----------',str(a)+'*'+str(i))
            newUrl=url%i
            getHtml(thmlpath, newUrl)


if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\自查报告网'
    main(txtPath,thmlpath)
