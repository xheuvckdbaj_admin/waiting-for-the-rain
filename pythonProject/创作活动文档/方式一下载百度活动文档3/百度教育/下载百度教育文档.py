import time
import urllib

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档3.b整理文档.html转成word import shear_dile
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver, getDriver2

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}



def getUrlAndTitle(driver,htmlPath,keyStr):
    try:
        elements = driver.find_elements(By.XPATH,
            '//div[@class="title"]')
        for i, element in enumerate(elements):
            try:
                title = element.text.replace(' ', '').strip()
                if keyStr[0:3] in title and (keyStr[-2:] in title or '字' in keyStr[-2:]):
                    driver.find_element(By.XPATH,'//div[@class="title"]/span').click()
                    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                    driver.switch_to.window(n[-1])
                    time.sleep(0.5)
                    try:
                        element = WebDriverWait(driver, 5).until(
                            EC.presence_of_element_located((By.XPATH, '//div[@class="composition-content"]'))
                        )
                    except:
                        time.sleep(0.1)
                    td=driver.find_element(By.XPATH,'//div[@class="composition-content"]')
                    list_soupa = td.get_attribute('innerHTML')
                    print(title)
                    thmlpath = htmlPath + '\\' + keyStr + '.html'
                    f = open(thmlpath, 'w', encoding='utf-8')
                    f.write(list_soupa)
                    break
            except:
                print('error')
                continue
    except:
        print('error')
def getPageUrl(key,newUrl,driver,htmlPath):
    try:
        keyStr=key.replace('/','')
        driver.get(newUrl)
        time.sleep(0.1)
        # driver.find_element(By.XPATH,'//div[@class="search-cont clearfix"]/form[@class="search-form"]/input[@class="hdi"]').send_keys(key)
        # time.sleep(0.1)
        # driver.find_element(By.XPATH,'//div[@class="search-cont clearfix"]/form[@class="search-form"]/button[@class="btn-global"]').click()
        # print(key)
        # n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        # driver.switch_to.window(n[-1])
        # time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="title"]'))
            )
        except:
            time.sleep(0.1)
        getUrlAndTitle(driver,htmlPath,keyStr)
    except:
        print('error')

def getKey(txtPath,url,driver,htmlPath):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i,line in enumerate(lines):
        if i > -1:
            key=line.replace('\n','')
            print('-----------------',key)
            newUrl=url%key
            getPageUrl(key, newUrl,driver,htmlPath)
    # pathFile.close()
def mainBaiDuJiaoYu(txtPath,htmlPath):
    urls=[]
    driver = getDriver2('baidu')
    url='https://easylearn.baidu.com/edu-page/tiangong/composition/list?query=%s'
    txtFilePaths=find_file(txtPath)
    for txtFilePath in txtFilePaths:
        getKey(txtFilePath, url,driver,htmlPath)
    driver.quit()
    shear_dile(htmlPath)
    # return urls


if __name__ == '__main__':
    #存放活动文档标题的文件夹
    txtPath = r'D:\文档\百度活动文档3\百度活动文档标题'
    #下载的活动文档存放文件夹
    htmlPath=r'D:\文档\百度活动文档3\百度html\百度教育'
    mainBaiDuJiaoYu(txtPath,htmlPath)
