# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getContent(thmlpath,pageUrl,title):
    requests_text = str(requests.get(url=pageUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('#result')[0])
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()


def getOtherUrl2(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('tr > td.dh > a.childclass')
    for i, a in enumerate(a_list):
        href = a.get('href')
        print(href)
        getPageUrl(thmlpath,href)
def getAllUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('tr > td > a.nr')
    for i,a in enumerate(a_list):
        href=a.get('href')
        title=a.text
        title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
        getContent(thmlpath, href, title)
        print(href,title)

def getPageUrl(thmlpath,strUrl):
    try:
        getAllUrl(thmlpath,strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_href = soup.select('tr > td > div.showpage > a')[0].get('href')
        print(a_href)
        url=strUrl+'List_(.*?).html'
        dUrl=strUrl+'List_%d.html'
        pageNumber=int(re.findall(url,a_href)[0])
        for i in range(1,pageNumber+1):
            newUrl=dUrl%i
            print(newUrl)
            getAllUrl(thmlpath,newUrl)
    except:
        getOtherUrl2(thmlpath,strUrl)


def getOtherUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('tr > td.dh > a.childclass')
    for i, a in enumerate(a_list):
        href = a.get('href')
        print(href)
        getPageUrl(thmlpath,href)
        # getOtherUrl(href)
def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('tr > td > a.dh')
    for i ,a in enumerate(a_list):
        if not i==0:
            href=a.get('href')
            if i==4:
                href='http://www.11665.com/computer'+href
            print('--------',href)
            # try:
            getOtherUrl(thmlpath,href)
            # except:
            #     print('error')
    # print(title)
    # title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
    #     .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    # contentText = str(soup.select('div.con')[0])
    # pageAs=soup.select('div.dede_pagess > ul.pagelist > li > a')
    # if len(pageAs) > 0:
    #     page=soup.select('div.dede_pagess')[0]
    #     contentPage=getOtherUrl(page, strUrl)
    #     contentText=contentText+contentPage
    # # contentText=re.sub('<span>(.*?)</span>','',content)
    # contentText=contentText.replace('※','')

def main(thmlpath):
    # 百度搜索“77
    # cn”或“免费范文网”即可找到本站免费阅读全部范文。收藏本站方便下次阅读，免费范文网，提供经典小说申报材料人民满意公务员先进集体申报材料在线全文阅读
    strUrl='http://www.11665.com/'
    getHtml(thmlpath, strUrl)



if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
