import time

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}



def getUrlAndTitle(newUrl,driver,file):
    try:
        driver.get(newUrl)
        time.sleep(0.3)
        elements = driver.find_elements_by_xpath(
            '//div[@class="normal-result"]/div[@class="sula-item search-result-wrap_8baa4"]/div[@class="title-box_8baa4"]/a[@class="search-result-title_8baa4"]')
        for i, element in enumerate(elements):
            try:
                href = element.get_attribute('href')
                hrefUrl = href.split('.html')[0] + '?pcqq.c2c&bfetype=old'
                title = element.text.replace('- 百度文库', '').replace('_百度文库', '').replace('百度文库', '').strip()
                print(hrefUrl)
                lineStr = hrefUrl + '————' + title+'\n'
                file.write(lineStr)
            except:
                print('error')
                continue
    except:
        time.sleep(15)
        print('error')
def getPageUrl(key,url,driver,filePath):
    keyStr=key.replace(' ','').replace('/','')
    path=filePath+'\\'+keyStr+'.txt'
    file=open(path,'w',encoding='utf-8')
    for i in range(1,80):
        newUrl=url%(key,i)
        print(key,i)
        getUrlAndTitle(newUrl,driver,file)
    file.close()

def getKey(txtPath,url,driver,filePath):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i,line in enumerate(lines):
        if i > 500:
            key=line.replace('\n','')
            print('-----------------',key)
            getPageUrl(key, url,driver,filePath)
def main():
    filePath=r'E:\文档\豆丁文档\test'
    driver = webdriver.Chrome()
    txtPath=r'.\key.txt'
    url='https://wenku.baidu.com/search?word=%s&searchType=0&lm=0&od=0&fr=search&ie=utf-8&pn=%d'
    getKey(txtPath, url,driver,filePath)


if __name__ == '__main__':
    main()