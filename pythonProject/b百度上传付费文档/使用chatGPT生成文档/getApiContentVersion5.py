import openai

def getApiContentByTitleAndKey(api_key,prompt):
    try:
        openai.api_key = api_key
        model_engine = "gpt-3.5-turbo"
        # answer_language='zh-CN'
        # try:
        completions = openai.ChatCompletion.create(
            model=model_engine,
            messages=[
                # {'role':'system','content':'你是一个文档生产者。'},
                {'role': 'user', 'content': prompt}
            ]
        )

        # message = completions.choices[0].text
        # print(completions)
        result = ''
        for choice in completions.choices:
            result += choice.message.content
        # print(result)
        return result
    except Exception as e:
        return str(e)

def main():
    api_key = "sess-MGZDMaM9rrp3SGs1Ujs1w3Eqx00n1Qh0k8Yh96j5"
    prompt = "老人八十岁过生邀请函"
    apiContent=getApiContentByTitleAndKey(api_key,prompt)
    print(apiContent)
if __name__ == '__main__':
    main()