# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

from pythonProject.FindFile import find_file, find_name
from pythonProject.初中.敏感词过滤.minganci import filterWord
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


def deletePara(file):
    for para in file.paragraphs:

        para.text = para.text.replace('\n', ' ')
        if para.text == "" or para.text == '\n' or para.text == ' ':
            # print('第{}段是空行段'.format(i))
            para.clear()  # 清除文字，并不删除段落，run也可以,
            delete_paragraph(para)


# 替换关键词
def tihuanguanjianci(lines, para):
    para.text = para.text.replace('(\
)', '(')
    for line in lines:
        keyValue = line.replace('\n', '')
        key = keyValue.split('===>')[0]
        # print(key)
        value = keyValue.split('===>')[1]
        para.text = para.text.replace(key, value)


def jianchaPara(para):
    if para.text.endswith('，') or para.text.endswith('、') or para.text.endswith('(') or para.text.endswith('{') \
            or para.text.endswith('[') or para.text.endswith('·') or para.text.endswith('——'):
        para.text = para.text[0:-1]
    if (para.text.startswith('。') or para.text.startswith('，') or para.text.startswith('：')
            or para.text.startswith('！') or para.text.startswith('？') or para.text.startswith('、')
            or para.text.startswith('；') or para.text.startswith(')')):
        para.text = para.text[1:]


titlePath = './标题违禁词.txt'


def panduanFileName(fileName):
    file = open(titlePath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = True
    for i, line in enumerate(lines):
        key = line.replace('\n', '')
        panduan = panduan and (not key in fileName)
    return panduan

def panduanHead(content):
    panduan=content.endswith('一：') or content.endswith('二：') or content.endswith('三：') or \
    content.endswith('四：') or content.endswith('五：') or content.endswith('六：') or \
    content.endswith('七：') or content.endswith('八：') or content.endswith('九：') or \
    content.endswith('十：') \
    or content.endswith('（一）') or content.endswith('（二）') or \
    content.endswith('（三）') or content.endswith('（四）') or content.endswith('（五）') or \
    content.endswith('（六）') or content.endswith('（七）') or content.endswith('（八）') or \
    content.endswith('（九）') or content.endswith('（十）') or content.endswith('(一)') or \
            content.endswith('(一)') or content.endswith('(二)') or content.endswith('(三)') or \
            content.endswith('(四)') or content.endswith('(五)') or content.endswith('(六)') or \
            content.endswith('(七)') or content.endswith('(八)') or content.endswith('(九)') or \
            content.endswith('(十)') or content.endswith('〔一〕') or content.endswith('〔二〕') or \
            content.endswith('〔三〕') or \
            content.endswith('〔四〕') or content.endswith('〔五〕') or content.endswith('〔六〕') or \
            content.endswith('〔七〕') or content.endswith('〔八〕') or content.endswith('〔九〕') or \
            content.endswith('〔十〕') or content.endswith('【一】') or content.endswith('【二】') or \
            content.endswith('【三】') or \
            content.endswith('【四】') or content.endswith('【五】') or content.endswith('【六】') or \
            content.endswith('【七】') or content.endswith('【八】') or content.endswith('【九】') or \
            content.endswith('【十】') or content.endswith('【篇一】') or content.endswith('【篇二】') or \
            content.endswith('【篇三】') or \
            content.endswith('【篇四】') or content.endswith('【篇五】') or content.endswith('【篇六】') or \
            content.endswith('【篇七】') or content.endswith('【篇八】') or content.endswith('【篇九】') or \
            content.endswith('【篇十】') or content.endswith('篇一') or content.endswith('篇二') or \
            content.endswith('篇三') or \
            content.endswith('篇四') or content.endswith('篇五') or content.endswith('篇六') or \
            content.endswith('篇七') or content.endswith('篇八') or content.endswith('篇九') or \
            content.endswith('篇十')  or content.startswith('第1篇') or content.startswith('第2篇') or \
            content.startswith('第3篇') or \
            content.startswith('第4篇') or content.startswith('第5篇') or content.startswith('第6篇') or \
            content.startswith('第7篇') or content.startswith('第8篇') or content.startswith('第9篇') or \
            content.startswith('第10篇')
    return panduan
def panduanPara(content):
    panduan=len(content) < 45 and(content.startswith('一、') or content.startswith('二、') or \
    content.startswith('三、') or \
    content.startswith('四、') or content.startswith('五、') or content.startswith('六、') or \
    content.startswith('七、') or content.startswith('八、') or content.startswith('九、') or \
    content.startswith('十、'))
    return panduan
def xuhaLoss(contentStr):
    panduan = False
    if '二、' in contentStr and (not '一、' in contentStr):
        print('a')
        panduan =True
    # if '二：' in contentStr and (not '一：' in contentStr):
    #     print('b')
    #     panduan = True
    if '（二）' in contentStr and (not '（一）' in contentStr):
        print('c')
        panduan =True
    if '(二)' in contentStr and (not '(一)' in contentStr):
        print('d')
        panduan =True
    if '〔二〕' in contentStr and (not '〔一〕' in contentStr):
        print('e')
        panduan =True
    if '【二】' in contentStr and (not '【一】' in contentStr):
        print('f')
        panduan =True
    if ('篇二' in contentStr or '篇三' in contentStr) and (not '篇一' in contentStr):
        print('g')
        panduan =True
    if ('第2篇' in contentStr or '第3篇' in contentStr) and (not '第1' in contentStr):
        print('h')
        panduan =True

    if '2、' in contentStr and (not '1、' in contentStr):
        print('j')
        panduan =True
    if ('篇2' in contentStr or '篇3' in contentStr) and (not '篇1' in contentStr):
        print('m')
        panduan =True
    return panduan

def deleteLaterParaPanduan(i, content, fileName):
    panduan = False
    content = content.replace(' ', '')
    if (not (i == 1 and i == 2)) and len(content) < 100:
        titleContent = fileName.replace('.docx', '').replace('[', '').replace(']', '').replace('【', '').replace('】',
                                                                                                                '').replace(
            '(', '').replace(')', '').replace(' ', '')
        content = content.replace('[', '').replace(']', '').replace('【', '').replace('】', '').replace('(', '').replace(
            ')', '').replace(' ', '')
        panduan = ((content[-5:-1] == titleContent[-5:-1]) or (content[0:5] == titleContent[0:5]))
    if content == '-' or content == '1' or content == '2' or content == '3' or content == '4' or content == '5' or content == '':
        panduan = True
    return panduan
def deleteLastParaclassfly(i, content,file):
    panduan = False
    if i==(len(file.paragraphs)-1):
        if content.startswith(':'):
            panduan = True
    return panduan


paghraPath = './删除段落的违禁词.txt'


def panduanPaghra(paghraPath, content):
    file = open(paghraPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan


filterTxt = './内容敏感词.txt'


def filterDocx(panduan, filterTxt, content):
    file = open(filterTxt, 'r', encoding='utf-8')
    lines = file.readlines()
    for i, line in enumerate(lines):
        key = line.replace('\n', '')
        panduan = panduan or (key in content)
        if  key in content:
            print(key)
    return panduan


# 删除最后一段含有“：”的段落
def merrageLastPrageContentStr(i, paragraphs, contentStr):
    if i == (len(paragraphs) - 1):
        if contentStr.endswith('：'):
            contentStr = ''
    return contentStr


paghraDownPath = './删除以下段落的违禁词.txt'


def panduanPaghraDown(paghraDownPath, content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan


# 删除word段落
def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    # p._p = p._element = None
    paragraph._p = paragraph._element = None


def insertPara(para, fileName):
    para = para.insert_paragraph_before('')
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    run = para.add_run(fileName)
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(18)
    run.font.color.rgb = RGBColor(255, 000, 000)
    para.paragraph_format.space_after = Pt(20)
    run.font.bold = True

def merrageLastPrage(i, paragraphs, para):
    if i == (len(paragraphs) - 1):
        if (not (para.text.endswith('.') or para.text.endswith('。') or para.text.endswith('！')
                 or para.text.endswith('!') or para.text.endswith('?') or para.text.endswith('？')
                 or para.text.endswith('……') or para.text.endswith('......') or para.text.endswith('”')
                 or para.text.endswith('；') or para.text.endswith(';') or para.text.endswith('；')
                 or para.text.startswith('（')
                 or para.text.startswith('2'))):
            if '。' in para.text:
                para.text=para.text.replace(para.text.split('。')[-1],'')
            elif '，' in para.text:
                para.text=para.text.replace(para.text.split('，')[-1],'')
    if para.text.endswith('1.') or para.text.endswith('2.') or para.text.endswith('3.') \
            or para.text.endswith('4.') or para.text.endswith('5.') or para.text.endswith('6.')\
            or para.text.endswith('7.') or para.text.endswith('8.') or para.text.endswith('9.') \
            or para.text.endswith('0.'):
        if '。' in para.text:
            para.text = para.text.replace(para.text.split('。')[-1], '')
        elif '，' in para.text:
            para.text = para.text.replace(para.text.split('，')[-1], '')
    return para.text
def deleAdvertising(filePath, newFolderPath, midFolderPath, newfileNames, lines, front, later):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        try:
            file = docx.Document(filePath)
            fileName = os.path.basename(filePath)
            fileName = fileName.replace('.docx', '')
            if len(fileName) < 8:
                fileName = fileName + '范文'
                fileName = fileName
            if panduanFileName(fileName):
                # print("段落数:" + str(len(file.paragraphs)))
                # print('删除前图形图像的数量：', len(file.inline_shapes))
                str = []
                contentStr = ''
                flag = False
                for i, para in enumerate(file.paragraphs):
                    merrageLastPrageContentStr(i, file.paragraphs, para.text)
                    merrageLastPrage(i, file.paragraphs, para)
                    # 替换关键词，洗稿
                    tihuanguanjianci(lines, para)
                    # 修改段落结尾符号使用错误
                    jianchaPara(para)
                    contentStr = contentStr + para.text
                    # para.text=modifyString(para.text)
                    # print(para.text)
                    if len(para.text) < 100 and panduanPaghraDown(paghraDownPath, para.text):
                        # print(para.text)
                        flag = True
                        # for par in range(i, len(file.paragraphs)):
                        #     str.append(par)
                    if panduanPaghra(paghraPath, para.text):
                        str.append(i)
                        delete_paragraph(para)
                        continue
                    if deleteLaterParaPanduan(i, para.text, fileName):
                        str.append(i)
                        delete_paragraph(para)
                        continue
                    if para.text.strip() == '':
                        delete_paragraph(para)
                        continue
                    if deleteLastParaclassfly(i, para.text, file):
                        delete_paragraph(para)
                        continue

                    if flag is True:
                        delete_paragraph(para)
                deletePara(file)
                # paraNumber = 0
                # for i in enumerate(str):
                #     number = i - paraNumber
                #     paragraph = file.paragraphs[number]
                #     delete_paragraph(paragraph)
                #     paraNumber = paraNumber + 1

                # print('删除前图形图像的数量：', len(file.inline_shapes))
                fileName = fileName.replace('2016', '2024').replace('2017', '2024').replace(
                    '2018', '2024').replace('2019', '2024').replace('2020', '2024').replace(
                    '【微信公众号：wkgx985 免费获取】',
                    '').replace('2015',
                                '2024').replace(
                    '2010', '2024').replace('2008', '2024').replace('2009', '2024').replace('2007', '2024').replace(
                    '2006', '2024').replace('2005', '2024').replace(
                    '2011', '2024').replace('2012', '2024').replace('2013', '2024').replace('2014', '2024').replace(
                    '2019', '2024').replace(
                    '2020', '2024').replace('2004', '2024') \
                    .replace('2017', '2024').replace('2018', '2024') \
                    .replace('2015', '2024').replace('2016', '2024')\
                    .replace('2021', '2024').replace('2022', '2024').replace('2023', '2024') \
                    .replace('._', '').replace('★', '').replace('#', '') \
                    .replace('•', '').replace('！', '').replace('?', '').replace('？', '') \
                    .replace('•', '').replace('~', '').replace('✓', '').replace('.', '').replace('+', '') \
                    .replace(',', '').replace('docx', '').replace('公众号：', '') \
                    .replace('､', '').replace('。', '').replace('▪', '').replace('，', '') \
                    .replace('．', '').replace('公众号:', '').replace('：', '') \
                    .replace('、', '').replace('（喜子的商铺）', '').replace('·', '') \
                    .replace('原创', '精品').replace('转载', '精品') \
                    .replace('&lt', '精品').replace(';', '精品').replace('&gt', '精品') \
                    .replace('11', '').replace('＊', '').replace(',', '') \
                    .replace('十篇', '').replace('九篇', '').replace('八篇', '')\
                    .replace('七篇', '').replace('六篇', '').replace('五篇', '')\
                    .replace('四篇', '').replace('三篇', '').replace('多篇', '')\
                    .replace('11篇', '').replace('10篇', '').replace('9篇', '')\
                    .replace('8篇', '').replace('7篇', '').replace('6篇', '') \
                    .replace('5篇', '').replace('4篇', '').replace('3篇', '').replace('（）','')
                file_name2 = ''.join(fileName.split())
                file_name3 = re.sub('【.*?】', '', file_name2)
                file_name1 = re.sub('\(.*?\)', '', file_name3)
                file_name = re.sub('www.*?com', '', file_name1)
                file_name = file_name
                paths = r'./敏感词过滤/minganci.txt'
                # 给word文档插入标题
                insertPara(file.paragraphs[0], file_name)
                panduan = False
                if len(contentStr) > 300 :
                    if file.save(newFolderPath + '\\' + file_name + '.docx') == None:
                        # newFile = docx.Document(newFolderPath + '\\' + fileName)
                        # delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                        os.remove(filePath)  # 删除原来修改前的docx文件
                    if file_name in newfileNames:
                        os.remove(filePath)
                else:
                    file.save(midFolderPath + '\\' + fileName + '.docx')
                    os.remove(filePath)
            else:
                file.save(midFolderPath + '\\' + fileName + '.docx')
                os.remove(filePath)
        except:
            print('错误')


# word.Quit()
def shear_dile(oldFolderPath, newFolderPath, midFolderPath, newfileNames, lines):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i, d in enumerate(os.listdir(oldFolderPath)):
                shear_dile(os.path.join(oldFolderPath, d), newFolderPath, midFolderPath, newfileNames, lines)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.docx'):
            print(oldFolderPath)
            deleAdvertising(oldFolderPath, newFolderPath, midFolderPath, newfileNames, lines, 0, 0)


def modifyContent(oldFolderPath, newFolderPath, midFolderPath):
    newfileNames = find_name(newFolderPath, [])
    paths = r'./doc_replace.txt'
    fileTxt = open(paths, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    shear_dile(oldFolderPath, newFolderPath, midFolderPath, newfileNames, lines)


def mainModify(oldFolderPath, newFolderPath, midFolderPath):
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    modifyContent(oldFolderPath, newFolderPath, midFolderPath)


if __name__ == '__main__':
    oldFolderPath = r'D:\文档\百度付费上传文档\百度html'
    newFolderPath = r'D:\文档\百度付费上传文档\中介'
    midFolderPath = r'D:\文档\百度付费上传文档\自查报告网url'
    mainModify(oldFolderPath, newFolderPath, midFolderPath)
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
