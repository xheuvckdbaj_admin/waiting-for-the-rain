# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os

from bs4 import BeautifulSoup


# 获取真实预览地址turl


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.kt250.com/fanwen/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    # try:
        #url='http://www.doczj.com/doc/3c412242cf84b9d528ea7a66.html'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        #print(title)
        # div = str(soup.select('script')[0])
        # num=re.match('(.*?)共(.*?)页',requests_text).group(2)
        contents=''
        for i in (1,10+1):
            try:
                newUrl=url.split('.html')[0]+'-'+str(i)+'.html'
                requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
                soup = BeautifulSoup(requests_text, 'lxml')
                content = str(soup.select('.tbody.clearfix > #contents ')[0])
            except:
                print('error')
                content=''
            contents=contents+content
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(contents)
    # except:
    #     print('错误')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.lists > .lista > p > a')
    # print(url_list)
    for a in a_list:
        src = a.get('href')
        href = 'http://www.doczj.com' + src
        print(href)
        title=a.text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
        getContent(thmlpath, href,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.lists > .lista > p > a')
    #print(url_list)
    for a in a_list:
            src=a.get('href')
            href='http://www.doczj.com'+src
            print(href)
            getUrl(thmlpath,href)

def getUrlPageNum(thmlpath,url,strMap):
    for num in strMap:


                    oldUrl = url%num
                    print(oldUrl)
                    getUrlIndex(thmlpath, oldUrl)







def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            getUrl(thmlpath, url)
        except:
            continue


def main(thmlpath):
        strMap=[203,47,75,11,10,89,106,126,17,71,77,229,64,68]
        url = 'http://www.doczj.com/topics/%d/'
        getUrlPageNum(thmlpath,url,strMap)


if __name__ == '__main__':
    thmlpath = r'E:\文档\文档之家'
    main(thmlpath)
