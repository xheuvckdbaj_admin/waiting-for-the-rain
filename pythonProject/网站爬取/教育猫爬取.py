# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = soup.select('title')[0].text
    #print(title)
    list_soupa = str(soup.select('.content')[0]).split('<script>')[0]
    #print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content)
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('.jiaoshimao_left_ul')
    print(url_list)
    for u in url_list:
        for i in u.select('li > a'):
            href=i.get('href')
            getContent(thmlpath,href)




def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['youer','xiaoxue','chuzhong','gaozhong','zuowen','kaoshi','sucai','jiaoxue']
    for s in str:
        if s == 'youer':
            strUrl = ['gushi']
            getHtml(thmlpath, strUrl, s)

        if s == 'xiaoxue':
            strUrl = ['yuwen','shuxue','yingyu']
            getHtml(thmlpath, strUrl, s)

        if s == 'chuzhong':
            strUrl = ['yuwen','shuxue','yingyu','lishi','zhengzhi','shengwu','huaxue','wuli']
            getHtml(thmlpath, strUrl, s)

        if s == 'gaozhong':
            strUrl = ['yuwen','shuxue','yingyu','lishi','zhengzhi','shengwu','huaxue','wuli']
            getHtml(thmlpath, strUrl, s)

        if s == 'zuowen':
            strUrl = ['']
            getHtml(thmlpath, strUrl, s)

        if s == 'kaoshi':
            strUrl = ['xiaoshengchu','zhongkao','gaokao']
            getHtml(thmlpath, strUrl, s)

        if s == 'sucai':
            strUrl = ['']
            getHtml(thmlpath, strUrl, s)

        if s == 'jiaoxue':
            strUrl = ['']
            getHtml(thmlpath, strUrl, s)


if __name__ == '__main__':
    thmlpath = r'E:\test'
    main(thmlpath)
