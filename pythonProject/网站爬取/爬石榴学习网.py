# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm
from lxml import etree

session=requests.session()
#获取源代码
def getText(url,decode):
    # 获取真实预览地址turl
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
    return session.get(url).content.decode(decode)
#获取参数字典
def getHashMap(url):
    return
#获取文件类型
def parser_type(content):
    return
#获得文件标题和该标题的网址
def parser_title(content,oldUrl):
    tree=etree.HTML(content)
    #bs=BeautifulSoup(content,'lxml')
    li_list=tree.xpath('//div[@class="col-lg-12"]/dl[@class="row"]/dt[@class="col-lg-8"]/ul/li')
    table={}
    for li in li_list:
        title=li.xpath('./h5/a/text()')[0]
        url=li.xpath('./h5/a/@href')[0]
        newUrl=oldUrl+url
        table[title]=newUrl
    #title=re.findall('<h5><a href=".*?">(.*?)</a>',content)
    #print(table)
    return table
#获取文件内容,保存文件
def parser_text(path,title,content):
    path=path+'\\'+title+'.docx'
    tree=etree.HTML(content)
    p=tree.xpath('//div[@class="shows_nr col-lg-12 col-12"]/p/text()')
    text=''
    doc = docx.Document()
    for str in p:
        doc.add_paragraph(str)
    #print(text)
    # 设置一个空白样式
    style = doc.styles['Normal']
    # 获取段落样式
    paragraph_format = style.paragraph_format
    # 首行缩进0.74厘米，即2个字符
    paragraph_format.first_line_indent = Cm(0.74)
    doc.save(path)
    return
#新建文件,保存内容
def saveFile(path,title,str):
    return
def main():
    path='E:\\石榴学习网'
    shuzu=['xiaoxue','chuzhong','gaozhong','danyuan','yingyu','huati','zhidao','sucai']
    for kaoshi in shuzu:
            for pageNum in range(1,3000):
                try:
                    url = 'https://www.46w.cn/zuowen/' + kaoshi + '/list_%d.html'
                    url = format(url%pageNum)
                    content=getText(url,'utf-8')
                    table = parser_title(content, 'https://www.46w.cn')
                    for key in table:
                        title=key
                        print(title)
                        newUrl=table[key]
                        content1=getText(newUrl,'utf-8')
                        parser_text(path,title,content1)
                except:
                    continue
if __name__ == '__main__':
    main()


