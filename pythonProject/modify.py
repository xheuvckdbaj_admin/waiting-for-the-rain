# -*- coding:utf-8 -*-
import re
from datetime import time

import docx
import os

from numba import jit

from FindFile import findAllFile, find_file
from blank import blank
from judgement import delBlankFile
from parameter import parameter
from win32com import client as wc
'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def  deleAdvertising(filePath,newFolderPath,midFolderPath,front,later):
    doc=filePath.split(".")[len(filePath.split("."))-1]
    #word = wc.Dispatch("Word.Application")
    if doc=='docx' :
        try:
            file=docx.Document(filePath)
            fileName = os.path.basename(filePath)
            if len(fileName)>=11:

                #print("段落数:" + str(len(file.paragraphs)))

                #print('删除前图形图像的数量：', len(file.inline_shapes))
                i = 0
                str=[]
                if len(file.paragraphs[0].text)>4:
                    file.paragraphs[0].text = file.paragraphs[0].text.replace('2016', '2021').replace('2017', '2021').replace(
                        '2018', '2021').replace('2019', '2021').replace('2020', '2021').replace('万唯中考', '').replace('2015', '2021').replace('2010', '2021').replace(
                        '2008', '2021').replace('2009', '2021').replace('2007', '2021').replace('2006', '').replace('2005', '2021').replace('2011', '2021').replace(
                        '2012', '2021').replace('2013', '2021').replace('2014', '2021')
                for para in file.paragraphs:
                   #print(para.text)
                   if '独家资料之' in para.text or '认真看完了' in para.text \
                           or '微信关注' in para.text or '让你涨分更多' in para.text \
                           or '更多初中数学资料' in para.text \
                           or '更多资料尽在' in para.text or '微信搜索' in para.text\
                           or '关注微信' in para.text or '资料下载' in para.text\
                           or '公众号' in para.text or '探究群' in para.text \
                           or '更多资料' in para.text or '全部下载' in para.text\
                           or '资料分享' in para.text or '小编' in para.text \
                           or 'time' in para.text or '收藏' in para.text or '编号' in para.text \
                           or '精彩文章' in para.text or '发布时间' in para.text \
                           or '收藏' in para.text or '标签' in para.text or '来源：' in para.text or \
                           '上一篇：' in para.text or '返回课文' in para.text or '资料' in para.text or 'QQ' in para.text:
                       str.append(i)
                   if '相关文章' in para.text or '题型：简答题' in para.text \
                           or '下载' in para.text or '推荐阅读' in para.text or '小编精心推荐' in para.text or '小编特别推荐' in para.text:
                        for par in range(i,len(file.paragraphs)):
                            str.append(par)
                   i += 1
                #'print(para.text)'
                for i in str:
                    paragraph = file.paragraphs[i]
                    paragraph.clear()

                #blank(file.paragraphs)  # 删除文件中的空白行
                #print('删除前图形图像的数量：', len(file.inline_shapes))   -2021学年第二学期教学工作总结
                fileName = fileName.replace('2016', '2021').replace('2017', '2021').replace(
                    '2018', '2021').replace('2019', '2021').replace('2020', '2021').replace('【微信公众号：wkgx985 免费获取】', '').replace('2015', '2021').replace('2010', '2021').replace(
                    '2008', '2021').replace('2009', '2021').replace('2007', '2021').replace('2006', '').replace('2005', '2021').replace('2011', '2021').replace(
                    '2012', '2021').replace('2013', '2021').replace('2014', '2021').replace('._', '').replace('★','').replace('#','').replace('_','')
                file_name = re.sub('【.*?】', '', fileName)
                if file.save(newFolderPath + '\\' + file_name)==None:
                    #newFile = docx.Document(newFolderPath + '\\' + fileName)

                    #delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                    os.remove(filePath)  # 删除原来修改前的docx文件
            else:
                file.save(midFolderPath + '\\' + fileName)
                os.remove(filePath)
        except:
            print('错误')

        #word.Quit()


def main():
    #os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'E:\除去眉页后的文件'
    newFolderPath = r'E:\上传'
    midFolderPath=r'E:\需要修改文件名的文件'
    filePaths = find_file(oldFolderPath)
    for filePath in filePaths:
        if not '$' in filePath:
            print(filePath)
            deleAdvertising(filePath, newFolderPath,midFolderPath,0, 0) # 修改，删除广告
if __name__ == '__main__':
    main()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
