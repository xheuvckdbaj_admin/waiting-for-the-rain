# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        time.sleep(2)
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser):
    getContent(thmlpath, strUrl)
    try:
        getBrowser(browser,strUrl)
        href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
        print(href)
        getHtml(thmlpath, href, number, browser)
    except:
        print('该模块已完毕')

def main(thmlpath):
            number=134000
            browser = webdriver.Chrome()
            strUrls=[
                'https://www.mipei.com/gongzuozongjie/202174.html',
                'https://www.mipei.com/huodongzongjie/202055.html',
                'https://www.mipei.com/nianzhongzongjie/202107.html',
                'https://www.mipei.com/shixizongjie/202180.html',
                'https://www.mipei.com/xuexizongjie/201638.html',
                'https://www.mipei.com/gongzuojihua/202171.html',
                'https://www.mipei.com/huodongjihua/202181.html',
                'https://www.mipei.com/xuexijihua/202203.html',
                'https://www.mipei.com/qingjiatiao/202082.html',
                'https://www.mipei.com/jietiao/202204.html',
                'https://www.mipei.com/zhengming/201918.html',
                'https://www.mipei.com/tongzhi/202175.html',
                'https://www.mipei.com/cizhixin/202083.html',
                'https://www.mipei.com/qiuzhixin/202173.html',
                'https://www.mipei.com/zijianxin/202193.html',
                'https://www.mipei.com/tuijianxin/201047.html',
                'https://www.mipei.com/guizhangzhidu/201979.html',
                'https://www.mipei.com/shijicailiao/202164.html',
                'https://www.mipei.com/yingjiyuan/202084.html',
                'https://www.mipei.com/xindetihui/202172.html',
                'https://www.mipei.com/dabianzhuang/200980.html',
                'https://www.mipei.com/biyedabian/201148.html',
                'https://www.mipei.com/kaitibaogao/185453.html'
            ]
            for i in range(0,len(strUrls)):
                strUrl=strUrls[i]
                getHtml(thmlpath, strUrl,number,browser)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
