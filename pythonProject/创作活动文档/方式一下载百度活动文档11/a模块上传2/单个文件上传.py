# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import win32com
import win32con
import win32gui
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt
from pywinauto import Desktop
from pywinauto.keyboard import *


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser


def panduanTitle(title):
    panduan = True
    if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title or '年级' in title \
            or '图' in title or '.' in title or '×' in title or (len(title) < 5):
        panduan = False
    return panduan


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getNumberPager(browser):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
        return pagerNumber
    except:
        return 0


def deleteWord(folderPath, title):
    filePath = folderPath + '\\' + title + '.docx'
    try:
        os.remove(filePath)
    except:
        print('无法删除' + folderPath)


def ctrlx(oldFilePath, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
    shutil.move(oldFilePath, newPath)


def getFileNameStr(folderPath):
    keyStrs = []
    filePaths = find_file(folderPath, [])
    if len(filePaths) >= 2:
        for filePath in filePaths:
            titleStr = os.path.basename(filePath).replace('.docx', '')
            keyStrs.append(titleStr)
    return keyStrs


def getTitleNumber(wordFolderPath):
    titleNumber = 1000000
    if wordFolderPath.endswith('推荐'):
        titleNumber = -1
    if wordFolderPath.endswith('学前教育'):
        titleNumber = 0
    if wordFolderPath.endswith('基础教育'):
        titleNumber = 1
    if wordFolderPath.endswith('高校与高等教育'):
        titleNumber = 2
    if wordFolderPath.endswith('资格考试'):
        titleNumber = 3
    if wordFolderPath.endswith('法律'):
        titleNumber = 4
    if wordFolderPath.endswith('建筑'):
        titleNumber = 5
    if wordFolderPath.endswith('互联网'):
        titleNumber = 6
    if wordFolderPath.endswith('行业资料'):
        titleNumber = 7
    if wordFolderPath.endswith('政务民生'):
        titleNumber = 8
    if wordFolderPath.endswith('商品说明书'):
        titleNumber = 9
    if wordFolderPath.endswith('实用模板'):
        titleNumber = 10
    if wordFolderPath.endswith('生活娱乐'):
        titleNumber = 11
    return titleNumber


def selectHeading(browser, wordFolderPath):
    time.sleep(1)
    pageNumber = getTitleNumber(wordFolderPath)
    if pageNumber >= 0:
        # pageNumber最小为0
        if pageNumber >= 9:
            xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
            browser.find_element(By.XPATH, xpathStrs).click()
            getNewWindow(browser, xpathStrs, 10)
            time.sleep(1)
        xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
        xpathStrScue = '//div[@class="privilege-list"]/div[@class="privilege-item-container action"]'
        browser.find_elements(By.XPATH, xpathStr)[pageNumber].click()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(1)
        titleText = browser.find_element(By.XPATH, xpathStrScue).text.strip()
        if not getTitleNumber(wordFolderPath) == pageNumber:
            titleEl = browser.find_elements(By.XPATH, xpathStr)[pageNumber]
            ActionChains(browser).click(titleEl).perform()
            getNewWindow(browser, xpathStr, 10)
            time.sleep(1)


def clickButton(ele):
    try:
        ele.click()
        time.sleep(0.1)
        ele.click()
        time.sleep(0.1)
        ele.click()
    except:
        print('')


def panduanLastPage(browser):
    panduan = True
    try:
        xpathStrButton = '//button[@class="btn-next"]'
        disabled = browser.find_element(By.XPATH, xpathStrButton).get_attribute('disabled')
        if disabled == 'true':
            panduan = False
        return panduan
    except:
        return panduan


def ClickNextPage(browser, wordFolderPath, pagerNumber, wordNumber):
    try:
        # pageNumber最大为1，最小为0
        xpathStr = '//button[@class="btn-next"]/i[@class="el-icon el-icon-arrow-right"]'
        if panduanLastPage(browser):
            browser.find_element(By.XPATH, xpathStr).click()
            getNewWindow(browser, xpathStr, 10)
            time.sleep(1)
        else:
            print('已经上传完毕')
            pagerNumber = 100
        return pagerNumber
    except:
        wordFolderPaths = os.listdir(wordFolderPath)
        if len(wordFolderPaths) < 5:
            print('点击下一页fast，重新开始上传')
            browser.get('https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
            getNewWindow(browser, xpathStr, 10)
            # getNewWindow(browser, '//div[@class="major-btns"]', 20)
            selectHeading(browser, wordFolderPath)
            pageXpathStr = '//div[@class="el-input el-pagination__editor is-in-pagination"]/input[@class="el-input__inner"]'
            browser.find_element(By.XPATH, pageXpathStr).clear()
            time.sleep(0.5)
            browser.find_element(By.XPATH, pageXpathStr).send_keys(str(pagerNumber))
            time.sleep(0.5)
            browser.find_element(By.XPATH, pageXpathStr).send_keys(Keys.ENTER)
            time.sleep(0.5)
            getNewWindow(browser, pageXpathStr, 10)
        return 0


def submitword(browser, wordFilePath):
    button = browser.find_element(By.XPATH,
                                  '//div[@class="doc-upload-btn-bar is-center"]/button[@class="el-button el-button--primary"]')
    ActionChains(browser).move_to_element(button).perform()
    ActionChains(browser).click(button).perform()
    print(wordFilePath, '上传成功')
    # filePathxs = find_file(folderPath, [])
    # print('已经成功上传' + str(dataNumber) + '个文档')
    # dataNumber1 = len(filePathxs)
    # print('此次成功上传' + str(dataNumber1) + '个文档')
    # together = dataNumber + dataNumber1
    # print('一共成功上传' + str(together) + '个文档')
    # longx = dataNumber1
    # for filePathx in filePathxs:
    try:
        os.remove(wordFilePath)
    except:
        print('删除不掉' + wordFilePath)

def deleteFile(wordFolderPath):
    filePaths=find_file(wordFolderPath)
    for filepath in filePaths:
        try:
            os.remove(filepath)
        except:
            print('删除失败',filepath)
def singlyUpload(browser, wordFolderPath, keyStrs, passedTitles, panduanUpload,panduanFilePath):
    xpathStr = '//div[@class="doc-row"]'
    xpathStrTitle = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    xpathStrTitleScue = '//div[@class="doc-row disabled-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    xpathStrButton = '//div[@class="doc-row"]/div[@class="upload-doc"]/div[@class="add-new-btn add-new-btn-right"]/button'
    xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button'
    try:
        getNewWindow(browser, '//div[@class="title-line"]/span[@class="doc-title"]', 10)
    except:
        panduanUpload = True
        print('error')
    elements = browser.find_elements(By.XPATH, xpathStrTitle)
    sueElements = browser.find_elements(By.XPATH, xpathStrTitleScue)
    for i, sueEle in enumerate(sueElements):
        scueTitle = sueEle.get_attribute('title')
        if scueTitle in keyStrs:
            try:
                scueTitleFilePath = os.path.join(wordFolderPath, scueTitle) + '.docx'
                os.remove(scueTitleFilePath)
                print('成功删除已上传文档', scueTitleFilePath)
            except:
                print('删除文档失败', scueTitleFilePath)
    for i, element in enumerate(elements):
        title = element.get_attribute('title')
        if title in keyStrs and (not title in passedTitles) and (not '~' in title):
            try:
                print(title)
                buttonEle = element.find_elements(By.XPATH, xpathStrButton)[i]
                ActionChains(browser).click(buttonEle).perform()
                time.sleep(0.5)
                wordFilePath = wordFolderPath + '\\' + title + '.docx'
                # pyperclip.copy(wordFilePath)
                # pyautogui.hotkey('ctrl', 'v')
                # pyautogui.press('enter', presses=2)
                app = Desktop()
                dialog = app['打开']
                time.sleep(0.5)
                # dialog.print_control_identifiers(depth=None, filename=None)
                dialog['Edit'].type_keys(wordFilePath, with_spaces=True)
                clickButton(dialog['Button'])
                passedTitles.append(title)
                panduanUpload = True
                time.sleep(3)
                getNewWindow(browser,
                             '//div[@class="doc-upload-btn-bar is-center"]/button[@class="el-button el-button--primary"]',
                             5)
                wordFilePath = wordFolderPath + '\\' + title + '.docx'
                submitword(browser, wordFilePath)
                break
            except:
                try:
                    fileTxt=open(panduanFilePath,'w',encoding='utf-8')
                    buttonSucess = element.find_elements(By.XPATH, xpathStrEle)[i]
                    print('账号已经传完')
                    fileTxt.write('账号已经传完')
                    fileTxt.close()
                    deleteFile(wordFolderPath)
                except:
                    print('文件匹配，但上传失败')
    return panduanUpload


def xunhuanPage(browser, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber, wordNumber,panduanFilePath):
    panduanUpload = singlyUpload(browser, wordFolderPath, keyStrs, passedTitles, panduanUpload,panduanFilePath)
    if panduanUpload == False:
        pagerNumber = pagerNumber + 1
        pagerNumber = ClickNextPage(browser, wordFolderPath, pagerNumber, wordNumber)
        if not pagerNumber == 100:
            pagerNumber = xunhuanPage(browser, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber,
                                      wordNumber,panduanFilePath)
        return pagerNumber
    else:
        return pagerNumber


def uploadFile(browser, wordFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber,panduanFilePath):
    panduanUpload = False
    browser.get('https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
    time.sleep(1)
    xpathStr = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    titleEl = browser.find_elements(By.XPATH, xpathStr)[0]
    ActionChains(browser).click(titleEl).perform()
    getNewWindow(browser, xpathStr, 10)
    # getNewWindow(browser, '//div[@class="major-btns"]', 20)
    selectHeading(browser, wordFolderPath)
    print(pagerNumber)
    if pagerNumber > 0:
        try:
            pageXpathStr = '//div[@class="el-input el-pagination__editor is-in-pagination"]/input[@class="el-input__inner"]'
            browser.find_element(By.XPATH, pageXpathStr).clear()
            time.sleep(0.3)
            browser.find_element(By.XPATH, pageXpathStr).send_keys(str(pagerNumber))
            time.sleep(0.5)
            browser.find_element(By.XPATH, pageXpathStr).send_keys(Keys.ENTER)
            getNewWindow(browser, pageXpathStr, 10)
        except:
            print('网页没刷新')
            pagerNumber = uploadFile(browser, wordFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber,panduanFilePath)

    pagerNumber = xunhuanPage(browser, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber, wordNumber,panduanFilePath)
    return pagerNumber


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.ma(filePath)


def mainTitleClassifyUpload(folderPath, cookiesPath,panduanFilePath):
    browser = getDriver('title1')
    url = 'https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask'
    addCookies(browser, url, cookiesPath)
    time.sleep(1)
    xpathStr = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    titleEl = browser.find_elements(By.XPATH, xpathStr)[0]
    ActionChains(browser).click(titleEl).perform()
    getNewWindow(browser, xpathStr, 10)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    titleFolderPaths = os.listdir(folderPath)
    dataNumber = 0
    for titleFolderPathName in titleFolderPaths:
        passedTitles = []
        pagerNumber = 0
        titleFolderPath = os.path.join(folderPath, titleFolderPathName)
        wordFolderPaths = os.listdir(titleFolderPath)
        wordNumber = 100
        if len(wordFolderPaths) < 100:
            wordNumber = len(wordFolderPaths)
        for i in range(0, wordNumber):
            # for wordFolderPathName in wordFolderPaths:
            #     print(dataNumber)
            keyStrs = getFileNameStr(titleFolderPath)
            if not keyStrs == None:
                pagerNumber = int(uploadFile(browser, titleFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber,panduanFilePath))
                if pagerNumber == 100:
                    break
                    break
        wordFolderPaths2 = os.listdir(titleFolderPath)
        uploadNumber = len(wordFolderPaths) - len(wordFolderPaths2)
        print('一共上传%d个文档' % uploadNumber)
        if len(wordFolderPaths) == 0:
            try:
                os.rmdir(titleFolderPath)
            except:
                print('无法删除' + titleFolderPath)


if __name__ == '__main__':
    folderPath = r'D:\文档\百度活动文档11\百度活动文档上传'
    panduanFilePath = r'D:\文档\百度活动文档11\用来分类文档标题\账号.txt'
    # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档11\百度活动文档上传\4高校与高等教育
    cookiesPath = './baiduCookies1.txt'
    # dataNumber = 0
    file=open(panduanFilePath,'w',encoding='utf-8')
    file.write('')
    file.close()
    mainTitleClassifyUpload(folderPath, cookiesPath,panduanFilePath)
