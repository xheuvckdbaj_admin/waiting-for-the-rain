# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.gaokao.com/zyk/gzja/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('div > h1')[0].text
    #print(title)
    list_soupa = str(soup.select('.content > .content_txt > p')[0])
    #latter_p=str(soup.select('.main_zhengw > p')[len(soup.select('.main_zhengw > p'))-1])
    #content=re.sub('<strong><a href=.*?</span></a></p>','',list_soupa)
    print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    li_list=soup.select('.wzList > .text_list1 > li')
    #print(url_list)
    for li in li_list:
                href = li.select('h2 > a')[0].get('href')
                title=li.select('h2 > a')[0].get('title')
                print(href)
                print(title)
                getContent(thmlpath,href,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    etr=etree.HTML(requests_text)
    #url_list=etr.xpath('//div[@class="w980"]/di')
    url_list1=soup.select('.w980 > div.b1.mar_10')
    #print(url_list)
    #print(url_list1)
    for u in url_list1:
            li_list=u.select('ul > li')
            for li in li_list:
                href=li.select('a')[0].get('href')
                #print(href)

                getUrlPageNum(thmlpath,href)

def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2000):
        try:
            if pageNum==1:
                oldUrl = url+'index.shtml'
                #print(oldUrl)
                getUrl(thmlpath, oldUrl)


            else:
                    oldUrl = url +'index_'+ str(pageNum) +'.shtml'
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)

        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['gyywja','gysxja','gyyyja','gywlja','gyhxja','gyswja','gydlja',
           'gylsja','gyzzja','geywja','gesxja','geyyja','gewlja',
           'gehxja','geswja','gedlja','gelsja','gezzja','gsywja','gssxja','gsyyja',
           'gswlja','gshxja','gsswja','gsdlja','gslsja','gszzja']
    for s in str:
        #string=s+'/map'
        url = groupUrl(s)
        print(url)
        getUrlPageNum(thmlpath,url)


if __name__ == '__main__':
    thmlpath = r'E:\高考网教案'
    main(thmlpath)
