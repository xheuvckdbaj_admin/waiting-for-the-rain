# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.fanwen9.com/gongzuozongjie/159439.html',
                'https://www.fanwen9.com/huodongzongjie/159287.html',
                'https://www.fanwen9.com/nianzhongzongjie/159420.html',
                'https://www.fanwen9.com/shixizongjie/159429.html',
                'https://www.fanwen9.com/xuexizongjie/158110.html',
                'https://www.fanwen9.com/peixunzongjie/158980.html',
                'https://www.fanwen9.com/gongzuobaogao/158755.html',
                'https://www.fanwen9.com/shijianbaogao/159233.html',
                'https://www.fanwen9.com/shixibaogao/159436.html',
                'https://www.fanwen9.com/lizhibaogao/157256.html',
                'https://www.fanwen9.com/cizhibaogao/159388.html',
                'https://www.fanwen9.com/shuzhibaogao/158979.html',
                'https://www.fanwen9.com/yanjianggao/159384.html',
                'https://www.fanwen9.com/fayangao/159368.html',
                'https://www.fanwen9.com/zhuchigao/159377.html',
                'https://www.fanwen9.com/jiayougao/159369.html',
                'https://www.fanwen9.com/tongxungao/159378.html',
                'https://www.fanwen9.com/jianghuagao/159387.html',
                'https://www.fanwen9.com/yaoqinghan/159373.html',
                'https://www.fanwen9.com/shenqingshu/159379.html',
                'https://www.fanwen9.com/baozhengshu/159381.html',
                'https://www.fanwen9.com/changyishu/159386.html',
                'https://www.fanwen9.com/chengnuoshu/159370.html',
                'https://www.fanwen9.com/jianyishu/159376.html',
                'https://www.fanwen9.com/gongzuojihua/159421.html',
                'https://www.fanwen9.com/gongzuocehua/151163.html',
                'https://www.fanwen9.com/gongzuofangan/159363.html',
                'https://www.fanwen9.com/hetong/159246.html',
                'https://www.fanwen9.com/jiaoxuejihua/159424.html',
                'https://www.fanwen9.com/jiaoxuefansi/159422.html',
                'https://www.fanwen9.com/jiaoxuexinde/159315.html',
                'https://www.fanwen9.com/shuokegao/159253.html',
                'https://www.fanwen9.com/zhiyeguihua/152333.html',
                'https://www.fanwen9.com/zhuanzhengshenqing/159438.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl+'\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath=r'E:\文档\自查报告网2\all.txt'
    # main(thmlpath)
    mainGetHtml(thmlpath, txtFilepath)
