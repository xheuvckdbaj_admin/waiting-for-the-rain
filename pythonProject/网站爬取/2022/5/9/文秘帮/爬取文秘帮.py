import re

391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):
    try:
        print(strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('div.art-text')[0]).replace('<br>', '<p><p/>').replace('<br/>',
                                                                                           '<p><p/>').replace(
            ' 　　', '<p><p/>').replace('　', '<p><p/>').replace(' ', '<p><p/>')
        title = soup.select('div.article > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.conlist-num > h5 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl = 'https://www.wenmi.com' + href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath, strUrl):
    for i in range(1,10):
        url=strUrl+'p%d.html'
        newUrl=url%i
        getListUrl(thmlpath,newUrl)


def getTitleUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('p > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='https://www.wenmi.com'+href
        getListUrl(thmlpath,newUrl)
def main(thmlpath):

        url='https://www.wenmi.com/article/'
        getTitleUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
