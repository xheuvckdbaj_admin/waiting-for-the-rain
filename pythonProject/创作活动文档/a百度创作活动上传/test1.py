import time

from playwright.sync_api import sync_playwright


def handle_dialog(popup):
    """监听后处理"""
    print(popup.message)
    # time.sleep(5)
    # popup.dismiss()


def run(playwright):
    chromium = playwright.chromium
    browser = chromium.launch(headless=False, slow_mo=3000)
    page = browser.new_page()
    page.on("dialog", handle_dialog)
    page.on('popup', handle_dialog)
    page.evaluate("alert('霍格沃兹测试开发学社')")
    browser.close()


with sync_playwright() as playwright:
    run(playwright)