import time

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}



def getUrlAndTitle(newUrl,driver,urls,keyStr):
    try:
        driver.get(newUrl)
        time.sleep(0.3)
        elements = driver.find_elements(By.XPATH,
            '//div[@class="title-box_e472a"]/a')
        for i, element in enumerate(elements):
            try:
                title = element.text.replace('- 百度文库', '').replace('_百度文库', '').replace('百度文库', '').strip()
                # if mainPanDuanTitle(keyStr, title):
                if True:
                    href = element.get_attribute('href')
                    driver.get(href)
                    time.sleep(0.2)
                    try:
                        title = driver.find_element(By.XPATH,
                                                    '//div[@class="doc-title normal-screen-mode normal-screen-mode"]').text
                    except:
                        title = keyStr
                    hrefUrl = href.split('.html')[0] + '?pcqq.c2c&bfetype=old'
                    lineStr = hrefUrl + '————' + title + '————' + keyStr
                    print(lineStr)
                    urls.append(lineStr)
                    break
            except:
                print('error')
                continue
    except:
        time.sleep(15)
        print('error')
def getPageUrl(key,url,driver,urls):
    keyStr=key.replace(' ','').replace('/','')
    for i in range(1,2):
        newUrl=url%(key,i)
        print(key,i)
        getUrlAndTitle(newUrl,driver,urls,keyStr)

def getKey(txtPath,url,driver,urls,startnumber,last):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    # path = filePath + '\\' + str(time.time()) + '.txt'
    # pathFile = open(path, 'w', encoding='utf-8')
    for i,line in enumerate(lines):
        if i >= startnumber and i < last:
            key=line.replace('\n','')
            print(str(i)+'百度文档-----------------',key)
            getPageUrl(key, url,driver,urls)
    # pathFile.close()
def mainUrl(txtPath,startnumber,last):
    # filePath=r'E:\文档\百度活动文档\百度获取文档\百度url'
    urls=[]
    driver = getDriver('baiduUrl')
    txtFilePath = txtPath+'\\all.txt'
    url = 'https://wenku.baidu.com/search?word=%s&searchType=0&lm=0&od=0&fr=search&ie=utf-8&pn=%d'
    getKey(txtFilePath, url,driver,urls,startnumber,last)
    driver.quit()
    return urls


if __name__ == '__main__':
    filePath = r'E:\文档\百度活动文档\百度获取文档\百度url'
    txtPath = r'E:\文档\百度活动文档\百度活动文档标题'
    startnumber = 0
    last = 100
    urls=mainUrl(txtPath,startnumber,last)
    path = filePath + '\\' + str(time.time()) + '.txt'
    pathFile = open(path, 'w', encoding='utf-8')
    for url in urls:
        pathFile.write(url+'\n')
