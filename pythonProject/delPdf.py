import os

from FindFile import find_file
def delPDF(folderPath):
    filePaths=find_file(folderPath)
    for path in filePaths:

        if path.split('.')[len(path.split('.'))-1]=='pdf' or path.split('.')[len(path.split('.'))-1]=='PDF':
            ss=path.replace('pdf', 'docx').replace('docx.docx','pdf.docx')
            if ss in filePaths:
                os.remove(path)
        if path.split('.')[len(path.split('.')) - 1] == 'doc'  or path.split('.')[len(path.split('.'))-1]=='DOC' :
            ss = path.replace('.doc', '.docx').replace('docx.docx','doc.docx')
            #sx = path.replace('DOC', 'docx')
            if ss in filePaths :
                os.remove(path)
def main():
    #os.remove('E:\\oc文件\\1 第1节\u3000圆周运动.doc')
    folderPath=r'E:\df'
    delPDF(folderPath)
if __name__ == '__main__':
    main()