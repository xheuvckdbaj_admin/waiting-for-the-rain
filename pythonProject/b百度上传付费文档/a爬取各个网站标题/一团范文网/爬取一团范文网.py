import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_yituanfanwenwang(url, file, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.head > h1')[0].text.replace('最新','').replace('通用','').replace('参考','')\
            .replace('精选','').replace('优秀','').replace('模板','').replace('优质','').replace('推荐','')
        print(title)
        file.write(title+'\n')
    except:
        print('error')


if __name__ == '__main__':
    # url = 'https://m.yituanyoupin.net/cehuashu/42767.html'
    # htmlPath = r'D:\文档\百度付费上传文档\百度html'
    # keyStr = 'kaixin'
    # title = ''
    # mian_yituanfanwenwang(url, htmlPath, keyStr, title)

    url = 'https://m.yituanyoupin.net/%s/%s.html'
    htmlPath = r'D:\文档\百度付费上传文档\需要的标题及其chatgpt\title\一团范文网.txt'
    keyStr = 'kaixin'
    title = ''
    file = open(htmlPath, 'w', encoding='utf-8')
    keys = ['yanjianggao', 'shenqingshu', 'xindetihui', 'cehuashu']
    for key in keys:
        for i in range(1, 53767):
            newUrl = url % (key, str(i))
            # print(newUrl)
            mian_yituanfanwenwang(newUrl, file, keyStr, title)
    file.close()
