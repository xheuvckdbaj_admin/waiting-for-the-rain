import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay


def getNewPage(page, url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    return page


def getTitleForquchong(playwright: Playwright, cookiesPath, accountPanduan):
    xpathStrButton = '//div[@class="doc-row"]/div[@class="upload-doc"]'
    xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button[@class="el-button no-agree btn el-button--default el-button--mini"]'
    brower = playwright.chromium.launch(headless=False)
    context = brower.new_context()
    page = context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    page.get_by_text("创作活动", exact=True).click()
    time.sleep(0.1)
    loc = page.locator(xpathStrButton)
    if loc.count() == 0:
        lanmuStrs = ['学前教育', '基础教育', '高校与高等教育', '语言/资格考试', '法律', '建筑',
                     '互联网', '行业资料', '政务民生', '商品说明书', '实用模板', '生活娱乐']
        for lanmuStr in lanmuStrs:
            page.get_by_text(lanmuStr).click()
            time.sleep(0.5)
            locxue = page.locator(xpathStrButton)
            print(lanmuStr, locxue.count())
            if locxue.count() > 0:
                locxueOver = page.locator(xpathStrEle)
                if locxueOver.count() > 0:
                    accountPanduan = True
                    print('账号已经上传完毕!!!!')
                break
    else:
        locOver = page.locator(xpathStrEle)
        if locOver.count() > 0:
            accountPanduan = True
            print('账号已经上传完毕!!!!!')
    return accountPanduan


def getPassTitle(cookiesPath, accountPanduan):
    with sync_playwright() as playwright:
        panduan = getTitleForquchong(playwright, cookiesPath, accountPanduan)
    return panduan


def panduanUploadOver(cookiesPath):
    accountPanduan = False
    return getPassTitle(cookiesPath, accountPanduan)


def main():
    # passTitleFolder = r'D:\文档\百度活动文档\百度上传过的标题'
    cookiesPath = r'./baiduCookies1.txt'
    print(panduanUploadOver(cookiesPath))


if __name__ == '__main__':
    main()
