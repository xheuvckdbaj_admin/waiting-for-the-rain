# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://baogao.oh100.com/a/fansibaogao/143517.html',
                'https://baogao.oh100.com/a/shixibaogao/154606.html',
                'https://baogao.oh100.com/gongzuobaogao/153705.html',
                'https://baogao.oh100.com/shijianbaogao/154920.html',
                'https://baogao.oh100.com/lizhibaogao/153797.html',
                'https://baogao.oh100.com/shuzhibaogao/153816.html',
                'https://baogao.oh100.com/diaochabaogao/153506.html',
                'https://baogao.oh100.com/zichabaogao/156639.html',
                'https://baogao.oh100.com/a/shixizongjie/155726.html',
                'https://baogao.oh100.com/gongzuozongjie/156559.html',
                'https://baogao.oh100.com/nianzhongzongjie/154651.html',
                'https://baogao.oh100.com/huodongzongjie/155456.html',
                'https://baogao.oh100.com/peixunzongjie/154458.html',
                'https://baogao.oh100.com/xuexizongjie/151956.html',
                'https://baogao.oh100.com/a/yanjianggao/154603.html',
                'https://baogao.oh100.com/fayangao/154584.html',
                'https://baogao.oh100.com/zhuchigao/153135.html',
                'https://baogao.oh100.com/jiayougao/154586.html',
                'https://baogao.oh100.com/guangbogao/154590.html',
                'https://baogao.oh100.com/zhuchici/153694.html',
                'https://baogao.oh100.com/jieshuoci/154035.html',
                'https://baogao.oh100.com/banjiangci/153943.html',
                'https://baogao.oh100.com/daoyouci/157218.html',
                'https://baogao.oh100.com/a/shideshifeng/153902.html',
                'https://baogao.oh100.com/jiaoxuesheji/154602.html',
                'https://baogao.oh100.com/jiaoxuejihua/154605.html',
                'https://baogao.oh100.com/jiaoxuezongjie/154925.html',
                'https://baogao.oh100.com/jiaoxuefansi/154585.html',
                'https://baogao.oh100.com/jiaoxuexinde/154581.html',
                'https://baogao.oh100.com/tingkebaogao/123633.html',
                'https://baogao.oh100.com/shuokegao/153880.html',
                'https://baogao.oh100.com/qiuzhixin/154604.html',
                'https://baogao.oh100.com/ganxiexin/154589.html',
                'https://baogao.oh100.com/xieyishu/157221.html',
                'https://baogao.oh100.com/shenqingshu/157223.html',
                'https://baogao.oh100.com/jiantaoshu/157224.html',
                'https://baogao.oh100.com/baozhengshu/157225.html',
                'https://baogao.oh100.com/dushubiji/157219.html',
                'https://baogao.oh100.com/gongzuofangan/156984.html',
                'https://baogao.oh100.com/xindetihui/157207.html',
                'https://baogao.oh100.com/hetong/156920.html',
                'https://baogao.oh100.com/jianli/150320.html',
                'https://baogao.oh100.com/zhufuyu/157043.html',
                'https://baogao.oh100.com/zuoyouming/151425.html',
                'https://baogao.oh100.com/wenhouyu/156310.html',
                'https://baogao.oh100.com/jiyu/157110.html',
                'https://baogao.oh100.com/yulu/157097.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
