import os
import time

from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.业百科.爬取业百科 import mian_yebaike
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.东方文库.爬取东方文库 import mian_dongfangwenku
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.个人简历.爬取个人简历 import mian_gerenjianli
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.互信范文网.爬取互信范文网 import mian_huxinfanwen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.五六范文网.爬取五六范文网 import mian_wuliu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.优文网.爬取优文网 import mian_unjs
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.作文吧.爬取作文吧 import mian_zuowen8
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.写写帮文库.爬取写写帮文库 import mian_xiexiebang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.创闻头条.爬取创闻头条 import mian_chuangwen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.同花顺财经.爬取同花顺财经 import mian_10jqka
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.复来作文.爬取复来作文 import mian_fulaizuowen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.大文斗范文网.爬取大文斗范文网 import miandawendou
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.学习啦.爬取学习啦 import mian_xuexila
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.幼儿园学习网.爬取幼儿园学习网 import mian_youer
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.应届生毕业网.爬取应届生毕业网 import mian_yinjiesheng
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.微信公众平台.爬取微信公众平台 import mian_weixin
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.找总结网.爬取找总结网 import mian_zhaozongjiewang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.搜狐网.爬取搜狐网 import mian_sohu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.文书帮.爬取文书帮 import mian_wenshubang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.查字典.爬取查字典 import mian_chazidian
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.泓景文档网.爬取泓景文档网 import mian_hongjingwendang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.满分作文网.爬取满分作文网 import mian_manfenzuowen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.澎湃新闻.爬取澎湃新闻 import mian_pengpai
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.点点范文网.爬取点点范文网 import mian_diandian
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.爱杨教育网.爬取爱杨教育网 import mian_aiyangjiaoyuwang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.瑞文网.爬取瑞文网 import mian_ruiwen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.百家号.爬取百家号 import mian_baijiahao
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.百度文库.vip爬取百度文库 import mainBaiDuVip

from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.百度知道.爬取百度知道 import mian_baiduzhidao
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.百文网.爬取百文网 import mian_oh100
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.看点句子.爬取看点句子 import mian_kandianjuzi
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.知乎.爬取知乎 import mian_zhihu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.短美文.爬取短美文 import mian_duanmeiwen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.童鞋会.爬取童鞋会 import mian_tongxiehui
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.第一范文网.爬取第一范文网 import mian_diyifanwen
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.简书.爬取简书 import mian_jianshu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.经典语录.爬取经典语录 import mian_jingdianyulu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.网易.爬取网易 import mian_163
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.网易新闻.爬取网易新闻 import mian_news
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.美篇.爬取美篇 import mian_meipian
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.群走网.爬取群走网 import mian_qunzou
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.聚优网.爬取聚优网 import mian_jy135
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.腾讯新闻.爬取腾讯新闻 import mian_rainNew
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.腾讯网.爬取腾讯网 import mian_rain
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.范文先生网.爬取范文先生网 import mian_fwsir
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.范文社.爬取范文社 import mian_fanwenshe
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.语录网.爬取语录网 import mian_yulu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.语文迷.爬取语文迷 import mian_yuwenmi
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.读后感.爬取读后感 import mian_duhougan
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.课堂作文网.爬取课堂作文网 import mian_kt250
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.贤学网.爬取贤学网 import mian_xianxuewang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.趣祝福.爬取趣祝福 import mian_quzhufu
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.问一问.爬取问一问 import mian_question
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.风知文章.爬取风知文章 import mian_fengzhiwenzhang
from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.高三网.爬取高三网 import mian_senior
from pythonProject.创作活动文档.方式一下载百度活动文档.各个网站选择爬取.爬取各个手机网站 import chociesWebsite
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)

def getUrl(driver,element):
    element.click()
    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    driver.switch_to.window(n[-1])
    time.sleep(0.5)
    try:
        element = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, '//div'))
        )
    except:
        time.sleep(0.1)
    wenKuUrl = driver.current_url
    return wenKuUrl


def mian_5068jiaoxueziyuan(hrefUrl, htmlPath, keyStr, title):
    pass


def kuakeWebsiteJude(element, folderPath, driver, keyStr, urls):
    try:
        hrefUrl = getUrl(driver,element)
        panduan=chociesWebsite( folderPath, driver, keyStr, urls, hrefUrl)
        return panduan
    except:
        print('error')
        return False


def main():
    index_content = ''
    folderPath = r''
    driver = getDriver('san')
    keyStr = ''
    kuakeWebsiteJude(index_content, folderPath, driver, keyStr)


if __name__ == '__main__':
    main()
