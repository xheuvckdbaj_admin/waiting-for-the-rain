import os
import time

from pythonProject.FindFile import find_file

def panduanTitle(folderpath,title):
    panduan=True
    file_line = open(folderpath, 'r', encoding="utf-8")
    readlines=file_line.readlines()
    for line in readlines:
        keyStr=line.replace('\n','')
        if keyStr in title:
            panduan=False
            break
    return panduan
def mergeGuolv(folderpath, path):
    # txtpaths=find_file(folderpath)
    newTxtPath = path
    paths = []
    try:
        file_line = open(newTxtPath, 'r', encoding="utf-8")
        readlines = file_line.readlines()
        for line in readlines:
            if not line in paths:
                if panduanTitle(folderpath,line) :
                    paths.append(line)
        file_line.close()
    except:
        try:
            file_line = open(newTxtPath, 'r', encoding="gbk")
            readlines = file_line.readlines()
            for line in readlines:
                if not line in paths:
                    if panduanTitle(folderpath, line):
                        paths.append(line.replace('2020','2023').replace('2021','2023').replace('2022','2023'))
            file_line.close()
        except:
            print('gbk读不出')
    time.sleep(2)
    txtfile_line = open(newTxtPath, 'w', encoding='utf-8')
    for ph in paths:
        try:
            txtfile_line.write(ph)
        except:
            print('error')
            continue
    txtfile_line.close()


def main():
    keyPath = r'./titleKey.txt'
    path = r'./标题.txt'
    mergeGuolv(keyPath, path)


if __name__ == '__main__':
    main()
