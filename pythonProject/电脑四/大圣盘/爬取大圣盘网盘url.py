# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLingcookies
import addProxyid
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"C:\Users\Administrator\PycharmProjects\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\大圣盘\cookies1.txt"
def getNewWindow(browser,xpathStr,times):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    z = browser.current_window_handle  # 当前页面的句柄
    browser.switch_to_window(z)  # 移动句柄
    time.sleep(1)
    element = WebDriverWait(browser, times).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser

def huoquUrl(browser,eles,file_handle):
    for i in range(0,len(eles)):
        try:
            newbrowser = getNewWindow(browser, '//div[@class="result-inner"]', 100)
            eles[i].click()
            newbrowser=getNewWindow(newbrowser,'//div[@class="button-wrap"]',100)
            try:
                text = newbrowser.find_element_by_css_selector('div.resource-meta > span.meta-item.copy-item').text
            except:
                text = ''
            submit = ''
            if not text == '':
                submit = text.replace('提取密码', '').replace('点击复制', '').strip()
                print(submit)
            time.sleep(1)
            newbrowser.find_element_by_css_selector('div.button-wrap > a.button').click()
            try:
                newbrowser = getNewWindow(newbrowser, '//div[@class="content"]/div[@class="resource-meta"]/div[@class="button"]/a', 10)
            except:
                try:
                    print('网页反应过慢')
                    time.sleep(5)
                    newbrowser.find_element_by_css_selector('div.button-wrap > a.button').click()
                    newbrowser = getNewWindow(newbrowser,'//div[@class="content"]/div[@class="resource-meta"]/div[@class="button"]/a', 10)
                except:
                    newbrowser.close()
                    print('获取网盘网址失败！！！')
                    continue
            hf = newbrowser.find_element_by_css_selector('div.content > div.resource-meta > div.button > a').get_attribute(
                'href')

            print(hf)
            strUrl = hf + '*' + submit
            file_handle.write(strUrl + '\n')
            newbrowser.close()
        except:
            print('没有文件')
            continue

def getLinUrl(browser,law,pageLittle,url,key,file_handle):
        error=law
    # try:
        # browser = getBrowser(r'https://www.lingfengyun.com/')
        # 添加cookies
        formats = ['3', '2', '1', '0']
        ways = [
            '6', '0', '4'
        ]

        map = []
        for formatNum in range(0, 4):
            for wayNum in range(0, 3):
                st = formats[formatNum] + '-' + ways[wayNum]
                map.append(st)
        for i in range(law-1, 12):
            print('类型：',i)
            string = map[i]
            ft = string.split('-')[0]
            way = string.split('-')[1]
        # browser.get('https://www.dashengpan.com/#/main/search?keyword=fefe&page=2')
        # addLingcookies.addCookies(browser, 'https://www.dashengpan.com/#/main/search?keyword=fefe&page=2', filePath)
            newUrl = url % (key,1,ft,way)
            if not i==law-1 :
                browser = getNewWindow(browser, '//div[@class="result-inner"]', 10)
                browser.get(newUrl)
                time.sleep(1)
            else:
                browser.get(newUrl)
                time.sleep(30)
            try:
                elements=browser.find_elements_by_xpath('//div[@class="pager-wrap"]/div[@class="pc-pager-wrap"]/a')
                number=int(elements[len(elements)-2].text)
                print(number)
            except:
                getLinUrl(browser,i+1, pageLittle, url, key, file_handle)
            for page in range(pageLittle,number+1):
                print('类型：', i)
                print('页码：',page)
                newUrl = url % (key, page, ft, way)
                if not page == pageLittle:
                    browser = getNewWindow(browser, '//div[@class="result-inner"]', 10)
                    browser.get(newUrl)
                else:
                    browser.get(newUrl)
                try:
                    element = WebDriverWait(browser, 10).until(
                        EC.presence_of_element_located((By.XPATH, '//div[@class="result-inner"]'))
                    )
                except:
                    browser.find_elements_by_xpath('//div[@class="clear-button"]').click()
                    getLinUrl(browser,i+1, page, url, key, file_handle)
                eles=browser.find_elements_by_xpath('//div[@class="result-inner"]/div[@class="result-wrap"]/div[@class="resource-item-wrap valid"]/'
                                                    'div[@class="resource-item"]/div[@class="resource-info"]/h1[@class="resource-title"]/a')
                print(len(eles))
                huoquUrl(browser, eles, file_handle)
            pageLittle=1

    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,pageLittle,key,path):
    url='https://www.dashengpan.com/#/main/search?keyword=%s&page=%d&size=%s&type=%s'
    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')
    browser = webdriver.Chrome()
    getLinUrl(browser,law,pageLittle, url, key,  file_handle)
    file_handle.close()
def main():
            key='放假通知'
            path=r'D:\文档\url\大圣盘'
            law=1
            pageLittle=1
            login(law,pageLittle,key,path)
if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
