# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        #url='http://m.pincai.com/article/2300000.htm'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa1 = str(soup.select('.article-content')[0])
        list_soupa2 = str(soup.select('#info')[0])
        #print(list_soupa)
        #title=soup.select('.xq3-2 > h1 > a')[0].text
        list_soupa=list_soupa1+list_soupa2
        print(title)
        print(url)
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#AListBox > ul > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.get('title')
            newUrl='https:'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.excerpt.excerpt-one > header > h2 > a')
    for a in a_list:
        # time.sleep(10)
        href = a.get('href')
        title=a.get('title')
        getContent(thmlpath, href,title)


def getHtml2(thmlpath, strUrl):
    getHtml3(thmlpath, strUrl)
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.pagelist > a')
    string=a_list[len(a_list)-1].get('href')
    number=int(re.match('(.*?)index_(.*?).html',string).group(2))
    newUrl = strUrl + 'index_%d.html'
    for i in range(2, number+1):
        url = newUrl % i
        getHtml3(thmlpath, url)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.nav > li > a')
    for i in range(0,len(a_list)):
        a=a_list[i]
        href = a.get('href')
        getHtml2(thmlpath, href)

def main(thmlpath):
            #number=1
            strUrl='http://www.qiquha.com/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档\制度大全'
    main(thmlpath)
