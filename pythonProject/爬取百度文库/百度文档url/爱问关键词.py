import re

391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.41 Safari/537.36'}


def getHtml(thmlpath, strUrl):
    try:
        print(strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('div.art-text')[0]).replace('<br>', '<p><p/>').replace('<br/>',
                                                                                           '<p><p/>').replace(
            ' 　　', '<p><p/>').replace('　', '<p><p/>').replace(' ', '<p><p/>')
        title = soup.select('div.article > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

def getListUrl(file,strUrl,driver):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        span_list = soup.select('div.fl.file-txt > h4.data-name.cf > a > span.desc.fl')
        for i,span in enumerate(span_list):
            title=span.text.replace('.docx','').replace('.doc','').replace('.pdf','').replace('.ppt','').replace(' ','').replace('\n','')
            print(title)
            file.write(title+'\n')
    except:
        print('error')
def getPageUrl(file, strUrl,driver,str_list):
    for i,keyStr in enumerate(str_list):
        hrefUrl=strUrl+keyStr
        for i in range(1,41):
            newUrl=hrefUrl%i
            getListUrl(file, newUrl,driver)


def getTitleUrl(txtPath,thmlpath, strUrl,driver):
    file=open(thmlpath,'r',encoding='utf-8')
    lines=file.readlines()
    str_list=['_all_%d_/','_all_%d_downNum/','_all_%d_createtime/']
    for i, line in enumerate(lines):
            url=line.replace('\n','')
            path = txtPath + '\\hello' + str(i) + '.txt'
            file = open(path, 'w', encoding='utf-8')
            getPageUrl(file, url,driver,str_list)
            file.close()


def getTitle2(thmlpath, url,driver):
    file=open(thmlpath,'w',encoding='utf-8')
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    dl = soup.select('div.mods > dl.fareas')[0]
    list_a=dl.select('dd > a')
    for i,a in enumerate(list_a):
        if i > 0:
            try:
                href=a.get('href')
                requests_text = str(requests.get(url=href, headers=headers).content.decode(encoding='utf-8'))
                soup = BeautifulSoup(requests_text, 'lxml')
                dl1 = soup.select('div.mods > dl.fareas')[1]
                list_a1 = dl1.select('dd > a')
                for i1, a1 in enumerate(list_a1):
                    if i1 > 0:
                        href1 = a1.get('href')
                        print(href1)
                        file.write(href1+'\n')
            except:
                continue
    file.close()

def main(thmlpath,txtPath):
        driver=webdriver.Chrome()
        url='https://www.jinchutou.com/c-00011.html'
        getTitleUrl(txtPath,thmlpath, url,driver)
        # getTitle2(thmlpath, url, driver)


if __name__ == '__main__':
    thmlpath = r'./url.txt'
    txtPath=r'C:\Users\Administrator\Desktop\关键字'
    main(thmlpath,txtPath)
