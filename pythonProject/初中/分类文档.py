# -*- coding:utf-8 -*-
import os

from pythonProject.文件剪切 import ctrlx

'''
把文件夹内部的东西提出来
'''
def shear_dile(src,newPathDoc,newPathPdfDocx,newPathPdf,newPathPpt,newPathZip):
    if os.path.isdir(src):
        if not os.listdir(src):
            try:
                os.rmdir(src)
                print(u'移除空目录: ' + src)
            except:
                print('error')
        else:
            for d in os.listdir(src):
                shear_dile(os.path.join(src, d),newPathDoc,newPathPdfDocx,newPathPdf,newPathPpt,newPathZip)
    if os.path.isfile(src):
        if '~$' in src:
            try:
                os.remove(src)
            except:
                print('error')
        print(u"文件剪切:", src)
        fn = os.path.basename(src)
        # if not os.path.exists(dst + './' + fn) and not '~$' in src:
        try:
                if fn.endswith('.doc') or fn.endswith('.DOC'):
                    print(fn + '需要')
                    ctrlx(src, newPathDoc)
                elif fn.endswith('.docx') or fn.endswith('.DOCX'):
                    print(fn + '需要')
                    ctrlx(src, newPathPdfDocx)
                elif fn.endswith('.pdf') or fn.endswith('.PDF'):
                    print(fn + '需要')
                    ctrlx(src, newPathPdf)
                elif fn.endswith('.pptx') or fn.endswith('.PPTX') or fn.endswith('.pps') \
                        or fn.endswith('.ppt') or fn.endswith('.PPT'):
                    print(fn + '需要')
                    ctrlx(src, newPathPpt)
                elif  fn.endswith('.wps') or fn.endswith('.txt'):
                    print(fn + '需要')
                    ctrlx(src, newPathDoc)
                elif fn.endswith('.zip') or fn.endswith('.rar') or fn.endswith('.gz') \
                        or fn.endswith('.cab'):
                    print(fn + '需要')
                    ctrlx(src, newPathZip)
                elif 'baidu' in fn:
                    print(fn + '正在下载')

                elif os.path.isfile(src) and (
                        (not 'baidu' in fn) and not fn.endswith('.docx') and not fn.endswith(
                        '.DOCX') and not fn.endswith('.doc') \
                        and not fn.endswith('.DOC') and not fn.endswith(
                    '.PPT') and not fn.endswith('.pptx') \
                        and not fn.endswith('.PPTX') and not fn.endswith(
                    '.pdf') and not fn.endswith('.PDF') \
                        and not fn.endswith('.ppt') \
                        and not fn.endswith('.wps') and not fn.endswith(
                    '.rar') and not fn.endswith('.RAR') \
                        and not fn.endswith('.zip') and not fn.endswith('.ZIP') \
                        and not fn.endswith('.tar') and not fn.endswith('.gz') \
                        and not fn.endswith('.7Z') and not fn.endswith(
                    '.cab') and not fn.endswith('.txt')):
                    print(fn + '删除')
                    os.remove(src)
        except:
                print('错误')
                # continue
def main():
    src=r'D:\文档\zip3'
    newPathDoc = r'D:\文档\oc'
    newPathPdfDocx = r'D:\文档\ocx'
    newPathPdf = r'D:\文档\df'
    newPathPpt = r'D:\文档\ppt'
    newPathZip = r'D:\文档\zip'
    shear_dile(src,newPathDoc,newPathPdfDocx,newPathPdf,newPathPpt,newPathZip)
if __name__ == '__main__':
    main()