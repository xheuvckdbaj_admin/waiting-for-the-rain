# -*- coding:utf-8 -*-
import re
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content.indent2en')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>')
            list_soupa.split('微信公众号：')[0]
            title = soup.select('div.viewbox > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace('\r', '').replace('\n', '').replace(' ', '')
            print(title)
            # list_soupa1 = re.sub('<a(.*?)</a>', '', list_soupa)
            # list_soupa2 = re.sub(title, '', list_soupa1)
            # content = re.sub('下载Word文档到电脑，方便收藏和打印', '', list_soupa2).replace('编辑推荐：','').replace('下载Word文档','').replace('TAG标签：','')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.listbox > ul.listimg > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://fw.qulaoshi.com'+href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,strUrl):
    getListUrl(thmlpath, strUrl)
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.pagesli > a')[-1].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    url=href.replace(href.split('_')[-1].replace('.html',''),'%d')
    hrefUrl='http://fw.qulaoshi.com'+url
    for i in range(2,pageNumber+1):
        newUrl=hrefUrl%i
        print(newUrl)
        getListUrl(thmlpath, newUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.main > ul > li > a')
    for i,a in enumerate(a_list):
        if i > 0:
            href=a.get('href')
            newUrl='http://fw.qulaoshi.com'+href
            getPageUrl(thmlpath, newUrl)
def main(thmlpath):
    url='http://fw.qulaoshi.com/'
    getTitleUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\爬取网站4.20\区老师范文网'
    main(thmlpath)
