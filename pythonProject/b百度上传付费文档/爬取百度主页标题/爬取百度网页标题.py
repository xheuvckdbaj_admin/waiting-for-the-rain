import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright
def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
def clickPage(page):
    time.sleep(5)
    page.get_by_text("下一页").click()
def getPageTitles(page, titlePath):
    file=open(titlePath,'w',encoding='utf-8')
    time.sleep(1)
    elements=page.locator('//div[@class="doc-item"]/div[@class="title"]')
    eleNumbers=elements.count()
    print(eleNumbers)
    for i in range(0,eleNumbers):
        title=elements.nth(i).text_content().strip()
        print(title)
        file.write(title+'\n')
    file.close()

def getTitles(page, titlefolder,startNumber,endNumber):
    newTitlefolder=titlefolder+'\\'+str(startNumber)
    makeFolder(newTitlefolder)
    page.locator('//div[@class="price-range"]/div[@class="price-start"]/input[@type="number"]').nth(0)
    page.get_by_role("spinbutton").first.click()
    page.get_by_role("spinbutton").first.fill(str(startNumber))

    time.sleep(0.2)
    page.get_by_role("spinbutton").nth(1).click()
    page.get_by_role("spinbutton").nth(1).fill(str(endNumber))

    time.sleep(0.2)
    page.get_by_text("确认").nth(1).click()
    time.sleep(0.2)
    titlePath=newTitlefolder+'\\'+'1.txt'
    getPageTitles(page, titlePath)
    for i in range(2,21):
        clickPage(page)
        titlePath = newTitlefolder + '\\' +str(i)+ '.txt'
        getPageTitles(page, titlePath)

    time.sleep(100)
def run(playwright: Playwright,titlefolder,urlFolder):
    brower = playwright.chromium.launch(headless=True)
    context = brower.new_context()
    page = context.new_page()
    page.set_default_timeout(6000)
    file=open(urlFolder,'r',encoding='utf-8')
    file_lines=file.readlines()
    newTitleFolder=titlefolder+'\\'+'页面标题'
    makeFolder(newTitleFolder)
    for i,line in enumerate(file_lines):
        if i > -1:
            try:
                url=line.replace('\n','')
                page.goto(url)
                time.sleep(2)
                titleContent=page.locator('//div[@class="doc-title-text"]').text_content()
                titleContent=titleContent.replace('\n','').strip()
                titlePath=newTitleFolder+'\\'+titleContent+'.txt'
                fileTitle=open(titlePath,'w',encoding='utf-8')
                elements=page.locator('//div[@class="catalog-main-right"]')
                numbers=elements.count()
                for i in range(0,numbers):
                    title=elements.nth(i).get_attribute('title').replace('\n','').strip()
                    print(title)
                    fileTitle.write(title + '\n')
                fileTitle.close()
            except:
                print('error')
                time.sleep(5)
                page.close()
                context.close()
                brower.close()
                brower = playwright.chromium.launch(headless=True)
                context = brower.new_context()
                page = context.new_page()
                page.set_default_timeout(6000)
                continue

def yuanchuangli(titlefolder,urlFolder):
    with sync_playwright() as playwright:
        run(playwright, titlefolder,urlFolder)


def main():
    urlFolder=r'D:\文档\百度付费上传文档\标题\url\all.txt'
    titlefolder = r'D:\文档\百度付费上传文档\标题\title'
    yuanchuangli(titlefolder,urlFolder)
if __name__ == '__main__':
    main()
