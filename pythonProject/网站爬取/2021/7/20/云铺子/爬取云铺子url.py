# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"D:\pycharm\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\来搜一下\cookies.txt"

def getElement(browser,hrefs,file_handle):
    newHrefs=[]
    for e in range(0, len(hrefs)):
        try:
            browser.get(hrefs[e])
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="file-actions"]'))
            )

            newhref=browser.find_element_by_css_selector('div.file-actions > a.file-action.goto').get_attribute('href')
            newHrefs.append(newhref)
        except:
            print('资源不存在')
            continue

    for href in newHrefs:
        browser.get(href)
        # thml=browser.find_element_by_xpath("//*").get_attribute("outerHTML")
        # print(thml)
        try:
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//a[@class="progress-button in-progress"]'))
            )
        except:
            continue
        newUrl=browser.find_element_by_xpath('//a[@class="progress-button in-progress"]').get_attribute("href")
        print(newUrl)
        if not 'html' in newUrl:
            file_handle.write(newUrl + '\n')



def getLinUrl(law,url,key,file_handle):
        error=law
    # try:
        # browser = getBrowser(r'https://www.lingfengyun.com/')
        # 添加cookies
        browser = webdriver.Chrome()
        browser.get(url)
        browser.find_element_by_xpath('//input[@class="search-bar-form-input"]').send_keys(key)
        browser.find_element_by_xpath('//input[@class="search-bar-form-input"]').send_keys(Keys.ENTER)
        # browser.find_element_by_xpath('//span[@class="search-bar-form-search"]').click()
        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        print('当前句柄: ', n)  # 会打印所有的句柄
        # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
        browser.switch_to.window(n[-1])
        time.sleep(2)
        element = WebDriverWait(browser, 100).until(
            EC.presence_of_element_located((By.XPATH, '//a[@class="ypzdn"]'))
        )
        url=browser.current_url
        eles=browser.find_elements_by_xpath('//div[@class="paging"]/a')
        text=eles[len(eles)-2].text
        allPage=int(text)

        for page in range(law,allPage+1):
            newUrl=url+'?paging='+str(page)
            error = page
            # newUrl = url % (key, page,way)
            browser.get(newUrl)
            try:
                element = WebDriverWait(browser, 100).until(
                    EC.presence_of_element_located((By.XPATH, '//div[@class="result result-document"]'))
                )
            except:
                continue
            eles=browser.find_elements_by_xpath('//div[@class="result result-document"]/h3/a')
            hrefs=[]
            for ele in eles:
                href=ele.get_attribute('href')
                hrefs.append(href)
            print(hrefs)
            getElement(browser, hrefs, file_handle)
    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,key,path):
    url='http://www.yunpz.net/wangpan.html'
    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')
    getLinUrl(law, url, key, file_handle)
    file_handle.close()


def main():
    # keys=['习题答案','辅导讲义','课程标准','政府工作']
    # for key in keys:
            key='中考'
            print(key)
            path=r'E:\文档\云铺子url'
            law=1
            login(law,key,path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
