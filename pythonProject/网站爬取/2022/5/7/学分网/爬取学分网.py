391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.con_article.f16.c6')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>').replace(' 　　','<p><p/>').replace('　','<p><p/>').replace('( 励志天下 www.lizhi123.net )','')
            title = soup.select('div.ar_con_title.louti > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')

def main(thmlpath):
    url='http://www.xuefen.com.cn/zuowen/%d.html'
    for i in range(1,24409):
        newUrl=url%i
        getHtml(thmlpath, newUrl)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
