# -*- coding:utf-8 -*-
import re
from datetime import time

import docx
import os

from numba import jit

from FindFile import findAllFile, find_file
from blank import blank
from judgement import delBlankFile
from parameter import parameter
from win32com import client as wc
'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''
def  deleAdvertising(filePath,newFolderPath,midFolderPath,front,later):
    doc=filePath.split(".")[len(filePath.split("."))-1]
    #word = wc.Dispatch("Word.Application")
    if doc=='docx' :
        try:
                file=docx.Document(filePath)
                fileName = os.path.basename(filePath)


                #print("段落数:" + str(len(file.paragraphs)))

                #print('删除前图形图像的数量：', len(file.inline_shapes))
                i = 0
                str=[]
                if len(file.paragraphs[0].text)>4:
                    file.paragraphs[0].text = file.paragraphs[0].text.replace('2016', '2021').replace('2017', '2021').replace(
                        '2018', '2021').replace('2019', '2021').replace('2020', '2021').replace('万唯中考', '').replace('2015', '2021').replace('2010', '2021').replace(
                        '2008', '2021').replace('2009', '2021').replace('2007', '2021').replace('2006', '').replace('2005', '2021').replace('2011', '2021').replace(
                        '2012', '2021').replace('2013', '2021').replace('2014', '2021')
                file_name=file.paragraphs[0].text.replace('\t','').replace(' ','')
                if '绝密' in file_name or '附件' in file_name or '秘密★' in file_name:
                    file_name=file.paragraphs[1].text.replace('\t','').replace(' ','')

                if file.save(newFolderPath + '\\' + file_name +'.docx')==None:
                    #newFile = docx.Document(newFolderPath + '\\' + fileName)

                    #delBlankFile(newFolderPath + '\\' + fileName)  # 删除修改后的空白文件
                    os.remove(filePath)  # 删除原来修改前的docx文件

        except:
            print('错误')

        #word.Quit()


def main():
    #os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'E:\文档\原创力文档'
    newFolderPath = r'E:\文档\原创力文档'
    midFolderPath=r'E:\初中群文件\修改好的文件'
    filePaths = find_file(oldFolderPath)
    for filePath in filePaths:
        if not '$' in filePath:
            print(filePath)
            deleAdvertising(filePath, newFolderPath,midFolderPath,0, 0) # 修改，删除广告
if __name__ == '__main__':
    main()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
