import time




from pywinauto.application import Application
from pywinauto.backend import registry
from pywinauto.findwindows import find_elements
from pywinauto.keyboard import send_keys
import pyautogui

from 百度文库下载器自动化操作.通过url获取标题 import getTitle

def modifyTitle(title,titleStrs,i):
    newTitle = title + str(i)
    if newTitle in titleStrs:
        i+=1
        newTitle=modifyTitle(title, titleStrs, i)
    return newTitle
def automatic(txtPath,timeStr,requestTime,codeStr):
    file=open(txtPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    titleStrs=[]
    for i,line in enumerate(file_lines):
        try:
            print('现在正在下载第%s行'%str(i))
            url=line.replace('\n','')
            # title = line.replace('\n', '').split('******')[-1]
            app = Application(backend="uia").start(r"D:\python\1010----百度下载法\wenku.exe")
            title = getTitle(url)
            titleStrs.append(title)
            if title in titleStrs:
                title=modifyTitle(title,titleStrs,1)
            if i==0:
                time.sleep(3)
            dlg = app.window(title_re='乐源')
            eles = find_elements(auto_id='in_url', parent=dlg.element_info, top_level_only=False)
            ele = registry.backends['uia'].generic_wrapper_class(eles[0])
            ele.set_text('haha')
            time.sleep(0.1)
            ele.set_text(url)
            time.sleep(0.1)
            eleButtons = find_elements(auto_id='but_serch', parent=dlg.element_info, top_level_only=False)
            registry.backends['uia'].generic_wrapper_class(eleButtons[0]).click()
            time.sleep(0.2)
            eleChromes = find_elements(auto_id='wvcode', parent=dlg.element_info, top_level_only=False)
            eleHtml = registry.backends['uia'].generic_wrapper_class(eleChromes[0])
            eleHtml.set_text(codeStr)
            time.sleep(0.2)
            eleButton2s = find_elements(auto_id='sub_but_sub', parent=dlg.element_info, top_level_only=False)
            registry.backends['uia'].generic_wrapper_class(eleButton2s[0]).click()
            time.sleep(int(requestTime))
            eleUploads = find_elements(auto_id='wk_down', parent=dlg.element_info, top_level_only=False)
            registry.backends['uia'].generic_wrapper_class(eleUploads[0]).click()
            time.sleep(int(timeStr))
            eleUploads = find_elements(auto_id='1001', parent=dlg.element_info, top_level_only=False)
            eleUp = registry.backends['uia'].generic_wrapper_class(eleUploads[0])
            # eleUp.set_text('')
            # 模拟按键 Shift + Home（选中所有文本）
            pyautogui.hotkey('shift', 'home')
            # 模拟按键 Delete（删除选中的文本）
            pyautogui.press('delete')
            time.sleep(0.1)
            eleUp.set_text(title)
            # eleUploads=find_elements(auto_id='FileTypeControlHost',class_name='AppControlHost',parent=dlg.element_info,top_level_only=False)
            # registry.backends['uia'].generic_wrapper_class(eleUploads[0]).clicked.connect(comboxclick)
            time.sleep(0.4)
            send_keys('^{ENTER}')
            time.sleep(0.2)
            # eleUploads = find_elements(auto_id='1', class_name='Button', parent=dlg.element_info, top_level_only=False)
            # registry.backends['uia'].generic_wrapper_class(eleUploads[0]).click()
            # time.sleep(0.2)
            print(title, '下载成功')
            app.kill()
        except:
            print(title,'下载失败')
            app.kill()


def main():
    titlePath=r'./url.txt'
    #文档等待时间
    requestTime='8'
    # 文档下载时间
    timeStr='15'
    codeStr='1010'
    automatic(titlePath,timeStr,requestTime,codeStr)
if __name__ == '__main__':
    main()