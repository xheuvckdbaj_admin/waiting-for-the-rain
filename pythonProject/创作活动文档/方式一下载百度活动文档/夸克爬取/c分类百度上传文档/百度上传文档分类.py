import os
import shutil

def find_file(wordFolder):
    filePaths=[]
    dir = os.listdir(wordFolder)
    for i, name in enumerate(dir):
            name = os.path.join(wordFolder, name)
            if os.path.isdir(name):
                find_file(name)
            else:
                filePath = name
                filePaths.append(filePath)
    return filePaths

def makeFolder(floderPath,fileName):
    print(fileName)
    floderPath=floderPath+'\\'+fileName
    os.makedirs(floderPath)
    return floderPath
def getFileName(wordFolder):
    fileNameNoDocxs=[]
    wordFiles = find_file(wordFolder)
    for wordFile in wordFiles:
        fileNameNoDocx = os.path.basename(wordFile).replace('.docx', '')
        fileNameNoDocxs.append(fileNameNoDocx)
    return fileNameNoDocxs
def panduanName(titleFolder,uploadFolder,wordFolder):
    fileNameNoDocxs=getFileName(wordFolder)
    filePaths=find_file(titleFolder)
    numberDocx=0
    for filePath in filePaths:
        number=1
        pathName=os.path.basename(filePath).replace('.txt', '')
        if pathName == '1':
            pathNamex = '第一页'
        elif pathName == '2':
            pathNamex = '第二页'
        elif pathName == '3':
            pathNamex = '第三页'
        elif pathName == '4':
            pathNamex = '第四页'
        elif pathName == '5':
            pathNamex = '第五页'
        elif pathName == '6':
            pathNamex = '第六页'
        elif pathName == '7':
            pathNamex = '第七页'
        elif pathName == '8':
            pathNamex = '第八页'
        elif pathName == '9':
            pathNamex = '第九页'
        elif pathName == '10':
            pathNamex = '第十页'
        elif pathName == '11':
            pathNamex = '第一页'
        elif pathName == '12':
            pathNamex = '第一页'
        elif pathName == '13':
            pathNamex = '第一页'
        elif pathName == 'all':
            os.remove(filePath)
            continue
        newfloderPath=uploadFolder+'\\'+pathNamex
        if not os.path.exists(newfloderPath):
            makeFolder(uploadFolder, pathNamex)
        file=open(filePath,'r',encoding='utf-8')
        keys=file.readlines()
        for fileNameNoDocx in fileNameNoDocxs:
            if numberDocx < 505:
                if fileNameNoDocx+'\n' in keys:
                    pagerNumber=str(int(number/11))
                    classifyfloderPath = newfloderPath + '\\' + pagerNumber
                    if not os.path.exists(classifyfloderPath):
                        makeFolder(newfloderPath, pagerNumber)
                    try:
                        shutil.move(wordFolder+'\\'+fileNameNoDocx+'.docx', classifyfloderPath)
                        number+=1
                        numberDocx+=1
                    except:
                        continue

def main():
    #存放活动文档标题的文件夹
    titleFolder=r'G:\文档\百度活动文档\百度活动文档标题'
    #修改后需要分类百度活动文档的文件夹
    wordFolder=r'G:\文档\百度活动文档\修改后百度活动文档'
    #存放分类后需要上传的活动文档的文件夹
    uploadFolder=r'G:\文档\百度活动文档\百度活动文档上传'
    panduanName(titleFolder, uploadFolder, wordFolder)
if __name__ == '__main__':
    main()