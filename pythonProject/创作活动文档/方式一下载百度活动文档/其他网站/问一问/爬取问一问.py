# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
# def getContent(hrefUrl,htmlPath,title,driver):
#     try:
#         requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
#         soup = BeautifulSoup(requests_text, 'lxml')
#         driver.get(hrefUrl)
#         td=driver.find_element(By.XPATH,'//pre[@class="replay-info-txt answer_con"]')
#         list_soupa = td.get_attribute('innerHTML')
#         print(title)
#         thmlpath = htmlPath + '\\' + title + '.html'
#         f = open(thmlpath, 'w', encoding='utf-8')
#         f.write(list_soupa)
#     except:
#         print('error')
def mian_question(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.wgt-ask > h1')[0].text
        list_soupa=str(soup.select('div.abstract-content-text')[0]).replace('摘要','').replace(title,keyStr)
        thmlpath = htmlPath + '\\' + keyStr + ".html"
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('问一问下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://wen.baidu.com/question/272332475969337965.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\第一范文网'
    keyStr='kaixin'
    title=''
    mian_question(url,htmlPath,keyStr,title)