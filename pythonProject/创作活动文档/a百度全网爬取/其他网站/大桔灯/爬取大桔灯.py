import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_dajudeng(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('#n-left > h1')[0].text 
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.contents')[0]).replace('相关推荐','').replace(title,keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('大桔灯下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://doc.wendoc.com/b865254b8a635ddcaa232956a.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\文书帮'
    keyStr='kaixin'
    title=''
    mian_dajudeng(url,htmlPath,keyStr,title)