import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_duohuawang(url, htmlPath, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('h1.entry-title.page-title')[0].text
        list_soupa = str(soup.select('div.entry-content')[0]).replace('相关推荐', '').replace(title, title).split('推荐访问:')[0]
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('多花网下载成功----', title)
    except:
        print('error')


if __name__ == '__main__':
    url = 'https://www.douhuafen.com/22457.html'
    htmlPath = r'D:\文档\百度付费上传文档\百度html\其他'
    keyStr = 'kaixin'
    title = ''
    mian_duohuawang(url, htmlPath, keyStr, title)
