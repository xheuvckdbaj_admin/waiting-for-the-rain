import docx

from pythonProject.FindFile import findAllFile
from docx.api import Document

def filterWord(path,paragraphs):

    string=''
    for i,para in enumerate(paragraphs):
        string=string+para.text
    file = open(path, 'r',encoding='utf-8')
    lines = file.readlines()
    panduan=False
    for i, line in enumerate(lines):
        key=line.replace('\n','').strip()
        panduan=panduan or (key in string)
    return not panduan

def main():
    paths = r'./minganci.txt'
    path=r'E:\文档\自查报告网'
    filePaths=findAllFile(path)
    for filePath in filePaths:
        if not '$' in filePath:
            print(filePath)
            file = docx.Document(filePath)
            filterWord(paths,file.paragraphs)

if __name__ == '__main__':
    main()