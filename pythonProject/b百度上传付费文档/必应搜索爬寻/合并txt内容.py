import os

from pythonProject.FindFile import find_file


def merge(folderpath,path):
    txtpaths=find_file(folderpath)
    newTxtPath=path+'\\'+'all.txt'
    paths=[]
    txtfile_line=open(newTxtPath,'w',encoding='utf-8')
    for txtpath in txtpaths:
        if txtpath.endswith('.txt'):
            print(txtpath)
            try:
                file_line=open(txtpath,'r',encoding="utf-8")
                readlines=file_line.readlines()
                for line in readlines:
                    if not line in paths:
                        paths.append(line)
                file_line.close()
                os.remove(txtpath)
            except:
                try:
                    file_line = open(txtpath, 'r', encoding="gbk")
                    readlines = file_line.readlines()
                    for line in readlines:
                        if not line in paths:
                            paths.append(line)
                    file_line.close()
                    os.remove(txtpath)
                except:
                    print('gbk读不出')
                    continue
    for ph in paths:
        try:
            txtfile_line.write(ph)
        except:
            print('error')
            continue
    txtfile_line.close()
def main():
    folderpath=r'C:\Users\x1899\Desktop\过滤标题0\过滤标题'
    path=r'C:\Users\x1899\Desktop\过滤标题0\过滤标题'
    merge(folderpath,path)
if __name__ == '__main__':
    main()