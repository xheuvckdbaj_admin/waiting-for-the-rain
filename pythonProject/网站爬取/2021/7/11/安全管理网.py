# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        #url='http://www.safehoo.com/Laws/Notice/202107/5644166.shtml'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('.c_content_text')[0])
        print(title)
        #print(list_soupa)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.text
            newUrl='http://www.safehoo.com'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('.pagecss > a')[0].get('href')
    number=int(re.match('(.*?)List_(.*?).shtml',href).group(2))
    src='http://www.safehoo.com'+href.split('List_')[0]+'List_%d.shtml'
    for i in range(1,number):
        newUrl=src%i
        getUrl(thmlpath,newUrl)

def getHtml2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.lmmore > a')
    for a in a_list:
        #time.sleep(10)
        href = a.get('href')
        newUrl='http://www.safehoo.com'+href
        getHtml3(thmlpath, newUrl)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list1 = soup.select('.nv.n1 > .top_lm > a')
    a_list2 = soup.select('.nv.n2 > .top_lm > a')
    for a in a_list1:
        href = a.get('href')
        newUrl='http://www.safehoo.com'+href
        getHtml2(thmlpath, newUrl)
    for a in a_list2:
        href = a.get('href')
        newUrl='http://www.safehoo.com'+href
        getHtml2(thmlpath, newUrl)

def main(thmlpath):
            #number=1
            strUrl='http://www.safehoo.com/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'E:\学习网'
    main(thmlpath)
