import random
import win32com.client as win32
from docx import Document

from FindFile import find_name, find_file
from 重庆标题 import getFileNameCQ


def getTitlePath(path,fileName):
    doc1 = Document()
    doc1.add_heading(fileName, 0)
    filePath = path + "\\" + 'all.docx'
    doc1.save(filePath)
    return filePath


def getFiles(path):
    word = win32.gencache.EnsureDispatch('Word.Application')
    # 非可视化运行
    word.Visible = False

    output = word.Documents.Add()  # 新建合并后空白文档

    # 拼接文档

    file_list = []
    path1 = r'E:\试卷题号\初一数学 (1)'
    path2 = r'E:\试卷题号\初一数学 (2)'
    path3 = r'E:\试卷题号\初一数学 (3)'
    path4 = r'E:\试卷题号\初一数学 (4)'
    path5 = r'E:\试卷题号\初一数学 (5)'
    path6 = r'E:\试卷题号\初一数学 (6)'
    path7 = r'E:\试卷题号\初一数学 (7)'
    path8 = r'E:\试卷题号\初一数学 (8)'
    path9 = r'E:\试卷题号\初一数学 (9)'
    path10 = r'E:\试卷题号\初一数学 (10)'
    path11 = r'E:\试卷题号\初一数学 (11)'
    path12 = r'E:\试卷题号\初一数学 (12)'
    path13 = r'E:\试卷题号\初一数学 (13)'
    path14 = r'E:\试卷题号\初一数学 (14)'
    path15 = r'E:\试卷题号\初一数学 (15)'
    path16 = r'E:\试卷题号\初一数学 (16)'
    path17 = r'E:\试卷题号\初一数学 (17)'
    path18 = r'E:\试卷题号\初一数学 (18)'
    path19 = r'E:\试卷题号\初一数学 (19)'
    path20 = r'E:\试卷题号\初一数学 (20)'
    path21 = r'E:\试卷题号\初一数学 (21)'
    path22 = r'E:\试卷题号\初一数学 (22)'
    path23 = r'E:\试卷题号\初一数学 (24)'
    fileName = getFileNameCQ()
    #file_list.append(getTitlePath(path,fileName))
    files1 = find_file(path1, [])
    files2 = find_file(path2, [])
    files3 = find_file(path3, [])
    files4 = find_file(path4, [])
    files5 = find_file(path5, [])
    files6 = find_file(path6, [])
    files7 = find_file(path7, [])
    files8 = find_file(path8, [])
    files9 = find_file(path9, [])
    files10 = find_file(path10, [])
    files11 = find_file(path11, [])
    files12 = find_file(path12, [])
    files13 = find_file(path13, [])
    files14 = find_file(path14, [])
    files15 = find_file(path15, [])
    files16 = find_file(path16, [])
    files17 = find_file(path17, [])
    files18 = find_file(path18, [])
    files19 = find_file(path19, [])
    files20 = find_file(path20, [])
    files21 = find_file(path21, [])
    files22 = find_file(path22, [])
    files23 = find_file(path23, [])
    output.Application.Selection.Range.InsertFile(files23[random.randint(0, len(files23) - 1)])
    output.Application.Selection.Range.InsertFile(files22[random.randint(0, len(files22) - 1)])

    output.Application.Selection.Range.InsertFile(files21[random.randint(0, len(files21) - 1)])
    #file_list.append(files17[random.randint(0, len(files17) - 1)])

    output.Application.Selection.Range.InsertFile(files20[random.randint(0, len(files20) - 1)])
    #file_list.append(files16[random.randint(0, len(files16) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files19[random.randint(0, len(files19) - 1)])
    output.Application.Selection.Range.InsertFile(files18[random.randint(0, len(files18) - 1)])

    output.Application.Selection.Range.InsertFile(files17[random.randint(0, len(files17) - 1)])
    #file_list.append(files17[random.randint(0, len(files17) - 1)])

    output.Application.Selection.Range.InsertFile(files16[random.randint(0, len(files16) - 1)])
    #file_list.append(files16[random.randint(0, len(files16) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(r'E:\试卷题号\第一部分\3.docx')

    output.Application.Selection.Range.InsertFile(files15[random.randint(0, len(files15) - 1)])
    #file_list.append(files15[random.randint(0, len(files15) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files14[random.randint(0, len(files14) - 1)])
    #file_list.append(files14[random.randint(0, len(files14) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files13[random.randint(0, len(files13) - 1)])
    #file_list.append(files13[random.randint(0, len(files13) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files12[random.randint(0, len(files12) - 1)])
    #file_list.append(files12[random.randint(0, len(files12) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files11[random.randint(0, len(files11) - 1)])
    #file_list.append(files11[random.randint(0, len(files11) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(r'E:\试卷题号\第一部分\2.docx')
    output.Application.Selection.Range.InsertFile(files10[random.randint(0, len(files10) - 1)])
    #file_list.append(files10[random.randint(0, len(files10) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files9[random.randint(0, len(files9) - 1)])
    #file_list.append(files9[random.randint(0, len(files9) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files8[random.randint(0, len(files8) - 1)])
    #file_list.append(files8[random.randint(0, len(files8) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files7[random.randint(0, len(files7) - 1)])
    #file_list.append(files7[random.randint(0, len(files7) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files6[random.randint(0, len(files6) - 1)])
    #file_list.append(files6[random.randint(0, len(files6) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files5[random.randint(0, len(files5) - 1)])
    #file_list.append(files5[random.randint(0, len(files5) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files4[random.randint(0, len(files4) - 1)])
    #file_list.append(files4[random.randint(0, len(files4) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files3[random.randint(0, len(files3) - 1)])
    #file_list.append(files3[random.randint(0, len(files3) - 1)])
    #print(file_list)
    output.Application.Selection.Range.InsertFile(files2[random.randint(0, len(files2) - 1)])
    #file_list.append(files2[random.randint(0, len(files2) - 1)])
    #print(file_list)

    output.Application.Selection.Range.InsertFile(files1[random.randint(0, len(files1) - 1)])
    #file_list.append(files1[random.randint(0, len(files1) - 1)])
    #print(files1[random.randint(0, len(files1) - 1)])
    output.Application.Selection.Range.InsertFile(r'E:\试卷题号\第一部分\1.docx')
    output.Application.Selection.Range.InsertFile(getTitlePath(path,fileName))
    # 获取合并后文档的内容
    doc = output.Range(output.Content.Start, output.Content.End)
    # doc.Font.Name = "黑体"  # 设置字体

    output.SaveAs(r'E:\卷子' + '\\' + fileName + '.docx')  # 保存
    output.Close()
    return file_list
    # print(files)
    # print(path1+'\\'+str(i))
    # print(files[random.randint(0,len(files)-1)])


def main():
    path = r'E:\卷子'
    # getTitlePath(path)
    for i in range(1,5500):
        file_list = getFiles(path)


if __name__ == '__main__':
    main()