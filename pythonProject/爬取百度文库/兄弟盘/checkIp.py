import requests


def check_proxy(ip):
    proxies = {'http': ip}
    url = "http://www.baidu.com/"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
    }
    try:
        response = requests.get(url, headers=headers, proxies=proxies, timeout=3)
        #print(response.text)
        return True
    except:
        return False
def main():
    ip='175.155.136.234:58593'
    check_proxy(ip)
if __name__ == '__main__':
    main()