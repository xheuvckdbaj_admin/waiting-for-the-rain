import re

391028# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.41 Safari/537.36'}


def getHtml(thmlpath, strUrl):
    try:
        print(strUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('div.art-text')[0]).replace('<br>', '<p><p/>').replace('<br/>',
                                                                                           '<p><p/>').replace(
            ' 　　', '<p><p/>').replace('　', '<p><p/>').replace(' ', '<p><p/>')
        title = soup.select('div.article > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

def getListUrl(file,strUrl,driver):
    driver.get(strUrl)
    time.sleep(1)
    elements=driver.find_elements_by_xpath('//ul[@class="doc-list"]/li/div[@class="title"]/a/span[@class="doc-title"]')
    for i,element in enumerate(elements):
        title=element.text.replace('.docx','').replace('.doc','').replace('.pdf','').replace('.ppt','')
        print(title)
        file.write(title+'\n')
def getPageUrl(file, strUrl,driver):

    hrefUrl=strUrl.replace('.html','-%d.html')
    for i in range(1,51):
        newUrl=hrefUrl%i
        getListUrl(file, newUrl,driver)


def getTitleUrl(txtPath,thmlpath, strUrl,driver):
    file=open(thmlpath,'r',encoding='utf-8')
    lines=file.readlines()
    for i, line in enumerate(lines):
        if i > 0:
            url=line.replace('\n','')
            path = txtPath + '\\' + str(i) + '.txt'
            file = open(path, 'w', encoding='utf-8')
            getPageUrl(file, url,driver)
            file.close()


def getTitle2(thmlpath, url,driver):
    driver.get(url)
    time.sleep(5)
    elements=driver.find_elements_by_xpath('//div[@class="nav"]/ul[@class="list"]/li/a')
    for i,element in enumerate(elements):
        href=element.get_attribute('href')
        print(href)

def main(thmlpath,txtPath):
        driver=webdriver.Chrome()
        url='https://max.book118.com/doclist/list-312.html'
        getTitleUrl(txtPath,thmlpath, url,driver)
        # getTitle2(thmlpath, url, driver)


if __name__ == '__main__':
    thmlpath = r'./test.txt'
    txtPath=r'C:\Users\Administrator\Desktop\关键字'
    main(thmlpath,txtPath)
