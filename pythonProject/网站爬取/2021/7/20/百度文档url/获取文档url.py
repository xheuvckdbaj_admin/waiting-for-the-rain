import time
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


def getBaiduUrl(browser,folderPath,url,pagelow):

    # try:
    el = browser.find_element_by_xpath('//div[@class="popper-title"]')
    ActionChains(browser).move_to_element(el).perform()
    time.sleep(0.1)
    element = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="popper-content"]/div[@class="pop-list"]/div[@class="item"]'))
    )
    try:
        browser.find_elements_by_xpath('//div[@class="popper-content"]/div[@class="pop-list"]/div[@class="item"]')[1].click()
    except:
        print('不可选取doc')
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    browser.switch_to.window(n[-1])
    element = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="search-doc-list"]'))
    )
    browser.find_element_by_xpath('//div[@class="filter-item"]/span').click()
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    browser.switch_to.window(n[-1])
    element = WebDriverWait(browser, 5).until(
        EC.presence_of_element_located((By.XPATH, '//div[@class="search-doc-list"]'))
    )
    pages=browser.find_elements_by_xpath('//div[@class="pager"]/span')
    pageNumber=int(pages[len(pages)-2].text)

    txtfilePath=folderPath+'\\'+url.split('=')[-1].replace('#doc','')+'.txt'
    file_line=open(txtfilePath,'w')
    for i in range(pagelow,pageNumber+1):

            print(pageNumber)
            print(i)
            if not i==1:
                pages = browser.find_elements_by_xpath('//div[@class="pager"]/span')
                pages[-1].click()
                n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                browser.switch_to.window(n[-1])
                time.sleep(1)

            eles=browser.find_elements_by_xpath('//div[@class="item-doc"]/div[@class="doc-item"]/div[@class="title"]')
            for ele in eles:
                try:
                    ele.click()
                    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                    browser.switch_to.window(n[-1])
                    element = WebDriverWait(browser, 100).until(
                        EC.presence_of_element_located((By.XPATH, '//h3[@class="doc-title"]'))
                    )
                    wordUrl=browser.current_url
                    file_line.write(wordUrl+'\n')
                    browser.close()
                    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                    browser.switch_to.window(n[-1])
                except:
                    print('获取文档页面出错了')
            time.sleep(2)

    file_line.close()
    # except:
    #     print('该url不可用')

def getshopUrl(filePath,folderPath,linelow,pagelow):
    file_line=open(filePath,'r')
    lines=file_line.readlines()
    browser = webdriver.Chrome()
    for i in range(linelow,len(lines)+1):
        url=lines[i-1].replace('\n','')+'#doc'
        print(url)
        browser.get(url)
        try:

            element = WebDriverWait(browser, 5).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="popper-title"]'))
            )
            print(url)
        except:
            print('该店铺不可用')
            continue
        try:
            getBaiduUrl(browser,folderPath,url,pagelow)
        except:
            print('文档太少了')
            continue
def main():
    filePath=r'G:\文档\店铺url\习题参考1631290808.9047995moval.txt'
    folderPath=r'G:\文档\百度文档url'
    linelow=10
    pagelow=1
    getshopUrl(filePath,folderPath,linelow,pagelow)
if __name__ == '__main__':
    main()