# -*- coding:utf-8 -*-
import re
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content.ft14')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>')
            list_soupa=list_soupa.split('相关推荐：')[0].split('上一页')[0]
            return list_soupa
        except:
            print('编码出错')
            return ''
def getContent(thmlpath,strUrl):
    try:
        content=getHtml(thmlpath, strUrl)
        url=strUrl.replace('.shtml', '_%d.shtml')
        for i in range(2,4):
            newUrl=url%i
            content=content+getHtml(thmlpath, newUrl)
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.ku-container.left.ku-content > h1.ft20.center')[0].text.replace('?', '').replace('/', '')
        print(title)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(content)
    except:
        print('error')

def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('dl.bk-item > dd > h3.ft16.bm5 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getContent(thmlpath,href)
def getPageUrl(thmlpath,strUrl):
    getListUrl(thmlpath, strUrl)
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.pages > a')[-2].get('href')
    pageNumber=int(href.split('_')[-1].replace('.shtml',''))
    url=href.replace(href.split('_')[-1].replace('.shtml',''),'%d')
    for i in range(2,pageNumber+1):
        newUrl=url%i
        getListUrl(thmlpath, newUrl)

def getRTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.mainR > div.sidebox > h2 > span > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getPageUrl(thmlpath, href)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.mainL > div.content.borderR.clearfix > div.inbox.bluebox > div.cont.clearfix > h3 > a')
    for i,a in enumerate(a_list):
            href=a.get('href')
            getPageUrl(thmlpath, href)

def main(thmlpath):
    url='http://www.youjiao.com/kjja/jiaoan/'
    getTitleUrl(thmlpath, url)



if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\爬取网站4.20\youjiao幼教网'
    main(thmlpath)
