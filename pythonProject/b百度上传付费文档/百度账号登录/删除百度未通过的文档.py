
# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import requests
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.b百度上传付费文档.百度上传.readTxt import readTxt

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewPage(page, url):
    try:
        page.goto(url)
        page.wait_for_timeout(500)
        try:
            page.get_by_role("button", name="我知道啦").click()
        except:
            print('')
    except:
        page.wait_for_timeout(500)
        getNewPage(page, url)
def deleteTitle(page1):
    elements = page1.locator('//tr[@class="el-table__row"]')
    for i in range(0,elements.count()):
        try:
        #     content = elements.nth(i).inner_html()
            content=elements.nth(i).locator('//td[@class="el-table_1_column_2 is-center table-status"]/div[@class="cell"]').text_content().strip()
            if content=='未通过':
                elements.nth(i).locator('//td[@class="el-table_1_column_3 is-center "]/div[@class="cell"]/button[@class="el-button el-button--default el-button--mini"]').nth(-1).click()
                time.sleep(0.5)
                page1.get_by_role("button", name="确定").click()
                time.sleep(2)
                break
        except Exception as e:
            print(str(e))
def getLocator(page,number):
    for i in range(number, 100000000):
        try:
            page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
            page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(str(i))
            page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
            time.sleep(0.5)
            deleteTitle(page)
        except:
            getNewPage(page,"https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
            page.get_by_text("文档", exact=True).click()
            time.sleep(0.1)
            getLocator(page, i+1)
def uploadFile(page, number):
    url = 'https://cuttlefish.baidu.com/shopmis?_wkts_=1679576168528&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation'
    panduanUpload = False
    getNewPage(page, url)
    getLocator(page, number)





def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1679576742834&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1679576742834&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.get_by_role("button", name="Close").click()
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page


def mainTitleClassifyUpload(playwright: Playwright, cookiesPath, number) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()

    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    time.sleep(1)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    uploadFile(page, number)


def mainTitleClassifyUploadApi(folderPath, cookiesPath, successFloder):
    with sync_playwright() as playwright:
        try:
            number=1
            mainTitleClassifyUpload(playwright, cookiesPath, number)
        except:
            print('这个出错，下一个')
    # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


def main():
    folderPath = r'D:\文档\百度付费上传文档\修改后百度活动文档'
    # panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    successFloder = r'D:\文档\百度付费上传文档\百度上传成功'
    cookiesPath1 = './baiduCookies9991.txt'
    cookiesPath2 = './baiduCookies2.txt'
    cookiesPath3 = './baiduCookies3.txt'
    cookiesPath4 = './baiduCookies4.txt'
    cookiesPath5 = './baiduCookies5.txt'
    cookiesPath6 = './baiduCookies6.txt'
    cookiesPath7 = './baiduCookies7.txt'
    cookiesPath8 = './baiduCookies8.txt'
    cookiesPath9 = './baiduCookies9.txt'
    cookiesPath10 = './baiduCookies10.txt'
    cookiesPath11 = './baiduCookies11.txt'
    cookiesPath12 = './baiduCookies12.txt'
    cookiesPath13 = './baiduCookies13.txt'
    cookiesPath14 = './baiduCookies14.txt'
    cookiesPath15 = './baiduCookies15.txt'
    cookiesPath16 = './baiduCookies16.txt'
    cookiesPath17 = './baiduCookies17.txt'
    cookiesPath18 = './baiduCookies18.txt'
    cookiesPath19 = './baiduCookies19.txt'
    cookiesPath20 = './baiduCookies20.txt'
    cookiesPath21 = './baiduCookies21.txt'
    cookiesPath22 = './baiduCookies22.txt'
    cookiesPath23 = './baiduCookies23.txt'
    cookiesPath24 = './baiduCookies24.txt'
    cookiesPath25 = './baiduCookies25.txt'
    print('------------------账号1开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath1, successFloder)
    print('------------------账号2开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath2,successFloder)
    print('------------------账号3开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath3,successFloder)
    print('------------------账号4开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath4,successFloder)
    print('------------------账号5开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath5,successFloder)
    print('------------------账号6开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath6,successFloder)
    print('------------------账号7开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath7,successFloder)
    print('------------------账号8开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath8,successFloder)
    print('------------------账号9开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath9,successFloder)
    print('------------------账号10开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath10,successFloder)
    print('------------------账号11开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath11,successFloder)
    print('------------------账号12开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath12,successFloder)
    print('------------------账号13开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath13,successFloder)
    print('------------------账号14开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath14,successFloder)
    print('------------------账号15开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath15,successFloder)
    print('------------------账号16开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath16,successFloder)
    print('------------------账号17开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath17,successFloder)
    print('------------------账号18开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath18,successFloder)
    print('------------------账号19开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath19,successFloder)
    print('------------------账号20开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath20,successFloder)
    print('------------------账号21开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath21,successFloder)
    print('------------------账号22开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath22,successFloder)
    print('------------------账号23开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath23,successFloder)
    print('------------------账号24开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath24,successFloder)
    print('------------------账号25开始运行')
    mainTitleClassifyUploadApi(folderPath, cookiesPath25,successFloder)


if __name__ == '__main__':
    for i in range(0, 1000):
        # mainzhengli()
        main()
