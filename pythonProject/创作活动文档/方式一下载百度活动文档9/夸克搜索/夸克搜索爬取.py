import time
import urllib

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档4.夸克搜索.网站选择 import kuakeWebsiteJude
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getPhoneDriver
def getNewWindow(browser,xpathStr,timeNumber):
    n=browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    element=WebDriverWait(browser,timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH,xpathStr)
        )
    )

def getContentByopenPhone(htmlPath,driver,keyStr,newUrl,urls):
    getNewWindow(driver, '//textarea', 10)
    driver.find_element(By.XPATH, '//textarea[@class="se-input"]').clear()
    try:
        driver.find_element(By.XPATH, '//textarea[@class="se-input"]').send_keys(keyStr)
    except:
        driver.find_element(By.XPATH, '//textarea[@class="se-input"]').send_keys(keyStr)
    time.sleep(2)
    driver.find_element(By.XPATH, '//textarea[@class="se-input"]').send_keys(Keys.ENTER)
    time.sleep(0.2)
    getNewWindow(driver, '//span/i[@c-bind="data.text"]', 10)
    elements=driver.find_elements(By.XPATH,'//span/i[@c-bind="data.text"]')
    for i,element in enumerate(elements):
        title=element.text
        if mainPanDuanTitle(keyStr,title):
            # element.click()
            print(keyStr + '++++++++' + title)
            panduan=kuakeWebsiteJude(element, htmlPath, driver, keyStr, urls)
            driver.back()
            break

def main_kuake(titlePath,htmlPath,wangzhiPath,startnumber,last):
    urls=[]
    wangzhifile=open(wangzhiPath,'w',encoding='utf-8')
    file=open(titlePath+'\\all.txt','r',encoding='utf-8')
    file_lines=file.readlines()
    driver=getPhoneDriver('kuake')
    url = 'https://quark.sm.cn/s'
    driver.get(url)
    for i,line in enumerate(file_lines):
        if i >= startnumber and i < last:
            try:
                keyStr=line.replace('\n','')
                print(str(i)+'-----'+keyStr)
                newUrl=url
                getContentByopenPhone(htmlPath, driver, keyStr, newUrl,urls)
            except:
                time.sleep(50)
                print('夸克需要验证')
    driver.quit()
    for hrefUrl in urls:
        wangzhifile.write(hrefUrl+'\n')
if __name__ == '__main__':
    titlePath=r'D:\文档\百度活动文档4\百度活动文档标题'
    htmlPath=r'D:\文档\百度活动文档4\百度html'
    wangzhiPath=r'./wangzhi,txt'
    # 标题开始值
    startnumber = 0
    # 标题结束值
    last = 10870
    main_kuake(titlePath,htmlPath,wangzhiPath,startnumber,last)
