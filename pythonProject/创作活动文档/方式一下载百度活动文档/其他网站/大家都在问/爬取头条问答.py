import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_toutiaowenda(url,htmlPath,keyStr,title):
    try:
        driver=getDriver('tou')
        driver.get(url)
        title=driver.find_element(By.XPATH,'//h2[@class="title_nBzUY7 f-c-1"]').text 
        title=re.sub('（(.*)）', '', title)
        title = re.sub('\d+篇', '', title)
        list_soupa=str(driver.find_element(By.XPATH, '//div[@class="answer_layout_wrapper_2CajRz"]').get_attribute('innerHTML').replace(title,keyStr))
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w',encoding='utf-8')
        f.write(list_soupa)
        print('头条问答下载成功----',keyStr)
        f.close()
        driver.quit()
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://so.toutiao.com/s/search_wenda_pc/list/?enter_from=search_result&qid=6964271704265195813&search_id=2022102200300701021217203417BCA07E&qname=%E4%BB%BF%E5%86%99%E9%87%91%E5%AD%97%E5%A1%94%E5%A4%95%E7%85%A7%E7%AC%AC%E4%BA%8C%E6%AE%B5%EF%BC%9F&enter_answer_id=6964291833032458509&outer_show_aid=6964291833032458509&query=%E4%BA%94%E5%B9%B4%E7%BA%A7%E4%B8%8B%E5%86%8C%E9%87%91%E5%AD%97%E5%A1%94%E4%BB%BF%E5%86%99%E7%AC%AC%E4%BA%8C%E8%87%AA%E7%84%B6%E6%AE%B5'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_toutiaowenda(url,htmlPath,keyStr,title)