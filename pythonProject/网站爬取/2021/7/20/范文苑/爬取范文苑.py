# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="fwy_next"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.fanwenyuan.com/zhichangfanwen/gongzuozongjie/218501.html',
                'https://www.fanwenyuan.com/zhichangfanwen/nianzhongzongjie/218129.html',
                'https://www.fanwenyuan.com/zhichangfanwen/gongzuojihua/218532.html',
                'https://www.fanwenyuan.com/zhichangfanwen/gongzuofangan/218253.html',
                'https://www.fanwenyuan.com/zhichangfanwen/gongzuocehua/207244.html',
                'https://www.fanwenyuan.com/zhichangfanwen/huodongcehua/218539.html',
                'https://www.fanwenyuan.com/zhichangfanwen/jianlimoban/150986.html',
                'https://www.fanwenyuan.com/zhichangfanwen/zhiyeguihua/217965.html',
                'https://www.fanwenyuan.com/jianghuazhici/yanjianggao/218522.html',
                'https://www.fanwenyuan.com/jianghuazhici/fayangao/218303.html',
                'https://www.fanwenyuan.com/jianghuazhici/zhuchigao/217862.html',
                'https://www.fanwenyuan.com/jianghuazhici/heci/217784.html',
                'https://www.fanwenyuan.com/jianghuazhici/zhujiuci/217799.html',
                'https://www.fanwenyuan.com/jianghuazhici/daxieci/218519.html',
                'https://www.fanwenyuan.com/jianghuazhici/huanyingci/217411.html',
                'https://www.fanwenyuan.com/baogaotihui/shixibaogao/218533.html',
                'https://www.fanwenyuan.com/baogaotihui/shuzhibaogao/217785.html',
                'https://www.fanwenyuan.com/baogaotihui/cizhibaogao/218353.html',
                'https://www.fanwenyuan.com/baogaotihui/shijianbaogao/216041.html',
                'https://www.fanwenyuan.com/baogaotihui/shiyongqibaogao/146271.html',
                'https://www.fanwenyuan.com/baogaotihui/diaochabaogao/207470.html',
                'https://www.fanwenyuan.com/baogaotihui/kaitibaogao/146112.html',
                'https://www.fanwenyuan.com/baogaotihui/xindetihui/218517.html',
                'https://www.fanwenyuan.com/tiaojushuxin/ganxiexin/218537.html',
                'https://www.fanwenyuan.com/tiaojushuxin/jieshaoxin/217537.html',
                'https://www.fanwenyuan.com/tiaojushuxin/cizhixin/217750.html',
                'https://www.fanwenyuan.com/tiaojushuxin/biaoyangxin/218523.html',
                'https://www.fanwenyuan.com/tiaojushuxin/daoqianxin/218482.html',
                'https://www.fanwenyuan.com/tiaojushuxin/jianjuxin/5333.html',
                'https://www.fanwenyuan.com/tiaojushuxin/weiwenxin/218526.html',
                'https://www.fanwenyuan.com/tiaojushuxin/qiuzhixin/218511.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/kejian/216495.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/jiaoan/218540.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/shiti/216511.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/jiaoxuejihua/218538.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/jiaoxuezongjie/218226.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/jiaoxuefansi/218444.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/jiaoxuexinde/217786.html',
                'https://www.fanwenyuan.com/jiaoxuewendang/jiaoxuesheji/218379.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
