import os
import time
import requests
from playwright.sync_api import Playwright, sync_playwright, expect
import requests
import json
import base64
import time
import cv2
import numpy as np

KEY_CODE = 'uDGISlU2'
IP = 'www.ocr.mobi:20000'
free = False
software_key_code = ''  # 0bagIqnf


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def on_request(request):
    htmlPath = r'D:\图片'
    makeFolder(htmlPath)
    title = 'haha'
    print(f'Request URL: {request.url}')
    print(f'Request Headers: {request.headers}')
    if request.post_data is not None:
        print(f'Request Body: {request.post_data}')
    if 'https://passport.baidu.com/cap/img?ak=' in request.url:
        requests_text = requests.get(url=request.url, headers=request.headers, timeout=5.0).content
        thmlpath = htmlPath + '\\' + title + '.jpg'
        f = open(thmlpath, 'wb')
        f.write(requests_text)
        print('图片下载成功----', title)


def predict(img_path, request_data={}):
    with open(img_path, 'rb') as r:
        img = r.read()
    img = base64.b64encode(img)
    img = str(img, encoding='utf-8')
    request_data['img'] = img
    begin_time = time.time()
    code = requests.post(
        'http://' + IP + '/predict?key_code={}&number={}&free={}&software_key_code={}'.format(KEY_CODE, 10003, free,
                                                                                              software_key_code),
        json.dumps(request_data))
    js = json.loads(code.text)
    data = json.loads(js['content'])
    return data


def anquanyanzheng(page):

    page.wait_for_timeout(2000)
    passModes = page.locator('//div[@class=" passMod_dialog-wrapper passMod_show"]')
    passNumbers = page.locator('//div[@class="b2c6f2cfda bd9d9df718"]')

    if not passModes.count() == 0:
        yanzheng = page.locator(".b0b2aae214")
        if yanzheng.count() == 0:
            yanzheng = page.locator(".passMod_slide-btn ")
        # yanzheng=page.get_by_text("|刷新")
        # except:
        #     yanzheng=page.locator('//div[@class="passMod_slide-btn"]')
        huadong = page.locator(".b8611b5686")
        if huadong.count() == 0:
            huadong = page.locator(".passMod_slide-tip.slideShine")
        box = yanzheng.bounding_box()
        # print(yanzheng1.count())
        box_hua = huadong.bounding_box()
        width_x = box_hua['width'] - box['width']
        image_path = "D:\图片\haha.jpg"
        angle = predict(image_path)
        angle = int(angle['angle']) / 360
        distance = int(width_x * angle)
        page.mouse.move(box['x'] + box['width'] / 2, box['y'] + box['height'] / 2)
        page.mouse.down()
        page.mouse.move(box['x'] + box['width'] / 2 + distance, box['y'] + box['height'] / 2)
        page.mouse.up()
        page.wait_for_timeout(2000)
        passModes = page.locator('//div[@class=" passMod_dialog-wrapper passMod_show"]')
        if not passModes.count() == 0:
            anquanyanzheng(page)
    print(passNumbers.count())
    if not passNumbers.count() == 0:
        print('手势验证通过不了')
        page.reload()
        page.wait_for_timeout(1000)
        anquanyanzheng(page)


def run(playwright: Playwright, i) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.set_default_timeout(6000)
    # 设置请求监听器
    page.on('request', on_request)
    page.goto(
        "https://seccaptcha.baidu.com/v1/webapi/verint/svcp.html?ak=M7bcdh2k6uqtYV5miaRiI8m8x6LIaONq&ext=3un16iDruRDTopLLdowMrYwmKx9B794PKa3Dq5qhtNhlNQlxvoZo8YNKvX1m%2FXp9PYBneGLgR5LVs5xV2B8AIc%2BJzSZMAGRWZHFYopIbJkBVmbAKyTpB3mrUU2Ezau3o&subid=cuttlefish_previewfile_bfe&ts=1709428231&sign=0573f64d66884fb02d8593f9f38a0c0b&backurl=https%3A%2F%2Fcuttlefish.baidu.com%2Fshopmis%3F_wkts_%3D1709428205563%26bdQuery%3D%25E7%2599%25BE%25E5%25BA%25A6%25E6%2596%2587%25E6%25A1%25A3%23%2Fdoc%2Fupload")
    time.sleep(1)

    anquanyanzheng(page)

    # print(srcHref)
    # page.get_by_text("拖动左侧滑块使图片为正").click()
    # page.get_by_text("百度安全验证 请完成下方验证后继续操作 正在验证... 验证通过 图片未转正 网络不给力，请刷新重试 加载中... 拖动左侧滑块使图片为正 扫码验证| 意见反馈").click()
    # page.get_by_text("|刷新").click()

    # time.sleep(1000)

    # ---------------------
    context.close()
    browser.close()


def getPicture(i):
    with sync_playwright() as playwright:
        run(playwright, i)


if __name__ == '__main__':
    for i in range (1,5000):
        i = 1
        getPicture(i)
