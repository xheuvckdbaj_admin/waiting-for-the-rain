# -*- coding:utf-8 -*-
import docx
import requests
import time
from bs4 import BeautifulSoup
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getBrowserUrl(browser,url):
    try:
        browser.get(url)
    except:
        time.sleep(0.5)
        browser.get(url)
def getContent(thmlpath, strUrl,title):
    requests_text = str(requests.get(url=strUrl).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.article-article')[0])
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getUrlList(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.index-list-right-list > ul > li > div.title > a')
    for a in a_list:
        href=a.get('href')
        title=a.text.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
        getContent(thmlpath, href,title)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.guide-infox > li > a')
    for a in a_list:
        href=a.get('href')
        getUrlList(thmlpath, href)
    # contentText=re.sub('<span>(.*?)</span>','',content)
    # title=str(soup.select('div.content > div > h1')[0].text)
    # print(title)
    # title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
    #     .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    # thmlpath = thmlpath + '\\' + title + '.html'
    # f = open(thmlpath, 'w', encoding='utf-8')
    # f.write(contentText)
    # f.close()
def main(thmlpath):
            #参考文献：
                strUrl='https://www.hnbllw.com/wendang/'
                # try:
                getHtml(thmlpath, strUrl)
                # except:
                #     print('出错')
                #     continue
if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
