import os

import time

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver
def getNewWindow(browser,xpathStr,number):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-原创力下载])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, number).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)


def txtWrite(browser,fileline):
    elements = browser.find_elements_by_xpath('//div/span[@class="wzbtlista font14"]/a')
    for ele in elements:
        href = ele.get_attribute('href')
        fileline.write(href + '\n')
def crawing(browser, newUrl, fileline):
   getBrowserUrl(browser, newUrl)
   txtWrite(browser, fileline)
   for i in range(0,99):
       try:
           text=browser.find_elements_by_xpath('//tr/td[@align="center"]/div[@class="ym"]/a')[-2].text
           if text=='下页':
               browser.find_elements_by_xpath('//tr/td[@align="center"]/div[@class="ym"]/a')[-2].click()
               getNewWindow(browser, '//tr/td[@align="center"]/div[@class="ym"]/a', 100)
               txtWrite(browser,fileline)
       except:
           print('网络已断开或没有文档')
           continue

def getKey(filePath,newPath,browser,url):
    file=open(filePath,'r',encoding="utf-8")
    file_lines=file.readlines()
    for i in range(0,len(file_lines)):
        print('第几个关键词：',str(i)+file_lines[i])
        key=file_lines[i].replace('\n','').strip()
        newUrl=url%key
        txtPath=newPath+'\\'+key+str(time.time())+'.txt'
        fileline=open(txtPath,'w')
        crawing(browser, newUrl, fileline)
        fileline.close()
def getAllUrl(browser,url,fileLine,number):
    if number < 3200:
        number+=1
        try:
            getBrowserUrl(browser, url)
            href=browser.find_element_by_xpath('//div[@class="prev_next"]/p[@class="p2"]/a').get_attribute('href')
            print(href)
            fileLine.write(href+'\n')
            getAllUrl(browser, url, fileLine,number)
        except:
            print('已经没有文档')
def getTxtUrl(filePath,newPath,browser):
    file=open(filePath,'r')
    file_lines=file.readlines()
    for file_line in file_lines:
        print(file_line)
        url=file_line.replace('\n','').strip()
        path=newPath+'\\'+str(time.time())+'.txt'
        fileLine=open(path,'w')
        fileLine.write(file_line)
        number=1
        getAllUrl(browser,url,fileLine,number)
        fileLine.close()
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#artContent')[0])
            title = soup.select('div.printArticle > h2.titiletext')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')
def openAllUrl(filePath,newPath):
    file=open(filePath,'r')
    file_lines=file.readlines()
    for file_line in file_lines:
        print(file_line)
        url=file_line.replace('\n','').strip()
        getContent(newPath, url)
def main():
    filePath = r"D:\文档\360图书馆\关键词11.4.txt"
    newPath=r'D:\文档\360图书馆\360url'
    url='http://www.360doc.com/search.html?type=0&word=%s#'
    browser = set_chrome_pref(newPath)
    getKey(filePath,newPath,browser,url)
def main2():
    filePath = r"E:\文档\自查报告网url\all.txt"
    newPath = r'E:\文档\自查报告网'
    browser = set_chrome_pref(newPath)
    getTxtUrl(filePath,newPath,browser)
def main3():
    filePath = r"E:\文档\自查报告网url\all.txt"
    newPath = r'E:\文档\自查报告网'
    openAllUrl(filePath,newPath)
if __name__ == '__main__':
    main()