import os
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.b百度上传付费文档.其他网站.B9教育.B9教育 import mian_B9jiaoyu
from pythonProject.b百度上传付费文档.其他网站.CN范文网.爬取CN范文网 import mian_CNfanwenwang
from pythonProject.b百度上传付费文档.其他网站.PPTOK.爬取PPTOK import mian_pptok
from pythonProject.b百度上传付费文档.其他网站.PPT宝藏.爬取PPT宝藏 import mian_pptbaozang
from pythonProject.b百度上传付费文档.其他网站.cooco.爬取cooco import mian_cooco
from pythonProject.b百度上传付费文档.其他网站.fanzidian.爬取fanzidian import mian_fanzidian
from pythonProject.b百度上传付费文档.其他网站.hao86.爬取hao86 import mian_hao86
from pythonProject.b百度上传付费文档.其他网站.pp作文网.爬取pp作文网 import mian_ppzuowenwang
from pythonProject.b百度上传付费文档.其他网站.t7t8美文号.爬取t7t8美文号 import mian_t7t8meiwenhao
from pythonProject.b百度上传付费文档.其他网站.一一合同网.爬取一一合同网 import mian_yyhetong
from pythonProject.b百度上传付费文档.其他网站.一七辞职网.爬取17辞职网 import mian_17cizhiwang
from pythonProject.b百度上传付费文档.其他网站.一团范文网.爬取一团范文网 import mian_yituanfanwenwang
from pythonProject.b百度上传付费文档.其他网站.一秘范文模板.爬取一秘范文模板 import mian_yimifanwenwang
from pythonProject.b百度上传付费文档.其他网站.七七范文网.爬取七七范文网 import mian_qiqifanwenwang
from pythonProject.b百度上传付费文档.其他网站.七安文库.爬取七安文库 import mian_qianwenku
from pythonProject.b百度上传付费文档.其他网站.七故事.爬取七故事 import mian_qigushi
from pythonProject.b百度上传付费文档.其他网站.三六零问答.爬取360问答 import mian_360wenda
from pythonProject.b百度上传付费文档.其他网站.三百字范文.爬取300字范文 import mian_300zi
from pythonProject.b百度上传付费文档.其他网站.上音文档网2.爬取上音文档网 import mian_shangyinwendangwang
from pythonProject.b百度上传付费文档.其他网站.世纪秘书.爬取世纪秘书 import mian_shijimishu
from pythonProject.b百度上传付费文档.其他网站.业百科.爬取业百科 import mian_yebaike
from pythonProject.b百度上传付费文档.其他网站.东方文库.爬取东方文库 import mian_dongfangwenku
from pythonProject.b百度上传付费文档.其他网站.个人简历.爬取个人简历 import mian_gerenjianli
from pythonProject.b百度上传付费文档.其他网站.中国建筑防水协会.爬取中国建筑防水协会 import \
    mian_zhongguojianzhufangshuixiehui
from pythonProject.b百度上传付费文档.其他网站.中国经济网.爬取中国经济网 import mian_zhongguojingjiwang
from pythonProject.b百度上传付费文档.其他网站.中工网.爬取中工网 import mian_zhongongwang
from pythonProject.b百度上传付费文档.其他网站.中文期刊.爬取中文期刊 import mian_zhonwenqikan
from pythonProject.b百度上传付费文档.其他网站.中考网.爬取中考网 import mian_zhonkaowang
from pythonProject.b百度上传付费文档.其他网站.中鸿认证服务.爬取中鸿认证服务 import mian_zhonhongrenzhengfuwu
from pythonProject.b百度上传付费文档.其他网站.久久散文网.爬取久久散文网 import mian_jiujiusanwenwang
from pythonProject.b百度上传付费文档.其他网站.乐哈文库.爬取乐哈文库 import mian_lehawenku
from pythonProject.b百度上传付费文档.其他网站.乐德范文网.爬取乐德范文网 import mian_ledefanwenwang
from pythonProject.b百度上传付费文档.其他网站.九九作文网.爬取99作文网 import mian_99zuowenwang
from pythonProject.b百度上传付费文档.其他网站.九九范文网.爬取99范文网 import mian_99fanwenwang
from pythonProject.b百度上传付费文档.其他网站.九二报告网.爬取九二报告网 import mian_jiuerbaogaowang
from pythonProject.b百度上传付费文档.其他网站.九二范文网.爬取92范文网 import mian_jiuerfanwenwang
from pythonProject.b百度上传付费文档.其他网站.九八五考试网.爬取985考试网 import mian_985kaoshiwang
from pythonProject.b百度上传付费文档.其他网站.书包范文.爬取书包范文 import mian_shubaofanwen
from pythonProject.b百度上传付费文档.其他网站.云文档网.爬取云文档网 import mian_yunwendangwang
from pythonProject.b百度上传付费文档.其他网站.云纵网.爬取云纵网 import mian_yunzongwang
from pythonProject.b百度上传付费文档.其他网站.互信范文网.爬取互信范文网 import mian_huxinfanwen
from pythonProject.b百度上传付费文档.其他网站.五一贴图网.爬取五一贴图网 import mian_51tietu
from pythonProject.b百度上传付费文档.其他网站.五一费宝网.爬取五一费宝网 import mian_wuyifeibaowang
from pythonProject.b百度上传付费文档.其他网站.五六范文网.爬取五六范文网 import mian_wuliu
from pythonProject.b百度上传付费文档.其他网站.五零六八教学资源网.爬取5068教学资源网 import mian_5068jiaoxueziyuan
from pythonProject.b百度上传付费文档.其他网站.产业在线.爬取产业在线 import mian_chanyezaixian
from pythonProject.b百度上传付费文档.其他网站.人人点.爬取人人点 import mian_renrendian
from pythonProject.b百度上传付费文档.其他网站.人人范文网.爬取人人范文网 import mian_renrenfanwenwang
from pythonProject.b百度上传付费文档.其他网站.亿百出版网.爬取亿百出版网 import mian_yibaichu
from pythonProject.b百度上传付费文档.其他网站.亿百出版网二.爬取亿百出版网二 import mian_yibaichubanwang
from pythonProject.b百度上传付费文档.其他网站.今日头条.爬取今日头条 import mian_jinritoutiao
from pythonProject.b百度上传付费文档.其他网站.今日热点.爬取今日热点 import mian_jinriredian
from pythonProject.b百度上传付费文档.其他网站.众乐法先知.爬取众乐法先知 import mian_zhonglefaxianzhi
from pythonProject.b百度上传付费文档.其他网站.众学堂.爬取众学堂 import mian_zhongxuetang
from pythonProject.b百度上传付费文档.其他网站.优发表.爬取优发表 import mian_youfabiao
from pythonProject.b百度上传付费文档.其他网站.优文网.爬取优文网 import mian_unjs
from pythonProject.b百度上传付费文档.其他网站.优秀作文网.爬取优秀作文网 import mian_youxiuzuowenwang
from pythonProject.b百度上传付费文档.其他网站.会议精神网.爬取会议精神网 import mian_huiyijingshengwang
from pythonProject.b百度上传付费文档.其他网站.会述职范文.爬取会述职范文 import mian_huishuzhifanwen
from pythonProject.b百度上传付费文档.其他网站.作作文吧.爬取作文吧 import mian_zuozuowenba
from pythonProject.b百度上传付费文档.其他网站.作文吧.爬取作文吧 import mian_zuowen8
from pythonProject.b百度上传付费文档.其他网站.作文大全.爬取作文大全 import mian_zuowendaquan
from pythonProject.b百度上传付费文档.其他网站.作文库网.爬取作文库网 import mian_zuowenkuwang
from pythonProject.b百度上传付费文档.其他网站.作文网.爬取作文网 import mian_zuowenwang
from pythonProject.b百度上传付费文档.其他网站.佳文.爬取佳文 import mian_jiawen
from pythonProject.b百度上传付费文档.其他网站.免费学习网.爬取免费学习网 import mian_mianfeixuexiwang
from pythonProject.b百度上传付费文档.其他网站.公务员之家.爬取公务员之家 import mian_gongwuyuanzhijia
from pythonProject.b百度上传付费文档.其他网站.公务员期刊网.爬取公务员期刊网 import mian_gongwuyuanqikanwang
from pythonProject.b百度上传付费文档.其他网站.公文搜.爬取公文搜 import mian_gongwensou
from pythonProject.b百度上传付费文档.其他网站.公文驿站.爬取公文驿站 import mian_gongwenyizhan
from pythonProject.b百度上传付费文档.其他网站.写写帮文库.爬取写写帮文库 import mian_xiexiebang
from pythonProject.b百度上传付费文档.其他网站.写范文.爬取写范文 import mian_xiefanwen
from pythonProject.b百度上传付费文档.其他网站.出国留学网.爬取出国留学网 import mian_chuguoliuxuewang
from pythonProject.b百度上传付费文档.其他网站.创闻头条.爬取创闻头条 import mian_chuangwen
from pythonProject.b百度上传付费文档.其他网站.制度大全.爬取制度大全 import mian_zhidudaquan
from pythonProject.b百度上传付费文档.其他网站.励志一生.爬取励志一生 import mian_lizhiyisheng
from pythonProject.b百度上传付费文档.其他网站.励志一生网.爬取励志一生网 import mian_lizhiyishengwang
from pythonProject.b百度上传付费文档.其他网站.励志的句子.爬取励志的句子 import mian_lizhidejuzi
from pythonProject.b百度上传付费文档.其他网站.匠子生活.爬取匠子生活 import mian_jiangzishenghuo
from pythonProject.b百度上传付费文档.其他网站.十号范文网.爬取十号范文网 import mian_shihaofanwenwang
from pythonProject.b百度上传付费文档.其他网站.千学网.爬取千学网 import mian_qianxuewang
from pythonProject.b百度上传付费文档.其他网站.华南创作网.爬取华南创作网 import mian_huananchuangzuowang
from pythonProject.b百度上传付费文档.其他网站.华容文档网.爬取华容文档网 import mian_huarongwendangwang
from pythonProject.b百度上传付费文档.其他网站.华律.爬取华律 import mian_hualv
from pythonProject.b百度上传付费文档.其他网站.华鼎网.爬取华鼎网 import mian_huadingwang
from pythonProject.b百度上传付费文档.其他网站.卓卓文著作网.爬取卓文著作网 import mian_zhuowenzhuzuowang
from pythonProject.b百度上传付费文档.其他网站.南开网.爬取南开网 import mian_nankaiwang
from pythonProject.b百度上传付费文档.其他网站.发表之家.爬取发表之家 import mian_fabiaozhijia
from pythonProject.b百度上传付费文档.其他网站.古诗词网.爬取古诗词网 import mian_gushiciwang
from pythonProject.b百度上传付费文档.其他网站.句子吧.爬取句子吧 import mian_juziba
from pythonProject.b百度上传付费文档.其他网站.可意文档网.爬取可意文档网 import mian_keyiwendnagwang
from pythonProject.b百度上传付费文档.其他网站.同花顺财经.爬取同花顺财经 import mian_10jqka
from pythonProject.b百度上传付费文档.其他网站.品才.爬取品才 import mian_pincai
from pythonProject.b百度上传付费文档.其他网站.品读360.爬取品读360 import mian_pindu360
from pythonProject.b百度上传付费文档.其他网站.啦啦作文网.爬取啦啦作文网 import mian_lalazuowenwang
from pythonProject.b百度上传付费文档.其他网站.在线学习网.爬取在线学习网 import mian_zaixianxuexiwang
from pythonProject.b百度上传付费文档.其他网站.复来作文.爬取复来作文 import mian_fulaizuowen
from pythonProject.b百度上传付费文档.其他网站.大家都在问.爬取头条问答 import mian_toutiaowenda
from pythonProject.b百度上传付费文档.其他网站.大文斗范文网.爬取大文斗范文网 import miandawendou
from pythonProject.b百度上传付费文档.其他网站.大海范文网.爬取大海范文网 import mian_dahaifanwenwang
from pythonProject.b百度上传付费文档.其他网站.大牛论文网.爬取大牛论文网 import mian_daniulunwenwang
from pythonProject.b百度上传付费文档.其他网站.大风车网.爬取大风车网 import mian_dafengcewang
from pythonProject.b百度上传付费文档.其他网站.大黑猫文档.爬取大黑猫文档 import mian_daheimao
from pythonProject.b百度上传付费文档.其他网站.天穆网.爬取天穆网 import mian_tianmuwang
from pythonProject.b百度上传付费文档.其他网站.好总结.爬取好总结 import mian_haozongjie
from pythonProject.b百度上传付费文档.其他网站.好期刊.爬取好期刊 import mian_haoqikan
from pythonProject.b百度上传付费文档.其他网站.好老师范文网.爬取好老师范文网 import mian_haolaoshifanwenwang
from pythonProject.b百度上传付费文档.其他网站.好职网.爬取好职网 import mian_haozhiwang
from pythonProject.b百度上传付费文档.其他网站.好范文网.爬取好范文网 import mian_haofanwenwang
from pythonProject.b百度上传付费文档.其他网站.好运范文网.爬取好运范文网 import mian_haoyunfanwenwang
from pythonProject.b百度上传付费文档.其他网站.妈妈文摘.爬取妈妈文摘 import mian_mamawenzhai
from pythonProject.b百度上传付费文档.其他网站.妙文网.爬取妙文网 import mian_miaowenwang
from pythonProject.b百度上传付费文档.其他网站.学习啦.爬取学习啦 import mian_xuexila
from pythonProject.b百度上传付费文档.其他网站.学习网.爬取学习网 import mian_xuexiwang
from pythonProject.b百度上传付费文档.其他网站.学习论.爬取学习论 import mian_xuexilun
from pythonProject.b百度上传付费文档.其他网站.学识屋.爬取学识屋 import mian_xueshiwu
from pythonProject.b百度上传付费文档.其他网站.学道文库.爬取学道文库 import mian_xuedaowenku
from pythonProject.b百度上传付费文档.其他网站.学霸作文网.爬取学霸作文网 import mian_xuebazuowenwang
from pythonProject.b百度上传付费文档.其他网站.宇文网.爬取宇文网 import mian_yuwenwang
from pythonProject.b百度上传付费文档.其他网站.安全管理网.爬取安全管理网 import mian_anquanguanliwang
from pythonProject.b百度上传付费文档.其他网站.安瑞范文网.爬取安瑞范文网 import mian_anruifanwenwang
from pythonProject.b百度上传付费文档.其他网站.小文学.爬取小文学 import mian_xiaowenxue
from pythonProject.b百度上传付费文档.其他网站.小文学范文网.爬取小文学范文网 import mian_xiaowenxuefanwenwang
from pythonProject.b百度上传付费文档.其他网站.小编范文网.爬取小编范文网 import mian_xiaobianfanwenwang
from pythonProject.b百度上传付费文档.其他网站.小语范文网.爬取小语范文网 import mian_xiaoyufanwenwang
from pythonProject.b百度上传付费文档.其他网站.就爱读.爬取就爱读 import mian_jiuaidu
from pythonProject.b百度上传付费文档.其他网站.山崖发表网.爬取山崖发表网 import mian_shanyafabiaowang
from pythonProject.b百度上传付费文档.其他网站.山草香.爬取山草香 import mian_shancaoxiang
from pythonProject.b百度上传付费文档.其他网站.工作总结之家.爬取工作总结之家 import mian_gongzuozongjiezhijia
from pythonProject.b百度上传付费文档.其他网站.幼儿园学习网.爬取幼儿园学习网 import mian_youer
from pythonProject.b百度上传付费文档.其他网站.应届生毕业网.爬取应届生毕业网 import mian_yinjiesheng
from pythonProject.b百度上传付费文档.其他网站.开心文学网.爬取开心文学网 import mian_kaixinwenxuewang
from pythonProject.b百度上传付费文档.其他网站.律威百科.爬取律威百科 import mian_lvweibaike
from pythonProject.b百度上传付费文档.其他网站.律知分享.爬取律知分享 import mian_lvzhifenxiang
from pythonProject.b百度上传付费文档.其他网站.律科网.爬取律科网 import mian_lvkewang
from pythonProject.b百度上传付费文档.其他网站.微信公众平台.爬取微信公众平台 import mian_weixin
from pythonProject.b百度上传付费文档.其他网站.必读社.爬取必读社 import mian_bidushe
from pythonProject.b百度上传付费文档.其他网站.快思网.爬取快思网 import mian_kuaisiwang
from pythonProject.b百度上传付费文档.其他网站.思而思学.爬取思而思学 import mian_siersixue
from pythonProject.b百度上传付费文档.其他网站.惊鸿范文网.爬取惊鸿范文网 import mian_jinghongfanwenwang
from pythonProject.b百度上传付费文档.其他网站.懂得网.爬取懂得网 import mian_dongde
from pythonProject.b百度上传付费文档.其他网站.我优学习网.爬取我优学习网 import mian_woyouxuexiwang
from pythonProject.b百度上传付费文档.其他网站.我的公文网.爬取我的公文网 import mian_wodegongwenwang
from pythonProject.b百度上传付费文档.其他网站.我的网站.爬取我的网站 import mian_wodewangzhan
from pythonProject.b百度上传付费文档.其他网站.战马教育.爬取战马教育 import mian_zhanmajiaoyu
from pythonProject.b百度上传付费文档.其他网站.手艺活.爬取手艺活 import mian_shouyihuo
from pythonProject.b百度上传付费文档.其他网站.扬帆号.爬取扬帆号 import mian_yangfanhao
from pythonProject.b百度上传付费文档.其他网站.找总结网.爬取找总结网 import mian_zhaozongjiewang
from pythonProject.b百度上传付费文档.其他网站.找法网.爬取找法网 import mian_zhaofawang
from pythonProject.b百度上传付费文档.其他网站.拿第一.爬取拿第一 import mian_nadiyi
from pythonProject.b百度上传付费文档.其他网站.推文网.爬取推文网 import mian_tui555
from pythonProject.b百度上传付费文档.其他网站.搜狐网.爬取搜狐网 import mian_sohu
from pythonProject.b百度上传付费文档.其他网站.携笔同道.爬取携笔同道 import mian_xiebitongdao
from pythonProject.b百度上传付费文档.其他网站.摘抄网.爬取摘抄网 import mian_zaicaowang
from pythonProject.b百度上传付费文档.其他网站.撇呆范文网.爬取撇呆范文网 import mian_piedaifanwenwang
from pythonProject.b百度上传付费文档.其他网站.文书帮.爬取文书帮 import mian_wenshubang
from pythonProject.b百度上传付费文档.其他网站.文化知识网.爬取文化知识网 import mian_wenhuazhishiwang
from pythonProject.b百度上传付费文档.其他网站.文小秘.爬取文小秘 import mian_wenxiaomi
from pythonProject.b百度上传付费文档.其他网站.文秘学习网.爬取文秘学习网 import mian_wenmixuexiwang
from pythonProject.b百度上传付费文档.其他网站.文秘帮.爬取文秘帮 import mian_wenmi
from pythonProject.b百度上传付费文档.其他网站.文秘族.爬取文秘族 import mian_wenmiwang
from pythonProject.b百度上传付费文档.其他网站.文秘范文网.爬取文秘范文网 import mian_wenmifanwenwang
from pythonProject.b百度上传付费文档.其他网站.文秘范文网2.爬取文秘范文网 import mian_wenmifanwenwang2
from pythonProject.b百度上传付费文档.其他网站.新飞文库网.爬取新飞文库网 import mian_xinfeiwenkuwang
from pythonProject.b百度上传付费文档.其他网站.无忧文档网.爬取无忧文档网 import mian_wuyouwendangwang
from pythonProject.b百度上传付费文档.其他网站.无忧考网.爬取无忧考网 import mian_wuyoukaowang
from pythonProject.b百度上传付费文档.其他网站.无忧考试网.爬取无忧考试网 import mian_wuyoukaoshiwang
from pythonProject.b百度上传付费文档.其他网站.无锡英才网.无锡英才网 import mian_wuxiyingcaiwang
from pythonProject.b百度上传付费文档.其他网站.明月秘书网.爬取明月秘书网 import mian_mingyuemishu
from pythonProject.b百度上传付费文档.其他网站.春秋美文网.爬取春秋美文网 import mian_cunqiumeiwenwang
from pythonProject.b百度上传付费文档.其他网站.本地通.爬取本地通 import mian_benditong
from pythonProject.b百度上传付费文档.其他网站.朵花网.爬取朵花网 import mian_duohuawang
from pythonProject.b百度上传付费文档.其他网站.查字典.爬取查字典 import mian_chazidian
from pythonProject.b百度上传付费文档.其他网站.查查通作文网.爬取查查通作文网 import mian_chachatong
from pythonProject.b百度上传付费文档.其他网站.毕业生查重网.爬取毕业生查重网 import mian_biyeshengchachong
from pythonProject.b百度上传付费文档.其他网站.求是网.爬取求是网 import mian_qiushiwang
from pythonProject.b百度上传付费文档.其他网站.汇报书.爬取汇报书 import mian_huibaoshu
from pythonProject.b百度上传付费文档.其他网站.泉景微视.爬取泉景微视 import mian_quanjingweishi
from pythonProject.b百度上传付费文档.其他网站.泓景文档网.爬取泓景文档网 import mian_hongjingwendang
from pythonProject.b百度上传付费文档.其他网站.法律.爬取法律 import mian_falv
from pythonProject.b百度上传付费文档.其他网站.泡面作文.爬取泡面作文 import mian_paomianzuowen
from pythonProject.b百度上传付费文档.其他网站.源文鉴.爬取源文鉴 import mian_yuanwenjian
from pythonProject.b百度上传付费文档.其他网站.满分作文网.爬取满分作文网 import mian_manfenzuowen
from pythonProject.b百度上传付费文档.其他网站.演示站.爬取演示站 import mian_yanshizhan
from pythonProject.b百度上传付费文档.其他网站.澎湃新闻.爬取澎湃新闻 import mian_pengpai
from pythonProject.b百度上传付费文档.其他网站.点点范文网.爬取点点范文网 import mian_diandian
from pythonProject.b百度上传付费文档.其他网站.爬72励志.爬取72励志 import mian_72lizhi
from pythonProject.b百度上传付费文档.其他网站.爬取新东方.爬取新东方 import mian_xindongfang
from pythonProject.b百度上传付费文档.其他网站.爱作文.爬取爱作文 import mian_aizuowen
from pythonProject.b百度上传付费文档.其他网站.爱华文秘网.爬取爱华文秘网 import mian_aihuawenmiwang
from pythonProject.b百度上传付费文档.其他网站.爱商辅.爬取爱商辅 import mian_aishangfu
from pythonProject.b百度上传付费文档.其他网站.爱喜匠.爬取爱喜匠 import mian_aixijiang
from pythonProject.b百度上传付费文档.其他网站.爱学范文.爬取爱学范文 import mian_aixuefanwen
from pythonProject.b百度上传付费文档.其他网站.爱情经典.爬取爱情经典 import mian_aiqingjingdian
from pythonProject.b百度上传付费文档.其他网站.爱文网.爬取爱文网 import mian_aiwenwang
from pythonProject.b百度上传付费文档.其他网站.爱杨教育网.爬取爱杨教育网 import mian_aiyangjiaoyuwang
from pythonProject.b百度上传付费文档.其他网站.爱杨网.爬取爱杨网 import mian_aiyangwang
from pythonProject.b百度上传付费文档.其他网站.爱立教育网.爬取爱立教育网 import mian_ailijiaoyuwang
from pythonProject.b百度上传付费文档.其他网站.爱语录.爬取爱语录 import mian_aiyulu
from pythonProject.b百度上传付费文档.其他网站.爱问合同屋.爬取爱问合同屋 import mian_aiwenhetongwu
from pythonProject.b百度上传付费文档.其他网站.牛牛范文.爬取牛牛范文 import mian_niuniufanwen
from pythonProject.b百度上传付费文档.其他网站.瑞文网.爬取瑞文网 import mian_ruiwen
from pythonProject.b百度上传付费文档.其他网站.甜蜜生活.爬取甜蜜生活 import mian_tianmishenghuo
from pythonProject.b百度上传付费文档.其他网站.生动范文网.爬取生动范文网 import mian_shengdongfanwenwang
from pythonProject.b百度上传付费文档.其他网站.留学社区.爬取留学社区 import mian_liuxueshequ
from pythonProject.b百度上传付费文档.其他网站.白话文.爬取白话文 import mian_baihuawen
from pythonProject.b百度上传付费文档.其他网站.白领范文网.爬取白领范文网 import mian_bailingfanwenwang
from pythonProject.b百度上传付费文档.其他网站.百分文库.爬取百分文库 import mian_baifenwenku
from pythonProject.b百度上传付费文档.其他网站.百分英语.爬取百分英语 import mian_baifenyingyu
from pythonProject.b百度上传付费文档.其他网站.百味书屋.爬取百味书屋 import mian_baiweishuwu
from pythonProject.b百度上传付费文档.其他网站.百家公文网.爬取百家公文网 import mian_baijiagongwenwang
from pythonProject.b百度上传付费文档.其他网站.百家号.爬取百家号 import mian_baijiahao
from pythonProject.b百度上传付费文档.其他网站.百度文库.vip爬取百度文库 import mainBaiDuVip
from pythonProject.b百度上传付费文档.其他网站.百度文库.爬取百度文库 import main_other_baiDu
from pythonProject.b百度上传付费文档.其他网站.百度知道.爬取百度知道 import mian_baiduzhidao
from pythonProject.b百度上传付费文档.其他网站.百度经验.爬取百度经验 import mian_baidujingyan
from pythonProject.b百度上传付费文档.其他网站.百文网.爬取百文网 import mian_oh100
from pythonProject.b百度上传付费文档.其他网站.相依文档网.爬取相依文档网 import mian_xiangyiwendangwang
from pythonProject.b百度上传付费文档.其他网站.看点句子.爬取看点句子 import mian_kandianjuzi
from pythonProject.b百度上传付费文档.其他网站.知乎.爬取知乎 import mian_zhihu
from pythonProject.b百度上传付费文档.其他网站.知识网.爬取知识网 import mian_zhishiwang
from pythonProject.b百度上传付费文档.其他网站.短句子网.爬取短句子网 import mian_duanjuzi
from pythonProject.b百度上传付费文档.其他网站.短学网.爬取短学网 import mian_duanxuewang
from pythonProject.b百度上传付费文档.其他网站.短文学.爬取短文学 import mian_duanwenxue
from pythonProject.b百度上传付费文档.其他网站.短美文.爬取短美文 import mian_duanmeiwen
from pythonProject.b百度上传付费文档.其他网站.科普读物.爬取科普读物 import mian_kepuduwu
from pythonProject.b百度上传付费文档.其他网站.秒懂生活.爬取秒懂生活 import mian_miaodongshenghuo
from pythonProject.b百度上传付费文档.其他网站.童鞋会.爬取童鞋会 import mian_tongxiehui
from pythonProject.b百度上传付费文档.其他网站.竹海文档网.爬取竹海文档网 import mian_zhuhaiwendangwang
from pythonProject.b百度上传付费文档.其他网站.笔宝写作文库.爬取笔宝写作文库 import mian_bibaoxiezuowenku
from pythonProject.b百度上传付费文档.其他网站.笔芯范文网.爬取笔芯范文网 import mian_bixinfanwenwang
from pythonProject.b百度上传付费文档.其他网站.第一句子网.爬取第一句子网 import mian_diyijuziwang
from pythonProject.b百度上传付费文档.其他网站.第一文档网.爬取第一文档网 import mian_diyiwendang
from pythonProject.b百度上传付费文档.其他网站.第一范文网.爬取第一范文网 import mian_diyifanwen
from pythonProject.b百度上传付费文档.其他网站.第八区.爬取第八区 import mian_dibaqu
from pythonProject.b百度上传付费文档.其他网站.策划范文网.爬取策划范文网 import mian_cehuafanwenwang
from pythonProject.b百度上传付费文档.其他网站.简书.爬取简书 import mian_jianshu
from pythonProject.b百度上传付费文档.其他网站.简笔画.爬取简笔画 import mian_jianbihua
from pythonProject.b百度上传付费文档.其他网站.管理文库吧.爬取管理文库吧 import mian_guangliwenkuba
from pythonProject.b百度上传付费文档.其他网站.红袖范文网.爬取红袖范文网 import mian_hongxiufanwenwang
from pythonProject.b百度上传付费文档.其他网站.纵文网.爬取纵文网 import mian_zongwenwang
from pythonProject.b百度上传付费文档.其他网站.经典语录.爬取经典语录 import mian_jingdianyulu
from pythonProject.b百度上传付费文档.其他网站.经营范围网.爬取经营范围网 import mian_jingyingfanweiwang
from pythonProject.b百度上传付费文档.其他网站.经验本.爬取经验本 import mian_jingyanben
from pythonProject.b百度上传付费文档.其他网站.综合文库.爬取综合文库 import mian_zonghewenku
from pythonProject.b百度上传付费文档.其他网站.绿色作文网.爬取绿色作文网 import mian_lvsezuowenwang
from pythonProject.b百度上传付费文档.其他网站.网易.爬取网易 import mian_163
from pythonProject.b百度上传付费文档.其他网站.网易新闻.爬取网易新闻 import mian_news
from pythonProject.b百度上传付费文档.其他网站.美德网.爬取美德网 import mian_meidewang
from pythonProject.b百度上传付费文档.其他网站.美文欣赏网.爬取美文欣赏网 import mian_meiwenxinshangwang
from pythonProject.b百度上传付费文档.其他网站.美篇.爬取美篇 import mian_meipian
from pythonProject.b百度上传付费文档.其他网站.群走网.爬取群走网 import mian_qunzou
from pythonProject.b百度上传付费文档.其他网站.耀景文档网.爬取耀景文档网 import mian_yaojingwendangwang
from pythonProject.b百度上传付费文档.其他网站.考研秘籍网.爬取考研密集网 import mian_kaoyanmijiwang
from pythonProject.b百度上传付费文档.其他网站.考试学习网.爬取考试学习网 import mian_kaoshixuexiwang
from pythonProject.b百度上传付费文档.其他网站.考试帮手网.爬取考试帮手网 import mian_kaoshibangshouwang
from pythonProject.b百度上传付费文档.其他网站.职业学校招生网.爬取职业学校招生网 import mian_zhiyexuexiaozhaoshengwang
from pythonProject.b百度上传付费文档.其他网站.职场指南网.爬取职场指南网 import mian_zhichangzhinan
from pythonProject.b百度上传付费文档.其他网站.职场范文网.爬取职场范文网 import mian_zhichangfanwenwang
from pythonProject.b百度上传付费文档.其他网站.职校招生网.爬取职校招生网 import mian_zhixiaozhaoshengwang
from pythonProject.b百度上传付费文档.其他网站.聚优网.爬取聚优网 import mian_jy135
from pythonProject.b百度上传付费文档.其他网站.腾游文库.爬取腾游文库 import mian_tengyouwenku
from pythonProject.b百度上传付费文档.其他网站.腾讯新闻.爬取腾讯新闻 import mian_rainNew
from pythonProject.b百度上传付费文档.其他网站.腾讯网.爬取腾讯网 import mian_rain
from pythonProject.b百度上传付费文档.其他网站.自学站.爬取自学站 import mian_zixuewang
from pythonProject.b百度上传付费文档.其他网站.花都知识网.爬取花都知识网 import mian_huadouzhishiwang
from pythonProject.b百度上传付费文档.其他网站.范文先生网.爬取范文先生网 import mian_fwsir
from pythonProject.b百度上传付费文档.其他网站.范文参考网手机版.爬取范文参考网手机版 import mian_fanwencankaowangshouji
from pythonProject.b百度上传付费文档.其他网站.范文大全.爬取范文大全 import mian_fanwendaquan
from pythonProject.b百度上传付费文档.其他网站.范文大全2.爬取范文大全2 import mian_fanwendaquan2
from pythonProject.b百度上传付费文档.其他网站.范文帮.爬取范文帮 import mian_fanwenbang
from pythonProject.b百度上传付费文档.其他网站.范文社.爬取范文社 import mian_fanwenshe
from pythonProject.b百度上传付费文档.其他网站.范文网.爬取范文网 import mian_fanwenwang
from pythonProject.b百度上传付费文档.其他网站.范文资料网.爬取范文资料网 import mian_fanwenziliaowang
from pythonProject.b百度上传付费文档.其他网站.范文通.爬取范文通 import mian_fanwentong
from pythonProject.b百度上传付费文档.其他网站.范本网.爬取范本网 import mian_fanbenwang
from pythonProject.b百度上传付费文档.其他网站.草料作文网.爬取草料作文网 import mian_caoliaozuowenwang
from pythonProject.b百度上传付费文档.其他网站.草根科学网.爬取草根科学网 import mian_caogengkexuewang
from pythonProject.b百度上传付费文档.其他网站.萌宝文学.爬取萌宝文学 import mian_mengbaowenxue
from pythonProject.b百度上传付费文档.其他网站.蒲城资源.爬取蒲城资源 import mian_puchengziyuan
from pythonProject.b百度上传付费文档.其他网站.蓬勃范文网.爬取蓬勃范文网 import mian_pengbofanwenwang
from pythonProject.b百度上传付费文档.其他网站.虎知道.爬取虎知道 import mian_huzhidao
from pythonProject.b百度上传付费文档.其他网站.蜗牛文章网.爬取蜗牛文章网 import mian_woniuwenzhang
from pythonProject.b百度上传付费文档.其他网站.蜜桃圈.爬取蜜桃圈 import mian_mitaoquan
from pythonProject.b百度上传付费文档.其他网站.规模知识网.爬取规模知识网 import mian_guimozhishiwang
from pythonProject.b百度上传付费文档.其他网站.试卷.爬取试卷 import mian_shijuan
from pythonProject.b百度上传付费文档.其他网站.诗词吾爱网.爬取诗词吾爱网 import mian_shiciwuai
from pythonProject.b百度上传付费文档.其他网站.语录网.爬取语录网 import mian_yulu
from pythonProject.b百度上传付费文档.其他网站.语文君.爬取语文君 import mian_yuwenjun
from pythonProject.b百度上传付费文档.其他网站.语文迷.爬取语文迷 import mian_yuwenmi
from pythonProject.b百度上传付费文档.其他网站.读书吧.爬取读书吧 import mian_dushuba
from pythonProject.b百度上传付费文档.其他网站.读后感.爬取读后感 import mian_duhougan
from pythonProject.b百度上传付费文档.其他网站.课件范文网.爬取课件范文网 import mian_kejianfanwenwang
from pythonProject.b百度上传付费文档.其他网站.课堂作文网.爬取课堂作文网 import mian_kt250
from pythonProject.b百度上传付费文档.其他网站.贝贝文库.爬取贝贝文库 import mian_beibeiwenku
from pythonProject.b百度上传付费文档.其他网站.财经新闻周刊.爬取财经新闻周刊 import mian_caijingxinwenzhoukan
from pythonProject.b百度上传付费文档.其他网站.贤学网.爬取贤学网 import mian_xianxuewang
from pythonProject.b百度上传付费文档.其他网站.趣小七.爬取趣小七 import mian_quxiaoqi
from pythonProject.b百度上传付费文档.其他网站.趣文库.爬取趣文库 import mian_quwenku
from pythonProject.b百度上传付费文档.其他网站.趣祝福.爬取趣祝福 import mian_quzhufu
from pythonProject.b百度上传付费文档.其他网站.酷猫写作.爬取酷猫写作 import mian_kumao
from pythonProject.b百度上传付费文档.其他网站.闪亮儿童网.爬取闪亮儿童网 import mian_shanliangertongwang
from pythonProject.b百度上传付费文档.其他网站.问一问.爬取问一问 import mian_question
from pythonProject.b百度上传付费文档.其他网站.随笔网.爬取随笔网 import mian_suibiwang
from pythonProject.b百度上传付费文档.其他网站.零二七艺考.爬取零二七艺考 import mian_lingerqiyikao
from pythonProject.b百度上传付费文档.其他网站.零思考方案网.爬取零思考方案网 import mian_lingsikaofanganwang
from pythonProject.b百度上传付费文档.其他网站.零零作文网.爬取零零作文网 import mian_linglingzuowenwang
from pythonProject.b百度上传付费文档.其他网站.非常励志网.爬取励志网 import mian_feichanglizhiwang
from pythonProject.b百度上传付费文档.其他网站.非常好.爬取非常好 import mian_feichanghao
from pythonProject.b百度上传付费文档.其他网站.顺风文档网.爬取顺丰文档网 import mian_sunfengwendnagwang
from pythonProject.b百度上传付费文档.其他网站.题问鸭.爬取题问鸭 import mian_tiwenya
from pythonProject.b百度上传付费文档.其他网站.风知文章.爬取风知文章 import mian_fengzhiwenzhang
from pythonProject.b百度上传付费文档.其他网站.飞外网.爬取飞外网 import mian_feiwaiwang
from pythonProject.b百度上传付费文档.其他网站.飞速成语.爬取飞速成语 import mian_feisuchengyu
from pythonProject.b百度上传付费文档.其他网站.驾驶员考试.爬取驾驶员考试 import mian_jiashiyuankaoshi
from pythonProject.b百度上传付费文档.其他网站.高三网.爬取高三网 import mian_senior
from pythonProject.b百度上传付费文档.其他网站.鬼故事网.爬取鬼故事网 import mian_guigushiwang

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)







def  baiduchociesWebsitePlay( folderPath, keyStr, urls,hrefUrl):
    panduan=False
    # if 'wenku.baidu.com' in hrefUrl:
    #     try:
    #         title = driver.find_element(By.XPATH,
    #                                     '//div[@class="doc-title normal-screen-mode normal-screen-mode"]').text
    #     except:
    #         title = keyStr
    #     htmlPath = folderPath + '\\' + '百度文库'
    #     makeFolder(htmlPath)
    #     # mainBaiDuVip(htmlPath, hrefUrl, keyStr, title)
    #     main_other_baiDu(htmlPath, hrefUrl, keyStr, title)
    #     
    #     panduan=True
    if 'unjs.com' in hrefUrl:
        print('优文网', hrefUrl)
        htmlPath = folderPath + '\\' + '优文网'
        makeFolder(htmlPath)
        title = ''
        mian_unjs(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zuowen8.com' in hrefUrl:
        print('作作文吧', hrefUrl)
        htmlPath = folderPath + '\\' + '作作文吧'
        makeFolder(htmlPath)
        title = ''
        mian_zuowen8(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '10jqka.com.cn' in hrefUrl:
        print('同花顺财经', hrefUrl)
        htmlPath = folderPath + '\\' + '同花顺财经'
        makeFolder(htmlPath)
        title = ''
        mian_10jqka(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'dawendou.com' in hrefUrl:
        print('大文斗范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '大文斗范文网'
        makeFolder(htmlPath)
        title = ''
        miandawendou(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xuexila.com' in hrefUrl:
        print('学习啦', hrefUrl)
        htmlPath = folderPath + '\\' + '学习啦'
        makeFolder(htmlPath)
        title = ''
        mian_xuexila(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yjbys.com' in hrefUrl:
        print('应届生毕业网', hrefUrl)
        htmlPath = folderPath + '\\' + '应届生毕业网'
        makeFolder(htmlPath)
        title = ''
        mian_yinjiesheng(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    # elif 'weixin.qq.com' in hrefUrl:
    #     print('微信公众平台', hrefUrl)
    #     htmlPath = folderPath + '\\' + '微信公众平台'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_weixin(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    # elif 'sohu.com' in hrefUrl:
    #     print('搜狐网', hrefUrl)
    #     htmlPath = folderPath + '\\' + '搜狐网'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_sohu(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    elif 'wenshubang.com' in hrefUrl:
        print('文书帮', hrefUrl)
        htmlPath = folderPath + '\\' + '文书帮'
        makeFolder(htmlPath)
        title = ''
        mian_wenshubang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'chazidian.com' in hrefUrl:
        print('查字典', hrefUrl)
        htmlPath = folderPath + '\\' + '查字典'
        makeFolder(htmlPath)
        title = ''
        mian_chazidian(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ruiwen.com' in hrefUrl:
        print('瑞文网', hrefUrl)
        htmlPath = folderPath + '\\' + '瑞文网'
        makeFolder(htmlPath)
        title = ''
        mian_ruiwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'oh100.com' in hrefUrl:
        print('百文网', hrefUrl)
        htmlPath = folderPath + '\\' + '百文网'
        makeFolder(htmlPath)
        title = ''
        mian_oh100(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    # elif 'zhihu.com' in hrefUrl:
    #     print('知乎', hrefUrl)
    #     htmlPath = folderPath + '\\' + '知乎'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_zhihu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'diyifanwen.com' in hrefUrl:
        print('第一范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '第一范文网'
        makeFolder(htmlPath)
        title = ''
        mian_diyifanwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jianshu.com' in hrefUrl:
        print('简书社区', hrefUrl)
        htmlPath = folderPath + '\\' + '简书'
        makeFolder(htmlPath)
        title = ''
        mian_jianshu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    # elif 'news.163.com' in hrefUrl:
    #     print('网易新闻', hrefUrl)
    #     htmlPath = folderPath + '\\' + '网易新闻'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_news(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    # elif '163.com' in hrefUrl:
    #     print('网易', hrefUrl)
    #     htmlPath = folderPath + '\\' + '网易'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_163(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    elif 'qunzou.com' in hrefUrl:
        print('群走网', hrefUrl)
        htmlPath = folderPath + '\\' + '群走网'
        makeFolder(htmlPath)
        title = ''
        mian_qunzou(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jy135.com' in hrefUrl:
        print('聚优网', hrefUrl)
        htmlPath = folderPath + '\\' + '聚优网'
        makeFolder(htmlPath)
        title = ''
        mian_jy135(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    # elif 'new.qq.com' in hrefUrl:
    #     print('腾讯新闻', hrefUrl)
    #     htmlPath = folderPath + '\\' + '腾讯新闻'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_rainNew(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    # elif 'qq.com' in hrefUrl:
    #     print('腾讯网', hrefUrl)
    #     htmlPath = folderPath + '\\' + '腾讯网'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_rain(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    elif 'fwsir.com' in hrefUrl:
        print('范文先生网', hrefUrl)
        htmlPath = folderPath + '\\' + '范文先生网'
        makeFolder(htmlPath)
        title = ''
        mian_fwsir(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yuwenmi.com' in hrefUrl:
        print('语文迷', hrefUrl)
        htmlPath = folderPath + '\\' + '语文迷'
        makeFolder(htmlPath)
        title = ''
        mian_yuwenmi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'kt250.com' in hrefUrl:
        print('课堂作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '课堂作文网'
        makeFolder(htmlPath)
        title = ''
        mian_kt250(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wen.baidu.com/question' in hrefUrl:
        print('b百度问一问', hrefUrl)
        htmlPath = folderPath + '\\' + 'b百度问一问'
        makeFolder(htmlPath)
        title = ''
        mian_question(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '6231188.com' in hrefUrl:
        print('五六范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '五六范文网'
        makeFolder(htmlPath)
        title = ''
        mian_wuliu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yjbys.com' in hrefUrl:
        print('应届毕业生网', hrefUrl)
        htmlPath = folderPath + '\\' + '应届毕业生网'
        makeFolder(htmlPath)
        title = ''
        mian_yinjiesheng(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'qinzibuy.com' in hrefUrl:
        print('幼儿园学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '幼儿园学习网'
        makeFolder(htmlPath)
        title = ''
        mian_youer(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gaosan.com' in hrefUrl:
        print('高三网', hrefUrl)
        htmlPath = folderPath + '\\' + '高三网'
        makeFolder(htmlPath)
        title = ''
        mian_senior(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    # elif 'baijiahao.baidu.com' in hrefUrl:
    #     print('百家号', hrefUrl)
    #     htmlPath = folderPath + '\\' + '百家号'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_baijiahao(hrefUrl, htmlPath, keyStr, title)
    #     
    #     panduan = True
        # colseDriver(driver)
    elif 'meipian.cn' in hrefUrl:
        print('美篇', hrefUrl)
        htmlPath = folderPath + '\\' + '美篇'
        makeFolder(htmlPath)
        title = ''
        mian_meipian(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xiexiebang.com' in hrefUrl:
        print('写写帮文库', hrefUrl)
        htmlPath = folderPath + '\\' + '写写帮文库'
        makeFolder(htmlPath)
        title = ''
        mian_xiexiebang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'chinansn.com' in hrefUrl:
        print('语录网', hrefUrl)
        htmlPath = folderPath + '\\' + '语录网'
        makeFolder(htmlPath)
        title = ''
        mian_yulu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'cwtea.net' in hrefUrl:
        print('创闻头条', hrefUrl)
        htmlPath = folderPath + '\\' + '创闻头条'
        makeFolder(htmlPath)
        title = ''
        mian_chuangwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'caibaojian.com/' in hrefUrl:
        print('点点范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '点点范文网'
        makeFolder(htmlPath)
        title = ''
        mian_diandian(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zhidao.baidu.com' in hrefUrl:
        print('百度知道', hrefUrl)
        htmlPath = folderPath + '\\' + '百度知道'
        makeFolder(htmlPath)
        title = ''
        mian_baiduzhidao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yiyyy.com' in hrefUrl:
        print('经典语录', hrefUrl)
        htmlPath = folderPath + '\\' + '经典语录'
        makeFolder(htmlPath)
        title = ''
        mian_jingdianyulu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    # elif 'thepaper.cn' in hrefUrl:
    #     print('澎湃新闻', hrefUrl)
    #     htmlPath = folderPath + '\\' + '澎湃新闻'
    #     makeFolder(htmlPath)
    #     title = ''
    #     mian_pengpai(hrefUrl, htmlPath, keyStr, title)
    #
    #     panduan = True
    #     # colseDriver(driver)
    elif 'xgjdsc.com' in hrefUrl:
        print('泓景文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '泓景文档网'
        makeFolder(htmlPath)
        title = ''
        mian_hongjingwendang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'vkandian.cn' in hrefUrl:
        print('看点句子', hrefUrl)
        htmlPath = folderPath + '\\' + '看点句子'
        makeFolder(htmlPath)
        title = ''
        mian_kandianjuzi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '5068.com' in hrefUrl:
        print('五零六八教学资源网', hrefUrl)
        htmlPath = folderPath + '\\' + '五零六八教学资源网'
        makeFolder(htmlPath)
        title = ''
        mian_5068jiaoxueziyuan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fzlbar.com' in hrefUrl:
        print('风知文章', hrefUrl)
        htmlPath = folderPath + '\\' + '风知文章'
        makeFolder(htmlPath)
        title = ''
        mian_fengzhiwenzhang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'duanmeiwen.com' in hrefUrl:
        print('短美文', hrefUrl)
        htmlPath = folderPath + '\\' + '短美文'
        makeFolder(htmlPath)
        title = ''
        mian_duanmeiwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'duhougan.net' in hrefUrl:
        print('读后感', hrefUrl)
        htmlPath = folderPath + '\\' + '读后感'
        makeFolder(htmlPath)
        title = ''
        mian_duhougan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fulay.cn' in hrefUrl:
        print('复来作文', hrefUrl)
        htmlPath = folderPath + '\\' + '复来作文'
        makeFolder(htmlPath)
        title = ''
        mian_fulaizuowen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gerenjianli.cn' in hrefUrl:
        print('个人简历', hrefUrl)
        htmlPath = folderPath + '\\' + '个人简历'
        makeFolder(htmlPath)
        title = ''
        mian_gerenjianli(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yebaike.com' in hrefUrl:
        print('业百科', hrefUrl)
        htmlPath = folderPath + '\\' + '业百科'
        makeFolder(htmlPath)
        title = ''
        mian_yebaike(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xianxue.com' in hrefUrl:
        print('贤学网', hrefUrl)
        htmlPath = folderPath + '\\' + '贤学网'
        makeFolder(htmlPath)
        title = ''
        mian_xianxuewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'eastca.com.cn' in hrefUrl:
        print('东方文库', hrefUrl)
        htmlPath = folderPath + '\\' + '东方文库'
        makeFolder(htmlPath)
        title = ''
        mian_dongfangwenku(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zhaozongjie.com' in hrefUrl:
        print('找总结网', hrefUrl)
        htmlPath = folderPath + '\\' + '找总结网'
        makeFolder(htmlPath)
        title = ''
        mian_zhaozongjiewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'qiuxueshe.com' in hrefUrl:
        print('范文社', hrefUrl)
        htmlPath = folderPath + '\\' + '范文社'
        makeFolder(htmlPath)
        title = ''
        mian_fanwenshe(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'rubber-label.com' in hrefUrl:
        print('爱杨教育网', hrefUrl)
        htmlPath = folderPath + '\\' + '爱杨教育网'
        makeFolder(htmlPath)
        title = ''
        mian_aiyangjiaoyuwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'huxinfoam.com' in hrefUrl:
        print('互信范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '互信范文网'
        makeFolder(htmlPath)
        title = ''
        mian_huxinfanwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zuowenwang.net' in hrefUrl:
        print('满分作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '满分作文网'
        makeFolder(htmlPath)
        title = ''
        mian_manfenzuowen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zf133.com' in hrefUrl:
        print('趣祝福', hrefUrl)
        htmlPath = folderPath + '\\' + '趣祝福'
        makeFolder(htmlPath)
        title = ''
        mian_quzhufu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'tongxiehui.net' in hrefUrl:
        print('童鞋会', hrefUrl)
        htmlPath = folderPath + '\\' + '童鞋会'
        makeFolder(htmlPath)
        title = ''
        mian_tongxiehui(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'duyou8.com' in hrefUrl:
        print('读书吧', hrefUrl)
        htmlPath = folderPath + '\\' + '读书吧'
        makeFolder(htmlPath)
        title = ''
        mian_dushuba(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'tui555.com' in hrefUrl:
        print('推文网', hrefUrl)
        htmlPath = folderPath + '\\' + '推文网'
        makeFolder(htmlPath)
        title = ''
        mian_tui555(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '027art.com' in hrefUrl:
        print('零二七艺考', hrefUrl)
        htmlPath = folderPath + '\\' + '零二七艺考'
        makeFolder(htmlPath)
        title = ''
        mian_lingerqiyikao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'search_wenda_pc' in hrefUrl:
        print('头条问答', hrefUrl)
        htmlPath = folderPath + '\\' + '头条问答'
        makeFolder(htmlPath)
        title = ''
        mian_toutiaowenda(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '72by.cn' in hrefUrl:
        print('爬72励志', hrefUrl)
        htmlPath = folderPath + '\\' + '爬72励志'
        makeFolder(htmlPath)
        title = ''
        mian_72lizhi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xuexiaodaquan.com' in hrefUrl:
        print('作文大全', hrefUrl)
        htmlPath = folderPath + '\\' + '作文大全'
        makeFolder(htmlPath)
        title = ''
        mian_zuowendaquan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jiasudai.com' in hrefUrl:
        print('我的网站', hrefUrl)
        htmlPath = folderPath + '\\' + '我的网站'
        makeFolder(htmlPath)
        title = ''
        mian_wodewangzhan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'bidushe.cn' in hrefUrl:
        print('必读社', hrefUrl)
        htmlPath = folderPath + '\\' + '必读社'
        makeFolder(htmlPath)
        title = ''
        mian_bidushe(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'idongde.com' in hrefUrl:
        print('懂得网', hrefUrl)
        htmlPath = folderPath + '\\' + '懂得网'
        makeFolder(htmlPath)
        title = ''
        mian_dongde(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'juziba.cn' in hrefUrl:
        print('句子吧', hrefUrl)
        htmlPath = folderPath + '\\' + '句子吧'
        makeFolder(htmlPath)
        title = ''
        mian_juziba(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'toutiao.com/article' in hrefUrl:
        print('今日头条', hrefUrl)
        htmlPath = folderPath + '\\' + '今日头条'
        makeFolder(htmlPath)
        title = ''
        mian_jinritoutiao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'duanwenxue.com' in hrefUrl:
        print('短学网', hrefUrl)
        htmlPath = folderPath + '\\' + '短学网'
        makeFolder(htmlPath)
        title = ''
        mian_duanxuewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zhongkao.com' in hrefUrl:
        print('中考网', hrefUrl)
        htmlPath = folderPath + '\\' + '中考网'
        makeFolder(htmlPath)
        title = ''
        mian_zhonkaowang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '52shici.com' in hrefUrl:
        print('诗词吾爱网', hrefUrl)
        htmlPath = folderPath + '\\' + '诗词吾爱网'
        makeFolder(htmlPath)
        title = ''
        mian_shiciwuai(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '00zuowen.com' in hrefUrl:
        print('零零作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '零零作文网'
        makeFolder(htmlPath)
        title = ''
        mian_linglingzuowenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '66law.cn' in hrefUrl:
        print('华律', hrefUrl)
        htmlPath = folderPath + '\\' + '华律'
        makeFolder(htmlPath)
        title = ''
        mian_hualv(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'smtxjs.com' in hrefUrl:
        print('闪亮儿童网', hrefUrl)
        htmlPath = folderPath + '\\' + '闪亮儿童网'
        makeFolder(htmlPath)
        title = ''
        mian_shanliangertongwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jxscct.com' in hrefUrl:
        print('查查通作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '查查通作文网'
        makeFolder(htmlPath)
        title = ''
        mian_chachatong(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'findlaw.cn' in hrefUrl:
        print('找法网', hrefUrl)
        htmlPath = folderPath + '\\' + '找法网'
        makeFolder(htmlPath)
        title = ''
        mian_zhaofawang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'haoword.com' in hrefUrl:
        print('好范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '好范文网'
        makeFolder(htmlPath)
        title = ''
        mian_haofanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'leha.com' in hrefUrl:
        print('乐哈文库', hrefUrl)
        htmlPath = folderPath + '\\' + '乐哈文库'
        makeFolder(htmlPath)
        title = ''
        mian_lehawenku(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'diyijuzi.com' in hrefUrl:
        print('第一句子网', hrefUrl)
        htmlPath = folderPath + '\\' + '第一句子网'
        makeFolder(htmlPath)
        title = ''
        mian_diyijuziwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'j458.com' in hrefUrl:
        print('励志的句子', hrefUrl)
        htmlPath = folderPath + '\\' + '励志的句子'
        makeFolder(htmlPath)
        title = ''
        mian_lizhidejuzi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ppzuowen.com' in hrefUrl:
        print('pp作文网', hrefUrl)
        htmlPath = folderPath + '\\' + 'pp作文网'
        makeFolder(htmlPath)
        title = ''
        mian_ppzuowenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'duanwenxue.com' in hrefUrl:
        print('短文学', hrefUrl)
        htmlPath = folderPath + '\\' + '短文学'
        makeFolder(htmlPath)
        title = ''
        mian_duanwenxue(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fanzidian.com' in hrefUrl:
        print('fanzidian', hrefUrl)
        htmlPath = folderPath + '\\' + 'fanzidian'
        makeFolder(htmlPath)
        title = ''
        mian_fanzidian(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '0279.net' in hrefUrl:
        print('绿色作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '绿色作文网'
        makeFolder(htmlPath)
        title = ''
        mian_lvsezuowenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zuowen.net' in hrefUrl:
        print('作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '作文网'
        makeFolder(htmlPath)
        title = ''
        mian_zuowenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'liuxue86.com' in hrefUrl:
        print('出国留学网', hrefUrl)
        htmlPath = folderPath + '\\' + '出国留学网'
        makeFolder(htmlPath)
        title = ''
        mian_chuguoliuxuewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'piedai.com' in hrefUrl:
        print('撇呆范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '撇呆范文网'
        makeFolder(htmlPath)
        title = ''
        mian_piedaifanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'qstheory.cn' in hrefUrl:
        print('求是网', hrefUrl)
        htmlPath = folderPath + '\\' + '求是网'
        makeFolder(htmlPath)
        title = ''
        mian_qiushiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ruyile.com' in hrefUrl:
        print('试卷', hrefUrl)
        htmlPath = folderPath + '\\' + '试卷'
        makeFolder(htmlPath)
        title = ''
        mian_shijuan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '5isxw.com' in hrefUrl:
        print('职校招生网', hrefUrl)
        htmlPath = folderPath + '\\' + '职校招生网'
        makeFolder(htmlPath)
        title = ''
        mian_zhixiaozhaoshengwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wz321.cn' in hrefUrl:
        print('华鼎网', hrefUrl)
        htmlPath = folderPath + '\\' + '华鼎网'
        makeFolder(htmlPath)
        title = ''
        mian_huadingwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'banzhuren.cn' in hrefUrl:
        print('快思网', hrefUrl)
        htmlPath = folderPath + '\\' + '快思网'
        makeFolder(htmlPath)
        title = ''
        mian_kuaisiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '99zuowen.com' in hrefUrl:
        print('九九作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '九九作文网'
        makeFolder(htmlPath)
        title = ''
        mian_99zuowenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wenmi.com' in hrefUrl:
        print('文秘帮', hrefUrl)
        htmlPath = folderPath + '\\' + '文秘帮'
        makeFolder(htmlPath)
        title = ''
        mian_wenmi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'koomao.com' in hrefUrl:
        print('酷猫写作', hrefUrl)
        htmlPath = folderPath + '\\' + '酷猫写作'
        makeFolder(htmlPath)
        title = ''
        mian_kumao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wenxm.cn' in hrefUrl:
        print('文小秘', hrefUrl)
        htmlPath = folderPath + '\\' + '文小秘'
        makeFolder(htmlPath)
        title = ''
        mian_wenxiaomi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wenda.so.com' in hrefUrl:
        print('360问答', hrefUrl)
        htmlPath = folderPath + '\\' + '360问答'
        makeFolder(htmlPath)
        title = ''
        mian_360wenda(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'pindu360.com' in hrefUrl:
        print('品读360', hrefUrl)
        htmlPath = folderPath + '\\' + '品读360'
        makeFolder(htmlPath)
        title = ''
        mian_pindu360(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '17cizhi.com' in hrefUrl:
        print('17辞职网', hrefUrl)
        htmlPath = folderPath + '\\' + '17辞职网'
        makeFolder(htmlPath)
        title = ''
        mian_17cizhiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'duanjuzi.com' in hrefUrl:
        print('短句子网', hrefUrl)
        htmlPath = folderPath + '\\' + '短句子网'
        makeFolder(htmlPath)
        title = ''
        mian_duanjuzi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'cs.xdf.cn' in hrefUrl:
        print('新东方', hrefUrl)
        htmlPath = folderPath + '\\' + '新东方'
        makeFolder(htmlPath)
        title = ''
        mian_xindongfang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'w.51tietu.net' in hrefUrl:
        print('五一贴图网', hrefUrl)
        htmlPath = folderPath + '\\' + '五一贴图网'
        makeFolder(htmlPath)
        title = ''
        mian_51tietu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'lcseo.cn' in hrefUrl:
        print('语文君', hrefUrl)
        htmlPath = folderPath + '\\' + '语文君'
        makeFolder(htmlPath)
        title = ''
        mian_yuwenjun(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xfanwen.cn' in hrefUrl:
        print('写范文', hrefUrl)
        htmlPath = folderPath + '\\' + '写范文'
        makeFolder(htmlPath)
        title = ''
        mian_xiefanwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'hnbllw.com' in hrefUrl:
        print('蜗牛文摘网', hrefUrl)
        htmlPath = folderPath + '\\' + '蜗牛文摘网'
        makeFolder(htmlPath)
        title = ''
        mian_woniuwenzhang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'hnbllw.com' in hrefUrl:
        print('蜗牛文摘网', hrefUrl)
        htmlPath = folderPath + '\\' + '蜗牛文摘网'
        makeFolder(htmlPath)
        title = ''
        mian_woniuwenzhang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'woyoujk.com' in hrefUrl:
        print('我优学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '我优学习网'
        makeFolder(htmlPath)
        title = ''
        mian_woyouxuexiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jingyan.baidu.com' in hrefUrl:
        print('百度经验', hrefUrl)
        htmlPath = folderPath + '\\' + '百度经验'
        makeFolder(htmlPath)
        title = ''
        mian_baidujingyan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'm.xiaowenxue.com' in hrefUrl:
        print('小文学', hrefUrl)
        htmlPath = folderPath + '\\' + '小文学'
        makeFolder(htmlPath)
        title = ''
        mian_xiaowenxue(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'n77.org' in hrefUrl:
        print('七七范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '七七范文网'
        makeFolder(htmlPath)
        title = ''
        mian_qiqifanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'pptok.com' in hrefUrl:
        print('PPTOK', hrefUrl)
        htmlPath = folderPath + '\\' + 'PPTOK'
        makeFolder(htmlPath)
        title = ''
        mian_pptok(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ahsrst.cn' in hrefUrl:
        print('范文资料网', hrefUrl)
        htmlPath = folderPath + '\\' + '范文资料网'
        makeFolder(htmlPath)
        title = ''
        mian_fanwenziliaowang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'windowchina.cn' in hrefUrl:
        print('文秘范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '文秘范文网'
        makeFolder(htmlPath)
        title = ''
        mian_wenmifanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gxscse.com' in hrefUrl:
        print('思而思学', hrefUrl)
        htmlPath = folderPath + '\\' + '思而思学'
        makeFolder(htmlPath)
        title = ''
        mian_siersixue(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'daheimao.com' in hrefUrl:
        print('大黑猫文档', hrefUrl)
        htmlPath = folderPath + '\\' + '大黑猫文档'
        makeFolder(htmlPath)
        title = ''
        mian_daheimao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '03kkk.com' in hrefUrl:
        print('零思考方案网', hrefUrl)
        htmlPath = folderPath + '\\' + '零思考方案网'
        makeFolder(htmlPath)
        title = ''
        mian_lingsikaofanganwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'baihuawen.cn' in hrefUrl:
        print('白话文', hrefUrl)
        htmlPath = folderPath + '\\' + '白话文'
        makeFolder(htmlPath)
        title = ''
        mian_baihuawen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '985ks.com' in hrefUrl:
        print('985考试网', hrefUrl)
        htmlPath = folderPath + '\\' + '985考试网'
        makeFolder(htmlPath)
        title = ''
        mian_985kaoshiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '985ks.com' in hrefUrl:
        print('985考试网', hrefUrl)
        htmlPath = folderPath + '\\' + '985考试网'
        makeFolder(htmlPath)
        title = ''
        mian_985kaoshiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ylwt22.com' in hrefUrl:
        print('十号范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '十号范文网'
        makeFolder(htmlPath)
        title = ''
        mian_shihaofanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'kelax.cn' in hrefUrl:
        print('简笔画', hrefUrl)
        htmlPath = folderPath + '\\' + '简笔画'
        makeFolder(htmlPath)
        title = ''
        mian_jianbihua(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'feedwhy.com' in hrefUrl:
        print('飞外网', hrefUrl)
        htmlPath = folderPath + '\\' + '飞外网'
        makeFolder(htmlPath)
        title = ''
        mian_feiwaiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fwwang.cn' in hrefUrl:
        print('范文参考网手机版', hrefUrl)
        htmlPath = folderPath + '\\' + '范文参考网手机版'
        makeFolder(htmlPath)
        title = ''
        mian_fanwencankaowangshouji(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zmbg.com' in hrefUrl:
        print('战马教育', hrefUrl)
        htmlPath = folderPath + '\\' + '战马教育'
        makeFolder(htmlPath)
        title = ''
        mian_zhanmajiaoyu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zmbg.com' in hrefUrl:
        print('战马教育', hrefUrl)
        htmlPath = folderPath + '\\' + '战马教育'
        makeFolder(htmlPath)
        title = ''
        mian_zhanmajiaoyu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'help315.com.cn' in hrefUrl:
        print('考试帮手网', hrefUrl)
        htmlPath = folderPath + '\\' + '考试帮手网'
        makeFolder(htmlPath)
        title = ''
        mian_kaoshibangshouwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '07nd.com' in hrefUrl:
        print('妙文网', hrefUrl)
        htmlPath = folderPath + '\\' + '妙文网'
        makeFolder(htmlPath)
        title = ''
        mian_miaowenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'haojob123.com' in hrefUrl:
        print('好职网', hrefUrl)
        htmlPath = folderPath + '\\' + '好职网'
        makeFolder(htmlPath)
        title = ''
        mian_haozhiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'aizwen.com' in hrefUrl:
        print('爱作文', hrefUrl)
        htmlPath = folderPath + '\\' + '爱作文'
        makeFolder(htmlPath)
        title = ''
        mian_aizuowen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'paihui8.com' in hrefUrl:
        print('课件范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '课件范文网'
        makeFolder(htmlPath)
        title = ''
        mian_kejianfanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wxycw.com' in hrefUrl:
        print('无锡英才网', hrefUrl)
        htmlPath = folderPath + '\\' + '无锡英才网'
        makeFolder(htmlPath)
        title = ''
        mian_wuxiyingcaiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'sheyinghouqi.com' in hrefUrl:
        print('明月秘书网', hrefUrl)
        htmlPath = folderPath + '\\' + '明月秘书网'
        makeFolder(htmlPath)
        title = ''
        mian_mingyuemishu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ylwt22.com' in hrefUrl:
        print('会议精神网', hrefUrl)
        htmlPath = folderPath + '\\' + '会议精神网'
        makeFolder(htmlPath)
        title = ''
        mian_huiyijingshengwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zixuezhan.cn' in hrefUrl:
        print('自学站', hrefUrl)
        htmlPath = folderPath + '\\' + '自学站'
        makeFolder(htmlPath)
        title = ''
        mian_zixuewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'cnrencai.com' in hrefUrl:
        print('职场指南网', hrefUrl)
        htmlPath = folderPath + '\\' + '职场指南网'
        makeFolder(htmlPath)
        title = ''
        mian_zhichangzhinan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'mengbao.aipingxiang.com' in hrefUrl:
        print('萌宝文学', hrefUrl)
        htmlPath = folderPath + '\\' + '萌宝文学'
        makeFolder(htmlPath)
        title = ''
        mian_mengbaowenxue(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zuoyezhan.com' in hrefUrl:
        print('白领范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '白领范文网'
        makeFolder(htmlPath)
        title = ''
        mian_bailingfanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'aiyangedu.com' in hrefUrl:
        print('爱杨网', hrefUrl)
        htmlPath = folderPath + '\\' + '爱杨网'
        makeFolder(htmlPath)
        title = ''
        mian_aiyangwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'anhui365.net' in hrefUrl:
        print('佳文', hrefUrl)
        htmlPath = folderPath + '\\' + '佳文'
        makeFolder(htmlPath)
        title = ''
        mian_jiawen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fanwen.hongwu.com' in hrefUrl:
        print('范文大全', hrefUrl)
        htmlPath = folderPath + '\\' + '范文大全'
        makeFolder(htmlPath)
        title = ''
        mian_fanwendaquan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '1000xuexi.com' in hrefUrl:
        print('千学网', hrefUrl)
        htmlPath = folderPath + '\\' + '千学网'
        makeFolder(htmlPath)
        title = ''
        mian_qianxuewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xde6.net' in hrefUrl:
        print('学道文库', hrefUrl)
        htmlPath = folderPath + '\\' + '学道文库'
        makeFolder(htmlPath)
        title = ''
        mian_xuedaowenku(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'iask.sina.com.cn' in hrefUrl:
        print('笔芯范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '笔芯范文网'
        makeFolder(htmlPath)
        title = ''
        mian_bixinfanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'pincai.com' in hrefUrl:
        print('品才', hrefUrl)
        htmlPath = folderPath + '\\' + '品才'
        makeFolder(htmlPath)
        title = ''
        mian_pincai(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'hrrsj.com' in hrefUrl:
        print('华容文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '华容文档网'
        makeFolder(htmlPath)
        title = ''
        mian_huarongwendangwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xiaowenxue.com' in hrefUrl:
        print('小文学范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '小文学范文网'
        makeFolder(htmlPath)
        title = ''
        mian_xiaowenxuefanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'cnfla.com' in hrefUrl:
        print('CN范文网', hrefUrl)
        htmlPath = folderPath + '\\' + 'CN范文网'
        makeFolder(htmlPath)
        title = ''
        mian_CNfanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '99fanwen.cn' in hrefUrl:
        print('99范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '99范文网'
        makeFolder(htmlPath)
        title = ''
        mian_99fanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '300zi.com' in hrefUrl:
        print('300字范文', hrefUrl)
        htmlPath = folderPath + '\\' + '300字范文'
        makeFolder(htmlPath)
        title = ''
        mian_300zi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yuwen.net' in hrefUrl:
        print('宇文网', hrefUrl)
        htmlPath = folderPath + '\\' + '宇文网'
        makeFolder(htmlPath)
        title = ''
        mian_yuwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fashionfield.cn' in hrefUrl:
        print('惊鸿范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '惊鸿范文网'
        makeFolder(htmlPath)
        title = ''
        mian_jinghongfanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'haozongjie.com' in hrefUrl:
        print('好总结', hrefUrl)
        htmlPath = folderPath + '\\' + '好总结'
        makeFolder(htmlPath)
        title = ''
        mian_haozongjie(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'tiwenya.com' in hrefUrl:
        print('题问鸭', hrefUrl)
        htmlPath = folderPath + '\\' + '题问鸭'
        makeFolder(htmlPath)
        title = ''
        mian_tiwenya(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'huibaoshu.com' in hrefUrl:
        print('汇报书', hrefUrl)
        htmlPath = folderPath + '\\' + '汇报书'
        makeFolder(htmlPath)
        title = ''
        mian_huibaoshu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gz85.com' in hrefUrl:
        print('工作总结之家', hrefUrl)
        htmlPath = folderPath + '\\' + '工作总结之家'
        makeFolder(htmlPath)
        title = ''
        mian_gongzuozongjiezhijia(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'shouyihuo.com' in hrefUrl:
        print('手艺活', hrefUrl)
        htmlPath = folderPath + '\\' + '手艺活'
        makeFolder(htmlPath)
        title = ''
        mian_shouyihuo(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'bjyld.com' in hrefUrl:
        print('考试学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '考试学习网'
        makeFolder(htmlPath)
        title = ''
        mian_kaoshixuexiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gjknj.com' in hrefUrl:
        print('科普读物', hrefUrl)
        htmlPath = folderPath + '\\' + '科普读物'
        makeFolder(htmlPath)
        title = ''
        mian_kepuduwu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'cuapp.net' in hrefUrl:
        print('在线学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '在线学习网'
        makeFolder(htmlPath)
        title = ''
        mian_zaixianxuexiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'niubb.net' in hrefUrl:
        print('牛牛范文', hrefUrl)
        htmlPath = folderPath + '\\' + '牛牛范文'
        makeFolder(htmlPath)
        title = ''
        mian_niuniufanwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'fclzw.com' in hrefUrl:
        print('励志网', hrefUrl)
        htmlPath = folderPath + '\\' + '励志网'
        makeFolder(htmlPath)
        title = ''
        mian_feichanglizhiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'shubaoc.com' in hrefUrl:
        print('书包范文', hrefUrl)
        htmlPath = folderPath + '\\' + '书包范文'
        makeFolder(htmlPath)
        title = ''
        mian_shubaofanwen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gzxgg.com' in hrefUrl:
        print('公文驿站', hrefUrl)
        htmlPath = folderPath + '\\' + '公文驿站'
        makeFolder(htmlPath)
        title = ''
        mian_gongwenyizhan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif '51test.net' in hrefUrl:
        print('无忧考网', hrefUrl)
        htmlPath = folderPath + '\\' + '无忧考网'
        makeFolder(htmlPath)
        title = ''
        mian_wuyoukaowang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'i1766.com' in hrefUrl:
        print('百家公文网', hrefUrl)
        htmlPath = folderPath + '\\' + '百家公文网'
        makeFolder(htmlPath)
        title = ''
        mian_baijiagongwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'anywen.com' in hrefUrl:
        print('爱文网', hrefUrl)
        htmlPath = folderPath + '\\' + '爱文网'
        makeFolder(htmlPath)
        title = ''
        mian_aiwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wenmizu.com' in hrefUrl:
        print('文秘族', hrefUrl)
        htmlPath = folderPath + '\\' + '文秘族'
        makeFolder(htmlPath)
        title = ''
        mian_wenmiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'qiquha.com' in hrefUrl:
        print('制度大全', hrefUrl)
        htmlPath = folderPath + '\\' + '制度大全'
        makeFolder(htmlPath)
        title = ''
        mian_zhidudaquan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'pptbz.com' in hrefUrl:
        print('ppt宝藏', hrefUrl)
        htmlPath = folderPath + '\\' + 'ppt宝藏'
        makeFolder(htmlPath)
        title = ''
        mian_pptbaozang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ksdsl.cn' in hrefUrl:
        print('范文大全2', hrefUrl)
        htmlPath = folderPath + '\\' + '范文大全2'
        makeFolder(htmlPath)
        title = ''
        mian_fanwendaquan2(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'dywdw.cn' in hrefUrl:
        print('第一文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '第一文档网'
        makeFolder(htmlPath)
        title = ''
        mian_diyiwendang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xuexilun.com' in hrefUrl:
        print('学习论', hrefUrl)
        htmlPath = folderPath + '\\' + '学习论'
        makeFolder(htmlPath)
        title = ''
        mian_xuexilun(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jiuaidu.com' in hrefUrl:
        print('就爱读', hrefUrl)
        htmlPath = folderPath + '\\' + '就爱读'
        makeFolder(htmlPath)
        title = ''
        mian_jiuaidu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'sbvv.cn' in hrefUrl:
        print('毕业生查重网', hrefUrl)
        htmlPath = folderPath + '\\' + '毕业生查重网'
        makeFolder(htmlPath)
        title = ''
        mian_biyeshengchachong(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'guimow.com' in hrefUrl:
        print('规模知识网', hrefUrl)
        htmlPath = folderPath + '\\' + '规模知识网'
        makeFolder(htmlPath)
        title = ''
        mian_guimozhishiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'taicanghenda.com' in hrefUrl:
        print('策划范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '策划范文网'
        makeFolder(htmlPath)
        title = ''
        mian_cehuafanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'nadiyi.com' in hrefUrl:
        print('拿第一', hrefUrl)
        htmlPath = folderPath + '\\' + '拿第一'
        makeFolder(htmlPath)
        title = ''
        mian_nadiyi(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'glwk8.com' in hrefUrl:
        print('管理文库吧', hrefUrl)
        htmlPath = folderPath + '\\' + '管理文库吧'
        makeFolder(htmlPath)
        title = ''
        mian_guangliwenkuba(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zhaichao1.com' in hrefUrl:
        print('美文欣赏网', hrefUrl)
        htmlPath = folderPath + '\\' + '美文欣赏网'
        makeFolder(htmlPath)
        title = ''
        mian_meiwenxinshangwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'guanlixi.com' in hrefUrl:
        print('经营范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '经营范文网'
        makeFolder(htmlPath)
        title = ''
        mian_jingyingfanweiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'jingyanben.com' in hrefUrl:
        print('经验本', hrefUrl)
        htmlPath = folderPath + '\\' + '经验本'
        makeFolder(htmlPath)
        title = ''
        mian_jingyanben(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'gongwensou.com' in hrefUrl:
        print('公文搜', hrefUrl)
        htmlPath = folderPath + '\\' + '公文搜'
        makeFolder(htmlPath)
        title = ''
        mian_gongwensou(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'lawov.com' in hrefUrl:
        print('律威百科', hrefUrl)
        htmlPath = folderPath + '\\' + '律威百科'
        makeFolder(htmlPath)
        title = ''
        mian_lvweibaike(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'paomian.net' in hrefUrl:
        print('泡面作文', hrefUrl)
        htmlPath = folderPath + '\\' + '泡面作文'
        makeFolder(htmlPath)
        title = ''
        mian_paomianzuowen(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'hetongwu.com' in hrefUrl:
        print('爱问合同屋', hrefUrl)
        htmlPath = folderPath + '\\' + '爱问合同屋'
        makeFolder(htmlPath)
        title = ''
        mian_aiwenhetongwu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'myplaymate.cn' in hrefUrl:
        print('我的公文网', hrefUrl)
        htmlPath = folderPath + '\\' + '我的公文网'
        makeFolder(htmlPath)
        title = ''
        mian_wodegongwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'studyofnet.com' in hrefUrl:
        print('秒懂生活', hrefUrl)
        htmlPath = folderPath + '\\' + '秒懂生活'
        makeFolder(htmlPath)
        title = ''
        mian_miaodongshenghuo(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'veryok.net' in hrefUrl:
        print('非常好', hrefUrl)
        htmlPath = folderPath + '\\' + '非常好'
        makeFolder(htmlPath)
        title = ''
        mian_feichanghao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'wljyyjy.com' in hrefUrl:
        print('大风车网', hrefUrl)
        htmlPath = folderPath + '\\' + '大风车网'
        makeFolder(htmlPath)
        title = ''
        mian_dafengcewang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'ahwmw.com' in hrefUrl:
        print('爱华文秘网', hrefUrl)
        htmlPath = folderPath + '\\' + '爱华文秘网'
        makeFolder(htmlPath)
        title = ''
        mian_aihuawenmiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xiebitongdao.com' in hrefUrl:
        print('携笔同道', hrefUrl)
        htmlPath = folderPath + '\\' + '携笔同道'
        makeFolder(htmlPath)
        title = ''
        mian_xiebitongdao(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'csibaskets.com' in hrefUrl:
        print('文秘学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '文秘学习网'
        makeFolder(htmlPath)
        title = ''
        mian_wenmixuexiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'puchedu.cn' in hrefUrl:
        print('蒲城资源', hrefUrl)
        htmlPath = folderPath + '\\' + '蒲城资源'
        makeFolder(htmlPath)
        title = ''
        mian_puchengziyuan(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xhmbeer.com' in hrefUrl:
        print('随笔网', hrefUrl)
        htmlPath = folderPath + '\\' + '随笔网'
        makeFolder(htmlPath)
        title = ''
        mian_suibiwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zhangdahai.com' in hrefUrl:
        print('大海范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '大海范文网'
        makeFolder(htmlPath)
        title = ''
        mian_dahaifanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'zhsm.net' in hrefUrl:
        print('范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '范文网'
        makeFolder(htmlPath)
        title = ''
        mian_fanwenwang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'd8qu.com' in hrefUrl:
        print('第八区', hrefUrl)
        htmlPath = folderPath + '\\' + '第八区'
        makeFolder(htmlPath)
        title = ''
        mian_dibaqu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'stokuaidi.com' in hrefUrl:
        print('留学社区', hrefUrl)
        htmlPath = folderPath + '\\' + '留学社区'
        makeFolder(htmlPath)
        title = ''
        mian_liuxueshequ(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'xijiang520.com' in hrefUrl:
        print('爱喜匠', hrefUrl)
        htmlPath = folderPath + '\\' + '爱喜匠'
        makeFolder(htmlPath)
        title = ''
        mian_aixijiang(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'aixiangfu.com' in hrefUrl:
        print('爱商辅', hrefUrl)
        htmlPath = folderPath + '\\' + '爱商辅'
        makeFolder(htmlPath)
        title = ''
        mian_aishangfu(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'yuanwenj.com' in hrefUrl:
        print('源文鉴', hrefUrl)
        htmlPath = folderPath + '\\' + '源文鉴'
        makeFolder(htmlPath)
        title = ''
        mian_yuanwenjian(hrefUrl, htmlPath, keyStr, title)
        
        panduan = True
        # colseDriver(driver)
    elif 'daniulw.com' in hrefUrl:
        print('大牛论文网', hrefUrl)
        htmlPath = folderPath + '\\' + '大牛论文网'
        makeFolder(htmlPath)
        title = ''
        mian_daniulunwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'm.ybchuban.com' in hrefUrl:
        print('亿百出版网二', hrefUrl)
        htmlPath = folderPath + '\\' + '亿百出版网二'
        makeFolder(htmlPath)
        title = ''
        mian_yibaichu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'm.fakanzx.com' in hrefUrl:
        print('卓文著作网', hrefUrl)
        htmlPath = folderPath + '\\' + '卓文著作网'
        makeFolder(htmlPath)
        title = ''
        mian_zhuowenzhuzuowang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'mamilist.com' in hrefUrl:
        print('妈妈文摘', hrefUrl)
        htmlPath = folderPath + '\\' + '妈妈文摘'
        makeFolder(htmlPath)
        title = ''
        mian_mamawenzhai(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'shancaoxiang.com' in hrefUrl:
        print('山草香', hrefUrl)
        htmlPath = folderPath + '\\' + '山草香'
        makeFolder(htmlPath)
        title = ''
        mian_shancaoxiang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fanwen.aixue.net' in hrefUrl:
        print('爱学范文', hrefUrl)
        htmlPath = folderPath + '\\' + '爱学范文'
        makeFolder(htmlPath)
        title = ''
        mian_aixuefanwen(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zuowenba.net' in hrefUrl:
        print('作文吧', hrefUrl)
        htmlPath = folderPath + '\\' + '作文吧'
        makeFolder(htmlPath)
        title = ''
        mian_zuozuowenba(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'syfabiao.com' in hrefUrl:
        print('山崖发表网', hrefUrl)
        htmlPath = folderPath + '\\' + '山崖发表网'
        makeFolder(htmlPath)
        title = ''
        mian_shanyafabiaowang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'safehoo.com' in hrefUrl:
        print('安全管理网', hrefUrl)
        htmlPath = folderPath + '\\' + '安全管理网'
        makeFolder(htmlPath)
        title = ''
        mian_anquanguanliwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'meidekan.com' in hrefUrl:
        print('美德网', hrefUrl)
        htmlPath = folderPath + '\\' + '美德网'
        makeFolder(htmlPath)
        title = ''
        mian_meidewang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'bwb5.com' in hrefUrl:
        print('趣文库', hrefUrl)
        htmlPath = folderPath + '\\' + '趣文库'
        makeFolder(htmlPath)
        title = ''
        mian_quwenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'quxiao7.com' in hrefUrl:
        print('趣小七', hrefUrl)
        htmlPath = folderPath + '\\' + '趣小七'
        makeFolder(htmlPath)
        title = ''
        mian_quxiaoqi(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'aiqingjingdian.com' in hrefUrl:
        print('爱情经典', hrefUrl)
        htmlPath = folderPath + '\\' + '爱情经典'
        makeFolder(htmlPath)
        title = ''
        mian_aiqingjingdian(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'baywatch.cn' in hrefUrl:
        print('中文期刊', hrefUrl)
        htmlPath = folderPath + '\\' + '中文期刊'
        makeFolder(htmlPath)
        title = ''
        mian_zhonwenqikan(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'benqdjg.com' in hrefUrl:
        print('无忧考试网', hrefUrl)
        htmlPath = folderPath + '\\' + '无忧考试网'
        makeFolder(htmlPath)
        title = ''
        mian_wuyoukaoshiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fwen.net' in hrefUrl:
        print('小语范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '小语范文网'
        makeFolder(htmlPath)
        title = ''
        mian_xiaoyufanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'mini.xnnews.com.cn' in hrefUrl:
        print('今日热点', hrefUrl)
        htmlPath = folderPath + '\\' + '今日热点'
        makeFolder(htmlPath)
        title = ''
        mian_jinriredian(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'jiangzi.com' in hrefUrl:
        print('匠子生活', hrefUrl)
        htmlPath = folderPath + '\\' + '匠子生活'
        makeFolder(htmlPath)
        title = ''
        mian_jiangzishenghuo(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'news.bangkaow' in hrefUrl:
        print('草根科学网', hrefUrl)
        htmlPath = folderPath + '\\' + '草根科学网'
        makeFolder(htmlPath)
        title = ''
        mian_caogengkexuewang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'jxstedu.com' in hrefUrl:
        print('知识网', hrefUrl)
        htmlPath = folderPath + '\\' + '知识网'
        makeFolder(htmlPath)
        title = ''
        mian_zhishiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '100ky.cn' in hrefUrl:
        print('百分文库', hrefUrl)
        htmlPath = folderPath + '\\' + '百分文库'
        makeFolder(htmlPath)
        title = ''
        mian_baifenwenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'chinaiol.com' in hrefUrl:
        print('产业在线', hrefUrl)
        htmlPath = folderPath + '\\' + '产业在线'
        makeFolder(htmlPath)
        title = ''
        mian_chanyezaixian(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'yxzwxz.com' in hrefUrl:
        print('优秀作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '优秀作文网'
        makeFolder(htmlPath)
        title = ''
        mian_youxiuzuowenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fabiao.com' in hrefUrl:
        print('发表之家', hrefUrl)
        htmlPath = folderPath + '\\' + '发表之家'
        makeFolder(htmlPath)
        title = ''
        mian_fabiaozhijia(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'haoqikan.com' in hrefUrl:
        print('好期刊', hrefUrl)
        htmlPath = folderPath + '\\' + '好期刊'
        makeFolder(htmlPath)
        title = ''
        mian_haoqikan(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'youfabiao.com' in hrefUrl:
        print('优发表', hrefUrl)
        htmlPath = folderPath + '\\' + '优发表'
        makeFolder(htmlPath)
        title = ''
        mian_youfabiao(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zjrcjd.cn' in hrefUrl:
        print('人人点', hrefUrl)
        htmlPath = folderPath + '\\' + '人人点'
        makeFolder(htmlPath)
        title = ''
        mian_renrendian(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '1mishu.com' in hrefUrl:
        print('世纪秘书', hrefUrl)
        htmlPath = folderPath + '\\' + '世纪秘书'
        makeFolder(htmlPath)
        title = ''
        mian_shijimishu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xinfet.com' in hrefUrl:
        print('新飞文库网', hrefUrl)
        htmlPath = folderPath + '\\' + '新飞文库网'
        makeFolder(htmlPath)
        title = ''
        mian_xinfeiwenkuwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'hezeshenlvshi.com' in hrefUrl:
        print('律知分享', hrefUrl)
        htmlPath = folderPath + '\\' + '律知分享'
        makeFolder(htmlPath)
        title = ''
        mian_lvzhifenxiang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'cnqr.org' in hrefUrl:
        print('中鸿认证服务', hrefUrl)
        htmlPath = folderPath + '\\' + '中鸿认证服务'
        makeFolder(htmlPath)
        title = ''
        mian_zhonhongrenzhengfuwu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'hcschool.cn' in hrefUrl:
        print('法律', hrefUrl)
        htmlPath = folderPath + '\\' + '法律'
        makeFolder(htmlPath)
        title = ''
        mian_falv(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '66laws.com' in hrefUrl:
        print('律科网', hrefUrl)
        htmlPath = folderPath + '\\' + '律科网'
        makeFolder(htmlPath)
        title = ''
        mian_lvkewang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xzxdxxg.com' in hrefUrl:
        print('云纵网', hrefUrl)
        htmlPath = folderPath + '\\' + '云纵网'
        makeFolder(htmlPath)
        title = ''
        mian_yunzongwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zhongleduo.net' in hrefUrl:
        print('众乐法先知', hrefUrl)
        htmlPath = folderPath + '\\' + '众乐法先知'
        makeFolder(htmlPath)
        title = ''
        mian_zhonglefaxianzhi(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zhongleduo.net' in hrefUrl:
        print('众乐法先知', hrefUrl)
        htmlPath = folderPath + '\\' + '众乐法先知'
        makeFolder(htmlPath)
        title = ''
        mian_zhonglefaxianzhi(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'haerbin.qingdao.bdtong.com.cn' in hrefUrl:
        print('本地通', hrefUrl)
        htmlPath = folderPath + '\\' + '本地通'
        makeFolder(htmlPath)
        title = ''
        mian_benditong(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'cnwb.net' in hrefUrl:
        print('中国建筑防水协会', hrefUrl)
        htmlPath = folderPath + '\\' + '中国建筑防水协会'
        makeFolder(htmlPath)
        title = ''
        mian_zhongguojianzhufangshuixiehui(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'ozbb.cn' in hrefUrl:
        print('贝贝文库', hrefUrl)
        htmlPath = folderPath + '\\' + '贝贝文库'
        makeFolder(htmlPath)
        title = ''
        mian_beibeiwenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '1mi.net' in hrefUrl:
        print('一秘范文模板', hrefUrl)
        htmlPath = folderPath + '\\' + '一秘范文模板'
        makeFolder(htmlPath)
        title = ''
        mian_yimifanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'quanjws.com' in hrefUrl:
        print('泉景微视', hrefUrl)
        htmlPath = folderPath + '\\' + '泉景微视'
        makeFolder(htmlPath)
        title = ''
        mian_quanjingweishi(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'cqwcsy.com' in hrefUrl:
        print('春秋美文网', hrefUrl)
        htmlPath = folderPath + '\\' + '春秋美文网'
        makeFolder(htmlPath)
        title = ''
        mian_cunqiumeiwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'jsyks.com' in hrefUrl:
        print('驾驶员考试', hrefUrl)
        htmlPath = folderPath + '\\' + '驾驶员考试'
        makeFolder(htmlPath)
        title = ''
        mian_jiashiyuankaoshi(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'hywsbj.com' in hrefUrl:
        print('好运范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '好运范文网'
        makeFolder(htmlPath)
        title = ''
        mian_haoyunfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'guidaye.com' in hrefUrl:
        print('鬼故事网', hrefUrl)
        htmlPath = folderPath + '\\' + '鬼故事网'
        makeFolder(htmlPath)
        title = ''
        mian_guigushiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'workercn.cn' in hrefUrl:
        print('中工网', hrefUrl)
        htmlPath = folderPath + '\\' + '中工网'
        makeFolder(htmlPath)
        title = ''
        mian_zhongongwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'qigushi.com' in hrefUrl:
        print('七故事', hrefUrl)
        htmlPath = folderPath + '\\' + '七故事'
        makeFolder(htmlPath)
        title = ''
        mian_qigushi(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zhaichao.net' in hrefUrl:
        print('摘抄网', hrefUrl)
        htmlPath = folderPath + '\\' + '摘抄网'
        makeFolder(htmlPath)
        title = ''
        mian_zaicaowang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xbzhaopin.com' in hrefUrl:
        print('小编范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '小编范文网'
        makeFolder(htmlPath)
        title = ''
        mian_xiaobianfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'inrrp.com.cn' in hrefUrl:
        print('人人范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '人人范文网'
        makeFolder(htmlPath)
        title = ''
        mian_renrenfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'vgushi.com' in hrefUrl:
        print('古诗词网', hrefUrl)
        htmlPath = folderPath + '\\' + '古诗词网'
        makeFolder(htmlPath)
        title = ''
        mian_gushiciwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'hnchuangzuo.com' in hrefUrl:
        print('华南创作网', hrefUrl)
        htmlPath = folderPath + '\\' + '华南创作网'
        makeFolder(htmlPath)
        title = ''
        mian_huananchuangzuowang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xuebazw.com' in hrefUrl:
        print('学霸作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '学霸作文网'
        makeFolder(htmlPath)
        title = ''
        mian_xuebazuowenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'bfenglish.com' in hrefUrl:
        print('百分英语', hrefUrl)
        htmlPath = folderPath + '\\' + '百分英语'
        makeFolder(htmlPath)
        title = ''
        mian_baifenyingyu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'hxfww.com' in hrefUrl:
        print('红袖范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '红袖范文网'
        makeFolder(htmlPath)
        title = ''
        mian_hongxiufanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '99sanwen.com' in hrefUrl:
        print('久久散文网', hrefUrl)
        htmlPath = folderPath + '\\' + '久久散文网'
        makeFolder(htmlPath)
        title = ''
        mian_jiujiusanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'jtcl.org.cn' in hrefUrl:
        print('草料作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '草料作文网'
        makeFolder(htmlPath)
        title = ''
        mian_caoliaozuowenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'b9yy.com' in hrefUrl:
        print('B9教育', hrefUrl)
        htmlPath = folderPath + '\\' + 'B9教育'
        makeFolder(htmlPath)
        title = ''
        mian_B9jiaoyu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'kixwx.com' in hrefUrl:
        print('开心文学网', hrefUrl)
        htmlPath = folderPath + '\\' + '开心文学网'
        makeFolder(htmlPath)
        title = ''
        mian_kaixinwenxuewang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'kaoyanmiji.com' in hrefUrl:
        print('考研秘籍网', hrefUrl)
        htmlPath = folderPath + '\\' + '考研秘籍网'
        makeFolder(htmlPath)
        title = ''
        mian_kaoyanmijiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '21ks.net' in hrefUrl:
        print('公务员期刊网', hrefUrl)
        htmlPath = folderPath + '\\' + '公务员期刊网'
        makeFolder(htmlPath)
        title = ''
        mian_gongwuyuanqikanwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'cooco.net.cn' in hrefUrl:
        print('cooco', hrefUrl)
        htmlPath = folderPath + '\\' + 'cooco'
        makeFolder(htmlPath)
        title = ''
        mian_cooco(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'yundocx.com' in hrefUrl:
        print('云文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '云文档网'
        makeFolder(htmlPath)
        title = ''
        mian_yunwendangwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'ce.cn' in hrefUrl:
        print('中国经济网', hrefUrl)
        htmlPath = folderPath + '\\' + '中国经济网'
        makeFolder(htmlPath)
        title = ''
        mian_zhongguojingjiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'cmpx.com.cn' in hrefUrl:
        print('综合文库', hrefUrl)
        htmlPath = folderPath + '\\' + '综合文库'
        makeFolder(htmlPath)
        title = ''
        mian_zonghewenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'pxwst.com' in hrefUrl:
        print('啦啦作文网', hrefUrl)
        htmlPath = folderPath + '\\' + '啦啦作文网'
        makeFolder(htmlPath)
        title = ''
        mian_lalazuowenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'yangfanhao.com' in hrefUrl:
        print('扬帆号', hrefUrl)
        htmlPath = folderPath + '\\' + '扬帆号'
        makeFolder(htmlPath)
        title = ''
        mian_yangfanhao(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'tech.yutainews.com' in hrefUrl:
        print('财经新闻周刊', hrefUrl)
        htmlPath = folderPath + '\\' + '财经新闻周刊'
        makeFolder(htmlPath)
        title = ''
        mian_caijingxinwenzhoukan(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'mestalk.com' in hrefUrl:
        print('蜜桃圈', hrefUrl)
        htmlPath = folderPath + '\\' + '蜜桃圈'
        makeFolder(htmlPath)
        title = ''
        mian_mitaoquan(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'pzzhd.com' in hrefUrl:
        print('花都知识网', hrefUrl)
        htmlPath = folderPath + '\\' + '花都知识网'
        makeFolder(htmlPath)
        title = ''
        mian_huadouzhishiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'cspengbo.com' in hrefUrl:
        print('蓬勃范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '蓬勃范文网'
        makeFolder(htmlPath)
        title = ''
        mian_pengbofanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'bbxzwk.com' in hrefUrl:
        print('笔宝写作文库', hrefUrl)
        htmlPath = folderPath + '\\' + '笔宝写作文库'
        makeFolder(htmlPath)
        title = ''
        mian_bibaoxiezuowenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'hunanhr.cn' in hrefUrl:
        print('职场范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '职场范文网'
        makeFolder(htmlPath)
        title = ''
        mian_zhichangfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xarfcw.com' in hrefUrl:
        print('安瑞范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '安瑞范文网'
        makeFolder(htmlPath)
        title = ''
        mian_anruifanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'nkedu.org' in hrefUrl:
        print('南开网', hrefUrl)
        htmlPath = folderPath + '\\' + '南开网'
        makeFolder(htmlPath)
        title = ''
        mian_nankaiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'lizhi135.com' in hrefUrl:
        print('励志一生网', hrefUrl)
        htmlPath = folderPath + '\\' + '励志一生网'
        makeFolder(htmlPath)
        title = ''
        mian_lizhiyishengwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'lz13.cn' in hrefUrl:
        print('励志一生', hrefUrl)
        htmlPath = folderPath + '\\' + '励志一生'
        makeFolder(htmlPath)
        title = ''
        mian_lizhiyisheng(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fanwen.hao86.com' in hrefUrl:
        print('hao86', hrefUrl)
        htmlPath = folderPath + '\\' + 'hao86'
        makeFolder(htmlPath)
        title = ''
        mian_hao86(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'gwyoo.com' in hrefUrl:
        print('公务员之家', hrefUrl)
        htmlPath = folderPath + '\\' + '公务员之家'
        makeFolder(htmlPath)
        title = ''
        mian_gongwuyuanzhijia(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '850500.com' in hrefUrl:
        print('百味书屋', hrefUrl)
        htmlPath = folderPath + '\\' + '百味书屋'
        makeFolder(htmlPath)
        title = ''
        mian_baiweishuwu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'daxue.tm022.com' in hrefUrl:
        print('天穆网', hrefUrl)
        htmlPath = folderPath + '\\' + '天穆网'
        makeFolder(htmlPath)
        title = ''
        mian_tianmuwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'douhuafen.com' in hrefUrl:
        print('朵花网', hrefUrl)
        htmlPath = folderPath + '\\' + '朵花网'
        makeFolder(htmlPath)
        title = ''
        mian_duohuawang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '92fanwen.com' in hrefUrl:
        print('九二范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '九二范文网'
        makeFolder(htmlPath)
        title = ''
        mian_jiuerfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'kaoshi.dzwww.com' in hrefUrl:
        print('众学堂', hrefUrl)
        htmlPath = folderPath + '\\' + '众学堂'
        makeFolder(htmlPath)
        title = ''
        mian_zhongxuetang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'mianfeixuexi.cn' in hrefUrl:
        print('免费学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '免费学习网'
        makeFolder(htmlPath)
        title = ''
        mian_mianfeixuexiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'yyhetong.com' in hrefUrl:
        print('一一合同网', hrefUrl)
        htmlPath = folderPath + '\\' + '一一合同网'
        makeFolder(htmlPath)
        title = ''
        mian_yyhetong(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fwtx.cgsbm.org' in hrefUrl:
        print('范文通', hrefUrl)
        htmlPath = folderPath + '\\' + '范文通'
        makeFolder(htmlPath)
        title = ''
        mian_fanwentong(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'sdwen.com' in hrefUrl:
        print('生动范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '生动范文网'
        makeFolder(htmlPath)
        title = ''
        mian_shengdongfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'yituanyoupin.net' in hrefUrl:
        print('一团范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '一团范文网'
        makeFolder(htmlPath)
        title = ''
        mian_yituanfanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'shuzhi99.com' in hrefUrl:
        print('会述职范文', hrefUrl)
        htmlPath = folderPath + '\\' + '会述职范文'
        makeFolder(htmlPath)
        title = ''
        mian_huishuzhifanwen(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'everhomeus.com' in hrefUrl:
        print('学习网', hrefUrl)
        htmlPath = folderPath + '\\' + '学习网'
        makeFolder(htmlPath)
        title = ''
        mian_xuexiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zwku.net' in hrefUrl:
        print('作文库网', hrefUrl)
        htmlPath = folderPath + '\\' + '作文库网'
        makeFolder(htmlPath)
        title = ''
        mian_zuowenkuwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'huzhidao.com' in hrefUrl:
        print('虎知道', hrefUrl)
        htmlPath = folderPath + '\\' + '虎知道'
        makeFolder(htmlPath)
        title = ''
        mian_huzhidao(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'laozhuhai.com.cn' in hrefUrl:
        print('竹海文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '竹海文档网'
        makeFolder(htmlPath)
        title = ''
        mian_zhuhaiwendangwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'iyulu.cn' in hrefUrl:
        print('爱语录', hrefUrl)
        htmlPath = folderPath + '\\' + '爱语录'
        makeFolder(htmlPath)
        title = ''
        mian_aiyulu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '51jzrc.cn' in hrefUrl:
        print('无忧文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '无忧文档网'
        makeFolder(htmlPath)
        title = ''
        mian_wuyouwendangwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'nhs-transmission.com' in hrefUrl:
        print('上音文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '上音文档网'
        makeFolder(htmlPath)
        title = ''
        mian_shangyinwendangwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'zwku.net' in hrefUrl:
        print('作文库网', hrefUrl)
        htmlPath = folderPath + '\\' + '作文库网'
        makeFolder(htmlPath)
        title = ''
        mian_zuowenkuwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'ilede.com.cn' in hrefUrl:
        print('乐德范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '乐德范文网'
        makeFolder(htmlPath)
        title = ''
        mian_ledefanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xadnpx.com' in hrefUrl:
        print('纵文网', hrefUrl)
        htmlPath = folderPath + '\\' + '纵文网'
        makeFolder(htmlPath)
        title = ''
        mian_zongwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'feisuxs.com' in hrefUrl:
        print('飞速成语网', hrefUrl)
        htmlPath = folderPath + '\\' + '飞速成语网'
        makeFolder(htmlPath)
        title = ''
        mian_feisuchengyu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '7an.com.cn' in hrefUrl:
        print('七安文库', hrefUrl)
        htmlPath = folderPath + '\\' + '七安文库'
        makeFolder(htmlPath)
        title = ''
        mian_qianwenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'gfd96.cn' in hrefUrl:
        print('演示站', hrefUrl)
        htmlPath = folderPath + '\\' + '演示站'
        makeFolder(htmlPath)
        title = ''
        mian_yanshizhan(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '51feibao.com' in hrefUrl:
        print('五一费宝网', hrefUrl)
        htmlPath = folderPath + '\\' + '五一费宝网'
        makeFolder(htmlPath)
        title = ''
        mian_wuyifeibaowang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'nhs-transmission.com' in hrefUrl:
        print('相依文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '相依文档网'
        makeFolder(htmlPath)
        title = ''
        mian_xiangyiwendangwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fjabr.com' in hrefUrl:
        print('甜蜜生活', hrefUrl)
        htmlPath = folderPath + '\\' + '甜蜜生活'
        makeFolder(htmlPath)
        title = ''
        mian_tianmishenghuo(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'tengoyou.com' in hrefUrl:
        print('腾游文库', hrefUrl)
        htmlPath = folderPath + '\\' + '腾游文库'
        makeFolder(htmlPath)
        title = ''
        mian_tengyouwenku(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'yituanyoupin.com' in hrefUrl:
        print('好老师范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '好老师范文网'
        makeFolder(htmlPath)
        title = ''
        mian_haolaoshifanwenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'ouyu158.com' in hrefUrl:
        print('职业学校招生网', hrefUrl)
        htmlPath = folderPath + '\\' + '职业学校招生网'
        makeFolder(htmlPath)
        title = ''
        mian_zhiyexuexiaozhaoshengwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '92baogao.com' in hrefUrl:
        print('九二报告网', hrefUrl)
        htmlPath = folderPath + '\\' + '九二报告网'
        makeFolder(htmlPath)
        title = ''
        mian_jiuerbaogaowang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fanwenbang.cn' in hrefUrl:
        print('范文帮', hrefUrl)
        htmlPath = folderPath + '\\' + '范文帮'
        makeFolder(htmlPath)
        title = ''
        mian_fanwenbang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'xue.shiwenwu.com' in hrefUrl:
        print('学识屋', hrefUrl)
        htmlPath = folderPath + '\\' + '学识屋'
        makeFolder(htmlPath)
        title = ''
        mian_xueshiwu(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'shiwenwu.com' in hrefUrl:
        print('文化知识网', hrefUrl)
        htmlPath = folderPath + '\\' + '文化知识网'
        makeFolder(htmlPath)
        title = ''
        mian_wenhuazhishiwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'fanbenwang.com' in hrefUrl:
        print('范本网', hrefUrl)
        htmlPath = folderPath + '\\' + '范本网'
        makeFolder(htmlPath)
        title = ''
        mian_fanbenwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 't7t8.net' in hrefUrl:
        print('t7t8美文号', hrefUrl)
        htmlPath = folderPath + '\\' + 't7t8美文号'
        makeFolder(htmlPath)
        title = ''
        mian_t7t8meiwenhao(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '010ky.com' in hrefUrl:
        print('可意文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '可意文档网'
        makeFolder(htmlPath)
        title = ''
        mian_keyiwendnagwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif '4000999668.com' in hrefUrl:
        print('耀景文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '耀景文档网'
        makeFolder(htmlPath)
        title = ''
        mian_yaojingwendangwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'wenshudoc.com' in hrefUrl:
        print('顺风文档网', hrefUrl)
        htmlPath = folderPath + '\\' + '顺风文档网'
        makeFolder(htmlPath)
        title = ''
        mian_sunfengwendnagwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'edufu.cn' in hrefUrl:
        print('爱立教育网', hrefUrl)
        htmlPath = folderPath + '\\' + '爱立教育网'
        makeFolder(htmlPath)
        title = ''
        mian_ailijiaoyuwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    elif 'shzj88.com' in hrefUrl:
        print('文秘范文网', hrefUrl)
        htmlPath = folderPath + '\\' + '文秘范文网'
        makeFolder(htmlPath)
        title = ''
        mian_wenmifanwenwang2(hrefUrl, htmlPath, keyStr, title)
    elif 'ybchuban.com' in hrefUrl:
        print('亿百出版网', hrefUrl)
        htmlPath = folderPath + '\\' + '亿百出版网'
        makeFolder(htmlPath)
        title = ''
        mian_yibaichubanwang(hrefUrl, htmlPath, keyStr, title)
        panduan = True
        # colseDriver(driver)
    else:
        contentUrl = hrefUrl + '-----' + keyStr
        urls.append(contentUrl)
        if 'image_keyword' in hrefUrl or 'dvpf=' in hrefUrl:
            print('不可关闭网页')
        # else:
            # colseDriver(driver)
    return panduan
if __name__ == '__main__':
    element=''
    folderPath=''
    driver=''
    keyStr=''
    urls=''
    hrefUrl=''
    baiduchociesWebsitePlay(element, folderPath, driver, keyStr, urls, hrefUrl)