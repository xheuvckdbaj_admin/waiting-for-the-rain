import os
import pypandoc
import requests

from pythonProject.FindFile import find_file


def htmlBeWOrd(src):
        try:
                print(src)
                docx=src.replace('.html','.docx').replace('\t','').replace('\n','')
                output = pypandoc.convert_file(src, 'docx', outputfile=docx,encoding='utf-8',extra_args=["-M2GB", "+RTS", "-K64m", "-RTS"])
                os.remove(src)
        except:
            print('错误')
def shear_dile(src):
    if os.path.isdir(src):
        if not os.listdir(src):
            try:
                os.rmdir(src)
                print(u'移除空目录: ' + src)
            except:
                print('error')
        else:
            for d in os.listdir(src):
                shear_dile(os.path.join(src, d))
    if os.path.isfile(src):
        if '~$' in src:
            try:
                os.remove(src)
            except:
                print('error')
        if src.endswith('.html'):
            htmlBeWOrd(src)

def mainHtoW():

    folder=r'D:\文档\百度活动文档\百度html\其他网站\百度文库'
    shear_dile(folder)
if __name__ == '__main__':
    mainHtoW()
