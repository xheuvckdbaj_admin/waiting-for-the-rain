import os
import shutil


def remove_folder(titleFolder,classifyFolder):
    if os.path.exists(titleFolder):
        shutil.rmtree(titleFolder)
        print('成功删除文件夹：',titleFolder)
    if os.path.exists(classifyFolder):
        shutil.rmtree(classifyFolder)
        print('成功删除文件夹：',classifyFolder)

def main():
    #存放活动文档标题的文件夹
    titleFolder = r'D:\文档\百度活动文档\百度活动文档标题'
    classifyFolder = r'D:\文档\百度活动文档\用来分类文档标题'
    #存放分类后需要上传的活动文档的文件夹
    uploadFolder=r'D:\文档\百度活动文档\百度活动文档上传'
    remove_folder(titleFolder,classifyFolder)
if __name__ == '__main__':
    main()