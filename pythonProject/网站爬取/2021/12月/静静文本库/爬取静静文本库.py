# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getBrowserUrl(browser,url):
    try:
        browser.get(url)
    except:
        time.sleep(0.5)
        browser.get(url)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.det_content')[0])
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.det_title_info > h1')[0].text)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
            #相关热词搜索：
            #相关作文：
            strUrl='https://www.lxzcp.com/jiaoyuwenben/2019/0317/%d.html'
            for i in range(1,792000):
                print(i)
                newUrl=strUrl%i
                # try:
                getHtml(thmlpath, newUrl)
                # except:
                #     print('出错')
                #     continue
if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
