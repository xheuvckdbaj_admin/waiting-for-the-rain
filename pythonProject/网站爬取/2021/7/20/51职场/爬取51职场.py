# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="qian-hou"]/span[@class="hou"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.51xyzp.com/qiuzhi/qiuzhixin/184615.html',
                'https://www.51xyzp.com/qiuzhi/gerenjianli/183847.html',
                'https://www.51xyzp.com/qiuzhi/jianlifanwen/182942.html',
                'https://www.51xyzp.com/qiuzhi/jianlimoban/175851.html',
                'https://www.51xyzp.com/qiuzhi/qiuzhijianli/183770.html',
                'https://www.51xyzp.com/qiuzhi/shixibaogao/184611.html',
                'https://www.51xyzp.com/qiuzhi/shixizongjie/184372.html',
                'https://www.51xyzp.com/qiuzhi/shixixinde/184522.html',
                'https://www.51xyzp.com/qiuzhi/shixizhengming/184003.html',
                'https://www.51xyzp.com/qiuzhi/shixizhouji/184352.html',
                'https://www.51xyzp.com/jiuye/news/183761.html',
                'https://www.51xyzp.com/jiuye/zhiyeguihua/177956.html',
                'https://www.51xyzp.com/jiuye/jiuyeqianjing/182307.html',
                'https://www.51xyzp.com/jiuye/zhichangliyi/184237.html',
                'https://www.51xyzp.com/jiuye/peixun/8585.html',
                'https://www.51xyzp.com/jiuye/shehuishijian/184619.html',
                'https://www.51xyzp.com/jiuye/gaokao/149054.html',
                'https://www.51xyzp.com/jiuye/mianshi/165430.html',
                'https://www.51xyzp.com/jiuye/mianshishiti/147161.html',
                'https://www.51xyzp.com/jiuye/bishitimu/133237.html',
                'https://www.51xyzp.com/jiuye/bishijingyan/130942.html',
                'https://www.51xyzp.com/hr/zhaopinxuanba/131228.html',
                'https://www.51xyzp.com/hr/yuangongguanli/184117.html',
                'https://www.51xyzp.com/hr/xinchouguanli/174362.html',
                'https://www.51xyzp.com/hr/jixiaokaohe/173673.html',
                'https://www.51xyzp.com/hr/peixunfazhan/143188.html',
                'https://www.51xyzp.com/hr/rencaizhanlue/175791.html',
                'https://www.51xyzp.com/hr/qiyewenhua/183704.html',
                'https://www.51xyzp.com/hr/gangweizhize/184613.html',
                'https://www.51xyzp.com/shiyongwen/yanjianggao/184486.html',
                'https://www.51xyzp.com/shiyongwen/fayangao/183024.html',
                'https://www.51xyzp.com/shiyongwen/zhuchici/184239.html',
                'https://www.51xyzp.com/shiyongwen/xieyishu/184623.html',
                'https://www.51xyzp.com/shiyongwen/guangbogao/184616.html',
                'https://www.51xyzp.com/shiyongwen/weituoshu/184609.html',
                'https://www.51xyzp.com/shiyongwen/zhufuyu/184620.html',
                'https://www.51xyzp.com/shiyongwen/guanggaoci/184110.html',
                'https://www.51xyzp.com/shiyongwen/cizhixin/184234.html',
                'https://www.51xyzp.com/shiyongwen/jiantaoshu/184621.html',
                'https://www.51xyzp.com/shiyongwen/cehuashu/184617.html',
                'https://www.51xyzp.com/shiyongwen/yaoqinghan/184614.html',
                'https://www.51xyzp.com/shiyongwen/qingjiatiao/184235.html',
                'https://www.51xyzp.com/shiyongwen/hetongfanben/184339.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
