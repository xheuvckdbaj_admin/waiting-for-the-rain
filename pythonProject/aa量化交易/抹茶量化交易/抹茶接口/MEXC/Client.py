import hashlib
import hmac
from urllib.parse import urlencode

import httpx

import json
from pythonProject.aa量化交易.抹茶量化交易.抹茶接口.MEXC import utils, consts
from pythonProject.aa量化交易.抹茶量化交易.抹茶接口.MEXC.utils import pre_hash, signl
import requests
import hmac
import hashlib
from urllib.parse import urlencode, quote

from urllib.parse import urlencode, quote


class Client:
    def __init__(self, APIKey, secret_key):
        self.APIKey = APIKey
        self.secret_key = secret_key
        # self.passphrase=passphrase
        self.domain = consts.DOMAIN
        self.client = httpx.Client(base_url=consts.DOMAIN, http2=True)

    def logOn(self):
        httpx.get()

    def _sign_v3(self, req_time, sign_params=None):
        if sign_params:
            sign_params = urlencode(sign_params, quote_via=quote)
            to_sign = "{}&timestamp={}".format(sign_params, req_time)
        else:
            to_sign = "timestamp={}".format(req_time)
        sign = hmac.new(self.secret_key.encode('utf-8'), to_sign.encode('utf-8'), hashlib.sha256).hexdigest()
        return sign

    def _getDefaultSymbols(self):
        url = consts.DOMAIN + consts.DEFAULTSYMBOLS
        response = httpx.get(url)
        print(response.text)
    def getGetHttpx(self,method,request_path,params,header):
        try:
            if method == consts.GET:
                return self.client.get(request_path, headers=header)
            elif  method == consts.POST:
                return self.client.post(request_path, data=params, headers=header)
            else:
                return self.client.delete(request_path, params=params, headers=header)
        except:
            return self.getGetHttpx(method, request_path, params, header)


    def _request(self, method, request_path, params):
        # client = Client.Client(self.APIKey, self.secret_key)
        # timeStamp = self._getSysTimeStamp
        # message = pre_hash(timeStamp, params)
        # sign = signl(message, self.secret_key)
        if method == consts.GET:
            if self.APIKey != '-1':
                header = utils.get_header(self.APIKey)
                request_path = request_path + utils.parse_params_to_str(params)
                response = self.client.get(request_path, headers=header)
                response = self.getGetHttpx( method, request_path, params, header)
        elif method == consts.POST:
            if self.APIKey != '-1':
                header = utils.get_header(self.APIKey)
                request_path = self.domain + request_path
                response = self.getGetHttpx( method, request_path, params, header)
                # response =requests.request(method, request_path, params=params, headers=header)
        elif method == consts.DELETE:
            if self.APIKey != '-1':
                header = utils.get_header(self.APIKey)
                request_path = self.domain + request_path
                response = self.getGetHttpx( method, request_path, params, header)
                # response =requests.request(method, request_path, params=params, headers=header)
        return response.json()

    def _getDepth(self, symbol, limit):
        params = {'symbol': symbol, 'limit': limit}
        return self._request(consts.GET, consts.DEPTH, params)
    def _getReponse(self,url):
        try:
            response = httpx.get(url)
            return response
        except:
            return self._getReponse(url)
    def _getSysTimeStamp(self):
        url = consts.DOMAIN + consts.SYSTIMESTAMP
        response = self._getReponse(url)
        return str(response.json()["serverTime"])

    def _getExchangeInfo(self, symbol):
        params = {'symbol': symbol}
        return self._request(consts.GET, consts.EXCHANGEINFO, params)

    def _getTrades(self, symbol, limit):
        params = {'symbol': symbol, 'limit': limit}
        return self._request(consts.GET, consts.TRADES, params)

    def _getAggTrades(self, symbol, startTime, endTime, limit):
        params = {'symbol': symbol, 'startTime': startTime, 'endTime': endTime, 'limit': limit}
        return self._request(consts.GET, consts.AGGTEADES, params)

    def _getKlines(self, symbol, interval, startTime, endTime, limit):
        params = {'symbol': symbol, 'interval': interval, 'startTime': startTime, 'endTime': endTime, 'limit': limit}
        return self._request(consts.GET, consts.KLINES, params)

    def _getAvgprice(self, symbol):
        params = {'symbol': symbol}
        return self._request(consts.GET, consts.AVGPRICE, params)

    def _get24Hr(self, symbol):
        params = {'symbol': symbol}
        return self._request(consts.GET, consts.TWENTYFOURHR, params)

    def _getPrice(self, symbol):
        params = {'symbol': symbol}
        return self._request(consts.GET, consts.PRICE, params)

    def _getBookTicker(self, symbol):
        params = {'symbol': symbol}
        return self._request(consts.GET, consts.BOOKTICKER, params)

    # def _sign_v3(self, req_time, sign_params=None):
    #     if sign_params:
    #         sign_params = urlencode(sign_params, quote_via=quote)
    #         to_sign = "{}&timestamp={}".format(sign_params, req_time)
    #     else:
    #         to_sign = "timestamp={}".format(req_time)
    #     sign = hmac.new(self.secret_key.encode('utf-8'), to_sign.encode('utf-8'), hashlib.sha256).hexdigest()
    #     return sign
    def _getParams(self, maps):
        params = {}
        for key, value in maps.items():
            if not value == '':
                params[key] = value
        return params

    def _getSelfSymbols(self):
        timeStamp = self._getSysTimeStamp()
        message = pre_hash(timeStamp, {})
        sign = signl(message, self.secret_key)
        params = {'signature': sign, 'timestamp': timeStamp}
        return self._request(consts.GET, consts.SELFSYMBOLS, params)

    def _postorder(self, symbol, side, type, quantity, quoteOrderQty, price, newClientOrderId, recvWindow, timestamp):
        timeStamp = self._getSysTimeStamp()
        maps = {'symbol': symbol,
                  'side': side,
                  'type': type,
                'quantity': quantity,
                  'quoteOrderQty': quoteOrderQty,
                  'price': price,
                'newClientOrderId':newClientOrderId,
                'recvWindow':recvWindow,
                'timestamp':timestamp
                  }
        params=self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        # paramss={'signature': sign,'symbol':symbol,'side':side,'type':type,'quantity':quantity,'quoteOrderQty':quoteOrderQty,
        #         'price':price,'newClientOrderId':newClientOrderId,'recvWindow':recvWindow,'timestamp':timeStamp}
        return self._request(consts.POST, consts.ORDER, params)

    def _getAccount(self, recvWindow=None, timestamp=None):
        timeStamp = self._getSysTimeStamp()
        params = {'recvWindow': recvWindow}
        if params:
            params['signature'] = self._sign_v3(req_time=timeStamp, sign_params=params)
        else:
            params = {}
            params['signature'] = self._sign_v3(req_time=timeStamp)
        params['timestamp'] = timeStamp
        return self._request(consts.GET, consts.ACCOUNT, params)
    def _postBatchorders(self,batchOrders):
        maps={
            'batchOrders':batchOrders
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.POST, consts.BATCHORDERS, params)
    def _deleteOrder(self,symbol,orderId,origClientOrderId,newClientOrderId,recvWindow,timestamp):
        maps={
            'symbol':symbol,
            'orderId':orderId,
            'origClientOrderId':origClientOrderId,
            'newClientOrderId':newClientOrderId,
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.DELETE, consts.ELETEORDER, params)
    def _deleteOpenOrder(self,symbol,recvWindow,timestamp):
        maps={
            'symbol':symbol,
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.DELETE, consts.OPENORDERS, params)
    def _getOrder(self,symbol,origClientOrderId,orderId,recvWindow,timestamp):
        maps={
            'symbol':symbol,
            'origClientOrderId':origClientOrderId,
            'orderId':orderId,
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.GET, consts.ORDER, params)
    def _getOpenOrder(self,symbol,recvWindow,timestamp):
        maps={
            'symbol':symbol,
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.GET, consts.OPENORDERS, params)
    def _getAllOrders(self,symbol,startTime,endTime,limit,recvWindow,timestamp):
        maps={
            'symbol':symbol,
            'startTime':startTime,
            'endTime':endTime,
            'limit':limit,
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.GET, consts.ALLORDERS, params)
    def _getACCOUNT(self,recvWindow,timestamp):
        maps={
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.GET, consts.ACCOUNT, params)
    def _getMyTrades(self,symbol,orderId,startTime,endTime,limit,recvWindow,timestamp):
        maps={
            'symbol':symbol,
            'orderId':orderId,
            'startTime':startTime,
            'endTime':endTime,
            'limit':limit,
            'recvWindow':recvWindow,
            'timestamp':timestamp
        }
        timeStamp = self._getSysTimeStamp()
        params = self._getParams(maps)
        message = pre_hash(timeStamp, params)
        sign = signl(message, self.secret_key)
        params['signature'] = sign
        params['timestamp'] = timeStamp
        return self._request(consts.GET, consts.MYTRADES, params)







if __name__ == '__main__':
    api_key = 'dcc90281-f342-4166-ad66-110a941ae3e6'
    secret_key = '9B2084459A62DC52E669482ED8DDBD0A'
    passphrase = '#Xsy103171'
    Client(api_key, secret_key, passphrase)._getSysTimeStamp()
