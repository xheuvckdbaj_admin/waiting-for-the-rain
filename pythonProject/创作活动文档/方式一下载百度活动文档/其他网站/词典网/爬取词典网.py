
import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_cidianwang(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.zd > h1')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.a_content')[0]).replace(title,keyStr)
        content=str(soup.select('div.a_content > h2')[0].text).strip()
        print(content)
        content2 = str(soup.select('div.a_content > div.sfimg > p > a')[0].text).strip()
        print(content2)
        list_soupa=list_soupa.replace(content,'').replace(content2,'').replace('书法图片','').replace('怎么写好看','')
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('词典网下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.cidianwang.com/cd/g/gulu214880.htm'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_cidianwang(url,htmlPath,keyStr,title)