import re

import requests


def getShopId(url,fileShop):
    try:
        print(url)
        source = requests.get(url).content
        htmlDaima=str(source,'utf-8')
        shopid = re.findall('\"shopId\":\"(.*?)\",\"shopName\":', htmlDaima)[0]
        uname=re.findall('\"uname\":\"(.*?)\",\"utype\":',htmlDaima)[0]
        print(shopid)
        print(uname)
        fileShop.write(uname+'------'+shopid+'\n')
    except Exception as e:
        print('该账号没有shopid')
def mainShopidAndUname(wangzhiPath,shopFilePath):
    fileWangzhi=open(wangzhiPath,'r',encoding='utf-8')
    fileShop = open(shopFilePath, 'w', encoding='utf-8')
    file_lines=fileWangzhi.readlines()
    fileWangzhi.close()
    for line in file_lines:
        url=line.replace('\n','').split('###')[-1]
        getShopId(url,fileShop)
    fileShop.close()
def main():
    wangzhiPath = r'./别人通过活动文档的网址.txt'
    shopFolder = r'D:\文档\百度店铺\shopidAndUname'
    shopFilePath = shopFolder + '\\' + 'shop4.txt'
    mainShopidAndUname(wangzhiPath, shopFilePath)
if __name__ == '__main__':
    main()