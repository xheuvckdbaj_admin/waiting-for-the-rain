# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.17fanwen.com/shiyongwen/huodongzongjie/90738.html',
                'https://www.17fanwen.com/shiyongwen/zhuanzhengshenqingshu/90275.html',
                'https://www.17fanwen.com/shiyongwen/ziwojianding/90754.html',
                'https://www.17fanwen.com/shiyongwen/ziwojieshao/90866.html',
                'https://www.17fanwen.com/shiyongwen/zhiyeguihua/90322.html',
                'https://www.17fanwen.com/shiyongwen/jianlimoban/86905.html',
                'https://www.17fanwen.com/shiyongwen/huodongcehua/90862.html',
                'https://www.17fanwen.com/shiyongwen/gongzuocehua/82405.html',
                'https://www.17fanwen.com/shiyongwen/gongzuofangan/90756.html',
                'https://www.17fanwen.com/shiyongwen/gongzuojihua/90852.html',
                'https://www.17fanwen.com/shiyongwen/nianzhongzongjie/90791.html',
                'https://www.17fanwen.com/shiyongwen/gongzuozongjie/90867.html',
                'https://www.17fanwen.com/zhici/huanyingci/90716.html',
                'https://www.17fanwen.com/zhici/daxieci/90861.html',
                'https://www.17fanwen.com/zhici/zhujiuci/90753.html',
                'https://www.17fanwen.com/zhici/heci/90232.html',
                'https://www.17fanwen.com/zhici/zhuchigao/90858.html',
                'https://www.17fanwen.com/zhici/fayangao/90782.html',
                'https://www.17fanwen.com/zhici/yanjianggao/90860.html',
                'https://www.17fanwen.com/baogao/xindetihui/90854.html',
                'https://www.17fanwen.com/baogao/kaiti/73822.html',
                'https://www.17fanwen.com/baogao/diaocha/89195.html',
                'https://www.17fanwen.com/baogao/shiyongqi/89007.html',
                'https://www.17fanwen.com/baogao/shijian/90316.html',
                'https://www.17fanwen.com/baogao/cizhi/90868.html',
                'https://www.17fanwen.com/baogao/shuzhi/90124.html',
                'https://www.17fanwen.com/baogao/shixi/90853.html',
                'https://www.17fanwen.com/shuxin/shenqingshu/90839.html',
                'https://www.17fanwen.com/shuxin/zerenshu/90382.html',
                'https://www.17fanwen.com/shuxin/jianyishu/90855.html',
                'https://www.17fanwen.com/shuxin/chengnuoshu/90849.html',
                'https://www.17fanwen.com/shuxin/changyishu/90845.html',
                'https://www.17fanwen.com/shuxin/baozhengshu/90847.html',
                'https://www.17fanwen.com/shuxin/jiantaoshu/90851.html',
                'https://www.17fanwen.com/shuxin/qiuzhixin/90863.html',
                'https://www.17fanwen.com/shuxin/weiwenxin/90857.html',
                'https://www.17fanwen.com/shuxin/daoqianxin/90864.html',
                'https://www.17fanwen.com/shuxin/biaoyangxin/90856.html',
                'https://www.17fanwen.com/shuxin/cizhixin/90822.html',
                'https://www.17fanwen.com/shuxin/jieshaoxin/89923.html',
                'https://www.17fanwen.com/shuxin/ganxiexin/90859.html',
                'https://www.17fanwen.com/jiaoxue/tingkebaogao/63113.html',
                'https://www.17fanwen.com/jiaoxue/shuokegao/89879.html',
                'https://www.17fanwen.com/jiaoxue/jiaoxuesheji/90758.html',
                'https://www.17fanwen.com/jiaoxue/jiaoxuexinde/90840.html',
                'https://www.17fanwen.com/jiaoxue/jiaoxuefansi/90836.html',
                'https://www.17fanwen.com/jiaoxue/jiaoxuezongjie/90723.html',
                'https://www.17fanwen.com/jiaoxue/jiaoxuejihua/90844.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
