# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm
import sys  # 导入sys模块
sys.setrecursionlimit(300000)
# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getContent(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.content')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title = str(soup.select('div.article > h1.title')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getupUrl(url,i,thmlpath):
    getContent(thmlpath, url)
    if i < 310:
        i += 1
        print('prev------------', i)
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.near_qx > div.prev > a')[0].get('href')
        getupUrl(href, i,thmlpath)
def getlowUrl(url,i,thmlpath):
    getContent(thmlpath, url)
    if i <310:
        i+=1
        print('next------------',i)
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.near_qx > div.next > a')[0].get('href')
        getlowUrl(href, i,thmlpath)

def main(txtPath,thmlpath):
    # 百度搜索“77
    # cn”或“免费范文网”即可找到本站免费阅读全部范文。收藏本站方便下次阅读，免费范文网，提供经典小说申报材料人民满意公务员先进集体申报材料在线全文阅读
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for line in lines:
        try:
            url=line.replace('\n','')
            print(url)
            i=0
            getlowUrl(url, i, thmlpath)
            getupUrl(url, i, thmlpath)
        except:
            print('error')
            continue

def getListUrl(file,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.list > div.item > div.title > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        file.write(href+'\n')
# def getTitleUrl(file,url):
#     requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
#     soup = BeautifulSoup(requests_text, 'lxml')
#     a_list = soup.select('div.header_box > ul.nav > li > a')
#     for i,a in enumerate(a_list):
#         if not i==0:
#             href=a.get('href')
#             getListUrl(file, href)
# def mainx(txtPath):
#     file=open(txtPath,'w')
#     url='https://www.qiuxuenet.com/'
#     # getTitleUrl(file, url)
#     file.close()
if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\爬取网站\求学网'
    # mainx(txtPath)
    main(txtPath,thmlpath)
