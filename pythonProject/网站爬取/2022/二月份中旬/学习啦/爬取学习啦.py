# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con_article.con_main')[0]).replace('<br/>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.ar_title >h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.list_rwap.bj_mt40 > ul.new.bj_mt20 > li.list_itme2 > a.itme_bt')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='https:'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('error')
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    pageContent = soup.select('div.pnum > ul > a')[-1].text
    allPage=int(re.findall('共(.*?)页',pageContent)[0])
    url=strUrl+'list_%d.html'
    for i in range(1,allPage+1):
        newUrl=url%i
        getListUrl(thmlpath, newUrl)
def getMoreUrl2(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm.lm_te.louti > div.lm_title > a.more')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='https:'+href
        getPageUrl(thmlpath, newUrl)
def getMoreUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm.lm_te.louti > div.lm_title > a.more')
    for i,a in enumerate(a_list):
        href=a.get('href')
        hrefUrl='https:'+href
        try:
            getPageUrl(thmlpath, hrefUrl)
        except:
            getMoreUrl2(thmlpath, hrefUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.in_nav.main > ul.nav_wrap > li.nav_item.nli.ts.fl > span > a')
    for i,a in enumerate(a_list):
        if i >1:
            href=a.get('href')
            newUrl='https:'+href
            try:
                getPageUrl(thmlpath, newUrl)
            except:
                getMoreUrl(thmlpath,newUrl)
def main(thmlpath):
            #打开文本图片集
            file=open('./lianjie.txt','r')
            file_lines=file.readlines()
            for line in file_lines:
                url=line.replace('\n','')
                getTitleUrl(thmlpath, url)



if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
