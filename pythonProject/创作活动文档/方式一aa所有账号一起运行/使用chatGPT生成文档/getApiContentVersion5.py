import openai

def getApiContentByTitleAndKey(api_key,prompt):
    try:
        openai.api_key = api_key
        model_engine = "gpt-3.5-turbo"
        # answer_language='zh-CN'
        # try:
        completions = openai.ChatCompletion.create(
            model=model_engine,
            messages=[
                # {'role':'system','content':'你是一个文档生产者。'},
                {'role': 'user', 'content': prompt}
            ]
        )

        # message = completions.choices[0].text
        # print(completions)
        result = ''
        for choice in completions.choices:
            result += choice.message.content
        # print(result)
        return result
    except Exception as e:
        return str(e)

def main():
    api_key = "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"
    # prompt = "以八年级下册学生的角度,写一篇交通安全为题英语作文，参考网上下载最多的范文，高质量仿写不少于1200字"
    prompt = "以我心目中的中国形象为题写一篇英语作文，参考网上下载最多的范文，高质量仿写不少于1200字"
    apiContent=getApiContentByTitleAndKey(api_key,prompt)
    print(apiContent)
if __name__ == '__main__':
    main()