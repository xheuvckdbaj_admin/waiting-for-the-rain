import os
import re
import time

import requests
from playwright.sync_api import Playwright, sync_playwright, expect

def getShopId(url,fileShop,page):
    try:
        print(url)
        page.goto(url)
        time.sleep(2)
        htmlDaima=page.content()
        # source = requests.get(url).content
        # htmlDaima=str(source,'utf-8')
        shopid = re.findall('\"shopId\":\"(.*?)\",\"shopName\":', htmlDaima)[0]
        uname=re.findall('\"uname\":\"(.*?)\",\"utype\":',htmlDaima)[0]
        print(shopid)
        print(uname)
        fileShop.write(uname+'------'+shopid+'\n')
    except Exception as e:
        print('该账号没有shopid')
def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
def mainShopidAndUname(wangzhiPath,shopFilePath,page):
    fileWangzhi=open(wangzhiPath,'r',encoding='utf-8')
    fileShop = open(shopFilePath, 'w', encoding='utf-8')
    file_lines=fileWangzhi.readlines()
    fileWangzhi.close()
    for line in file_lines:
        url=line.replace('\n','').split('###')[-1]
        getShopId(url,fileShop,page)
        time.sleep(5)
    fileShop.close()
def run(playwright: Playwright,wangzhiPath, shopFilePath):
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    mainShopidAndUname(wangzhiPath, shopFilePath,page)
def mainRun(wangzhiPath, shopFilePath):
    with sync_playwright() as playwright:
        run(playwright,wangzhiPath, shopFilePath)

def main():
    wangzhiPath = r'./别人通过活动文档的网址.txt'
    shopFolder = r'D:\文档\百度店铺\shopidAndUname'
    makeFolder(shopFolder)
    shopFilePath = shopFolder + '\\' + 'shop4.txt'
    mainRun( wangzhiPath, shopFilePath)
if __name__ == '__main__':
    main()