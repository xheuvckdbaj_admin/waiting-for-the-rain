# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.kt250.com/fanwen/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
    try:
        print(url)
        time.sleep(1)
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('.content > div > h1')[0].text.replace('\r','').replace('\n','').replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','').strip()
        print(title)
        list_soupa = str(soup.select('.article-article')[0])

        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('错误')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = requests.get(url=url, headers=headers)
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.news-list-left-content > ul > li > h2 > a')
    #print(url_list)
    for a in a_list:
            href=a.get('href')
            newUrl='https://www.51test.net'+href
            print(newUrl)
            title = a.text
            getContent(thmlpath,newUrl,title)
def getUrlIndex(thmlpath,url,number):
   for i in range(number,1500000):
       print(i)
       newUrl=url%i
       getContent(thmlpath, newUrl)

def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2):


                    oldUrl = url
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)







def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            getUrl(thmlpath, url)
        except:
            continue


def main(thmlpath):
        number=69543
        start=69543
        url = 'https://www.zqwdw.com/gongzuojihua/2021/0709/%d.html'
        getUrlIndex(thmlpath,url,number)


if __name__ == '__main__':
    thmlpath = r'E:\文档\大文斗范文网'
    main(thmlpath)
