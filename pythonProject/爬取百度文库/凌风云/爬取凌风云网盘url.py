# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLingcookies
import getToken
import ipAgency
import addProxyid
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r".\cookies.txt"
filePath1=r".\ip网址.txt"
def getBrowser(browser,newUrl):
    # try:
        browser = addProxyid.addId(browser,filePath1)
        time.sleep(1)
        browser.get(newUrl)
        return browser
    # except:
    #     print('网络延迟')
    #     try:
    #         browser.close()
    #     except:
    #        print('browser为空')
    #     browser=getBrowser(browser,newUrl)
    #     return browser
def getElement(browser,urls,file_handle):
    for e in range(0, len(urls)):
        print(urls[e])
        try:
            browser.get(urls[e])
        except:
            print('没网了')
            continue
        elements = browser.find_elements_by_css_selector('div.x-box__link > a.x-button.x-button--info')
        if len(elements) == 2:
            elements[1].click()
            browser.close()
        elif len(elements) == 1:
            elements[0].click()
            browser.close()
        elif len(elements) == 0:
            continue
        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        browser.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="button"]'))
            )
        except:
            continue
        try:
            hf = browser.find_element_by_css_selector(
                'div.button > a.x-button.x-button--primary').get_attribute('href')
        except:
            continue
        print(hf)
        file_handle.write(hf + '\n')


def getLinUrl(browser,cisu,law,url,so_token,key,ft,way,file_handle):
        error=law
    # try:
    #     browser = getBrowser(browser,r'https://www.lingfengyun.com/')
        # 添加cookies
        browser.get(r'https://www.lingfengyun.com/')
        addLingcookies.addCookies(browser, r'https://www.lingfengyun.com/', filePath)
        newUrl = url % (1, key,so_token, 'baidu_pan', ft, way)
        print(newUrl)
        browser.get(newUrl)
        elements=browser.find_elements_by_xpath('//div[@class="main__right"]/div/div[@class="fy"]/ul/a')
        number=elements[len(elements)-2].find_element_by_css_selector('li').text
        html = browser.page_source
        cishu=(int(number)+19)//20
        for i in range(law,cishu):
                first=i*20+1
                error=i
                print(cisu)
                print('page:',first)
                newUrl = url % (first, key,so_token, 'all_pan', ft, way)
                browser.get(newUrl)
                hrefs=[]
                for page in range(0, 35):
                    if not page==0:
                        next_btn=browser.find_element_by_xpath('//div[@class="x-box"]/div[@class="x-box__main"]/div[@class="x-box__tags"]/span[3]/a')
                        browser.execute_script("arguments[0].click();", next_btn)
                        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                        print('当前句柄: ', n)  # 会打印所有的句柄
                        # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
                        browser.switch_to.window(n[-1])
                        time.sleep(0.01)
                        try:
                            element = WebDriverWait(browser, 10).until(
                                EC.presence_of_element_located((By.XPATH, '//div[@class="x-where"]'))
                            )
                        except:
                            print('没网络')
                            browser.quit()
                            browser=None
                            getLinUrl(browser,cisu,error, url, so_token,key, ft, way, file_handle)
                    try:
                            hf = browser.find_element_by_css_selector('div.x-box > div.x-box__main > a.x-box__title').get_attribute('href')
                    except:
                        break

                    hrefs.append(hf)

                getElement(browser, hrefs, file_handle)

    # except:
    #     print('cookies过期了')
    #     try:
    #         browser.quit()
    #     except:
    #         print('browser关闭错误')
    #     try:
    #         browser = webdriver.Chrome()
    #     except:
    #         print('browser出错')
    #         time.sleep(100)
    #         browser = webdriver.Chrome()
    #     token=getToken.getToken(browser)
    #     browser.quit()
    #     browser=None
    #     getLinUrl(browser,cisu,error, url, token,key, ft, way, file_handle)

def login(browser,law,so_token,key,number,path):

    url='https://www.lingfengyun.com/search?page=%d&wd=%s&so_token=%s&so_file=wang_pan&so_source=%s&so_ext=%s&so_array=%s&so_accur=100'
    formats=['6','7','8','12','13']
    ways=[
        'file_asc','file_desc','sdate_asc','sdate_desc','fdate_asc','fdate_asc'
    ]
    map = []
    for formatNum in range(0,5):
        for wayNum in range(0,6):
            st = formats[formatNum] + '-' + ways[wayNum]
            map.append(st)
    for i in range(number, 30):
            newPath = path + '\\' + key + str(time.time()) + '.txt'
            file_handle = open(newPath, mode='w')
            print(i)
            string = map[i]
            ft = string.split('-')[0]
            way = string.split('-')[1]
            getLinUrl(browser,i,law, url,so_token, key, ft, way, file_handle)
            law=0
            file_handle.close()

def main():
            formatNumber=1
            wayNumber=1
            strUrl='     '
            key='第一课'
            #getHtml(thmlpath, strUrl,number)
            #getContent(thmlpath, strUrl)
            path=r'D:\文档\url'
            # number初始值0,law初始值为0
            number=8
            law=0
            so_token='1d26a9b0347a437fc1ac4edeb0936235'
            browser = webdriver.Chrome()
            # browser = None
            login(browser,law,so_token,key,number,path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
