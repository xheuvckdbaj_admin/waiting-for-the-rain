import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_diyifanwen(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=50.0).content.decode(encoding='gbk'))
        except:
            requests_text = str(requests.get(url=url, headers=headers, timeout=50.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div > h1')[0].text 
        
        try:
            list_soupa=str(soup.select('div.content')[0]).replace('相关推荐','').replace(title,title)
        except:
            content1=str(soup.select('#artinfo')[0])
            # content2 = str(soup.select('div.download_card')[0])
            list_soupa = str(soup.select('#ArtContent')[0]).replace('相关推荐', '').replace(content1, '').replace(title, title).split('推荐阅读：')[0]
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('第一范文网下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.diyifanwen.com/yanjianggao/fayangao/3305319.html'
    htmlPath=r'D:\文档\百度付费上传文档\百度html'
    keyStr='kaixin'
    title=''
    mian_diyifanwen(url,htmlPath,keyStr,title)