# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.content')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.article > h1.title')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.list_news > ul > li > h2 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.wendang123.cn'+href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,strUrl):
    getListUrl(thmlpath, strUrl)
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a = soup.select('div.page > a')[-1]
    href=a.get('href')
    print(href)
    allPage=int(href.split('_')[-1].replace('.html',''))
    hrefUrl=strUrl+'index_%d.html'
    for i in range(2,allPage+1):
        newUrl=hrefUrl%i
        getListUrl(thmlpath, newUrl)



def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.channellist > div.top_news > div.comtitle > h2 > a')
    for a in a_list:
                href=a.get('href')
                newUrl = 'http://www.wendang123.cn' + href
                getPageUrl(thmlpath, newUrl)
def main(txtPath,thmlpath):
    file=open(txtPath,'r')
    file_lines=file.readlines()
    for line in file_lines:
        url=line.replace('\n','')
        getTitleUrl(thmlpath,url)


if __name__ == '__main__':
    txtPath='./lianjie.txt'
    thmlpath = r'E:\文档\自查报告网'
    main(txtPath,thmlpath)
