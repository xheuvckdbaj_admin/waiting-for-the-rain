import re
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver


headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_jiaoanwang(url,htmlPath,keyStr,title):
    try:
        driver = getDriver('jiao')
        driver.get(url)
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, '//div[@class="hd"]/h1')
            )
        )
        title = driver.find_element(By.XPATH, '//div[@class="hd"]/h1').text
        title=re.sub('（(.*)）', '', title)
        title = re.sub('\d+篇', '', title)
        list_soupa = str(driver.find_element(By.XPATH,'//div[@class="news-body"]').get_attribute('innerHTML').replace(title, keyStr))
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('教案网下载成功----', keyStr)
        f.close()
        driver.quit()
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://www.jiaoanw.com/%E6%95%99%E6%A1%88%E6%A0%BC%E5%BC%8F/article-473182-1.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_jiaoanwang(url,htmlPath,keyStr,title)