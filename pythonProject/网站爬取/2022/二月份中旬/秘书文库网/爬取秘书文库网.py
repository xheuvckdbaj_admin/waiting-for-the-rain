# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import sys
sys.setrecursionlimit(5000)
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.post_body.mt-3')[0]).replace('<br/>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.post_title > h1.text-left.mt-4')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.list-page.mt-3 > ul > li > h4 > a')
    filePath=thmlpath+'\\'+str(time.time())+'.txt'
    file = open(filePath, 'w')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='http://www.mishu323.com'+href
        print(newUrl)
        file.write(newUrl+'\n')
        # getHtml(thmlpath, newUrl)
    file.close()
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    pageContent = soup.select('div.pd_c_xslb_left_fenye > ul > li > a')[-1].text
    if '末页'==pageContent:
        href=soup.select('div.pd_c_xslb_left_fenye > ul > li > a')[-1].get('href')
        allPageNumber=href.split('_')[-1].replace('.html','')
        url=strUrl+href.replace(href.split('_')[-1],'')
        for i in range(1,allPageNumber+1):
            newUrl=url+str(i)+'.html'
            getListUrl(thmlpath, newUrl)
    elif '下一页'==pageContent:
        href = soup.select('div.pd_c_xslb_left_fenye > ul > li > a')[-1].get('href')
        allPageNumber = int(href.split('_')[-2].replace('.html', ''))
        url = strUrl + href.replace(href.split('_')[-1], '')
        for i in range(1, allPageNumber + 1):
            newUrl = url + str(i) + '.html'
            getListUrl(thmlpath, newUrl)
def getTitleUrl2(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('ul.navbar-nav > li.nav-item > a.nav-link.text-white')
    for i,a in enumerate(a_list):
        if i > 0:
            href = a.get('href')
            newUrl = 'http://www.mishu323.com' + href
            getListUrl(thmlpath,newUrl)
def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.dropdown-menu > a.dropdown-item')
    for a in a_list:
                href=a.get('href')
                newUrl='http://www.mishu323.com'+href
                getListUrl(thmlpath,newUrl)
def getTopUrl(thmlpath, strUrl,topNumber):
    if topNumber <2200:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href= soup.select('div.mt-3 > p > a')[0].get('href')
        url='http://www.mishu323.com'+href
        try:
            getHtml(thmlpath, url)
            topNumber+=1
            print(url)
            getTopUrl(thmlpath, url, topNumber)
        except:
            print('完成')
def getDrownUrl(thmlpath, strUrl,drownNumber):
    if drownNumber <2200:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href= soup.select('div.mt-3 > p > a')[-1].get('href')
        url='http://www.mishu323.com'+href
        print(url)
        try:
            getHtml(thmlpath, url)
            drownNumber+=1
            getDrownUrl(thmlpath, url, drownNumber)
        except:
            print('完成')
def main(thmlpath):

                url='http://www.mishu323.com/'
                getTitleUrl(thmlpath,url)
                getTitleUrl2(thmlpath, url)

def mainx(thmlpath,txtPath):
    file=open(txtPath,'r')
    file_lines=file.readlines()
    for i,line in enumerate(file_lines):
        url=line.replace('\n','')
        getHtml(thmlpath, url)
        topNumber=0
        drownNumber=0
        getTopUrl(thmlpath, url,topNumber)
        getDrownUrl(thmlpath, url, drownNumber)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    txtPath=r'./lianjie.txt'
    # main(thmlpath)
    mainx(thmlpath,txtPath)