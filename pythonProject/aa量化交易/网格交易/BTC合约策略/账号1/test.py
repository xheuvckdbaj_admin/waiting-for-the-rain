import time
from decimal import Decimal

from okx import Account, Funding, Trade

from pythonProject.aa量化交易.网格交易.BTC合约策略.账号1.所有币每日合约下单 import mainCoinSwapAll

def getLossCoins(accountAPI):
    # 查询所做的所有币的合约
    result_positions = accountAPI.get_positions('SWAP', '')
    result_positions['data']
    # 筛选出损失的合约
    lossCoins = []
    # 筛选出盈利的合约
    profitCoins = []
    for i, position in enumerate(result_positions['data']):
        coin = {}
        print(position)
        # 持仓币种
        instId = position['instId']
        # 持仓数量
        pos = position['pos']
        # 未实现收益（以标记价格计算）
        upl = position['upl']
        # 未实现收益率（以标记价格计算
        uplRatio = position['uplRatio']
        # 预估强平价
        liqPx = position['liqPx']
        # 最新成交价格
        last = position['last']
        # 最新标记价格
        markPx = position['markPx']
        coin['instId'] = instId
        coin['pos'] = pos
        coin['upl'] = upl
        coin['uplRatio'] = uplRatio
        coin['liqPx'] = liqPx
        coin['last'] = last
        coin['markPx'] = markPx
        print('持仓币种', instId, '未实现收益', upl, '未实现收益率', uplRatio, '最新标记价格', markPx)
        # 筛选出损失和盈利的合约
        if Decimal(uplRatio) < Decimal(0):
            lossCoins.append(coin)
        else:
            profitCoins.append(coin)
    return lossCoins
def runCoinContract(api_key, secret_key, passphrase, flag):
    accountAPI = Account.AccountAPI(api_key, secret_key, passphrase, False, flag)
    #判断
    if len(getLossCoins(accountAPI))< 10:
        #所有币合约下单
        # mainCoinSwapAll()
        # 获取账号余额，可转余额，币种占用金额，未实现盈亏
        result_balance = accountAPI.get_account_balance('USDT')
        # 可用余额
        availBal = result_balance['data'][0]['details'][0]['availBal']
        # 币种余额
        cashBal = result_balance['data'][0]['details'][0]['cashBal']
        # 币种权益美金价值
        eqUsd = result_balance['data'][0]['details'][0]['eqUsd']
        # 币种占用金额
        frozenBal = result_balance['data'][0]['details'][0]['frozenBal']
        # 未实现盈亏
        upl = result_balance['data'][0]['details'][0]['upl']
        # 交易账户转到资金账户
        fundingAPI = Funding.FundingAPI(api_key, secret_key, passphrase, False, flag)
        result_transfer = fundingAPI.funds_transfer('USDT', str(int(float(availBal))), '18', '6', '0', '', '', '', '')
        print(result_transfer)
        # time.sleep(64800)
        # 查询所做的所有币的合约
        result_positions = accountAPI.get_positions('SWAP', '')
        result_positions['data']
        # 筛选出损失的合约
        lossCoins = []
        # 筛选出盈利的合约
        profitCoins = []
        for i,position in enumerate(result_positions['data']):
            coin= {}
            print(position)
            # 持仓币种
            instId = position['instId']
            # 持仓数量
            pos = position['pos']
            # 未实现收益（以标记价格计算）
            upl = position['upl']
            # 未实现收益率（以标记价格计算
            uplRatio = position['uplRatio']
            # 预估强平价
            liqPx = position['liqPx']
            #最新成交价格
            last=position['last']
            #最新标记价格
            markPx=position['markPx']
            coin['instId']=instId
            coin['pos'] = pos
            coin['upl'] = upl
            coin['uplRatio'] = uplRatio
            coin['liqPx'] = liqPx
            coin['last'] = last
            coin['markPx'] = markPx
            print('持仓币种', instId, '未实现收益', upl, '未实现收益率', uplRatio,'最新标记价格',markPx)
            # 筛选出损失和盈利的合约
            if Decimal(uplRatio) < Decimal(0):
                lossCoins.append(coin)
            else:
                profitCoins.append(coin)
        # 亏损的合约需要添加保证金或加仓合约
        # 盈利的合约，看情况进行修改止盈
        # 查询历史订单
        tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
        # 查询历史止盈
        result_amend_order = tradeAPI.order_algos_list('conditional', '', '', '', '', '', '', '')
        for data in result_amend_order['data']:
            for i,coin in enumerate(profitCoins):
                if data['instId'] == coin['instId']:
                    # 计算新的止盈价格
                    newTpOrdPx=Decimal(coin['markPx'])*Decimal(1.005)
                    print(data['instId'], data['algoId'], data['tpTriggerPx'])
                    result_algo_order = tradeAPI.amend_algo_order(data['instId'], data['algoId'], '', '', '', '',
                                                                  str(newTpOrdPx), str(newTpOrdPx), '', '', '', '')
        # time.sleep(21000)
        # # 取消当前所有委托
        # # (1)查询委托合约
        # result_order = tradeAPI.get_order_list('SWAP', '', '', 'limit', '', '', '', '', '')
        # for data in result_order['data']:
        #     print(data['instId'], data['ordId'])
        #     # 取消当前委托
        #     result_cancel_order = tradeAPI.cancel_order(data['instId'], data['ordId'], '')
        #     if not result_cancel_order['code'] == '0':
        #         result_cancel_order = tradeAPI.cancel_order(data['instId'], data['ordId'], '')


def main():
    flag = "0"  # live trading: 0, demo trading: 1
    api_key = 'dcc90281-f342-4166-ad66-110a941ae3e6'
    secret_key = '9B2084459A62DC52E669482ED8DDBD0A'
    passphrase = '#Xsy103171'
    # for i in range(0,100):
    runCoinContract(api_key, secret_key, passphrase, flag)


if __name__ == '__main__':
    main()
