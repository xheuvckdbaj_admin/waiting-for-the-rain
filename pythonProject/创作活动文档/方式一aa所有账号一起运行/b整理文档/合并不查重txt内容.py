import os

from pythonProject.FindFile import find_file


def merge(folderpath,path):
    txtpaths=find_file(folderpath)
    newTxtPath=path+'\\'+'all.txt'
    txtfile_line=open(newTxtPath,'w',encoding='utf-8')
    for txtpath in txtpaths:
        if txtpath.endswith('.txt'):
            print(txtpath)
            try:
                file_line=open(txtpath,'r',encoding="utf-8")
                readlines=file_line.readlines()
                for line in readlines:
                    txtfile_line.write(line)
                file_line.close()
                # os.remove(txtpath)
            except:
                try:
                    file_line = open(txtpath, 'r', encoding="gbk")
                    readlines = file_line.readlines()
                    for line in readlines:
                        txtfile_line.write(line)
                    file_line.close()
                    # os.remove(txtpath)
                except:
                    print('gbk读不出')
                    continue

    txtfile_line.close()
def main():
    folderpath=r'D:\文档\百度付费上传文档\标题\各个电脑'
    path=r'D:\文档\百度付费上传文档\标题\各个电脑'
    merge(folderpath,path)
if __name__ == '__main__':
    main()