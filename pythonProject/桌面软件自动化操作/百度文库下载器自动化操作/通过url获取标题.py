import time


from playwright.sync_api import sync_playwright, Playwright


def run(playwright:Playwright, url):
    browser = playwright.chromium.launch(headless=True)
    context = browser.new_context()
    page = context.new_page()
    page.goto(url)
    time.sleep(0.2)
    title=page.locator('//div[@class="doc-title-text"]').text_content().strip()
    print(title)
    return title

def getTitle(txtPath,titlePath,url):
    with sync_playwright() as playwright:
        title=run(playwright,url)
    return title
def main():
    txtPath=r'./url.txt'
    titlePath=r'./urlAndtitle.txt'
    txtFile = open(txtPath, 'r', encoding='utf-8')
    file_lines = txtFile.readlines()
    titleFile=open(titlePath,'w',encoding='utf-8')
    for line in file_lines:
        try:
            url = line.replace('\n', '')
            title=getTitle(txtPath,titlePath,url)
            titleFile.write(url+'******'+title+'\n')
        except:
            print('error')
    txtFile.close()
    titleFile.close()

if __name__ == '__main__':
    main()