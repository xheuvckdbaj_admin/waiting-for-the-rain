import time
import urllib

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}



def getUrlAndTitle(driver,urls,keyStr):
    try:
        elements = driver.find_elements(By.XPATH,
            '//div[@class="vrwrap"]/div[@class="struct201102"]/h3/a')
        for i, element in enumerate(elements):
            try:
                title = element.text.replace(' ', '').strip()
                if keyStr[0:3] in title and (keyStr[-2:] in title or '字' in keyStr[-2:]):
                    href = element.get_attribute('href')
                    hrefUrl = href
                    print(hrefUrl)
                    lineStr = hrefUrl + '————' + keyStr
                    urls.append(lineStr)
                    break
            except:
                print('error')
                continue
    except:
        print('error')
def getPageUrl(key,url,driver,urls):
    try:
        keyStr=key.replace('/','')
        driver.get(url)
        time.sleep(0.1)
        # driver.find_element(By.XPATH,'//div[@class="search-cont clearfix"]/form[@class="search-form"]/input[@class="hdi"]').send_keys(key)
        # time.sleep(0.1)
        # driver.find_element(By.XPATH,'//div[@class="search-cont clearfix"]/form[@class="search-form"]/button[@class="btn-global"]').click()
        # print(key)
        # n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        # driver.switch_to.window(n[-1])
        # time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="vrwrap"]/div[@class="struct201102"]/h3/a'))
            )
        except:
            time.sleep(0.1)
        getUrlAndTitle(driver,urls,keyStr)
    except:
        print('error')

def getKey(txtPath,url,driver,urls):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i,line in enumerate(lines):
        if i > -1:
            if 2 == i % 25:
                try:
                    driver.quit()
                    driver = getDriver(str(i))
                except:
                    print('关闭失败')

            key=line.replace('\n','')
            newUrl=url%key
            print('-----------------',key)
            getPageUrl(key, newUrl,driver,urls)
    # pathFile.close()
def mainSougou(txtPath):
    urls=[]
    driver = getDriver('souUrl')
    url='https://www.sogou.com/sogou?query=%s&ie=utf8&insite=wenwen.sogou.com&pid=sogou-wsse-a9e18cb5dd9d3ab4&rcer='
    txtFilePaths=find_file(txtPath)
    for txtFilePath in txtFilePaths:
        getKey(txtFilePath, url,driver,urls)
    driver.quit()
    return urls


if __name__ == '__main__':
    txtPath = r'E:\文档\百度活动文档\全网获取文档\全网url'
    mainSougou(txtPath)
