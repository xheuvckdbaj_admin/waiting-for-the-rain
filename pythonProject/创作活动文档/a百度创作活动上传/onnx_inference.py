import onnxruntime
import cv2
import numpy as np
import torchvision.transforms as T


class Predict:
    def __init__(self,model_path):
        self.transform = T.Compose([
            T.ToTensor(),
            T.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
        self.session = onnxruntime.InferenceSession(model_path)
    def to_rgb(self,im):
        """
        图片转 RGB
        :param im: 图片 np.array
        :return: 图片 np.array
        """
        if len(im.shape) == 2:  # 灰度
            return cv2.cvtColor(im, cv2.COLOR_GRAY2RGB)
        elif im.shape[2] == 4:  # PNG (RGBA)
            return cv2.cvtColor(im, cv2.COLOR_BGRA2RGB)
        assert len(im.shape) == 3 and im.shape[2] == 3  # 如果 Channel 不为3, 肯定出问题了
        return cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

    def preproc(self,img, input_size, swap=(2, 0, 1)):
        if len(img.shape) == 3:
            padded_img = np.ones((input_size[0], input_size[1], 3), dtype=np.uint8) * 114
        else:
            padded_img = np.ones(input_size, dtype=np.uint8) * 114

        r = min(input_size[0] / img.shape[0], input_size[1] / img.shape[1])
        resized_img = cv2.resize(
            img,
            (int(img.shape[1] * r), int(img.shape[0] * r)),
            interpolation=cv2.INTER_LINEAR,
        ).astype(np.uint8)
        padded_img[: int(img.shape[0] * r), : int(img.shape[1] * r)] = resized_img

        padded_img = padded_img.transpose(swap)
        padded_img = np.ascontiguousarray(padded_img, dtype=np.float32)
        return padded_img, r
    def __call__(self, img):
        img = cv2.resize(img, (128, 128))
        img = self.to_rgb(img)
        mask = np.zeros((128, 128), dtype=np.uint8)
        cv2.circle(mask, (64, 64), 62, 255, -1)
        img[mask != 255] = 0

        img = self.transform(img)
        img = np.array(img, np.float32)
        # img, ratio = preproc(img, (128,128))

        ort_inputs = {self.session.get_inputs()[0].name: img[None, :, :, :]}
        output = self.session.run(None, ort_inputs)
        angle = int(output[0].argmax(1)[0])
        return angle

if __name__ == '__main__':
    ocr = Predict('rotate.onnx')
    img = np.fromfile(r"D:\文档\百度付费上传文档\代码\haha1709456750.3357866.jpg",np.uint8)
    img = cv2.imdecode(img,cv2.IMREAD_COLOR)
    angle = ocr(img)
    print(angle)

    # (h, w) = img.shape[:2]
    # # 计算旋转中心和旋转角度
    # center = (w // 2, h // 2)
    # # 构建旋转矩阵
    # M = cv2.getRotationMatrix2D(center, -angle, 1.0)
    #
    # # 应用旋转变换
    # img = cv2.warpAffine(img, M, (w, h))
    # cv2.imshow('img',img)
    # cv2.waitKey(0)

