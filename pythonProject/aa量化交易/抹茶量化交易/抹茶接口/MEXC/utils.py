import base64
import hashlib
import hmac
from urllib.parse import urlencode, quote

import httpx
from pythonProject.aa量化交易.抹茶量化交易.抹茶接口.MEXC import consts
from pythonProject.aa量化交易.抹茶量化交易.抹茶接口.MEXC import Client


def parse_params_to_str(params):
    url = '?'
    for key, value in params.items():
        if value!='':
            url = url + str(key) + '=' + str(value) + '&'
    url = url[0:-1]
    # print(url)
    return url


def get_header(api_key):
    header = dict()
    header[consts.OK_ACCESS_KEY] = api_key
    header[consts.CONTENT_TYPE] = consts.APPLICATION_JSON
    # header['signature'] = sign
    # header['timestamp']=timestamp
    # & recvWindow = 5000 & timestamp = 1644489390087
    # header['timestamp'] = str(timestamp)
    # header['recvWindow']='5000'
    return header


def getTimestamp(AccessKey, SecretKey):
    client = Client.Client(AccessKey, SecretKey)
    response = client._getSysTimeStamp()


def signl(message, secretKey):
    # mac = hmac.new(bytes(secretKey, encoding='utf8'), bytes(message, encoding='utf-8'), digestmod='sha256')
    # d = mac.digest()
    sign = hmac.new(secretKey.encode('utf-8'), message.encode('utf-8'), hashlib.sha256).hexdigest()
    return sign


def pre_hash(timestamp, params):
    paramsString = parse_params_to_str(params).replace('?', '')
    if paramsString=='':
        to_sign = "timestamp={}".format(timestamp)
        return to_sign

    else:
        sign_params = urlencode(params, quote_via=quote)
        to_sign = "{}&timestamp={}".format(sign_params, timestamp)
        return to_sign


if __name__ == '__main__':
    params = {'hello': 'hello', 'java': 'java'}
    parse_params_to_str(params)
