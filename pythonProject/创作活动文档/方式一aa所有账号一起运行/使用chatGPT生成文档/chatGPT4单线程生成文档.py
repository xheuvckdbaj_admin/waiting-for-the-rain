import os

from pythonProject.b百度上传付费文档.使用chatGPT生成文档.chatGPT4生成单个文件 import main_chatGPT4Single


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)

def makeFile(titleKey, folderPath, apiFile_lines, i,passFile):
    content = main_chatGPT4Single(titleKey, folderPath, apiFile_lines[i].replace('\n', ''))
    if 'You exceeded your current quota' in content or 'please check your plan' in content or 'chatGPT额度用光了' in content:
        passFile.write(apiFile_lines[i])
        i = i + 1
        print(i)
        makeFile(titleKey, folderPath, apiFile_lines, i,passFile)
    return i


def getKeyTitle(apiKeyPath, titlePath, folderPath,passApiKeyPath):
    titleFile = open(titlePath, 'r', encoding='utf-8')
    passFile=open(passApiKeyPath, 'w', encoding='utf-8')
    titleFile_lines = titleFile.readlines()
    i = 0
    apiFile = open(apiKeyPath, 'r', encoding='utf-8')
    apiFile_lines = apiFile.readlines()
    for line in titleFile_lines:
        titleKey = line.replace('\n', '')
        print(titleKey)
        i = makeFile(titleKey, folderPath, apiFile_lines, i,passFile)
        i=i+1
        if i >= len(apiFile_lines):
            i=0
    apiFile.close()
    titleFile.close()
    passFile.close()



def main():
    apiKeyPath = r'./api_key.txt'
    passApiKeyPath = r'./passApiKey.txt'
    titlePath = r'./标题.txt'
    folderPath = r'D:\文档\百度付费上传文档\百度html\其他网站'
    makeFolder(folderPath)
    getKeyTitle(apiKeyPath, titlePath, folderPath,passApiKeyPath)


if __name__ == '__main__':
    main()
