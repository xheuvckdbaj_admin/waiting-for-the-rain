# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con_article.f16.c6')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.ar_con_title.bj_mt30 > h1.f30')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getAllUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('a.f18.bt_a.nobold.chao')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.xiegw.cn'+href
        try:
            getHtml(thmlpath, newUrl)
        except:
            print('错误')
            continue
def getPageUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.pnum > ul > a')[-2].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    url=href.replace(href.split('_')[-1],'')
    hrefUrl='http://www.xiegw.cn'+url
    for i in range(2,pageNumber+1):
        newUrl=hrefUrl+str(i)+'.html'
        getAllUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm_head > div.fl > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.xiegw.cn'+href
        getAllUrl(thmlpath, newUrl)
        try:
            getPageUrl(thmlpath, newUrl)
        except:
            print('error')
            continue
def main(thmlpath):
                url='http://www.xiegw.cn/xindetihui/'
                getTitleUrl(thmlpath, url)
                # getTitleUrl2(thmlpath, url)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
