# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getContent(contentText):
    contentText1=re.sub('<div class="info">(.*?)</div>', '',contentText)
    contentText2 = re.sub('<div class="gg2">(.*?)</div>', '', contentText1)
    contentText3 = re.sub('<div class="gg3">(.*?)</div>', '', contentText2)
    contentText=contentText3.split('gg4')[0]
    return contentText
def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con_article.f16.c6')[0]).replace('<br/>','</p><p>')
    contentText=getContent(contentText)
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.ar_con_title.bj_mt30.louti > h1.f30')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(txtPath,thmlpath):
            file=open(txtPath,'r')
            lines=file.readlines()
            for line in lines:
                url=line.replace('\n','')
                print(url)
                for i in range(1,50000):
                    print('---------',i)
                    try:
                        newUrl=url+'/'+str(i)+'.html'
                        getHtml(thmlpath, newUrl)
                    except:
                        print('error')
                        continue

if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\爬取网站\求职1号站'
    main(txtPath,thmlpath)
