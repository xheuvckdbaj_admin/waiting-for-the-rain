from flask import Flask
app = Flask(__name__)
@app.route('/')
def hello_world():
    return 'hello world'
if __name__ == '__main__':
    # app.run()    # 可以指定运行的主机IP地址，端口，是否开启调试模式
    app.run(host="0.0.0.0", port=8888, debug = True)