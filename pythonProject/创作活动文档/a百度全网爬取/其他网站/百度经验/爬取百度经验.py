import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_baidujingyan(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('h1 > span')[0].text 
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        try:
            elements=soup.select('div.list-icon')
            list_soupa = str(soup.select('div.exp-content-body')[-1]).replace('相关推荐', '').replace('END', '').replace(title, keyStr)
            for element in elements:
                content=str(element)
                list_soupa.replace(content,'')
        except:
            list_soupa = str(soup.select('#rtcontent-content')[0]).replace(title, keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('百度经验下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://jingyan.baidu.com/article/ab69b2709c0f096da6189f42.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_baidujingyan(url,htmlPath,keyStr,title)