# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup

def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#content')[0])
            title = soup.select('#article > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('#content')[0])
            title = soup.select('#article > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.mleft > div > ul > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        url='http://www.sxnet.com.cn'+href
        getHtml(thmlpath, url)

def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#pages > ul > li > a')
    if len(a_list) > 0:
        href = soup.select('#pages > ul > li > a')[-1].get('href')
        hrefUrl='http://www.sxnet.com.cn'+href
        allPage=int(hrefUrl.split('_')[-1].replace('.html',''))
        url=hrefUrl.replace(hrefUrl.split('_')[-1],'')+'%d.html'
        for i in range(1,allPage+1):
            newUrl=url%i
            getListUrl(thmlpath, newUrl)
    else:
        getListUrl(thmlpath, strUrl)

def getLittleTitleUrl(thmlpath,url):
    print(url)
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('#lanmu > ul > li > a')
    if len(a_list) > 0:
        for i,a in enumerate(a_list):
            href=a.get('href')
            newUrl = 'http://www.sxnet.com.cn' + href
            getPageUrl(thmlpath, newUrl)
    else:
        getPageUrl(thmlpath, url)

def getTitleUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('div.wbox > ul > li > a')
    for i,a in enumerate(a_list):
        if i > 3:
            href=a.get('href')
            newUrl='http://www.sxnet.com.cn'+href
            getLittleTitleUrl(thmlpath, newUrl)


def main(thmlpath):
    url='http://www.sxnet.com.cn/'
    getTitleUrl(thmlpath,url)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
