import os
import threading

import pypandoc
import requests

from pythonProject.FindFile import find_file


def htmlBeWOrd(src):
        try:
                print(src)
                docx=src.replace('.html','.docx').replace('\t','').replace('\n','')
                output = pypandoc.convert_file(src, 'docx', outputfile=docx,encoding='utf-8',extra_args=["-M2GB", "+RTS", "-K64m", "-RTS"])
                os.remove(src)
        except:
            print('错误')
def makeWord(filePaths,startNumber,endNumber):
    for i in range(startNumber,endNumber):
        htmlBeWOrd(filePaths[i])
def shear_dile(src):
    filePaths=find_file(src)
    mokuaiLong=int(len(filePaths)/8)
    t1 = threading.Thread(target=makeWord, args=(filePaths,0,mokuaiLong,))
    t2 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong,mokuaiLong*2,))
    t3 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong*2,mokuaiLong*3,))
    t4 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong*3,mokuaiLong*4,))
    t5 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong*4,mokuaiLong*5,))
    t6 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong*5,mokuaiLong*6,))
    t7 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong*6,mokuaiLong*7,))
    t8 = threading.Thread(target=makeWord, args=(filePaths,mokuaiLong*7,len(filePaths,)))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    t7.start()
    t8.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()
    t7.join()
    t8.join()

def mainHtoW():

    folder=r'D:\文档\百度活动文档2\百度html\其他网站\第一范文网'
    shear_dile(folder)
if __name__ == '__main__':
    mainHtoW()
