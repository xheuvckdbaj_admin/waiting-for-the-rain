# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="sx_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.shixibaogao.com.cn/moban/article_122169.html',
                'https://www.shixibaogao.com.cn/siqianzi/article_32671.html',
                'https://www.shixibaogao.com.cn/sanqianzi/article_53205.html',
                'https://www.shixibaogao.com.cn/erqianzi/article_53131.html',
                'https://www.shixibaogao.com.cn/yiqianzi/article_53181.html',
                'https://www.shixibaogao.com.cn/canguan/article_122174.html',
                'https://www.shixibaogao.com.cn/renshi/article_122145.html',
                'https://www.shixibaogao.com.cn/dinggang/article_122180.html',
                'https://www.shixibaogao.com.cn/qianyan/article_102629.html',
                'https://www.shixibaogao.com.cn/pingyu/article_122071.html',
                'https://www.shixibaogao.com.cn/zongjie/article_102663.html',
                'https://www.shixibaogao.com.cn/shixizhouji/article_122158.html',
                'https://www.shixibaogao.com.cn/jisuanji/article_122168.html',
                'https://www.shixibaogao.com.cn/faxue/article_121045.html',
                'https://www.shixibaogao.com.cn/pingmiansheji/article_121077.html',
                'https://www.shixibaogao.com.cn/xingzhengguanli/article_121054.html',
                'https://www.shixibaogao.com.cn/gongshangguanli/article_120939.html',
                'https://www.shixibaogao.com.cn/renliziyuan/article_120926.html',
                'https://www.shixibaogao.com.cn/gongchengzaojia/article_122157.html',
                'https://www.shixibaogao.com.cn/gongchengceliang/article_122160.html',
                'https://www.shixibaogao.com.cn/tumugongcheng/article_122172.html',
                'https://www.shixibaogao.com.cn/yingyuzhuanye/article_89792.html',
                'https://www.shixibaogao.com.cn/lvyou/article_122156.html',
                'https://www.shixibaogao.com.cn/meikuang/article_122116.html',
                'https://www.shixibaogao.com.cn/jianzhu/article_122179.html',
                'https://www.shixibaogao.com.cn/wuliu/article_122184.html',
                'https://www.shixibaogao.com.cn/jiudian/article_122166.html',
                'https://www.shixibaogao.com.cn/tielu/article_110451.html',
                'https://www.shixibaogao.com.cn/waimao/article_122176.html',
                'https://www.shixibaogao.com.cn/dianzishangwu/article_122171.html',
                'https://www.shixibaogao.com.cn/jiaoyu/article_122182.html',
                'https://www.shixibaogao.com.cn/hanjia/article_122096.html',
                'https://www.shixibaogao.com.cn/biye/article_122086.html',
                'https://www.shixibaogao.com.cn/shifansheng/article_122173.html',
                'https://www.shixibaogao.com.cn/daxuesheng/article_122183.html',
                'https://www.shixibaogao.com.cn/yinlejiaoshi/article_105510.html',
                'https://www.shixibaogao.com.cn/meishujiaoshi/article_110632.html',
                'https://www.shixibaogao.com.cn/tiyujiaoshi/article_90361.html',
                'https://www.shixibaogao.com.cn/yingyujiaoshi/article_121109.html',
                'https://www.shixibaogao.com.cn/shuxuejiaoshi/article_99951.html',
                'https://www.shixibaogao.com.cn/lvshishiwusuo/article_122185.html',
                'https://www.shixibaogao.com.cn/paichusuo/article_118653.html',
                'https://www.shixibaogao.com.cn/jianchayuan/article_122070.html',
                'https://www.shixibaogao.com.cn/weishengju/article_105251.html',
                'https://www.shixibaogao.com.cn/dianshitai/article_122186.html',
                'https://www.shixibaogao.com.cn/sifa/article_120948.html',
                'https://www.shixibaogao.com.cn/fayuan/article_122149.html',
                'https://www.shixibaogao.com.cn/yinxing/article_122146.html',
                'https://www.shixibaogao.com.cn/yiyuan/article_122155.html',
                'https://www.shixibaogao.com.cn/chejian/article_122087.html',
                'https://www.shixibaogao.com.cn/yinshuachang/article_111194.html',
                'https://www.shixibaogao.com.cn/jixiechang/article_122163.html',
                'https://www.shixibaogao.com.cn/chuanchang/article_102828.html',
                'https://www.shixibaogao.com.cn/yaochang/article_122167.html',
                'https://www.shixibaogao.com.cn/gongchang/article_122153.html',
                'https://www.shixibaogao.com.cn/baoxiangongsi/article_122144.html',
                'https://www.shixibaogao.com.cn/guanggaogongsi/article_122161.html',
                'https://www.shixibaogao.com.cn/dianzigongyi/article_121043.html',
                'https://www.shixibaogao.com.cn/shengchan/article_122170.html',
                'https://www.shixibaogao.com.cn/wushuichulichang/article_120998.html',
                'https://www.shixibaogao.com.cn/qiantai/article_122181.html',
                'https://www.shixibaogao.com.cn/chegong/article_121197.html',
                'https://www.shixibaogao.com.cn/diangong/article_122151.html',
                'https://www.shixibaogao.com.cn/qiangong/article_121297.html',
                'https://www.shixibaogao.com.cn/jingong/article_122175.html',
                'https://www.shixibaogao.com.cn/hushi/article_122081.html',
                'https://www.shixibaogao.com.cn/yishi/article_97933.html',
                'https://www.shixibaogao.com.cn/xiaoshou/article_122159.html',
                'https://www.shixibaogao.com.cn/shenji/article_122177.html',
                'https://www.shixibaogao.com.cn/chuna/article_122162.html',
                'https://www.shixibaogao.com.cn/huiji/article_122074.html',
                'https://www.shixibaogao.com.cn/caiwu/article_122178.html',
                'https://www.shixibaogao.com.cn/wenyuan/article_122164.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
