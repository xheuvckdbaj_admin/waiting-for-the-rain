import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_hao86(url, file, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.coommnew_view1 > h2')[0].text.replace('\n', '').strip()
        print(title)
        file.write(title+'\n')
    except:
        print('error')


if __name__ == '__main__':

    url = 'https://fanwen.hao86.com/%s/%s.html'
    htmlPath = r'D:\文档\百度付费上传文档\需要的标题及其chatgpt\title\hao86.txt'
    keyStr = 'kaixin'
    title = ''
    file = open(htmlPath, 'w', encoding='utf-8')
    keys=['shiyongwen','gongzuozongjie','gongzuojihua','shuzhibaogao','xindetihui','ziwojianding']
    for key in keys:
        for i in range(25005, 100000):
            newUrl = url % (key,str(i))
            # print(newUrl)
            mian_hao86(newUrl, file, keyStr, title)
    file.close()
