import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_baidujiankang(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.health-title')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.health-article-content-small')[0]).replace(title,keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('百度健康下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://m.baidu.com/bh/m/detail/ar_3939679827590079141'
    htmlPath=r'D:\文档\百度付费上传文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_baidujiankang(url,htmlPath,keyStr,title)