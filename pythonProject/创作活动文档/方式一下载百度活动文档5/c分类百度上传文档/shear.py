# -*- coding:utf-8 -*-
import os

'''
把文件夹内部的东西提出来
'''
def shear_dile(src, dst):
    if os.path.isdir(src):
        if not os.listdir(src):
            os.rmdir(src)
            print(u'移除空目录: ' + src)
        else:
            for d in os.listdir(src):
                shear_dile(os.path.join(src, d), dst)
    if os.path.isfile(src):
        if '~$' in src:
            try:
                os.remove(src)
            except:
                print('error')
        print(u"文件剪切:", src)
        fn = os.path.basename(src)
        if not os.path.exists(dst + './' + fn):
            os.rename(src, dst + './' + fn)
def main():
    src=r'D:\文档\百度活动文档\修改后百度活动文档'
    dst=r'D:\文档\百度活动文档\修改后百度活动文档'
    shear_dile(src,dst)
if __name__ == '__main__':
    main()