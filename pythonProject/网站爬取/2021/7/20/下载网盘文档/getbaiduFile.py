# -*- coding:utf-8 -*-

import time

import requests

import addcookies
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
filePath=r".\cookies.txt"
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def getNewWindow(browser,xpathStr):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def judge(i,browser,picPath,txtfile_handle):


    imgSrc = browser.find_element_by_css_selector('img.img-code').get_attribute('src')
    # browser.find_element_by_css_selector('div.verify-body > input.input-code').send_keys()
    time.sleep(8)
    getNewWindow(browser, '//div[@class="verify-body"]')
    codeName=browser.find_element_by_css_selector('div.verify-body > input.input-code').get_attribute('value')
    browser.find_element_by_css_selector('a.g-button.g-button-blue > span.g-button-right > span.text').click()
    try:
        browser.find_element_by_css_selector('a.g-button.g-button-blue > span.g-button-right > span.text').click()
        judge(i,browser,picPath,txtfile_handle)
    except:
        print(imgSrc)
        sponse = requests.get(url=imgSrc, headers=headers).content
        picName=str(time.time()) + '.jpg'
        newPath = picPath + '\\' + picName
        file_handle = open(newPath, mode='wb')
        file_handle.write(sponse)
        file_handle.close()
        txt_line=newPath+' '+codeName+'\n'
        txtfile_handle.write(txt_line)
        print('success')
        if i < 10:
            time.sleep(4)
def getFile(path,picPath,txtPath,number,last):
    txtFile = open(path, 'r')
    hrefs = txtFile.readlines()
    browser = webdriver.Chrome()
    txtfile_handle = open(txtPath, mode='w')
    for i in range(number,last):
        # try:
            if 'baidu' in hrefs[i]:
                print(i)
                browser.get(hrefs[i])
                print(hrefs[i])
                if i==number:
                    addcookies.addCookies(browser, hrefs[i], filePath)
                try:
                    browser.find_element_by_class_name('zbyDdwb').click()
                except:
                    print('error')
                try:
                    browser.find_elements_by_css_selector('span.g-button-right > span.text')[1].click()
                    browser = getNewWindow(browser, '//div[@class="verify-body"]')
                except:
                    continue

                judge(i,browser,picPath,txtfile_handle)
        # except:
        #
        #     print('错误')
        #     continue
    txtfile_handle.close()
def main():
    path=r'D:\文档\url\全册教案1629713717.1741707.txt'
    picPath=r'D:\文档\url\test'
    txtPath=r'D:\文档\url\test.txt'
    number=0
    last=2000
    getFile(path,picPath,txtPath,number,last)
if __name__ == '__main__':
    main()