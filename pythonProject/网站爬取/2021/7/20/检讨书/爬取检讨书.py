# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="jtnear"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.jiantaoshu.com.cn/daquan/article_43049.html',
                'https://www.jiantaoshu.com.cn/zenmexie/article_42203.html',
                'https://www.jiantaoshu.com.cn/geshi/article_42885.html',
                'https://www.jiantaoshu.com.cn/moban/article_43047.html',
                'https://www.jiantaoshu.com.cn/shizhi/article_37014.html',
                'https://www.jiantaoshu.com.cn/weiji/article_43042.html',
                'https://www.jiantaoshu.com.cn/chidao/article_43048.html',
                'https://www.jiantaoshu.com.cn/geilaopode/article_43032.html',
                'https://www.jiantaoshu.com.cn/wanneng/article_43033.html',
                'https://www.jiantaoshu.com.cn/shangkechidao/article_43050.html',
                'https://www.jiantaoshu.com.cn/taoke/article_43036.html',
                'https://www.jiantaoshu.com.cn/dajia/article_43011.html',
                'https://www.jiantaoshu.com.cn/kuangke/article_43040.html',
                'https://www.jiantaoshu.com.cn/shangkeshuohua/article_43046.html',
                'https://www.jiantaoshu.com.cn/kaoshizuobi/article_43045.html',
                'https://www.jiantaoshu.com.cn/kaoshimeikaohao/article_43035.html',
                'https://www.jiantaoshu.com.cn/gzjtsfanwen/article_42877.html',
                'https://www.jiantaoshu.com.cn/gzjtszenmexie/article_42204.html',
                'https://www.jiantaoshu.com.cn/shangbanchidao/article_43043.html',
                'https://www.jiantaoshu.com.cn/gongzuoshiwu/article_43038.html',
                'https://www.jiantaoshu.com.cn/gongzuotaidu/article_43044.html',
                'https://www.jiantaoshu.com.cn/gongzuoweiji/article_43041.html',
                'https://www.jiantaoshu.com.cn/gongzuoshizhi/article_43014.html',
                'https://www.jiantaoshu.com.cn/erbaizi/article_37872.html',
                'https://www.jiantaoshu.com.cn/sanbaizi/article_38321.html',
                'https://www.jiantaoshu.com.cn/sibaizi/article_38344.html',
                'https://www.jiantaoshu.com.cn/wubaizi/article_38346.html',
                'https://www.jiantaoshu.com.cn/babaizi/article_20667.html',
                'https://www.jiantaoshu.com.cn/yiqianzi/article_18245.html',
                'https://www.jiantaoshu.com.cn/erqianzi/article_8707.html',
                'https://www.jiantaoshu.com.cn/sanqianzi/article_11009.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
