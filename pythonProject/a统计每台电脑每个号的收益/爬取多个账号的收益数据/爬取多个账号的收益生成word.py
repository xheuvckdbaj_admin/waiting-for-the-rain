import time
import urllib

import playwright
from playwright.sync_api import sync_playwright, Playwright
from selenium import webdriver

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import readTxt
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档 import addCookies
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

def addCookiesPlay(filePath):
    cookiesJion=[]
    cookies = readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['path'] = '/'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        cookiesJion.append(cookie)
    return  cookiesJion
def getCookiesBytxt(cookiesPath):
    file=open(cookiesPath,'r',encoding='utf-8')
    cookiesContent=file.readline().replace('\n','')
    return cookiesContent
# def getName():
# def getAmount():
def makeTxt(playwright: Playwright,cookiesPath,zhanghaoTxtPath):
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(6000)
    addCookiesPlay(cookiesPath)
    time.sleep(100)

def main():
   cookiesPath='./cookies.txt'
   zhanghaoTxtPath='./账号.txt'
   with sync_playwright() as playwright:
       makeTxt(playwright, cookiesPath,zhanghaoTxtPath)
   makeTxt(cookiesPath,zhanghaoTxtPath)
if __name__ == '__main__':
    main()