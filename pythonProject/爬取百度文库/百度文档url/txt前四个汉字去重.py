
def getPanduan(allStr,content):
    panduan = True
    for line in allStr:
        if content[:4] in line and (not '2' in content):
            panduan = False
    return panduan
def makeTxt(preTxtPath,downFlodPath):
    file=open(preTxtPath,'r',encoding='utf-8')
    lines=file.readlines()
    filePath=downFlodPath+'\\'+'整理后的txt.txt'
    file_last = open(filePath, 'w', encoding='utf-8')
    allStr=[]
    for i,line in enumerate(lines):
        panduan=getPanduan(allStr, line)
        if panduan:
            allStr.append(line)
            file_last.write(line)
    file.close()
    file_last.close()

def main():
    preTxtPath=r'C:\Users\Administrator\Desktop\关键字\all.txt'
    downFlodPath=r'E:\文档\自查报告网url'
    makeTxt(preTxtPath, downFlodPath)
if __name__ == '__main__':
    main()