# -*- coding:utf-8 -*-
import re
import time

import requests

from bs4 import BeautifulSoup

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #time.sleep(10)
            #url='http://www.beiku.com/xieyishu/article_145325.html'
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.content')[0])
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.e2 > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.text
            newUrl='http://www.lw54.com'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.pagelist > li > a')
    hrefStr=a_list[len(a_list)-1].get('href')
    numberStr=re.match('(.*?)_(.*?)_(.*?).html',hrefStr).group(3)
    newUrl=hrefStr.split(numberStr)[0]+'%d.html'
    for i in range(1,int(numberStr)):
        newHref=newUrl%i
        getUrl(thmlpath, newHref)

def getHtml2(thmlpath, strUrl,file_line):
    try:
        #strUrl='https://www.beiku.com/xieyishu/'
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('.main-left > ul > li > a')[0]
        href = a_list.get('href')
        file_line.write(href+'\n')
    except:
        print('该模块没有文档')

def getHtml(thmlpath, strUrl,file_line):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.jhw_sort_title > ul > li > a')
    for i in range(0, len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        getHtml2(thmlpath, href,file_line)
def main(thmlpath):
            number=1
            strUrl='https://www.beiku.com/'
            txtfilepath=thmlpath+'\\'+str(time.time())+'.txt'
            file_line=open(txtfilepath,'w')
            getHtml(thmlpath, strUrl,file_line)
            file_line.close()


if __name__ == '__main__':
    thmlpath = r'E:\文档\test'
    main(thmlpath)
