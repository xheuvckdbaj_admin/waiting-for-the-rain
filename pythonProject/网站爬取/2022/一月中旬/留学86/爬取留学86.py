# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.main_zhengw')[0]).replace('<br/>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.conter_main_one.clearfix > h1 > a')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
            #打开文本图片集
            strUrl='https://www.liuxue86.com/a/%d.html'
            for i in range(4000162,4300162):
                try:
                    print(i)
                    newUrl=strUrl%i
                    getHtml(thmlpath, newUrl)
                except:
                    print('出错')
                    continue


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
