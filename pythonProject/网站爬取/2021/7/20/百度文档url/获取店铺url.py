# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=原创力下载'}


def getElement(browser,urls,file_handle):
    for e in range(0, len(urls)):
        print(urls[e])
        browser.get(urls[e])
        elements = browser.find_elements_by_css_selector('div.x-box__link > a.x-button.x-button--info')
        if len(elements) == 2:
            elements[1].click()
            browser.close()
        elif len(elements) == 1:
            elements[0].click()
            browser.close()
        elif len(elements) == 0:
            continue
        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        browser.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="button"]'))
            )
        except:
            continue
        try:
            hf = browser.find_element_by_css_selector(
                'div.button > a.x-button.x-button--primary').get_attribute('href')
        except:
            continue
        print(hf)
        file_handle.write(hf + '\n')


def getLinUrl(browser,newUrl, file_handle):

    # try:


        browser.get(newUrl)
        elements=browser.find_elements_by_xpath('//div[@class="search-result-wrap"]/div[@class="search-result-content"]'
                                                '/div[@class="content-info"]/div[@class="content-auth"]/a[@class="content-username"]')
        for element in elements:
            href=element.get_attribute('href')
            print(href)
            file_handle.write(href+'\n')
    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(browser,key,path):

    url='https://wenku.baidu.com/search?word=%s&lm=0&od=0&fr=top_home&ie=utf-8&pn=%d'

    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')

    for i in range(1, 1000):
            newUrl=url%(key,i)
            getLinUrl(browser,newUrl, file_handle)

    file_handle.close()

def main():
        browser = webdriver.Chrome()
        keys=['教学进度表','第一学期','案例分析','党员','重要讲话',
              '建造师','检查细则','实训报告','绩效评价','护理服务',
              '法律法规','市场分析','文学选读','离散数学','策划书','策划书','解题方法'
            ,'电大','分班考试','物理','化学','生物','历史','地理','语文','电路设计','抗洪'
            ,'风险管理','阅读理解','教材讲稿','应用文写作','模拟试题','期末试题','检验流程','同步练习','防控考试','建模'
            ,'验资报告','教育专题','人力资源','高级评审','一轮复习','行政规划'
            ,'数据结构','工作质量','课程设计','施工方案','管理制度','培养计划'
            ,'课程教学大纲','家书','执法业务培训','人事制度','招聘试题','寒假'
            ,'中职实习','建设章程','有效策划','应急救援','万能模板','申请书'
            ,'广告销售','寒假生活答案','规格书','知识框架','提纲','缴费制度'
            ,'安全文化学习手册','安全教育','员工规章','专插本','前景预测','设计规范'
            ,'重点整理','会议纪要','设计意图','教学指导','必刷题','环卫行业'
            ,'指标','综合理论','教学规划','单词学习','知识点','阶段性测试'
            ,'决策管理','自查报告','责任险','复习资料','病例分析','注册计量师']
        for key in keys:
            strUrl='https://wenku.baidu.com/'
            # key='中考'
            path=r'D:\文档\店铺url'
            login(browser,key,path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
