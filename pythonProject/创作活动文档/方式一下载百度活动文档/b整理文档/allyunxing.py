from pythonProject.修改文件提高文档质量.重新生成文档.修改文档格式 import mainGeshi
from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.modify import mainModify
from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.提出含有vip文档 import mainModifyVip
from pythonProject.创作活动文档.方式一下载百度活动文档.html转成word import shear_dile


def main_arrangeDocx(oldFolderPathx,mediumFolderPath,midFolderPath,newPath):

    # mainHtml()
    # html转word
    shear_dile(oldFolderPathx)
    #过滤敏感词和修改关键词
    mainModify(oldFolderPathx,mediumFolderPath,midFolderPath)
    #删除文档内容中的图片，并修改文档内容格式
    mainGeshi(mediumFolderPath,newPath)
    #提出含有vip文档
    mainModifyVip(newPath,oldFolderPathx)
    # mainYouzhi()
    # mainGuolv()
if __name__ == '__main__':
    # 需要修改格式文档的文件夹
    oldFolderPathx = r'D:\文档\百度活动文档25\百度html'
    # 中介文件夹
    mediumFolderPath = r'D:\文档\百度活动文档25\中介'
    # 不要设置的文件夹，不需要管他
    midFolderPath = r'D:\文档\自查报告网url'
    # 修改格式后最终存放文档的文件夹，也就是文档上传文件夹，最终文件夹
    newPath = r'D:\文档\百度活动文档25\修改后百度活动文档'
    main_arrangeDocx(oldFolderPathx,mediumFolderPath,midFolderPath,newPath)