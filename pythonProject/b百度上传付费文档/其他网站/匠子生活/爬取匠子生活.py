import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_jiangzishenghuo(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.detailTitle > h2')[0].text
        list_soupa = str(soup.select('div.detailText')[0]).replace(title, title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('匠子生活下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.jiangzi.com/tuwen/shenghuo/184244.html'
    htmlPath=r'D:\文档\百度付费上传文档\百度html\第一范文网'
    keyStr='kaixin'
    title=''
    mian_jiangzishenghuo(url,htmlPath,keyStr,title)