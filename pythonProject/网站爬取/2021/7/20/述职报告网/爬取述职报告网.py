# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="q-h"]/span[@class="h"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.shuzhibaogao.net/geshi/article_44503.html',
                'https://www.shuzhibaogao.net/zenmexie/article_155370.html',
                'https://www.shuzhibaogao.net/moban/article_155369.html',
                'https://www.shuzhibaogao.net/nianzhong/article_155324.html',
                'https://www.shuzhibaogao.net/zhuanzheng/article_155330.html',
                'https://www.shuzhibaogao.net/caiwu/article_155360.html',
                'https://www.shuzhibaogao.net/huiji/article_155166.html',
                'https://www.shuzhibaogao.net/chuna/article_155218.html',
                'https://www.shuzhibaogao.net/zongzhi/article_136957.html',
                'https://www.shuzhibaogao.net/xiaoshou/article_155164.html',
                'https://www.shuzhibaogao.net/jingli/article_155349.html',
                'https://www.shuzhibaogao.net/shiguan/article_41190.html',
                'https://www.shuzhibaogao.net/kaitoujiewei/article_153240.html',
                'https://www.shuzhibaogao.net/shulian/article_110535.html',
                'https://www.shuzhibaogao.net/dangyuan/article_22248.html',
                'https://www.shuzhibaogao.net/dangjian/article_30925.html',
                'https://www.shuzhibaogao.net/xuexiaozbsj/article_15666.html',
                'https://www.shuzhibaogao.net/xiangzhendwsj/article_13286.html',
                'https://www.shuzhibaogao.net/cunshuji/article_26734.html',
                'https://www.shuzhibaogao.net/dangweishuji/article_20813.html',
                'https://www.shuzhibaogao.net/dangzhibushuji/article_26874.html',
                'https://www.shuzhibaogao.net/shuji/article_41226.html',
                'https://www.shuzhibaogao.net/nakeyisheng/article_135732.html',
                'https://www.shuzhibaogao.net/waikeyisheng/article_145104.html',
                'https://www.shuzhibaogao.net/fuchankeyisheng/article_43099.html',
                'https://www.shuzhibaogao.net/yishi/article_153238.html',
                'https://www.shuzhibaogao.net/hushi/article_139996.html',
                'https://www.shuzhibaogao.net/hushichang/article_155373.html',
                'https://www.shuzhibaogao.net/xiangcunyisheng/article_142145.html',
                'https://www.shuzhibaogao.net/weishengyuanyz/article_131428.html',
                'https://www.shuzhibaogao.net/yisheng/article_154344.html',
                'https://www.shuzhibaogao.net/youeryuanyz/article_152952.html',
                'https://www.shuzhibaogao.net/xiaoxuexz/article_142180.html',
                'https://www.shuzhibaogao.net/zhongxuexz/article_152981.html',
                'https://www.shuzhibaogao.net/jiaoshi/article_155352.html',
                'https://www.shuzhibaogao.net/youeryuanjiaoshi/article_154340.html',
                'https://www.shuzhibaogao.net/xiaoxuejiaoshi/article_154948.html',
                'https://www.shuzhibaogao.net/chuzhongjiaoshi/article_154369.html',
                'https://www.shuzhibaogao.net/gaozhongjiaoshi/article_153271.html',
                'https://www.shuzhibaogao.net/xiaochang/article_154809.html',
                'https://www.shuzhibaogao.net/gonghuizhuxi/article_126466.html',
                'https://www.shuzhibaogao.net/zhigongdaibiao/article_145844.html',
                'https://www.shuzhibaogao.net/cunzhuren/article_131393.html',
                'https://www.shuzhibaogao.net/funvzhuren/article_139452.html',
                'https://www.shuzhibaogao.net/chejianzhuren/article_154971.html',
                'https://www.shuzhibaogao.net/juweihuizhuren/article_12584.html',
                'https://www.shuzhibaogao.net/zongwuzhuren/article_136873.html',
                'https://www.shuzhibaogao.net/junren/article_14861.html',
                'https://www.shuzhibaogao.net/gongdiansuosz/article_137562.html',
                'https://www.shuzhibaogao.net/bangongshizhuren/article_152928.html',
                'https://www.shuzhibaogao.net/shequ/article_155191.html',
                'https://www.shuzhibaogao.net/yinxing/article_153850.html',
                'https://www.shuzhibaogao.net/budui/article_20723.html',
                'https://www.shuzhibaogao.net/tuanweishuji/article_128173.html',
                'https://www.shuzhibaogao.net/tuanweishuji/article_128173.html',
                'https://www.shuzhibaogao.net/fudaoyuan/article_153019.html',
                'https://www.shuzhibaogao.net/banzhuren/article_155185.html',
                'https://www.shuzhibaogao.net/jiaodaozhuren/article_154382.html',
                'https://www.shuzhibaogao.net/zhengjiaozhuren/article_148883.html',
                'https://www.shuzhibaogao.net/baoxiangongsi/article_154998.html',
                'https://www.shuzhibaogao.net/shequ/article_155191.html',
                'https://www.shuzhibaogao.net/yinxing/article_153850.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
