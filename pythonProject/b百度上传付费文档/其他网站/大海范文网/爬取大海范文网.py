import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_dahaifanwenwang(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.main_left > h1')[0].text
        title = re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.content')[0]).replace(title,title).split('推荐访问:')[0]
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('大海范文网下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://www.zhangdahai.com/xinde/2022/1123/519087.html'
    htmlPath=r'D:\文档\百度付费上传文档\百度html\第一范文网'
    keyStr='kaixin'
    title=''
    mian_dahaifanwenwang(url,htmlPath,keyStr,title)