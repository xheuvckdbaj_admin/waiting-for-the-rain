# -*- coding:utf-8 -*-
import re
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.nr4')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>').replace('</div>','').replace('<div id="cc" class="nr4">','')
            content=re.sub('幼儿教育(.*?)om','',list_soupa)
            content=re.sub('(幼儿教育(.*?)om)','',content)
            return content
        except:
            print('编码出错')
            return ''
def getContentPage(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        text = soup.select('div.nr8 > li.gong > a')[0].text
        pageNumber=int(re.findall('共(.*?)页: ',text)[0])
        return pageNumber
    except:
        return 1

def getContent(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = soup.select('div.hc1')[0].text.replace('?', '').replace('？', '').replace('|', '')
    print(title)
    contentStr=getHtml(thmlpath, strUrl)
    pageNumber=getContentPage(thmlpath, strUrl)
    url=strUrl.replace('.html','_%d.html')
    if pageNumber > 1:
        for i in range(2,pageNumber+1):
            newUrl=url%i
            contentStr=contentStr+getHtml(thmlpath, newUrl)
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentStr)


def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lie3 > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getContent(thmlpath, href)
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.nr8 > li > a')[-1].get('href')
    pageNumber=int(href.split('_')[-1].replace('.html',''))
    url=strUrl.replace('index.html','')+href.replace(href.split('_')[-1],'')+'%d.html'
    for i in range(1,pageNumber):
        newUrl=url%i
        getListUrl(thmlpath, newUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.nr2 > div.lan > li.lan1 > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        getPageUrl(thmlpath, href)
def main(txtPath,thmlpath):
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        url=line.replace('\n','')
        getTitleUrl(thmlpath, url)



if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\自查报告网'
    main(txtPath,thmlpath)
