# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'https://fanwen.chazidian.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    list_soupa = str(soup.select('.article-main')[0])
    #print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('.item-ct')
    url_list1 = soup.select('.main-ct-left')
    for div in url_list1:
        li_list1=div.select('ul > li')
        for li in li_list1:
            href=li.select('a')[0].get('href')
            title=li.select('a')[0].get('title')
            newUrl=f'https://fanwen.chazidian.com'+href
            getUrlPageNum(thmlpath, newUrl)
    print(url_list)
    for u in url_list:
            li_list=u.select('ul > li')
            for li in li_list:
                title=li.select('a')[0].get('title')
                href=li.select('a')[0].get('href')
                newUrl=f'https://fanwen.chazidian.com'+href
                #print(title)
                #print(newUrl)
                getContent(thmlpath, newUrl, title)
#获取最终的地址
def getUrlIndex(path,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    div_list=soup.select('.listpage-main-left > .listpage-item')
    #print(url_list)
    for div in div_list:
            src=div.select('h4 > a')[0].get('href')
            href=f'https://fanwen.chazidian.com'+src
            print(href)
            title = div.select('h4 > a')[0].get('title')
            getContent(path,href,title)

def getUrlPageNum(path,url):
    for pageNum in range(1,1000):
        try:
                    oldUrl=url+'%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(path, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['gongzuofanwen','dangtuanfanwen','hetongfanben','gongzuobaogao','yanjiangfanwen','xindetihui','tiaojushuxin',
           'liyifanwen','lw']
    for s in str:
        url = groupUrl(s)
        if s == 'lw':
            strUrl_list= ['jingji','zhengquan','guanli','kuaijishenji','faxue','yiyao','shehui','jiaoyu','jisuanji']
            for strUrl in strUrl_list:
                newUrl=url+strUrl
                getUrl(thmlpath, newUrl)

        else:
            getUrl(thmlpath, url)


if __name__ == '__main__':
    thmlpath = r'E:\test'
    main(thmlpath)
