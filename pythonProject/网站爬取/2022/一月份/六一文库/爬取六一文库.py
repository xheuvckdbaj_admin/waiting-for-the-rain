# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getOtherContent(pageUrl):
    requests_text = str(requests.get(url=pageUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con')[0])
    return contentText
def getOtherUrl(page,newUrl):
    text=page.select('ul.pagelist > li > a')[0].text
    pageNumber=int(re.findall('共(.*?)页: ',text)[0])
    ur=newUrl.split('.html')[0]
    content=''
    for i in range(2,pageNumber+1):
        pageUrl=ur+'_'+str(i)+('.html')
        pageContent=getOtherContent(pageUrl)
        content=content+pageContent
    return content



def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = str(soup.select('div.g_con > h1')[0].text)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    contentText = str(soup.select('div.con')[0])
    pageAs=soup.select('div.dede_pagess > ul.pagelist > li > a')
    if len(pageAs) > 0:
        page=soup.select('div.dede_pagess')[0]
        contentPage=getOtherUrl(page, strUrl)
        contentText=contentText+contentPage
    # contentText=re.sub('<span>(.*?)</span>','',content)
    contentText=contentText.replace('※','')
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
    # 百度搜索“77
    # cn”或“免费范文网”即可找到本站免费阅读全部范文。收藏本站方便下次阅读，免费范文网，提供经典小说申报材料人民满意公务员先进集体申报材料在线全文阅读
    strUrl='https://www.05166.com/fanwen/%d.html'
    for i in range(1,133635):
                try:
                    print(i)
                    newUrl=strUrl%i
                    getHtml(thmlpath, newUrl)
                except:
                    print('出错')
                    continue


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
