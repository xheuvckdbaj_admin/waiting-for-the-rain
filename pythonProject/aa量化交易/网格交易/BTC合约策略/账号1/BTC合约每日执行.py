import time
from decimal import Decimal

import requests
from okx import Account, Trade, PublicData, MarketData
from okx.Trade import TradeAPI

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getMA(instId, marketAPI, Hnumber):
    millisecondNumber = int(Hnumber) * 3600000
    # （1）获取当前MA5
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '5')
    cnumber5 = 0
    for i in range(0, 5):
        c0 = result_market_SWAP['data'][i][4]
        cnumber5 += Decimal(c0)
    MA5 = Decimal(cnumber5 / 5)
    # （2）获取当前MA10
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '10')
    cnumber10 = 0
    for i in range(0, 10):
        c0 = result_market_SWAP['data'][i][4]
        cnumber10 += Decimal(c0)
    MA10 = Decimal(cnumber10 / 10)
    # （3）获取当前MA20
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '20')
    cnumber20 = 0
    for i in range(0, 20):
        c0 = result_market_SWAP['data'][i][4]
        cnumber20 += Decimal(c0)
    MA20 = Decimal(cnumber20 / 20)
    print(MA20)
    # （4）获取当前MA30
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '30')
    cnumber30 = 0
    for i in range(0, 30):
        c0 = result_market_SWAP['data'][i][4]
        cnumber30 += Decimal(c0)
    MA30 = Decimal(cnumber30 / 30)
    # （5）获取当前MA60
    result_market_SWAP = marketAPI.get_candlesticks(instId, str(int(time.time() * 1000 - millisecondNumber)), '', '1H',
                                                    '60')
    cnumber60 = 0
    for i in range(0, 60):
        c0 = result_market_SWAP['data'][i][4]
        cnumber60 += Decimal(c0)
    MA60 = Decimal(cnumber60 / 60)
    return [MA5, MA10, MA20, MA30, MA60]


def MakeBTC_SWAP(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber):
    # 获取当前币种合约价格
    marketAPI = MarketData.MarketAPI(api_key, secret_key, passphrase, False, flag)
    result_market_SWAP = marketAPI.get_ticker(instId)
    print(result_market_SWAP)
    nowPrice = result_market_SWAP['data'][0]['last']
    print(nowPrice)
    # 获取当前币种24小时最高最低价
    hightPrice24 = result_market_SWAP['data'][0]['high24h']
    print(hightPrice24)
    lowPrice24 = result_market_SWAP['data'][0]['low24h']
    print(lowPrice24)
    # 获取当前币种各个均线价格,MA5,MA10,MA20,MA30,MA60
    MAs = getMA(instId, marketAPI, '0')
    # 获取24小时前币种各个均线价格,MA5,MA10,MA20,MA30,MA60
    beforeMAs = getMA(instId, marketAPI, '24')
    # 计算出当前趋势走向
    beforeMAs[3]
    if MAs[3] >= beforeMAs[3]:
        # 今日上涨
        print('今日上涨')
    else:
        # 今日下跌
        print('今日下跌')
    # 设置杠杆倍数
    accountAPI = Account.AccountAPI(api_key, secret_key, passphrase, False, flag)
    result_lever = accountAPI.set_leverage('10', 'isolated', instId, '', 'long')
    # 计算出阶梯买入价格和买入张数
    # 计算出买入价格
    buyPrice = (Decimal(MAs[2] + Decimal(lowPrice24)) / 2).quantize(Decimal("0.000000000000000"))
    if Decimal(nowPrice) <= buyPrice:
        buyPrice = (Decimal(Decimal(nowPrice) * Decimal(0.98))).quantize(Decimal("0.000000000000000"))
    # 计算出买入张数
    sheetNumber = int(Decimal(int(Unumber) * 10) / (buyPrice * Decimal(coinNumber)))
    # 计算出止盈止损价格
    # 止盈价格
    surplusPrice = (Decimal(hightPrice24) * Decimal(0.998)).quantize(Decimal("0.000000000000000"))
    if sheetNumber > 0:
        # 买进合约（单位：张），并设置止盈止损
        tradeAPI = Trade.TradeAPI(api_key, secret_key, passphrase, False, flag)
        result_order = tradeAPI.place_order(instId, 'isolated', 'buy', 'limit', str(sheetNumber), '', '', '', 'long',
                                            str(buyPrice),
                                            '', '', str(surplusPrice), str(surplusPrice), '', '',
                                            '', '', '', '', '')
        if result_order['data'][0]['sMsg'] == 'Order placed':
            print('下单成功',
                  '买了%s张%s,价格：%s，止盈价格：%s' % (str(sheetNumber), instId, str(buyPrice), str(surplusPrice)))


def main():
    flag = "0"  # live trading: 0, demo trading: 1
    api_key = 'dcc90281-f342-4166-ad66-110a941ae3e6'
    secret_key = '9B2084459A62DC52E669482ED8DDBD0A'
    passphrase = '#Xsy103171'
    instId = 'SHIB-USDT-SWAP'
    Unumber = '10'
    coinNumber = '1000000'
    MakeBTC_SWAP(api_key, secret_key, passphrase, flag, instId, Unumber, coinNumber)


if __name__ == '__main__':
    main()
