import re
import time

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLingcookies
import ipAgency
import addProxyid

def getToken(browser):
    browser.get(r'https://www.lingfengyun.com/')
    addLingcookies.addCookies(browser, r'https://www.lingfengyun.com/', r".\cookies.txt")
    browser.find_element_by_xpath('//input[@class="main-sousuo__input"]').send_keys('中考')
    browser.find_element_by_xpath('//input[@class="main-sousuo__input"]').send_keys(Keys.ENTER)
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(0.01)
    url=browser.current_url
    print(url)
    token=re.match(r'(.*?)so_token=(.*?)&so_file=wang_pan&so_source=all_pan',url).group(2)
    print(token)
    return token
def main():
    browser = webdriver.Chrome()
    getToken(browser)
if __name__ == '__main__':
    main()