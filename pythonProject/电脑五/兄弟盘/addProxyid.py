# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium
from bs4 import BeautifulSoup
from selenium import webdriver
import checkIp
import addcookies
import readTxt
def addId(browser,filePath1):
    chromeOptions = webdriver.ChromeOptions()
    liurl = readTxt.readTxt(filePath1)
    if not browser == None:
        browser.get(liurl)
    else:
        browser = webdriver.Chrome()
        browser.get(liurl)
    proxy = browser.find_element_by_css_selector('pre').text
    print(proxy)  # 如果某个代理访问失败,可从proxy_arr中去除
    newbrowser = None
    if checkIp.check_proxy(proxy) == True:
        browser.quit()
        http = '--proxy-server=http://' + proxy
        chromeOptions.add_argument(http)  # 添加代理
        browser = webdriver.Chrome(options=chromeOptions)
        newbrowser = browser
        if newbrowser == None:
            return addId(browser, filePath1)
        else:
            return newbrowser

    else:
        return addId(browser, filePath1)
def main():
    filePath1=r"C:\Users\Administrator\PycharmProjects\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\兄弟盘\ip网址.txt"
    addId(filePath1)
if __name__ == '__main__':
    main()