import os

from docx import Document

from FindFile import find_file, find_name


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None
def testblank(filePath,newfilePath,fileName):
    try:
        # 创建空白文档，并设置样式
        myDocument = Document(filePath+'\\'+fileName)
        # 设置一个空白样式
        for num,paragraphs in enumerate(myDocument.paragraphs):
            if paragraphs.text == "":
                print('第{}段是空行段'.format(num))
                paragraphs.clear()  #清除文字，并不删除段落，run也可以,
                delete_paragraph(paragraphs)
        myDocument.save(newfilePath+'\\'+fileName)
        os.remove(filePath+'\\'+fileName)
    except:
        print('错误')
def main():
    filePath=r'E:\卷子'
    newfilePath=r'E:\没处理的docx文件'
    file_list=find_name(filePath)

    for fileName in file_list:
        if not '~$' in fileName:
            print(fileName)
            #file = Document(file)
            testblank(filePath,newfilePath,fileName)
if __name__ == '__main__':
    main()