import os
import time

import nose.util

import addLingcookies
import requests
from selenium import webdriver
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    # option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver
def getNewWindow(browser,xpathStr,number):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-原创力下载])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, number).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def keepRename(path,newPath,newtitle):
    files = find_file(path)
    if len(files) == 1:
        fileName = newPath + '\\' + newtitle
        try:
            os.rename(files[0], fileName)
        except:
            os.remove(files[0])
    elif len(files) == 0:
        time.sleep(10)
        keepRename(path, newPath, newtitle)
    else:
        print(len(files))
        for file in files:
            print(file)
        e=1/0
def crawing(path,newPath):
    files = find_file(path)
    print(len(files))

def main():
    path = r"G:\文档\原创力文档"
    newPath=r'G:\文档\原创力改名后的文档'

    crawing(path,newPath)
if __name__ == '__main__':
    main()