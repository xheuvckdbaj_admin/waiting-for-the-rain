import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay


def getNewPage(page, url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    return page


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)
        print(filePath, '创建成功！！！')


def getTitle(page1, fileTtitle,fileTtitleNo):
    xpathStr = '//tr[@class="el-table__row"]'
    xpathBian = '//td/div[@class="cell"]/span[@style="margin-left: 6px;"]'
    xpathTitle = '//td/div[@class="cell"]/span[@style="cursor: pointer;"]'
    xpathStrs = page1.locator(xpathStr)
    # elements = page1.locator('xpath=//span[@style="cursor: pointer;"]')
    for i in range(0, xpathStrs.count()):
        contentStr = xpathStrs.nth(i).locator(xpathBian).text_content().strip()
        titleStr = xpathStrs.nth(i).locator(xpathTitle).text_content().strip()
        if contentStr == '未通过':
            fileTtitleNo.write(titleStr+'\n')
            print(titleStr + '----', contentStr)
        elif contentStr == '已上架':
            fileTtitle.write(titleStr + '\n')
            print(titleStr + '----', contentStr)


def getTitleForquchong(playwright: Playwright, passTitleFolder,passTitleFolderNo, cookiesPath):
    brower = playwright.chromium.launch(headless=False)
    context = brower.new_context()
    page = context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    page.get_by_text("文档", exact=True).click()
    time.sleep(0.1)
    xpathStrZhanghao = '//div[@class="top-bar-wrapper"]/div[@class="right-bar"]/div[@class="link"]'
    zhanghaoName = page.locator(xpathStrZhanghao).nth(0).text_content().strip().replace('*','')
    titlePassPath = passTitleFolder + '\\' +zhanghaoName+ 'Pass.txt'
    fileTtitle = open(titlePassPath, 'w', encoding='utf-8')
    titleNoPath = passTitleFolderNo + '\\' + zhanghaoName + 'no.txt'
    fileTtitleNo = open(titleNoPath, 'w', encoding='utf-8')
    getTitle(page, fileTtitle,fileTtitleNo)
    for i in range(2, 38):
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(str(i))
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
        time.sleep(0.5)
        getTitle(page, fileTtitle,fileTtitleNo)
    fileTtitle.close()
    fileTtitleNo.close()


def getPassTitle(passTitleFolder,passTitleFolderNo, cookiesPath):
    with sync_playwright() as playwright:
        getTitleForquchong(playwright, passTitleFolder,passTitleFolderNo, cookiesPath)


def mainPassAndNoTitle(passTitleFolder,passTitleFolderNo, cookiesPath):
    makeFolder(passTitleFolder)
    makeFolder(passTitleFolderNo)
    getPassTitle(passTitleFolder,passTitleFolderNo, cookiesPath)


def main():
    passTitleFolderPass = r'D:\文档\账号通过的标题'
    passTitleFolderNo = r'D:\文档\账号没通过的标题'
    cookiesPath = r'./baiduCookies1.txt'
    mainPassAndNoTitle(passTitleFolderPass,passTitleFolderNo, cookiesPath)


if __name__ == '__main__':
    main()
