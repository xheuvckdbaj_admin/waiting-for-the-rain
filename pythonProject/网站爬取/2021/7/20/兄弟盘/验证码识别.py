#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/8/29 16:18
# @Author  : ziyu
# @File    : yanzhengma.py
# @Software: PyCharm
# 导入必要的库
# pip install captcha numpy matplotlib tensorflow-gpu
from captcha.image import ImageCaptcha
import matplotlib.pyplot as plt
import numpy as np
import random

# %matplotlib inline
# %config InlineBackend.figure_format = 'retina'

import string

characters = string.digits + string.ascii_uppercase
print(characters)

width, height, n_len, n_class = 128, 64, 4, len(characters)

import tensorflow as tf
import tensorflow.keras.backend as K

# 防止 tensorflow 占用所有显存
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess)

from tensorflow.keras.utils import Sequence


# 定义数据生成器
class CaptchaSequence(Sequence):
    def __init__(self, characters, batch_size, steps, n_len=4, width=128, height=64):
        self.characters = characters
        self.batch_size = batch_size
        self.steps = steps
        self.n_len = n_len
        self.width = width
        self.height = height
        self.n_class = len(characters)
        self.generator = ImageCaptcha(width=width, height=height)

    def __len__(self):
        return self.steps

    def __getitem__(self, idx):
        X = np.zeros((self.batch_size, self.height, self.width, 3), dtype=np.float32)
        y = [np.zeros((self.batch_size, self.n_class), dtype=np.uint8) for i in range(self.n_len)]
        for i in range(self.batch_size):
            random_str = ''.join([random.choice(self.characters) for j in range(self.n_len)])
            X[i] = np.array(self.generator.generate_image(random_str)) / 255.0
            for j, ch in enumerate(random_str):
                y[j][i, :] = 0
                y[j][i, self.characters.find(ch)] = 1
        return X, y


# 定义网络结构
from tensorflow.keras.models import *
from tensorflow.keras.layers import *

input_tensor = Input((height, width, 3))
x = input_tensor
for i, n_cnn in enumerate([2, 2, 2, 2, 2]):
    for j in range(n_cnn):