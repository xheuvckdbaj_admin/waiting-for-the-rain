# -*- coding:utf-8 -*-

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #time.sleep(10)
        #url='http://m.pincai.com/article/2300000.htm'
        try:
            requests_text =requests.get(url=url, headers=headers).content

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.art-text')[0])
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    try:
        requests_text = requests.get(url=url, headers=headers).content
        soup = BeautifulSoup(requests_text, 'lxml')
    except:
        requests_text = 'hello'
        soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('.conlist-num > h5 > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.text.replace('?','').replace('？','').replace('|','')\
                    .replace('”','').replace('*','').replace('/','').replace('\\','')
            newUrl='https://www.wenmi.com'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    newUrl=strUrl+'p%d.html'
    for i in range(1,1000):
        href=newUrl%i
        getUrl(thmlpath, href)



def getHtml2(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('p > a')
    for i in range(5,len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        newUrl='https://www.wenmi.com'+href
        getHtml3(thmlpath, newUrl)
def getHtml(thmlpath, strUrl,number):
    for i in range(number,90000):
        try:
            newUrl=strUrl%i
            getContent(thmlpath, newUrl)
        except:
            print('error')
            continue
def main(thmlpath):
            number=1
            strUrl='https://www.wenmi.com/article/'
            getHtml2(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'D:\文档\文秘帮'
    main(thmlpath)
