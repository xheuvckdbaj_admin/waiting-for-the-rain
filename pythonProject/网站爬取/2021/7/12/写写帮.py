# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        #url='http://m.pincai.com/article/2300000.htm'
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        list_soupa = str(soup.select('.entry-content')[0])
        print(title)
        print(url)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#AListBox > ul > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.get('title')
            newUrl='https:'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('.content-list > li > h2 > a')
        for a in a_list:
            #time.sleep(10)
            href = a.get('href')
            newUrl='https://www.xiexiebang.com'+href
            title=a.text
            getContent(thmlpath, newUrl,title)
    except:
        print('错误')


def getHtml2(thmlpath, strUrl):
    getHtml3(thmlpath, strUrl)
    newUrl = strUrl + 'index_%d.html'
    for i in range(2,10):
        url=newUrl%i
        getHtml3(thmlpath, url)

def getHtml1(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.tagcloud > a')
    for i in range(0,len(a_list)):
        a=a_list[i]
        href = a.get('href')
        newUrl='https://www.xiexiebang.com'+href
        getHtml2(thmlpath, newUrl)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.more')
    for i in range(0,len(a_list)):
        a=a_list[i]
        href = a.get('href')
        newUrl='https://www.xiexiebang.com'+href
        getHtml1(thmlpath, newUrl)

def main(thmlpath):
            #number=1
            strUrl='https://www.xiexiebang.com/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档\写写帮'
    main(thmlpath)
