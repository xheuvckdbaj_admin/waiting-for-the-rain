# -*- coding:utf-8 -*-
import os            #导入文件操作模块
import re             #导入正则模块
import time

import win32com  #导入word操作模块
#准备打开word
from docx.shared import Inches

from win32com.client import Dispatch,constants

from FindFile import find_file


def wordFormat(folderPath,i):
        tt = time.time()
        path=folderPath + '\\' + i
        if not '$' in path and (path.endswith('.docx') or path.endswith('.doc')) :
            #try:
                w = win32com.client.Dispatch('Word.Application')
                w.Visible = 0
                w.DisplayAlerts = 0
                doc = w.Documents.Open(path)
                print(path)
                for paragraph in doc.Paragraphs:
                        par1 = paragraph.Range
                        paragraph.left_indent = Inches(.25)
                        # par1.ParagraphFormat.Reset()#取消首行缩进
                        par1.Font.Name = "微软雅黑"
                        par1.Font.Color='0'
                        par1.Font.Size = "14"#小四
        print('spendTime:{} sec'.format(time.time() - tt))
            #except:
                #print('错误')
def main():
    folderPath=r'E:\验证码图片'
    for i in os.listdir(folderPath):
        wordFormat(folderPath,i)
if __name__ == '__main__':
    main()