import time

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )

def inputKey(driver,lianjieFile,key):
    # driver.find_element(By.XPATH, '//input[@class="s-ipt"]').clear()
    # driver.find_element(By.XPATH,'//input[@class="s-ipt"]').send_keys(key)
    # time.sleep(0.2)
    # driver.find_element(By.XPATH, '//input[@class="s-ipt"]').send_keys(Keys.ENTER)
    try:
        getNewWindow(driver, '//h3[@class="h3url"]/a', 10)
    except:
        print('没有内容，或者网速过慢')
    elements=driver.find_elements(By.XPATH,'//h3[@class="h3url"]/a')
    for element in elements:
        title=element.text
        if mainPanDuanTitle(key, title):
            hrefUrl=element.get_attribute('href')
            print(hrefUrl)
            lianjieFile.write(hrefUrl+'\n')
            break
def mian_Douding(titleFolderPath,txtFilePath):
    driver=getDriver('zhiku')
    url='https://www.mbalib.com/s?q=%s'
    driver.get('https://doc.mbalib.com/')
    time.sleep(10)
    file=open(titleFolderPath+'\\'+'all.txt','r',encoding='utf-8')
    lianjieFile=open(txtFilePath,'w',encoding='utf-8')
    key_lines=file.readlines()
    for i,key_line in enumerate(key_lines):
        # try:

            key=key_line.replace('\n','')
            print(str(i)+'---------'+key)
            newUrl=url%key
            driver.get(newUrl)
            inputKey(driver, lianjieFile, key)
        # except:
        #     print('该关键词出错')
    lianjieFile.close()
if __name__ == '__main__':
    titleFolderPath=r'D:\文档\百度活动文档\百度活动文档标题'
    txtFilePath=r'./mbazhiku.txt'
    mian_Douding(titleFolderPath,txtFilePath)