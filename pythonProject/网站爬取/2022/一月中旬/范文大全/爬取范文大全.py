# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('#result')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('tr > td.main_ArticleTitle')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getAllUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('tr > td > a')
    for a in a_list:
        href=a.get('href')
        try:
            getHtml(thmlpath, href)
        except:
            print('错误')
            continue
def getPageUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.show_page > a')[-1].get('href')
    pageNumber=int(href.split('=')[-1].replace('.html',''))
    url=href.replace(href.split('=')[-1],'')
    for i in range(2,pageNumber+1):
        newUrl=url+str(i)
        getAllUrl(thmlpath, newUrl)
def getTitleUrl(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('tr > td > a')
    for a in a_list:
        href=a.get('href')
        getAllUrl(thmlpath, href)
        try:
            getPageUrl(thmlpath, href)
        except:
            print('error')
            continue
def main(thmlpath):
                url='http://www.ybask.com/'
                getTitleUrl(thmlpath, url)
                # getTitleUrl2(thmlpath, url)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
