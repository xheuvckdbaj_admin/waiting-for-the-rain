# -*- coding:utf-8 -*-
import re
import time
import urllib

import addcookies
import selenium

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"D:\pycharm\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\小白盘\cookies.txt"
filePathbaidu=r"D:\pycharm\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\小白盘\cookiesbaidu.txt"
def getNewWindow(browser,xpathStr):
    n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    print('当前句柄: ', n)  # 会打印所有的句柄
    # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, 100).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr))
    )
    return browser
def huoquUrl(browser,newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(30)
        huoquUrl(browser,newUrl)
def getElement(browser,hrefs,file_handle):
    for e in range(0, len(hrefs)):
        try:
            print(hrefs[e])
            huoquUrl(browser, hrefs[e])
            time.sleep(0.2)
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="bd-abstract"]'))
            )

            href=browser.find_element_by_css_selector('div.bd-abstract > div.bd-alist > div > div > a.bdylink').get_attribute('href')
            print(href)
            file_handle.write(href+'\n')
        except:
            print('资源不存在')
            continue



def getLinUrl(law,url,key,file_handle):
        error=law
    # try:
        # browser = getBrowser(r'https://www.lingfengyun.com/')
        # 添加cookies
        browser=None
        try:
            browser = webdriver.Chrome()
        except:
            getLinUrl(law, url, key, file_handle)
        newUrl = url % (key,1)
        huoquUrl(browser,newUrl)
        elements=browser.find_elements_by_xpath('//div[@class="pagination light-theme simple-pagination"]/ul/li')
        try:
            text=elements[len(elements)-2].text
            allPage=int(text)
            print('总页数：',allPage)
        except:
            allPage=1
        for page in range(law,allPage+1):
                print(page)
                newUrl = url % (key,page)
                print(newUrl)
                huoquUrl(browser, newUrl)
                try:
                    element = WebDriverWait(browser, 100).until(
                        EC.presence_of_element_located((By.XPATH, '//span[@class="bd-title"]'))
                    )
                    eles = browser.find_elements_by_xpath('//div[@class="bd-list"]/span[@class="bd-title"]'
                                                          '/a')
                except:
                    print('该关键词没有文档')
                    break

                hrefs = []
                for ele in eles:
                    href = ele.get_attribute('href')
                    hrefs.append(href)
                print(hrefs)
                print(len(hrefs) * allPage)
                getElement(browser, hrefs, file_handle)


    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,key,file_handle):
    url='https://www.ttyunsou.cn/s?keyword=%s&t=0&o=1&page=%d'
    getLinUrl(law, url, key,file_handle)


def main():
    keys = [
        '电子元件',
'元器件',
'ppt素材',
'商铺出租',
'起诉状',
'试用期',
'电路',
'班主任工作',
'民营医院',
'工作调动',
'股票入门基础',
'租房',
'语法大全',
'项目合作',
'先进教师',
'食品安全',
'幼儿园工作',
'会议通知',
'语文复习',
'公式大全',
'家教',
'外贸流程',
'文学常识',
'危险化学品',
'教学反思',
'植树节活动',
'整改报告',
'服装设计',
'广告合同',
'工作经历',
'师德学习',
'压缩语段',
'环境安全',
'薪酬设计',
'卫生标准',
'党员转正',
'合作意向',
'承包经营',
'实习生',
'护理制度',
'收费标准',
'高压电',
'简介模板',
'防火规范',
'分级管理',
'宪法宣誓',
'岗位竞聘',
'工作经验交流',
'注意事项',
'中医学概论',
'数电',
'模电',
'客户关系管理',
'新目标英语',
'服务方案',
'艺术教育',
'监察指南',
'责任险',
'审查要点',
'网络搭建',
'课程学习',
'培训大纲',
'自动控制原理'
    ]
    for key in keys:
    #         key='中考'
            print(key)
            path=r'D:\文档\url\天天云盘'
            law=1
            newPath = path + '\\' + key + str(time.time()) + '.txt'
            file_handle = open(newPath, mode='w')
            login(law,key,file_handle)
            file_handle.close()

if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
