# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.a百度全网爬取.百度全网爬取.网站选择 import websiteJudeBaidu

from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


def compareTitle(title,content):
    panduan=True
    if "《" in title and "》" in title:
        title=re.search('《(.*?)》',title)[0]
    if title in content:
        panduan=True
    return panduan


def getFirstUrl(fileTest,folderPath,keyStr,urls,driver):
    try:
        time.sleep(0.1)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span/input[@class="s_ipt"]'))
            )
        except:
            time.sleep(1)
        driver.find_element(By.XPATH,'//span/input[@class="s_ipt"]').clear()
        try:
            driver.find_element(By.XPATH,'//span/input[@class="s_ipt"]').send_keys(keyStr)
        except:
            driver.find_element(By.XPATH,'//span/input[@class="se-input adjust-input"]').send_keys(keyStr)
        time.sleep(2)
        driver.find_element(By.XPATH,'//span/input[@class="s_ipt"]').send_keys(Keys.ENTER)
        time.sleep(0.2)
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 20).until(
                EC.presence_of_element_located((By.XPATH, '//h3[@class="c-title t t tts-title"]/a'))
            )
        except:
            time.sleep(1)
        try:
            element=driver.find_element(By.XPATH, '//div[@class="title-row_2ymNF"]/a')
            websiteJudeBaidu(element, folderPath, driver, keyStr,urls)
        except:
            elements=driver.find_elements(By.XPATH, '//h3[@class="c-title t t tts-title"]/a')
            for i,element in enumerate(elements):
                title=element.text
                if mainPanDuanTitle(keyStr, title):
                    fileTest.write(keyStr+'++++++++'+title+'\n')
                    print(keyStr+'++++++++'+title)
                    # baidu=driver.find_element(By.XPATH,'//span[@class="c-color-gray"]')
                    panduan=websiteJudeBaidu(element, folderPath, driver, keyStr,urls)
                    if panduan:
                        break
    except:
        time.sleep(5)
        print('driver错误')
        strUrl = 'https://www.baidu.com/'
        driver.get(strUrl)



def makeFolder(floderPath,fileName):
    try:
        print(fileName)
        floderPath=floderPath+'\\'+fileName
        os.makedirs(floderPath)
    except:
        print(fileName,'该文件夹已存在，无需再新建')
    return floderPath
def main(fileTest,folderPath,urls,txtPath,driver,number,last):
        file=open(txtPath,'r',encoding='utf-8')
        lines=file.readlines()
        for i,line in enumerate(lines):
            try:
                if i >= number and i < last:
                    try:
                        print(str(i)+'百度搜索'+'----',line)
                        strUrl = 'https://www.baidu.com/'
                        driver.get(strUrl)
                    except:
                        driver = getDriver('b')
                        strUrl = 'https://www.baidu.com/'
                        driver.get(strUrl)
                    if 45==i%50:
                        try:
                            driver.quit()
                            driver = getDriver(str(i))
                            driver.get(strUrl)
                        except:
                            print('error')
                    keyStr=line.replace('\n','')
                    print(keyStr)
                    getFirstUrl(fileTest,folderPath,keyStr, urls,driver)
            except:
                print('打开浏览器出错')

def mianBaiduW(fileTest,txtPath,folderPath,wangzhiPath,number,last):
    urls=[]
    file=open(wangzhiPath,'w',encoding='utf-8')
    txtFilePath=txtPath
    driver = getDriver('b')
    main(fileTest,folderPath,urls, txtFilePath, driver,number,last)
    driver.quit()
    for i,url in enumerate(urls):
        file.write(url+'\n')
    file.close()
    return urls
if __name__ == '__main__':
    #标题存放文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'+'\\'+'all.txt'
    folderPath=r'D:\文档\百度活动文档\百度html\其他网站'
    wangzhiPath=r'./wangzhi.txt'
    titleTestFolder = r'E:\文档\百度活动文档\标题算法测试'
    makeFolder(titleTestFolder)
    fileTest = open(titleTestFolder + '\\' + '百度搜索爬取标题与活动标题对比' + '.txt', 'w', encoding='utf-8')
    number=0
    last=100
    mianBaiduW(fileTest,txtPath,folderPath,wangzhiPath,number,last)
