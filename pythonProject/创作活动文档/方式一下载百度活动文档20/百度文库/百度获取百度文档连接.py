# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


def compareTitle(title,content):
    panduan=True
    if "《" in title and "》" in title:
        title=re.search('《(.*?)》',title)[0]
    if title in content:
        panduan=True
    return panduan


def getFirstUrl(keyStr,urls,driver):
    try:
        time.sleep(0.1)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//span/input[@class="s_ipt"]'))
            )
        except:
            time.sleep(1)
        driver.find_element(By.XPATH,'//span/input[@class="s_ipt"]').clear()
        try:
            driver.find_element(By.XPATH,'//span/input[@class="s_ipt"]').send_keys(keyStr)
        except:
            driver.find_element(By.XPATH,'//span/input[@class="se-input adjust-input"]').send_keys(keyStr)
        time.sleep(2)
        driver.find_element(By.XPATH,'//span/input[@class="s_ipt"]').send_keys(Keys.ENTER)
        time.sleep(0.2)
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.5)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="c-row source_1Vdff OP_LOG_LINK c-gap-top-xsmall source_s_3aixw"]/a[@class="siteLink_9TPP3"]'))
            )
        except:
            time.sleep(0.1)
        baidu=driver.find_element(By.XPATH,'//span[@class="c-color-gray"]')
        if baidu.text=='百度文库':
            driver.find_element(By.XPATH,'//div[@class="c-row source_1Vdff OP_LOG_LINK c-gap-top-xsmall source_s_3aixw"]/a[@class="siteLink_9TPP3"]').click()
            n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
            driver.switch_to.window(n[-1])
            time.sleep(0.5)
            try:
                element = WebDriverWait(driver, 5).until(
                    EC.presence_of_element_located((By.XPATH, '//div[@class="ql-editor"]'))
                )
            except:
                time.sleep(0.1)
            wenKuUrl=driver.current_url
            if 'wenku.baidu.com' in wenKuUrl:
                hrefUrl = wenKuUrl.split('.html')[0] + '?pcqq.c2c&bfetype=old'
                newUrl=hrefUrl+'————'+keyStr
                print(newUrl)
                urls.append(newUrl)
            driver.close()
            n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
            driver.switch_to.window(n[-1])
            time.sleep(0.5)

    except:
        time.sleep(5)
        print('driver错误')
        strUrl = 'https://www.baidu.com/'
        driver.get(strUrl)



def makeFolder(floderPath,fileName):
    print(fileName)
    floderPath=floderPath+'\\'+fileName
    os.makedirs(floderPath)
    return floderPath
def main(urls,txtPath,driver):
        file=open(txtPath,'r',encoding='utf-8')
        lines=file.readlines()
        strUrl = 'https://www.baidu.com/'
        driver.get(strUrl)
        for i,line in enumerate(lines):
            if 70==i%71:
                try:
                    driver.quit()
                    driver = getDriver(str(i))
                    driver.get(strUrl)
                except:
                    print('error')
            keyStr=line.replace('\n','')
            print(keyStr)
            getFirstUrl(keyStr, urls,driver)

def mianBaiduX(txtPath):
    urls=[]
    txtFilePaths = find_file(txtPath)
    for txtFilePath in txtFilePaths:
        driver = getDriver('b')
        main(urls, txtFilePath, driver)
        driver.quit()
    return urls
if __name__ == '__main__':
    #标题存放文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'
    mianBaiduX(txtPath)
