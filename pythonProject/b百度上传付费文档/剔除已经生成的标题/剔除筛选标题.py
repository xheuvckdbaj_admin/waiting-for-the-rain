def getTitle(alreadyPath,needPath,successPath):
    alreadyFile=open(alreadyPath,'r',encoding='utf-8')
    alreadyFile_lines=alreadyFile.readlines()
    needFile = open(needPath, 'r', encoding='utf-8')
    needFile_lines = needFile.readlines()
    successFile = open(successPath, 'w', encoding='utf-8')
    for i ,line in enumerate(needFile_lines):
        if not line in alreadyFile_lines:
            successFile.write(line)
    needFile.close()
    alreadyFile.close()
    successFile.close()
def main():
    alreadyPath=r'./已经生成的标题.txt'
    needPath=r'./需要筛选的标题.txt'
    successPath=r'./筛选后的标题.txt'
    getTitle(alreadyPath,needPath,successPath)
if __name__ == '__main__':
    main()