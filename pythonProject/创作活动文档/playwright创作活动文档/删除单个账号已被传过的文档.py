# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.FindFile import find_file
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt
from pywinauto import Desktop
from pywinauto.keyboard import *


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser


def panduanTitle(title):
    panduan = True
    if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title or '年级' in title \
            or '图' in title or '.' in title or '×' in title or (len(title) < 5):
        panduan = False
    return panduan


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getNumberPager(browser):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
        return pagerNumber
    except:
        return 0


def deleteWord(folderPath, title):
    filePath = folderPath + '\\' + title + '.docx'
    try:
        os.remove(filePath)
    except:
        print('无法删除' + folderPath)


def ctrlx(oldFilePath, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
    shutil.move(oldFilePath, newPath)


def getFileNameStr(folderPath):
    keyStrs = []
    filePaths = find_file(folderPath, [])
    if len(filePaths) >= 2:
        for filePath in filePaths:
            titleStr = os.path.basename(filePath).replace('.docx', '')
            keyStrs.append(titleStr)
    return keyStrs


def getTitleNumber(wordFolderPath):
    titleNumber = 1000000
    if wordFolderPath.endswith('推荐'):
        titleNumber = -1
    if wordFolderPath.endswith('学前教育'):
        titleNumber = 0
    if wordFolderPath.endswith('基础教育'):
        titleNumber = 1
    if wordFolderPath.endswith('高校与高等教育'):
        titleNumber = 2
    if wordFolderPath.endswith('资格考试'):
        titleNumber = 3
    if wordFolderPath.endswith('法律'):
        titleNumber = 4
    if wordFolderPath.endswith('建筑'):
        titleNumber = 5
    if wordFolderPath.endswith('互联网'):
        titleNumber = 6
    if wordFolderPath.endswith('行业资料'):
        titleNumber = 7
    if wordFolderPath.endswith('政务民生'):
        titleNumber = 8
    if wordFolderPath.endswith('商品说明书'):
        titleNumber = 9
    if wordFolderPath.endswith('实用模板'):
        titleNumber = 10
    if wordFolderPath.endswith('生活娱乐'):
        titleNumber = 11
    return titleNumber


def selectHeading(page, wordFolderPath):
    try:
        time.sleep(1)
        pageNumber = getTitleNumber(wordFolderPath)
        if pageNumber >= 0 and pageNumber < 12:
            # pageNumber最小为0
            if pageNumber >= 9:
                xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
                page.locator(xpathStrs).click()
            xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
            xpathStrScue = '//div[@class="privilege-list"]/div[@class="privilege-item-container action"]'
            try:
                page.locator(xpathStr).nth(pageNumber).click()
            except:
                page.locator(xpathStr).nth(pageNumber).click()
            time.sleep(0.5)
            # titleText = page.locator(xpathStrScue).text.strip()
            if not getTitleNumber(wordFolderPath) == pageNumber:
                titleEl = page.locator(xpathStr).nth(pageNumber).click()
                time.sleep(1)
    except:
        selectHeading(page, wordFolderPath)


def clickButton(ele):
    try:
        ele.click()
        time.sleep(0.1)
        ele.click()
        time.sleep(0.1)
        ele.click()
    except:
        print('')


def panduanLastPage(page):
    panduan = 1
    try:
        xpathStrButton = '//button[@class="btn-next"]'
        disabled = page.locator(xpathStrButton).get_attribute('disabled')
        if disabled == 'disabled':
            panduan = 0
        else:
            panduan = 1
        return panduan
    except:
        panduan = 0
        return panduan


def ClickNextPage(page, wordFolderPath, pagerNumber, wordNumber):
    try:
        # pageNumber最大为1，最小为0
        xpathStr = '//button[@class="btn-next"]/i[@class="el-icon el-icon-arrow-right"]'
        print(pagerNumber)
        if panduanLastPage(page) == 1:
            try:
                page.locator(xpathStr).click()
                time.sleep(1)
            except:
                print('点击下一页失败')
        elif panduanLastPage(page) == 0:
            print('已经上传完毕')
            pagerNumber = 100
        elif panduanLastPage(page) == 2:
            print('没刷新出来')
            5 / 0
        return pagerNumber
    except:
        wordFolderPaths = os.listdir(wordFolderPath)
        if len(wordFolderPaths) > 5:
            print('点击下一页fast，重新开始上传')
            page.goto('https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
            # getNewWindow(browser, '//div[@class="major-btns"]', 20)
            selectHeading(page, wordFolderPath)
            pageXpathStr = '//div[@class="el-input el-pagination__editor is-in-pagination"]/input[@class="el-input__inner"]'
            page.locator(pageXpathStr).click()
            page.locator(pageXpathStr).fill(str(pagerNumber))
            page.locator(pageXpathStr).press("Enter")
            time.sleep(0.5)
        return 0


def submitword(page, file_chooser, fileDocxPath):
    file_chooser.set_files(fileDocxPath)
    page.wait_for_timeout(6000)
    page.get_by_role('button', name='确认提交').click()
    print('上传成功：' + fileDocxPath)
    # filePathxs = find_file(folderPath, [])
    # print('已经成功上传' + str(dataNumber) + '个文档')
    # dataNumber1 = len(filePathxs)
    # print('此次成功上传' + str(dataNumber1) + '个文档')
    # together = dataNumber + dataNumber1
    # print('一共成功上传' + str(together) + '个文档')
    # longx = dataNumber1
    # for filePathx in filePathxs:
    try:
        os.remove(fileDocxPath)
    except:
        print('删除不掉' + fileDocxPath)


def deleteFile(wordFolderPath):
    filePaths = find_file(wordFolderPath)
    for filepath in filePaths:
        try:
            os.remove(filepath)
        except:
            print('删除失败', filepath)


def singlyUpload(page, wordFolderPath, keyStrs, passedTitles, panduanUpload, panduanFilePath):
    xpathStrTitle = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    xpathStrTitleScue = '//div[@class="doc-row disabled-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    titleNumbers = page.locator(xpathStrTitle).count()
    deleteTitleNumbers = page.locator(xpathStrTitleScue).count()
    for deleteTitleNumber in range(0, deleteTitleNumbers):
        print('总量：', deleteTitleNumbers)
        print('当前量：', deleteTitleNumber)
        deleteTitleText = page.locator(xpathStrTitleScue).nth(deleteTitleNumber).get_attribute('title').strip()
        if deleteTitleText in keyStrs:
            try:
                scueTitleFilePath = os.path.join(wordFolderPath, deleteTitleText) + '.docx'
                os.remove(scueTitleFilePath)
                print('成功删除已上传文档', scueTitleFilePath)
            except:
                print('删除文档失败', scueTitleFilePath)
    return panduanUpload


def xunhuanPage(page, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber, wordNumber,
                panduanFilePath):
    panduanUpload = singlyUpload(page, wordFolderPath, keyStrs, passedTitles, panduanUpload, panduanFilePath)
    if panduanUpload == False:
        pagerNumber = pagerNumber + 1
        pagerNumber = ClickNextPage(page, wordFolderPath, pagerNumber, wordNumber)
        if not pagerNumber == 100:
            pagerNumber = xunhuanPage(page, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber,
                                      wordNumber, panduanFilePath)
        return pagerNumber
    else:
        return pagerNumber


def uploadFile(page, wordFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber, panduanFilePath):
    panduanUpload = False
    page.goto('https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask')
    time.sleep(0.5)
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('点击创作活动失败')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    selectHeading(page, wordFolderPath)
    print(pagerNumber)
    if pagerNumber > 0:
        try:
            pageXpathStr = '//div[@class="el-input el-pagination__editor is-in-pagination"]/input[@class="el-input__inner"]'
            # browser.find_element(By.XPATH, pageXpathStr).clear()
            # time.sleep(0.3)
            page.locator(pageXpathStr).click()
            page.locator(pageXpathStr).fill(str(pagerNumber))
            page.locator(pageXpathStr).press("Enter")
            time.sleep(0.5)
        except:
            print('网页没刷新')
            pagerNumber = uploadFile(page, wordFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber,
                                     panduanFilePath)

    pagerNumber = xunhuanPage(page, wordFolderPath, keyStrs, passedTitles, panduanUpload, pagerNumber, wordNumber,
                              panduanFilePath)
    return pagerNumber


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.ma(filePath)

def getNewPage(page,url):
    try:
        page.goto(url)
    except:
        time.sleep(1)
        getNewPage(page, url)

def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    return page


def mainTitleClassifyUploadApi(playwright: Playwright, folderPath, cookiesPath, panduanFilePath) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(5000)
    run(context, page, cookiesPath)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    titleFolderPaths = os.listdir(folderPath)
    dataNumber = 0
    for titleFolderPathName in titleFolderPaths:
        passedTitles = []
        pagerNumber = 0
        titleFolderPath = os.path.join(folderPath, titleFolderPathName)
        try:
            wordFolderPaths = os.listdir(titleFolderPath)
        except Exception as e:
            print(str(e))
        wordNumber = 100
        if len(wordFolderPaths) < 100:
            wordNumber = len(wordFolderPaths)
        for i in range(0, wordNumber):
            # for wordFolderPathName in wordFolderPaths:
            #     print(dataNumber)
            keyStrs = getFileNameStr(titleFolderPath)
            if not keyStrs == None:
                pagerNumber = int(
                    uploadFile(page, titleFolderPath, keyStrs, passedTitles, pagerNumber, wordNumber, panduanFilePath))
                if pagerNumber == 100:
                    break
                    break
        wordFolderPaths2 = os.listdir(titleFolderPath)
        uploadNumber = len(wordFolderPaths) - len(wordFolderPaths2)
        print('一共上传%d个文档' % uploadNumber)
        if len(wordFolderPaths) == 0:
            try:
                os.rmdir(titleFolderPath)
            except:
                print('无法删除' + titleFolderPath)


def mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath):
    with sync_playwright() as playwright:
        mainTitleClassifyUploadApi(playwright, folderPath, cookiesPath, panduanFilePath)


if __name__ == '__main__':
    folderPath = r'D:\文档\百度活动文档\百度活动文档上传'
    panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    cookiesPath = './baiduCookies.txt'
    # dataNumber = 0
    file = open(panduanFilePath, 'w', encoding='utf-8')
    file.write('')
    file.close()
    mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)