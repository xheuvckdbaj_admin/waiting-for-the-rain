import wenxin_api
from wenxin_api.tasks.composition import Composition

wenxin_api.ak = "wXTCySasjLzs36p75gefOzwegdwjiEvN"  # 填写自己的API Key
wenxin_api.sk = "IGD2T6ngudMfz5y6hRYisWXswOp3nbda"  # 填写自己的Secret Key
input_dict = {
    "text": "全是人才中的人才",  # 题目/内容·
    "seq_len": 1000,  # 文本长度（最长生成结果文本长度）
    "topp": 1.0,  # 多样性（取值越大，生成文本的多样性越强）
    "penalty_score": 1.0,  # 重复惩罚（1-2之前，通过对已生成的token增加惩罚，减少重复生成的现象）
    "min_dec_len": 100,  # 最小生成长度
    "is_unidirectional": 0,  # 单双向控制开关（0表示模型为双向生成，1表示模型为单向生成。）
    #建议续写与few-shot等通用场景建议采用单向生成方式，而完型填空等任务相关场景建议采用双向生成方式。
    "task_prompt": "zuowen"  # 任务类型
}
rst = Composition.create(**input_dict)
content=rst.get('result')
print(content)