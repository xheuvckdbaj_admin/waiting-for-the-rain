# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.zlcool.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('div > h1')[0].text
    #print(title)
    href = soup.select('.down > a')[0].get('href')
    for i in range(1, 3):
        char = href[0]
        href = href.replace(char, "")
    newUrl='http://www.zlcool.com/e/DownSys'+href
    print(newUrl)
    path=thmlpath+'\\'+title+'.doc'
    requests_text = requests.get(url=newUrl, headers=headers)
    with open(path, 'wb') as df:
        for chunk in requests_text.iter_content(chunk_size=128):
            df.write(chunk)


# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href=soup.select('.main > .main_l > .down > a')[0].get('href')
    newUrl ='http://www.zlcool.com'+href
    getContent(thmlpath,newUrl,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    dd_list=soup.select('.main_l > .slist > dl > dd')
    print(dd_list)
    for u in dd_list:
            src=u.select('a')[0].get('href')
            title = u.select('a')[0].get('title')
            href='http://www.zlcool.com'+src
            print(href)
            getUrl(thmlpath,href,title)
def againgeturl(thmlpath,url,pageNum):
    if pageNum==1 or pageNum==2:
        str_list=['1','2','3','4','5','6','7']
        for string in str_list:
            try:
                newUrl=url+string+'/'
                getUrlIndex(thmlpath, newUrl)
            except:
                continue
    else:

        str_list = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'c1', 'c2', 'c3', 'g1', 'g2', 'g3']
        for string in str_list:
            try:
                newUrl = url + string + '/'
                getUrlIndex(thmlpath, newUrl)
            except:
                continue

def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,15):
        try:

                    oldUrl = url + str(pageNum) + '/'

                    print(oldUrl)
                    againgeturl(thmlpath,oldUrl,pageNum)

        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):
    str = ['st']
    for s in str:
        url = groupUrl(s)
        getUrlPageNum(thmlpath,url)


if __name__ == '__main__':
    thmlpath = r'E:\test'
    main(thmlpath)
