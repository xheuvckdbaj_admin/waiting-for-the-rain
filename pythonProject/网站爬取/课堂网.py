# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.kt250.com/fanwen/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        time.sleep(60)
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        #print(title)
        list_soupa = str(soup.select('.main-left > .article > .content')[0])
        print(list_soupa)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='gbk')
        f.write(list_soupa)
    except:
        print('错误')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    li_list=soup.select('.conList > ul > li')
    #print(url_list)
    for u in li_list:
            href=u.select('a')[0].get('href')
            print(href)
            title = u.select('a')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            getContent(thmlpath,href,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    try:
        a_list = soup.select('.chNews > h3 > span > a')
        # print(url_list)
        for a in a_list:
            src = a.get('href')
            print(src)
            getUrl(thmlpath, src)
    except:
            getUrl(thmlpath, url)


def getUrlPageNum(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.chNews > h3 > span > a')
    # print(url_list)
    for a in a_list:
        src = a.get('href')
        print(src)
        getUrlIndex(thmlpath,src)







def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            getUrl(thmlpath, url)
        except:
            continue


def main(thmlpath):

        url ='http://www.kt250.com/fanwen/'
        getUrlPageNum(thmlpath,url)


if __name__ == '__main__':
    thmlpath = r'G:\文档\课堂网'
    main(thmlpath)
