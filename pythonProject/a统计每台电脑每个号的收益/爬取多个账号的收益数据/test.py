# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pypandoc
import pyperclip
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.FindFile import find_file
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档检查账号是否传满 import \
    mainTitleClassifyUploadApiGuish
from pythonProject.创作活动文档.playwright创作活动文档.删除单个账号已被传过的文档 import mainTitleClassifyUploadDelete
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt
from pywinauto import Desktop
from pywinauto.keyboard import *





def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getNewPage(page, url):
    try:
        page.goto(url)
    except:
        time.sleep(1)
        getNewPage(page, url)





def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1701156929388&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/trade")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1701156929388&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/trade")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page
def getName(page):
    try:
        name_html=page.locator('//div[@class="link"]').nth(0).text_content()
    except:
        name_html=''
    return name_html
def getbalance(page):
    try:
        balance_html=page.locator('//span[@class="num"]').nth(0).inner_html()
    except:
        balance_html=''
    return balance_html
def getOldIncome(page):
    try:
        oldIncome_html = page.locator('//table[@class="el-table__body"]').nth(0).inner_html()
    except:
        oldIncome_html=''
    return oldIncome_html
def getNowIncome(page,context):

    try:
        with context.expect_page() as new_page_info:
            page.click('//div[@class="nw-btn-box"]/button[@type="button"]')
        new_page = new_page_info.value
        new_page.wait_for_load_state()
        # page.locator('//div[@class="nw-btn-box"]/button[@type="button"]').nth(0).click()
        time.sleep(0.5)
        page.locator('//div[@class="veui-tabs-item-label-content"]').nth(0).click()
        time.sleep(0.5)
        nowIncome_html = page.locator('//div[@class="veui-table-main"]/table').nth(0).inner_html()
    except:
        nowIncome_html=''
    return nowIncome_html

def htmlBeWOrd(src):
        try:
            print(src)
            docx = src.replace('.html', '.docx').replace('\t', '').replace('\n', '')
            output = pypandoc.convert_file(src, 'docx', outputfile=docx, encoding='utf-8',
                                           extra_args=["-M2GB", "+RTS", "-K64m", "-RTS"])
            os.remove(src)
        except:
            print('错误')
def mainTitleClassifyUpload(playwright: Playwright, htmlPath, cookiesPath) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(10000)
    page.goto('https://kns.cnki.net/kns8s/defaultresult/index?crossids=YSTT4HG0%2CLSTPFY1C%2CJUP3MUPD%2CMPMFIG1A%2CWQ0UVIAA%2CBLZOG7CK%2CEMRPGLPA%2CPWFIRAGL%2CNLBO1Z6R%2CNN3FJMUV&korder=SU&kw=%E4%BB%A5%E4%BA%A4%E9%80%9A%E8%BF%9D%E6%B3%95%E8%A1%8C%E4%B8%BA%E6%A1%88%E4%BE%8B')
    time.sleep(100)


def mainTitleClassifyUploadApi(folderPath, cookiesPaths):
    paths=find_file(cookiesPaths)
    for cookiesPath in paths:
        titleName=os.path.basename(cookiesPath).replace('.txt','')
        print(titleName)
        htmlPath=folderPath+'\\'+titleName+'.html'
        with sync_playwright() as playwright:
            mainTitleClassifyUpload(playwright, htmlPath, cookiesPath)

        # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


if __name__ == '__main__':
    folderPath = r'D:\文档\百度店铺\店铺收益'
    # panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    # successFloder=r'D:\文档\百度付费上传文档\百度上传成功'
    cookiesPaths = 'D:\各个电脑cookies\陈钱'

    mainTitleClassifyUploadApi(folderPath, cookiesPaths)
