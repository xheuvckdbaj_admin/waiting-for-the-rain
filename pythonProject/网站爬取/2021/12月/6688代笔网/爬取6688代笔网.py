# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getBrowserUrl(browser,url):
    try:
        browser.get(url)
    except:
        time.sleep(0.5)
        browser.get(url)

def getHtml(thmlpath, strUrl):
    # getBrowserUrl(browser, strUrl)
    # title=browser.find_element_by_xpath('//div[@class="content"]/div/h1').text
    # contentText=str(browser.find_element_by_xpath('//div[@class="contentxy"]').get_attribute('innerHTML'))
    requests_text = str(requests.get(url=strUrl).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con')[0])
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.g_con > h1')[0].text)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
            #相关关键词：
            #number=1
            # browser = webdriver.Chrome()
            strUrl='https://www.txiaodao.com/dangtuangongzuo/2021/0302/%d.html'
            for i in range(1,83355):
                print(i)
                newUrl=strUrl%i
                try:
                    getHtml(thmlpath, newUrl)
                except:
                    print('出错')
                    continue
if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\6688代笔网'
    main(thmlpath)
