def getProportion(keyPath,sortPath,passTitlePath,noPassTitlePath):
    file=open(keyPath,'r',encoding='utf-8')
    sortFile=open(sortPath, 'w', encoding='utf-8')
    file_lines=file.readlines()
    bigs=[]
    iss = []
    titles=[]
    for i1 in range(0,len(file_lines)):
        big=0
        inumber=0
        title=''
        for i,line in enumerate(file_lines):
            key=line.replace('\n','')
            passNumber=0
            noPassNumber=0
            passFile=open(passTitlePath,'r',encoding='utf-8')
            passFile_lines=passFile.readlines()
            for line in passFile_lines:
                if key in line:
                    passNumber=passNumber+1
            noPassFile = open(noPassTitlePath, 'r', encoding='utf-8')
            noPassFile_lines = noPassFile.readlines()
            for line in noPassFile_lines:
                if key in line:
                    noPassNumber = noPassNumber + 1
            allNumber=passNumber+noPassNumber
            if allNumber==0:
                passZhanbi=0
                noPassZhanbi=0
            else:
                passZhanbi=passNumber*100/allNumber
                noPassZhanbi = noPassNumber * 100 / allNumber
            print(key+'，一共上传的数量：'+str(allNumber)+'，通过的数量：'+str(passNumber)+
                  '，未通过的数量：'+str(noPassNumber)+'，通过的占比：'+str(passZhanbi)+'%'+'，未通过的占比：'+str(noPassZhanbi)+'%')
            if passZhanbi >=big and (not i in iss):
                big=passZhanbi
                inumber=i
                title=key
        bigs.append(big)
        iss.append(inumber)
        titles.append(title)
        sortFile.write(title+'\n')
    print(bigs)
    print(iss)
    print(titles)


def main():
    passTitlePath=r'D:\文档\账号通过的标题\6.25\账号通过的标题\账号通过的标题\all.txt'
    noPassTitlePath=r'D:\文档\账号没通过的标题\6.25\账号没通过的标题\账号没通过的标题\all.txt'
    keyPath=r'./必会通过标题.txt'
    sortPath=r'./排序后的关键词.txt'
    getProportion(keyPath,sortPath, passTitlePath, noPassTitlePath)
if __name__ == '__main__':
    main()