# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.article-text')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.article-main > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
#www.hyheiban.com--工作总结
                url='https://www.hyheiban.com/fanwen/%d/'
                for i in range(1,830032):
                    newUrl=url%i
                    getHtml(thmlpath, newUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
