import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}
def getContent(hrefUrl,htmlPath,title,driver):
    try:
        requests_text = str(requests.get(url=hrefUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        driver.get(hrefUrl)
        td=driver.find_element(By.XPATH,'//pre[@class="replay-info-txt answer_con"]')
        list_soupa = td.get_attribute('innerHTML')
        print(title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('error')
def mian_weixin(url,htmlPath,keyStr,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('div.rich_media_wrp > h1')[0].text
        list_soupa=str(soup.select('div.rich_media_content')[0]).replace(title,title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('微信公众平台下载成功----',title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://mp.weixin.qq.com/s?__biz=MzU1MzM3NjQ0MA==&mid=2247490840&idx=1&sn=df1049609bfbac1250616b9f18d3d7eb&chksm=fbf292cfcc851bd9553d852c8a09a7995ec55128214ff1c67b1d93782a6b9592324fce5cda85&scene=27'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\微信公众平台'
    keyStr='kaixin'
    title=''
    mian_weixin(url,htmlPath,keyStr,title)