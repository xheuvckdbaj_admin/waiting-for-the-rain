# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con_article.f16.c6')[0]).replace('<br/>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.ar_con_title.bj_mt30 > h1.f30')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getAllUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.li_r.fr > h3 > a.f20.nobold.chao')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.eastca.com.cn'+href
        getHtml(thmlpath, newUrl)
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.con_l.fl > div.pnum.hidden.y_ju.clear > a')[-2].get('href')
    allPageNumber=int(href.split('_')[-1].replace('.html',''))
    url=href.split('_')[0]
    for i in range(1,allPageNumber+1):
        newUrl='http://www.eastca.com.cn'+url+'_'+str(i)+'.html'
        print(newUrl)
        getAllUrl(thmlpath, newUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.main > div.con_head.bj_mt30.hd > a')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.eastca.com.cn'+href
        getPageUrl(thmlpath, newUrl)
def main(thmlpath):
            #打开文本图片集
            strUrl='http://www.eastca.com.cn/'
            getTitleUrl(thmlpath,strUrl)



if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
