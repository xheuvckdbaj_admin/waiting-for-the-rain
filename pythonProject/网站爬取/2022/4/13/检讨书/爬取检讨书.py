# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url




def getHtml(thmlpath, strUrl):

        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            ulContent=str(soup.select('ul.PContent'))
            print(strUrl)
            list_soupa = str(soup.select('div.content')[0]).replace(ulContent,'')
            title = soup.select('div.article > h1.title')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getRelativeUrl(urlFile,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.content > p > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        print(href)
        urlFile.write(href+'\n')
def getDownUrl(number,urlFile,thmlpath,strUrl):
    if number < 3200:
        number+=1
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.jtnear > div.prev > a')[0].get('href')
        print(href)
        urlFile.write(href+'\n')
        getRelativeUrl(urlFile, strUrl)
        getDownUrl(number, urlFile, thmlpath, href)
def getUpUrl(number,urlFile,thmlpath,strUrl):
    if number < 3200:
        number+=1
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.jtnear > div.prev > a')[0].get('href')
        print(href)
        urlFile.write(href+'\n')
        getRelativeUrl(urlFile, strUrl)
        getUpUrl(number, urlFile, thmlpath, href)


def getListUrl(urlFile,thmlpath, strUrl):
    # if number < 3200:
    #     number+=1
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('div.jtml_item > a')
        for i,a in enumerate(a_list):
            href=a.get('href')
            print(href)
            number=0
            urlFile.write(href+'\n')
            getRelativeUrl(urlFile,href)
            getDownUrl(number, urlFile, thmlpath, href)
            getUpUrl(number, urlFile, thmlpath, href)

def main(thmlpath):
    # urlTxtPath=r'./url.txt'
    # urlFile=open(urlTxtPath,'w',encoding='utf-8')
    txtPath=r'./url.txt'
    file=open(txtPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for i,line in enumerate(file_lines):
        print('---------',i)
        url=line.replace('\n','')
        getHtml(thmlpath, url)
    # urlFile.close()

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
