# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import time
import urllib
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver2
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options=webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser=webdriver.Chrome(options=options)
    time.sleep(1)
    return browser

def addCookies(browser, url, filePath):
        browser.get(url)
        time.sleep(1)
        cookies = readTxt.readTxt(filePath)
        for item in cookies.split(';'):
            cookie = {}
            itemname = item.split('=')[0]
            iremvalue = item.split('=')[1]
            cookie['domain'] = '.baidu.com'
            cookie['name'] = itemname.strip()
            cookie['value'] = urllib.parse.unquote(iremvalue).strip()
            browser.add_cookie(cookie)
        browser.get(url)

def getNewWindow(browser,xpathStr,timeNumber):
    n=browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element=WebDriverWait(browser,timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH,xpathStr)
        )
    )
def getTitle(browser,file):
    xpathStr='//span[@class="doc-title"]'
    getNewWindow(browser, xpathStr, 10)
    elements=browser.find_elements(By.XPATH,xpathStr)
    for element in elements:
        title=element.get_attribute('title')
        print(title)
        file.write(title+'\n')

def ClickNextPage(browser,pageNumber):
    try:
        # pageNumber最大为1，最小为0
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        browser.find_elements(By.XPATH, xpathStr)[pageNumber].click()
        getNewWindow(browser, xpathStr, 10)
        time.sleep(2)
    except:
        print('error')
def clickNextTitle(browser,pageNumber):
    # pageNumber最小为0
    if pageNumber==9:
        xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
        browser.find_element(By.XPATH,xpathStrs).click()
        getNewWindow(browser, xpathStrs, 10)
        time.sleep(2)
    xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
    browser.find_elements(By.XPATH,xpathStr)[pageNumber].click()
    getNewWindow(browser, xpathStr, 10)
    time.sleep(2)
def mainTitle(folderPath, cookiesPath):
   browser=getDriver2('title2')
   url='https://cuttlefish.baidu.com/shopmis#/taskCenter/majorTask'
   addCookies(browser, url, cookiesPath)
   time.sleep(1)
   # getTitle(browser)
   # browser=getBrowserNow1()
   filePath = folderPath + '\\' + str(1) + '.txt'
   file=open(filePath, 'w', encoding='utf-8')
   getTitle(browser,file)
   for pageNumber in range(0,6):
     if pageNumber ==3:
        for i in range(0,3):
            ClickNextPage(browser, pageNumber)
            getTitle(browser,file)
     ClickNextPage(browser, pageNumber)
     getTitle(browser,file)
   file.close()
   for titleNumber in range(0,12):
     filePath = folderPath + '\\' + str(titleNumber+2) + '.txt'
     file = open(filePath, 'w', encoding='utf-8')
     clickNextTitle(browser, titleNumber)
     getTitle(browser,file)
     for pageNumber in range(0, 6):
         if pageNumber == 3:
             for i in range(0, 3):
                 ClickNextPage(browser, pageNumber)
                 getTitle(browser,file)
         ClickNextPage(browser, pageNumber)
         getTitle(browser,file)


   time.sleep(100)


if __name__ == '__main__':
    folderPath = r'G:\文档\百度活动文档2\百度活动文档标题'
    cookiesPath = './baiduCookies1.txt'
    mainTitle(folderPath, cookiesPath)