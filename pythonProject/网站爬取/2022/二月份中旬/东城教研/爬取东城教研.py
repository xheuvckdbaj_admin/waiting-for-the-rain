# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
'Connection': 'close'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl,broswer):
    broswer.get(strUrl)
    time.sleep(2)
    contentText=str(broswer.find_element_by_xpath('//div[@class="artTxt"]/span[@id="str_content"]').get_attribute('innerHTML')).split('<div')[0]
    title=str(broswer.find_element_by_xpath('//div[@class="newsdsp_content"]/h1[@class="artTitle"]').get_attribute('innerHTML'))
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    # soup = BeautifulSoup(requests_text, 'lxml')
    # contentText = str(soup.select('div.artTxt')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>').split('<center')[0]
    # # contentText=re.sub('<span>(.*?)</span>','',content)
    # title=str(soup.select('div.newsdsp_content > h1.artTitle')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getListUrl(thmlpath,strUrl,broswer):
    broswer.get(strUrl)
    time.sleep(2)
    a_list=broswer.find_elements_by_xpath('//div[@class="news_list"]/ul[@class="newsUl"]/li/h2/a')
    # requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    # soup = BeautifulSoup(requests_text, 'lxml')
    # a_list = soup.select('div.news_list > ul.newsUl > li > h2.title > a')
    maps=[]
    for a in a_list:
        href=a.get_attribute('href')
        maps.append(href)
        # try:
    for newUrl in maps:
        getHtml(thmlpath, newUrl,broswer)
        # except:
        #     print('error')
        #     continue

def getMoreListUrl(thmlpath,strUrl,broswer):
    broswer.get(strUrl)
    time.sleep(5)
    a_list=broswer.find_elements_by_xpath('//div[@class="news_list"]/ul[@class="newsUl"]/li/h2/a')
    maps=[]
    for a in a_list:
        href=a.get_attribute('href')
        maps.append(href)
    for href in maps:
        getListUrl(thmlpath,href,broswer)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.tb > ul.mainMenu > li > a')
    broswer=webdriver.Chrome()
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.bjdcfy.com'+href
        getMoreListUrl(thmlpath, newUrl,broswer)
def main(thmlpath):
            #打开文本图片集
                strUrl='http://www.bjdcfy.com/'
                getTitleUrl(thmlpath,strUrl)

if __name__ == '__main__':

    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
