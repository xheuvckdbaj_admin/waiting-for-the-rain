# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')
def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="shuniao_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):
            browser = webdriver.Chrome()
            strUrls=[
                'https://www.shuniao.com/zhouzongjie/66173.html',
                'https://www.shuniao.com/jiduzongjie/63529.html',
                'https://www.shuniao.com/jiduzongjie/63529.html',
                'https://www.shuniao.com/danweizongjie/5801.html',
                'https://www.shuniao.com/xiangmuzongjie/5361.html',
                'https://www.shuniao.com/huiyizongjie/6902.html',
                'https://www.shuniao.com/jiaoxuezongjie/66758.html',
                'https://www.shuniao.com/shixizongjie/66738.html',
                'https://www.shuniao.com/huodongzongjie/66498.html',
                'https://www.shuniao.com/nianzhongzongjie/66582.html',
                'https://www.shuniao.com/gongzuozongjie/66877.html',
                'https://www.shuniao.com/kaitibaogao/5445.html',
                'https://www.shuniao.com/shijianbaogao/65570.html',
                'https://www.shuniao.com/shixibaogao/66857.html',
                'https://www.shuniao.com/diaochabaogao/65604.html',
                'https://www.shuniao.com/diaoyanbaogao/2304.html',
                'https://www.shuniao.com/shuzhibaogao/66376.html',
                'https://www.shuniao.com/gongzuobaogao/5800.html',
                'https://www.shuniao.com/caigouhetong/66846.html',
                'https://www.shuniao.com/zhuangxiuhetong/66829.html',
                'https://www.shuniao.com/gongchenghetong/66845.html',
                'https://www.shuniao.com/xiaoshouhetong/66848.html',
                'https://www.shuniao.com/shigonghetong/66856.html',
                'https://www.shuniao.com/laodonghetong/66756.html',
                'https://www.shuniao.com/goufanghetong/66583.html',
                'https://www.shuniao.com/zulinhetong/66863.html',
                'https://www.shuniao.com/jianyishu/66858.html',
                'https://www.shuniao.com/chengnuoshu/66849.html',
                'https://www.shuniao.com/changyishu/66865.html',
                'https://www.shuniao.com/baozhengshu/66851.html',
                'https://www.shuniao.com/shenqingshu/66870.html',
                'https://www.shuniao.com/weiwenxin/66867.html',
                'https://www.shuniao.com/jieshaoxin/66371.html',
                'https://www.shuniao.com/daoqianxin/66847.html',
                'https://www.shuniao.com/biaoyangxin/66854.html',
                'https://www.shuniao.com/ganxiexin/66839.html',
                'https://www.shuniao.com/tuijianxin/5860.html',
                'https://www.shuniao.com/zijianxin/66832.html',
                'https://www.shuniao.com/cizhixin/1634.html',
                'https://www.shuniao.com/qiuzhixin/66836.html',
                'https://www.shuniao.com/zhuchici/66411.html',
                'https://www.shuniao.com/fayangao/66850.html',
                'https://www.shuniao.com/yanjianggao/66875.html',
                'https://www.shuniao.com/jingxuangao/5406.html',
                'https://www.shuniao.com/guangbogao/5798.html',
                'https://www.shuniao.com/yaoqinghan/66861.html',
                'https://www.shuniao.com/huodongcehua/66868.html',
                'https://www.shuniao.com/xuexijihua/66866.html',
                'https://www.shuniao.com/huodongjihua/66859.html',
                'https://www.shuniao.com/gongzuojihua/66879.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    # main(thmlpath)
    mainGetHtml(thmlpath, txtFilepath)
