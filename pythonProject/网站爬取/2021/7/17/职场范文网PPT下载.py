# -*- coding:utf-8 -*-
import re
import time

import docx
import requests

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath,url,title):

        #time.sleep(10)
            #url='http://www.beiku.com/xieyishu/article_145325.html'
        # try:

            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            # soup = BeautifulSoup(requests_text, 'lxml')
            # list_soupa = str(soup.select('.content')[0])
            thmlpath = thmlpath + '\\' + title + '.rar'
            f = open(thmlpath, 'wb')
            f.write(requests_text)
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
        # except:
        #     print('error')
        #     return ''

# 获取最终网址
def getUrl(thmlpath,url,title):
    requests_text = requests.get(url=url, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    href=soup.select('a')[0].get('href')
    newHref='https://www.hunanhr.cn/index.php'+href
    getContent(thmlpath,newHref,title)



def getHtml3(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('#ppt_a > a')[0].get('href')
    title=soup.select('.content > h1')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
    getUrl(thmlpath,href,title)
def getHtml2(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.bt5 > span > a')
    for a in a_list:
        href = a.get('href')
        getHtml3(thmlpath, href)
def getHtml(thmlpath, strUrl):
    requests_text = requests.get(url=strUrl, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.mleft.fl > .ppt_ul > li > a')
    for i in range(0, len(a_list)):
        # time.sleep(10)
        href = a_list[i].get('href')
        getHtml3(thmlpath, href)
def combination(thmlpath, strUrl):
    getHtml(thmlpath, strUrl)
    newUrl=strUrl+'2.html'
    getHtml(thmlpath, newUrl)

def main(thmlpath):
            number=1
            strUrl='https://www.hunanhr.cn/pptxiazai/'
            combination(thmlpath, strUrl)



if __name__ == '__main__':
    thmlpath = r'E:\职场范文网PPT下载'
    main(thmlpath)
