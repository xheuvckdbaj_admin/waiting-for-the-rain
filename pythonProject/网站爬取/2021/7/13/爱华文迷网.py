# -*- coding:utf-8 -*-

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):

        #time.sleep(10)
        #url='http://m.pincai.com/article/2300000.htm'
        try:
            requests_text = requests.get(url=url, headers=headers).content

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.article_content')[0])
            title = soup.select('.overview > .artitle_title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            #list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#AListBox > ul > li > a')
    for a in a_list:
            #time.sleep(10)
            href=a.get('href')
            title=a.get('title').replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            newUrl='https:'+href
            print(href)
            getContent(thmlpath,newUrl,title)
def getHtml3(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.list_ul > li > a')
    for a in a_list:
        #time.sleep(10)
        href = 'http://www.zhuna.cn'+a.get('href')
        title=a.get('title')
        getContent(thmlpath, href,title)


def getHtml2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('ul > li > a')
    for a in a_list:
        # time.sleep(10)
        href = a.get('href')
        title = a.get('title')
        getContent(thmlpath, href, title)
def getHtml(thmlpath, strUrl,number):
    for i in range(number,90000):
        newUrl=strUrl%i
        getContent(thmlpath, newUrl)
def main(thmlpath):
            number=28684
            strUrl='http://www.ahanw.cn/zongjiebaogao/%d/'
            getHtml(thmlpath, strUrl,number)


if __name__ == '__main__':
    thmlpath = r'D:\文档\爱华文迷网'
    main(thmlpath)
