import requests


def check_proxy(ip):
    proxies = {'http':ip}
    url = "http://www.baidu.com/"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36'
    }
    try:
        response = requests.get(url, headers=headers, proxies=proxies, timeout=2)
        #print(response.text)
        return True
    except:
        print('ip不可用')
        return False
def main():
    ip='120.38.17.60:10474'
    check_proxy(ip)
if __name__ == '__main__':
    main()