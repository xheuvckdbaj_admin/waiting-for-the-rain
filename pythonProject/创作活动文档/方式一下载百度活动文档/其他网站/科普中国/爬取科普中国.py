import re

import requests
from bs4 import BeautifulSoup
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_kepuzhonguo(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('span > h1')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.lemma-summary')[0]).replace(title,keyStr).split('本文网址：')[0]
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('科普中国下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://baike.baidu.com/item/%E5%BA%9E%E5%8A%A0%E8%8E%B1%E4%B8%8D%E7%AD%89%E5%BC%8F/19127706'
    htmlPath=r'D:\文档\百度付费上传文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_kepuzhonguo(url,htmlPath,keyStr,title)