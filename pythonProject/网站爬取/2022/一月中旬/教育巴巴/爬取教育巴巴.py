# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

def getContent(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con_article.f16.c6')[0]).replace('<br/>', '</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title = str(soup.select('div.ar_con_title.louti > h1.f30')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()



def getAllUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.li_r > h3 > a.f18.bt_a.chao.nobold')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.jiaoyubaba.com'+href
        try:
            getContent(thmlpath, newUrl)
        except:
            print('error')
def getPageUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    href = soup.select('div.pnum > ul > a')[-2].get('href')
    allPageNumber=int(href.split('_')[-1].replace('.html',''))
    url='http://www.jiaoyubaba.com'+href.replace(href.split('_')[-1],'')
    for i in range(1,allPageNumber+1):
        newUrl=url+str(i)+'.html'
        try:
            getAllUrl(thmlpath, newUrl)
        except:
            print('error')
            continue

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.lm_head.bj_mt10 > a.head.f20.fl')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.jiaoyubaba.com'+href
        try:
            getPageUrl(thmlpath, newUrl)
        except:
            print('error')
            continue

def main(txtPath,thmlpath):
    # 百度搜索“77
    # cn”或“免费范文网”即可找到本站免费阅读全部范文。收藏本站方便下次阅读，免费范文网，提供经典小说申报材料人民满意公务员先进集体申报材料在线全文阅读
    file=open(txtPath,'r',encoding='utf-8')
    lines=file.readlines()
    for line in lines:
        url=line.replace('\n','')
        print(url)
        try:
            getTitleUrl(thmlpath, url)
        except:
            getPageUrl(thmlpath, url)



if __name__ == '__main__':
    txtPath=r'./lianjie.txt'
    thmlpath = r'E:\文档\爬取网站\教育巴巴'
    main(txtPath,thmlpath)
