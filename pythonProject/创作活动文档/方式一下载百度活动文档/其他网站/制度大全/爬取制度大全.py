import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_zhidudaquan(url, htmlPath, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('h1.article-title > a')[0].text.replace('\r','').replace('\n','').replace(' ','')
        list_soupa = str(soup.select('article.article-content')[0])
        list_soupa=list_soupa.replace(title, title).split('<style')[0]
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('制度大全下载成功----', title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'http://www.qiquha.com/html/202203/788458.html'
    htmlPath = r'D:\文档\百度付费上传文档\百度html\第一范文网'
    keyStr = 'kaixin'
    title = ''
    mian_zhidudaquan(url, htmlPath, keyStr, title)
