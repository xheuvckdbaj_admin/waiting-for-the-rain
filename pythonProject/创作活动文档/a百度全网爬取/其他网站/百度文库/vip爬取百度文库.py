# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import re
import time
import urllib
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.其他网站.百度文库.爬取百度文库 import main_other_baiDu
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt


def getBrowserNow():
    options=webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser=webdriver.Chrome(options=options)
    time.sleep(1)
    return browser

def addCookies(browser, url):
        browser.get(url)
        time.sleep(1)
        cookies = 'BIDUPSID=4711EA38DE38D236D37E466302D33A1F; PSTM=1666180901; BAIDUID=4711EA38DE38D236700B6426810862D3:FG=1; H_PS_PSSID=37546_36552_37357_37491_36885_34813_37486_36786_37534_37498_26350_37351_37459; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; BAIDUID_BFESS=4711EA38DE38D236700B6426810862D3:FG=1; ZFY=jp9Y2crTO3dfGamrQSy:BNMP86jV90LLRyyfBVgvn2Rc:C; delPer=0; PSINO=7; BA_HECTOR=810lak21212h042ha50409oi1hkvqcu1b; Hm_lvt_d8bfb560f8d03bbefc9bdecafc4a4bf6=1666181537; BDUSS=cwUDVLYlhWQjdxdDRxQ2h3dWdoa0dtYzhLOVJvZHpIeUVjVkZSclR6cE9zSGRqRVFBQUFBJCQAAAAAAQAAAAEAAABgrB551tzDzuf3tcS6o73HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE4jUGNOI1BjNW; BDUSS_BFESS=cwUDVLYlhWQjdxdDRxQ2h3dWdoa0dtYzhLOVJvZHpIeUVjVkZSclR6cE9zSGRqRVFBQUFBJCQAAAAAAQAAAAEAAABgrB551tzDzuf3tcS6o73HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE4jUGNOI1BjNW; layer_show_times_by_day_1_cfb795fa7e954cd24c2dca0215f3061e=1; layer_show_times_total_1_cfb795fa7e954cd24c2dca0215f3061e=1; Hm_lpvt_d8bfb560f8d03bbefc9bdecafc4a4bf6=1666196751; ab_sr=1.0.1_MmU0MzVlYjVmOWI2ZTVlMWRlMzY0MTI2ODA3YzhhM2QwYjJmNDgyZDQ4YzIzOGRmZDZiNWU1ZTFjM2U3YmE4ZmYxNTY4MTJmMDAxMzU2NWQ4Nzg3M2Q5NjE5ZGQ1MjliYzgyNDVmMjA4MGI4MmRiYmEzYjhkNWQwMGMxNjMzMGJhM2ZkMmZlZjAzNTgzMjMxMzUyNzgzYmRkZmE5ZDEzNjFhNWQ0MGUwZTE0NmM1NzJhZmQ0NWYxYWNkMWI2YjU2; bcat=927531d6cbda6fdc43fede22cca18b43c20e236e6b984a67ecd667bcc0543fc82de074e8bfd117c0f68ca4db55faa214863056bb57b910fe5067c9b3d6e834d4dfa4a171a0f045319063ab28b8a52cec0fa0e0982161d708a30871b10b7af15f'
        for item in cookies.split(';'):
            cookie = {}
            itemname = item.split('=')[0]
            iremvalue = item.split('=')[1]
            cookie['domain'] = '.baidu.com'
            cookie['name'] = itemname.strip()
            cookie['value'] = urllib.parse.unquote(iremvalue).strip()
            browser.add_cookie(cookie)
        browser.get(url)

def getNewWindow(browser,xpathStr,timeNumber):
    n=browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    time.sleep(2)
    element=WebDriverWait(browser,timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH,xpathStr)
        )
    )
def getTitle(browser,file):
    xpathStr='//span[@class="doc-title"]'
    getNewWindow(browser, xpathStr, 10)
    elements=browser.find_elements(By.XPATH,xpathStr)
    for element in elements:
        title=element.get_attribute('title')
        print(title)
        file.write(title+'\n')

def ClickNextPage(browser,pageNumber):
    #pageNumber最大为1，最小为0
    xpathStr='//ul[@class="el-pager"]/li[@class="number"]'
    browser.find_elements(By.XPATH,xpathStr)[pageNumber].click()
    getNewWindow(browser, xpathStr, 10)
    time.sleep(2)
def saveFile(folderPath,title,driver,keyStr,url,number):
    # number += 1
    try:
        if number < 5:
            # driver.find_element(By.XPATH,'div[@class="doc-title normal-screen-mode normal-screen-mode"]').text
            driver.get(url)
            time.sleep(0.5)
            try:
                element=driver.find_element(By.XPATH, '//div[@class="load-more-link"]')
                text=driver.find_element(By.XPATH, '//div[@class="load-more-link"]/div/span[@class="text"]').get_attribute('innerHTML').replace('\n','').replace('        ','').replace('    ','')
                print(text)
                if  not '剩余0%未加载' in text:
                    try:
                        element.click()
                    except:
                        time.sleep(0.5)
                        element.click()
                n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                driver.switch_to.window(n[-1])
                time.sleep(0.5)
            except:
                print('不能点击未加载')
            try:
                element = WebDriverWait(driver, 5).until(
                    EC.presence_of_element_located((By.XPATH, '//div[@class="ie-fix"]'))
                )
            except:
                time.sleep(0.1)
            td = driver.find_element(By.XPATH, '//div[@class="ie-fix"]')
            list_soupa = td.get_attribute('innerHTML').replace(title,keyStr).replace('p','a').replace('&nbsa; ','</p>').replace('&nbsa;','</p>')
            title = title.replace(' - ', '')
            thmlpath = folderPath + '\\' + keyStr + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
            print('百度文档下载成功----', keyStr)
        else:
            print('该文档不能下载')
    except:
        print('百度文档复制失败')
        driver.quit()
        main_other_baiDu(folderPath,url,keyStr,title)
        # saveFile(folderPath, title, driver, keyStr, url, number)
def mainBaiDuVip(folderPath,url,keyStr,title):
    if 'wenku.baidu.com' in url:
        driver = getDriver('baidu')
        addCookies(driver, url)
        title = re.sub('\d+篇', '', title)
        title=re.sub('【.*?】','',title)
        title = title.replace('（', '').replace('）', '').replace('）', '')
        number=0
        saveFile(folderPath,title,driver,keyStr,url,number)
        driver.quit()
if __name__ == '__main__':
    url=r'https://wenku.baidu.com/view/afc85853dcccda38376baf1ffc4ffe473268fd4e?fr=hp_sub'
    title='第9课 古代的商路、贸易与文化交流 课件-【新教材】统编版(2019)高中历史选择性必修3'
    thmlpath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    cookiesPath=r'./baiduCookies1.txt'
    mainBaiDuVip(thmlpath,url,keyStr,title)