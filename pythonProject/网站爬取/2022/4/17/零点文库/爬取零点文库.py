# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl

import sys

from selenium import webdriver

sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            print(strUrl)
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.article_content')[0]).replace('<br>','<p><p/>').replace('<br/>','<p><p/>')\
                .replace('　　','<p><p/>').replace('一、','<p><p/>一、').replace('二、','<p><p/>二、').replace('三、','<p><p/>三、')\
                .replace('四、','<p><p/>四、').replace('五、','<p><p/>五、').replace('(一)','<p><p/>(一)').replace('(二)','<p><p/>(二)')\
                .replace('(三)','<p><p/>(三)').replace('(四)','<p><p/>(四)').replace('(五)','<p><p/>(五)').replace('1、','<p><p/>1、')\
                .replace('2、','<p><p/>2、').replace('3、','<p><p/>3、').replace('（1）','<p><p/>（1）').replace('（2）','<p><p/>（2）')\
                .replace('（3）','<p><p/>（3）').replace('（4）','<p><p/>（4）').replace('①','<p><p/>①').replace('②','<p><p/>②')\
                .replace('【','<p><p/>【').replace('】','】<p><p/>').replace('：','：<p><p/>').replace('精品办公范文荟萃','').replace(' ‎','<p><p/>')
            title = soup.select('div.article_article > h1 > a')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')

def main(thmlpath):
    url='https://ld.foosun.cn/doc/%d.html'
    for i in range(1,1576267):
        newUrl=url%i
        getHtml(thmlpath, newUrl)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
