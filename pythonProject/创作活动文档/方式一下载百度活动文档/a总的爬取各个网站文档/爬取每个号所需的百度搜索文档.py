import time

from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.allyunxing import main_arrangeDocx
from pythonProject.创作活动文档.方式一下载百度活动文档.b整理文档.合并txt内容 import merge
from pythonProject.创作活动文档.方式一下载百度活动文档.c分类百度上传文档.获取活动文档标题 import mainTitleClassify
from pythonProject.创作活动文档.方式一下载百度活动文档.三六零搜索.爬取360搜索 import mianSanLiuLing
from pythonProject.创作活动文档.方式一下载百度活动文档.中国搜索.allchina import mian_zhonguosousuo
from pythonProject.创作活动文档.方式一下载百度活动文档.今日头条.allJinRiWebsit import main_jinritoutiao
from pythonProject.创作活动文档.方式一下载百度活动文档.全网爬取.allWebsit import main_baidu, makeFolder
from pythonProject.创作活动文档.方式一下载百度活动文档.夸克搜索.夸克搜索爬取 import main_kuake
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索.allWebsitBiYing import main_biying
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索国际版.allWebsitBiYing import main_biyingGuoji
from pythonProject.创作活动文档.方式一下载百度活动文档.搜狗搜索.allWebsitSougou import main_sougou
from pythonProject.创作活动文档.方式一下载百度活动文档.百度文库.总获取百度文档word import mainBaiDuWenDang
from pythonProject.创作活动文档.方式一下载百度活动文档.百度问答.allUpload import mainBaiDuWenDa


def main():
    # 该账号cookies
    cookiesPath = './baiduCookies1.txt'
    # 标题存放文件夹
    titleFolder1 = r'E:\文档\百度活动文档\百度活动文档标题'
    titleFolder2 = r'E:\文档\百度活动文档2\百度活动文档标题'
    titleFolder3 = r'E:\文档\百度活动文档3\百度活动文档标题'
    titleFolder4 = r'E:\文档\百度活动文档4\百度活动文档标题'
    # 文档存放文件夹
    folderPath1 = r'E:\文档\百度活动文档\百度html\其他网站'
    folderPath2 = r'E:\文档\百度活动文档2\百度html\其他网站'
    folderPath3 = r'E:\文档\百度活动文档3\百度html\其他网站'
    folderPath4 = r'E:\文档\百度活动文档4\百度html\其他网站'
   #未爬取的网站存放的文件
    wangzhiPath1 = r'./wangzhi1.txt'
    wangzhiPath2 = r'./wangzhi2.txt'
    wangzhiPath3 = r'./wangzhi3.txt'
    wangzhiPath4 = r'./wangzhi4.txt'
    #标题算法测试——爬取的文档原标题与活动标题对比
    titleTestFolder=r'E:\文档\百度活动文档\标题算法测试百度'
    makeFolder(titleTestFolder)
    for i in range(0, 20):
        startnumber = 0 + (300* i)
        last = startnumber + 300
        fileTest=open(titleTestFolder+'\\'+str(i)+'.txt','w',encoding='utf-8')
        #百度搜索账号一
        main_baidu(fileTest, titleFolder1, folderPath1, wangzhiPath1, startnumber, last)
        # 百度搜索账号二
        time.sleep(500)
        main_baidu(fileTest, titleFolder2, folderPath2, wangzhiPath2, startnumber, last)
        # 百度搜索账号三
        time.sleep(500)
        main_baidu(fileTest, titleFolder3, folderPath3, wangzhiPath3, startnumber, last)
        # 百度搜索账号四
        # time.sleep(500)
        # main_baidu(fileTest, titleFolder4, folderPath4, wangzhiPath4, startnumber, last)
        fileTest.close()
    merge(titleTestFolder, titleTestFolder)


if __name__ == '__main__':
    time.sleep(1500)
    main()
