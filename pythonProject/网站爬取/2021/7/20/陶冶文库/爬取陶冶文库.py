# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.taoye.com/gongzuozongjie/27041.html',
                'https://www.taoye.com/huodongzongjie/26967.html',
                'https://www.taoye.com/nianzhongzongjie/27046.html',
                'https://www.taoye.com/shixizongjie/26963.html',
                'https://www.taoye.com/gongzuobaogao/24776.html',
                'https://www.taoye.com/shijianbaogao/26259.html',
                'https://www.taoye.com/shixibaogao/27003.html',
                'https://www.taoye.com/lizhibaogao/25672.html',
                'https://www.taoye.com/yanjianggao/27053.html',
                'https://www.taoye.com/fayangao/27045.html',
                'https://www.taoye.com/zhuchigao/27055.html',
                'https://www.taoye.com/jiayougao/27042.html',
                'https://www.taoye.com/guangbogao/27051.html',
                'https://www.taoye.com/zhujiuci/26153.html',
                'https://www.taoye.com/huanyingci/26149.html',
                'https://www.taoye.com/banjiangci/24409.html',
                'https://www.taoye.com/cizhixin/26993.html',
                'https://www.taoye.com/qiuzhixin/27013.html',
                'https://www.taoye.com/zijianxin/27015.html',
                'https://www.taoye.com/tuijianxin/25710.html',
                'https://www.taoye.com/ganxiexin/27014.html',
                'https://www.taoye.com/biaoyangxin/27054.html',
                'https://www.taoye.com/daoqianxin/27036.html',
                'https://www.taoye.com/jieshaoxin/25936.html',
                'https://www.taoye.com/jiaoxuesheji/27039.html',
                'https://www.taoye.com/jiaoxuejihua/27012.html',
                'https://www.taoye.com/jiaoxuezongjie/27026.html',
                'https://www.taoye.com/jiaoxuefansi/27056.html',
                'https://www.taoye.com/jiaoxuexinde/26849.html',
                'https://www.taoye.com/shuokegao/26769.html',
                'https://www.taoye.com/liuyan/26976.html',
                'https://www.taoye.com/ganyan/27059.html',
                'https://www.taoye.com/xinyu/26163.html',
                'https://www.taoye.com/zhufuyu/27038.html',
                'https://www.taoye.com/shenqingshu/27027.html',
                'https://www.taoye.com/baozhengshu/27049.html',
                'https://www.taoye.com/changyishu/27043.html',
                'https://www.taoye.com/chengnuoshu/27025.html',
                'https://www.taoye.com/jianyishu/27037.html',
                'https://www.taoye.com/yixiangshu/25473.html',
                'https://www.taoye.com/weituoshu/27030.html',
                'https://www.taoye.com/jiantaoshu/27048.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
