# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="inear"]/div[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.yanjianggaowang.com/geshi/article_175355.html',
                'https://www.yanjianggaowang.com/kaichangbai/article_123737.html',
                'https://www.yanjianggaowang.com/zenmexie/article_181370.html',
                'https://www.yanjianggaowang.com/yingyu/article_186532.html',
                'https://www.yanjianggaowang.com/jingpin/article_186381.html',
                'https://www.yanjianggaowang.com/aiguo/article_186345.html',
                'https://www.yanjianggaowang.com/chengxin/article_186365.html',
                'https://www.yanjianggaowang.com/huanbao/article_186531.html',
                'https://www.yanjianggaowang.com/anquan/article_186504.html',
                'https://www.yanjianggaowang.com/zhonghuahun/article_182654.html',
                'https://www.yanjianggaowang.com/lizhi/article_186387.html',
                'https://www.yanjianggaowang.com/qingchun/article_186396.html',
                'https://www.yanjianggaowang.com/lixiang/article_186378.html',
                'https://www.yanjianggaowang.com/mengxiang/article_186364.html',
                'https://www.yanjianggaowang.com/taidu/article_186392.html',
                'https://www.yanjianggaowang.com/yuandan/article_186518.html',
                'https://www.yanjianggaowang.com/xinnian/article_186478.html',
                'https://www.yanjianggaowang.com/chunjie/article_186141.html',
                'https://www.yanjianggaowang.com/sanbafunvjie/article_180566.html',
                'https://www.yanjianggaowang.com/zhishujie/article_186513.html',
                'https://www.yanjianggaowang.com/xiaoxuesheng/article_186362.html',
                'https://www.yanjianggaowang.com/zhongxuesheng/article_186382.html',
                'https://www.yanjianggaowang.com/daxuesheng/article_186499.html',
                'https://www.yanjianggaowang.com/wufenzhong/article_137913.html',
                'https://www.yanjianggaowang.com/yifenzhong/article_186375.html',
                'https://www.yanjianggaowang.com/hushi/article_186488.html',
                'https://www.yanjianggaowang.com/jiaoshi/article_186536.html',
                'https://www.yanjianggaowang.com/jiuzhi/article_186331.html',
                'https://www.yanjianggaowang.com/aigangjingye/article_186525.html',
                'https://www.yanjianggaowang.com/youxiuyuangong/article_186349.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
