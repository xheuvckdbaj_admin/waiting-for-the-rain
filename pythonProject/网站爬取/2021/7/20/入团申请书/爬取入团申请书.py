# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="rt_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.rtsqs.cn/yiqianzi/article_15247.html',
                'https://www.rtsqs.cn/zenmexie/article_14881.html',
                'https://www.rtsqs.cn/chuzhongrutuan/article_15421.html',
                'https://www.rtsqs.cn/gaozhongrutuan/article_15395.html',
                'https://www.rtsqs.cn/daxuerutuan/article_15234.html',
                'https://www.rtsqs.cn/yibaizi/article_11003.html',
                'https://www.rtsqs.cn/erbaizi/article_11016.html',
                'https://www.rtsqs.cn/sanbaizi/article_14986.html',
                'https://www.rtsqs.cn/sibaizi/article_15346.html',
                'https://www.rtsqs.cn/wubaizi/article_15323.html',
                'https://www.rtsqs.cn/liubaizi/article_15350.html',
                'https://www.rtsqs.cn/babaizi/article_15380.html',
                'https://www.rtsqs.cn/geshi/article_14942.html',
                'https://www.rtsqs.cn/chuyiyiqianzi/article_14534.html',
                'https://www.rtsqs.cn/chuyibabaizi/article_14546.html',
                'https://www.rtsqs.cn/chuyiliubaizi/article_15137.html',
                'https://www.rtsqs.cn/chuyiwubaizi/article_15095.html',
                'https://www.rtsqs.cn/chuyisibaizi/article_15142.html',
                'https://www.rtsqs.cn/chuyisanbaizi/article_14984.html',
                'https://www.rtsqs.cn/chuyierbaizi/article_9479.html',
                'https://www.rtsqs.cn/chuyiyibaizi/article_5077.html',
                'https://www.rtsqs.cn/chueryiqianzi/article_13860.html',
                'https://www.rtsqs.cn/chuerbabaizi/article_15327.html',
                'https://www.rtsqs.cn/chuerliubaizi/article_15324.html',
                'https://www.rtsqs.cn/chuerwubaizi/article_15325.html',
                'https://www.rtsqs.cn/chuersibaizi/article_15248.html',
                'https://www.rtsqs.cn/chuersanbaizi/article_15033.html',
                'https://www.rtsqs.cn/chuererbaizi/article_10955.html',
                'https://www.rtsqs.cn/chueryibaizi/article_5225.html',
                'https://www.rtsqs.cn/chusanyiqianzi/article_14726.html',
                'https://www.rtsqs.cn/chusanbabaizi/article_15150.html',
                'https://www.rtsqs.cn/chusanliubaizi/article_15328.html',
                'https://www.rtsqs.cn/chusanwubaizi/article_15329.html',
                'https://www.rtsqs.cn/chusansibaizi/article_15326.html',
                'https://www.rtsqs.cn/chusansanbaizi/article_14893.html',
                'https://www.rtsqs.cn/chusanerbaizi/article_10753.html',
                'https://www.rtsqs.cn/chusanyibaizi/article_4728.html',
                'https://www.rtsqs.cn/gaoyiyiqianzi/article_14558.html',
                'https://www.rtsqs.cn/gaoyibabaizi/article_14588.html',
                'https://www.rtsqs.cn/gaoyiliubaizi/article_15194.html',
                'https://www.rtsqs.cn/gaoyiwubaizi/article_15192.html',
                'https://www.rtsqs.cn/gaoyisibaizi/article_15145.html',
                'https://www.rtsqs.cn/gaoyisanbaizi/article_14871.html',
                'https://www.rtsqs.cn/gaoyierbaizi/article_5023.html',
                'https://www.rtsqs.cn/gaoyiyibaizi/article_4940.html',
                'https://www.rtsqs.cn/gaoeryiqianzi/article_13889.html',
                'https://www.rtsqs.cn/gaoerbabaizi/article_14537.html',
                'https://www.rtsqs.cn/gaoerliubaizi/article_14978.html',
                'https://www.rtsqs.cn/gaoerwubaizi/article_15136.html',
                'https://www.rtsqs.cn/gaoersibaizi/article_15034.html',
                'https://www.rtsqs.cn/gaoersanbaizi/article_14919.html',
                'https://www.rtsqs.cn/gaoererbaizi/article_5026.html',
                'https://www.rtsqs.cn/gaoeryibaizi/article_4796.html',
                'https://www.rtsqs.cn/gaosanyiqianzi/article_14577.html',
                'https://www.rtsqs.cn/gaosanbabaizi/article_14583.html',
                'https://www.rtsqs.cn/gaosanliubaizi/article_14955.html',
                'https://www.rtsqs.cn/gaosanwubaizi/article_14781.html',
                'https://www.rtsqs.cn/gaosansibaizi/article_14897.html',
                'https://www.rtsqs.cn/gaosansanbaizi/article_14826.html',
                'https://www.rtsqs.cn/gaosanerbaizi/article_4855.html',
                'https://www.rtsqs.cn/gaosanyibaizi/article_5264.html',
                'https://www.rtsqs.cn/daxueyiqianzi/article_13895.html',
                'https://www.rtsqs.cn/daxuebabaizi/article_14852.html',
                'https://www.rtsqs.cn/daxueliubaizi/article_14732.html',
                'https://www.rtsqs.cn/daxuewubaizi/article_14916.html',
                'https://www.rtsqs.cn/daxuesibaizi/article_14923.html',
                'https://www.rtsqs.cn/daxuesanbaizi/article_14658.html',
                'https://www.rtsqs.cn/daxueerbaizi/article_10972.html',
                'https://www.rtsqs.cn/daxueyibaizi/article_4570.html',
                'https://www.rtsqs.cn/duituanderenshi/article_15480.html',
                'https://www.rtsqs.cn/geshizys/article_14915.html',
                'https://www.rtsqs.cn/zenmexiezys/article_14976.html',
                'https://www.rtsqs.cn/chuzhongzys/article_13615.html',
                'https://www.rtsqs.cn/chuyizys/article_15478.html',
                'https://www.rtsqs.cn/chuerzys/article_15021.html',
                'https://www.rtsqs.cn/chusanzys/article_15295.html',
                'https://www.rtsqs.cn/gaozhongzys/article_14190.html',
                'https://www.rtsqs.cn/gaoyizys/article_14186.html',
                'https://www.rtsqs.cn/gaoerzys/article_14503.html',
                'https://www.rtsqs.cn/gaosanzys/article_13552.html',
                'https://www.rtsqs.cn/daxuezys/article_15472.html',
                'https://www.rtsqs.cn/fanwenzys/article_14461.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
