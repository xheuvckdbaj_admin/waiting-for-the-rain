import random
def getFileName():
    time=['','最新','2021','最近','2020-2021']
    site=['','重庆']
    school=['','人和街小学','巴蜀小学','人民小学','树人小学','谢家湾小学','渝中区中华路小学','沙坪坝小学',
            '南岸区珊瑚小学','沙坪坝第一实验小学校','江北区新村同创小学','南开小学','江北区新村小学',
            '天台岗小学','北部新区星光小学','铁路小学','江北区鲤鱼池小学',
            '九龙坡区第一实验小学','渝中区马家堡小学','江北区华新实验小学','南坪实验小学',
            '万州区','涪陵区','渝中区','大渡口区','江北区','沙坪坝区','九龙坡区','南岸区',
            '北碚区','綦江区','大足区','渝北区','巴南区','黔江区','长寿区','江津区','合川区',
            '永川区','南川区','璧山区','铜梁区','潼南区','荣昌区','开州区','梁平区','武隆区','城口县',
            '丰都县','垫江县','忠县','云阳县','奉节县','巫山县','巫溪县','石柱土家族自治县','秀山土家族苗族自治县',
            '酉阳土家族苗族自治县','彭水苗族土家族自治县']
    subject=['','语文','语文','语文','语文','语文','语文','语文']
    ce=['二年级上册','二年级下册','二年级','','二年级上册','二年级下册','二年级','二年级上册','二年级下册','二年级']
    classify=['','练习','入学','升学','期中','第一月考','第二月考','第三月考','第四月考','月考','期末',
              '周末','统考','思维训练','水平测试','模拟']
    quanzhen=['全真','','','','','','','','']
    exam=['试卷','试题']
    answer=['及答案分析','（含答案）','','及答案分析','（含答案）']
    upload=['','','下载']
    #random.randint(0,len(time)-1)
    fileName=time[random.randint(0,len(time)-1)]+site[random.randint(0,len(site)-1)]\
             +school[random.randint(0,len(school)-1)]+subject[random.randint(0,len(subject)-1)]+ce[random.randint(0,len(ce)-1)]+classify[random.randint(0,len(classify)-1)]+quanzhen[random.randint(0,len(quanzhen)-1)]+exam[random.randint(0,len(exam)-1)]\
    +answer[random.randint(0,len(answer)-1)]+upload[random.randint(0,len(upload)-1)]
    print(fileName)
    return fileName
