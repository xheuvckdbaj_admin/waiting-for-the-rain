import os
import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from pythonProject.创作活动文档.方式一下载百度活动文档.各个网站选择爬取.爬取各个网站 import chociesWebsite
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索.爬取各个网站 import chociesBiYingWebsite

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


# index_content是首页查询时每栏下标网址;driver；keyStr是活动文档标题;folderPath是存放各类文件夹的文件夹
def colseDriver(driver):
    driver.close()
    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    driver.switch_to.window(n[-1])
    time.sleep(0.5)


def getUrl(driver,element):
    element.click()
    hrefUrl = element.get_attribute('href')
    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
    driver.switch_to.window(n[-1])
    wenKuUrl = hrefUrl
    return wenKuUrl


def websiteJudeBiYing(element, folderPath, driver, keyStr, urls):
    try:
        hrefUrl = getUrl(driver,element)
        chociesBiYingWebsite( folderPath, driver, keyStr, urls, hrefUrl)
    except:
        print('error')


def main():
    index_content = ''
    folderPath = r''
    driver = getDriver('san')
    keyStr = ''
    websiteJudeBiYing(index_content, folderPath, driver, keyStr)


if __name__ == '__main__':
    main()
