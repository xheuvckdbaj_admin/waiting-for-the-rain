import re

import requests
from bs4 import BeautifulSoup
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_hanyucidian(url,htmlPath,keyStr,title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        except:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title=soup.select('span.crumbs > h1')[0].text
        title=re.sub('（(.*)）', '', title)
        title=re.sub('\d+篇','',title)
        list_soupa=str(soup.select('div.row.mt10')[0]).replace(title,keyStr).replace('&gt;','')
        try:
            content=str(soup.select('div.row.mt10 > div.panel-content-text.f14.word-ah.ml10')[0])
            content1=str(soup.select('div.row.mt10 > h3')[-1])
            list_soupa=list_soupa.replace(content,'').replace(content1,'')
        except:
            print('hh')
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('汉语词典下载成功----',keyStr)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://mcd.xiaowazi.com/jyc-0nti.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\其他网站\第一范文网'
    keyStr='kaixin'
    title=''
    mian_hanyucidian(url,htmlPath,keyStr,title)