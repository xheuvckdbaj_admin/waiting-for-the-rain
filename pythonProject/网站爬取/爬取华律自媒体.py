# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'}
cookie={
    'cookie': 'hl.guid=0263fba0-d088-4fa5-91a1-650e20794a7d; fp=0172a5de-9cdd-d93a-42b1-b3d3d6edf867; Hm_lvt_3aaad93af25e7a89cb4819ab0e71455e=1624631715; Hm_lvt_ac3abb01a9ad71f2dc9f7344e138c993=1622547162,1624634540,1624841846,1624842538; b2=0331a4025141697282a4419f8df765aac1f0cd54; a=1500105; FG=637604878452491759; hl.uid=7edce4fd-990c-4a2e-8cd3-f7deb1c25f74; Hm_lvt_423c58b68be551b57320c9c21aab7d6f=1624862343,1624878193; Hm_lpvt_423c58b68be551b57320c9c21aab7d6f=1624878193; hm.sid=97ed0c32-44cb-04ec-2e36-941d075608f1'
}

def groupUrl(str):
    url = f'http://www.ruiwen.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers,cookie=cookie,).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title,i):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        print(title)
        list_soupa = str(soup.select('.det-nr')[0])
        #print(list_soupa)

        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
    except:
        print('错误')
        number=i
        print(i)
        nurl = 'https://news.66law.cn/article/ajax/getarticlepage/'
        getUrl(thmlpath, nurl, number)

# 获取最终网址
def getUrl(thmlpath,url,number):
    for i in range(number, 10000):
        data = {
            'PageIndex': i,
            'PageSize': '6',
            'IsRecommend': '1',
            'IsUser': '1',
            'IsAuth': '1'
        }
        print(i)
        requests_text = str(requests.post(url=url, data=data,headers=headers).content.decode(encoding='utf-8'))
        json1=json.loads(requests_text)
        data_list=json1['data']['data']
        for data in data_list:
            id=data['id']
            createTimeLink=data['createTimeLink']
            newUrl='https://news.66law.cn/a/'+str(createTimeLink)+'/'+str(id)+'.html'
            title=data['articleTitle'].replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','').replace('X','')

            getContent(thmlpath,newUrl,title,i)

def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('#NEW_INFO_LIST > dl')
    #print(url_list)
    for u in url_list:
            src=u.select('dt > a')[0].get('href')
            href='http://www.banzhuren.cn'+src
            print(href)
            title = u.select('dt > a')[0].text
            getContent(thmlpath,href,title)

def getUrlPageNum(url,str):
    for pageNum in range(1,1000):
        try:
            if pageNum==1:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    newUrl=url+str+'.html'
                    getUrlIndex(thmlpath, newUrl)

            else:
                if str=='catalog':
                    oldUrl = url + str + '-%d' + '.html'
                    newUrl = format(oldUrl % pageNum)
                    getUrl(thmlpath, newUrl)
                else:
                    oldUrl=url+str+'_%d'+'.html'
                    newUrl=format(oldUrl%pageNum)
                    getUrlIndex(thmlpath, newUrl)
        except:
            continue





def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        url = groupUrl(s)
        url = url + strName + '/'
        getUrl(thmlpath, url)


def main(thmlpath):


        url = 'https://news.66law.cn/article/ajax/getarticlepage/'
        number=1415
        getUrl(thmlpath, url,number)


if __name__ == '__main__':
    thmlpath = r'E:\华律自媒体'
    main(thmlpath)
