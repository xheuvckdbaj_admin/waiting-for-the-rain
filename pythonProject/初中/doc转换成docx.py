import os
from win32com import client as wc

from FindFile import find_name


def save_doc_to_docx(rawpath, newPath):  # doc转docx
    '''
    :param rawpath: 传入和传出文件夹的路径
    :return: None
    '''
    word = wc.Dispatch("Word.Application")
    # 不能用相对路径，老老实实用绝对路径
    # 需要处理的文件所在文件夹目录
    # filenamelist = os.listdir(rawpath)
    newfileNames = find_name(newPath, [])
    for i in os.listdir(rawpath):
        try:
                print(i)
                filename = i.replace('doc', 'docx')
                fileName1 = i.replace('DOC', 'docx')
                if filename in newfileNames or fileName1 in newfileNames:
                    os.remove(rawpath + '\\' + i)
                else:
                    # 找出文件中以.doc结尾并且不以~$开头的文件（~$是为了排除临时文件的）
                    if (i.endswith('.doc') or i.endswith('.docx') or i.endswith('.DOC')) and not i.startswith('~$'):

                        # try
                        # 打开文件
                        # os.remove(rawpath +'\\'+ '2018-2019学年湖北省武汉市洪山区八年级（下）期中数学试卷.doc')
                        doc = word.Documents.Open(rawpath + '\\' + i)
                        # # 将文件名与后缀分割
                        # rename = os.path.splitext(i)
                        # 将文件另存为.docx
                        # for paragraph in doc.Paragraphs:
                        # par1 = paragraph.Range
                        # paragraph.left_indent = Inches(.25)
                        # par1.ParagraphFormat.Reset()#取消首行缩进
                        # par1.Font.Name = "宋体"
                        # par1.Font.Color = '0'
                        # par1.Font.Size = "10.5"  # 小四

                        if doc.SaveAs(newPath + '\\' + os.path.splitext(i)[0] + '.docx', 12) == None:
                            doc.Close()  # 12表示docx格式 :
                            os.remove(rawpath + '\\' + i)

        except:
            print('error')

            word = wc.Dispatch("Word.Application")
            continue

    word.Quit()


def main():
    # 注意：目录的格式必须写成双反斜杠
    path = r'E:\上传1'
    newPath = r'E:\失败1'
    save_doc_to_docx(path, newPath)


if __name__ == '__main__':
    main()
