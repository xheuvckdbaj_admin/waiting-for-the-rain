import time
import urllib

import requests
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver, getDriver2

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}



def getUrlAndTitle(driver,htmlTxtFilePath,keyStr):
    try:
        elements = driver.find_elements(By.XPATH,
            '//ul[@class="qa-list"]/li[@class="item js-normal-item"]/div[@class="qa-i-hd"]'
                                                      '/h3[@class="g-ellipsis"]/a[@class="item__title"]')
        for i, element in enumerate(elements):
            try:
                title = element.text.replace(' ', '').strip()
                if keyStr[0:2] in title and (keyStr[-2:] in title or '字' in keyStr[-2:]):
                    element.click()
                    n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
                    driver.switch_to.window(n[-1])
                    time.sleep(0.5)
                    try:
                        element = WebDriverWait(driver, 5).until(
                            EC.presence_of_element_located((By.XPATH, '//div[@id="answer"]'))
                        )
                    except:
                        time.sleep(0.1)
                    td = ''
                    try:
                        td = driver.find_element(By.XPATH,'//div[@class="answer-content"]')
                    except:
                        td = driver.find_element(By.XPATH,'//div[@class="answer-content js-resolved-answer src-import"]')
                    list_soupa = td.get_attribute('innerHTML')
                    print(title)
                    thmlpath = htmlTxtFilePath + '\\' + keyStr + '.html'
                    f = open(thmlpath, 'w', encoding='utf-8')
                    f.write(list_soupa)
                    # href = element.get_attribute('href')
                    # hrefUrl = href
                    # print(hrefUrl)
                    # lineStr = hrefUrl + '————' + keyStr
                    # urlFile.write(lineStr+'\n')
                    break
            except:
                print('error')
                continue
    except:
        print('error')
def getPageUrl(key,url,driver,htmlTxtFilePath):
    try:
        keyStr=key.replace('/','')
        driver.get(url)
        time.sleep(0.1)
        try:
            element = WebDriverWait(driver, 2).until(
                EC.presence_of_element_located((By.XPATH, '//ul[@class="qa-list"]/li[@class="item js-normal-item"]/div[@class="qa-i-hd"]'
                                                          '/h3[@class="g-ellipsis"]/a[@class="item__title"]'))
            )
        except:
            time.sleep(15)
        getUrlAndTitle(driver,htmlTxtFilePath,keyStr)
    except:
        print('error')

def getKey(txtPath,url,driver,htmlTxtFilePath):
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i,line in enumerate(lines):
        if i > -1:
            if 2==i%25:
                try:
                    driver.quit()
                    driver = getDriver2(str(i))
                except:
                    print('关闭错误')
            time.sleep(1)
            key=line.replace('\n','')
            newUrl=url%key
            print('-----------------',key)
            getPageUrl(key, newUrl,driver,htmlTxtFilePath)
    # pathFile.close()
def main360wenda(txtPath,htmlTxtFilePath):
    urls=[]
    driver = getDriver2('san')
    url='https://wenda.so.com/search/?q=%s&src=tab_www'
    txtFilePaths=find_file(txtPath)
    for txtFilePath in txtFilePaths:
        getKey(txtFilePath, url,driver,htmlTxtFilePath)
    driver.quit()
    # return urls


if __name__ == '__main__':
    #标题文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'
    #存放360问答文档的文件夹
    htmlTxtFilePath=r'D:\文档\百度活动文档\百度html'
    main360wenda(txtPath,htmlTxtFilePath)
