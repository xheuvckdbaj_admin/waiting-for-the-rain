# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.xianxue.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    try:
        requests_text = requests.get(url=url, headers=headers).content
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        #print(title)
        soupa = soup.select('.xx_lanmums')[0]
        content=str(soupa)
        print(content)
        title=soupa.select('.xx_lanmums_jj > .xx_lanmums_jj_tl')[0].text.encode(encoding='gbk').decode(encoding='gbk').replace('\r','').replace('\n','').replace('\t','')
        print(title)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='gbk')
        f.write(content)
    except:
        print('错误')
def getContentgushi(thmlpath, url,title):
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        #title = soup.select('div > h1')[0].text
        #print(title)
        soupa = soup.select('.article')[0]
        title=soupa.select('.title')[0].text

        content=str(soupa.select(('.content'))[0])
        content_txt=re.sub('<a href=.*?</a></p>','',content)
        print(content_txt)
        #title=soupa.select('.xx_lanmums_jj > .xx_lanmums_jj_tl')[0].text
        print(title)
        #print('python' + '\r' + 'java' + '\n' + 'java')
        thmlpath=thmlpath+'\\'+title+'.html'
        f = open(thmlpath, 'w', encoding='gbk')
        f.write(content_txt)
    except:
        print('错误')
# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    li_list=soup.select('.conList > ul > li')
    #print(url_list)
    for u in li_list:
            href=u.select('a')[0].get('href')
            print(href)
            title = u.select('a')[0].text
            getContent(thmlpath,href,title)
def getUrlIndex(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    div_list=soup.select('.xx_lanmums > .xx_mulu')
    #print(url_list)
    for div in div_list:
            li_list=div.select('.xx_mulu_ul1 > li')
            for li in li_list:
                src=li.select('a')[0].get('href')
                href='http://www.xianxue.com'+src
                print(href)
                title = li.select('a')[0].get('title')
                #title = div.select('dt > a')[0].text
                getContent(thmlpath,href,title)
def getUrlIndexgushi(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    div_list=soup.select('.xx_lanmums > .xx_mulu')
    #print(url_list)
    for div in div_list:
            li_list=div.select('.xx_mulu_ul6 > li')
            for li in li_list:
                src=li.select('a')[0].get('href')
                #href='http://www.xianxue.com'+src
                print(src)
                title = li.select('a')[0].get('title')
                #title = div.select('dt > a')[0].text
                getContentgushi(thmlpath,src,title)
def getUrlIndexyuwen(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    li_list=soup.select('.xx_libox_left  ul > li')


    for li in li_list:
                src=li.select('a')[0].get('href')
                #href='http://www.xianxue.com'+src
                print(src)
                title = li.select('a')[0].get('title')
                #title = div.select('dt > a')[0].text
                getContentgushi(thmlpath,src,title)
def getUrlIndexchengyu(thmlpath,url):
    requests_text = requests.get(url=url, headers=headers).content
    soup = BeautifulSoup(requests_text, 'lxml')
    li_list=soup.select('.xx_left > .xx_left_ul > li')


    for li in li_list:
                src=li.select('a')[0].get('href')
                #href='http://www.xianxue.com'+src
                print(src)
                title = li.select('a')[0].get('title')
                #title = div.select('dt > a')[0].text
                getContentgushi(thmlpath,src,title)
def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2):


                    oldUrl = url
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)







def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            getUrl(thmlpath, url)
        except:
            continue


def main(thmlpath):
    str = ['guoxue','gushiwen','yuwen','wenxue',
           'fanwen','yuwen/chengyu','yuwen/zuowen','yuwen/yuedudaan','wenxue/yuyangushi','wenxue/tonghuagushi'
           ]
    for s in str:
        url = groupUrl(s)
        if s=='guoxue':
            getUrlIndex(thmlpath,url)
        elif s=='gushiwen':
            getUrlIndexgushi(thmlpath, url)
        elif s=='yuwen':
            getUrlIndexyuwen(thmlpath, url)
        elif s=='wenxue':
            getUrlIndexyuwen(thmlpath, url)
        elif s=='fanwen':
            getUrlIndexyuwen(thmlpath, url)
        else:
            getUrlIndexchengyu(thmlpath, url)

if __name__ == '__main__':
    thmlpath = r'E:\贤学网'
    main(thmlpath)
