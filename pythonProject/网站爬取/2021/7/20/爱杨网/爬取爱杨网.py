# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number < 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="prev"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.aiyangedu.com/KaiXueDiYiKe/1408874.html',
                'https://www.aiyangedu.com/GuiZhangZhiDu/995546.html',
                'https://www.aiyangedu.com/TongZhiGongGao/966288.html',
                'https://www.aiyangedu.com/YanJiangGao/1408883.html',
                'https://www.aiyangedu.com/HuoJiangGanYan/1398513.html',
                'https://www.aiyangedu.com/KeHouDaAn/965683.html',
                'https://www.aiyangedu.com/GuangBoGaoDaQuan/1402395.html',
                'https://www.aiyangedu.com/SiXiangBaoGao/1403224.html',
                'https://www.aiyangedu.com/CeHuaShu/1409946.html',
                'https://www.aiyangedu.com/BanHuiPinDao/995568.html',
                'https://www.aiyangedu.com/FaYanGao/1408291.html',
                'https://www.aiyangedu.com/ChangYiShu/1408389.html',
                'https://www.aiyangedu.com/ZhuChiCi/1408228.html',
                'https://www.aiyangedu.com/QiHuaWenAn/996069.html',
                'https://www.aiyangedu.com/GuiZhangZhiDu/995546.html',
                'https://www.aiyangedu.com/YaoQingHan/1409432.html',
                'https://www.aiyangedu.com/BaoZhengShu/1409418.html',
                'https://www.aiyangedu.com/QingJiaTiao/995564.html',
                'https://www.aiyangedu.com/ZhiYeSYGH/1402832.html',
                'https://www.aiyangedu.com/XieZuoMoBan/995550.html',
                'https://www.aiyangedu.com/JXXDTH/1408407.html',
                'https://www.aiyangedu.com/HuoDongFangAn/1408377.html',
                'https://www.aiyangedu.com/ZhiYeGuiHuaShu/995559.html',
                'https://www.aiyangedu.com/XinXueQiGZJH/1408403.html',
                'https://www.aiyangedu.com/JianTaoShu/1408397.html',
                'https://www.aiyangedu.com/GanXieXin/1408372.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
