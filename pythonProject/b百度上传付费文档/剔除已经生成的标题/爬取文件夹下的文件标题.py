# -*- coding:utf-8 -*-
import re
import threading
from datetime import time


import os

from pythonProject.FindFile import find_file, find_name
from pythonProject.修改文件提高文档质量.重新生成文档.修改文档格式 import makeWord
from pythonProject.初中.敏感词过滤.minganci import filterWord

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的txt文件
'''


def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    p._p = p._element = None


def deletePara(file):
    for para in file.paragraphs:

        para.text = para.text.replace('\n', ' ')
        if para.text == "" or para.text == '\n' or para.text == ' ':
            # print('第{}段是空行段'.format(i))
            para.clear()  # 清除文字，并不删除段落，run也可以,
            delete_paragraph(para)


# 替换关键词
def tihuanguanjianci(lines, para):
    para.text = para.text.replace('(\
)', '(')
    for line in lines:
        keyValue = line.replace('\n', '')
        key = keyValue.split('===>')[0]
        # print(key)
        value = keyValue.split('===>')[1]
        para.text = para.text.replace(key, value)


def jianchaPara(para):
    if para.text.endswith('，') or para.text.endswith('、') or para.text.endswith('(') or para.text.endswith('{') \
            or para.text.endswith('[') or para.text.endswith('·') or para.text.endswith('——'):
        para.text = para.text[0:-1]
    if (para.text.startswith('。') or para.text.startswith('，') or para.text.startswith('：')
            or para.text.startswith('！') or para.text.startswith('？') or para.text.startswith('、')
            or para.text.startswith('；') or para.text.startswith(')')):
        para.text = para.text[1:]


titlePath = './标题违禁词.txt'


def panduanFileName(fileName):
    file = open(titlePath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = True
    for i, line in enumerate(lines):
        key = line.replace('\n', '')
        panduan = panduan and (not key in fileName)
    return panduan


def deleteLaterParaPanduan(i, content, fileName):
    panduan = False
    content = content.replace(' ', '')
    if (not (i == 1 and i == 2)) and len(content) < 100:
        titleContent = fileName.replace('.txt', '').replace('[', '').replace(']', '').replace('【', '').replace('】',
                                                                                                                '').replace(
            '(', '').replace(')', '').replace(' ', '')
        content = content.replace('[', '').replace(']', '').replace('【', '').replace('】', '').replace('(', '').replace(
            ')', '').replace(' ', '')
        panduan = ((content[-5:-1] == titleContent[-5:-1]) or (content[0:5] == titleContent[0:5]))
    if content == '-' or content == '1' or content == '2' or content == '3' or content == '4' or content == '5' or content == '':
        panduan = True
    return panduan


paghraPath = './删除段落的违禁词.txt'


def panduanPaghra(paghraPath, content):
    file = open(paghraPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan


filterTxt = './内容敏感词.txt'


def filtertxt(panduan, filterTxt, content):
    file = open(filterTxt, 'r', encoding='utf-8')
    lines = file.readlines()
    for i, line in enumerate(lines):
        key = line.replace('\n', '')
        panduan = panduan or (key in content)
    return panduan


# 删除最后一段含有“：”的段落
def merrageLastPrageContentStr(i, paragraphs, contentStr):
    if i == (len(paragraphs) - 1):
        if contentStr.endswith('：'):
            contentStr = ''
    return contentStr


paghraDownPath = './删除以下段落的违禁词.txt'


def panduanPaghraDown(paghraDownPath, content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or word in content
    return panduan


# 删除word段落
def delete_paragraph(paragraph):
    p = paragraph._element
    p.getparent().remove(p)
    # p._p = p._element = None
    paragraph._p = paragraph._element = None


def deleAdvertising(fileFolder,filePath,titleFile):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")

    if doc == 'txt':
        try:
            fileName = os.path.basename(filePath)
            fileName = fileName.replace('.txt', '')
            print(fileName)
            titleFile.write(fileName+'\n')
        except Exception as e:
            print(str(e))




# word.Quit()
def shear_dile(fileFolder,oldFolderPath,titleFile):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i, d in enumerate(os.listdir(oldFolderPath)):
                shear_dile(fileFolder,os.path.join(oldFolderPath, d),titleFile)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.txt'):
            # print(oldFolderPath)
            deleAdvertising(fileFolder,oldFolderPath,titleFile)


def modifyContent(oldFolderPath):
    titlePath = oldFolderPath + '\\' + 'title.txt'
    titleFile = open(titlePath, 'w', encoding='utf-8')
    shear_dile(oldFolderPath,oldFolderPath,titleFile)
    titleFile.close()


def mainElimi(oldFolderPath):
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.txt')
    modifyContent(oldFolderPath)


if __name__ == '__main__':
    oldFolderPath = r'D:\文档\百度付费上传文档\百度html\其他网站'
    mainElimi(oldFolderPath)
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.txt','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
