# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getBrowserUrl(browser,url):
    try:
        browser.get(url)
    except:
        time.sleep(0.5)
        browser.get(url)

def getHtml(browser,thmlpath, strUrl):
    getBrowserUrl(browser, strUrl)
    title=browser.find_element_by_xpath('//div[@class="article"]/h1').text.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    contentText=str(browser.find_element_by_xpath('//div[@class="content"]').get_attribute('innerHTML'))
    # soup = BeautifulSoup(requests_text, 'lxml')
    # contentText = str(soup.select('div.content')[0])
    # # contentText=re.sub('<span>(.*?)</span>','',content)
    # title=str(soup.select('div.article > h1')[0].text)
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').replace(':', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def main(thmlpath):
            # 免责声明：
            #产生版权问题
            #请联系我们及时删除
            browser = webdriver.Chrome()
            strUrl='http://www.51kaogwy.cn/zhengcefagui/2021/0726/%d.html'
            for i in range(1,368829):
                print(i)
                newUrl=strUrl%i
                try:
                    getHtml(browser,thmlpath, newUrl)
                except:
                    print('出错')
                    continue
if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
