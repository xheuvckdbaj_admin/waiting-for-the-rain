# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.kt250.com/fanwen/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(url):

        req = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        req1=re.sub('try.*?\({','{',req)
        #req2 = re.sub('\).*?catch(e){}', '}', req1)
        str_req=req1.split('});')[0]+'}'
        requests_json=json.loads(str_req)
        datas=requests_json.get('result').get('data')
        urls=[]
        for data in datas:
            newUrl1=data.get('url')
            urls.append(newUrl1)
        return urls

#获取每个博主的每篇文章的网址
def getUrlIndex(href):

        #href = 'http://blog.sina.com.cn/s/articlelist_1280818914_0_8.html'
        requests_text = str(requests.get(url=href, headers=headers).content.decode(encoding='utf-8'))
        #print(requests_text)
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list=soup.select('.articleList > div.articleCell.SG_j_linedot1 > p.atc_main.SG_dot > .atc_title > a')
        #print(url_list)
        srcs=[]
        for a in a_list:
                src=a.get('href')
                srcs.append(src)
        return  srcs


#获取每个博主的每篇文章内容
def getUrlContent(thmlpath,href):
    #href = 'http://blog.sina.com.cn/s/blog_537368000102x09k.html'
    try:

        requests_text = str(requests.get(url=href, headers=headers).content.decode(encoding='utf-8'))
        #print(requests_text)
        soup = BeautifulSoup(requests_text, 'lxml')
        if not soup.select('.artical')==[]:
            title = str(soup.select('.artical > .articalTitle > h2.titName.SG_txta')[0].text).replace('?','')\
                .replace('“','').replace('"','').replace('”','').replace('？','').replace('/','')
            content = str(soup.select('.artical > div.articalContent')[0])
            # print(newContent)
            # print('python' + '\r' + 'java' + '\n' + 'java')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(content)
        else:
            title = str(soup.select('.BNE_title > .h1_tit')[0].text).replace('?', '') \
                .replace('“', '').replace('"', '').replace('”', '').replace('？', '').replace('/', '')
            content = str(soup.select('.BNE_cont')[0])
            # print(newContent)
            # print('python' + '\r' + 'java' + '\n' + 'java')
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(content)
    except:
        print('error')

# all
def getUrl(thmlpath,url):
    urls=getContent(url)
    for ul in urls:

            requests_text = requests.get(url=ul, headers=headers).content.decode(encoding='utf-8')
            soup = BeautifulSoup(requests_text, 'lxml')
            a=soup.select('.blognav > .blognavInfo > span > a.on')
            if not a==[]:
                href=str(a[0].get('href')).replace('1.html','%d.html')
                print(href)
                for i in range(1,100):
                    newHref=href%i
                    #print(newHref)
                    srcs=getUrlIndex(newHref)
                    for src in srcs:
                        print(src)
                        getUrlContent(thmlpath,src)



def getUrlPageNum(thmlpath,url):
    for pageNum in range(1,2):


                    oldUrl = url
                    print(oldUrl)
                    getUrl(thmlpath, oldUrl)







def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            getUrl(thmlpath, url)
        except:
            continue
def redTxt(filePath):
    f1 = open(filePath, 'r')
    data1 = f1.readlines()
    urls=[]
    for line in data1:
        print(line)
        urls.append(line)
        return urls

def main(thmlpath,filePath):
    urls=redTxt(filePath)
    for url in urls:
        getUrl(thmlpath,url)


if __name__ == '__main__':
    thmlpath = r'E:\博客文档\新建文件夹 (6)'
    filePath=r'E:\博客文档\新建文本文档 (6).txt'
    main(thmlpath,filePath)
