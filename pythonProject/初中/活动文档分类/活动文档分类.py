# -*- coding:utf-8 -*-
import os

from pythonProject.文件剪切 import ctrlx

'''
把文件夹内部的东西提出来
'''
def shear_dile(src,newPathcidu,newPathhangye,newPathjingli,newPathdushu,newPathxiaoxue,newPathyanjiang,newPathdaxue,newPathzhichang):
    if os.path.isdir(src):
        if not os.listdir(src):
            try:
                os.rmdir(src)
                print(u'移除空目录: ' + src)
            except:
                print('error')
        else:
            for d in os.listdir(src):
                shear_dile(os.path.join(src, d), newPathcidu, newPathhangye, newPathjingli, newPathdushu, newPathxiaoxue,
                           newPathyanjiang, newPathdaxue, newPathzhichang)
    if os.path.isfile(src):
        if '~$' in src:
            try:
                os.remove(src)
            except:
                print('error')
        print(u"文件剪切:", src)
        fn = os.path.basename(src)
        # if not os.path.exists(dst + './' + fn) and not '~$' in src:
        try:
                if fn.endswith('制度') or fn.endswith('规范') or fn.endswith('准则') or fn.endswith('章程')  :
                    print(fn + '需要')
                    ctrlx(src, newPathcidu)
                elif fn.endswith('读书笔记') or fn.endswith('读后感') or fn.endswith('读书心得') :
                    print(fn + '需要')
                    ctrlx(src, newPathdushu)
                elif  (not '高'in fn and not '初'in fn and not '七年级'in fn and not '八年级'in fn \
                        and not  '九年级'in fn and not  '中'in fn) and \
                        ('教案'in fn or '导学案'in fn or '讲义'in fn or '课件'in fn \
                        or '小学'in fn or '教学反思'in fn or '小升初'in fn or '教学计划'in fn) :
                    print(fn + '需要')
                    ctrlx(src, newPathxiaoxue)
                elif '演讲'in fn or '致辞'in fn or '主持词'in fn or '发言'in fn:
                    print(fn + '需要')
                    ctrlx(src, newPathyanjiang)
                elif '医学'in fn or '计算机'in fn or '工程管理'in fn:
                    print(fn + '正在下载')
                    ctrlx(src, newPathdaxue)
                elif (not '实践' in fn or '高' in fn) and ('工作总结'in fn or '日报'in fn or '周报'in fn or '月报'in fn or '季度报'in fn or '年报'in fn\
                        or '报告'in fn or '记录'in fn or '规划'in fn or '晋升技巧'in fn
                        or '年度总结'in fn or '年终总结'in fn or '述职报告'in fn or '报告范文'in fn ):
                    print(fn + '需要')
                    ctrlx(src, newPathzhichang)
                elif '电子'in fn or '电路'in fn or '机械'in fn or '建筑'in fn:
                    print(fn + '需要')
                    ctrlx(src, newPathhangye)
                elif '交互'in fn or '用户研究'in fn or '数据分析'in fn or '产品设计'in fn \
                        or '产品分析'in fn or '业界动态'in fn :
                    print(fn + '需要')
                    ctrlx(src, newPathjingli)

        except:
                print('错误')
                # continue
def main():
    src=r'D:\文档\百度上传文档'
    newPathcidu = r'D:\文档\百度活动文档分类\制度规范'
    newPathhangye = r'D:\文档\百度活动文档分类\行业专业资料'
    newPathjingli = r'D:\文档\百度活动文档分类\产品经理学习资料'
    newPathdushu = r'D:\文档\百度活动文档分类\读书笔记'
    newPathxiaoxue = r'D:\文档\百度活动文档分类\小学语数外资源'
    newPathyanjiang = r'D:\文档\百度活动文档分类\演讲致辞'
    newPathdaxue = r'D:\文档\百度活动文档分类\大学专业试卷'
    newPathzhichang = r'D:\文档\百度活动文档分类\职场办公'
    shear_dile(src,newPathcidu,newPathhangye,newPathjingli,newPathdushu,newPathxiaoxue,newPathyanjiang,newPathdaxue,newPathzhichang)
if __name__ == '__main__':
    main()