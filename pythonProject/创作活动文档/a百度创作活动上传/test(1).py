import requests
import json
import base64
import time
import cv2
import numpy as np
KEY_CODE = 'uDGISlU2'
IP = 'www.ocr.mobi:20000'
free=False
software_key_code = ''#0bagIqnf

def predict(img_path,request_data={}):
    with open(img_path, 'rb') as r:
        img = r.read()
    img = base64.b64encode(img)
    img = str(img, encoding='utf-8')
    request_data['img'] = img
    begin_time = time.time()
    code = requests.post('http://' + IP + '/predict?key_code={}&number={}&free={}&software_key_code={}'.format(KEY_CODE, 10003, free,software_key_code),json.dumps(request_data))
    js = json.loads(code.text)
    data = json.loads(js['content'])
    print(time.time() - begin_time, data)
    return data




image_path="D:\图片\haha.jpg"
angle = predict(image_path)
angle=-int(angle['angle'])
print(angle)

image = np.fromfile(image_path,np.uint8)
image = cv2.imdecode(image,cv2.IMREAD_COLOR)

# 获取图片中心点
image_center = tuple(np.array(image.shape[1::-1]) / 2)

# 获取旋转矩阵
# 参数1是图片中心点，参数2是旋转角度，参数3是缩放比例
scale = 1.0
rotation_matrix = cv2.getRotationMatrix2D(image_center, angle, scale)

# 执行旋转
rotated_image = cv2.warpAffine(image, rotation_matrix, image.shape[1::-1], flags=cv2.INTER_LINEAR)
cv2.imwrite('D:\图片\\1.jpg',rotated_image)
# cv2.imshow('im',rotated_image)
# cv2.waitKey(0)