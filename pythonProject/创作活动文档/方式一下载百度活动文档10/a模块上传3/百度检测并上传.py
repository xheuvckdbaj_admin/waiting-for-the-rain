import os
import time

from playwright.sync_api import Playwright, sync_playwright, expect
from playwright.sync_api import sync_playwright

from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.方式一下载百度活动文档.a模块上传3.百度选择标题上传 import mainBaiduSelectUpload1

guolvPath=r'./过滤词.txt'
def panduanTitle(title):
    panduan = True
    file=open(guolvPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    file.close()
    for line in file_lines:
        key=line.replace('\n','')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan
def panduanTitleUpPass(title,passPath):
    panduan = True
    file=open(passPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    file.close()
    for line in file_lines:
        key=line.replace('\n','')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan
def getNewPage(page,url):
    try:
        page.goto(url)
        time.sleep(0.5)
    except:
        time.sleep(1)
        getNewPage(page, url)
def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page, "https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,"https://cuttlefish.baidu.com/shopmis?_wkts_=1675531752848&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/taskCenter/majorTask")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    try:
        time.sleep(0.5)
        page.get_by_role("button", name="Close").click()
    except:
        print('没有关闭任务规则')
    return page
def getTitleHuodong(page,passTitlePath,titles):
    xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    elements=page.locator(xpathStr)
    try:
            titleStr = elements.nth(0).text_content().strip()
            if panduanTitle(titleStr) and panduanTitleUpPass(titleStr,passTitlePath) and (not titleStr in titles):
                print(titleStr)
                titles.append(titleStr)
    except Exception as e:
            print(e,'获取标题出错')
    return titles

def getNextPage(page,number):
    xpathPageStr='//input[@class="el-input__inner"]'
    page.locator(xpathPageStr).click()
    page.locator(xpathPageStr).fill(str(number))
    page.locator(xpathPageStr).press('Enter')
    time.sleep(1)

def PanduanTitleForquchong(playwright: Playwright,cookiesPath,passTitlePath):

    xpathStrButton = '//div[@class="doc-row"]/div[@class="upload-doc"]'
    xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button[@class="el-button no-agree btn el-button--default el-button--mini"]'
    xpathTitle = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    xpathContent = '//p[@class="major-progress"]/span[@class="context"]'
    xpathzhuanghao = '//div[@class="right-bar"]/div[@class="link"]'
    brower=playwright.chromium.launch(headless=False)
    context=brower.new_context()
    page=context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)

    page.get_by_text("创作活动", exact=True).click()
    lanmuStrs = ['学前教育', '基础教育', '高校与高等教育', '语言/资格考试', '法律', '建筑',
                 '互联网', '行业资料', '政务民生', '商品说明书', '实用模板', '生活娱乐']
    time.sleep(1)
    mukuaiS=[]
    content=page.locator(xpathContent).nth(0).text_content().strip()
    contentzhanghao = page.locator(xpathzhuanghao).nth(0).text_content().strip()
    print(contentzhanghao+'###',content)
    loc=page.locator(xpathStrButton)
    if loc.count() > 0:
        print('推荐','有标题')
        xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
        elements = page.locator(xpathStr)
        if elements.count() > 8:
            titles = []
            number=getTitleHuodong(page, passTitlePath, titles)
            if len(number) >0:
                print('推荐','--------更新了')
                mainBaiduSelectUpload1(cookiesPath)
                mukuaiS.append('推荐')

    time.sleep(2)
    for number,lanmuStr in enumerate(lanmuStrs):
        page.get_by_text(lanmuStr).nth(0).click()
        time.sleep(1)
        content = page.locator(xpathContent).nth(0).text_content().strip()
        contentzhanghao = page.locator(xpathzhuanghao).nth(0).text_content().strip()
        print(contentzhanghao + '###', content)
        locxue = page.locator(xpathStrButton)
        if locxue.count() > 0:
            print(lanmuStr, '有标题')
            xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
            elements = page.locator(xpathStr)
            if elements.count() > 8:
                titles = []
                number = getTitleHuodong(page, passTitlePath, titles)
                if len(number) > 0:

                    print(lanmuStr, '--------更新了')
                    mainBaiduSelectUpload1(cookiesPath)
                    mukuaiS.append(lanmuStr)

def panduanTitleX(cookiesPath,passTitlePath):
    with sync_playwright() as playwright:
        PanduanTitleForquchong(playwright,cookiesPath,passTitlePath)

def titleTestAndUpload(cookiesPath,passTitlePath):
    panduanTitleX(cookiesPath,passTitlePath)
def main():
    # passTitleFolder = r'D:\文档\百度活动文档\百度上传过的标题'
    # titleFolder = r'D:\文档\百度活动文档\百度活动文档标题'
    # titleFolderClassify = r'D:\文档\百度活动文档\用来分类文档标题'
    passTitlePath = r'D:\文档\百度活动文档\百度上传过的标题\titlePass.txt'
    # selectTitlePath = r'D:\文档\百度活动文档\百度活动文档标题' + '\\' + 'select.txt'
    # titleAllPath = r'D:\文档\百度活动文档\百度活动文档标题' + '\\' + 'all.txt'
    # selectPath = r'./必会通过标题.txt'
    cookiesPath = r'./baiduCookies1.txt'
    titleTestAndUpload(cookiesPath,passTitlePath)
if __name__ == '__main__':
    main()