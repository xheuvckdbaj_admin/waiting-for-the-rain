from logging import exception

import win32com.client as win32
def panduanHead(content):
    panduan=content.endswith('一：') or content.endswith('二：') or content.endswith('三：') or \
    content.endswith('四：') or content.endswith('五：') or content.endswith('六：') or \
    content.endswith('七：') or content.endswith('八：') or content.endswith('九：') or \
    content.endswith('十：') or content.endswith('一') or content.endswith('二') or \
    content.endswith('三') or content.endswith('四') or content.endswith('五') or \
    content.endswith('六') or content.endswith('七') or content.endswith('八') or \
    content.endswith('九') or content.endswith('十') or content.endswith('1') or content.endswith('2') \
    or content.endswith('3') or content.endswith('4') or content.endswith('5') or \
    content.endswith('6') or content.endswith('7') or content.endswith('8') or \
    content.endswith('9') or content.endswith('10') or content.endswith('（一）') or content.endswith('（二）') or \
    content.endswith('（三）') or content.endswith('（四）') or content.endswith('（五）') or \
    content.endswith('（六）') or content.endswith('（七）') or content.endswith('（八）') or \
    content.endswith('（九）') or content.endswith('（十）') or content.endswith('(一)') or \
            content.endswith('(一)') or content.endswith('(二)') or content.endswith('(三)') or \
            content.endswith('(四)') or content.endswith('(五)') or content.endswith('(六)') or \
            content.endswith('(七)') or content.endswith('(八)') or content.endswith('(九)') or \
            content.endswith('(十)') or content.endswith('〔一〕') or content.endswith('〔二〕') or \
            content.endswith('〔三〕') or \
            content.endswith('〔四〕') or content.endswith('〔五〕') or content.endswith('〔六〕') or \
            content.endswith('〔七〕') or content.endswith('〔八〕') or content.endswith('〔九〕') or \
            content.endswith('〔十〕') or content.endswith('【一】') or content.endswith('【二】') or \
            content.endswith('【三】') or \
            content.endswith('【四】') or content.endswith('【五】') or content.endswith('【六】') or \
            content.endswith('【七】') or content.endswith('【八】') or content.endswith('【九】') or \
            content.endswith('【十】') or content.endswith('【篇一】') or content.endswith('【篇二】') or \
            content.endswith('【篇三】') or \
            content.endswith('【篇四】') or content.endswith('【篇五】') or content.endswith('【篇六】') or \
            content.endswith('【篇七】') or content.endswith('【篇八】') or content.endswith('【篇九】') or \
            content.endswith('【篇十】' or content.startswith('第1篇') or content.startswith('第2篇') or \
            content.startswith('第3篇') or \
            content.startswith('第4篇') or content.startswith('第5篇') or content.startswith('第6篇') or \
            content.startswith('第7篇') or content.startswith('第8篇') or content.startswith('第9篇') or \
            content.startswith('第10篇'))
    return panduan
def modifyPara(i,content0,content1):
    panduan=False
    if i==0:
        content0x=content0.replace(']','').replace('[','').replace('（','').replace('）','').replace('〔','').replace('〕','').replace('范文','').replace('范本','')
        content1x=content1.replace(']','').replace('[','').replace('（','').replace('）','').replace('〔','').replace('〕','').replace('范文','').replace('范本','')
        panduan=(not panduanHead(content1x) and (((content0x[0:4]==content1x[0:4]) or (content0x[-5:-1]==content1x[-5:-1])) and len(content1x)<50) )
    if content1=='':
        panduan = True

    return not panduan

if __name__ == '__main__':
    file=r'E:\文档\test\hello.docx'
    word = win32.Dispatch("Word.Application")
    word.Visible = 0
    doc=word.Documents.Open(file)
    # try:
    csp= doc.CustomDocumentProperties('Name').value
    print('property is %s' % csp)

    # except:
    #     print('error')

    doc.Saved= False
    doc.Save()
    doc.Close()

    word.Quit()