# -*- coding:utf-8 -*-
import time

import requests

from bs4 import BeautifulSoup


# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def getHtml(thmlpath, strUrl):

        try:
            requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.zw')[0])
            title = soup.select('div.contentlist > h1')[0].text.replace('?', '').replace('？', '').replace('|', '') \
                .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            print(title)
            thmlpath = thmlpath + '\\' + title + '.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('编码出错')
def getListUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.infolist > ul > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='https://www.fanwenzhan.com'+href
        getHtml(thmlpath, newUrl)

def getPageUrl(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        startHref=soup.select('div.pagelist > li > a')[0].get('href')
        lastHref = soup.select('div.pagelist > li > a')[-1].get('href')
        startPageNumber=int(startHref.split('st')[-1].replace('.shtml',''))
        allPageNumber=int(lastHref.split('st')[-1].replace('.shtml',''))
        url='https://www.fanwenzhan.com'+startHref
        hrefUrl=url.replace(startHref.split('st')[-1],'')+'%d.shtml'
        for i in range(startPageNumber,allPageNumber+1):
            newUrl=hrefUrl%i
            print(newUrl)
            getListUrl(thmlpath, newUrl)
    except:
        getListUrl(thmlpath, strUrl)

def getTitleUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.child > dl > dd > li > a')
    for i,a in enumerate(a_list):
            href=a.get('href')
            newUrl='https://www.fanwenzhan.com'+href
            getPageUrl(thmlpath, newUrl)

def main(thmlpath):
    url='https://www.fanwenzhan.com/fanwen/gongzuozongjie/'
    getTitleUrl(thmlpath,url)

if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
