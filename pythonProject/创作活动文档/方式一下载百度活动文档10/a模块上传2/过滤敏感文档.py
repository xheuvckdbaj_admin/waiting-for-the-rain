# -*- coding:utf-8 -*-
import re
import threading
from datetime import time

import docx
import os

from pythonProject.FindFile import find_file, find_name
from pythonProject.初中.敏感词过滤.minganci import filterWord
from pythonProject.文件剪切 import ctrlx

'''
删除广告，删除文件中的空白行，删除修改后的空白文件，删除原来修改前的docx文件
'''





filterTxt='./内容敏感词.txt'
def filterDocx(panduan,filterTxt,content):
    file=open(filterTxt,'r',encoding='utf-8')
    lines=file.readlines()
    for i,line in enumerate(lines):
        key=line.replace('\n','')
        panduan=panduan or (key in content)
    return panduan

def deleAdvertising(filePath, newFolderPath, midFolderPath, newfileNames, lines, front, later):
    doc = filePath.split(".")[len(filePath.split(".")) - 1]
    # word = wc.Dispatch("Word.Application")
    if doc == 'docx':
        try:
            file = docx.Document(filePath)
            contentStr=''
            for i,para in enumerate(file.paragraphs):
                 # 替换关键词，洗稿
                contentStr=contentStr+para.text
            panduan=False
            if filterDocx(panduan, filterTxt, contentStr):
                print('剪切：',filePath)
                ctrlx(filePath, newFolderPath)
        except:
            print('error')

# word.Quit()
def shear_dile(oldFolderPath,newFolderPath,midFolderPath,newfileNames,lines):
    if os.path.isdir(oldFolderPath):
        if not os.listdir(oldFolderPath):
            try:
                os.rmdir(oldFolderPath)
                print(u'移除空目录: ' + oldFolderPath)
            except:
                print('error')
        else:
            for i,d in enumerate(os.listdir(oldFolderPath)):
                    shear_dile(os.path.join(oldFolderPath, d),newFolderPath, midFolderPath, newfileNames, lines)
    if os.path.isfile(oldFolderPath):
        if '~$' in oldFolderPath:
            try:
                os.remove(oldFolderPath)
            except:
                print('error')
        if oldFolderPath.endswith('.docx'):
            print(oldFolderPath)
            deleAdvertising(oldFolderPath, newFolderPath,midFolderPath, newfileNames, lines, 0, 0)

def modifyContent(oldFolderPath, newFolderPath,midFolderPath):
    newfileNames = find_name(newFolderPath, [])
    paths = r'./doc_replace.txt'
    fileTxt = open(paths, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    shear_dile(oldFolderPath,newFolderPath,midFolderPath, newfileNames, lines)

def mainGuolv():
    # os.remove(r'F:\文件上传\ocx\2019高考13套及解析无水印无logo.docx')
    oldFolderPath = r'D:\文档\百度上传文档（精品）'
    newFolderPath = r'D:\文档\敏感文档'
    midFolderPath = r'E:\文档\自查报告网url'
    modifyContent(oldFolderPath, newFolderPath,midFolderPath)


if __name__ == '__main__':
    mainGuolv()
'''
def main():
    deleAdvertising('C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\新建 DOC 文档.docx','C:\\Users\\Administrator\\Desktop\\新建文件夹 (2)\\hello\\')
if __name__ == '__main__':
    main()
'''
