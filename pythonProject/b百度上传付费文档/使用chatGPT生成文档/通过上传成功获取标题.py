import os
import re
import time

from pythonProject.FindFile import find_file


def panduanTitle(folderpath, title):
    panduan = True
    file_line = open(folderpath, 'r', encoding="utf-8")
    readlines = file_line.readlines()
    for line in readlines:
        keyStr = line.replace('\n', '')
        if keyStr in title:
            panduan = False
            break
    return panduan


def mergeGuolv(folderpath, path,sensitivePath):
    txtpaths=find_file(folderpath)
    newTxtPath = path
    paths = []
    try:
        newfile_line = open(newTxtPath, 'w', encoding="utf-8")
        for txtpath in txtpaths:
            fileName=os.path.basename(txtpath)
            print(fileName)
        for txtpath in txtpaths:
            line = os.path.basename(txtpath).replace('.docx','')
            if not line in paths:
                if panduanTitle(sensitivePath, line):
                    keyStr = re.sub(r'[0-9]+字', '', line)
                    keyStr = re.sub(r'\(.*?\)', '', keyStr)
                    keyStr = re.sub(r'[0-9]+篇', '', keyStr)
                    keyStr = re.sub(r'【.*?】', '', keyStr)
                    keyStr = keyStr.replace('二篇', '').replace('三篇', '').replace('四篇', '').replace('五篇','').replace(
                        '六篇', '').replace('七篇', '') \
                        .replace('八篇', '').replace('九篇', '').replace('十篇', '').replace('十一篇', '').replace(
                        '十二篇', '').replace('十三篇', '')
                    keyStr = re.sub(r'[0-9]+年', '', keyStr)
                    keyStr = re.sub(r'[0-9]+', '', keyStr)
                    print(keyStr)
                    paths.append(keyStr.replace('2020', '2023').replace('2021', '2023').replace('2022', '2023')+'\n')
        newfile_line.close()
    except:
        print('')
        # # try:
        #     file_line = open(folderpath, 'r', encoding="gbk")
        #     newfile_line = open(newTxtPath, 'r', encoding="gbk")
        #     readlines = file_line.readlines()
        #     for line in readlines:
        #         if not line in paths:
        #             if panduanTitle(folderpath, line):
        #                 keyStr = re.sub(r'[0-9]+字', '', line)
        #                 keyStr = re.sub(r'\(.*?\)', '', keyStr)
        #                 keyStr = re.sub(r'[0-9]+篇', '', keyStr)
        #                 keyStr = re.sub(r'【.*?】', '', keyStr)
        #                 keyStr = keyStr.replace('二篇', '').replace('三篇', '').replace('四篇', '').replace('五篇','').replace(
        #                     '六篇', '').replace('七篇', '') \
        #                     .replace('八篇', '').replace('九篇', '').replace('十篇', '').replace('十一篇', '').replace(
        #                     '十二篇', '').replace('十三篇', '')
        #                 keyStr = re.sub(r'[0-9]+年', '', keyStr)
        #                 keyStr = re.sub(r'[0-9]+', '', keyStr)
        #                 paths.append(keyStr.replace('2020', '2023').replace('2021', '2023').replace('2022', '2023'))
        #     newfile_line.close()
        # except:
        #     print('gbk读不出')
    time.sleep(2)
    txtfile_line = open(newTxtPath, 'w', encoding='utf-8')
    for ph in paths:
        try:
            txtfile_line.write(ph)
        except:
            print('error')
            continue
    txtfile_line.close()


def main():
    keyPath = r'D:\文档\百度付费上传文档\百度上传成功'
    path = r'D:\文档\百度付费上传文档\标题all.txt'
    sensitivePath=r'./titleKey.txt'
    mergeGuolv(keyPath, path,sensitivePath)


if __name__ == '__main__':
    main()
