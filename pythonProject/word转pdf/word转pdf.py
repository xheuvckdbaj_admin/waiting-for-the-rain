import os

import comtypes.client

def get_path(path):
    filename_list=os.listdir(path)
    wordname_list=[filename for filename in filename_list if filename.endswith(('.doc','.docx'))]
    for wordname in wordname_list:
        pdfname=os.path.splitext(wordname)[0]+'.pdf'
        if pdfname in filename_list:
            continue
        wordpath=os.path.join(path,wordname)
        pdfpath=os.path.join(path,pdfname)
        yield wordpath,pdfpath
def mainWordZhuanPdf(path):
    word=comtypes.client.CreateObject("Word.Application")
    word.Visiable=0
    for w,p in get_path(path):
        try:
            print(w)
            newpdf=word.Documents.Open(w)
            newpdf.SaveAS(p,FileFormat=17)
            newpdf.Close()
            os.remove(w)
        except:
            print(w+'----文件转化pdf失败')
if __name__ == '__main__':
    path = r'D:\文档\百度活动文档\修改后百度活动文档'
    mainWordZhuanPdf(path)