# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"D:\pycharm\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\来搜一下\cookies.txt"

def getElement(browser,hrefs,file_handle):
    for e in range(0, len(hrefs)):
        try:
            browser.get(hrefs[e])
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="down"]'))
            )

            newhref=browser.find_element_by_css_selector('div.down > a').get_attribute('href')
            print(newhref)
            file_handle.write(newhref+'\n')
        except:
            print('资源不存在')
            continue



def getLinUrl(law,url,key,file_handle):

            browser = webdriver.Chrome()
            newUrl=url%key
            browser.get(newUrl)

            element = WebDriverWait(browser, 100).until(
                    EC.presence_of_element_located((By.XPATH, '//div[@class="file-item"]'))
                )

            eles=browser.find_elements_by_xpath('//div[@class="file-item"]/div[@class="title"]/h2/a[@class="object-link fpm"]')
            hrefs=[]
            for ele in eles:
                href=ele.get_attribute('href')
                hrefs.append(href)
            print(hrefs)
            getElement(browser, hrefs, file_handle)
    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,key,path):
    url='https://www.sosuopan.cn/search?q=%s'
    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')
    getLinUrl(law, url, key, file_handle)
    file_handle.close()


def main():
    keys=['全册教案'
          ,'成人教育','教师资格','公务员','安全员','计算机','公共基础','造价工程师'
          ,'第一课','疫情','电大','企业实践报告','护师专业试题','讲卫生教案']
    for key in keys:
            # key='中考'
            print(key)
            path=r'E:\文档\搜索盘url'
            law=1
            login(law,key,path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
