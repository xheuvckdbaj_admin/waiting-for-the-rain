# -*- coding:utf-8 -*-
import os
'''
找出文件夹所有路径
'''
def findAllFile(base):
    for root, ds, fs in os.walk(base):
        for f in fs:
            fullname = os.path.join(root, f)
            yield fullname
def find_file(path,file_list=[]):
    dir = os.listdir(path)
    for i in dir:
        i = os.path.join(path, i)
        if os.path.isdir(i):
            file_list=find_file(i)
        else:
                file_list.append(i)
    return file_list
def find_name(path,file_list=[]):
    dir = os.listdir(path)
    for i in dir:
        #i = os.path.join(path, i)
        if os.path.isdir(i):
            find_name(i)
        else:
                file_list.append(i)
    return file_list
def main():
    oldFolderPath='C:\\Users\\Administrator\\Desktop\\hello'
    filePaths = findAllFile(oldFolderPath)
    for filePath in filePaths:
        print(filePath)

if __name__ == '__main__':
    main()
