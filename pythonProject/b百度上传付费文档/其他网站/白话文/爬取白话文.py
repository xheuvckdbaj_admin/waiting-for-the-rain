import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def mian_baihuawen(url, htmlPath, keyStr, title):
    try:
        try:
            requests_text = str(requests.get(url=url, headers=headers,timeout=10.0).content.decode(encoding='utf-8'))
        except:
            requests_text = str(requests.get(url=url, headers=headers,timeout=5.0).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        title = soup.select('div.article > h1')[0].text.replace('\r','')
        list_soupa = str(soup.select('div.content')[0]).split('<div class="dow_box">')[0]
        list_soupa=list_soupa.replace(title, title)
        thmlpath = htmlPath + '\\' + title + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('白话文下载成功----', title)
    except:
        print('error')

if __name__ == '__main__':
    url = 'https://www.baihuawen.cn/yuedu/gushi/75983.html'
    htmlPath = r'D:\文档\百度付费上传文档\百度html'
    keyStr = 'kaixin'
    title = ''
    mian_baihuawen(url, htmlPath, keyStr, title)
