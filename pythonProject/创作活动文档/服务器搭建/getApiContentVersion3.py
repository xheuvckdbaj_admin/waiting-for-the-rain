import openai

def getApiContentByTitleAndKey3(api_key,prompt):
    openai.api_key = api_key
    model_engine = "text-davinci-003"
    # answer_language='zh-CN'
    try:
        completions = openai.Completion.create(
            engine=model_engine,
            prompt=prompt,
            max_tokens=1024,
            n=1,
            stop=None,
            temperature=0.9,
            top_p=1,
            # language=answer_language,
        )

        message = completions.choices[0].text
        return message
    except Exception as e:
        return str(e)
def main():
    api_key = "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"
    prompt = "三年级语文第七课生字组词"
    apiContent=getApiContentByTitleAndKey3(api_key,prompt)
    print(apiContent)
if __name__ == '__main__':
    main()