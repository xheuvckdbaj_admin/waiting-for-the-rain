def panduanTitleUpPass(title, passPath):
    panduan = True
    file = open(passPath, 'r', encoding='utf-8')
    file_lines = file.readlines()
    for line in file_lines:
        key = line.replace('\n', '')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan
if __name__ == '__main__':
    title='京剧的演讲稿'
    passPath=r'D:\文档\百度上传过的标题\titlePass.txt'
    print(panduanTitleUpPass(title, passPath))