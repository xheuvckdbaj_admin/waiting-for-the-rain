# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="rd_previous"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.cnrencai.com/qiuzhixin/qiuzhixinfanwen/1856713.html',
                'https://www.cnrencai.com/qiuzhixin/qiuzhixinmoban/1856350.html',
                'https://www.cnrencai.com/qiuzhixin/qiuzhixinfengmian/742045.html',
                'https://www.cnrencai.com/bishi/timu/1166321.html',
                'https://www.cnrencai.com/bishi/jingyan/1047052.html',
                'https://www.cnrencai.com/mianshishiti/1300429.html',
                'https://www.cnrencai.com/mianshiskill/1841226.html',
                'https://www.cnrencai.com/mianshi/mianshiziwojieshao/1856724.html',
                'https://www.cnrencai.com/mianshi/mianshijiqiao/767827.html',
                'https://www.cnrencai.com/mianshi/mianshiwenti/1841464.html',
                'https://www.cnrencai.com/mianshi/mianshiyingyu/1840140.html',
                'https://www.cnrencai.com/cizhibaogao/1856709.html',
                'https://www.cnrencai.com/cizhixin/1849915.html',
                'https://www.cnrencai.com/cizhi/shenqingshu/1856237.html',
                'https://www.cnrencai.com/ruzhi/ziwojieshao/1856663.html',
                'https://www.cnrencai.com/ruzhi/shenqingshu/1841216.html',
                'https://www.cnrencai.com/shixibaogao/1856720.html',
                'https://www.cnrencai.com/shixizongjie/1855370.html',
                'https://www.cnrencai.com/shiximudi/1842564.html',
                'https://www.cnrencai.com/shixixinde/1856640.html',
                'https://www.cnrencai.com/shixijianding/1855374.html',
                'https://www.cnrencai.com/shixizhengming/1854924.html',
                'https://www.cnrencai.com/shixizhouji/1853688.html',
                'https://www.cnrencai.com/shebao/zhengce/1016952.html',
                'https://www.cnrencai.com/shebao/gongjijin/1827124.html',
                'https://www.cnrencai.com/shebao/yanglao/1762472.html',
                'https://www.cnrencai.com/shebao/shiye/1680499.html',
                'https://www.cnrencai.com/shebao/shengyu/1808183.html',
                'https://www.cnrencai.com/shebao/shengyu/1808183.html',
                'https://www.cnrencai.com/shebao/shengyu/1808183.html',
                'https://www.cnrencai.com/shebao/yiliao/1810603.html',
                'https://www.cnrencai.com/shebao/gongshang/1739889.html',
                'https://www.cnrencai.com/law/1855058.html',
                'https://www.cnrencai.com/salarinfo/1167043.html',
                'https://www.cnrencai.com/zhichangzixun/1792842.html',
                'https://www.cnrencai.com/qiuzhigushi/1382191.html',
                'https://www.cnrencai.com/train/1038902.html',
                'https://www.cnrencai.com/qiuzhiziliao/93846.html',
                'https://www.cnrencai.com/qiuzhi/1728817.html',
                'https://www.cnrencai.com/employment/1084688.html',
                'https://www.cnrencai.com/easy/1166761.html',
                'https://www.cnrencai.com/zhiyeguihua/1807157.html',
                'https://www.cnrencai.com/hr/1013273.html',
                'https://www.cnrencai.com/ceping/ability/1093598.html',
                'https://www.cnrencai.com/ceping/knowledge/985459.html',
                'https://www.cnrencai.com/ceping/nature/985457.html',
                'https://www.cnrencai.com/ceping/zonghe/1835150.html',
                'https://www.cnrencai.com/goldjob/guide/1773878.html',
                'https://www.cnrencai.com/goldjob/jihua/1854680.html',
                'https://www.cnrencai.com/goldjob/manager/1127691.html',
                'https://www.cnrencai.com/goldjob/money/1825454.html',
                'https://www.cnrencai.com/goldjob/saleskill/1780793.html',
                'https://www.cnrencai.com/goldjob/story/1836397.html',
                'https://www.cnrencai.com/goldjob/xiangmu/1855410.html',
                'https://www.cnrencai.com/nianzhongzongjie/1855731.html',
                'https://www.cnrencai.com/zongjie/jszj/1855713.html',
                'https://www.cnrencai.com/zongjie/pingyou/1353640.html',
                'https://www.cnrencai.com/zongjie/nianzhong/1841632.html',
                'https://www.cnrencai.com/zongjie/sixiang/1852596.html',
                'https://www.cnrencai.com/zongjie/huiyi/1769548.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
