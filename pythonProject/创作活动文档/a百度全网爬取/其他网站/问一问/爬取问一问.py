# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import re
import time
from telnetlib import EC

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}

def mian_question(url,htmlPath,keyStr,title):
    try:
        driver = getDriver('meipian')
        driver.get(url)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH,
                                                '//h1[@accuse="qTitle"]'))
            )
        except:
            time.sleep(0.1)

        title = driver.find_element(By.XPATH, '//h1[@accuse="qTitle"]/span').text
        title=re.sub('（(.*)）', '', title)
        title = re.sub('\d+篇', '', title)
        print(title)
        list_soupa = driver.find_element(By.XPATH, '//div[@class="abstract-content-text"]/span[@class="abstract-text"]').get_attribute('innerHTML').replace(title, keyStr)
        thmlpath = htmlPath + '\\' + keyStr + '.html'
        f = open(thmlpath, 'w', encoding='utf-8')
        f.write(list_soupa)
        print('百度知道下载成功----',keyStr)
        driver.quit()
    except:
        print('error')
    # try:
    #     try:
    #         requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    #     except:
    #         requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    #     soup = BeautifulSoup(requests_text, 'lxml')
    #     title=soup.select('div.wgt-ask > h1')[0].text
    #     list_soupa=str(soup.select('div.abstract-content-text')[0]).replace('摘要','').replace(title,keyStr)
    #     thmlpath = htmlPath + '\\' + keyStr + ".html"
    #     f = open(thmlpath, 'w', encoding='utf-8')
    #     f.write(list_soupa)
    #     print('问一问下载成功----',keyStr)
    # except:
    #     print('error')

if __name__ == '__main__':
    url = 'https://wen.baidu.com/question/272332475969337965.html'
    htmlPath=r'D:\文档\百度活动文档\百度html\第一范文网'
    keyStr='kaixin'
    title=''
    mian_question(url,htmlPath,keyStr,title)