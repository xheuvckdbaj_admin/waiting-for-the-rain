# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'https://www.hunanhr.cn/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    print(title)
    list_soupa = str(soup.select('.zhengwen > .content')[0])
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(list_soupa)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    ul_list=soup.select('.listlie > .lie1')
    #print(url_list)
    for u in ul_list:
            href=u.select('a')[0].get('href')
            print(href)
            title = u.select('a')[0].text.replace('?', '').replace('？', '').replace('|', '') \
            .replace('”', '').replace('*', '').replace('/', '').replace('\\', '')
            getContent(thmlpath,href,title)

def main(thmlpath):
    str = ['gongzuozongjie','gongzuojihua','duhougan','fayangao','xindetihui','shenqingshudaquan','sixianghuibao',
           'shuzhibaogao','jiaoxuesheji','zhuantifanwen','yanjianggao','jiaoshifanwen',''
           ]
    for s in str:
        url = groupUrl(s)
        getUrl(thmlpath,url)

def main1(thmlpath):
    url='https://www.hunanhr.cn/xuexiqiangguo/'
    requests_text=str(requests.get(url=url,headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.guide-info > span.redcolor > li > a')
    for a in a_list:
        href=a.get('href')
        getUrl(thmlpath,href)
def main2(thmlpath):
    url='https://www.hunanhr.cn/renshiwendang/'
    requests_text=str(requests.get(url=url,headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.guide-info > span.redcolor > li > a')
    for a in a_list:
        href=a.get('href')
        getUrl(thmlpath,href)
if __name__ == '__main__':
    #范文网
    thmlpath = r'D:\文档\爬取网站\职能范文网'
    main(thmlpath)
    main1(thmlpath)
    main2(thmlpath)
