# -*- coding:utf-8 -*-
import threading

import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver

# 获取真实预览地址turl
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.方式一下载百度活动文档.必应搜索.网站选择 import websiteJudeBiYing
from pythonProject.创作活动文档.方式一下载百度活动文档.标题匹配算法.标题匹配 import mainPanDuanTitle

from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver2, getDriver2

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


def compareTitle(title, content):
    panduan = True
    if "《" in title and "》" in title:
        title = re.search('《(.*?)》', title)[0]
    if title in content:
        panduan = True
    return panduan


def getFirstUrl(fileTest, folderPath, keyStr, urls, driver):
    try:
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//input'))
            )
        except:
            time.sleep(1)
        driver.find_element(By.XPATH, '//input[@class="sb_form_q"]').clear()
        try:
            driver.find_element(By.XPATH, '//input[@class="sb_form_q"]').send_keys(keyStr)
        except:
            driver.find_element(By.XPATH, '//input[@class="sb_form_q"]').send_keys(keyStr)
        time.sleep(0.2)
        driver.find_element(By.XPATH, '//input[@class="sb_form_q"]').send_keys(Keys.ENTER)
        time.sleep(0.2)
        n = driver.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        driver.switch_to.window(n[-1])
        time.sleep(0.1)
        try:
            element = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="b_searchboxForm"]'))
            )
        except:
            time.sleep(0.1)
        # baidu=driver.find_element(By.XPATH,'//div[@class="b_attribution"]/cite')
        elements = driver.find_elements(By.XPATH, '//div[@class="b_title"]/h2/a')
        for i, element in enumerate(elements):
            title = element.text
            if mainPanDuanTitle(keyStr, title):
                fileTest.write(keyStr + '++++++++' + title + '\n')
                print(keyStr + '++++++++' + title)
                # baidu=driver.find_element(By.XPATH,'//span[@class="c-color-gray"]')
                panduan = websiteJudeBiYing(element, folderPath, driver, keyStr, urls)
                if panduan:
                    break
    except:
        time.sleep(2)
        print('driver错误')
        strUrl = 'https://cn.bing.com/?scope=web&FORM=BEHPTB&ensearch=1'
        driver.get(strUrl)


def makeFolder(floderPath, fileName):
    print(fileName)
    floderPath = floderPath + '\\' + fileName
    os.makedirs(floderPath)
    return floderPath


def main(fileTest, folderPath, urls, txtPath, number, last):
    driver = getDriver2('b')
    file = open(txtPath, 'r', encoding='utf-8')
    lines = file.readlines()
    for i, line in enumerate(lines):
        try:
            if i >= number and i < last:
                try:
                    print(str(i) + '----', line)
                    strUrl = 'https://cn.bing.com/?scope=web&FORM=BEHPTB&ensearch=1'
                    driver.get(strUrl)
                except:
                    driver = getDriver2('b')
                    strUrl = 'https://cn.bing.com/?scope=web&FORM=BEHPTB&ensearch=1'
                    driver.get(strUrl)
                keyStr = line.replace('\n', '')
                print(keyStr)
                getFirstUrl(fileTest, folderPath, keyStr, urls, driver)
        except:
            print('打开浏览器出错')
    driver.quit()


def mianBiYing(fileTest, folderPath, wangzhiPath, titleFilePath, number, last):
    urls = []
    file = open(wangzhiPath, 'w', encoding='utf-8')
    txtFilePath = titleFilePath
    # fileTxt = open(txtPath, 'r', encoding='utf-8')
    # lines = fileTxt.readlines()
    # last = len(lines)
    # print(last)
    # an=int((last-number)/5)
    # t1=threading.Thread(target=main, args=(fileTest,folderPath,urls, txtFilePath,number,number+an))
    # t2=threading.Thread(target=main, args=(fileTest, folderPath, urls, txtFilePath, number+an, number + an*2))
    # t3=threading.Thread(target=main, args=(fileTest, folderPath, urls, txtFilePath, number + an*2, number + an*3))
    # t4=threading.Thread(target=main, args=(fileTest, folderPath, urls, txtFilePath, number + an*3, number + an*4))
    # t5=threading.Thread(target=main, args=(fileTest, folderPath, urls, txtFilePath, number + an*4, last))
    # t1.start()
    # t2.start()
    # t3.start()
    # t4.start()
    # t5.start()
    main(fileTest, folderPath, urls, txtFilePath, number, last)
    for i, url in enumerate(urls):
        file.write(url + '\n')
    file.close()


if __name__ == '__main__':
    # 标题存放文件夹
    txtPath = r'D:\文档\百度活动文档\百度活动文档标题'
    folderPath = r'D:\文档\百度活动文档\百度html\其他网站'
    wangzhiPath = r'./wangzhi.txt'
    titleFilePath = txtPath + '\\' + 'all' + '.txt'
    fileTest = open(folderPath + '\\' + 'biying' + '.txt', 'w', encoding='utf-8')
    number = 0
    last = 100000
    fileTxt = open(titleFilePath, 'r', encoding='utf-8')
    lines = fileTxt.readlines()
    last = len(lines)
    print(last)
    an = int((last - number) / 6)
    t1 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, number, number + an))
    t2 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, number + an, number + an * 2))
    t3 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, number + an * 2, number + an * 3))
    t4 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, number + an * 3, number + an * 4))
    t5 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, number + an * 5, number + an * 6))
    t6 = threading.Thread(target=mianBiYing,args=(fileTest, folderPath, wangzhiPath, titleFilePath, number + an * 6, last))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()
    mianBiYing(fileTest, folderPath, wangzhiPath, titleFilePath, number, last)
