# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time
import os
import sys
import pypandoc
from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
from lxml import etree
from lxml.etree import HTMLParser

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.qinzibuy.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    etree1=etree.HTML(requests_text)
    title=etree1.xpath('//div[@class="g-gx-detail f-fl"]/div[@class="g-cont-detail g-main-bg"]/h1/text()')[0]
    print(title)
    soup=BeautifulSoup(requests_text,'lxml')
    p=str(soup.select('.m_qmview')[0])
    #list_soupa=etree1.xpath('//div[@id="newsnr"]/p')
    ##xml=HTMLParser().unescape(content2)[19:-8]
    print(p)


    #soup = BeautifulSoup(requests_text, 'lxml')
    #title = soup.select('title')[0].text
    #print(title)
    #list_soupa = str(soup.select('.content')[0]).split('<script>')[0]
    #print(list_soupa)
    #print('python' + '\r' + 'java' + '\n' + 'java')
    thmlpath=thmlpath+'\\'+title+'.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(p)

# 获取最终网址
def getUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    url_list=soup.select('.g-gxlist-article > li')
    #print(url_list)
    for u in url_list:
        for i in u.select('a'):
            href=i.get('href')
            newUrl=f'http://www.qinzibuy.com'+href
            #Qprint(newUrl)
            getContent(thmlpath,newUrl)




def getHtml(thmlpath, strUrl, s):
    for strName in strUrl:
        try:
            url = groupUrl(s)
            url = url + strName + '/'
            for pageNum in range(1,200):
                if pageNum==1:
                    newurl=url+'index.html'
                    #print(newurl)
                    getUrl(thmlpath, newurl)
                else:
                    newurl=url+'index_%d.html'
                    maturl=format(newurl%pageNum)
                    #print(maturl)
                    getUrl(thmlpath, maturl)
        except:
            continue


def main(thmlpath):
    str = ['daban','zhongban','xiaoban','tuoban']
    for s in str:
            strUrl = ['kexue','shuxue','zhuti','yinyue','anquan','yuyan','meishu','shehui','jiankang','youxi','yishu','tiyu']
            getHtml(thmlpath, strUrl, s)




if __name__ == '__main__':
    thmlpath = r'E:\oc文件'
    main(thmlpath)
