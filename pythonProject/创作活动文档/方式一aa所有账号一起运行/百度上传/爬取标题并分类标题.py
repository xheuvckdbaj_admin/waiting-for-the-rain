# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import requests
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.FindFile import find_file, find_name
from pythonProject.FindFile import find_file
from pythonProject.b百度上传付费文档.b整理文档.allyunxing import mainzhengli
from pythonProject.b百度上传付费文档.百度上传.readTxt import readTxt
from pythonProject.b百度上传付费文档.百度上传.百度安全验证 import anquanyanzheng
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档检查账号是否传满 import \
    mainTitleClassifyUploadApiGuish
from pythonProject.创作活动文档.playwright创作活动文档.删除单个账号已被传过的文档 import mainTitleClassifyUploadDelete
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver

from pywinauto import Desktop
from pywinauto.keyboard import *


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser

guolvPath=r'./过滤词.txt'
def panduanTitle(title):
    panduan = True
    file=open(guolvPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for line in file_lines:
        key=line.replace('\n','')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getNumberPager(page):
    # try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(page.locator(xpathStr).nth(-1).text_content().strip())
        return pagerNumber
    # except:
    #     return 0


def deleteWord(folderPath, title):
    filePath = folderPath + '\\' + title + '.docx'
    try:
        os.remove(filePath)
    except:
        print('无法删除' + folderPath)


def ctrlx(oldFilePath, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
    shutil.move(oldFilePath, newPath)


def getFileNameStr(folderPath):
    keyStrs = []
    filePaths = find_file(folderPath, [])
    if len(filePaths) >= 1:
        for filePath in filePaths:
            titleStr = os.path.basename(filePath).replace('.docx', '')
            keyStrs.append(titleStr)
    return keyStrs









def on_request(request):
    htmlPath = r'D:\图片'
    makeFolder(htmlPath)
    title = 'haha'
    print(f'Request URL: {request.url}')
    print(f'Request Headers: {request.headers}')
    if request.post_data is not None:
        print(f'Request Body: {request.post_data}')
    if 'https://passport.baidu.com/cap/img?ak=' in request.url:
        requests_text = requests.get(url=request.url, headers=request.headers, timeout=5.0).content
        thmlpath = htmlPath + '\\' + title + '.jpg'
        f = open(thmlpath, 'wb')
        f.write(requests_text)
        print('图片下载成功----', title)

def ctrlx(oldFilePath,newPath):
    #if oldFilePath.endswith('html'):
        #移动文件夹示例
        shutil.move(oldFilePath,newPath)
def submitword(page, file_chooser, fileDocxPath,successFloder):
    file_chooser.set_files(fileDocxPath)
    anquanyanzheng(page)
    page.wait_for_timeout(5000)
    page.get_by_role("radio", name="VIP文档").click()
    time.sleep(1)
    page.get_by_role('button', name='确认提交').click()
    print('上传成功：' + fileDocxPath)

    try:
        ctrlx(fileDocxPath,successFloder)
    except:
        print('剪切不掉' + fileDocxPath)
        os.remove(fileDocxPath)


def deleteFile(wordFolderPath):
    filePaths = find_file(wordFolderPath)
    for filepath in filePaths:
        try:
            os.remove(filepath)
        except:
            print('删除失败', filepath)


def singlyUpload(page, titleFolderPath, cookiesPath,successFloder):
    xpathStrButton = '//div[@class="upload-doc btn-posution"]/div[@class="add-new-btn add-new-btn-right"]/button'
    xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button'
    try:
        page.locator(xpathStrButton).nth(0).first.click()
        with page.expect_file_chooser() as fc_info:
            page.locator(xpathStrButton).nth(0).first.click()
        panduanUpload = True
        file_chooser = fc_info.value
        fileDocxPath = titleFolderPath
        submitword(page, file_chooser, fileDocxPath,successFloder)
    except:
        print('上传失败')
        try:
            os.remove(fileDocxPath)
        except:
            print('上传失败，没有删除文件')





def getNewPage(page, url):
    try:
        page.goto(url)
    except:
        time.sleep(1)
        getNewPage(page, url)


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1709859132124#/commodityManage/documentation")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.get_by_role("button", name="Close").click()
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page

def determine(page):
    page.get_by_text("创作活动", exact=True).click()
    page.wait_for_timeout(1000)
    print(page.get_by_role("button", name="我知道了").count())
    if page.get_by_role("button", name="我知道了").count()==0:
        panduan=False
    else:
        panduan = True
    return panduan
def getAccountName(page):
    nameXpath='//div[@class="top-bar-wrapper"]/div[@class="right-bar"]/div[@class="link"]'
    accountName=page.locator(nameXpath).first.text_content().strip()
    return accountName
def getPassingRate(page):
    rateXpath = '//span[@class="green-txt fs-17"]'
    rate = page.locator(rateXpath).nth(2).text_content().strip()
    return rate
def saveCookiesFile(page,folderNoPassPath,accountName,cookiesPath):
    # 获取通过率
    rate = getPassingRate(page)
    # 保存cookies文件
    makeFolder(folderNoPassPath)
    fileName = folderNoPassPath + '\\' + rate + accountName + '.txt'
    file = open(fileName, 'w', encoding='utf-8')
    cookiesFile = open(cookiesPath, 'r', encoding='utf-8')
    file_lines = cookiesFile.read()
    file.write(file_lines)
    file.close()
    cookiesFile.close()
def panduanTitleUpPass(title,passPath):
    panduan = True
    file=open(passPath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for line in file_lines:
        key=line.replace('\n','')
        if key in title:
            panduan = False
    if (len(title) < 5):
        panduan = False
    return panduan
def getTitle(page, file,fileClassify,passTitlePath):
    fileNames=find_name(passTitlePath)
    xpathStr = '//div[@class="doc-row"]/div[@class="row-content"]/div[@class="title-line"]/span[@class="doc-title"]'
    elements=page.locator(xpathStr)
    for i in range(0,elements.count()):
        try:
            title = elements.nth(i).get_attribute('title')
            # if panduanTitle(title) and (not ' ' in title):
            if  '英语' in title and (not title in fileNames):
                print(title)
                file.write(title + '\n')
                fileClassify.write(title + '\n')
        except:
            print('该标题爬取出错了')
def clickNextPage(page, i):
    try:

        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').click()
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').fill(str(i))
        page.locator('//div[@class="el-input el-pagination__editor is-in-pagination"]/input').press("Enter")
        page.wait_for_timeout(1000)
    except:
        print('error')
def getBigPageNumber(page):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(page.locator(xpathStr).nth(-1).text_content().strip())
    except:
        pagerNumber=0
    return pagerNumber
def clickNextTitle(page, pageNumber):
    page.wait_for_timeout(500)
    if pageNumber >= 0:
        # pageNumber最小为0
        if pageNumber >= 9:
            xpathStrs = '//div[@class="arrow-wrap control"]/button[@class="el-button el-button--default"]'
            page.locator(xpathStrs).click()
            page.wait_for_timeout(1000)
        xpathStr = '//div[@class="privilege-list"]/div[@class="privilege-item-container"]'
        page.locator(xpathStr).nth(pageNumber).click()
        page.wait_for_timeout(1000)
#accounts：不能上传的账号数
#titleFolder 标题存放的文件夹
#classifyFolder 分类标题的文件夹
#passTitlePath 通过的标题路径
#folderNoPassPath 未开通账号cookies保存路径
#cookiesPath cookies路径
def crawingTitle(page,accounts,titleFolder,classifyFolder,passTitlePath,folderNoPassPath,cookiesPath):
    # 获取账号名称
    accountName = getAccountName(page)
    # 判断该账号是否值得上传
    if not accountName in accounts:
        # 判断账号是否开通定产
        if determine(page):
            print('该账号：%s能上传' % accountName)
            page.get_by_role("button", name="我知道了").click()
            page.wait_for_timeout(2000)
            #判断账号是否传满
            classStr=page.get_by_role("button", name="上传").nth(0).get_attribute('class')
            if not 'no-agree' in classStr:
                # 爬取所有模块所有页
                moduleNames = ['推荐', '学前教育', '基础教育', '高校与高等教育', '语言/资格考试', '法律', '建筑', '互联网',
                               '行业资料', '政务民生', '商品说明书', '实用模板', '生活娱乐']
                for titleNumber in range(0, 13):
                    if not titleNumber == 0:
                        clickNextTitle(page, titleNumber - 1)
                    # 爬取单模块所有页
                    # 最大页数
                    pagerNumber = getBigPageNumber(page)
                    for i in range(1, pagerNumber + 1):
                        # 点击下一页
                        clickNextPage(page, i)
                        # 爬取单个模块单页
                        titlePath = titleFolder + '\\' + moduleNames[titleNumber] + '\\' + 'page' + str(i) + '.txt'
                        makeFolder(titleFolder + '\\' + moduleNames[titleNumber])
                        filePathClassify = classifyFolder + '\\' + moduleNames[titleNumber] + '\\' + 'page' + str(
                            i) + '.txt'
                        makeFolder(classifyFolder + '\\' + moduleNames[titleNumber])
                        fileClassify = open(filePathClassify, 'w', encoding='utf-8')
                        file = open(titlePath, 'w', encoding='utf-8')
                        getTitle(page, file, fileClassify, passTitlePath)
                        file.close()
                        fileClassify.close()
            else:
                print('该账号已传满')
        else:
            saveCookiesFile(page, folderNoPassPath, accountName, cookiesPath)
def mainTitleClassifyUpload(playwright: Playwright,cookiesPath,accounts,folderNoPassPath,passTitlePath,titleFolder,classifyFolder) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    page.wait_for_timeout(500)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    time.sleep(1000000)
    crawingTitle(page,accounts,titleFolder,classifyFolder,passTitlePath,folderNoPassPath,cookiesPath)




def mainTitleClassifyUploadApiDing(cookiesPath,accounts,folderNoPassPath,passTitlePath,titleFolder,classifyFolder):
    with sync_playwright() as playwright:
        mainTitleClassifyUpload(playwright,cookiesPath,accounts,folderNoPassPath,passTitlePath,titleFolder,classifyFolder)
    # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


def main():
    # folderPath = r'D:\文档\百度付费上传文档\修改后百度活动文档'
    # # panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    # successFloder=r'D:\文档\百度付费上传文档\百度上传成功'
    folderNoPassPath=r'D:\文档\百度活动文档\未开通账号'
    titleFolder = r'D:\文档\百度活动文档\百度活动文档标题'
    classifyFolder=r'D:\文档\百度活动文档\用来分类文档标题'
    passTitlePath=r'D:\文档\百度活动文档\百度上传成功'
    #黑名单账号
    accounts=[]
    cookiesPath1 = './baiduCookies1.txt'
    cookiesPath2 = './baiduCookies2.txt'
    cookiesPath3 = './baiduCookies3.txt'
    cookiesPath4 = './baiduCookies4.txt'
    cookiesPath5 = './baiduCookies5.txt'
    cookiesPath6 = './baiduCookies6.txt'
    cookiesPath7 = './baiduCookies7.txt'
    cookiesPath8 = './baiduCookies8.txt'
    cookiesPath9 = './baiduCookies9.txt'
    cookiesPath10 = './baiduCookies10.txt'
    cookiesPath11 = './baiduCookies11.txt'
    cookiesPath12 = './baiduCookies12.txt'
    cookiesPath13 = './baiduCookies13.txt'
    cookiesPath14 = './baiduCookies14.txt'
    cookiesPath15 = './baiduCookies15.txt'
    cookiesPath16 = './baiduCookies16.txt'
    cookiesPath17 = './baiduCookies17.txt'
    cookiesPath18 = './baiduCookies18.txt'
    cookiesPath19 = './baiduCookies19.txt'
    cookiesPath20 = './baiduCookies20.txt'
    cookiesPath21 = './baiduCookies21.txt'
    cookiesPath22 = './baiduCookies22.txt'
    cookiesPath23 = './baiduCookies23.txt'
    cookiesPath24 = './baiduCookies24.txt'
    cookiesPath25 = './baiduCookies25.txt'
    print('------------------账号1开始运行')
    mainTitleClassifyUploadApiDing(cookiesPath1,accounts,folderNoPassPath,passTitlePath,titleFolder,classifyFolder)
    # print('------------------账号2开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath2,successFloder)
    # print('------------------账号3开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath3,successFloder)
    # print('------------------账号4开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath4,successFloder)
    # print('------------------账号5开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath5,successFloder)
    # print('------------------账号6开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath6,successFloder)
    # print('------------------账号7开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath7,successFloder)
    # print('------------------账号8开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath8,successFloder)
    # print('------------------账号9开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath9,successFloder)
    # print('------------------账号10开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath10,successFloder)
    # print('------------------账号11开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath11,successFloder)
    # print('------------------账号12开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath12,successFloder)
    # print('------------------账号13开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath13,successFloder)
    # print('------------------账号14开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath14,successFloder)
    # print('------------------账号15开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath15,successFloder)
    # print('------------------账号16开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath16,successFloder)
    # print('------------------账号17开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath17,successFloder)
    # print('------------------账号18开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath18,successFloder)
    # print('------------------账号19开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath19,successFloder)
    # print('------------------账号20开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath20,successFloder)
    # print('------------------账号21开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath21,successFloder)
    # print('------------------账号22开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath22,successFloder)
    # print('------------------账号23开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath23,successFloder)
    # print('------------------账号24开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath24,successFloder)
    # print('------------------账号25开始运行')
    # mainTitleClassifyUploadApi(folderPath, cookiesPath25,successFloder)
    # time.sleep(23200)
if __name__ == '__main__':
    # for i in range(0,1000):
        main()