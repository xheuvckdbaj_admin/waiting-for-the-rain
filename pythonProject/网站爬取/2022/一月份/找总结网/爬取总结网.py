# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('div.con_article.f16.c6')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    contentText=contentText.split('<hr>')[len(contentText.split('<hr>'))-1]
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.ar_con_title.bj_mt30 > h1.f30')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def getpageUrl(file,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.jx_lb.bj_mt20 > a.lb_1_a.chao.fl')
    h_list=soup.select('div.li_r.li_nr > h3 > a.f18.bt_a.nobold.chao')

    for h in h_list:
        href=h.get('href')
        newUrl = 'http://www.zhaozongjie.com' + href
        file.write(newUrl+'\n')
    for a in a_list:
        href=a.get('href')
        newUrl='http://www.zhaozongjie.com'+href
        file.write(newUrl + '\n')

def getUrl(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.con_a > a.f14')
    file = open(thmlpath, 'w')
    for i,a in enumerate(a_list):
        href=a.get('href')
        print(href)
        newUrl='http://www.zhaozongjie.com'+href
        getpageUrl(file,newUrl)
    file.close()
def getUrl2(thmlpath,strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.fr.tow_lm > a.more')
    file = open(thmlpath, 'w')
    for i,a in enumerate(a_list):
        href=a.get('href')
        print(href)
        newUrl='http://www.zhaozongjie.com'+href
        getpageUrl(file,newUrl)
    file.close()
def getHtmlUrl(number,file2, strUrl):
    try:
        number+=1
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        href = soup.select('div.con_prev.clear > p.fr.chao.f16 > a')[0].get('href')
        print(href)
        newUrl = 'http://www.zhaozongjie.com' + href
        file2.write(newUrl+'\n')
        if number < 2000:
            getHtmlUrl(number,file2, newUrl)
    except:
        print('error')


def main(thmlpath,thmlpath2):
            #打开文本图片集
            # strUrl='http://www.zhaozongjie.com/tags.html'
            # strUrl2='http://www.zhaozongjie.com/jiaoxue/'
            # getUrl2(thmlpath,strUrl2)
            file=open(thmlpath,'r')
            lines=file.readlines()
            for i,line in enumerate(lines):
                if i > 4478:
                    print('------',i)
                    newPath=thmlpath2+'\\'+str(time.time())+'.txt'
                    file2=open(newPath,'w')
                    file2.write(line)
                    url=line.replace('\n','')
                    number=0
                    getHtmlUrl(number,file2, url)
                    file2.close()


def mainx(txtPath, filePath):
    # 打开文本图片集
    # strUrl='http://www.zhaozongjie.com/tags.html'
    # strUrl2='http://www.zhaozongjie.com/jiaoxue/'
    # getUrl2(thmlpath,strUrl2)
    file = open(txtPath, 'r')
    lines = file.readlines()
    for i, line in enumerate(lines):
        print('------', i)
        url = line.replace('\n', '')
        getHtml(filePath, url)
if __name__ == '__main__':
    thmlpath = r'./lianjie.txt'
    thmlpath2 = r'D:\文档\爬取网站\找总结网\url'
    main(thmlpath,thmlpath2)
    # txtPath=r'./lianjie.txt'
    # filePath=r'E:\文档\自查报告网'
    # mainx(txtPath, filePath)
