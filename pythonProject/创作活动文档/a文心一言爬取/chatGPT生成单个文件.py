import os
import threading
import time

import openai
from docx import Document
from docx.enum.base import XmlMappedEnumMember
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.opc.oxml import qn
from docx.shared import Pt, RGBColor, Inches, Mm
from docx.oxml.ns import qn

from pythonProject.创作活动文档.chatGPT自动生成文档.getApiContentVersion3 import getApiContentByTitleAndKey3
from pythonProject.创作活动文档.chatGPT自动生成文档.getApiContentVersion4 import getApiContentByTitleAndKey
from pythonProject.创作活动文档.chatGPT自动生成文档.getApiContentVersion5 import getApiContentByTitleAndKey0301
from pythonProject.创作活动文档.chatGPT自动生成文档.调用chatGPT3 import getChatGPTConet3
from pythonProject.创作活动文档.chatGPT自动生成文档.调用chatGPT4 import getChatGPTConet4

# from pythonProject.创作活动文档.chatGPT自动生成文档.getApiContentVersion3 import getApiContentByTitleAndKey3
# from pythonProject.创作活动文档.chatGPT自动生成文档.getApiContentVersion4 import getApiContentByTitleAndKey


def sezhiParas(doc, content):
    # 段落正文
    para = doc.add_paragraph('')
    run = para.add_run(content)
    run.font.name = u'宋体'
    run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
    run.font.size = Pt(14)
    para.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
    para.paragraph_format.first_line_indent = Inches(0.4)
    # 段前间距
    para.paragraph_format.space_before = Pt(5)
    # 段后间距
    para.paragraph_format.space_after = Pt(10)
    para.paragraph_format.line_spacing = 1.5  # 1.5倍行距 单倍行距 1.0


def sezhiTtitle(doc, content):
    if not '年' in content:
        run = doc.add_heading('', 0).add_run(content)  # 应用场景示例标题
        run.font.name = u'宋体'
        run.font.element.rPr.rFonts.set(qn('w:eastAsia'), u'宋体')
        run.font.size = Pt(18)
        # run.font.color.rgb = RGBColor(250,000,000)
        doc.paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER


def sezhiPaper(doc):
    section = doc.sections[0]
    section.page_height = Mm(297)
    section.page_width = Mm(210)
    section.left_margin = Mm(25.4)
    section.right_margin = Mm(25.4)
    section.top_margin = Mm(25.4)
    section.bottom_margin = Mm(25.4)
    section.header_distance = Mm(12.7)
    section.footer_distance = Mm(12.7)


def markjudge(contentStr):
    if (contentStr.startswith('。') or contentStr.startswith('，') or contentStr.startswith('：')
            or contentStr.startswith('！') or contentStr.startswith('？') or contentStr.startswith('、')
            or contentStr.startswith('；') or contentStr.startswith(')')):
        contentStr = contentStr[1:]
    if contentStr.endswith('，') or contentStr.endswith('、') or contentStr.endswith('(') or contentStr.endswith('{') \
            or contentStr.endswith('[') or contentStr.endswith('·') or contentStr.endswith('——'):
        contentStr = contentStr[0:-1]
    if (not (contentStr.endswith('.') or contentStr.endswith('。') or contentStr.endswith('！')
             or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
             or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
             or contentStr.endswith('：') or contentStr.endswith('；') or contentStr.endswith('：'))):
        contentStr = contentStr
    return contentStr


def deletesingle(contentStr):
    if len(contentStr) == 1:
        contentStr = ''
    return contentStr


paghraDownPath = './删除活动文档段落违禁词.txt'


def deletePragram(paghraDownPath, content):
    file = open(paghraDownPath, 'r', encoding='utf-8')
    lines = file.readlines()
    panduan = False
    for i, line in enumerate(lines):
        word = line.replace('\n', '')
        panduan = panduan or (word in content and
                              not (content.startswith('1.') or content.startswith('1、')))
    if panduan:
        content = ''
    return content


def merrageLastPrage(i, file_lines, contentStr):
    if i == (len(file_lines) - 1):
        if (not (contentStr.endswith('.') or contentStr.endswith('。') or contentStr.endswith('！')
                 or contentStr.endswith('!') or contentStr.endswith('?') or contentStr.endswith('？')
                 or contentStr.endswith('……') or contentStr.endswith('......') or contentStr.endswith('”')
                 or contentStr.endswith('；') or contentStr.endswith(';') or contentStr.endswith('；')
                 or contentStr.startswith('（')
                 or contentStr.startswith('2'))):
            contentStr = ''
    if contentStr.endswith('1.') or contentStr.endswith('2.') or contentStr.endswith('3.') \
            or contentStr.endswith('4.') or contentStr.endswith('5.') or contentStr.endswith('6.')\
            or contentStr.endswith('7.') or contentStr.endswith('8.') or contentStr.endswith('9.') \
            or contentStr.endswith('0.'):
        contentStr = ''
    return contentStr


def merrageLastPrageContentStr(i, file_lines, contentStr):
    if i == (len(file_lines) - 1):
        if contentStr.endswith('：'):
            contentStr = ''
    return contentStr


def makeWordChatGPT(folderPath, title, content):
    try:
        title = title
        filePathTxt = folderPath + '\\' + title + '.txt'
        filePathDocx = folderPath + '\\' + title + '.docx'
        doc = Document()
        core_properties = doc.core_properties
        core_properties.author = ''
        sezhiPaper(doc)
        sezhiTtitle(doc, title)
        file = open(filePathTxt, 'w', encoding='utf-8')
        file.write(content)
        file.close()
        filer = open(filePathTxt, 'r', encoding='utf-8')
        file_lines = filer.readlines()
        for i, line in enumerate(file_lines):
            contentStr = line.replace('\n', '')
            contentStr = merrageLastPrage(i, file_lines, contentStr)
            contentStr = merrageLastPrageContentStr(i, file_lines, contentStr)
            contentStrA = markjudge(contentStr)
            contentStrB = deletesingle(contentStrA)
            contentStrC = deletePragram(paghraDownPath, contentStrB)
            sezhiParas(doc, contentStrC)
        doc.save(filePathDocx)
        filer.close()
    except:
        print('文档修改错误error')


# def getChatGPT_content(title, api_key):
#     openai.api_key = api_key
#     prompt = title
#     model_engine = "gpt-3.5-turbo"
#     # answer_language='zh-CN'
#     # try:
#     completions = openai.ChatCompletion.create(
#         model=model_engine,
#         messages=[
#             # {'role':'system','content':'你是一个文档生产者。'},
#             {'role': 'user', 'content': prompt}
#         ]
#     )
#
#     # message = completions.choices[0].text
#     # print(completions)
#     message = ''
#     for choice in completions.choices:
#         message += choice.message.content
#     print(message)
#     if 'AI' in message:
#         return ''
#     else:
#         return message


def mid_chatGPT(folderPath, title, api_key):
    try:

        if  '翻译' in title and (not '英' in title):
            lengthTitle = title.replace('翻译','中文翻译')
        elif '答案' in title:
            lengthTitle = '出十道'+title.replace('答案', '试题及答案')
        elif '试题' in title:
            lengthTitle = '出十道'+title
        elif '电子版' in title or '吉他谱' in title or '琴谱' in title \
            or '曲谱' in title or '筝曲' in title or '鼓谱' in title \
                or '简谱' in title or '电子版' in title or title.endswith('证书'):
            lengthTitle = title+',是什么'
        elif '手抄报' in title:
            lengthTitle = title + ',内容是什么'
        elif title.endswith('文案'):
            lengthTitle = title+'，八条以上'
        elif '字' in title:
            lengthTitle = title
        elif '句' in title or '词' in title:
            lengthTitle = title
        elif '简短' in title:
            lengthTitle = title
        else:
            lengthTitle = title+'，1200字以上'
        # content = str(getChatGPT_content(lengthTitle, api_key)).strip()
        content = getApiContentByTitleAndKey(api_key, lengthTitle).strip()

        if 'AI' in content or 'gpt-3.5' in content or 'sorry' in content \
                or '人工智能' in content or '对不起' in content or '抱歉' in content \
                or 'Sorry' in content or '智能辅助' in content \
                or '请提供更多的上下文信息' in content or 'The server' in content or 'OpenAI' in content \
                or '请提供更多信息' in content:
            time.sleep(5)
            content = getApiContentByTitleAndKey3 (api_key,lengthTitle).strip()
        if len(content) < 20:
            time.sleep(5)
            content = content + '\n' + getApiContentByTitleAndKey(api_key, lengthTitle).strip()
        if 'You exceeded your current quota' in content or 'please check your plan' in content:
            print('*************','chatGPT额度用光了'+api_key)
            content=''
        try:
            print(content)
            makeWordChatGPT(folderPath, title, content)
            time.sleep(5)
        except:
            print('chatGPT error')

    except Exception as e:
        print(e)
        if 'You exceeded your current quota' in str(e) or 'You requested a model' in str(e):
            print('api-key用光了了，或着api-key出问题了了了')
            time.sleep(100000)


def main_chatGPTSingle(title, folderPath, api_key):
    # file = open(titleFilePath, 'r', encoding='utf-8')
    # file_lines = file.readlines()
    # for i, line in enumerate(file_lines):
    #     if i >= start and i < last:
    mid_chatGPT(folderPath, title, api_key)


if __name__ == '__main__':
    api_key = "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"
    # 标题存放文件夹
    title = r'你好'
    folderPath = r'D:\文档\百度活动文档\百度html\其他网站'
    main_chatGPTSingle(title, folderPath, api_key)
