# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8

import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm

# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup


def getHtml(thmlpath, strUrl):

    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    contentText = str(soup.select('#contentText')[0]).replace('<br/>','</p><p>').replace('<br>','</p><p>')
    # contentText=re.sub('<span>(.*?)</span>','',content)
    title=str(soup.select('div.con_top > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentText)
    f.close()
def eachPageContent(url):
    contentText = ''
    try:
        requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        contentText = str(soup.select('#contents')[0]).replace('<br/>', '</p><p>').replace('<br>', '</p><p>')
    except:
        contentText=''
    return contentText
def getContent(thmlpath,url):
    contentStr=''
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    title = str(soup.select('#mainleft > h1')[0].text).split('\n')[0]
    print(title)
    title = title.replace('?', '').replace('？', '').replace('|', '').replace('"', '') \
        .replace('”', '').replace('*', '').replace('/', '').replace('\\', '').replace(' ', '').strip()
    for i in range(1,16):
        newUrl=url+'/'+str(i)
        content=eachPageContent(url)
        contentStr=contentStr+content
    thmlpath = thmlpath + '\\' + title + '.html'
    f = open(thmlpath, 'w', encoding='utf-8')
    f.write(contentStr)
    f.close()


def getListUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list=soup.select('#main > ul.ullist > li > a')
    for i,a in enumerate(a_list):
        href=a.get('href')
        newUrl='https://www.wendangwang.com'+href
        try:
            getContent(thmlpath, newUrl)
        except:
            print('error')
            continue

def main(txtPath,thmlpath):
#收藏本页
#范文参考网 www.fwwang.cn
    file=open(txtPath,'r')
    file_lines=file.readlines()
    for i,line in enumerate(file_lines):
        url=line.replace('\n','')
        newUrl=url+'%d'
        for i in range(0,100):
            number=i*25
            pageUrl=newUrl%number
            getListUrl(thmlpath, pageUrl)



if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    txtPath=r'./lianjie.txt'
    main(txtPath,thmlpath)
