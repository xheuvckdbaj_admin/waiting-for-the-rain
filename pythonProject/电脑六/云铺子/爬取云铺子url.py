# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"D:\pycharm\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\来搜一下\cookies.txt"
def getBrowserUrl(browser,newUrl):
    try:
        browser.get(newUrl)
    except:
        getBrowserUrl(browser, newUrl)
def getElement(browser,hrefs,file_handle):
    newHrefs=[]
    for e in range(0, len(hrefs)):
        try:
            browser.get(hrefs[e])
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="file-actions"]'))
            )

            newhref=browser.find_element_by_css_selector('div.file-actions > a.file-action.goto').get_attribute('href')
            newHrefs.append(newhref)
        except:
            print('资源不存在')
            continue

    for href in newHrefs:
        getBrowserUrl(browser,href)
        # thml=browser.find_element_by_xpath("//*").get_attribute("outerHTML")
        # print(thml)
        try:
            element = WebDriverWait(browser, 100).until(
                EC.presence_of_element_located((By.XPATH, '//a[@class="progress-button in-progress"]'))
            )
            newUrl = browser.find_element_by_xpath('//a[@class="progress-button in-progress"]').get_attribute("href")
        except:
            element = WebDriverWait(browser, 100).until(
                EC.presence_of_element_located((By.XPATH, '//div'))
            )
            newUrl=browser.current_url
        print(newUrl)
        if not 'html' in newUrl:
            file_handle.write(newUrl + '\n')



def getLinUrl(browser,law,url,key,file_handle):
    error=law
    # try:
        # browser = getBrowser(r'https://www.lingfengyun.com/')
        # 添加cookies
    try:
        browser.get(url)
        browser.find_element_by_xpath('//input[@class="search-bar-form-input"]').send_keys(key)
        browser.find_element_by_xpath('//input[@class="search-bar-form-input"]').send_keys(Keys.ENTER)
        # browser.find_element_by_xpath('//span[@class="search-bar-form-search"]').click()
        n = browser.window_handles  # 这个时候会生成一个新窗口或新标签页的句柄，代表这个窗口的模拟driver
        print('当前句柄: ', n)  # 会打印所有的句柄
        # browser.switch_to_window(n[-1])  # driver切换至最新生产的页面
        browser.switch_to.window(n[-1])
        time.sleep(2)

        element = WebDriverWait(browser, 100).until(
            EC.presence_of_element_located((By.XPATH, '//a[@class="ypzdn"]'))
        )
        url=browser.current_url
        eles=browser.find_elements_by_xpath('//div[@class="paging"]/a')
        text=eles[len(eles)-2].text
        allPage=int(text)
        print(allPage)
        for page in range(law,allPage+1):
            newUrl=url+'?paging='+str(page)
            error = page
            # newUrl = url % (key, page,way)
            print(page)
            getBrowserUrl(browser,newUrl)
            try:
                element = WebDriverWait(browser, 100).until(
                    EC.presence_of_element_located((By.XPATH, '//div/h3/a'))
                )
            except:
                continue
            eles=browser.find_elements_by_xpath('//div/h3/a')
            hrefs=[]
            for ele in eles:
                href=ele.get_attribute('href')
                hrefs.append(href)
            print(hrefs)
            getElement(browser, hrefs, file_handle)
    except:
        print('关键词没有内容')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(browser,law,key,path):
    url='http://www.yunpz.net/wangpan.html'
    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')
    getLinUrl(browser,law, url, key, file_handle)
    file_handle.close()


def main():
    browser = webdriver.Chrome()
    keys=[
'卫生标准',
'党员转正',
'合作意向',
'承包经营',
'实习生',
'护理制度',
'收费标准',
'高压电',
'简介模板',
'防火规范',
'分级管理',
'宪法宣誓',
'岗位竞聘',
'工作经验交流',
'注意事项',
'中医学概论',
'数电',
'模电',
'客户关系管理',
'新目标英语',
'服务方案',
'艺术教育',
'监察指南',
'责任险',
'审查要点',
'网络搭建',
'课程学习',
'培训大纲',
'自动控制原理',
'设计报告',
'安全法规',
'施工规范',
'数字乡村',
'装配图',
'传染病',
'市场洞察',
'装置设计',
'监察工作',
'放射治疗',
'通识教育',
'重点专科',
'工程育种',
'在线作业',
'信息公开',
'网站答辩',
'技术协议',
'生产环评',
'应链激励',
'研究报告',
'研讨发言',
'操作手册',
'演练案例',
'模拟定位',
'项目制度',
'市场需求',
'成人高考',
'评价标准',
'教育整顿',
'发展报告',
'防腐技术',
'大数据',
'职位名称规范',
'记账凭证',
'样本大全',
'档案材料',
'分级管控',
'数据可视化',
'策划书精选',
'模型分析',
'防控工作',
'线上营销',
'保证',
'措施']
    for i in range(0,len(keys)):
            print(keys[i])
            path=r'D:\文档\url\云铺子'
            # law初始值为1
            law=1
            if i==0:
                law=23
            login(browser,law,keys[i],path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
