# -*- coding:utf-8 -*-
import time

import requests
import json
import re

from bs4 import BeautifulSoup


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.content')[0])
            title=str(soup.select('div.mainleft_article > h1.title')[0].text).replace('?','')\
                .replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
    except:
        requests_text = 'hello'
        soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.infoco3 > ul > li > a')
    for a in a_list:
        # time.sleep(10)
        href = a.get('href')
        title = a.text
        getContent(thmlpath, href, title)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('div.article_list > ul.grid_list > li > a')
    for i in range(0,len(a_list)):
        time.sleep(1)
        a=a_list[i]
        href = a.get('href')
        getContent(thmlpath, href)
def getLabUrl(thmlpath,url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_last = soup.select('p#pages > a')[-1]
    href=a_last.get('href')
    newHref='http://www.fwdq.com'+href
    newurl=url+'index_(.*?).html'
    testUrl=url+'index_%d.html'
    number=int(re.findall(newurl,newHref)[0])
    print('一共有多少页',number)
    newUrls=[]
    for i in range(1,number+1):
        if i==1:
            nextUrl=url
        else:
            nextUrl=testUrl%i
        newUrls.append(nextUrl)
    for strUrl in newUrls:
        time.sleep(2)
        getHtml(thmlpath, strUrl)


def main(thmlpath):
    urls=[
        'http://www.fwdq.com/ziwojieshao/',
        'http: // www.fwdq.com / jianlimuban /',
       ' http: // www.fwdq.com / rudangshenqingshu /',
       'http: // www.fwdq.com / rutuanshenqingshu /',
       'http: // www.fwdq.com / sixianghuibao /',
       'http: // www.fwdq.com / ziwopingjia /',
       'http: // www.fwdq.com / zhufuyu /',
       'http: // www.fwdq.com / duhougan /',
       'http: // www.fwdq.com / ziwojianding /',
       'http: // www.fwdq.com / hetongfanben / lihunxieyishu /',
       'http: // www.fwdq.com / gerenjianli /',
       'http: // www.fwdq.com / cizhibaogao /',
       'http: // www.fwdq.com / shixibaogao /',
       'http: // www.fwdq.com / shuzhibaogao /',
       'http: // www.fwdq.com / diaochabaogao /',
       'http: // www.fwdq.com / diaoyanbaogao /',
       'http: // www.fwdq.com / shehuishijianbaogao /',
       'http: // www.fwdq.com / gongzuobaogao /',
       'http: // www.fwdq.com / zichabaogao /',
       'http: // www.fwdq.com / xindetihui /',
       'http: // www.fwdq.com / gongzuozongjie /',
       'http: // www.fwdq.com / rudangshenqingshu /',
       'http: // www.fwdq.com / rudangshici /',
       'http: // www.fwdq.com / rudangzizhuan /',
       'http: // www.fwdq.com / rudangzhuanzhengshenqingshu /',
       'http: // www.fwdq.com / rudangjieshaorenyijian /',
       'http: // www.fwdq.com / rudangzhiyuanshu /',
       'http: // www.fwdq.com / rutuanshenqingshu /',
       'http: // www.fwdq.com / rutuanzhiyuanshu /',
       'http: // www.fwdq.com / shaoxianduiruduishenqingshu /',
       'http: // www.fwdq.com / hetongfanben /',
       'http: // www.fwdq.com / hetongfanben / lihunxieyishu /',
       'http: // www.fwdq.com / hetongfanben / zufang /',
       'http: // www.fwdq.com / hetongfanben / fangwuzulin /',
       'http: // www.fwdq.com / hetongfanben / goufang /',
       'http: // www.fwdq.com / hetongfanben / laodong /',
       'http: // www.fwdq.com / hetongfanben / zulin /',
       'http: // www.fwdq.com / hetongfanben / maimai /',
       'http: // www.fwdq.com / hetongfanben / shigong /',
       'http: // www.fwdq.com / hetongfanben / xiaoshou /',
       'http: // www.fwdq.com / hetongfanben / jiekuan /',
       'http: // www.fwdq.com / qingshu /',
       'http: // www.fwdq.com / jiantaoshu /',
       'http: // www.fwdq.com / weituoshu /',
       'http: // www.fwdq.com / baozhengshu /',
       'http: // www.fwdq.com / chengnuoshu /',
       'http: // www.fwdq.com / zhengming /',
       'http: // www.fwdq.com / changyishu /',
       'http: // www.fwdq.com / gongzhengshu /',
       'http: // www.fwdq.com / huiguoshu /',
       'http: // www.fwdq.com / yixiangshu /',
       'http: // www.fwdq.com / danbaoshu /',
       'http: // www.fwdq.com / qingjiatiao /',
       'http: // www.fwdq.com / jietiao /',
       'http: // www.fwdq.com / chuangyejihuashu /',
       'http: // www.fwdq.com / qiuzhixin /',
       'http: // www.fwdq.com / yaoqinghan /',
       'http: // www.fwdq.com / zijianxin /',
       'http: // www.fwdq.com / ganxiexin /',
       'http: // www.fwdq.com / jieshaoxin /',
       'http: // www.fwdq.com / daoqianxin /',
       'http: // www.fwdq.com / biaoyangxin /',
       'http: // www.fwdq.com / weiwenxin /',
       'http: // www.fwdq.com / zhufuyu /',
       'http: // www.fwdq.com / yanjianggao /',
       'http: // www.fwdq.com / ziwojieshao /',
       'http: // www.fwdq.com / zhuchici /',
       'http: // www.fwdq.com / huanyingci /',
       'http: // www.fwdq.com / daoyouci /',
       'http: // www.fwdq.com / heci /',
       'http: // www.fwdq.com / fayangao /',
       'http: // www.fwdq.com / kouhao /',
       'http: // www.fwdq.com / guangbogao /',
       'http: // www.fwdq.com / daxieci /',
       'http: // www.fwdq.com / gongzuojihua /',
       'http: // www.fwdq.com / xuexijihua /',
       'http: // www.fwdq.com / ziwopingjia /',
       'http: // www.fwdq.com / ziwojianding /',
       'http: // www.fwdq.com / jianlimuban /',
       'http: // www.fwdq.com / duhougan /',
       'http: // www.fwdq.com / chengyu /',
       'http: // www.fwdq.com / ciyu /',
       'http: // www.fwdq.com / gerenjianli /',
    ]
    for i in range(0,len(urls)):
        print('这是第几的板块：',i)
        strUrl=urls[i].replace(' ','')
        getLabUrl(thmlpath,strUrl)


if __name__ == '__main__':
    thmlpath = r'E:\文档\自查报告网'
    main(thmlpath)
