
import  socket
import threading

from pythonProject.创作活动文档.服务器搭建.getApiContentVersion4 import getApiContentByTitleAndKey


def getContentChat(newclient):
    recv_data = newclient.recv(999256)
    rec_content = recv_data.decode('utf-8')
    # print('接受客户的数据：',rec_content)
    api_key = rec_content.split("#")[0]
    prompt = rec_content.replace(api_key, '').replace('#', '')
    send_content = getApiContentByTitleAndKey(api_key, prompt)
    # send_content='数据正在处理中----'
    send_data = send_content.encode('utf-8')
    newclient.send(send_data)
    newclient.close()
if __name__ == '__main__':
    tcp_socket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    # 设置心跳防止断开
    tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, True)
    tcp_socket.ioctl(socket.SIO_KEEPALIVE_VALS, (1, 60 * 1000, 30 * 1000))
    tcp_socket.bind(("",8080))
    tcp_socket.listen(999999256)
    while(1):
        newclient,ip_port=tcp_socket.accept()
        print('客户的ip和端口号：',ip_port)
        sub_thread=threading.Thread(target=getContentChat,args=(newclient,))
        sub_thread.setDaemon(True)
        sub_thread.start()


