# -*- coding:utf-8 -*-
import time
import requests
from bs4 import BeautifulSoup
# 获取真实预览地址turl
from selenium import webdriver
import sys
sys.setrecursionlimit(100000)
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup
def getBrowser(browser,strUrl):
    try:
        browser.get(strUrl)
    except:
        getBrowser(browser, strUrl)
# 获取标题和内容，并生成html文件
def getContent(thmlpath, url):
        try:
            print(url)
            requests_text = requests.get(url=url, headers=headers).content
            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('div.content')[0])
            title = soup.select('div.article > h1.title')[0].text.replace('?','').replace('？','').replace('|','')\
                .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa)
        except:
            print('error')


def getHtml(thmlpath, strUrl,number,browser,file_line):
    if number> 3200:
        number+=1
        # getContent(thmlpath, strUrl)
        try:
            getBrowser(browser,strUrl)
            href=browser.find_element_by_xpath('//div[@class="page"]/span[@class="next"]/a').get_attribute('href')
            print(href)
            file_line.write(href+'\n')
            getHtml(thmlpath, href, number, browser,file_line)
        except:
            print('该模块已完毕')
def main(thmlpath):

            browser = webdriver.Chrome()
            strUrls=[
                'https://www.tuanwen.com/jiaoxuezongjie/761063.html',
                'https://www.tuanwen.com/jiaoxuexinde/761059.html',
                'https://www.tuanwen.com/jiaoxuepingjia/756394.html',
                'https://www.tuanwen.com/jiaoxuemubiao/747204.html',
                'https://www.tuanwen.com/jiaoxuedagang/705335.html',
                'https://www.tuanwen.com/jiaoxuefangfa/756375.html',
                'https://www.tuanwen.com/kejian/759281.html',
                'https://www.tuanwen.com/jiaoan/761294.html',
                'https://www.tuanwen.com/gongzuojihua/761278.html',
                'https://www.tuanwen.com/shehuishijian/761233.html',
                'https://www.tuanwen.com/zhiyeguihuafanwen/760425.html',
                'https://www.tuanwen.com/html/shixibaogao/736447.html',
                'https://www.tuanwen.com/html/cizhixin/761260.html',
                'https://www.tuanwen.com/cizhishenqingshu/761248.html',
                'https://www.tuanwen.com/ziwojieshao/761271.html',
                'https://www.tuanwen.com/ziwopingjia/760076.html',
                'https://www.tuanwen.com/ziwojianding/761050.html',
                'https://www.tuanwen.com/zijianxin/761251.html',
                'https://www.tuanwen.com/html/qiuzhixin/761296.html',
                'https://www.tuanwen.com/xieyishu/761295.html',
                'https://www.tuanwen.com/weituohetong/761290.html',
                'https://www.tuanwen.com/diaochabaogao/754743.html',
                'https://www.tuanwen.com/shuzhibaogao/759713.html',
                'https://www.tuanwen.com/jihuashu/761080.html',
                'https://www.tuanwen.com/wenhouyu/759646.html',
                'https://www.tuanwen.com/ganyan/759958.html',
                'https://www.tuanwen.com/huanyingci/761093.html',
                'https://www.tuanwen.com/tuijianxin/753641.html',
                'https://www.tuanwen.com/zhufuyu/760276.html',
                'https://www.tuanwen.com/zhuci/757862.html',
                'https://www.tuanwen.com/hexin/755385.html',
                'https://www.tuanwen.com/daxieci/761266.html',
                'https://www.tuanwen.com/changyishu/761282.html',
                'https://www.tuanwen.com/ganxiexin/761239.html',
                'https://www.tuanwen.com/yaoqinghan/761298.html',
                'https://www.tuanwen.com/daoqianxin/761228.html',
                'https://www.tuanwen.com/biaoyangxin/761255.html',
                'https://www.tuanwen.com/weiwenxin/761279.html',
                'https://www.tuanwen.com/daoyouci/761121.html',
                'https://www.tuanwen.com/biyeganyan/760639.html',
                'https://www.tuanwen.com/guanggaoci/757907.html',
                'https://www.tuanwen.com/zhengming/760628.html',
                'https://www.tuanwen.com/xindetihui/761245.html',
                'https://www.tuanwen.com/shenqingshu/761252.html',
                'https://www.tuanwen.com/jiantaoshu/761273.html',
                'https://www.tuanwen.com/huodongfangan/761289.html',
                'https://www.tuanwen.com/huodongzongjie/760337.html',
                'https://www.tuanwen.com/shijianzongjie/761237.html'
            ]
            for i in range(0,len(strUrls)):
                txtFilePath = thmlpath + '\\' + str(time.time()) + '.txt'
                file_line = open(txtFilePath, 'w')
                strUrl=strUrls[i]
                number = 1
                file_line.write(strUrl + '\n')
                getHtml(thmlpath, strUrl,number,browser,file_line)
                file_line.close()
def mainGetHtml(thmlpath,txtFilepath):
    filetxt=open(txtFilepath,'r')
    file_lines=filetxt.readlines()
    for file_line in file_lines:
        strUrl=file_line.replace('\n','').strip()
        getContent(thmlpath, strUrl)
if __name__ == '__main__':
    # 编辑推荐
    # 发表时间
    # www.f132.com
    # 范文资讯网
    thmlpath = r'E:\文档\自查报告网'
    txtFilepath = r'E:\文档\自查报告网2\all.txt'
    main(thmlpath)
    # mainGetHtml(thmlpath, txtFilepath)
