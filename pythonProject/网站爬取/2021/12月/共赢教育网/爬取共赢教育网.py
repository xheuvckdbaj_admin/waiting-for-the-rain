# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


filePath=r'./cookies.txt.txt'
def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)



def getContent(thmlpath, href,title):
    try:
        requests_text = str(requests.get(url=href, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        content = str(soup.select('div.gydetails-content.gymargin-b20')[0])
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w',encoding='utf-8')
        f.write(content)
        f.close()
    except:
        print('错误')
def getAllUrl(thmlpath,allUrl):
    try:
        requests_text=str(requests.get(url=allUrl,headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_lists=soup.select('div.gyarticle > h4.gyfn > a')
        for a in a_lists :
            href=a.get('href')
            title=a.text.replace('?','').replace('？','').replace('|','')\
                        .replace('”','').replace('*','').replace('/','').replace('\\','')
            print(title)
            getContent(thmlpath, href,title)
    except:
        print('error')

def getAllPage(thmlpath):
    for number in range(68, 82):
        url = 'https://www.gywlwh.com/list-%d-%d.html'
        strUrl = 'https://www.gywlwh.com/list-%d-1.html'
        newurl = strUrl % number
        print(newurl)
        requests_text = str(requests.get(url=newurl, headers=headers).content.decode(encoding='utf-8'))
        soup = BeautifulSoup(requests_text, 'lxml')
        pageNumber = int(soup.select('div.gypaginate > a')[-2].text)
        for page in range(1, pageNumber):
            allUrl = url % (number, page)
            getAllUrl(thmlpath,allUrl)
def main(thmlpath):
    getAllPage(thmlpath)


if __name__ == '__main__':
    #https://www.dyhzdl.cn/
    #下面是本站
    #本文来源
    thmlpath = r'D:\文档\爬取网站\共赢教育网'
    main(thmlpath)
