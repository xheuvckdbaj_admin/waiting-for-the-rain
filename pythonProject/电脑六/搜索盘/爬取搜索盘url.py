# -*- coding:utf-8 -*-
import re
import time
import urllib

import requests
import selenium

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import addLaisoucookies
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'
,'cookies.txt':'HMACCOUNT_BFESS=3ABD19FD749A7C2B; BDUSS_BFESS=JhdVg5cTlxMGZlVmpsY1NVRTg5eE9mQ252Z0VZVUlGTHFzOXZtV0VLc1VJdzloRVFBQUFBJCQAAAAAAQAAAAEAAABZuyE716jXor3M0~01MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABSW52AUludgc3; BCLID_BFESS=9963759693009502146; BDSFRCVID_BFESS=67kOJeC629ysVhnen-M2riNV1fAip3QTH6f3hZ7kbvCi0s5z3ClwEG0P-x8g0KuMsTnFogKKKmOTHcPF_2uxOjjg8UtVJeC6EG0Ptf8g0f5; H_BDCLCKID_SF_BFESS=tbA8_C0XJCK3DnCk5-nV5nQH5Mnjq5Kf22OZ0l8KtDTBqUQdh4o_qfrXKxrwLjoh0C5joMjmWIQthnnLjPRD5xttDh-thjJmaTv4KKJxH4PWeIJo5fc53-CzhUJiBMnLBan73MJIXKohJh7FM4tW3J0ZyxomtfQxtNRJ0DnjtnLhbRO4-TFKD5bBDf5; BAIDUID_BFESS=4938295D3E644D71A6BF6654DBC8A411:FG=1'}
filePath=r"D:\pycharm\waiting-for-the-rain\pythonProject\网站爬取\2021\7\20\来搜一下\cookies.txt"

def getElement(browser,hrefs,file_handle):
    for e in range(0, len(hrefs)):
        try:
            browser.get(hrefs[e])
            element = WebDriverWait(browser, 10).until(
                EC.presence_of_element_located((By.XPATH, '//div[@class="down"]'))
            )

            newhref=browser.find_element_by_css_selector('div.down > a').get_attribute('href')
            print(newhref)
            file_handle.write(newhref+'\n')
        except:
            print('资源不存在')
            continue



def getLinUrl(law,url,key,file_handle):

            browser = webdriver.Chrome()
            newUrl=url%key
            browser.get(newUrl)

            element = WebDriverWait(browser, 100).until(
                    EC.presence_of_element_located((By.XPATH, '//div[@class="file-item"]'))
                )

            eles=browser.find_elements_by_xpath('//div[@class="file-item"]/div[@class="title"]/h2/a[@class="object-link fpm"]')
            hrefs=[]
            for ele in eles:
                href=ele.get_attribute('href')
                hrefs.append(href)
            print(hrefs)
            getElement(browser, hrefs, file_handle)
    # except:
    #     print('获取网页失败')
    #     getLinUrl(error, url, so_token,key, ft, way, file_handle)

def login(law,key,path):
    url='https://www.sosuopan.cn/search?q=%s'
    newPath = path + '\\' +key+ str(time.time()) + '.txt'
    file_handle = open(newPath, mode='w')
    getLinUrl(law, url, key, file_handle)
    file_handle.close()


def main():
    keys=['我为群众办实事',
'说明指导书',
'国学概论',
'团队愿景',
'教学计划',
'处理方案',
'文言文',
'计算方法',
'施工技术',
'真题',
'合同',
'商务英语',
'分项汇编',
'理疗',
'管理制度',
'习题参考',
'实施计划',
'标准',
'筛查与评估',
'问题探讨',
'大学英语',
'课后习题',
'写作模板',
'高等数学',
'试卷及答案',
'强化训练',
'教学分析',
'备考',
'知识考点',
'教学进度表',
'第一学期',
'案例分析',
'党员',
'重要讲话',
'建造师',
'检查细则',
'实训报告',
'实验报告',
'绩效评价',
'文学选读',
'离散数学',
'护理服务',
'法律法规',
'市场分析',
'策划书',
'说明书',
'解题方法',
'电路设计',
'抗洪',
'风险管理',
'阅读理解',
'教材讲稿',
'应用文写作',
'模拟试题',
'期末试题',
'检验流程',
'同步练习',
'防控考试',
'建模',
'验资报告',
'教育专题',
'人力资源',
'高级评审',
'一轮复习',
'行政规划',
'数据结构',
'工作质量',
'课程设计',
'施工方案',
'管理制度',
'培养计划',
'课程教学大纲',
'家书',
'执法业务培训',
'人事制度',
'招聘试题',
'寒假',
'中职实习',
'建设章程',
'有效策划',
'应急救援',
'万能模板',
'申请书',
'广告销售',
'寒假生活答案',
'规格书',
'知识框架',
'提纲',
'缴费制度',
'安全文化学习手册',
'安全教育',
'员工规章',
'专插本',
'前景预测',
'设计规范',
'重点整理',
'会议纪要',
'设计意图',
'教学指导',
'必刷题',
'环卫行业',
'指标',
'综合理论',
'教学规划',
'单词学习',
'知识点',
'阶段性测试',
'决策管理',
'自查报告',
'责任险',
'复习资料',
'病例分析',
'注册计量师',
'资源配置',
'线上营销',
'竣工验收',
'质量调研',
'分级管理',
'模型分析',
'许可协议',
'情况说明',
'预测预警',
'技术标准',
'年度工作总结'
]
    for key in keys:
            # key='中考'
            print(key)
            path=r'D:\文档\搜索盘url'
            law=1
            login(law,key,path)


if __name__ == '__main__':
    thmlpath = r'G:\文档\test\筑龙学社'
    main()
