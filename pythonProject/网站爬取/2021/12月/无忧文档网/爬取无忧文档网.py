# -*- coding:utf-8 -*-
import docx
import requests
import json
import re
import time

from bs4 import BeautifulSoup
from docx import Document
from docx.oxml.ns import qn

from docx.shared import Cm
import  addLingcookies
# 获取真实预览地址turl
from selenium import webdriver

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


filePath=r'./cookies.txt'
def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url
def getBrowserUrl(browser, newUrl):
    try:
        browser.get(newUrl)
    except:
        time.sleep(0.5)
        getBrowserUrl(browser, newUrl)
def set_chrome_pref(path):
    prefs = {"download.default_directory":path}
    option = webdriver.ChromeOptions()
    option.add_argument("--user-data-dir="+r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)   # 打开chrome浏览器
    time.sleep(10)
    return driver



def getContent(browser,thmlpath, strUrl):
    try:
        getBrowserUrl(browser, strUrl)
        # addLingcookies.addCookies(browser, strUrl, filePath)
        title=browser.find_element_by_xpath('//div[@id="article"]/h1').text.replace('?','').replace('？','').replace('|','')\
                    .replace('”','').replace('*','').replace('/','').replace('\\','')
        print(title)
        content=str(browser.find_element_by_xpath('//div[@id="article"]/div[@id="article_body"]').get_attribute('innerHTML'))
        contentTxt = re.sub('<span style="color:red;">(.*?)</span>', '', content)
        thmlpath = thmlpath + '\\' + title + '.html'
        f = open(thmlpath, 'w')
        f.write(contentTxt)
        f.close()
    except:
        print('错误')

def main(thmlpath):
            browser=set_chrome_pref(thmlpath)
            #number=1
            strUrl='https://www.51jzrc.cn/show-18-%d-1.html'
            getBrowserUrl(browser, 'https://www.51jzrc.cn/show-18-10-1.html')
            addLingcookies.addCookies(browser, 'https://www.51jzrc.cn/show-18-10-1.html', filePath)
            for i in range(44718,629282):
                print('--------------',i)
                time.sleep(1)
                newUrl=strUrl%i
                getContent(browser,thmlpath, newUrl)


if __name__ == '__main__':
    thmlpath = r'D:\文档\爬取网站\无忧文档网'
    main(thmlpath)
