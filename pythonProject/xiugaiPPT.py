
from pptx import Presentation
from pptx.util import Cm, Pt

# 新建PPT
prs = Presentation(r'F:\test\1.主题一   第一次世界大战和战后初期的世界.pptx')
prs1 = Presentation()
blank_slide_layout = prs1.slide_layouts[6]
slide=prs.slides.add_slide(blank_slide_layout)
left = top = width = height = Cm(3)
# slide.shapes.add_textbox(距离左边，距离顶端，宽度，高度)
text_box = slide.shapes.add_textbox(left, top, width, height)

for slid in prs.slides:

    pic = slid.shapes.add_picture('hh.jpg', left-0.1*prs.slide_width, top, height = prs.slide_height)
print(len(prs.slides))
# # 调整文本框背景颜色
# fill = text_box.fill
# fill.solid()  # 纯色填充
# fill.fore_color.rgb = RGBColor(247, 150, 70)
# tf = text_box.text_frame
# tf.text = '这是一段文本框里的文字'
#
# # 文本框边框样式调整
# line = text_box.line
# line.color.rgb = RGBColor(255, 0, 0)
# line.width = Cm(0.3)
#
# # 文本框样式调整
# tf.margin_bottom = Cm(0.1)  # 下边距
# tf.margin_left = 0  # 左边距
# tf.vertical_anchor = MSO_VERTICAL_ANCHOR.BOTTOM  # 对齐文本方式：底端对齐
# tf.word_wrap = True  # 文本框的文字自动对齐
#
# # 段落对齐调整
# p = tf.add_paragraph()
# p.text = '这是第一段文字'
# # p.alignment = PP_PARAGRAPH_ALIGNMENT  # 对齐方式
#
# # 字体样式调整
# p.text = '这是第二段文字'
# p.font.name = '微软雅黑'  # 字体名称
# p.font.bold = True  # 是否加粗
# p.font.italic = True  # 是否斜体
# p.font.color.rgb = RGBColor(255, 0, 0)  # 字体颜色
# p.font.size = Pt(20)  # 字体大小
prs.save(r'F:\test\test1.pptx')
