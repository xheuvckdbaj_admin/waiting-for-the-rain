import re
replacePath=r'./replace.txt'
def replaceStr(title,replacePath):
    file=open(replacePath,'r',encoding='utf-8')
    file_lines=file.readlines()
    for line in file_lines:
        key=line.replace('\n','')
        title=title.replace(key,'')
    return title
def deleteEnd(title):
    if title.endswith('十') or title.endswith('一') or title.endswith('二') or title.endswith('三') or title.endswith('四') \
        or title.endswith('五') or title.endswith('六') or title.endswith('七') \
        or title.endswith('八') or title.endswith('九') :
        title=title.replace('二十','').replace('十九', '').replace('十八','').replace('十七','').replace('十六','')\
            .replace('十五','').replace('十四','').replace('十三','').replace('十二','').replace('十一','')\
            .replace('十','').replace('九','').replace('八','').replace('七','').replace('六','')\
            .replace('五','').replace('四','').replace('三','').replace('二','').replace('二十一','')\
            .replace('_1','').replace('_2','').replace('_3','').replace('_4','').replace('_5','')\
            .replace('_6','').replace('_7','').replace('_8','').replace('_9','').replace('_10','')
    return title
def modifyTitle(txtPath,newTxtPath):
    txt_file=open(txtPath,'r',encoding='utf-8')
    file_lines=txt_file.readlines()
    new_txt_file = open(newTxtPath, 'w', encoding='utf-8')

    for line in file_lines:
        title=line.replace('\n','').split('，')[0].split(',')[0]
        title=re.sub('\d*篇','',re.sub('\(.*?\)','',re.sub('','',re.sub('（.*?）','',re.sub('【.*?】','',title)))))
        title=re.sub('[.*?]','',title)
        title=replaceStr(title,replacePath)
        title=deleteEnd(title)
        print(title)
        new_txt_file.write(title+'\n')
    new_txt_file.close()
    txt_file.close()

def main():
    txtPath=r'D:\文档\百度付费上传文档\标题\title\all.txt'
    newTxtPath=r'D:\文档\百度付费上传文档\标题\title\new.txt'
    modifyTitle(txtPath,newTxtPath)
if __name__ == '__main__':
    main()