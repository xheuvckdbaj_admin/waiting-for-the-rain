# -*- coding:utf-8 -*-
# !/usr/bin/env python
# coding=utf-8
import os
import shutil
import sys
import time
import urllib

import pyautogui
import pyperclip
import win32com
import win32con
import win32gui
from playwright.sync_api import sync_playwright
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os

from playwright.sync_api import Playwright, sync_playwright, expect
import time

from pythonProject.FindFile import find_file
from pythonProject.FindFile import find_file
from pythonProject.创作活动文档.playwright创作活动文档.addCookiesPlay import addCookiesPlay
from pythonProject.创作活动文档.playwright创作活动文档.上传单个账号文档检查账号是否传满 import \
    mainTitleClassifyUploadApiGuish
from pythonProject.创作活动文档.playwright创作活动文档.删除单个账号已被传过的文档 import mainTitleClassifyUploadDelete
from pythonProject.创作活动文档.方式一下载百度活动文档.获取driver.getDriver import getDriver
from pythonProject.定产活动文档.获取活动文档的标题 import readTxt
from pywinauto import Desktop
from pywinauto.keyboard import *


def getBrowserNow():
    options = webdriver.ChromeOptions()
    options.add_argument("--user-data-dir=" + r"C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/")
    browser = webdriver.Chrome(options=options)
    time.sleep(1)
    return browser


def panduanTitle(title):
    panduan = True
    if '表' in title or 'ppt' in title or 'PPT' in title or '招生简章' in title or '年级' in title \
            or '图' in title or '.' in title or '×' in title or (len(title) < 5):
        panduan = False
    return panduan


def addCookies(browser, url, filePath):
    browser.get(url)
    time.sleep(1)
    cookies = readTxt.readTxt(filePath)
    for item in cookies.split(';'):
        cookie = {}
        itemname = item.split('=')[0]
        iremvalue = item.split('=')[1]
        cookie['domain'] = '.baidu.com'
        cookie['name'] = itemname.strip()
        cookie['value'] = urllib.parse.unquote(iremvalue).strip()
        browser.add_cookie(cookie)
    browser.get(url)


def getNewWindow(browser, xpathStr, timeNumber):
    n = browser.window_handles
    browser.switch_to.window(n[-1])
    time.sleep(1)
    browser.switch_to.window(browser.window_handles[0])
    element = WebDriverWait(browser, timeNumber).until(
        EC.presence_of_element_located(
            (By.XPATH, xpathStr)
        )
    )


def getNumberPager(browser):
    try:
        xpathStr = '//ul[@class="el-pager"]/li[@class="number"]'
        pagerNumber = int(browser.find_elements(By.XPATH, xpathStr)[-1].text) - 1
        return pagerNumber
    except:
        return 0


def deleteWord(folderPath, title):
    filePath = folderPath + '\\' + title + '.docx'
    try:
        os.remove(filePath)
    except:
        print('无法删除' + folderPath)


def ctrlx(oldFilePath, newPath):
    # if oldFilePath.endswith('html'):
    # 移动文件夹示例
    shutil.move(oldFilePath, newPath)


def getFileNameStr(folderPath):
    keyStrs = []
    filePaths = find_file(folderPath, [])
    if len(filePaths) >= 1:
        for filePath in filePaths:
            titleStr = os.path.basename(filePath).replace('.docx', '')
            keyStrs.append(titleStr)
    return keyStrs











def ctrlx(oldFilePath,newPath):
    #if oldFilePath.endswith('html'):
        #移动文件夹示例
        shutil.move(oldFilePath,newPath)
def submitword(page, file_chooser, fileDocxPath,successFloder):
    file_chooser.set_files(fileDocxPath)
    page.wait_for_timeout(5000)
    page.get_by_role("radio", name="VIP文档").click()
    time.sleep(1)
    page.get_by_role('button', name='确认提交').click()
    print('上传成功：' + fileDocxPath)
    # filePathxs = find_file(folderPath, [])
    # print('已经成功上传' + str(dataNumber) + '个文档')
    # dataNumber1 = len(filePathxs)
    # print('此次成功上传' + str(dataNumber1) + '个文档')
    # together = dataNumber + dataNumber1
    # print('一共成功上传' + str(together) + '个文档')
    # longx = dataNumber1
    # for filePathx in filePathxs:
    try:
        ctrlx(fileDocxPath,successFloder)
    except:
        print('剪切不掉' + fileDocxPath)
        os.remove(fileDocxPath)


def deleteFile(wordFolderPath):
    filePaths = find_file(wordFolderPath)
    for filepath in filePaths:
        try:
            os.remove(filepath)
        except:
            print('删除失败', filepath)


def singlyUpload(page, titleFolderPath, cookiesPath,successFloder):
    xpathStrButton = '//div[@class="upload-doc btn-posution"]/div[@class="add-new-btn add-new-btn-right"]/button'
    xpathStrEle = '//div[@class="doc-row"]/div[@class="upload-doc"]/button'
    try:
        page.locator(xpathStrButton).nth(0).first.click()
        with page.expect_file_chooser() as fc_info:
            page.locator(xpathStrButton).nth(0).first.click()
        panduanUpload = True
        file_chooser = fc_info.value
        fileDocxPath = titleFolderPath
        submitword(page, file_chooser, fileDocxPath,successFloder)
    except:
        print('上传失败')


def xunhuanPage(page, titleFolderPath, cookiesPath,successFloder):
    singlyUpload(page, titleFolderPath, cookiesPath,successFloder)


def getNewPage(page, url):
    try:
        page.goto(url)
    except:
        time.sleep(1)
        getNewPage(page, url)


def uploadFile(page, titleFolderPath, cookiesPath,successFloder):
    panduanUpload = False
    url = 'https://cuttlefish.baidu.com/shopmis?_wkts_=1679576168528&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation'
    getNewPage(page, url)
    time.sleep(0.5)

    xunhuanPage(page, titleFolderPath, cookiesPath,successFloder)


def makeFolder(filePath):
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def run(context, page, cookiesPath):
    # Go to https://www.baidu.com/
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1679576742834&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation")
    cookies = addCookiesPlay(cookiesPath)
    # 设置cookies
    context.add_cookies(cookies)
    time.sleep(0.5)
    getNewPage(page,
               "https://cuttlefish.baidu.com/shopmis?_wkts_=1679576742834&bdQuery=%E7%99%BE%E5%BA%A6%E6%96%87%E6%A1%A3#/commodityManage/documentation")
    xpathStrChuanzuo = '//li[@class="el-menu-item is-active"]/span[@class="el-main-new"]'
    try:
        page.locator("#app section").click()
    except:
        page.locator(xpathStrChuanzuo).click()
        print('没找打我知道啦按钮')
    return page


def mainTitleClassifyUpload(playwright: Playwright, folderPath, cookiesPath,successFloder) -> None:
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    # Open new page
    page = context.new_page()
    page.set_default_timeout(6000)
    run(context, page, cookiesPath)
    # filePath = folderPath + '\\' + str(1) + '.txt'
    titleFolderPaths = os.listdir(folderPath)
    time.sleep(1)
    try:
        page.get_by_role("button", name="我知道啦").click()
    except:
        print('')
    if not len(titleFolderPaths) == 0:
        dataNumber = 0
        for i, titleFolderPathName in enumerate(titleFolderPaths):
            if i < 100:
                titleFolderPath = os.path.join(folderPath, titleFolderPathName)
                uploadFile(page, titleFolderPath, cookiesPath,successFloder)


def mainTitleClassifyUploadApi(folderPath, cookiesPath,successFloder):
    with sync_playwright() as playwright:
        mainTitleClassifyUpload(playwright, folderPath, cookiesPath,successFloder)
    # mainTitleClassifyUploadDelete(folderPath, cookiesPath, panduanFilePath)


if __name__ == '__main__':
    folderPath = r'D:\文档\百度付费上传文档\修改后百度活动文档'
    # panduanFilePath = r'D:\文档\百度活动文档\百度活动文档上传\账号\账号.txt'
    # newPath = r'E:\文档\百度活动文档\修改后百度活动文档':\文档\百度活动文档2\百度活动文档上传\4高校与高等教育
    successFloder=r'D:\文档\百度付费上传文档\百度上传成功'
    cookiesPath = './baiduCookies1.txt'

    mainTitleClassifyUploadApi(folderPath, cookiesPath,successFloder)
