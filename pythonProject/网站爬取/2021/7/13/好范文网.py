# -*- coding:utf-8 -*-

import requests
import json
import re

from bs4 import BeautifulSoup


# 获取真实预览地址turl
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36'}


def groupUrl(str):
    url = f'http://www.jiaoyumao.com/' + str + '/'
    return url


# 获取soup
def getSoup(url):
    requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='utf-8'))
    soup = BeautifulSoup(requests_text, 'lxml')
    return soup

# 获取标题和内容，并生成html文件
def getContent(thmlpath, url,title):

        #time.sleep(10)
        #url='http://m.pincai.com/article/2300000.htm'
        try:
            requests_text = str(requests.get(url=url, headers=headers).content.decode(encoding='gbk'))

            soup = BeautifulSoup(requests_text, 'lxml')
            list_soupa = str(soup.select('.content')[0])
            list_soupa1=re.sub('<li><a href="(.*?)" target="_blank">(.*?)</a></li>','',list_soupa)
            print(title)
            print(url)
            thmlpath=thmlpath+'\\'+title+'.html'
            f = open(thmlpath, 'w', encoding='utf-8')
            f.write(list_soupa1)
        except:
            print('error')

# 获取最终网址
def getUrl(thmlpath,strUrl):
    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
    except:
        requests_text = 'hello'
        soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.infoco3 > ul > li > a')
    for a in a_list:
        # time.sleep(10)
        href = a.get('href')
        title = a.text
        getContent(thmlpath, href, title)
def getHtml3(thmlpath, strUrl):
    getUrl(thmlpath, strUrl)

    try:
        requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
        soup = BeautifulSoup(requests_text, 'lxml')
        a_list = soup.select('.page_list > a')
        hrefStr=a_list[len(a_list)-1].get('href')
        numstr=re.match('(.*?)_(.*?)_(.*?).htm',hrefStr).group(3)
        href='http://www.haoword.com'+hrefStr.split(numstr)[0]+'%d.htm'
        for i in range(2,int(numstr)+1):
            newUrl=href%i
            getUrl(thmlpath, newUrl)
    except:
        print('error')

def getHtml2(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.news_title > p > a')
    for i in range(0, len(a_list)):
        a = a_list[i]
        href = a.get('href')
        getHtml3(thmlpath, href)

def getHtml(thmlpath, strUrl):
    requests_text = str(requests.get(url=strUrl, headers=headers).content.decode(encoding='gbk'))
    soup = BeautifulSoup(requests_text, 'lxml')
    a_list = soup.select('.news_title > p > a')
    for i in range(0,len(a_list)):
        a=a_list[i]
        href = a.get('href')
        getHtml2(thmlpath, href)

def main(thmlpath):
            #number=1
            strUrl='http://www.haoword.com/'
            getHtml(thmlpath, strUrl)


if __name__ == '__main__':
    thmlpath = r'D:\文档\5U文学网'
    main(thmlpath)
